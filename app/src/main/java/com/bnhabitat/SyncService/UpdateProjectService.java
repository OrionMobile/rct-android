package com.bnhabitat.SyncService;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.bnhabitat.connection.ConnectionManager;
import com.bnhabitat.models.UpdateProject;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONObject;

import java.io.InputStream;

import de.greenrobot.event.EventBus;

/**
 * Created by developer15 on 9/9/2016.
 */
public class UpdateProjectService extends IntentService {
    private String accessToken = Constants.UTILITY_TOKEN;
    private String userId="";
    private String modifiedDate="";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     *   Used to name the worker thread, important only for debugging.
     */
    public UpdateProjectService() {
        super("UpdateProjectService");
        Log.e("UpdateProjectService","name");
    }

    @Override
    protected void onHandleIntent(Intent intent) {



    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("UpdateProjectService","onCreate");



    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        try{
            String projectId = intent.getStringExtra("projectId");
            modifiedDate= intent.getStringExtra("modifiedDate");
            userId= PreferenceConnector.getInstance(getApplicationContext()).loadSavedPreferences(Constants.USER_ID,"");
          //  Log.e("UpdateProjectService",projectId);
            new CommonAsync(getApplicationContext(),
                    Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_MODIFIED +projectId, "",
                    "",
                    Urls.URL_PROJECT_MODIFIED,
                    Constants.GET).execute();
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public class CommonAsync extends AsyncTask<Void, Void, Void> {


        private ConnectionManager connectionManager;
        private Context context;
        private String url;
        //private OnAsyncResultListener onAsyncResultListener;
        private String input = "";
        private InputStream inputStream;
        private ProgressDialog authDialog;
        private String message;
        private String output = "";
        private String which = "";
        private int requestType;

        public CommonAsync(Context context,
                           String url, String input,
                           String message,
                           String which,
                           int requestType) {
            // TODO Auto-generated constructor stub
            this.context = context;
            this.url = url;
            this.input = input;
            this.message = message;
            //this.onAsyncResultListener=onAsyncResultListener;
            this.which = which;
            connectionManager = ConnectionManager.getInstance(context);
            this.requestType = requestType;

            if (this.url.contains("utilities"))
                accessToken = Constants.UTILITY_TOKEN;
            else
                accessToken = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.APP_AUTH_KEY, "c4cdd3aa-6b9c-4eac-98b9-077fe2fad6ef");


            Log.e("Data", "url: " + url + "  input: " + input);
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            try {


                if (requestType == Constants.GET) {

                    inputStream = connectionManager.connectionEstablishedGet(url, accessToken);

                } else {

                    if (input.trim().equalsIgnoreCase(""))
                        inputStream = connectionManager.connectionEstablished(url, accessToken);
                    else
                        inputStream = connectionManager.connectionEstablished(url, input, accessToken);

                }

                output = connectionManager.converResponseToString(inputStream);
                Log.e("JSON", output);

            } catch (Exception e) {

                output = "";
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            Log.e("RESULT", output);

            if (null != output && !output.trim().equalsIgnoreCase("")) {

                if (which.equalsIgnoreCase(Urls.URL_PROJECT_MODIFIED)) {

                    try {

                        JSONObject jsonObject = new JSONObject(output);


                        if (jsonObject.getString("StatusCode").equalsIgnoreCase("200")) {

                            String resultString = jsonObject.getString("Result");
                            if (!resultString.equalsIgnoreCase(modifiedDate))
                                EventBus.getDefault().post(new UpdateProject());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }


        }

    }

}
