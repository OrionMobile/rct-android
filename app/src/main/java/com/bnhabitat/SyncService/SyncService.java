package com.bnhabitat.SyncService;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bnhabitat.data.contacts.AddressTable;
import com.bnhabitat.data.contacts.ContatctTable;
import com.bnhabitat.data.contacts.EmailTable;
import com.bnhabitat.data.contacts.LocalAddressTable;
import com.bnhabitat.data.contacts.LocalContatctTable;
import com.bnhabitat.data.contacts.LocalEmailTable;
import com.bnhabitat.data.contacts.LocalPhoneTable;
import com.bnhabitat.data.contacts.PhoneTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.ui.activities.MainDashboardActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.everything.providers.android.contacts.Contact;
import me.everything.providers.core.Data;

/**
 * Created by ysharma on 8/12/2017.
 */

public class SyncService extends Service implements CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener {

    private static final String TAG = "SyncService";
    private Timer timer;
    private Data<Contact> datacontacts;
    private List<Contact> contactLists;
    private ArrayList<ContactsModel> contactsFirebaseModels;
    private ArrayList<ContactsModel> localContactsFirebaseModels;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        timer = new Timer();
//        timer.schedule(new DelayedTask(), 300, 30000);// 300000 is 5min
        contactsFirebaseModels= ContatctTable.getInstance().getNoSynced();
        localContactsFirebaseModels = LocalContatctTable.getInstance().getNoSynced();

        for (int index=0;index<contactsFirebaseModels.size();index++){
            contactsFirebaseModels.get(index).setPhones(PhoneTable.getInstance().getPhones(contactsFirebaseModels.get(index).getId()));
            contactsFirebaseModels.get(index).setEmails(EmailTable.getInstance().getEmails(contactsFirebaseModels.get(index).getId()));
            contactsFirebaseModels.get(index).setAddresses(AddressTable.getInstance().addressesArrayList(contactsFirebaseModels.get(index).getId()));


        }

        for (ContactsModel contactsModel:localContactsFirebaseModels){
            ArrayList<ContactsModel.Phones> phoneModel = LocalPhoneTable.getInstance().getPhones(contactsModel.getId());
            ArrayList<ContactsModel.Email> emails = LocalEmailTable.getInstance().getEmails(contactsModel.getId());
            ArrayList<AddressModel> addresses = LocalAddressTable.getInstance().getAddress(contactsModel.getId());
            contactsModel.setPhones(phoneModel);
            contactsModel.setEmails(emails);
            contactsModel.setAddresses(addresses);

            contactsModel.setId("0");
            contactsFirebaseModels.add(contactsModel);
        }


        if(contactsFirebaseModels.size()>0)
            onPostContacts(false);
        else
            stopService(new Intent(this,SyncService.class));

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopService(new Intent(getApplicationContext(),SyncService.class));
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_ALL_CONTACTS)) {
                contactsFirebaseModels.clear();
                try {
                    JSONArray projectsArray = new JSONArray(result);


                    for (int index = 0; index < projectsArray.length(); index++) {
                        JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                        ContactsModel contactsModel = new ContactsModel();
                        contactsModel.setId(jsonObject1.getString("Id"));
                        contactsModel.setFirstName(jsonObject1.getString("FirstName"));
                        contactsModel.setLastName(jsonObject1.getString("LastName"));
                        contactsModel.setDateOfBirthStr(jsonObject1.getString("DateOfBirthStr"));

                        JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("Addresses");
                        ArrayList<AddressModel> addresses = new ArrayList();
                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                            AddressModel addresses1 = new AddressModel();
                            JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);
                            addresses1.setId(dataObj.getString("Id"));
                            addresses1.setContactId(dataObj.getString("ContactId"));
                            addresses1.setStateName(dataObj.getString("StateName"));
//                            addresses1.setCountryName(dataObj.getString("CountryName"));
                            addresses1.setCountryId(dataObj.getString("CountryId"));

                            addresses1.setCity_name(dataObj.getString("CityName"));
                            addresses1.setAddress1(dataObj.getString("Address1"));
                            addresses1.setAddress2(dataObj.getString("Address2"));
                            addresses1.setAddress3(dataObj.getString("Address3"));
                            addresses1.setDeveloper_name(dataObj.getString("DeveloperName"));
                            addresses1.setTower(dataObj.getString("Tower"));
                            addresses1.setFloor(dataObj.getString("Floor"));
                            addresses1.setUnitName(dataObj.getString("UnitNo"));
                            addresses.add(addresses1);
                        }

                        JSONArray phonesJsonArray = projectsArray.getJSONObject(index).getJSONArray("Phones");
                        ArrayList<ContactsModel.Phones> phones = new ArrayList();
                        for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                            ContactsModel.Phones phones1 = contactsModel.new Phones();
                            JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                            phones1.setId(dataObj.getString("Id"));
                            phones1.setContactId(dataObj.getString("ContactId"));
                            phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
                            phones1.setPhoneType(dataObj.getString("Mode"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                            phones.add(phones1);
                        }
                        JSONArray emailJsonArray = projectsArray.getJSONObject(index).getJSONArray("EmailAddresses");
                        ArrayList<ContactsModel.Email> emails = new ArrayList();
                        for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                            ContactsModel.Email email = contactsModel.new Email();
                            JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                            email.setId(dataObj.getString("Id"));
                            email.setContactId(dataObj.getString("ContactId"));
                            email.setAddress(dataObj.getString("Address"));
                            email.setLabel(dataObj.getString("Label"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                            emails.add(email);
                        }


                        JSONArray contactFileJsonArray = projectsArray.getJSONObject(index).getJSONArray("ContactFiles");
                        ArrayList<ContactsModel.ContactFiles> contactFiles = new ArrayList();
                        for (int indexJ = 0; indexJ < contactFileJsonArray.length(); indexJ++) {

                            ContactsModel.ContactFiles contactFiles1 = contactsModel.new ContactFiles();
                            JSONObject dataObj = contactFileJsonArray.getJSONObject(indexJ);

                            contactFiles1.setId(dataObj.getString("Id"));
                            contactFiles1.setContactId(dataObj.getString("ContactId"));
                            contactFiles1.setName(dataObj.getString("Name"));
                            contactFiles1.setType(dataObj.getString("Type"));
                            contactFiles1.setPath(dataObj.getString("Path"));
                            contactFiles1.setCompanyId(dataObj.getString("CompanyId"));


                            contactFiles.add(contactFiles1);
                        }
//                        JSONArray contactTermJsonArray = projectsArray.getJSONObject(index).getJSONArray("ContactTerms");
//                        ArrayList<ContactsModel.ContactTerms> contactTermses = new ArrayList();
//                        for (int indexJ = 0; indexJ < contactTermJsonArray.length(); indexJ++) {
//                            ContactsModel.ContactTerms contactTerms = contactsModel.new ContactTerms();
//                            JSONObject dataObj = contactTermJsonArray.getJSONObject(indexJ);
//                            contactTerms.setId(dataObj.getString("Id"));
//
//                            contactTerms.setTermId(dataObj.getString("TermId"));
//
//                            JSONObject jsonObject11=dataObj.getJSONObject("Term");
//                            ContactsModel.ContactTerms.Term term=contactsModel.new ContactTerms().new Term();
//                            term.setName(jsonObject11.getString("Name"));
////                            term.set(jsonObject11.getString("Name"));
//                            contactsModel.setGroup_id(jsonObject11.getString("Id"));
//                            contactsModel.setGroup_name(jsonObject11.getString("Name"));
//                            Log.e("jsonObject11","jsonObject11"+jsonObject11.getString("Id"));
////                            term.setDescription(jsonObject11.getString("Description"));
////                            term.setTaxonomyId(jsonObject11.getString("TaxonomyId"));
////                            term.setCompanyId(jsonObject11.getString("CompanyId"));
//                        }
                        JSONArray GroupsJsonArray = projectsArray.getJSONObject(index).getJSONArray("Groups");
                        ArrayList<ContactsModel.Groups> groupses = new ArrayList();
                        for (int indexJ = 0; indexJ < GroupsJsonArray.length(); indexJ++) {
                            ContactsModel.Groups groups = contactsModel.new Groups();
                            JSONObject dataObj = GroupsJsonArray.getJSONObject(indexJ);
//                            groups.setId(dataObj.getString("Id"));
//
//                            groups.setGroupName(dataObj.getString("GroupName"));


//                            term.set(jsonObject11.getString("Name"));
                            contactsModel.setGroup_id(dataObj.getString("Id"));
                            contactsModel.setGroup_name(dataObj.getString("GroupName"));
//                            Log.e("jsonObject11","jsonObject11"+jsonObject11.getString("Id"));
//
                        }

//                            contactsModel.setState(state);
//                            contactsModel.setCountry(country);
                        contactsModel.setAddresses(addresses);
                        contactsModel.setPhones(phones);
                        contactsModel.setContactFiles(contactFiles);
                        contactsModel.setEmails(emails);
                        contactsModel.setGroupses(groupses);

                        contactsFirebaseModels.add(contactsModel);

                        String phone = "";
                        String emai_address = "";
                        if (contactsModel.getPhones().size() > 0) {
                            phone = contactsModel.getPhones().get(0).getPhoneNumber();
                            if (LocalContatctTable.getInstance().isPhoneAvailable(phone)) {
                                LocalContatctTable.getInstance().updateSync(phone);
                            }
                        }

                        if (contactsModel.getEmails().size() > 0) {
                            emai_address = contactsModel.getEmails().get(0).getAddress();
//                            if (LocalContatctTable.getInstance().isPhoneAvailable(phone)) {
//                                LocalContatctTable.getInstance().updateSync(phone);
//                            }
                        }

                        if (ContatctTable.getInstance().isContactAvailable(contactsModel.getId())) {
                            ContatctTable.getInstance().updateCategory(Integer.parseInt(contactsModel.getId()), contactsModel.getFirstName(), contactsModel.getLastName(), contactsModel.getCompanyName(), phone,emai_address, 1,contactsModel.getGroup_id(),contactsModel.getGroup_name(),contactsModel.getDateOfBirthStr(),contactsModel.getImageUrl(),"");
                            for (int indexJ = 0; indexJ < contactsModel.getPhones().size(); indexJ++) {
                                if(PhoneTable.getInstance().isPhoneAvailable(contactsModel.getPhones().get(indexJ).getId())) {
                                    PhoneTable.getInstance().updateCategory(
                                            Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()),
                                            contactsModel.getPhones().get(indexJ).getPhoneNumber(), "");
                                }else{
                                    PhoneTable.getInstance().write(Integer.parseInt(contactsModel.getPhones().get(indexJ).getContactId()), Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()), contactsModel.getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");

                                }
                            }
//
                            for (int indexJ1 = 0; indexJ1 < contactsModel.getEmails().size(); indexJ1++) {
                                if(EmailTable.getInstance().isEmailAvailable(contactsModel.getEmails().get(indexJ1).getId())) {
                                    EmailTable.getInstance().updateEmail(Integer.parseInt(contactsModel.getEmails().get(indexJ1).getId()), contactsModel.getEmails().get(indexJ1).getAddress(), "");
                                }else{
                                    EmailTable.getInstance().write(Integer.parseInt(contactsModel.getEmails().get(indexJ1).getContactId()), Integer.parseInt(contactsModel.getEmails().get(indexJ1).getId()), contactsModel.getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");

                                }
                            }
                            for (int indexJ2 = 0; indexJ2 < contactsModel.getAddresses().size(); indexJ2++) {
                                if (AddressTable.getInstance().isAddressAvailable(contactsModel.getAddresses().get(indexJ2).getId())) {
                                    AddressTable.getInstance().updateAddress(Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(), contactsModel.getAddresses().get(indexJ2).getAddress2(),contactsModel.getAddresses().get(indexJ2).getAddress3(),contactsModel.getAddresses().get(indexJ2).getDeveloper(),contactsModel.getAddresses().get(indexJ2).getTower(),contactsModel.getAddresses().get(indexJ2).getFloor(),contactsModel.getAddresses().get(indexJ2).getUnitName(), contactsModel.getAddresses().get(indexJ2).getCity_name(), contactsModel.getAddresses().get(indexJ2).getZipcode(), contactsModel.getAddresses().get(indexJ2).getCountryId(), contactsModel.getAddresses().get(indexJ2).getStateName(), "01-01-2017");
                                } else {
                                    AddressTable.getInstance().write(Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getContactId()), Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(), contactsModel.getAddresses().get(indexJ2).getAddress2(),contactsModel.getAddresses().get(indexJ2).getAddress3(),contactsModel.getAddresses().get(indexJ2).getDeveloper(),contactsModel.getAddresses().get(indexJ2).getTower(),contactsModel.getAddresses().get(indexJ2).getFloor(),contactsModel.getAddresses().get(indexJ2).getUnitName(), contactsModel.getAddresses().get(indexJ2).getCity_name(), contactsModel.getAddresses().get(indexJ2).getZipcode(), contactsModel.getAddresses().get(indexJ2).getCountryId(), contactsModel.getAddresses().get(indexJ2).getStateName(), "01-01-2017", "01-01-2017");

                                }
                            }


                        } else {
                            ContatctTable.getInstance().write(Integer.parseInt(contactsModel.getId()), contactsModel.getFirstName(), contactsModel.getLastName(), contactsModel.getCompanyName(), phone,emai_address, 1,contactsModel.getGroup_id(),contactsModel.getGroup_name(), contactsModel.getDateOfBirthStr(),contactsModel.getImageUrl(),"01-01-2017", "01-01-2017");
                            for (int indexJ = 0; indexJ < contactsModel.getPhones().size(); indexJ++) {
                                PhoneTable.getInstance().write(Integer.parseInt(contactsModel.getPhones().get(indexJ).getContactId()), Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()), contactsModel.getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");

                            }
                            for (int indexJ1 = 0; indexJ1 < contactsModel.getEmails().size(); indexJ1++) {
                                EmailTable.getInstance().write(Integer.parseInt(contactsModel.getEmails().get(indexJ1).getContactId()), Integer.parseInt(contactsModel.getEmails().get(indexJ1).getId()), contactsModel.getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");

                            }
                            for (int indexJ2 = 0; indexJ2 < contactsModel.getEmails().size(); indexJ2++) {
                                AddressTable.getInstance().write(Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getContactId()), Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(),contactsModel.getAddresses().get(indexJ2).getAddress2(),contactsModel.getAddresses().get(indexJ2).getAddress3(),contactsModel.getAddresses().get(indexJ2).getDeveloper(),contactsModel.getAddresses().get(indexJ2).getTower(),contactsModel.getAddresses().get(indexJ2).getFloor(),contactsModel.getAddresses().get(indexJ2).getUnitName(),contactsModel.getAddresses().get(indexJ2).getCity_name(), contactsModel.getAddresses().get(indexJ2).getZipcode(),contactsModel.getAddresses().get(indexJ2).getCountryId(),contactsModel.getAddresses().get(indexJ2).getStateName(),"01-01-2017", "01-01-2017");

                            }

                        }
//                    Type listType = new TypeToken<ArrayList<ContactsModel>>() {
//                    }.getType();
//                    contactsFirebaseModels = new Gson().fromJson(result, listType);
                    Log.e(TAG, "contact list" + contactsFirebaseModels.size());
//                    for (ContactsModel contactsModel : contactsFirebaseModels) {
//                        String phonenumber = null;
//                        if (contactsModel.getPhones().size() > 0)
//                            phonenumber = contactsModel.getPhones().get(0).getPhoneNumber();
//                        if (!ContatctTable.getInstance().isContactAvailable(contactsModel.getId())) {
//                            ContatctTable.getInstance().write(
//                                    Integer.parseInt(contactsModel.getId()),
//                                    contactsModel.getFirstName(),
//                                    contactsModel.getLastName(),
//                                    contactsModel.getCompanyName(),
//                                    phonenumber,
//                                    contactsModel.getCreatedOn(),
//                                    contactsModel.getUpdatedOn(), "true");
//                            for (ContactsModel.Phones phones : contactsModel.getPhones()) {
//                                PhoneTable.getInstance().write(Integer.parseInt(phones.getContactId()), Integer.parseInt(phones.getId()), phones.getPhoneNumber(), contactsModel.getCreatedOn(), contactsModel.getUpdatedOn());
//                            }
//                        } else {
//                            ContatctTable.getInstance().updateCategory(
//                                    Integer.parseInt(contactsModel.getId()),
//                                    contactsModel.getFirstName(),
//                                    contactsModel.getLastName(),
//                                    contactsModel.getCompanyName(),
//                                    phonenumber,
//                                    contactsModel.getUpdatedOn(), "true");
//                            for (ContactsModel.Phones phones : contactsModel.getPhones()) {
//                                PhoneTable.getInstance().updatePhoneNumber(Integer.parseInt(phones.getContactId()), Integer.parseInt(phones.getId()), phones.getPhoneNumber(), contactsModel.getCreatedOn(), contactsModel.getUpdatedOn());
//                            }
//                        }
//                    }
//                    contactsFirebaseModels = ContatctTable.getInstance().getContacts();
                    PreferenceConnector.getInstance(this).savePreferences(Constants.DATE_TIME, Utils.getInstance().getDateTime());
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(MainDashboardActivity.mBroadcastStringAction);
                    broadcastIntent.putExtra("Data", "Broadcast Data");
                    sendBroadcast(broadcastIntent);

                        stopService(new Intent(this,SyncService.class));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_POST_CONTACTS)) {
                try {
                ContatctTable.getInstance().updateAllSync();

                        onSyncRequest(false);
                  //  timer.cancel();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private class DelayedTask extends TimerTask {
        @Override
        public void run() {
            Log.e(TAG, "updating assets");

//            if (!PreferenceConnector.getInstance(SyncService.this).loadSavedPreferences(Constants.IS_SYNC, "").equalsIgnoreCase(""))
//                onPostContacts(false);
//            else
//                onPostContacts(false);

        }
    }


    private void onPostContacts(Boolean b) {
        new CommonAsync(SyncService.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_POST_CONTACTS,
                getLoginInputJson(),
                "Loading...",
                this,
                Urls.URL_POST_CONTACTS,
                Constants.POST,b).execute();
    }


    private String getLoginInputJson() {
        String inputJson = "";

        try {




//

       JSONArray jsonArrayTop = new JSONArray();
            for (int index = 0; index < contactsFirebaseModels.size(); index++) {
                ContactsModel contactsModel = contactsFirebaseModels.get(index);
                JSONObject jsonObject = new JSONObject();
                if (!contactsModel.getId().equals(""))
                    jsonObject.put("Id", contactsModel.getId());
                jsonObject.put("FirstName", contactsModel.getFirstName());
                jsonObject.put("LastName", contactsModel.getLastName());
                jsonObject.put("DateOfBirth", contactsModel.getDob());
                jsonObject.put("CompanyId", "1");
                jsonObject.put("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));
                JSONArray jsonArray = new JSONArray();
                ArrayList<ContactsModel.Phones> phones = contactsModel.getPhones();
                for (int i = 0; i < phones.size(); i++) {

                    JSONObject jsonObject1 = new JSONObject();
                    if (!contactsModel.getId().equals("0")) {
                        jsonObject1.put("Id", phones.get(i).getId());
                        jsonObject1.put("IsModify", "true");
                    }else{
                        jsonObject1.put("Id", 0);
                        jsonObject1.put("IsModify", "false");
                    }
                    jsonObject1.put("PhoneNumber", phones.get(i).getPhoneNumber());

//                    jsonObject1.put("PhoneType", 1);
                    jsonObject1.put("Mode", "");

                    jsonArray.put(jsonObject1);

                }
                JSONArray jsonArray1 = new JSONArray();
                ArrayList<ContactsModel.Email> emails = contactsModel.getEmails();
                for (int i = 0; i < emails.size(); i++) {

                    JSONObject jsonObject1 = new JSONObject();
                    if (!contactsModel.getId().equals("0")) {
                        jsonObject1.put("Id", emails.get(i).getId());
                        jsonObject1.put("IsModify", "true");
                    }else{
                        jsonObject1.put("Id", 0);
                        jsonObject1.put("IsModify", "false");
                    }
                    jsonObject1.put("Address", emails.get(i).getAddress());

//                    jsonObject1.put("PhoneType", 1);
                    jsonObject1.put("Label", "");

                    jsonArray1.put(jsonObject1);

                }
                JSONArray jsonArray2 = new JSONArray();
                ArrayList<AddressModel> addresses = contactsModel.getAddresses();
                for (int i = 0; i < addresses.size(); i++) {

                    JSONObject jsonObject1 = new JSONObject();
                    if (!contactsModel.getId().equals("0")) {
                        jsonObject1.put("Id", addresses.get(i).getId());
                        jsonObject1.put("IsModify", "true");
                    }else{
                        jsonObject1.put("Id", 0);
                        jsonObject1.put("IsModify", "false");
                    }
                    jsonObject1.put("Address1", addresses.get(i).getAddress1());
                    jsonObject1.put("Address2", addresses.get(i).getAddress2());
                    jsonObject1.put("Address3", addresses.get(i).getAddress3());

                    jsonObject1.put("DeveloperName", addresses.get(i).getDeveloper_name());
                    jsonObject1.put("Tower", addresses.get(i).getTower());
                    jsonObject1.put("Floor", addresses.get(i).getFloor());
                    jsonObject1.put("UnitName", addresses.get(i).getUnitName());

                    jsonObject1.put("CityName", addresses.get(i).getCity_name());
                    jsonObject1.put("CountryId", addresses.get(i).getCountryId());
                    jsonObject1.put("StateName", addresses.get(i).getStateName());
                    jsonObject1.put("ZipCode", addresses.get(i).getZipcode());

//                    jsonObject1.put("PhoneType", 1);


                    jsonArray2.put(jsonObject1);

                }
                JSONArray grouparray = new JSONArray();
                JSONObject jsonObject4 = new JSONObject();
                jsonObject4.put("Id", "2");
                grouparray.put(jsonObject4);


                jsonObject.put("Phones", jsonArray);
                jsonObject.put("EmailAddresses", jsonArray1);
                jsonObject.put("Addresses", jsonArray2);
//                jsonObject.put("Groups", grouparray);
                jsonArrayTop.put(jsonObject);
            }



            inputJson = jsonArrayTop.toString();
            return inputJson;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }


    private void onSyncRequest(Boolean b) {
        new CommonSyncwithoutstatus(SyncService.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS+ PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,"")+"/"+ Constants.APP_ID + "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00")+ "'&IsDeleted+eq+false", "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET, b).execute();
    }
}
