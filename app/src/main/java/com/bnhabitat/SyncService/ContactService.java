package com.bnhabitat.SyncService;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;

import com.bnhabitat.ContactObserver;

/**
 * Created by gourav on 8/18/2017.
 */

public class ContactService extends Service {

    ContactObserver observer;
    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        getContentResolver().unregisterContentObserver(observer);
        super.onDestroy();
    }

    @Override
    public void onCreate() {
        System.out.println("service created");
        observer=new ContactObserver(new Handler());
        // TODO Auto-generated method stub
        getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, false, observer);
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
}