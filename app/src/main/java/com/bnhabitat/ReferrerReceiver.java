package com.bnhabitat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bnhabitat.utils.PreferenceConnector;

/**
 * Created by gourav on 9/4/2017.
 */

public class ReferrerReceiver extends BroadcastReceiver {
    Context context;
    String referrer;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context=context;
        final String action = intent.getAction();
        try {
            referrer = intent.getStringExtra("referrer");

            Log.e("referrer", "referrer" + referrer);
//        AlertMessage();

            PreferenceConnector.getInstance(context).savePreferences("refer", referrer);
        }catch (Exception e){
            e.printStackTrace();
        }

//        if (action != null && TextUtils.equals(action, "com.android.vending.INSTALL_REFERRER")) {
//            try {
//                final String referrer = intent.getStringExtra("referrer");
//
//                Log.e("referrer","referrer"+referrer);
//                // Parse parameters
////                String[] params = referrer.split("&");
////                for (String p : params) {
////                    if (p.startsWith("utm_content=")) {
////                        final String content = p.substring("utm_content=".length());
////
////                        /**
////                         * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
////                         */
////                        Log.i("ReferrerReceiver", content);
////
////                        break;
////                    }
////                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            /**
             * OPTIONAL: Forward the intent to Google Analytics V2 receiver
             */
            // new com.google.analytics.tracking.android.AnalyticsReceiver().onReceive(context, intent);
//        }

    }

}
