package com.bnhabitat.interfaces;

import org.json.JSONObject;

/**
 * Created by gourav on 4/20/2018.
 */

public interface ActivityCommunicator{
    public JSONObject passDataToActivity();
}