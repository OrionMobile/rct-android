package com.bnhabitat.interfaces;

import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.InventoryModel;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by gourav on 2/1/2018.
 */


public interface SendMessage {
    public void sendData(JSONObject message, int position);

    public void sendData(ArrayList<ContactsModel> contactsModels, int position);

    public void sendData(int position);

    public void sendData(String id, int position);

    public void EditId(ArrayList<InventoryModel> inventoryModels, String EditId);

}
