package com.bnhabitat.interfaces;

import com.onegravity.contactpicker.contact.Contact;

import java.util.List;

public interface ContactCall {

   public void contactListCall(List<Contact> countryList, int i);
}
