package com.bnhabitat.interfaces;

import java.util.ArrayList;

/**
 * Created by romesh on 26-05-2016.
 */
public interface TabChangeListener {

    void onChangeTab(int position);
    void onChangeTab(ArrayList<String> contacts);
}
