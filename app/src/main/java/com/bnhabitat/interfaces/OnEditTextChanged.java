package com.bnhabitat.interfaces;

import com.bnhabitat.models.ContactsModel;

import java.util.ArrayList;
import java.util.HashMap;

public interface OnEditTextChanged {

    public void onTextChange(ArrayList<HashMap<Integer, ContactsModel.Phones>> arrayList);
    public void onTextChange1(ArrayList<HashMap<Integer, ContactsModel.Email>> emailArrayList);

}
