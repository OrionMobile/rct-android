package com.bnhabitat.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.data.projects.AllProjectsTable;
import com.bnhabitat.data.projects.RecentViewProjectsTable;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.ui.activities.DashboardActivity;
import com.bnhabitat.ui.activities.LoginActivity;
import com.bnhabitat.ui.adapters.CommonApdater;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 2/8/2017.
 */

public class Utils {
    public static Utils instance;
    Dialog dialog;

    static Dialog progressDialog;

    static Dialog dialog1;

    public static Utils getInstance() {
        if (instance == null) {
            synchronized (Utils.class) {
                if (instance == null) {
                    instance = new Utils();
                }
            }
        }
        return instance;
    }


    public static String encodeImage(Bitmap bm) {

        /*ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
        String   encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP| Base64.URL_SAFE);
        return encodedImage;*/

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.NO_WRAP);

        return encImage;
    }




    public static Gson getGsonObject() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        Gson gson = gsonBuilder.create();
        return gson;
    }

//    public static void showCommonListDialog(Context context, String title, ArrayList<CommonDialogModel> commonModels) {
//        final Dialog dialog = new Dialog(context);
////        dialog.setTitle(title);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_common_listing);
//        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
//        CommonApdater commonAdapter = new CommonApdater(context, commonModels,0);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(commonAdapter);
//        dialog.show();
//    }


    public static void hideKeyboard(View view, Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm", Locale.getDefault());
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, -5);
        Date oneHourBack = cal.getTime();
        return dateFormat.format(oneHourBack);
    }

    public void shareCatalyst(Context context, String numbe) {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra("address", numbe);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Check out Bnhabitat App a for your smartphone. Best assistant for Real Estate Professions. Download it today from http://realcrafttech.com/dl");
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    public void showCustomDialog(Context context, String title) {
        if (dialog == null) {
            dialog = new Dialog(context, android.R.style.Theme_NoTitleBar);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.price_pay_dialog);
            dialog.setCancelable(false);
            TextView loadertext = (TextView) dialog.findViewById(R.id.message);
            loadertext.setText(title);
            dialog.show();
        }
    }

    public static void showProgressDialog(Context context) {


        if (progressDialog == null) {
            progressDialog = new Dialog(context, android.R.style.Theme_NoTitleBar);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.custom_loading_dialog);
            progressDialog.setCancelable(false);
            TextView loadertext = (TextView) progressDialog.findViewById(R.id.message);
            loadertext.setText("");
            progressDialog.show();
        }
    }


    public static void setImageView(String url, ImageView imageView, Context context) {

        if (null == url || url.equalsIgnoreCase("null") || url.equalsIgnoreCase("")) {

            imageView.setImageResource(R.drawable.image_logo);

        } else
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.image_logo)
                    .into(imageView);
    }

    //    public static void showCustomErrorDialog(Context context, String title) {
//        if (dialog1 == null) {
//            dialog1 = new Dialog(context, android.R.style.Theme_NoTitleBar);
//            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            dialog1.setContentView(R.layout.dialog_error_lay);
//            dialog1.setCancelable(false);
//            TextView loadertext = (TextView) dialog1.findViewById(R.id.error);
//            loadertext.setText(title);
//            dialog1.show();
//        }
//    }
    public int getCalculatedPrice(String type, double areaUnitPrice, double bspUnit, double bspTotalAmount, float amount) {

        //  Lumpsum|UnitPrice|Percentage

        // DecimalFormat form = new DecimalFormat("##,##,##,##0");

        if (type.equalsIgnoreCase("Lumpsum")) {

            return (int) amount;

        } else if (type.equalsIgnoreCase("UnitPrice")) {

            return (int) Math.round((amount * areaUnitPrice));

        } else if (type.equalsIgnoreCase("Percentage")) {


            return (int) Math.round(((bspTotalAmount * amount) / 100));
        }
        return 0;
    }

    public String getCalculatedPriceType(Context context,
                                         String type,
                                         float amount,
                                         String unitName) {

        //  Lumpsum|UnitPrice|Percentage

        DecimalFormat form = new DecimalFormat("##,##,##,##0");

        if (type.equalsIgnoreCase("Lumpsum")) {

            //  return context.getResources().getString(R.string.rs) + " " + String.valueOf(form.format(amount));

            return getConvertedPrice(Long.parseLong(String.valueOf(Math.round(amount))), context);

        } else if (type.equalsIgnoreCase("UnitPrice")) {

            // return context.getResources().getString(R.string.rs) + " " + amount + "/" + unitName;
            return getConvertedPrice(Long.parseLong(String.valueOf(Math.round(amount))), context) + "/" + unitName;

        } else if (type.equalsIgnoreCase("Percentage")) {


            // return context.getResources().getString(R.string.rs) + " " + amount + "% of BSP";
            return getSymbol(context) + " " + String.valueOf(Math.round(amount)) + "% of BSP";
        }
        return "";
    }

    public String subStringChars(String str, int noOfCracters) {
        if (str != null && str.length() > noOfCracters) {
            str = str.substring(0, str.length() - noOfCracters);
        }
        return str;
    }

    public static String getDeviceId(Context context) {


        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getGeneratedImageUrl(String value) {

        if (null != value) {
            if (value.trim().equalsIgnoreCase(""))
                return "";
            else
                return Urls.BASE_IMAGE_URL + value.replace(" ", "%20");
        } else
            return "";
    }

    public void cancelCustomDialog() {
        try {
            if (null != dialog && dialog.isShowing()) {
                dialog.cancel();
                dialog = null;
            }
        } catch (Exception e) {
            dialog = null;
            e.printStackTrace();
        }
    }


    public void cancelProgressDialog() {
        try {
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.cancel();
                progressDialog = null;
            }
        } catch (Exception e) {
            progressDialog = null;
            e.printStackTrace();
        }
    }


    public static String getUnit(String value, Context context) {


        String currencyName = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME, "INR");


        if (currencyName.toLowerCase().contains("INR".toLowerCase())) {
            if (value.length() == 5)
                return getSymbol(currencyName) + " " + value.substring(0, Math.min(value.length(), 2)) + " k";
            else if (value.length() == 6)
                return getSymbol(currencyName) + " " + value.substring(0, Math.min(value.length(), 1)) + " lacs";
            else if (value.length() == 7)
                return getSymbol(currencyName) + " " + value.substring(0, Math.min(value.length(), 2)) + " lacs";
            else if (value.length() == 8) {

                String values = value.substring(0, Math.min(value.length(), 3));
                StringBuilder stringBuilder = new StringBuilder();
                for (int index = 0; index < 3; index++) {

                    if (index == 1)
                        stringBuilder.append(".");

                    stringBuilder.append(values.charAt(index));

                }

                return getSymbol(currencyName) + " " + stringBuilder.toString() + " cr";
            }
        } else {

            if (null != value && !value.equalsIgnoreCase("")) {
                value = getConvertedPriceWithoutSymbol(Long.parseLong(value), context);

                if (value.length() == 5)
                    return getSymbol(currencyName) + " " + value.substring(0, Math.min(value.length(), 2)) + " k";
                else if (value.length() == 6) {

                    return getSymbol(currencyName) + " " + "0." + value.substring(0, Math.min(value.length(), 2)) + " m";

                } else if (value.length() == 7)
                    return getSymbol(currencyName) + " " + value.substring(0, Math.min(value.length(), 1)) + " m";
                else if (value.length() == 8) {

                    return getSymbol(currencyName) + " " + value.substring(0, Math.min(value.length(), 2)) + " m";
                }
            }
        }

        return getSymbol(currencyName) + " " + "0";
    }

    public static String getConvertedPriceWithoutSymbol(long actualPrice, Context context) {

        String currencyName = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME, "INR");
        float currencyPrice = PreferenceConnector.getInstance(context).loadSavedFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, 1);

        double convertedPrice = actualPrice / currencyPrice;
        return String.valueOf(Math.round(convertedPrice));
    }

    public static String formatDecimal(String value, Context context) {
        DecimalFormat df = new DecimalFormat("#,###,##0");
        return df.format(Double.valueOf(value));
    }

    public static String getConvertedPrice(long actualPrice, Context context) {

        String currencyName = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME, "INR");
        float currencyPrice = PreferenceConnector.getInstance(context).loadSavedFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, 1);

        DecimalFormat form = new DecimalFormat("##,##,##,##0");
        if (currencyPrice != 1)
            form = new DecimalFormat("###,###,##0");

        double convertedPrice = actualPrice / currencyPrice;

        return getSymbol(currencyName) + " " + form.format(Math.round(convertedPrice));
    }

    public static String getSymbol(Context context) {
        String name = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME, "INR");
        String symbol = "";
        int index = 0;
        for (String currencyName : Constants.CURRENCY_NAME) {
            if (name.equalsIgnoreCase(currencyName)) {
                symbol = Constants.CURRENCY_SYMBOL[index];
                break;
            }
            index++;
        }
        return symbol;
    }

    public static String getSymbol(String name) {
        String symbol = "";
        int index = 0;
        for (String currencyName : Constants.CURRENCY_NAME) {
            if (name.equalsIgnoreCase(currencyName)) {
                symbol = Constants.CURRENCY_SYMBOL[index];
                break;
            }
            index++;
        }
        return symbol;
    }

    public static String getFilteredValue(String value) {
        if (null != value) {
            if (value.trim().equalsIgnoreCase(""))
                return "None";
            else if (value.trim().equalsIgnoreCase("null"))
                return "None";
            else
                return value;
        } else
            return "None";
    }

    public static String getEmptyValue(String value) {
        if (null != value) {
            if (value.trim().equalsIgnoreCase(""))
                return "";
            else if (value.trim().equalsIgnoreCase("null"))
                return "";
            else
                return value;
        } else
            return "";
    }

    public static int getFilteredValueInt(int value) {
        if (0 != value) {
            if (value==0)
                return 0;
            else if (value==0)
                return 0;
            else
                return value;
        } else
            return 0;
    }

    public static String getConvertedPrice(double value) {
        DecimalFormat form = new DecimalFormat("###,###,###");
        return String.valueOf(form.format(value));
    }

    public static boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }


    public static boolean isEmailAddressValid(String email) {
        boolean isEmailValid = false;

        String strExpression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern objPattern = Pattern.compile(strExpression, Pattern.CASE_INSENSITIVE);
        Matcher objMatcher = objPattern.matcher(inputStr);
        if (objMatcher.matches()) {
            isEmailValid = true;
        }
        return isEmailValid;
    }


    public static Map<String, String> getHeader_a(Context context) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
//        headers.put("Content-Type", "application/json; charset=UTF-8");
//        headers.put(Constants.CONTENT_TYPE, Constants.TEXT_HTML);
        String token = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.AUTHENTICATION_KEY, "");
        if (!token.equalsIgnoreCase(""))
            headers.put(Constants.AUTH_TOKEN, token);
        headers.put(Constants.USER_ID, PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_ID, ""));
        Log.d("token111", "token" + token);

        return headers;
    }

    public static Map<String, String> getHeaders(Context context) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
        headers.put(Constants.UserId, "67");
        headers.put(Constants.Accept, Constants.APPLICATION_JSON);


        String token = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.APP_AUTH_KEY, "");
        if (!token.equalsIgnoreCase(""))
            headers.put(Constants.AuthToken,token);
        Log.d("token111", "token" + token);

        return headers;
    }

    public static void logOut(Activity activity) {
        LoginManager.getInstance().logOut();
        AllProjectsTable.getInstance().deleteAllRecords();
        RecentViewProjectsTable.getInstance().deleteAllRecords();
        PreferenceConnector.getInstance(activity).savePreferences(Constants.USER_ID, "");
        PreferenceConnector.getInstance(activity).savePreferences(Constants.EMAIL, "");
        PreferenceConnector.getInstance(activity).savePreferences(Constants.PHONE_NUMBER, "");
        PreferenceConnector.getInstance(activity).savePreferences(Constants.USERNAME, "");
        PreferenceConnector.getInstance(activity).savePreferences(Constants.REGISTRATION_ID, "");
//
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.finishAffinity();

    }


    public static void showSuccessErrorMessage(String title,
                                               String message,
                                               String buttonName,
                                               final Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(buttonName)

                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        DialogModel dialogModel = new DialogModel();
                        dialogModel.setClickedOn(context.getString(R.string.ok));
                        EventBus.getDefault().post(dialogModel);
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    public static void showSuccessMessage(String title,
                                          String message,
                                          String buttonName,
                                          final Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(buttonName)

                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    public static void shownotiMessage(String title, String message,
                                       String buttonName,
                                       final Context context) {

        new SweetAlertDialog(context)
                .setContentText(message)
                .setConfirmText(buttonName)

                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
//                        DialogModel dialogModel = new DialogModel();
//                        dialogModel.setClickedOn(context.getString(R.string.ok));
//                        EventBus.getDefault().post(dialogModel);
//                        sDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    public static void ShowSuccessMessage(String msg, Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(msg)

                .show();
    }

    public static void showWarningErrorMessage(String title,
                                               String message,
                                               String buttonName,
                                               final Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(buttonName)

                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    public static void showWarningErrorMessageOTP(String title,
                                                  String message,
                                                  String buttonName,
                                                  final Activity context) {

        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(buttonName)

                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                       /* Intent it = new Intent(context, LoginActivity.class);
                        context.startActivity(it);
                        context.finishAffinity();*/

                    }
                })
                .show();

    }


    public static void showErrorMessage(String message, final Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(context.getString(R.string.message))
                .setContentText(message)
                .show();

    }

    public static void showErrorMessage(String message, final EditText editText, final Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(context.getString(R.string.message))
                .setContentText(message)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        editText.requestFocus();
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    public static void showErrorMessage(int message, Context context) {


        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(context.getString(R.string.error))
                .setMessage(context.getResources().getString(message))

                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .show();
    }


    public static void showErrorMessagestatus(String message, String buttonname, Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(context.getString(R.string.message))
                .setContentText(message)
                .setConfirmText(buttonname)
                .show();

    }

    public String numberToWords(int number, Context context) {

        float currencyPrice = PreferenceConnector.getInstance(context).loadSavedFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, 1);
        String words = "";

        if (currencyPrice == 1) {

            if (number == 0) {
                return "zero";
            }
            if (number < 0) {
                return "minus " + numberToWords(Math.abs(number), context);
            }

            if ((number / 10000000) > 0) {
                words += numberToWords(number / 10000000, context) + " Crore ";
                number %= 10000000;
            }
            if ((number / 100000) > 0) {
                words += numberToWords(number / 100000, context) + " Lakh ";
                number %= 100000;
            }
            if ((number / 1000) > 0) {
                words += numberToWords(number / 1000, context) + " Thousand ";
                number %= 1000;
            }
            if ((number / 100) > 0) {
                words += numberToWords(number / 100, context) + " Hundred ";
                number %= 100;
            }
            if (number > 0) {
                if (words != "") {
                    words += "and ";
                }
                String[] unitsMap = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
                String[] tensMap = {"Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
                if (number < 20) {
                    words += unitsMap[number];
                } else {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0) {
                        words += "-" + unitsMap[number % 10];
                    }
                }
            }
        } else {
            words = EnglishNumberToWords.convert(number);
            words = words.length() > 0 ? words.substring(0, 1).toUpperCase() + words.substring(1) : words;
        }
        return words;
    }


    public static String getMessageDate(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss";
       // String outputPattern = "dd-MM-yyyy";
        String outputPattern = "MM-dd-yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getMessageDateFromMillis(long time) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        String str = null;
        try {
            str = outputFormat.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public String getTimeInMillis(String dateString) {
        // TODO Auto-generated method stub
        //String timeInMillis="";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date testDate = null;
        try {
            testDate = sdf.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("Milliseconds==" + testDate.getTime());

        return String.valueOf(testDate.getTime());
    }

    public static String getMonthName(int month) {

        switch (month) {

            case 1:
                return "Jan, ";
            case 2:
                return "Feb, ";
            case 3:
                return "Mar, ";
            case 4:
                return "Apr, ";
            case 5:
                return "May, ";
            case 6:
                return "Jun, ";
            case 7:
                return "Jul, ";
            case 8:
                return "Aug, ";
            case 9:
                return "Sept, ";
            case 10:
                return "Oct, ";
            case 11:
                return "Nov, ";
            case 12:
                return "Dec, ";
            default:
                return "";

        }
    }
}
