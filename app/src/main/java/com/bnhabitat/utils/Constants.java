package com.bnhabitat.utils;

import com.bnhabitat.BuildConfig;

/**
 * Created by gourav on 2/8/2017.
 */

public class Constants {
    public static final String UTILITY_TOKEN = BuildConfig.GUEST_TOKEN;
    public static final String APP_COMPANY_ID = BuildConfig.COMPANY_ID;
    public static final String APP_AUTH_KEY = BuildConfig.AuthKey;
    public static final String DeveloperCompanyLogo = "DeveloperCompanyLogo";
    public static final String AuthToken = "AuthToken";
    public static final String TownName = "TownName";
    public static final String ProjectLocalities = "ProjectLocalities";
    public static final String TownshipName = "TownshipName";
    public static final String PROPERTY_LAY_DISPLAY="property_lay_display";
    public static final String  ProjectFiles="ProjectFiles";
    public static final String GET_ALL_SIZE_UNITS_LENGTH_API = "get_all_size_units_length_api";
    public static final String  ProjectAmenities="ProjectAmenities";
    public static final String  COMPANY_ID_BN="1";
    public static final String URL_PROJECT_NEIGHBOURHOODS = "/project/neighborhoods/";

    public static final String AUTHENTICATION_KEY = "AUTHENTICATION_KEY";
    public static final String  FileUrl="FileUrl";
    public static final String  TownshipId="TownshipId";
    public static final String  AboutProject="AboutProject";
    public static final String  AreaNagetiveMark="AreaNagetiveMark";
    public static final String  CategoryTypeId="CategoryTypeId";
    public static final String  ProjectCategoryTypes="ProjectCategoryTypes";

    public static final String APP_USER_ID = BuildConfig.ID;
    //  public static final String APP_ID = BuildConfig.APPLI_ID;
    public static final String APP_ID = "0";

    public static final String PAGENUMBER = "1";

    public static final String COMPAN_ID = BuildConfig.COMPANY_ID;
    public static final String DEFAULT_CURRENCY_VALUE = "default_currency_string_value";
    public static final String DATABASE_NAME = "BnHabitat.db";
    public static final int DATABASE_VERSION = 8;
    public static final int DATABASE_VERSION_06 = 5;
    public static final int DATABASE_VERSION_07 = 6;
    public static final int DATABASE_VERSION_08 = 8;
    public static final String COMPANY_ID = "company_id";
    public static final String CITY_ID_CONST = "4";
    public static final String TOWER_VALUE = "tower_value";
    public static final String UNIT_VALUE = "unit_value";
    public static final String EFFECTIVE_PRICE = "efective_price";
    public static final String EFFECTIVE_UNIT = "efective_unit";
    public static final String ZOOM_LIST = "zoom_list";
    public static final String DATE_TIME = "date_time";
    public static final String CONTACT_MODIFIED = "contact_modified";
    public static final String CREATED_ID = "created_id";
    public static final String POSITION = "position";
    public static final String COUNTRY_PHONE_CODE = "country_phone_code";
    public static final String CITY_NAME = "city_name";
    public static final String CITY_ID = "city_id";
    public static final String BROKERUSER_TYPE = "BrokerUserType";
    public static final String IS_INTIALIZED = "is_intilized";
    public static final String PROPERTY_POST = "property_post";
    public static final String REMOVE_IMAGE_API = "remove_image_api";
    public static final String ADD_PROJECT_API = "project_api";
    public static final String Accept = "Accept";
    public static final String UserId = "UserId";
    //    public static final String IS_CUSTOM_APP = BuildConfig.CUSTOM_APP;
//    public static final String BUILDER_ID = BuildConfig.BUILDER_ID;
    public static final String TIMEOUT = "time_out";
    public final static int CONNECTION_TIMEOUT = 20000;
    public static String ARRAY_LIST = "array_list";
    public static String FLOOR_NUMBER = "floor_number";

    public final static String PROPERTY_ID = "property_id";
    public final static String URL_PROPERTY_POST_DRAFT = "url_property_draft";


    public static String IS_SYNC = "is_sync";
    public static String GROUP_NAMES = "group_names";
    public final static String LOGIN_API = "login_api";
    public static final String DEFAULT_CURRENCY_NAME = "default_currency_string_name";
    public final static String SIGNUP_API = "signup_api";
    public final static String CHECK_USENAME_API = "check_username_api";
    public final static String CHECK_FBUSEERID_API = "check_fbuserid_api";
    public final static String CHECK_FBUSENAME_API = "check_fbusername_api";
    public final static String USERNAME = "userName";
    public final static String PASSWORD = "password";
    public final static String CONFIRM_PASSWORD = "confirm_password";
    public final static String DEVICE_TYPE = "deviceType";
    public final static String DEVICE_ID = "deviceId";
    public final static String CONTENT_TYPE = "Content-Type";
    public final static String APPLICATION_JSON = "application/json";

    public final static String TEXT_HTML = "text/html";
    public final static String ACCESS_TOKEN = "x-access-token";
    public static final String MOBILE_NUMBER_WITH_CODE = "mobile_number_with_code";
    public static final String PHONE_CODE = "phone_code";
    public static final String MobileNumber = "MobileNumber";
    public static final String CountryCode = "CountryCode";


    public static final String MOBILE_NUMBER_WITHOUT_CODE = "mobile_number_without_code";
    public static final String PIN_SENT = "pin_sent";
    public final static String USER_NAME = "user_name";
    public final static String UD_ID = "udId";
    public final static String REGISTRATION_ID = "registration_id";
    public final static String ANDROID = "android";
    public final static String EMAIL = "email";
    public final static String PHONE_NUMBER = "phoneNo";
    public final static String NAME = "name";
    public final static String FROM = "from";
    public final static String CHECKED = "checked";
    public final static String USER_ID = "userId";
    public final static String USER_ID_REGISTER = "userIdregister";
    public final static String EmailOrPhone = "EmailOrPhone";
    public final static String VERIFY_USER_API = "verify_user_api";
    public final static String PhoneVerify = "PhoneVerify";


    public final static String AUTH_TOKEN = "authtoken";
    public final static String AUTH_KEY = "AuthKey";


    public final static String ISLOGIN = "islogin";
    public final static String MULTI_STOREY = "multi_storey";

    public final static String MINT_KEY = "5fa837dc";

    public final static String GET_LOCATION_API = "get_location_api";
    public final static String GET_ALL_SIZE_UNITS_API = "get_all_size_units_api";

    public static final String APP_TOKEN = "t";
    public static final String USER_AUTH_KEY = "auth_key";
    public static final String SIZE_UNIT_ID = "unit_id";
    public static final String PLOT_SIZE_UNIT_ID = "plot_size_unit_id";
    public static final String PLOT_DIMENSION_UNIT_ID = "plot_size_unit_id";
    public static final String SIZE_UNIT_NAME = "unit_name";
    public static final String PLOT_SIZE_UNIT_NAME = "plotunit_name";
    public static final String PLOT_DIMENSION_UNIT_NAME = "plotdimension_unit_name";
    public static final String PLOT_UNIT_AREA = "plotunit_area";
    public static final String PLOT_AREA = "plot_area";
    public static final String CURRENCY_VALUES = "currency_string_value";
    public static final int GET = 1;
    public static final int POST = 2;


    public static final String PROJECT_ID = "PROJECT_ID";


    public static final String CompanyId = "CompanyId";
    public static final String Type = "Type";
    public static final String ImageCode = "ImageCode";
    public static final String jpg = "jpg";
    public static final String png = "png";
    public static final String Id = "Id";

//    public static final String ProjectCategoryTypeId = "ProjectCategoryTypeId";
    public static final String Name = "Name";
    public static final String FRAGMENT_Name = "FRAGMENT_Name";
    public static final String TotalArea = "TotalArea";
    public static final String TotalAreaSizeUnitId = "TotalAreaSizeUnitId";
    public static final String ZipCode = "ZipCode";
    public static final String StateName = "StateName";
    public static final String DistrictName = "DistrictName";
    public static final String TehsilName = "TehsilName";
    public static final String LocalBodyType = "LocalBodyType";
    public static final String LocalBodyName = "LocalBodyName";
    public static final String DeveloperCompanyName = "DeveloperCompanyName";
    public static final String AreaNegativeMark = "AreaNegativeMark";

    public static final String CreatedById = "CreatedById";
    public static final String UpdatedById = "UpdatedById";
    public static final String TownshipFiles = "TownshipFiles";

    public static final String FileId = "FileId";
    public static final String Category = "Category";
    public static final String TownshipLocalities = "TownshipLocalitiesModel";
    public static final String LocalityName = "LocalityName";


    public static final String Description = "Description";
    public static final String Area = "Area";
    public static final String AreaSizeUnitId = "AreaSizeUnitId";
    public static final String CoverAreaSize = "CoverAreaSize";
    public static final String CoverAreaSizeUnitId = "CoverAreaSizeUnitId";
    public static final String SuperAreaSize = "SuperAreaSize";

    public static final String SuperAreaSizeUnitId = "SuperAreaSizeUnitId";
    public static final String BuiltUpAreaSize = "BuiltUpAreaSize";
    public static final String BuiltUpAreaSizeUnitId = "BuiltUpAreaSizeUnitId";
    public static final String CarpetAreaSize = "CarpetAreaSize";
    public static final String CarpetAreaSizeUnitId = "CarpetAreaSizeUnitId";
    public static final String ClubAmenities = "ClubAmenities";
    public static final String Amenity = "Amenity";
    public static final String Club = "Club";

    public static final String Title = "Title";
    public static final String Value = "Value";
    public static final String Size = "Size";
    public static final String SizeUnitId = "SizeUnitId";
    public static final String ClubFiles = "ClubFiles";
    public static final String IsDeletable = "IsDeletable";
    public static final String IsModify = "IsModify";


    public final static int PASSWORD_MIN_LENGTH = 6;
    public static final String ADD_TOWNSHIP_API = "township_api";
    public static final String UPLOAD_IMAGE_API = "upload_image_api";
    public static final String LEGAL_INFO_URL = "LEGAL_INFO_URL";
    public static final String AUTHENTICATE_USER_API = "authenticate_user_api";
    public static final String LOGIN_USER_API = "login_user_api";
    public final static String AREA = "area";
    public final static String LENGTH = "length";


    //////////////////////////////////////////////// State data ///////////////////////////////////////////////

    public static String ENTITY_ID = "0" ;
    public  static String STATE_ID = "0";
    public  static String DISTRCT_ID = "0";
    public  static String SUB_DIVISION_ID = "0";
    public  static String TEHSIL_ID = "0";
    public  static String TOWNID = "0";
    public  static String TOWNSHIP_ID = "0";
    public  static String LOCALITY_ID = "0";

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static String[] CURRENCY_NAME = {
            "ALL", "AFN", "ARS", "AWG", "AUD", "BSD",
            "BBD", "BYR", "BZD", "BMD",
            "BOB", "BAM", "BWP", "BGN", "BRL", "BND", "KHR", "CAD", "KYD", "CLP",
            "CNY", "COP", "CRC", "HRK", "CUP", "CZK", "DKK", "DOP", "XCD", "EGP",
            "SVC", "EEK", "EUR", "FKP", "FJD", "GHC", "GIP", "GTQ", "GGP", "GYD",
            "HNL", "HKD", "HUF", "ISK", "INR", "IDR", "IRR", "IMP", "ILS", "JMD",
            "JPY", "JEP", "KZT", "KPW", "KRW", "KGS", "LAK", "LVL", "LBP", "LRD",
            "LTL", "MKD", "MYR", "MUR", "MXN", "MNT", "MZN", "NAD", "NPR", "ANG",
            "NZD", "NIO", "NGN", "NOK", "OMR", "PKR", "PAB", "PYG", "PEN", "PHP",
            "PLN", "QAR", "RON", "RUB", "SHP", "SAR", "RSD", "SCR", "SGD", "SBD",
            "SOS", "ZAR", "LKR", "SEK", "CHF", "SRD", "SYP", "TWD", "THB", "TTD",
            "TRL", "TVD", "UAH", "GBP", "USD", "UYU", "UZS", "VEF", "VND", "YER",
            "ZWD"
    };
    public static String[] CURRENCY_SYMBOL = {
            "Lek", "\u060B", "$", "ƒ", "$", "$",
            "$", "p.", "BZ$", "$",
            "$b", "KM", "P", "лв", "R$", "$", "៛", "$", "$", "$",
            "¥", "$", "₡", "kn", "₱", "Kč", "kr", "RD$", "$", "£",
            "$", "kr", "€", "£", "$", "¢", "£", "Q", "£", "$",
            "L", "$", "Ft", "kr", "\u20B9", "Rp", "﷼", "£", "₪", "J$",
            "¥", "£", "лв", "₩", "₩", "лв", "₭", "Ls", "£", "$",
            "Lt", "ден", "RM", "₨", "$", "₮", "MT", "$", "₨", "ƒ",
            "$", "C$", "₦", "kr", "﷼", "₨", "B/.", "Gs", "S/.", "₱",
            "zł", "﷼", "lei", "\u20BD", "£", "﷼", "Дин.", "₨", "$", "$",
            "S", "S", "₨", "kr", "CHF", "$", "£", "NT$", "฿", "TT$",
            "\u20BA", "$", "\u20B4", "£", "$", "$U", "лв", "Bs", "₫", "﷼",
            "Z$"
    };
}
