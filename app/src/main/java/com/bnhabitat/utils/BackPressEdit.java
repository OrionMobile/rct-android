package com.bnhabitat.utils;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

/**
 * Created by gourav on 12/19/2016.
 */


    public class BackPressEdit extends EditText
    {
        /* Must use this constructor in order for the layout files to instantiate the class properly */
        public BackPressEdit(Context context, AttributeSet attrs)
        {
            super(context, attrs);
            // TODO Auto-generated constructor stub
        }

        private KeyImeChange keyImeChangeListener;

        public void setKeyImeChangeListener(KeyImeChange listener)
        {
            keyImeChangeListener = listener;
        }

        public interface KeyImeChange
        {
            public void onKeyIme(int keyCode, KeyEvent event);
        }

        @Override
        public boolean onKeyPreIme(int keyCode, KeyEvent event)
        {
            if (keyImeChangeListener != null)
            {
                keyImeChangeListener.onKeyIme(keyCode, event);
            }
            return false;
        }
    }

