package com.bnhabitat.utils;


public class Urls {
//public static int pos = 0;
  public static final String URL = "https://catalystapi.realsynergy.estate/";
  public static final String URL_PROPERTY_ADD_BEDROOM_PROJECT = "project/accomodation/post";
// public static final String CONTACT_MANAGEMENT_URL = "http://rctcontactsdevapi.realsynergy.in/api/";
   public static final String CONTACT_MANAGEMENT_URL = "http://rctcontactsqaapi.realsynergy.in/api/";
 // public static final String BASE_CONTACT_IMAGE_URL = "http://rctcontactsdevapi.realsynergy.in";
  public static final String BASE_CONTACT_IMAGE_URL = "http://rctcontactsqaapi.realsynergy.in";

    public static final String REMOVE_IMAGE = CONTACT_MANAGEMENT_URL + "township/removefile";
    public static final String URL_PROPERTY_ADD_ACCOMODATION_BEDROOM = "project/accomodation/room/post";
    public static final String URL_PROJECT_ADD_BEDROOM = "project/bedroom/post";
    public static final String URL_ADD_BASEMENT = "project/basement/post";
    public static final String URL_ADD_TOWER_INFORMATION = "project/tower/post";
    public static final String URL_PROJECT_LISTS = "project/bycity/";
    public static final String URL_ALL_PROJECT_LISTING = "project/all/";
    public static final String URL_PROPERTY_PHOTOS_PROJECT = "project/upload";
    public static final String URL_PROPERTY_PHOTOS_PROJECT_ACCOMO = "project/upload";

    public static final String URL_PROPERTY_PHOTOS_NEIGHBOURHOOD = "Project/neighborhood/upload";
    public static final String URL_PROPERTY_DELETE_ROOM_OR_BALCONY_ACCOMODATION = "project/accomodation/room/remove";
    public static final String URL_PROPERTY_DELETE_ACCOMODATION = "project/accomodation/remove";
    public static final String URL_PROPERTY_DELETE_TOWERS = "project/tower/remove";

    public static final String URL_TOWER_LISTING = "/project/towerbytype/";
    public static final String URL_PROJECT_LISTING = "/project/accomodation/list/";
    public static final String URL_ACCOMODATION_ROOM_LISTING = "/project/accomodation/room/all";
    public static final String URL_REMOVE_PROPERTY_PHOTOS_PROJECT = "project/removefile";
    public static final String PROPERTY_LAY_DISPLAY = CONTACT_MANAGEMENT_URL + "property/types/";
//*************************************************Area Module********************************************************

    public static final String URL_STATE_LIST = "area/states/1/";
    public static final String URL_DISTRICT_LIST = "area/districts/0/";
    public static final String URL_STATE_DETAIL = "area/state/";
    public static final String URL_CITY_LIST = "area/areas/Town/1/";
    public static final String URL_VILLAGE_LIST = "area/villages/0/";
    public static final String URL_DISTRICT_DETAIL = "area/district/";
    public static final String URL_CITY_DETAIL = "area/area/";
    public static final String URL_VILLAGE_DETAIL = "area/village/";
    public static final String URL_SECTOR_LIST = "area/areas/Locality/1/";
    public static final String URL_TOWNSHIP_LIST = "township/bytownid/";

    public static final String URL_SECTOR_DETAIL = "area/area/";
    public static final String URL_TOWNSHIP_DETAIL = "township/detail/";
    public static final String URL_PROPERTY_LISTING = "property/propertiesbycity/";
    public static final String URL_PROPERTY_DETAILING = "property/detail/";


    public static final String URL_PROJECT_DETAIL = "project/details/";
    //public static final String BASE_CONTACT_IMAGE_URL = "http://rctcontactsqaapi.realsynergy.in";

    public static final String URL_PROJECT_LIST = "project/search/";
    public static final String URL_PUBLISH_PROJECT_LIST = "search/";
    public static final String URL_AUTHENTICATE = "Authenticate";
    public static final String URL_FORGOT = "utilities/forgotpassword";
    public static final String URL_PROJECT_DETAILS = "project/detail/";
    public static final String URL_REGISTER = "utilities/customer/register";
    public static final String URL_LOGIN = "utilities/customer/login";
    public static final String URL_LOGIN_FB = "utilities/customer/login/social";
    public static final String URL_SEND_OTP = "utilities/sendotp";
    public static final String URL_CHANGE_PASSWORD = "utilities/changepassword";
    public static final String URL_GET_TOKEN = "user/authenticate";
    public static final String URL_PROJECT_MODIFIED = "project/ModifyDate/";
    public static final String ADD_PROJECT = CONTACT_MANAGEMENT_URL + "project/post";
    public static final String ADD_TOWNSHIP = CONTACT_MANAGEMENT_URL + "township/post";

    public static final String BASE_IMAGE_URL = "http://developer.realsynergy.estate";

    public static final String URL_MASTER = "utilities/masterdata";
    public static final String URL_CURRENCY_CONVERTOR = "utilities/currencyrate/INR";
    public static final String SEARCH_PROJECT = "project/search/";
    public static final String PUBLISHED_PROJECT = "broker/brokerproject/add";
    public static final String UNPUBLISHED_PROJECT = "broker/brokerproject/remove";
    // public static final String URL_ALL_CONTACTS = "contact/search/1/";
    public static final String URL_ALL_CONTACTS = "contact/search/1/";

    public static final String URL_CONTACT_DETAIL = "contact/detail/";
    public static final String URL_ALL_USERS = "user/all/";
    //    public static final String URL_POST_CONTACTS = "http://rctcontactsqaapi.realsynergy.in/api/contact/create";
    public static final String URL_POST_CONTACTS = "contact/bulkinsert";
    public static final String URL_CREATE_CONTACTS = "contact/post";
    public static final String URL_VIEW_CONTACTS = "contact/detail/";
    public static final String URL_CREATE_GROUP = "group/post";

    public static final String URL_GROUP_ADD = "term/add";
    public static final String URL_POST_CONTACTS_GROUP = "contact/bulkgroupmap";
    public static final String URL_GROUP_LISTING = "group/Get/1/";
    public static final String URL_TERM_TAXONOMY = "term/texonomy/";
    public static final String URL_GET_ALL_GROUPS = "group/Get/1/";
    public static final String URL_GET_ALL_USERS = "user/all/";
    public static final String URL_GET_ALL_FIELDS = "contact/fields";
    public static final String URL_GET_COMPANY_NAMES = "company/all/1";
    public static final String URL_POST_ASSIGN_GROUP = "contact/bulkgroupmap";
    public static final String URL_POST_ASSIGN_USER = "contact/bulkusermap";
    public static final String URL_POST_ASSIGN_COMPANY_INVITE = "contact/addinvite";
    public static final String URL_GET_DASHBOARD_GRAPH_DATA = "user/dashboard/1/";
    public static final String URL_GET_TERMS_DATA = "Term/Get/";
    public static final String URL_PERMISSIONS = "group/GetPermission";
    public static final String URL_FIELD = "contact/fields/0";


    //********************INVENTORY MODULE API**********************************
    public static final String URL_PROPERTY_TYPE = "property/types/";
    public static final String URL_AREA_ENTITY = "area/entities/";
    public static final String URL_PIN_DETAIL = "utilities/pincodedetail/";
    public static final String URL_PROPERTY_UNITS = "property/units";
    public static final String URL_PROJECT_NEIGHBOURHOODS = "project/neighborhoods";
    public static final String URL_PROPERTY_BANKS = "property/banks";
    public static final String URL_PROPERTY_PHOTOS = "property/upload";
    public static final String URL_REMOVE_PROPERTY_PHOTOS = "property/removefile";
    public static final String URL_PROPERTY_POST = "property/post";
    public static final String URL_PROPERTY_NEIGHBORHOOD = "Project/neighborhood";
    public static final String URL_PROPERTY_STATE_LISTING ="/utilities/states";

   // public static final String URL_PROPERTY_STATE_LISTING_MAIN ="/area/states/1/30";

    public static final String URL_STATE_LISTING_FINAL ="/area/entities/";

    public static final String URL_PROPERTY_FLOOR = "property/floor/post";
    public static final String URL_PROPERTY_FLOOR_PROJECT = "project/floor/post";

    public static final String URL_REMOVE_FLOOR = "property/floor/remove";
    public static final String URL_PROPERTY_BEDROOM = "property/bedrooms/";
    public static final String URL_PROPERTY_BEDROOM_TYPES = "property/bedrooms/types";
    public static final String URL_PROPERTY_BEDROOM_ALL = "property/bedroom/all/";
    public static final String URL_PROPERTY_ADD_BEDROOM = "property/bedroom/post";
    public static final String URL_PROPERTY_ALL_BEDROOM_PROJECT = "project/bedroom/all";
    public static final String URL_PROPERTY_DELETE_BEDROOM = "property/bedroom/remove";
    public static final String URL_INVENTORY_LIST = "property/all/1";
    //  public static final String URL_FORYOU_LIST = "property/all/1/true";
    public static final String URL_RECOMMENDED_LIST = "property/recommended/1";
    public static final String URL_PROPERTY_DETAIL = "property/detail/";
    public static final String URL_GET_COUNTRY_LIST = "utilities/countries/";
    public static final String ALL_PROPERTY = "property/all/1";
    public static final String FLOOR_LIST = "property/floors/";
    public static final String FLOOR_LIST_NEIGHBOURBOOD = "project/neighborhoodfloors/";

    public static final String LEGAL_INFO_URL = CONTACT_MANAGEMENT_URL + "project/legalinfo/post";
    public static final String UPLOAD_IMAGE = CONTACT_MANAGEMENT_URL + "township/upload";
    public static final String AUTHENTICATE_USER = CONTACT_MANAGEMENT_URL + "user/authenticate";
    public static final String LOGIN_USER = CONTACT_MANAGEMENT_URL + "utilities/customer/login";
    public static final String VERIFY_USER_TRUE = CONTACT_MANAGEMENT_URL + "utilities/customer/verify";
    public static final String VERIFY_USER = "https://control.msg91.com/api/verifyRequestOTP.php?";
    public static final String GET_COUNTRY_LIST = "http://rctcontactsdevapi.realsynergy.in/api/utilities/countries";

}
