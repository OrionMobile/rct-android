package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.CountryModel;
import com.bnhabitat.models.FeatureParkModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.township.model.BasementModel;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.utils.BackPressEdit;
import com.bnhabitat.utils.MultiTextWatcher;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class BasementAdapter extends RecyclerView.Adapter<BasementAdapter.ViewHolder> {
    Context context;

    ArrayList<BasementModel> propertyBedroomModels = new ArrayList<>();

    public BasementAdapter(Context context, ArrayList<BasementModel> propertyBedroomModels) {
        this.context = context;
        this.propertyBedroomModels = propertyBedroomModels;
        this.propertyBedroomModels = propertyBedroomModels;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_basement, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.title.setText(propertyBedroomModels.get(position).getTitle());

        new MultiTextWatcher()
                .registerEditText(holder.et_basement)
                .setCallback(new MultiTextWatcher.TextWatcherWithInstance() {
                    @Override
                    public void beforeTextChanged(EditText editText, CharSequence s, int start, int count, int after) {
                        // TODO: Do some thing with editText
                    }

                    @Override
                    public void onTextChanged(EditText editText, CharSequence s, int start, int before, int count) {
                        // TODO: Do some thing with editText
                    }

                    @Override
                    public void afterTextChanged(EditText editText, Editable editable) {
                        // TODO: Do some thing with editText

                        final BasementModel basementModel = new BasementModel();

                        basementModel.setTitle(holder.et_basement.getText().toString());

                        propertyBedroomModels.set(position, basementModel);
                    }
                });

    }

    @Override
    public int getItemCount() {
        return propertyBedroomModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        EditText et_basement;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            et_basement = (EditText) itemView.findViewById(R.id.et_basement);
        }


    }


}
