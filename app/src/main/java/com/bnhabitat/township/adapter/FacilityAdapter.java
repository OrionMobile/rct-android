package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.bnhabitat.R;
import com.bnhabitat.township.model.BasementModel;

import java.util.ArrayList;

public class FacilityAdapter extends RecyclerView.Adapter<FacilityAdapter.ViewHolder> {
    Context context;

    ArrayList<BasementModel> propertyBedroomModels = new ArrayList<>();

    public FacilityAdapter(Context context, ArrayList<BasementModel> propertyBedroomModels) {
        this.context = context;
        this.propertyBedroomModels = propertyBedroomModels;
        this.propertyBedroomModels = propertyBedroomModels;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_type_of_accomo, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.cb_title.setText(propertyBedroomModels.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return propertyBedroomModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox cb_title;

        public ViewHolder(View itemView) {
            super(itemView);

            cb_title = (CheckBox) itemView.findViewById(R.id.cb_title);

        }


    }


}
