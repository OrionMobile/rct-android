package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.township.model.PropertyTypeModel;

import java.util.ArrayList;
import java.util.HashSet;

import de.greenrobot.event.EventBus;

public class SelectTypeAdapter extends RecyclerView.Adapter<SelectTypeAdapter.ViewHolder> {

    Context context;

    //String[] category_type;
    ArrayList<PropertyTypeModel> category_type = new ArrayList<>();
    ArrayList<PropertyTypeModel> selected_category_type = new ArrayList<>();
    PropertyTypeModel propertyTypeModel = new PropertyTypeModel();


    int selectedPos = -1;
    String selectedNames = "";

    ArrayList<String> listPrefCategory = new ArrayList<>();
    ArrayList<Boolean> isContained = new ArrayList<>();

    public SelectTypeAdapter(Context context, ArrayList<PropertyTypeModel> category_type) {

        this.context = context;
        this.category_type = category_type;

    }


    @Override
    public SelectTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_select_type, parent, false);

        return new SelectTypeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SelectTypeAdapter.ViewHolder holder, final int position) {


        holder.slctText.setText(category_type.get(position).getName().toString());
        holder.slctText.setTextColor(context.getResources().getColor(R.color.light_circle_blue));
        holder.lnrOut.setBackground(context.getResources().getDrawable(R.drawable.background_blue_white));

        holder.lnrOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedNames = "";

                if (listPrefCategory.contains(category_type.get(position).getName().toString())) {
                    listPrefCategory.remove(category_type.get(position).getName());
                    //  selected_category_type.remove(position);
                   /* holder.slctText.setTextColor(context.getResources().getColor(R.color.white));
                    holder.lnrOut.setBackground(context.getResources().getDrawable(R.drawable.background_blue_slctd));
*/
                    propertyTypeModel.setId(category_type.get(position).getId());
                    propertyTypeModel.setName(category_type.get(position).getName());
                    propertyTypeModel.setType(category_type.get(position).getType());
                    propertyTypeModel.setIsActive(category_type.get(position).getIsActive());
                    propertyTypeModel.setDelete("yes");

                    holder.slctText.setTextColor(context.getResources().getColor(R.color.light_circle_blue));
                    holder.lnrOut.setBackground(context.getResources().getDrawable(R.drawable.background_blue_white));

                    HashSet<String> hashSet = new HashSet<String>();
                    hashSet.addAll(listPrefCategory);
                    listPrefCategory.clear();
                    listPrefCategory.addAll(hashSet);
                    EventBus.getDefault().post(selected_category_type);
                } else {
                    listPrefCategory.add(category_type.get(position).getName());

                    propertyTypeModel.setId(category_type.get(position).getId());
                    propertyTypeModel.setName(category_type.get(position).getName());
                    propertyTypeModel.setType(category_type.get(position).getType());
                    propertyTypeModel.setIsActive(category_type.get(position).getIsActive());
                    propertyTypeModel.setDelete("no");

                   /* holder.slctText.setTextColor(context.getResources().getColor(R.color.light_circle_blue));
                    holder.lnrOut.setBackground(context.getResources().getDrawable(R.drawable.background_blue_white));
                   */
                    holder.slctText.setTextColor(context.getResources().getColor(R.color.white));
                    holder.lnrOut.setBackground(context.getResources().getDrawable(R.drawable.background_blue_slctd));

                    selected_category_type.clear();
                    selected_category_type.add(propertyTypeModel);

                    HashSet<String> hashSet = new HashSet<String>();
                    hashSet.addAll(listPrefCategory);
                    listPrefCategory.clear();
                    listPrefCategory.addAll(hashSet);
                    EventBus.getDefault().post(selected_category_type);
                }

               /* HashSet<String> hashSet = new HashSet<String>();
                hashSet.addAll(listPrefCategory);
                listPrefCategory.clear();
                listPrefCategory.addAll(hashSet);
                EventBus.getDefault().post(selected_category_type);*/


            }
        });


    }

    @Override
    public int getItemCount() {
        return category_type.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView slctText;
        LinearLayout lnrOut;

        public ViewHolder(View itemView) {
            super(itemView);

            slctText = (TextView) itemView.findViewById(R.id.slctText);
            lnrOut = (LinearLayout) itemView.findViewById(R.id.lnrOut);
        }
    }
}
