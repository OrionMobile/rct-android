/*
package com.bnhabitat.township.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.township.fragment.Flat_apartment_fragment;
import com.bnhabitat.township.model.AccomodationModel;
import com.bnhabitat.township.model.FloorModel;
import com.bnhabitat.township.model.FloorUnitModel;
import com.bnhabitat.township.model.TownshipFile;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.township.townactivities.Project_name;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;

 public class FloorListAdapter extends RecyclerView.Adapter<FloorListAdapter.ViewHolder> {

    Context context;
    ArrayList<String> demoFloorLists = new ArrayList<>();
    SpinnerAdapter spinnerAdapter;
    ArrayList<AccomodationModel> accomodationList = new ArrayList<>();
    ArrayList<String> acomoNamesList = new ArrayList<>();
    int range = 0;
    ArrayList<FloorUnitModel> floorUnitModels = new ArrayList<>();
    private PlotLotUnitAdapter plotLotUnitAdapter;
    private ArrayList<FloorUnitModel> plotlotUnitModels = new ArrayList<>();
    int minVal = 0;
    ArrayList<String> listUnit = new ArrayList<>();

    public FloorListAdapter(Context context, ArrayList<String> demoFloorLists, ArrayList<AccomodationModel> accomodationList) {
        this.context = context;
        this.demoFloorLists = demoFloorLists;
        this.accomodationList = accomodationList;

        for (int i = 0; i < accomodationList.size(); i++) {
            acomoNamesList.add(accomodationList.get(i).getTitle());
        }

        spinnerAdapter = new SpinnerAdapter(context, R.layout.new_spinner_item);
        spinnerAdapter.addAll(acomoNamesList);
        spinnerAdapter.add(context.getString(R.string.select_type));

    }


    @Override
    public FloorListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.floor_data_show, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FloorListAdapter.ViewHolder holder, final int position) {
        holder.floor_name_txt.setText(demoFloorLists.get(position).toString().trim());

        holder.floor_name_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.card_view.setVisibility(View.GONE);
                holder.full_detailing_of_floor.setVisibility(View.VISIBLE);
                holder.floor_name.setText(holder.floor_name_txt.getText().toString().trim());
                holder.floorName.setText(holder.floor_name_txt.getText().toString().trim());

            }
        });

*/
/*
        holder.upload_pics.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    holder.upload_image_video.setVisibility(View.VISIBLE);
                } else
                    holder.upload_image_video.setVisibility(View.GONE);
            }
        });
*//*


        holder.unit_to.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                        && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                    if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                            || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                        plotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                        holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_unit.setAdapter(plotLotUnitAdapter);


                    } else {

                        if (Integer.parseInt(holder.unit_to.getText().toString())

                                > Integer.parseInt(holder.unit_from.getText().toString())) {

                            minVal = Integer.parseInt(holder.unit_from.getText().toString());

                            range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                    Integer.parseInt(holder.unit_from.getText().toString());
                            plotlotUnitModels.clear();

                            if (range > 0) {
                                for (int i = 0; i <= range; i++) {
                                    FloorUnitModel floorUnitModel = new FloorUnitModel();
                                    floorUnitModel.setValue(minVal + i);
                                    plotlotUnitModels.add(floorUnitModel);
                                }

                                plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                                holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                holder.rv_unit.setAdapter(plotLotUnitAdapter);

                            }
                        }
                    }


                } else {

                    plotlotUnitModels.clear();

                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                    holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    holder.rv_unit.setAdapter(plotLotUnitAdapter);

                }

            }
        });

        holder.unit_from.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                        && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                    if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                            || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                        plotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                        holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_unit.setAdapter(plotLotUnitAdapter);


                    } else {

                        if (Integer.parseInt(holder.unit_to.getText().toString())

                                > Integer.parseInt(holder.unit_from.getText().toString())) {

                            minVal = Integer.parseInt(holder.unit_from.getText().toString());

                            range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                    Integer.parseInt(holder.unit_from.getText().toString());
                            plotlotUnitModels.clear();

                            if (range > 0) {
                                for (int i = 0; i <= range; i++) {
                                    FloorUnitModel floorUnitModel = new FloorUnitModel();
                                    floorUnitModel.setValue(minVal + i);
                                    plotlotUnitModels.add(floorUnitModel);
                                }

                                plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                                holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                holder.rv_unit.setAdapter(plotLotUnitAdapter);

                            }
                        }
                    }


                } else {

                    plotlotUnitModels.clear();

                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                    holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    holder.rv_unit.setAdapter(plotLotUnitAdapter);

                }
            }
        });

        final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                context, R.layout.custom_select_spinner, listUnit) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);
                    // tv.setBackgroundColor(getResources().getColor(R.color.black));

                } else {
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);
                    // tv.setBackgroundColor(getResources().getColor(R.color.black));
                }
                return view;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);

                } else {
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);

                }
                return view;
            }
        };

        spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

        holder.spinner_accomodation.setAdapter(spinnerArrayMs);

        holder.iv_del_floor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(position);

            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(position);

            }
        });

        holder.lnrAddMore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

    }


    @Override
    public int getItemCount() {
        return demoFloorLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView card_view;
        LinearLayout full_detailing_of_floor;
       // LinearLayout upload_image_video;
        TextView floor_name_txt, floor_name;
        ImageView delete, edit, copy,iv_del_floor;
        EditText floorName, unit_from, unit_to;
        CheckBox upload_pics;
        Spinner spinner_accomodation;
        RecyclerView rv_unit;
        LinearLayout lnrAddMore1;


        public ViewHolder(View itemView) {
            super(itemView);

            full_detailing_of_floor = (LinearLayout) itemView.findViewById(R.id.full_detailing_of_floor);
         //   upload_image_video = (LinearLayout) itemView.findViewById(R.id.upload_image_video);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            floor_name_txt = (TextView) itemView.findViewById(R.id.floor_name_txt);
            floor_name = (TextView) itemView.findViewById(R.id.floor_name);
            floorName = (EditText) itemView.findViewById(R.id.floorName);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            copy = (ImageView) itemView.findViewById(R.id.copy);
            upload_pics = (CheckBox) itemView.findViewById(R.id.upload_pics);
            spinner_accomodation = (Spinner) itemView.findViewById(R.id.spinner_accomodation);
            unit_from = (EditText) itemView.findViewById(R.id.unit_from);
            unit_to = (EditText) itemView.findViewById(R.id.unit_to);
            rv_unit = (RecyclerView) itemView.findViewById(R.id.rv_unit);
            lnrAddMore1 = (LinearLayout) itemView.findViewById(R.id.lnrAddMore1);
            iv_del_floor= (ImageView) itemView.findViewById(R.id.iv_del_floor);

        }
    }

    public void showDialog(final int pos){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Confirm");
        builder.setMessage("Are you sure?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                // delete Item and close dialog

                demoFloorLists.remove(pos);
                notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}*/
