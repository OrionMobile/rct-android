package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.township.model.BasementModel;
import com.bnhabitat.township.model.FloorUnitModel;
import com.bnhabitat.utils.MultiTextWatcher;

import java.util.ArrayList;

/**
 * Created by gourav on 3/27/2018.
 */

public class FloorUnitAdapter extends RecyclerView.Adapter<FloorUnitAdapter.ViewHolder> {
    Context context;

    ArrayList<FloorUnitModel> propertyBedroomModels = new ArrayList<>();

    public FloorUnitAdapter(Context context, ArrayList<FloorUnitModel> propertyBedroomModels) {
        this.context = context;
        this.propertyBedroomModels = propertyBedroomModels;
        this.propertyBedroomModels = propertyBedroomModels;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_floor_unit, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

      //  holder.et_unit.setText(propertyBedroomModels.get(position).getValue());

        new MultiTextWatcher()
                .registerEditText(holder.et_unit)
                .setCallback(new MultiTextWatcher.TextWatcherWithInstance() {
                    @Override
                    public void beforeTextChanged(EditText editText, CharSequence s, int start, int count, int after) {
                        // TODO: Do some thing with editText
                    }

                    @Override
                    public void onTextChanged(EditText editText, CharSequence s, int start, int before, int count) {
                        // TODO: Do some thing with editText
                    }

                    @Override
                    public void afterTextChanged(EditText editText, Editable editable) {
                        // TODO: Do some thing with editText

                        final FloorUnitModel basementModel = new FloorUnitModel();

                        basementModel.setValue(Integer.parseInt(holder.et_unit.getText().toString()));

                        propertyBedroomModels.set(position, basementModel);
                    }
                });

    }

    @Override
    public int getItemCount() {
        return propertyBedroomModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        EditText et_unit;

        public ViewHolder(View itemView) {
            super(itemView);
            et_unit = (EditText) itemView.findViewById(R.id.et_unit);
        }


    }


}
