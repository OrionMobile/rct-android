package com.bnhabitat.township.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.township.fragment.Flat_apartment_fragment;
import com.bnhabitat.township.model.AccomodationModel;
import com.bnhabitat.township.model.BasementModel;
import com.bnhabitat.township.model.FloorListModel;
import com.bnhabitat.township.model.FloorUnitModel;
import com.bnhabitat.township.model.TowerListModel;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.ui.views.MultiSelectionSpinner;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;


public class TowerListAdapter extends RecyclerView.Adapter<TowerListAdapter.ViewHolder> {

    Activity context;
    ArrayList<TowerListModel> towerList = new ArrayList<>();
    ArrayList<String> listEmbeded = new ArrayList<>();
    LinearLayout lnrEmbeded, lnrAddEmbdedVideo;
    ArrayList<TowerListModel> groupWithTowers = new ArrayList<>();
    String userChoosenTask;
    private static int REQUEST_CAMERA = 101;
    private static int SELECT_FILE = 102;
    private static final int REQUEST_CAMERA_PERMISSION = 100;
    static int flagTower = 0;
    static int flagTower1 = 0;
    static int flagTower2 = 0;
    static int flagTower3 = 0;
    static int flagTower4 = 0;
    String strTower;
    String strTower1;
    String strTower2;
    String strTower3;
    String strTower4;
    TowerDetailAdapter towerDetailAdapter;
    BasementAdapter basementAdapter;
    TypeOfAccomoAdapter typeOfAccomoAdapter;
    FacilityAdapter facilityAdapter;
    ArrayList<BasementModel> basementList = new ArrayList<>();
    ArrayList<BasementModel> typeAccomoList = new ArrayList<>();
    ArrayList<BasementModel> facilityList = new ArrayList<>();
    ArrayList<AccomodationModel> accomodationList;
    Flat_apartment_fragment flat_apartment_fragment;
    private int id = 0;
    ArrayList<PhotosModel> photosModels = new ArrayList<>();
    private ArrayList<FloorListModel> floorArrayList = new ArrayList<>();

    public TowerListAdapter(Activity context, ArrayList<TowerListModel> demoTowerLists, ArrayList<TowerListModel> groupWithTowers, ArrayList<AccomodationModel> accomodationList, ArrayList<PhotosModel> photosModels, Flat_apartment_fragment flat_apartment_fragment) {
        this.context = context;
        this.towerList = demoTowerLists;
        this.groupWithTowers = groupWithTowers;
        this.accomodationList = accomodationList;
        this.flat_apartment_fragment = flat_apartment_fragment;
        this.photosModels=photosModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.tower_data_show, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        lnrEmbeded = (LinearLayout) view.findViewById(R.id.lnrEmbeded);
        lnrAddEmbdedVideo = (LinearLayout) view.findViewById(R.id.lnrAddEmbdedVideo);
        listEmbeded.add("");
        //addEmbededVideo(listEmbeded, lnrEmbeded, lnrAddEmbdedVideo);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tower_name_txt.setText(towerList.get(position).getName());

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  holder.iv_down.performClick();

                Toast.makeText(context, "Under development", Toast.LENGTH_SHORT).show();
            }
        });

        holder.copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TowerListModel towerListModel = new TowerListModel();

                towerListModel.setId(towerList.get(position).getId());
                towerListModel.setProjectCategoryTypeId(towerList.get(position).getProjectCategoryTypeId());
                towerListModel.setName(towerList.get(position).getName());
                towerList.add(towerListModel);
                notifyDataSetChanged();

            }
        });

        holder.iv_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.iv_down.getDrawable().getConstantState() == ContextCompat.getDrawable(context, R.drawable.downarrow).getConstantState()) {

                    holder.full_detailing_of_tower.setVisibility(View.VISIBLE);
                    holder.tower_name.setText(holder.tower_name_txt.getText().toString().trim());
                    holder.et_towername.setText(holder.tower_name_txt.getText().toString().trim());

                    holder.iv_down.setImageDrawable(context.getResources().getDrawable(R.drawable.upwardarrow));
                } else {
                    holder.full_detailing_of_tower.setVisibility(View.GONE);
                    holder.iv_down.setImageResource(R.drawable.downarrow);
                }
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(context.getString(R.string.message))
                        .setContentText(context.getString(R.string.are_you_sure))
                        .setConfirmText(context.getString(R.string.yes))
                        .setCancelText(context.getString(R.string.cancel))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                int towerDeletedId = towerList.get(position).getId();
                           //     flat_apartment_fragment.deleteTower(towerDeletedId);

                                towerList.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .show();

            }
        });

        // holder.multiselect_spinner.setItems();
        //  holder.multiselect_spinner.setListener(context);
        GridLayoutManager gridlayoutManager1 = new GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false);
        towerDetailAdapter = new TowerDetailAdapter(context, groupWithTowers);
        basementAdapter = new BasementAdapter(context, basementList);
        adAccomoType();
        adFacilities();
        typeOfAccomoAdapter = new TypeOfAccomoAdapter(context, typeAccomoList);
        facilityAdapter = new FacilityAdapter(context, facilityList);

        holder.rv_tower_detail.setLayoutManager(gridlayoutManager1);
        holder.rv_basement.setLayoutManager(new LinearLayoutManager(context));
        holder.rv_tower_detail.setAdapter(towerDetailAdapter);
        holder.rv_basement.setAdapter(basementAdapter);
        holder.rv_typeof_accomo.setLayoutManager(new GridLayoutManager(context, 2));
        holder.rv_typeof_accomo.setAdapter(typeOfAccomoAdapter);

        holder.rv_facility.setLayoutManager(new GridLayoutManager(context, 2));
        holder.rv_facility.setAdapter(facilityAdapter);


        if (!groupWithTowers.isEmpty()) {
            holder.rv_tower_detail.setVisibility(View.VISIBLE);
        } else {
            holder.rv_tower_detail.setVisibility(View.GONE);

        }

/*
        holder.tower_name_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.card_view.setVisibility(View.GONE);


            }
        });
*/

        holder.iv_total_tower_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagTower > 0)
                    flagTower--;
                strTower = "TotalRooms%20eq%20" + flagTower + "%20and%20";
                holder.tv_total_tower.setText(String.valueOf(flagTower));

            }
        });

        holder.tv_total_tower.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                if (!holder.tv_total_tower.getText().toString().equalsIgnoreCase("")) {

                    int size = Integer.parseInt(holder.tv_total_tower.getText().toString());

                    floorArrayList.clear();

                    for (int i = 0; i < size; i++) {

                        if (i == 0) {
                            FloorListModel floorListModel = new FloorListModel();
                            floorListModel.setName("Ground Floor");
                            floorListModel.setAccomodationList(new ArrayList<AccomodationModel>());
                            floorArrayList.add(floorListModel);
                        } else {

                            FloorListModel floorListModel = new FloorListModel();
                            floorListModel.setName(i + " " + "Floor");
                            floorListModel.setAccomodationList(new ArrayList<AccomodationModel>());
                            floorArrayList.add(floorListModel);

                        }

                    }

                    holder.floorListAdapter = new FloorListAdapter(context, floorArrayList, accomodationList);
                    holder.floors_listing.setLayoutManager(new LinearLayoutManager(context));
                    holder.floors_listing.setAdapter(holder.floorListAdapter);

                }

            }
        });

        holder.ll_groupOfTower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StatusModel statusModel = new StatusModel();
                statusModel.setOpen("yes");
                statusModel.setPos(position);
                EventBus.getDefault().postSticky(statusModel);

            }
        });

        holder.iv_total_tower_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagTower++;
                strTower = "TotalRooms%20eq%20" + flagTower + "%20and%20";
                holder.tv_total_tower.setText(String.valueOf(flagTower));
            }
        });

        holder.iv_passenger_lift_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagTower1 > 0)
                    flagTower1--;
                strTower1 = "TotalRooms%20eq%20" + flagTower1 + "%20and%20";
                holder.tv_passenger_lift.setText(String.valueOf(flagTower1));

            }
        });

        holder.iv_passenger_lift_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagTower1++;
                strTower1 = "TotalRooms%20eq%20" + flagTower1 + "%20and%20";
                holder.tv_passenger_lift.setText(String.valueOf(flagTower1));
            }
        });

        holder.iv_cargo_lift_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagTower2 > 0)
                    flagTower2--;
                strTower2 = "TotalRooms%20eq%20" + flagTower2 + "%20and%20";
                holder.tv_cargo_lift.setText(String.valueOf(flagTower2));

            }
        });

        holder.iv_cargo_lift_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagTower2++;
                strTower2 = "TotalRooms%20eq%20" + flagTower2 + "%20and%20";
                holder.tv_cargo_lift.setText(String.valueOf(flagTower2));
            }
        });

        holder.iv_staircase_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagTower3 > 0)
                    flagTower3--;
                strTower3 = "TotalRooms%20eq%20" + flagTower3 + "%20and%20";
                holder.tv_staircase.setText(String.valueOf(flagTower3));

            }
        });

        holder.iv_staircase_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagTower3++;
                strTower3 = "TotalRooms%20eq%20" + flagTower3 + "%20and%20";
                holder.tv_staircase.setText(String.valueOf(flagTower3));
            }
        });

        holder.decBasement1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagTower4 > 0)
                    flagTower4--;
                strTower4 = "TotalRooms%20eq%20" + flagTower4 + "%20and%20";
                holder.basement_text1.setText(String.valueOf(flagTower4));

            }
        });

        holder.incBasement1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagTower4++;
                strTower4 = "TotalRooms%20eq%20" + flagTower4 + "%20and%20";
                holder.basement_text1.setText(String.valueOf(flagTower4));
            }
        });

        holder.lnrUploadCluster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flat_apartment_fragment.checkRunTimePermission();

            }
        });
        imageinflateview(photosModels,holder);

//      holder.floorListAdapter.notifyDataSetChanged();

        holder.lnrAddMore4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAdding();
            }
        });

        holder.lnrAddMore3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddingTypeAccomo();
            }
        });

        holder.lnrAddMore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddingFac();
            }
        });

    }

    public void imageinflateview(final ArrayList<PhotosModel> photosModels,ViewHolder vh) {
        vh.ll_layoutZoingImages.removeAllViews();

        for (int i = 0; i < photosModels.size(); i++) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    flat_apartment_fragment.deletePropertyImages(String.valueOf(id));
                                    photosModels.remove(id);
                                    notifyDataSetChanged();
//                                    imageinflateview(photosModels);
                                }
                            })
                            .show();

                    //  Toast.makeText(context, "cancel is clicked", Toast.LENGTH_SHORT).show();

                }
            });

            vh.ll_layoutZoingImages.addView(view);
        }
    }

    private void adAccomoType() {

        typeAccomoList.clear();

        BasementModel basementModel = new BasementModel();
        basementModel.setTitle("Studio");
        typeAccomoList.add(basementModel);

        BasementModel basementMode1 = new BasementModel();
        basementMode1.setTitle("2BHK Apartment");
        typeAccomoList.add(basementMode1);

        BasementModel basementMode2 = new BasementModel();
        basementMode2.setTitle("3BHK Apartment");
        typeAccomoList.add(basementMode2);

        BasementModel basementMode3 = new BasementModel();
        basementMode3.setTitle("4BHK Apartment");
        typeAccomoList.add(basementMode3);

        BasementModel basementMode4 = new BasementModel();
        basementMode4.setTitle("Duplex Apartment");
        typeAccomoList.add(basementMode4);

        BasementModel basementMode5 = new BasementModel();
        basementMode5.setTitle("Triplex Apartment");
        typeAccomoList.add(basementMode5);

        BasementModel basementMode6 = new BasementModel();
        basementMode6.setTitle("Pentahouse");
        typeAccomoList.add(basementMode6);
    }

    private void adFacilities() {

        facilityList.clear();

        BasementModel basementModel = new BasementModel();
        basementModel.setTitle("Reception Area");
        facilityList.add(basementModel);

        BasementModel basementMode1 = new BasementModel();
        basementMode1.setTitle("Security");
        facilityList.add(basementMode1);

        BasementModel basementMode2 = new BasementModel();
        basementMode2.setTitle("Interior Completed");
        facilityList.add(basementMode2);

        BasementModel basementMode3 = new BasementModel();
        basementMode3.setTitle("Electrical Filings");
        facilityList.add(basementMode3);

        BasementModel basementMode4 = new BasementModel();
        basementMode4.setTitle("Sanitary Fittings");
        facilityList.add(basementMode4);

        BasementModel basementMode5 = new BasementModel();
        basementMode5.setTitle("Ramp for Wheel Chair");
        facilityList.add(basementMode5);

        BasementModel basementMode6 = new BasementModel();
        basementMode6.setTitle("CCTV Camera");
        facilityList.add(basementMode6);
    }

    @Override
    public int getItemCount() {
        return towerList.size();
    }

    public void onAdding() {


        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        // ...Irrelevant code for customizing the buttons and title

        LayoutInflater inflater = context.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.add_other_basement, null);
        dialogBuilder.setView(dialogView);


        final EditText title = (EditText) dialogView.findViewById(R.id.title);
        Button button = (Button) dialogView.findViewById(R.id.save);
        ImageView iv_close = (ImageView) dialogView.findViewById(R.id.iv_close);
        final AlertDialog ad = dialogBuilder.show();
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ad.dismiss();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Commond here......
                if (title.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter the title", Toast.LENGTH_SHORT).show();
                } else {
                    final BasementModel basementModel = new BasementModel();
                    basementModel.setTitle(title.getText().toString());

                    basementList.add(basementModel);
                    basementAdapter.notifyDataSetChanged();
                    ad.dismiss();
                }

            }
        });


    }

    public void onAddingTypeAccomo() {


        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        // ...Irrelevant code for customizing the buttons and title

        LayoutInflater inflater = context.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.add_other_basement, null);
        dialogBuilder.setView(dialogView);


        final EditText title = (EditText) dialogView.findViewById(R.id.title);
        Button button = (Button) dialogView.findViewById(R.id.save);
        ImageView iv_close = (ImageView) dialogView.findViewById(R.id.iv_close);
        final AlertDialog ad = dialogBuilder.show();
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ad.dismiss();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Commond here......
                if (title.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter the title", Toast.LENGTH_SHORT).show();
                } else {
                    final BasementModel basementModel = new BasementModel();
                    basementModel.setTitle(title.getText().toString());

                    typeAccomoList.add(basementModel);
                    typeOfAccomoAdapter.notifyDataSetChanged();
                    ad.dismiss();
                }

            }
        });


    }

    public void onAddingFac() {


        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        // ...Irrelevant code for customizing the buttons and title

        LayoutInflater inflater = context.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.add_other_basement, null);
        dialogBuilder.setView(dialogView);


        final EditText title = (EditText) dialogView.findViewById(R.id.title);
        Button button = (Button) dialogView.findViewById(R.id.save);
        ImageView iv_close = (ImageView) dialogView.findViewById(R.id.iv_close);
        final AlertDialog ad = dialogBuilder.show();
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ad.dismiss();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Commond here......
                if (title.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please enter the title", Toast.LENGTH_SHORT).show();
                } else {
                    final BasementModel basementModel = new BasementModel();
                    basementModel.setTitle(title.getText().toString());

                    facilityList.add(basementModel);
                    facilityAdapter.notifyDataSetChanged();
                    ad.dismiss();
                }

            }
        });


    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView card_view;
        EditText towerName, et_towername;
        LinearLayout full_detailing_of_tower, ll_groupOfTower;
        TextView tv_total_tower, tower_name_txt, tower_name, floor_text, pessenger_text, cargo_text, staircase_text, basement_text1, tv_passenger_lift, tv_cargo_lift,
                tv_staircase;
        ImageView delete, edit, copy;
        RecyclerView floors_listing;
        FloorListAdapter floorListAdapter;
        Button decFloor, incFloor, decPessengerNo, incPessengerNo, decCargoNo, incCargoNo, decStaircaseNo, incStaircaseNo;
        private MultiSelectionSpinner multiselect_spinner;
        RecyclerView rv_tower_detail, rv_basement, rv_typeof_accomo, rv_facility;

        LinearLayout lnrAddMore4, lnrUploadCluster, lnrAddMore3, lnrAddMore1;
        ImageView decBasement1, incBasement1, iv_down;

        ImageView iv_total_tower_minus, iv_total_tower_plus, iv_passenger_lift_minus, iv_passenger_lift_plus,
                iv_staircase_minus, iv_staircase_plus, iv_cargo_lift_minus, iv_cargo_lift_plus;
        LinearLayout ll_layoutZoingImages;
        public ViewHolder(View itemView) {

            super(itemView);
            floors_listing = (RecyclerView) itemView.findViewById(R.id.floors_listing);
            full_detailing_of_tower = (LinearLayout) itemView.findViewById(R.id.full_detailing_of_tower);
            rv_tower_detail = (RecyclerView) itemView.findViewById(R.id.rv_tower_detail);
            rv_basement = (RecyclerView) itemView.findViewById(R.id.rv_basement);
            rv_typeof_accomo = (RecyclerView) itemView.findViewById(R.id.rv_typeof_accomo);
            rv_facility = (RecyclerView) itemView.findViewById(R.id.rv_facility);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            tv_total_tower = (TextView) itemView.findViewById(R.id.tv_total_tower);
            et_towername = (EditText) itemView.findViewById(R.id.et_towername);
            tower_name_txt = (TextView) itemView.findViewById(R.id.tower_name_txt);
            tower_name = (TextView) itemView.findViewById(R.id.tower_name);
            floor_text = (TextView) itemView.findViewById(R.id.floor_text);
            pessenger_text = (TextView) itemView.findViewById(R.id.pessenger_text);
           /* cargo_text = (TextView) itemView.findViewById(R.id.cargo_text);
            staircase_text = (TextView) itemView.findViewById(R.id.staircase_text);*/
            basement_text1 = (TextView) itemView.findViewById(R.id.basement_text1);
            ll_groupOfTower = (LinearLayout) itemView.findViewById(R.id.ll_groupOfTower);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            copy = (ImageView) itemView.findViewById(R.id.copy);
            decFloor = (Button) itemView.findViewById(R.id.decFloor);
            incFloor = (Button) itemView.findViewById(R.id.incFloor);
            decPessengerNo = (Button) itemView.findViewById(R.id.decPessengerNo);
            incPessengerNo = (Button) itemView.findViewById(R.id.incPessengerNo);
           /* decCargoNo = (Button) itemView.findViewById(R.id.decCargoNo);
            incCargoNo = (Button) itemView.findViewById(R.id.incCargoNo);
            decStaircaseNo = (Button) itemView.findViewById(R.id.decStaircaseNo);
            incStaircaseNo = (Button) itemView.findViewById(R.id.incStaircaseNo);*/
            multiselect_spinner = (MultiSelectionSpinner) itemView.findViewById(R.id.multiselectSpinner);
            iv_total_tower_minus = (ImageView) itemView.findViewById(R.id.iv_total_tower_minus);
            iv_passenger_lift_minus = (ImageView) itemView.findViewById(R.id.iv_passenger_lift_minus);
            iv_passenger_lift_plus = (ImageView) itemView.findViewById(R.id.iv_passenger_lift_plus);
            tv_passenger_lift = (TextView) itemView.findViewById(R.id.tv_passenger_lift);
            iv_cargo_lift_minus = (ImageView) itemView.findViewById(R.id.iv_cargo_lift_minus);
            iv_cargo_lift_plus = (ImageView) itemView.findViewById(R.id.iv_cargo_lift_plus);
            tv_cargo_lift = (TextView) itemView.findViewById(R.id.tv_cargo_lift);
            iv_staircase_minus = (ImageView) itemView.findViewById(R.id.iv_staircase_minus);
            iv_staircase_plus = (ImageView) itemView.findViewById(R.id.iv_staircase_plus);
            tv_staircase = (TextView) itemView.findViewById(R.id.tv_staircase);
            iv_total_tower_plus = (ImageView) itemView.findViewById(R.id.iv_total_tower_plus);
            lnrAddMore3 = (LinearLayout) itemView.findViewById(R.id.lnrAddMore3);
            lnrAddMore4 = (LinearLayout) itemView.findViewById(R.id.lnrAddMore4);
            decBasement1 = (ImageView) itemView.findViewById(R.id.decBasement1);
            incBasement1 = (ImageView) itemView.findViewById(R.id.incBasement1);
            lnrUploadCluster = (LinearLayout) itemView.findViewById(R.id.lnrUploadCluster);
            lnrAddMore1 = (LinearLayout) itemView.findViewById(R.id.lnrAddMore1);
            ll_layoutZoingImages = (LinearLayout) itemView.findViewById(R.id.ClusterImg);
            iv_down = (ImageView) itemView.findViewById(R.id.iv_down);

        }
    }

    public void addEmbededVideo(final ArrayList<String> list, final LinearLayout lnrEmbeded, final LinearLayout lnrAddEmbdedVideo) {

        lnrEmbeded.removeAllViews();

        EditText edtAddMore = null;
        View v;

        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = context.getLayoutInflater();
            v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore.setText(list.get(i));

            ImageView imgCloseAddMore = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore.setId(i);

            edtAddMore.setId(i);

            if (i == 0) {
                imgCloseAddMore.setVisibility(View.GONE);
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore.setText(list.get(i));
                }

            }

            final int finalI = i;

            imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) v.findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {

                        if (!list.contains(text)) {

                            list.add(list.size() - 1, text);
                        }
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addEmbededVideo(list, lnrEmbeded, lnrAddEmbdedVideo);
                }
            });

            lnrEmbeded.addView(v);
        }

        final EditText finalEdtAddMore = edtAddMore;
        lnrAddEmbdedVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = finalEdtAddMore.getText().toString().trim();

                if (!finalEdtAddMore.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).trim().equalsIgnoreCase("")) {


                        list.add("");

                    } else {

                        list.add(list.size() - 1, finalEdtAddMore.getText().toString());
                    }

                    addEmbededVideo(list, lnrEmbeded, lnrAddEmbdedVideo);
                } else {
                    Utils.showErrorMessage("Please add embded video", context);
                }
            }
        });


    }

    public class FloorListAdapter extends RecyclerView.Adapter<FloorListAdapter.ViewHolder> {

        Context context;
        ArrayList<FloorListModel> demoFloorLists = new ArrayList<>();
        SpinnerAdapter spinnerAdapter;
        ArrayList<AccomodationModel> accomodationList = new ArrayList<>();
        ArrayList<String> acomoNamesList = new ArrayList<>();


        FloorAccomoAdapter floorAccomoAdapter;

        public FloorListAdapter(Context context, ArrayList<FloorListModel> demoFloorLists, ArrayList<AccomodationModel> accomodationList) {
            this.context = context;
            this.demoFloorLists = demoFloorLists;
            this.accomodationList = accomodationList;

            for (int i = 0; i < accomodationList.size(); i++) {
                acomoNamesList.add(accomodationList.get(i).getTitle());
            }

            spinnerAdapter = new SpinnerAdapter(context, R.layout.new_spinner_item);
            spinnerAdapter.addAll(acomoNamesList);
            spinnerAdapter.add(context.getString(R.string.select_type));

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.floor_data_show, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.floor_name_txt.setText(demoFloorLists.get(position).getName().trim());

            holder.floor_name_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.card_view.setVisibility(View.GONE);
                    holder.full_detailing_of_floor.setVisibility(View.VISIBLE);
                    holder.floor_name.setText(holder.floor_name_txt.getText().toString().trim());
                    holder.floorName.setText(holder.floor_name_txt.getText().toString().trim());

                }
            });

/*
        holder.upload_pics.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    holder.upload_image_video.setVisibility(View.VISIBLE);
                } else
                    holder.upload_image_video.setVisibility(View.GONE);
            }
        });
*/


            holder.iv_del_floor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showDialog(position);

                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showDialog(position);

                }
            });

            if (floorArrayList.get(position).getAccomodationList().isEmpty()) {
                ArrayList<AccomodationModel> accomodationLists = new ArrayList<>();
                AccomodationModel accomodationModel = new AccomodationModel();
                accomodationModel.setBedrooms(0);
                accomodationModel.setTitle("");
                accomodationLists.add(accomodationModel);
                FloorListModel floorListModel = new FloorListModel();
                floorListModel.setAccomodationList(accomodationLists);
                floorListModel.setName(floorArrayList.get(position).getName());
                floorArrayList.set(position, floorListModel);
            }

            floorAccomoAdapter = new FloorAccomoAdapter(context, floorArrayList, floorArrayList.get(position).getAccomodationList());
            holder.rv_floor_accomo.setLayoutManager(new LinearLayoutManager(context));
            holder.rv_floor_accomo.setAdapter(floorAccomoAdapter);

            holder.lnrAddMore1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ArrayList<AccomodationModel> accomodationLists = floorArrayList.get(position).getAccomodationList();
                    AccomodationModel accomodationModel = new AccomodationModel();
                    accomodationModel.setBedrooms(0);
                    accomodationModel.setTitle("");
                    accomodationLists.add(accomodationModel);
                    FloorListModel floorListModel = new FloorListModel();
                    floorListModel.setAccomodationList(accomodationLists);
                    floorArrayList.set(position, floorListModel);
                    floorAccomoAdapter = new FloorAccomoAdapter(context, floorArrayList, floorArrayList.get(position).getAccomodationList());
                    holder.rv_floor_accomo.setLayoutManager(new LinearLayoutManager(context));
                    holder.rv_floor_accomo.setAdapter(floorAccomoAdapter);
                }
            });

        }


        @Override
        public int getItemCount() {
            return demoFloorLists.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CardView card_view;
            LinearLayout full_detailing_of_floor;
            // LinearLayout upload_image_video;
            TextView floor_name_txt, floor_name;
            ImageView delete, edit, copy, iv_del_floor;
            EditText floorName;
            Spinner spinner_accomodation;
            CheckBox upload_pics;

            RecyclerView rv_unit;
            LinearLayout lnrAddMore1;
            RecyclerView rv_floor_accomo;

            public ViewHolder(View itemView) {
                super(itemView);

                full_detailing_of_floor = (LinearLayout) itemView.findViewById(R.id.full_detailing_of_floor);
                //   upload_image_video = (LinearLayout) itemView.findViewById(R.id.upload_image_video);
                card_view = (CardView) itemView.findViewById(R.id.card_view);
                floor_name_txt = (TextView) itemView.findViewById(R.id.floor_name_txt);
                floor_name = (TextView) itemView.findViewById(R.id.floor_name);
                floorName = (EditText) itemView.findViewById(R.id.floorName);
                delete = (ImageView) itemView.findViewById(R.id.delete);
                edit = (ImageView) itemView.findViewById(R.id.edit);
                copy = (ImageView) itemView.findViewById(R.id.copy);
                upload_pics = (CheckBox) itemView.findViewById(R.id.upload_pics);
                spinner_accomodation = (Spinner) itemView.findViewById(R.id.spinner_accomodation);

                rv_unit = (RecyclerView) itemView.findViewById(R.id.rv_unit);
                lnrAddMore1 = (LinearLayout) itemView.findViewById(R.id.lnrAddMore1);
                iv_del_floor = (ImageView) itemView.findViewById(R.id.iv_del_floor);
                rv_floor_accomo = (RecyclerView) itemView.findViewById(R.id.rv_floor_accomo);

            }
        }

        public void showDialog(final int pos) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle("Confirm");
            builder.setMessage("Are you sure to delete?");

            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {

                    // delete Item and close dialog

                    demoFloorLists.remove(pos);
                    notifyDataSetChanged();
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public class FloorAccomoAdapter extends RecyclerView.Adapter<FloorAccomoAdapter.ViewHolder> {

        Context context;
        SpinnerAdapter spinnerAdapter;
        ArrayList<AccomodationModel> accomodationList = new ArrayList<>();
        ArrayList<String> acomoNamesList = new ArrayList<>();
        private ArrayList floorAccomoArrayList = new ArrayList();
        ArrayList<String> listUnit = new ArrayList<>();

        int range = 0;
        ArrayList<FloorUnitModel> floorUnitModels = new ArrayList<>();
        private PlotLotUnitAdapter plotLotUnitAdapter;
        private ArrayList<FloorUnitModel> plotlotUnitModels = new ArrayList<>();
        int minVal = 0;

        public FloorAccomoAdapter(Context context, ArrayList<FloorListModel> demoFloorLists, ArrayList<AccomodationModel> accomodationList) {
            this.context = context;
            this.floorAccomoArrayList = accomodationList;
            for (int i = 0; i < accomodationList.size(); i++) {
                acomoNamesList.add(accomodationList.get(i).getTitle());
            }

            spinnerAdapter = new SpinnerAdapter(context, R.layout.new_spinner_item);
            spinnerAdapter.addAll(acomoNamesList);
            spinnerAdapter.add(context.getString(R.string.select_type));

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.floor_accomo_show, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            //  holder.floor_name_txt.setText(demoFloorLists.get(position).toString().trim());

            holder.unit_to.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                            && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                        if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                                || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                            plotlotUnitModels.clear();

                            plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                            holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            holder.rv_unit.setAdapter(plotLotUnitAdapter);


                        } else {

                            if (Integer.parseInt(holder.unit_to.getText().toString())

                                    >= Integer.parseInt(holder.unit_from.getText().toString())) {

                                minVal = Integer.parseInt(holder.unit_from.getText().toString());

                                range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                        Integer.parseInt(holder.unit_from.getText().toString());
                                plotlotUnitModels.clear();

                                if (range >= 0) {
                                    for (int i = 0; i <= range; i++) {
                                        FloorUnitModel floorUnitModel = new FloorUnitModel();
                                        floorUnitModel.setValue(minVal + i);
                                        plotlotUnitModels.add(floorUnitModel);
                                    }

                                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                                    holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    holder.rv_unit.setAdapter(plotLotUnitAdapter);

                                }
                            }

                            else {

                                Toast.makeText(context, "Unit To value must be greater than Unit From.", Toast.LENGTH_LONG).show();

                            }

                        }


                    } else {

                        plotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                        holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_unit.setAdapter(plotLotUnitAdapter);

                    }

                }
            });

            holder.unit_from.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                            && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                        if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                                || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                            plotlotUnitModels.clear();

                            plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                            holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            holder.rv_unit.setAdapter(plotLotUnitAdapter);


                        } else {

                            if (Integer.parseInt(holder.unit_to.getText().toString())

                                    >= Integer.parseInt(holder.unit_from.getText().toString())) {

                                minVal = Integer.parseInt(holder.unit_from.getText().toString());

                                range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                        Integer.parseInt(holder.unit_from.getText().toString());
                                plotlotUnitModels.clear();

                                if (range >= 0) {
                                    for (int i = 0; i <= range; i++) {
                                        FloorUnitModel floorUnitModel = new FloorUnitModel();
                                        floorUnitModel.setValue(minVal + i);
                                        plotlotUnitModels.add(floorUnitModel);
                                    }

                                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                                    holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    holder.rv_unit.setAdapter(plotLotUnitAdapter);

                                }
                            }

                            else {

                                Toast.makeText(context, "Unit To value must be greater than Unit From.", Toast.LENGTH_LONG).show();

                            }

                        }


                    } else {

                        plotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                        holder.rv_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_unit.setAdapter(plotLotUnitAdapter);

                    }
                }
            });

            final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                    context, R.layout.custom_select_spinner, listUnit) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        // Disable the first item from Spinner
                        // First item will be use for hint
                        return false;
                    } else {
                        return true;
                    }
                }

                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the hint text color gray

                        tv.setTextColor(context.getResources().getColor(R.color.black));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //   tv.setPadding(10, 5, 10, 5);
                        tv.setLayoutParams(params);
                        // tv.setBackgroundColor(getResources().getColor(R.color.black));

                    } else {
                        tv.setTextColor(context.getResources().getColor(R.color.black));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //   tv.setPadding(10, 5, 10, 5);
                        tv.setLayoutParams(params);
                        // tv.setBackgroundColor(getResources().getColor(R.color.black));
                    }
                    return view;
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the hint text color gray
                        tv.setTextColor(context.getResources().getColor(R.color.black));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //   tv.setPadding(10, 5, 10, 5);
                        tv.setLayoutParams(params);

                    } else {
                        tv.setTextColor(context.getResources().getColor(R.color.black));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //   tv.setPadding(10, 5, 10, 5);
                        tv.setLayoutParams(params);

                    }
                    return view;
                }
            };

            spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

            holder.spinner_accomodation.setAdapter(spinnerArrayMs);
        }


        @Override
        public int getItemCount() {
            return floorAccomoArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            Spinner spinner_accomodation;
            EditText unit_from, unit_to;
            RecyclerView rv_unit;

            public ViewHolder(View itemView) {
                super(itemView);

                spinner_accomodation = (Spinner) itemView.findViewById(R.id.spinner_accomodation);
                unit_from = (EditText) itemView.findViewById(R.id.unit_from);
                unit_to = (EditText) itemView.findViewById(R.id.unit_to);
                rv_unit = (RecyclerView) itemView.findViewById(R.id.rv_unit);
            }
        }

    }


}
