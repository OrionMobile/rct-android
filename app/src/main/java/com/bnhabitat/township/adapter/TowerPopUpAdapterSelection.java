package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.township.model.TowerListModel;

import java.util.ArrayList;

/**
 * Created by gourav on 3/27/2018.
 */

public class TowerPopUpAdapterSelection extends RecyclerView.Adapter<TowerPopUpAdapterSelection.ViewHolder> {
    Context context;
    Callback callback;
    ArrayList<TowerListModel> propertyBedroomModels = new ArrayList<>();

    public TowerPopUpAdapterSelection(Context context, ArrayList<TowerListModel> propertyBedroomModels, Callback callback) {
        this.context = context;
        this.propertyBedroomModels = propertyBedroomModels;
        this.callback = callback;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_popup_tower_selection, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.cb_towername.setText(propertyBedroomModels.get(position).getName());
        if (propertyBedroomModels.get(position).getSelected() != null)
            if (propertyBedroomModels.get(position).getSelected().equalsIgnoreCase("checked")) {
                holder.cb_towername.setChecked(true);
            }

        holder.cb_towername.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    TowerListModel towerListModel = new TowerListModel();
                    towerListModel.setSelected("checked");
                    towerListModel.setName(propertyBedroomModels.get(position).getName());
                    towerListModel.setId(propertyBedroomModels.get(position).getId());
                    propertyBedroomModels.set(position, towerListModel);
                    callback.onItemClicked(position, "checked", propertyBedroomModels);
                } else {
                    TowerListModel towerListModel = new TowerListModel();
                    towerListModel.setSelected("");
                    towerListModel.setName(propertyBedroomModels.get(position).getName());
                    towerListModel.setId(propertyBedroomModels.get(position).getId());
                    propertyBedroomModels.set(position, towerListModel);
                    callback.onItemClicked(position, "unchecked", propertyBedroomModels);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return propertyBedroomModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox cb_towername;

        public ViewHolder(View itemView) {
            super(itemView);

            cb_towername = (CheckBox) itemView.findViewById(R.id.cb_towername);
        }


    }

    public interface Callback {

        public void onItemClicked(int position, String selection, ArrayList<TowerListModel> propertyBedroomModels);


    }

}
