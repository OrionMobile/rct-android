package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.township.model.TownshipFile;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.utils.MultiTextWatcher;
import com.bnhabitat.utils.Utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class EmbedClubVideoAdapter extends RecyclerView.Adapter<EmbedClubVideoAdapter.ViewHolder> {

    Context context;
    ArrayList<TownshipFile> arealist = new ArrayList<>();
    String number_count;

    public EmbedClubVideoAdapter(Context context, ArrayList<TownshipFile> arealist) {
        this.context = context;
        this.arealist = arealist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_add_more, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.imgCloseAddMore.setTag(position);

        holder.edtAddMore.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT) {
                    number_count = holder.edtAddMore.getText().toString();
                    EventBus.getDefault().post(number_count);
                    return true;
                }
                return false;
            }
        });

        new MultiTextWatcher()
                .registerEditText(holder.edtAddMore)
                .setCallback(new MultiTextWatcher.TextWatcherWithInstance() {
                    @Override
                    public void beforeTextChanged(EditText editText, CharSequence s, int start, int count, int after) {
                        // TODO: Do some thing with editText
                    }

                    @Override
                    public void onTextChanged(EditText editText, CharSequence s, int start, int before, int count) {
                        // TODO: Do some thing with editText
                    }

                    @Override
                    public void afterTextChanged(EditText editText, Editable editable) {
                        // TODO: Do some thing with editText

                        String video = holder.edtAddMore.getText().toString().trim();
                        if(!video.equalsIgnoreCase("") && !video.equalsIgnoreCase(null)){
                            if(isValid(video)){
                                final TownshipFile townshipFile = new TownshipFile();
                                townshipFile.setUrl(holder.edtAddMore.getText().toString());
                                arealist.set(position, townshipFile);
                            }else {
                                Utils.showErrorMessage("Enter a valid Url", context);
                            }
                        }else{

                        }



                    }
                });

        holder.imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(context.getString(R.string.message))
                        .setContentText(context.getString(R.string.are_you_sure))
                        .setConfirmText(context.getString(R.string.yes))
                        .setCancelText(context.getString(R.string.cancel))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                int i = (Integer) view.getTag();
                                arealist.remove(i);
                                notifyDataSetChanged();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arealist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rltvAddMore;
        EditText edtAddMore;
        ImageView imgCloseAddMore;

        public ViewHolder(View itemView) {
            super(itemView);

            rltvAddMore = (RelativeLayout) itemView.findViewById(R.id.rltvAddMore);
            edtAddMore = (EditText) itemView.findViewById(R.id.edtAddMore);
            imgCloseAddMore = (ImageView) itemView.findViewById(R.id.imgCloseAddMore);
        }
    }

    private boolean isValid(String urlString) {
        try {
            URL url = new URL(urlString);
            return URLUtil.isValidUrl(urlString) && Patterns.WEB_URL.matcher(urlString).matches();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
