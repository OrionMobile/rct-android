package com.bnhabitat.township.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.township.fragment.Residential_fragment;
import com.bnhabitat.township.model.FloorUnitModel;
import com.bnhabitat.township.model.ResidentialFloorModel;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class ResidentialFloorAdapter extends RecyclerView.Adapter<ResidentialFloorAdapter.ViewHolder> {

    final Context context;
    ArrayList<ResidentialFloorModel> residentialFloorModels = new ArrayList<>();
    static int uploadImageType = 1;
    public static final int REQUEST_CAMERA = 5;
    public static final int SELECT_FILE = 0x3;
    LinearLayout layoutZoingImages, lnrEmbededVideo, lnrAddEmbdedVideo;
    EditText edtAddMore;
    ImageView imgCloseAddMore;
    int id = 0;
    Residential_fragment residential_fragment;
    ArrayList<String> embedVideo1 = new ArrayList<>();
    ArrayList<String> listUnit = new ArrayList<>();
    private ArrayList<FloorUnitModel> plotlotUnitModels = new ArrayList<>();
    private PlotLotUnitAdapter plotLotUnitAdapter;
    int minVal = 0;
    private int range;
    static int flagTower1 = 0;
    static int flagTower2 = 0;
    static int flagTower3 = 0;
    String strTower1;
    String strTower2;
    String strTower3;
    ArrayList<String> property_units_id = new ArrayList<>();
    ArrayList<PropertyUnitsModel> propertyUnitsModels = new ArrayList<>();
    private String floorSizeText, floorSizeUnitId;
    LinearLayout ClusterImg;
    SpinnerAdapter adapter1;

    public ResidentialFloorAdapter(Context context, ArrayList<ResidentialFloorModel> residentialFloorModels, ArrayList<String> listUnit, Residential_fragment residential_fragment, ArrayList<PropertyUnitsModel> propertyUnitsModels) {
        this.context = context;
        this.residentialFloorModels = residentialFloorModels;
        this.residential_fragment = residential_fragment;
        this.listUnit = listUnit;
        this.propertyUnitsModels = propertyUnitsModels;
        property_units_id.clear();

        for (int i = 0; i < this.propertyUnitsModels.size(); i++) {
            property_units_id.add(this.propertyUnitsModels.get(i).getSizeUnitId());
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.residential_floor_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int i) {

        holder.street.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                DialogModel dialogModel = new DialogModel();
                dialogModel.setUnit_to(holder.unit_to.getText().toString());
                dialogModel.setUnit_from(holder.unit_from.getText().toString());
                dialogModel.setName(holder.street.getText().toString());
                dialogModel.setPosSpinner(holder.spnrTotalArea.getSelectedItemPosition());
                if (!holder.area.getText().toString().equalsIgnoreCase(""))
                    dialogModel.setArea(Float.parseFloat(holder.area.getText().toString()));

                EventBus.getDefault().post(dialogModel);

            }
        });

        holder.unit_to.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                        && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                    if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                            || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                        plotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                        holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                        holder.ll_unit_dependent.setVisibility(View.GONE);
                        holder.ll_unit.setVisibility(View.GONE);


                    } else {

                        if (Integer.parseInt(holder.unit_to.getText().toString())

                                >= Integer.parseInt(holder.unit_from.getText().toString())) {

                            minVal = Integer.parseInt(holder.unit_from.getText().toString());

                            range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                    Integer.parseInt(holder.unit_from.getText().toString());
                            plotlotUnitModels.clear();

                            if (range >= 0) {
                                for (int i = 0; i <= range; i++) {
                                    FloorUnitModel floorUnitModel = new FloorUnitModel();
                                    floorUnitModel.setValue(minVal + i);
                                    plotlotUnitModels.add(floorUnitModel);
                                }

                                plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                                holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                                holder.ll_unit_dependent.setVisibility(View.VISIBLE);
                                holder.ll_unit.setVisibility(View.VISIBLE);

                            }
                        }

                        else {

                            Toast.makeText(context, "Unit To value must be greater than Unit From.", Toast.LENGTH_LONG).show();

                        }

                    }


                } else {
                    plotlotUnitModels.clear();

                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                    holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                    holder.ll_unit_dependent.setVisibility(View.GONE);
                    holder.ll_unit.setVisibility(View.GONE);

                }

            }
        });

        holder.unit_from.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                        && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                    if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                            || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                        plotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                        holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                        holder.ll_unit_dependent.setVisibility(View.GONE);
                        holder.ll_unit.setVisibility(View.GONE);


                    } else {

                        if (Integer.parseInt(holder.unit_to.getText().toString())

                                >= Integer.parseInt(holder.unit_from.getText().toString())) {

                            minVal = Integer.parseInt(holder.unit_from.getText().toString());

                            range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                    Integer.parseInt(holder.unit_from.getText().toString());
                            plotlotUnitModels.clear();

                            if (range >= 0) {
                                for (int i = 0; i <= range; i++) {
                                    FloorUnitModel floorUnitModel = new FloorUnitModel();
                                    floorUnitModel.setValue(minVal + i);
                                    plotlotUnitModels.add(floorUnitModel);
                                }

                                plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                                holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                                holder.ll_unit_dependent.setVisibility(View.VISIBLE);
                                holder.ll_unit.setVisibility(View.VISIBLE);

                            }
                        }

                        else {

                            Toast.makeText(context, "Unit To value must be greater than Unit From.", Toast.LENGTH_LONG).show();

                        }

                    }


                } else {

                    plotlotUnitModels.clear();

                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                    holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                    holder.ll_unit_dependent.setVisibility(View.GONE);
                    holder.ll_unit.setVisibility(View.GONE);

                }
            }
        });

        final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                context, R.layout.custom_select_spinner, listUnit) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);
                    // tv.setBackgroundColor(getResources().getColor(R.color.black));

                } else {
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);
                    // tv.setBackgroundColor(getResources().getColor(R.color.black));
                }
                return view;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);

                } else {
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);

                }
                return view;
            }
        };

        spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);
        holder.spnrTotalArea.setAdapter(spinnerArrayMs);
        holder.selectSQFT.setAdapter(spinnerArrayMs);
        holder.snrCoverArea.setAdapter(spinnerArrayMs);
        holder.spnrSuperArea.setAdapter(spinnerArrayMs);
        holder.spnrBuildArea.setAdapter(spinnerArrayMs);
        holder.spnrCarpetArea.setAdapter(spinnerArrayMs);

        embedVideo1.clear();
        embedVideo1.add("");
        addEmbededVideo(embedVideo1);

        adapter1 = new SpinnerAdapter(context, R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(context.getString(R.string.select_type));
        holder.spinner_select_floor.setAdapter(adapter1);

        holder.spinner_select_floor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (holder.spinner_select_floor.getSelectedItem() == context.getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    floorSizeText = holder.spinner_select_floor.getSelectedItem().toString();
                    floorSizeUnitId = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        try {

            holder.uploadZoingImages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    residential_fragment.checkRunTimePermission();

                }
            });

        }

        catch (Exception e){
            e.printStackTrace();
        }
        holder.upload_pics.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    holder.add_layout.setVisibility(View.VISIBLE);

                } else
                    holder.add_layout.setVisibility(View.GONE);
            }
        });

        holder.other_info.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    holder.other_info_lay.setVisibility(View.VISIBLE);
                else
                    holder.other_info_lay.setVisibility(View.GONE);
            }

        });

        holder.upload_pics.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    holder.add_layout.setVisibility(View.VISIBLE);

                } else
                    holder.add_layout.setVisibility(View.GONE);
            }
        });

        holder.other_info.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    holder.other_info_lay.setVisibility(View.VISIBLE);
                else
                    holder.other_info_lay.setVisibility(View.GONE);
            }

        });

        holder.dec_floor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagTower1 > 0)
                    flagTower1--;
                strTower1 = "TotalRooms%20eq%20" + flagTower1 + "%20and%20";
                holder.floor_count.setText(String.valueOf(flagTower1));

            }
        });

        holder.inc_floor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagTower1++;
                strTower1 = "TotalRooms%20eq%20" + flagTower1 + "%20and%20";
                holder.floor_count.setText(String.valueOf(flagTower1));
            }
        });

        holder.dec_bedroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagTower2 > 0)
                    flagTower2--;
                strTower2 = "TotalRooms%20eq%20" + flagTower2 + "%20and%20";
                holder.bedroom_count.setText(String.valueOf(flagTower2));

            }
        });

        holder.inc_bedroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagTower2++;
                strTower2 = "TotalRooms%20eq%20" + flagTower2 + "%20and%20";
                holder.bedroom_count.setText(String.valueOf(flagTower2));
            }
        });

        holder.dec_carParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagTower3 > 0)
                    flagTower3--;
                strTower3 = "TotalRooms%20eq%20" + flagTower3 + "%20and%20";
                holder.carParking_count.setText(String.valueOf(flagTower3));

            }
        });

        holder.inc_carParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagTower3++;
                strTower3 = "TotalRooms%20eq%20" + flagTower3 + "%20and%20";
                holder.carParking_count.setText(String.valueOf(flagTower3));
            }
        });

        /*holder.uploadZoingImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageType = 1;
                residential_fragment.checkDrawerPermision();

            }
        });*/
    }

    @Override
    public int getItemCount() {
        return residentialFloorModels.size();
    }

    public void imageinflateview(final ArrayList<PhotosModel> photosModels) {
        ClusterImg.removeAllViews();

        for (int i = 0; i < photosModels.size(); i++) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    residential_fragment.deletePropertyImages(String.valueOf(id));
                                    photosModels.remove(id);
                                    imageinflateview(photosModels);
                                }
                            })
                            .show();

                 //   Toast.makeText(context, "cancel is clicked", Toast.LENGTH_SHORT).show();

                }
            });

            ClusterImg.addView(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox upload_pics, other_info;
        LinearLayout other_info_lay, add_layout, uploadZoingImages, ll_unit_dependent;
        Spinner spnrTotalArea, selectSQFT, snrCoverArea, spnrSuperArea, spnrBuildArea, spnrCarpetArea;
        RecyclerView rv_rowhouse_unit;
        EditText unit_from, unit_to;
        ImageView dec_floor, inc_floor, dec_bedroom, inc_bedroom, dec_carParking, inc_carParking;
        TextView floor_count, bedroom_count, carParking_count;
        EditText street, area;
        LinearLayout ll_unit;
        Spinner spinner_select_floor;

        public ViewHolder(View itemView) {
            super(itemView);

            upload_pics = (CheckBox) itemView.findViewById(R.id.upload_pics);
            other_info = (CheckBox) itemView.findViewById(R.id.other_info);
            other_info_lay = (LinearLayout) itemView.findViewById(R.id.other_info_lay);
            add_layout = (LinearLayout) itemView.findViewById(R.id.add_layout);
            uploadZoingImages = (LinearLayout) itemView.findViewById(R.id.uploadZoingImages);
            layoutZoingImages = (LinearLayout) itemView.findViewById(R.id.layoutZoingImages);
            lnrEmbededVideo = (LinearLayout) itemView.findViewById(R.id.lnrEmbededVideo);
            lnrAddEmbdedVideo = (LinearLayout) itemView.findViewById(R.id.lnrAddEmbdedVideo);
            spnrTotalArea = (Spinner) itemView.findViewById(R.id.spnrTotalArea);
            selectSQFT = (Spinner) itemView.findViewById(R.id.selectSQFT);
            snrCoverArea = (Spinner) itemView.findViewById(R.id.snrCoverArea);
            spnrSuperArea = (Spinner) itemView.findViewById(R.id.spnrSuperArea);
            spnrBuildArea = (Spinner) itemView.findViewById(R.id.spnrBuildArea);
            spnrCarpetArea = (Spinner) itemView.findViewById(R.id.spnrCarpetArea);
            rv_rowhouse_unit = (RecyclerView) itemView.findViewById(R.id.rv_rowhouse_unit);
            unit_from = (EditText) itemView.findViewById(R.id.unit_from);
            unit_to = (EditText) itemView.findViewById(R.id.unit_to);
            ll_unit_dependent = (LinearLayout) itemView.findViewById(R.id.ll_unit_dependent);
            dec_floor = (ImageView) itemView.findViewById(R.id.dec_floor);
            inc_floor = (ImageView) itemView.findViewById(R.id.inc_floor);
            dec_bedroom = (ImageView) itemView.findViewById(R.id.dec_bedroom);
            inc_bedroom = (ImageView) itemView.findViewById(R.id.inc_bedroom);
            dec_carParking = (ImageView) itemView.findViewById(R.id.dec_carParking);
            inc_carParking = (ImageView) itemView.findViewById(R.id.inc_carParking);
            floor_count = (TextView) itemView.findViewById(R.id.floor_count);
            bedroom_count = (TextView) itemView.findViewById(R.id.bedroom_count);
            carParking_count = (TextView) itemView.findViewById(R.id.carParking_count);
            street = (EditText) itemView.findViewById(R.id.street);
            area = (EditText) itemView.findViewById(R.id.area);
            ll_unit = (LinearLayout) itemView.findViewById(R.id.ll_unit);
            spinner_select_floor = (Spinner) itemView.findViewById(R.id.spinner_select_floor);
            ClusterImg = (LinearLayout) itemView.findViewById(R.id.layoutZoingImages);
        }
    }

    public void addEmbededVideo(final ArrayList<String> list) {

        lnrEmbededVideo.removeAllViews();
        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            final View view = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore = (EditText) view.findViewById(R.id.edtAddMore);
            edtAddMore.setText(list.get(i));

            imgCloseAddMore = (ImageView) view.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore.setId(i);

            edtAddMore.setId(i);

            if (i == 0) {
                imgCloseAddMore.setVisibility(View.GONE);
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore.setText(list.get(i));
                } else
                    edtAddMore.setHint(context.getResources().getString(R.string.add_embed_youtube));
            }

            final int finalI = i;

            imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int id1 = finalI;

                    EditText t = (EditText) view.findViewById(id1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            list.add(list.size() - 1, text);
                        }
                    }


                    int id = finalI;
                    list.remove(id);

                    addEmbededVideo(list);
                }
            });

            lnrEmbededVideo.addView(view);
        }

        lnrAddEmbdedVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).equalsIgnoreCase("")) {

                        list.add("");

                    } else {
                        list.add(list.size() - 1, edtAddMore.getText().toString());
                    }

                    addEmbededVideo(list);
                } else {
                    Utils.showErrorMessage("Please add url", context);
                }
            }
        });

    }


}
