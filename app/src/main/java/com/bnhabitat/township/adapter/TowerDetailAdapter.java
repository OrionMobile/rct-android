package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.township.model.BasementModel;
import com.bnhabitat.township.model.TowerListModel;
import com.bnhabitat.utils.MultiTextWatcher;

import java.util.ArrayList;

/**
 * Created by gourav on 3/27/2018.
 */

public class TowerDetailAdapter extends RecyclerView.Adapter<TowerDetailAdapter.ViewHolder> {
    Context context;

    ArrayList<TowerListModel> groupWithTowers = new ArrayList<>();

    public TowerDetailAdapter(Context context, ArrayList<TowerListModel> groupWithTowers) {
        this.context = context;
        this.groupWithTowers = groupWithTowers;


    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_tower_detail, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.btn_towername.setText(groupWithTowers.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return groupWithTowers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        Button btn_towername;

        public ViewHolder(View itemView) {
            super(itemView);

            btn_towername = (Button) itemView.findViewById(R.id.btn_towername);
        }


    }


}
