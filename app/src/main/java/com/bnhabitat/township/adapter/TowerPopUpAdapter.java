package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.township.model.BasementModel;
import com.bnhabitat.utils.MultiTextWatcher;

import java.util.ArrayList;

/**
 * Created by gourav on 3/27/2018.
 */

public class TowerPopUpAdapter extends RecyclerView.Adapter<TowerPopUpAdapter.ViewHolder> {
    Context context;

    ArrayList<String> propertyBedroomModels = new ArrayList<>();

    public TowerPopUpAdapter(Context context, ArrayList<String> propertyBedroomModels) {
        this.context = context;
        this.propertyBedroomModels = propertyBedroomModels;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_popup_tower, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tv_towername.setText(propertyBedroomModels.get(position));

    }

    @Override
    public int getItemCount() {
        return propertyBedroomModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_towername;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_towername = (TextView) itemView.findViewById(R.id.tv_towername);
        }


    }


}
