package com.bnhabitat.township.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.township.model.ProjectCategoryTypesModel;
import com.bnhabitat.township.townactivities.Project_name;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashSet;

public class PropertyTypeAdapter extends RecyclerView.Adapter<PropertyTypeAdapter.ViewHolder> {

    Context context;
    int[] category_images;
    int[] category_imagesSlt;
    String[] category_type;

    String selectedNames = "";

    ArrayList<String> listPrefCategory = new ArrayList<>();

    ArrayList<ProjectCategoryTypesModel> arrayList = new ArrayList<>();
    ProjectCategoryTypesModel projectCategoryTypesModel = new ProjectCategoryTypesModel();

    public PropertyTypeAdapter(Context context, int[] category_images, int[] category_imagesSlt, String[] category_type, ArrayList<ProjectCategoryTypesModel> arrayList) {
        this.context = context;
        this.category_images = category_images;
        this.category_imagesSlt = category_imagesSlt;
        this.category_type = category_type;
        this.arrayList = arrayList;
    }


    @Override
    public PropertyTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_select_property_type, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PropertyTypeAdapter.ViewHolder holder, final int position) {

        holder.iconResidential.setImageResource(category_images[position]);

        holder.lnrOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedNames = "";

                if (position <= 1) {

                    if (listPrefCategory.contains(category_type[position])) {
                        listPrefCategory.remove(category_type[position]);
                        holder.iconResidential.setImageResource(category_images[position]);
                        projectCategoryTypesModel.setId("");
                        arrayList.remove(projectCategoryTypesModel);
                        if (category_type[position].equalsIgnoreCase("Residential"))
                            ((Project_name) context).residential_type_lay.setVisibility(View.GONE);
                        else if (category_type[position].equalsIgnoreCase("Commercial"))

                            ((Project_name) context).commercial_type_lay.setVisibility(View.GONE);

                    } else {
                        listPrefCategory.add(category_type[position]);
                        holder.iconResidential.setImageResource(category_imagesSlt[position]);

                        projectCategoryTypesModel.setId("0");
                        projectCategoryTypesModel.setCategoryTypeId(Integer.toString(position + 1));
                        arrayList.add(projectCategoryTypesModel);
                        ((Project_name) context).propertyData(category_type[position]);
                        ((Project_name) context).type = category_type[position];
                        if (category_type[position].equalsIgnoreCase("Residential"))
                            ((Project_name) context).residential_type_lay.setVisibility(View.VISIBLE);
                        else if (category_type[position].equalsIgnoreCase("Commercial"))

                            ((Project_name) context).commercial_type_lay.setVisibility(View.VISIBLE);

                    }

                    HashSet<String> hashSet = new HashSet<String>();
                    hashSet.addAll(listPrefCategory);
                    listPrefCategory.clear();
                    listPrefCategory.addAll(hashSet);

                }

                //   EventBus.getDefault().post(listPrefCategory);


            }
        });

        /*holder.iconResidential.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position == 0){
                    holder.residential_type_lay.setVisibility(View.VISIBLE);
                }
                if(position == 1){
                    holder.commercial_type_lay.setVisibility(View.VISIBLE);
                }
                if(position == 2){
                    holder.industrial_type_lay.setVisibility(View.VISIBLE);
                }
                if(position == 3){
                    holder.agricultural_type_lay.setVisibility(View.VISIBLE);
                }
                if(position == 4){
                    holder.institutional_type_lay.setVisibility(View.VISIBLE);
                }
            }
        });*/


    }

    @Override
    public int getItemCount() {
        return category_images.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        ImageView iconResidential;
        LinearLayout lnrOut, residential_type_lay, commercial_type_lay, industrial_type_lay, agricultural_type_lay, institutional_type_lay;

        public ViewHolder(View itemView) {
            super(itemView);

            iconResidential = (ImageView) itemView.findViewById(R.id.iconResidential);
            lnrOut = (LinearLayout) itemView.findViewById(R.id.lnrOut);
            residential_type_lay = (LinearLayout) itemView.findViewById(R.id.residential_type_lay);
            commercial_type_lay = (LinearLayout) itemView.findViewById(R.id.commercial_type_lay);
            industrial_type_lay = (LinearLayout) itemView.findViewById(R.id.industrial_type_lay);
            agricultural_type_lay = (LinearLayout) itemView.findViewById(R.id.agricultural_type_lay);
            institutional_type_lay = (LinearLayout) itemView.findViewById(R.id.institutional_type_lay);
        }
    }

}
