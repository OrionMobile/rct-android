package com.bnhabitat.township.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.township.fragment.Flat_apartment_fragment;
import com.bnhabitat.township.fragment.Plot_lotLand_Fragment;
import com.bnhabitat.township.model.FloorUnitModel;
import com.bnhabitat.township.model.FloorUnitModelUnit;
import com.bnhabitat.township.model.PlotLandLot;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class PlotLandLotAdapter extends RecyclerView.Adapter<PlotLandLotAdapter.ViewHolder> {

    final Context context;
    ArrayList<PlotLandLot> plotLandLotArrayList = new ArrayList<>();
    ArrayList<String> listUnit = new ArrayList<>();
    static int uploadImageType = 1;
    public static final int REQUEST_CAMERA = 5;
    public static final int SELECT_FILE = 0x3;
    private ArrayList<String> embededVideoUrls = new ArrayList<>();

    EditText edtAddMore;
    ImageView imgCloseAddMore;
    int id = 0;
    Plot_lotLand_Fragment plot_lotLand_fragment;
    ArrayList<String> embedVideo1 = new ArrayList<>();
    private int range;
    private ArrayList<FloorUnitModel> localPlotlotUnitModels = new ArrayList<>();
    private ArrayList<FloorUnitModelUnit> finalplotlotUnitModels = new ArrayList<>();
    private PlotLotUnitAdapter plotLotUnitAdapter;
    ArrayList<PhotosModel> photosModels = new ArrayList<>();

    int minVal = 0;

    int pos = 0;
    private boolean first = true;

    public PlotLandLotAdapter(Context context, ArrayList<PlotLandLot> plotLandLotArrayList, ArrayList<String> listUnit, ArrayList<FloorUnitModelUnit> finalplotlotUnitModels, Plot_lotLand_Fragment plot_lotLand_fragment, ArrayList<PhotosModel> photosModels) {
        this.context = context;
        this.plotLandLotArrayList = plotLandLotArrayList;
        this.listUnit = listUnit;
        this.plot_lotLand_fragment = plot_lotLand_fragment;
        this.finalplotlotUnitModels = finalplotlotUnitModels;
        this.photosModels = photosModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.plot_lot_land_fragment, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        embededVideoUrls.clear();
        embededVideoUrls.add("");
        holder.rv_embeded_video.setLayoutManager(new LinearLayoutManager(context));
        final EmbededVideoAdapter embededVideoAdapter = new EmbededVideoAdapter(context, embededVideoUrls);
        holder.rv_embeded_video.setAdapter(embededVideoAdapter);

        holder.lnrAddEmbdedVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (embededVideoUrls.contains("")) {

                    Toast.makeText(context, "Please add embed youtube/vimeo code", Toast.LENGTH_SHORT).show();

                } else {

                    embededVideoUrls.add("");
                    embededVideoAdapter.notifyDataSetChanged();
                }

            }
        });

        holder.street.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                DialogModel dialogModel = new DialogModel();
                dialogModel.setUnit_to(holder.unit_to.getText().toString());
                dialogModel.setUnit_from(holder.unit_from.getText().toString());
                dialogModel.setName(holder.street.getText().toString());
                dialogModel.setPosSpinner(holder.spnrTotalArea.getSelectedItemPosition());
                if (!holder.area.getText().toString().equalsIgnoreCase(""))
                    dialogModel.setArea(Float.parseFloat(holder.area.getText().toString()));

                EventBus.getDefault().post(dialogModel);

            }
        });

        holder.unit_to.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                        && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                    if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                            || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                        localPlotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, localPlotlotUnitModels);
                        holder.rv_plot_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_plot_unit.setAdapter(plotLotUnitAdapter);
                        holder.ll_main.setVisibility(View.GONE);


                    } else {

                        if (Integer.parseInt(holder.unit_to.getText().toString())

                                >= Integer.parseInt(holder.unit_from.getText().toString())) {

                            minVal = Integer.parseInt(holder.unit_from.getText().toString());

                            range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                    Integer.parseInt(holder.unit_from.getText().toString());
                            localPlotlotUnitModels.clear();

                            if (range >= 0) {
                                for (int i = 0; i <= range; i++) {
                                    FloorUnitModel floorUnitModel = new FloorUnitModel();
                                    floorUnitModel.setValue(minVal + i);
                                    localPlotlotUnitModels.add(floorUnitModel);
                                }

                                plotLotUnitAdapter = new PlotLotUnitAdapter(context, localPlotlotUnitModels);
                                holder.rv_plot_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                holder.rv_plot_unit.setAdapter(plotLotUnitAdapter);
                                holder.ll_main.setVisibility(View.VISIBLE);

                            }
                        } else {

                            Toast.makeText(context, "Unit To value must be greater than Unit From.", Toast.LENGTH_LONG).show();

                        }
                    }


                } else {
                    localPlotlotUnitModels.clear();

                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, localPlotlotUnitModels);
                    holder.rv_plot_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    holder.rv_plot_unit.setAdapter(plotLotUnitAdapter);
                    holder.ll_main.setVisibility(View.GONE);

                }

            }
        });

        holder.unit_from.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                        && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                    if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                            || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                        localPlotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, localPlotlotUnitModels);
                        holder.rv_plot_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_plot_unit.setAdapter(plotLotUnitAdapter);
                        holder.ll_main.setVisibility(View.GONE);

                    } else {

                        if (Integer.parseInt(holder.unit_to.getText().toString())

                                >= Integer.parseInt(holder.unit_from.getText().toString())) {

                            minVal = Integer.parseInt(holder.unit_from.getText().toString());

                            range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                    Integer.parseInt(holder.unit_from.getText().toString());
                            localPlotlotUnitModels.clear();

                            if (range >= 0) {
                                for (int i = 0; i <= range; i++) {
                                    FloorUnitModel floorUnitModel = new FloorUnitModel();
                                    floorUnitModel.setValue(minVal + i);
                                    localPlotlotUnitModels.add(floorUnitModel);
                                }

                                plotLotUnitAdapter = new PlotLotUnitAdapter(context, localPlotlotUnitModels);
                                holder.rv_plot_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                holder.rv_plot_unit.setAdapter(plotLotUnitAdapter);
                                holder.ll_main.setVisibility(View.VISIBLE);

                            }
                        } else {

                            Toast.makeText(context, "Unit To value must be greater than Unit From.", Toast.LENGTH_LONG).show();

                        }

                    }


                } else {

                    localPlotlotUnitModels.clear();

                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, localPlotlotUnitModels);
                    holder.rv_plot_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    holder.rv_plot_unit.setAdapter(plotLotUnitAdapter);
                    holder.ll_main.setVisibility(View.GONE);

                }
            }
        });

        final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                context, R.layout.custom_select_spinner, listUnit) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray

                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);
                    // tv.setBackgroundColor(getResources().getColor(R.color.black));

                } else {
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);
                    // tv.setBackgroundColor(getResources().getColor(R.color.black));
                }
                return view;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);

                } else {
                    tv.setTextColor(context.getResources().getColor(R.color.black));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    //   tv.setPadding(10, 5, 10, 5);
                    tv.setLayoutParams(params);

                }
                return view;
            }
        };

        spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

        if (first) {

            holder.spnrTotalArea.setAdapter(spinnerArrayMs);
            holder.spinner_sqft.setAdapter(spinnerArrayMs);
            first = false;

        }

      /*  if(mLastSpinnerPosition == i){
            return; //do nothing
        }

        mLastSpinnerPosition = i;
*/

      /*  if (first){


            first = false;

        }*/


        holder.upload_pics.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    holder.ll_add_layout.setVisibility(View.VISIBLE);
                } else
                    holder.ll_add_layout.setVisibility(View.GONE);
            }
        });

        holder.uploadZoingImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageType = 1;
                plot_lotLand_fragment.checkDrawerPermision();

            }
        });

        imageinflateview(photosModels, holder);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return plotLandLotArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox upload_pics;
        LinearLayout ll_add_layout, uploadZoingImages, ll_main;
        Spinner spnrTotalArea, spinner_sqft;
        EditText unit_from, unit_to, street, area;
        RecyclerView rv_plot_unit;
        LinearLayout ll_layoutZoingImages;
        RecyclerView rv_embeded_video;
        LinearLayout lnrAddEmbdedVideo;

        public ViewHolder(View itemView) {
            super(itemView);

            upload_pics = (CheckBox) itemView.findViewById(R.id.upload_pics);
            ll_add_layout = (LinearLayout) itemView.findViewById(R.id.ll_add_layout);
            uploadZoingImages = (LinearLayout) itemView.findViewById(R.id.uploadZoingImages);
            ll_layoutZoingImages = (LinearLayout) itemView.findViewById(R.id.ll_layoutZoingImages);
            //  lnrEmbededVideo = (LinearLayout) itemView.findViewById(R.id.lnrEmbededVideo);
            lnrAddEmbdedVideo = (LinearLayout) itemView.findViewById(R.id.lnrAddEmbdedVideo);
            spnrTotalArea = (Spinner) itemView.findViewById(R.id.spnrTotalArea);
            spinner_sqft = (Spinner) itemView.findViewById(R.id.spinner_sqft);
            unit_from = (EditText) itemView.findViewById(R.id.unit_from);
            unit_to = (EditText) itemView.findViewById(R.id.unit_to);
            rv_plot_unit = (RecyclerView) itemView.findViewById(R.id.rv_plot_unit);
            street = (EditText) itemView.findViewById(R.id.street);
            area = (EditText) itemView.findViewById(R.id.area);
            ll_main = (LinearLayout) itemView.findViewById(R.id.ll_main);
            rv_embeded_video = (RecyclerView) itemView.findViewById(R.id.rv_embeded_video);
        }
    }

    public void imageinflateview(final ArrayList<PhotosModel> photosModels, ViewHolder viewHolder) {
        viewHolder.ll_layoutZoingImages.removeAllViews();

        for (int i = 0; i < photosModels.size(); i++) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    plot_lotLand_fragment.deletePropertyImages(String.valueOf(id));
                                    photosModels.remove(id);
                                    notifyDataSetChanged();
                                }
                            })
                            .show();

                    //    Toast.makeText(context, "cancel is clicked", Toast.LENGTH_SHORT).show();

                }
            });

            viewHolder.ll_layoutZoingImages.addView(view);
        }
    }

    public class EmbededVideoAdapter extends RecyclerView.Adapter<EmbededVideoAdapter.ViewHolder> {

        Context context;
        ArrayList<String> floorNumberList = new ArrayList<>();

        public EmbededVideoAdapter(Context context, ArrayList<String> accomodationList) {
            this.context = context;
            this.floorNumberList = accomodationList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_accomodation_embeded_video, parent, false);
            ViewHolder viewHolder = new EmbededVideoAdapter.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final EmbededVideoAdapter.ViewHolder holder, final int position) {

            holder.et_vidoes.setText(embededVideoUrls.get(position));

            holder.et_vidoes.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    embededVideoUrls.remove(position);
                    embededVideoUrls.add(position, holder.et_vidoes.getText().toString());
                    //   notifyDataSetChanged();

                }
            });

            if (position == 0) {

                holder.imgCloseAddMore.setVisibility(View.GONE);
            } else {

                holder.imgCloseAddMore.setVisibility(View.VISIBLE);

            }

            holder.imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String text = holder.et_vidoes.getText().toString();

                    embededVideoUrls.remove(position);
                    notifyDataSetChanged();


                }
            });

        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            EditText et_vidoes;
            ImageView imgCloseAddMore;

            public ViewHolder(View itemView) {
                super(itemView);
                et_vidoes = (EditText) itemView.findViewById(R.id.et_vidoes);
                imgCloseAddMore = (ImageView) itemView.findViewById(R.id.imgCloseAddMore);

            }

        }
    }


}
