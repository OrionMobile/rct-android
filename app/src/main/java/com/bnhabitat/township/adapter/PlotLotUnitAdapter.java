package com.bnhabitat.township.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.township.model.FloorUnitModel;
import com.bnhabitat.utils.MultiTextWatcher;

import java.util.ArrayList;

public class PlotLotUnitAdapter extends RecyclerView.Adapter<PlotLotUnitAdapter.ViewHolder> {
    Context context;

    ArrayList<FloorUnitModel> unitModelArrayList = new ArrayList<>();

    public PlotLotUnitAdapter(Context context, ArrayList<FloorUnitModel> unitModelArrayList) {
        this.context = context;
        this.unitModelArrayList = unitModelArrayList;
//      this.unitModelArrayList = unitModelArrayList;

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_floor_unit, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.et_unit.setHint(String.valueOf(unitModelArrayList.get(position).getValue()));

        holder.et_unit.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //  Toast.makeText(context, "CLickkk"+holder.et_unit.getText().toString(), Toast.LENGTH_SHORT).show();

                // ((Activity)context).registerForContextMenu(holder.et_unit);

                final CharSequence[] items = {"Edit", "Delete"};

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Select The Action");

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        if (item == 0) {

                            // Toast.makeText(context,String.valueOf(unitModelArrayList.get(position).getValue()), Toast.LENGTH_SHORT).show();

                            showChangeLangDialog(position, String.valueOf(unitModelArrayList.get(position).getValue()));

                        } else if (item == 1) {

                            new AlertDialog.Builder(context)
                                    .setTitle("Alert")
                                    .setMessage("Do you really want to delete unit?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            //    Toast.makeText(context, "Yaay", Toast.LENGTH_SHORT).show();
                                            unitModelArrayList.remove(position);
                                            notifyDataSetChanged();

                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.cancel();
                                        }
                                    }).show();
                        }
                    }
                });
                builder.show();

                return true;
            }
        });

    }

    public void showChangeLangDialog(final int position, String s) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // LayoutInflater inflater = this.getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.et_unit_update);
        edt.setText(s);

        dialogBuilder.setTitle("Update Unit Value");
        dialogBuilder.setMessage("Enter unit value below");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

                unitModelArrayList.remove(position);
                FloorUnitModel floorUnitModel = new FloorUnitModel();
                floorUnitModel.setValue(Integer.valueOf(edt.getText().toString()));
                unitModelArrayList.add(position, floorUnitModel);
                notifyDataSetChanged();

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.cancel();

            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @Override
    public int getItemCount() {
        return unitModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView et_unit;

        public ViewHolder(View itemView) {
            super(itemView);
            et_unit = (TextView) itemView.findViewById(R.id.et_unit);
        }


    }

}
