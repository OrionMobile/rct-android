package com.bnhabitat.township.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.township.adapter.BasementAdapter;
import com.bnhabitat.township.adapter.TowerListAdapter;
import com.bnhabitat.township.adapter.TowerPopUpAdapter;
import com.bnhabitat.township.adapter.TowerPopUpAdapterSelection;
import com.bnhabitat.township.model.AccomodationModel;
import com.bnhabitat.township.model.BasementModel;
import com.bnhabitat.township.model.FloorModel;
import com.bnhabitat.township.model.TowerListModel;
import com.bnhabitat.township.model.TowerModel;
import com.bnhabitat.township.townactivities.Project_name;
import com.bnhabitat.township.townactivities.TabChangeInterface;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.ui.views.MultiSelectionSpinner;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class Flat_apartment_fragment extends Fragment implements CommonAsync.OnAsyncResultListener {

    @BindView(R.id.lnrTypeAcmdtn)
    LinearLayout lnrTypeAcmdtn;
    @BindView(R.id.next_tower)
    Button next_tower;
    @BindView(R.id.add_tower)
    LinearLayout add_tower;
    @BindView(R.id.tower_list_lay)
    LinearLayout tower_list_lay;
    @BindView(R.id.tower_list)
    RecyclerView tower_list;
    TowerListAdapter towerListAdapter;
    TowerPopUpAdapter towerPopUpAdapter;
    ArrayList<String> demoTowerList = new ArrayList<>();
    ImageView minus_btn, plus_btn;
    private RecyclerView rv_accomodation;
    private TextView no_of_count, tv_add_more_accomo, tv_add_more;
    private RelativeLayout rl_add_more_accomo, rl_add_more_basement;
    private AccomoAdapter accomoAdapter;
    private RadioGroup basement_radio_group;
    RadioButton rb_up, rb_ut, rb_nb;
    private AccomoFloorAdapter accomoFloorAdapter;
    private Dialog dialog, dialog1;
    private Spinner spinner_accomo_unit;
    private String spinner_select_id, spinner_select_cover, spinner_select_super, spinner_select_built, spinner_select_carpet;
    private String spinner_accomo_text, spinner_cover, spinner_super, spinner_built, spinner_carpet;
    ArrayList<PropertyUnitsModel> propertyUnitsModels = new ArrayList<>();
    ArrayList<AccomodationModel> accomodationList = new ArrayList<>();
    RecyclerView rv_accomo_detail, rv_floor_names;
    String userChoosenTask;
    ArrayList<PhotosModel> photosModels = new ArrayList<>();
    private boolean isPermitted = false;
    ArrayList<String> checked_room = new ArrayList<>();
    Button next_basement, done_tower;
    SpinnerAdapter adapter, adapter1;
    ArrayList<String> property_units_id = new ArrayList<>();
    ArrayList<String> property_area_id = new ArrayList<>();
    private static int REQUEST_CAMERA = 101;
    private static int REQUEST_CAMERA_ACCOMO = 1011;
    private static int SELECT_FILE = 102;
    private static int SELECT_FILE_ACCOOMO = 1012;
    private static final int REQUEST_CAMERA_PERMISSION = 100;
    int id = 0;
    private String pid;
    private String encodedImageData;
    LinearLayout image_view_infate;
    private EditText et_title, et_unit;
    private String title;
    double totalArea, coverAreaSize = 0, superArea = 0, builtUpArea = 0, carpetArea = 0;
    int totalAreaSizeiD, bedrooms, floors, coverAreaId, superAreaId, builtUpAreaid, carpetAreaId;
    private int proCatTypeId;
    ArrayList<FloorModel> floorModelList = new ArrayList<>();
    ArrayList<TowerModel> towerList = new ArrayList<>();
    ArrayList<TowerListModel> towerListArrayLIst = new ArrayList<>();
    int quantity = 0;
    int quantityFloor = 0;
    private ImageView plus_btn_basement, minus_btn_basement;
    private TextView no_of_count_basement;
    private int accomoId;
    private int accomoDeleteId;
    private int deleteRoomOrBalconyId;
    private int EditRoomOrBalconyId;
    private EditText length;
    private EditText width;
    private EditText room_size;
    private EditText room_name;
    private String type = "";
    private ArrayList<String> embededVideoUrls = new ArrayList<>();
    ArrayList<BasementModel> basementList = new ArrayList<>();
    private int insertIndex = 0;
    BasementAdapter basementAdapter;
    RecyclerView rv_basement;
    ArrayList<String> specificationModels = new ArrayList<>();
    ArrayList<BasementModel> room_list = new ArrayList<>();
    LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    private int AccommodationId = 0;
    private int addRoomId;
    private String room_type = "";
    LinearLayout ll_basement;
    private String basement_type = "Under Project";
    ArrayList<String> towerPopupList = new ArrayList<>();
    ArrayList<TowerListModel> towerPopupListSelected = new ArrayList<>();
    ArrayAdapter<String> adapterTower;
    Spinner tower_type;
    SendMessage SM;
    Button btn_done_basement;
    boolean open = false;
    // private String ProjectCategoryTypeId = "";
    boolean clickedFromNext = true;
    TabChangeInterface tabChangeInterface;
    boolean accomo = false;
    NestedScrollView scroll_view;
    LinearLayout ll_main;
    private int quantityRoom = 0;
    private String blockCharacterSet = "~#^|$%&/*!'";
    private boolean isShown = true;
    //   private boolean first = true;
    EditText et_cover_area, et_buildup_area, et_super_area, et_carpet_area;
    boolean accomoFirst = true;


    Spinner spinner_carpet_area,spinner_built_up,spinner_super_area,spinner_cover_area;
    Spinner  select_unit,select_unit_room,room_type_spinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.flat_apartment_fragment, null);

        initializeView(view);
        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);
        ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
        onClicks();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            if (isShown) {
                getPropertylengthSizes();
                getAccomoListing();
                getPropertyArea();
                isShown = false;

            } else {
                isShown = false;
            }
        }
    }

    private void initializeView(View view) {

        ButterKnife.bind(this, view);
        tabChangeInterface = (TabChangeInterface) getActivity();
        btn_done_basement = (Button) view.findViewById(R.id.btn_done_basement);
        minus_btn = (ImageView) view.findViewById(R.id.minus_btn);
        plus_btn = (ImageView) view.findViewById(R.id.plus_btn);
        no_of_count = (TextView) view.findViewById(R.id.no_of_count);
        rv_accomo_detail = (RecyclerView) view.findViewById(R.id.rv_accomo_detail);
        rv_floor_names = (RecyclerView) view.findViewById(R.id.rv_floor_names);
        tv_add_more_accomo = (TextView) view.findViewById(R.id.tv_add_more_accomo);
        tv_add_more = (TextView) view.findViewById(R.id.tv_add_more);
        rl_add_more_accomo = (RelativeLayout) view.findViewById(R.id.rl_add_more_accomo);
        rl_add_more_basement = (RelativeLayout) view.findViewById(R.id.rl_add_more_basement);
        plus_btn_basement = (ImageView) view.findViewById(R.id.plus_btn_basement);
        minus_btn_basement = (ImageView) view.findViewById(R.id.minus_btn_basement);
        no_of_count_basement = (TextView) view.findViewById(R.id.no_of_count_basement);
        rv_basement = (RecyclerView) view.findViewById(R.id.rv_basement);
        basementAdapter = new BasementAdapter(getActivity(), basementList);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager1 = new LinearLayoutManager(getActivity());
        rv_basement.setLayoutManager(linearLayoutManager);
        rv_basement.setAdapter(basementAdapter);
        next_basement = (Button) view.findViewById(R.id.next_basement);
        done_tower = (Button) view.findViewById(R.id.done_tower);
        ll_basement = (LinearLayout) view.findViewById(R.id.ll_basement);
        rb_up = (RadioButton) view.findViewById(R.id.rb_up);
        rb_ut = (RadioButton) view.findViewById(R.id.rb_ut);
        rb_nb = (RadioButton) view.findViewById(R.id.rb_nb);

    }

    private void getPropertylengthSizes() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/length",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_UNITS + "/length",
                Constants.GET).execute();

    }

    private void getPropertyArea() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/area",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_UNITS + "/area",
                Constants.GET).execute();

    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    private void onClicks() {
/*
        ll_main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideSoftKeyboard(getActivity());
                return true;
            }
        });
*/

        next_tower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tabChangeInterface.onTabChange(1);

                addTowersDetailApi();

            }
        });

        rb_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rb_up.isChecked()) {
                    ll_basement.setVisibility(View.VISIBLE);
                    rb_nb.setChecked(false);
                    rb_ut.setChecked(false);
                    basement_type = "Under Project";

                } else {

                    ll_basement.setVisibility(View.GONE);

                }

            }
        });

        rb_ut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rb_ut.isChecked()) {

                    rb_nb.setChecked(false);
                    rb_up.setChecked(false);
                    ll_basement.setVisibility(View.GONE);
                    basement_type = "Under Tower";

                } else
                    ll_basement.setVisibility(View.GONE);

            }
        });

        rb_nb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rb_nb.isChecked()) {

                    ll_basement.setVisibility(View.GONE);
                    rb_ut.setChecked(false);
                    rb_up.setChecked(false);
                    basement_type = "No Basement";

                } else
                    ll_basement.setVisibility(View.GONE);

            }
        });

        rl_add_more_basement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onAdding();

            }
        });

        minus_btn_basement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityFloor = Integer.parseInt(no_of_count_basement.getText().toString());

                if (quantityFloor > 0)
                    quantityFloor--;
                no_of_count_basement.setText(String.valueOf(quantityFloor));

            }
        });

        plus_btn_basement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int quantity = Integer.parseInt(no_of_count_basement.getText().toString());

                quantity++;
                no_of_count_basement.setText(String.valueOf(quantity));
            }
        });

        next_basement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Integer.parseInt(no_of_count.getText().toString()) == 0)
                    Toast.makeText(getActivity(), "Please add tower number", Toast.LENGTH_SHORT).show();
                else {

                    addBasementApi();
                    add_tower.setVisibility(View.GONE);
                    tower_list_lay.setVisibility(View.VISIBLE);
                    towerListAdapter = new TowerListAdapter(getActivity(), towerListArrayLIst, towerPopupListSelected, accomodationList, photosModels, Flat_apartment_fragment.this);
                    tower_list.setLayoutManager(new LinearLayoutManager(getActivity()));
                    tower_list.setAdapter(towerListAdapter);
                    getTowerListing();

                }

            }
        });

        btn_done_basement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Integer.parseInt(no_of_count.getText().toString()) == 0)
                    Toast.makeText(getActivity(), "Please add tower number", Toast.LENGTH_SHORT).show();
                else {

                    clickedFromNext = false;

                    addBasementApi();
                   /* add_tower.setVisibility(View.GONE);
                    tower_list_lay.setVisibility(View.VISIBLE);
                    towerListAdapter = new TowerListAdapter(getActivity(), towerListArrayLIst, towerPopupListSelected, accomodationList, Flat_apartment_fragment.this);
                    tower_list.setLayoutManager(new LinearLayoutManager(getActivity()));
                    tower_list.setAdapter(towerListAdapter);
                    getTowerListing();*/

                }

            }
        });

        done_tower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickedFromNext = false;

                addTowersDetailApi();

            }
        });

        rl_add_more_accomo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                roomDetaildialog();

            }
        });

        minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantity = Integer.parseInt(no_of_count.getText().toString());

                if (quantity > 0)
                    quantity--;
                no_of_count.setText(String.valueOf(quantity));

            }
        });

        plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantity = Integer.parseInt(no_of_count.getText().toString());

                quantity++;
                no_of_count.setText(String.valueOf(quantity));
            }
        });

    }

    public void onAdding() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        // ...Irrelevant code for customizing the buttons and title

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.add_other_basement, null);
        dialogBuilder.setView(dialogView);


        final EditText title = (EditText) dialogView.findViewById(R.id.title);
        Button button = (Button) dialogView.findViewById(R.id.save);
        ImageView iv_close = (ImageView) dialogView.findViewById(R.id.iv_close);
        final AlertDialog ad = dialogBuilder.show();
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ad.dismiss();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Commond here......
                if (title.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter the title", Toast.LENGTH_SHORT).show();
                } else {
                    final BasementModel basementModel = new BasementModel();
                    basementModel.setTitle(title.getText().toString());

                    basementList.add(basementModel);
                    basementAdapter.notifyDataSetChanged();
                    ad.dismiss();
                }

            }
        });


    }

    private void roomDetaildialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_add_accomo);
        spinner_accomo_unit = (Spinner) dialog.findViewById(R.id.spinner_accomo_unit);
        TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        ImageView minus_btn_a = (ImageView) dialog.findViewById(R.id.minus_btn_a);
        final TextView no_of_count_a = (TextView) dialog.findViewById(R.id.no_of_count_a);
        ImageView plus_btn_a = (ImageView) dialog.findViewById(R.id.plus_btn_a);
        LinearLayout lnrEmbededVideo = (LinearLayout) dialog.findViewById(R.id.lnrEmbededVideo);
        final LinearLayout ll_other_info = (LinearLayout) dialog.findViewById(R.id.ll_other_info);
        final LinearLayout upload_photos = (LinearLayout) dialog.findViewById(R.id.upload_photos);
        final EditText et_vidoes = (EditText) dialog.findViewById(R.id.et_vidoes);
        final TextView tv_add_embeded = (TextView) dialog.findViewById(R.id.tv_add_embeded);
        image_view_infate = (LinearLayout) dialog.findViewById(R.id.image_view_infate);
        et_title = (EditText) dialog.findViewById(R.id.et_title);
        et_unit = (EditText) dialog.findViewById(R.id.et_unit);
        et_title.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(100)});

        et_cover_area = (EditText) dialog.findViewById(R.id.et_cover_area);
        et_buildup_area = (EditText) dialog.findViewById(R.id.et_buildup_area);
        et_super_area = (EditText) dialog.findViewById(R.id.et_super_area);
        et_carpet_area = (EditText) dialog.findViewById(R.id.et_carpet_area);

        TextView tv_done = (TextView) dialog.findViewById(R.id.tv_done);

        final RecyclerView rv_embeded_video = (RecyclerView) dialog.findViewById(R.id.rv_embeded_video);
        embededVideoUrls.clear();
        embededVideoUrls.add("");
        rv_embeded_video.setLayoutManager(new LinearLayoutManager(getActivity()));
        final EmbededVideoAdapter embededVideoAdapter = new EmbededVideoAdapter(getActivity(), embededVideoUrls);
        rv_embeded_video.setAdapter(embededVideoAdapter);
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_title.getText().toString().equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please add title name", Toast.LENGTH_SHORT).show();
                } else {

                    postAddRoom();
                    dialog.dismiss();

                }

            }
        });

        tv_add_embeded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (embededVideoUrls.contains("")) {

                    Toast.makeText(getActivity(), "Please add embed youtube/vimeo code", Toast.LENGTH_SHORT).show();

                } else {

                    embededVideoUrls.add("");
                    embededVideoAdapter.notifyDataSetChanged();
                }

            }
        });
        upload_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);*/

                checkRunTimePermissionAccomo();

            }
        });

        ImageView minus_btn_b = (ImageView) dialog.findViewById(R.id.minus_btn_b);
        final TextView no_of_count_b = (TextView) dialog.findViewById(R.id.no_of_count_b);
        ImageView plus_btn_b = (ImageView) dialog.findViewById(R.id.plus_btn_b);
        final CheckBox cb_other_info = (CheckBox) dialog.findViewById(R.id.cb_other_info);

        cb_other_info.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    ll_other_info.setVisibility(View.VISIBLE);

                } else {

                    ll_other_info.setVisibility(View.GONE);

                }
            }
        });

        spinner_accomo_unit.setAdapter(adapter1);

        spinner_accomo_unit.setSelection(adapter1.getCount());

        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);

         spinner_cover_area = (Spinner) dialog.findViewById(R.id.spinner_cover_area);

        spinner_cover_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

              /*  if (spinner_cover_area.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_cover = spinner_cover_area.getSelectedItem().toString();
                    spinner_select_cover = propertyUnitsModels.get(position).getId();

                }*/

                spinner_cover = spinner_cover_area.getSelectedItem().toString();
                spinner_select_cover = propertyUnitsModels.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

         spinner_super_area = (Spinner) dialog.findViewById(R.id.spinner_super_area);

        spinner_super_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_super_area.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_super = spinner_super_area.getSelectedItem().toString();
                    spinner_select_super = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_built_up = (Spinner) dialog.findViewById(R.id.spinner_built_up);


        spinner_built_up.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_built_up.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_built = spinner_built_up.getSelectedItem().toString();
                    spinner_select_built = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spinner_carpet_area = (Spinner) dialog.findViewById(R.id.spinner_carpet_area);

        spinner_cover_area.setAdapter(adapter);
        spinner_cover_area.setSelection(adapter.getCount());
        spinner_super_area.setAdapter(adapter);
        spinner_super_area.setSelection(adapter.getCount());
        spinner_built_up.setAdapter(adapter);
        spinner_built_up.setSelection(adapter.getCount());
        spinner_carpet_area.setAdapter(adapter);
        spinner_carpet_area.setSelection(adapter.getCount());

        spinner_carpet_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_carpet_area.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_carpet = spinner_carpet_area.getSelectedItem().toString();
                    spinner_select_carpet = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        minus_btn_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityRoom = Integer.parseInt(no_of_count_a.getText().toString());

                if (quantityRoom > 0)
                    quantityRoom--;
                no_of_count_a.setText(String.valueOf(quantityRoom));

            }
        });

        plus_btn_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityRoom = Integer.parseInt(no_of_count_b.getText().toString());

                quantityRoom++;
                no_of_count_b.setText(String.valueOf(quantityRoom));
            }
        });

        minus_btn_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityFloor = Integer.parseInt(no_of_count_b.getText().toString());

                if (quantityFloor > 0)
                    quantityFloor--;
                no_of_count_b.setText(String.valueOf(quantityFloor));

            }
        });

        plus_btn_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityRoom = Integer.parseInt(no_of_count_a.getText().toString());

                quantityRoom++;
                no_of_count_a.setText(String.valueOf(quantityRoom));
            }
        });


        //room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    public void addRoomDetaildialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.room_detail_popup);
        TextView room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        room_type_spinner = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
         select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
         select_unit_room = (Spinner) dialog.findViewById(R.id.select_unit_room);

        RecyclerView room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        LinearLayout attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        Button save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));

        String[] countries = getResources().getStringArray(R.array.RoomTypes);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, countries);

        room_type_spinner.setAdapter(adapter);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type_spinner.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add

                if (room_name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter room name", Toast.LENGTH_SHORT).show();
                } else if (length.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter length", Toast.LENGTH_SHORT).show();
                } else if (width.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter width", Toast.LENGTH_SHORT).show();
                } else {
                    type = "room";
                    addRoomApi();
                }


            }
        });

        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        select_unit_room.setAdapter(adapter1);
        select_unit_room.setSelection(adapter1.getCount());
        dialog.show();

    }

    private void EditRoomDetaildialog(AccomodationModel.AccommodationRooms accommodationRooms) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.room_detail_popup);
        TextView room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        final Spinner room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        final Spinner select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        final Spinner select_unit_room = (Spinner) dialog.findViewById(R.id.select_unit_room);

        RecyclerView room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        LinearLayout attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        Button save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));

        room_name.setText(accommodationRooms.getBedRoomName());
        length.setText(Utils.getFilteredValue(accommodationRooms.getLength()));
        width.setText(Utils.getFilteredValue(accommodationRooms.getWidth()));
        room_size.setText(Utils.getFilteredValue(accommodationRooms.getFloorSize()));

        String[] countries = getResources().getStringArray(R.array.RoomTypes);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, countries);

        room_type.setAdapter(adapter);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add


                if (room_name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter room name", Toast.LENGTH_SHORT).show();
                } else if (length.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter length", Toast.LENGTH_SHORT).show();
                } else if (width.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter width", Toast.LENGTH_SHORT).show();
                } else {
                    type = "room";
                    EditRoomApi();
                }


            }
        });

        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        select_unit_room.setAdapter(adapter1);
        select_unit_room.setSelection(adapter1.getCount());
        dialog.show();

    }

    private void addBalconyDetaildialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.balcony_detail_popup);
        TextView room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        final Spinner room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        final Spinner select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        RecyclerView room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        LinearLayout attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        Button save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);
        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final Spinner select_unit_room = (Spinner) dialog.findViewById(R.id.select_unit_balcony);

        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        String[] countries = getResources().getStringArray(R.array.RoomTypes);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, countries);

        room_type.setAdapter(adapter);

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//
//                    select_unit_txt = select_unit.getSelectedItem().toString();
//                    select_unit_id = propertyUnitsModels.get(position).getId();
////
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.

                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

//        roomAttachedDataAdapter = new RoomAttachedDataAdapter(getActivity(), room_list);
//
//        room_data.setAdapter(roomAttachedDataAdapter);

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        room_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                same = false;
//
//            }
//        });

//        room_name.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                roomName = room_name.getText().toString().trim();
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add


                if (room_name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter room name", Toast.LENGTH_SHORT).show();
                } else if (length.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter length", Toast.LENGTH_SHORT).show();
                } else if (width.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter width", Toast.LENGTH_SHORT).show();
                } else {
                    type = "balcony";
                    addRoomApi();
                }


            }
        });

        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        select_unit_room.setAdapter(adapter1);
        select_unit_room.setSelection(adapter1.getCount());
        dialog.show();
//
    }

    private void EditBalconyDetaildialog(AccomodationModel.AccommodationRooms accommodationRooms) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.balcony_detail_popup);
        TextView room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        final Spinner room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        final Spinner select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        RecyclerView room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        LinearLayout attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        Button save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);
        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        room_name.setText(accommodationRooms.getBedRoomName());
        length.setText(Utils.getFilteredValue(accommodationRooms.getLength()));
        width.setText(Utils.getFilteredValue(accommodationRooms.getWidth()));
        room_size.setText(Utils.getFilteredValue(accommodationRooms.getFloorSize()));

        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        String[] countries = getResources().getStringArray(R.array.RoomTypes);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, countries);

        room_type.setAdapter(adapter);

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//
//                    select_unit_txt = select_unit.getSelectedItem().toString();
//                    select_unit_id = propertyUnitsModels.get(position).getId();
////
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.

                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

//        roomAttachedDataAdapter = new RoomAttachedDataAdapter(getActivity(), room_list);
//
//        room_data.setAdapter(roomAttachedDataAdapter);

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        room_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                same = false;
//
//            }
//        });

//        room_name.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                roomName = room_name.getText().toString().trim();
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add


                if (room_name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter room name", Toast.LENGTH_SHORT).show();
                } else if (length.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter length", Toast.LENGTH_SHORT).show();
                } else if (width.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter width", Toast.LENGTH_SHORT).show();
                } else {
                    type = "balcony";
                    EditRoomApi();
                }


            }
        });

        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
      /*room_type.setAdapter(adapter);
        room_type.setSelection(adapter.getCount());*/
        dialog.show();

    }

    private void postAddRoom() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ADD_BEDROOM_PROJECT,
                getAddinput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_ADD_BEDROOM_PROJECT,
                Constants.POST).execute();

    }

    private void addRoomApi() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ADD_ACCOMODATION_BEDROOM,
                getAddRoominput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_ADD_ACCOMODATION_BEDROOM,
                Constants.POST).execute();

    }

    private void EditRoomApi() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ADD_ACCOMODATION_BEDROOM,
                getEditRoominput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_ADD_ACCOMODATION_BEDROOM,
                Constants.POST).execute();

    }

    private void addBasementApi() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ADD_BASEMENT,
                getAddBasementinput(),
                "Loading..",
                this,
                Urls.URL_ADD_BASEMENT,
                Constants.POST).execute();

    }

    private void addTowersDetailApi() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ADD_TOWER_INFORMATION,
                getAddTowersApi(),
                "Loading..",
                this,
                Urls.URL_ADD_TOWER_INFORMATION,
                Constants.POST).execute();

    }

    private String getAddinput() {
        floorModelList.clear();
        for (int i = 0; i < quantityFloor; i++) {


            if (i == 0) {
                FloorModel floorModel = new FloorModel();
                floorModel.setId(0);
                floorModel.setFloorNumber("Ground Floor");
                floorModelList.add(floorModel);
            } else {
                FloorModel floorModel = new FloorModel();
                floorModel.setId(0);
                floorModel.setFloorNumber(i + " " + "Floor");
                floorModelList.add(floorModel);
            }

        }

        int mineId = 0;

        for (int i = 0; i < Project_name.proCatTypesArrayList.size(); i++) {

            if (Project_name.proCatTypesArrayList.get(i).getCategoryType().equalsIgnoreCase("Flat/Apartment")) {

                mineId = Project_name.proCatTypesArrayList.get(i).getId();
            }

        }

        String input = "";

        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {

            for(int i=0;i<propertyUnitsModels.size();i++){

                if(propertyUnitsModels.get(i).getSizeUnitId().equalsIgnoreCase(spinner_carpet_area.getSelectedItem().toString())){

                    jsonObject1.put("CarpetAreaSizeUnitId", propertyUnitsModels.get(i).getId());
                }
                if(propertyUnitsModels.get(i).getSizeUnitId().equalsIgnoreCase(spinner_cover_area.getSelectedItem().toString())){

                    jsonObject1.put("CoverAreaSizeUnitId", propertyUnitsModels.get(i).getId());
                }
                if(propertyUnitsModels.get(i).getSizeUnitId().equalsIgnoreCase(spinner_super_area.getSelectedItem().toString())){

                    jsonObject1.put("SuperAreaSizeUnitId", propertyUnitsModels.get(i).getId());
                }
                if(propertyUnitsModels.get(i).getSizeUnitId().equalsIgnoreCase(spinner_built_up.getSelectedItem().toString())){

                    jsonObject1.put("BuiltUpAreaSizeUnitId", propertyUnitsModels.get(i).getId());
                }
//                else{
//                    jsonObject1.put("CarpetAreaSizeUnitId", 1);
//                }
            }

            jsonObject1.put("Id", 0);
            jsonObject1.put("ProjectCategoryTypeId", mineId);
            jsonObject1.put("Title", et_title.getText().toString());
            if (!et_unit.getText().toString().equalsIgnoreCase(""))
                jsonObject1.put("TotalArea", et_unit.getText().toString());

            jsonObject1.put("Bedrooms", quantityRoom);
            jsonObject1.put("Floors", quantityFloor);
            jsonObject1.put("CoverAreaSize", et_cover_area.getText().toString());
//            jsonObject1.put("CoverAreaSizeUnitId", 1);
            jsonObject1.put("SuperAreaSize", et_super_area.getText().toString());
//            jsonObject1.put("SuperAreaSizeUnitId", 1);
            jsonObject1.put("BuiltUpAreaSize", et_buildup_area.getText().toString());
//            jsonObject1.put("BuiltUpAreaSizeUnitId", 1);
            jsonObject1.put("CarpetAreaSize", et_carpet_area.getText().toString());

            jsonObject1.put("Parkings", 0);

          /*  JSONObject jsonObject2 = new JSONObject();
            for (int i = 0; i < checked_room.size(); i++) {
                jsonObject2.put("AttachPropertyBedroomId", checked_room.get(i));
                jsonArray1.put(jsonObject2);
                Log.e("checked_room", "checked_room" + checked_room.get(i));
            }*/

            JSONArray jsonArrayFloor = new JSONArray();

            for (int i = 0; i < floorModelList.size(); i++) {
                JSONObject jsonObjectFloorList = new JSONObject();
                jsonObjectFloorList.put("Id", floorModelList.get(i).getId());
                jsonObjectFloorList.put("FloorNumber", floorModelList.get(i).getFloorNumber());
                jsonArrayFloor.put(jsonObjectFloorList);
                Log.e("FloorNumber", "FloorNumber" + floorModelList.get(i).getFloorNumber());
            }

//          jsonObject1.put("AccomodationFiles", jsonArray1);
            jsonObject1.put("AccommodationFloors", jsonArrayFloor);

            input = jsonObject1.toString();

        } catch (Exception e) {

            e.printStackTrace();
        }


        return input;

    }

    private String getAddRoominput() {

        String input = "";

        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {

            jsonObject1.put("Id", 0);
            jsonObject1.put("AccommodationId", AccommodationId);
            jsonObject1.put("BedRoomName", room_name.getText().toString());
            jsonObject1.put("BedRoomType", room_type);
            jsonObject1.put("BedRoomSize", room_size.getText().toString());
            jsonObject1.put("BedRoomSizeUnitId", 1);
            jsonObject1.put("Length", length.getText().toString());
            jsonObject1.put("Width", width.getText().toString());
            jsonObject1.put("LengthWidthUnitId", 1);
            jsonObject1.put("Category", type);
            jsonObject1.put("PropertyFloorId", 1);
            jsonObject1.put("AccommodationFloorId", addRoomId);

            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }

    private String getEditRoominput() {

        String input = "";

        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {

            jsonObject1.put("Id", EditRoomOrBalconyId);
            jsonObject1.put("AccommodationId", AccommodationId);
            jsonObject1.put("BedRoomName", room_name.getText().toString());
            jsonObject1.put("BedRoomType", room_type);
            jsonObject1.put("BedRoomSize", room_size.getText().toString());
            jsonObject1.put("BedRoomSizeUnitId", 1);
            jsonObject1.put("Length", length.getText().toString());
            jsonObject1.put("Width", width.getText().toString());
            jsonObject1.put("LengthWidthUnitId", 1);
            jsonObject1.put("Category", type);
            jsonObject1.put("PropertyFloorId", 1);
            jsonObject1.put("AccommodationFloorId", addRoomId);

            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }

    private String getAddBasementinput() {

        String input = "";

        towerList.clear();
        for (int i = 1; i <= Integer.valueOf(no_of_count.getText().toString()); i++) {

            TowerModel towerModel = new TowerModel();
            towerModel.setName("Tower" + " " + i);
            towerList.add(towerModel);

        }
        int id = 0;
        for (int i = 0; i < Project_name.proCatTypesArrayList.size(); i++) {

            if (Project_name.proCatTypesArrayList.get(i).getCategoryType().equalsIgnoreCase("Flat/Apartment")) {

                id = Project_name.proCatTypesArrayList.get(i).getId();
            }

        }
        JSONObject jsonObject1 = new JSONObject();
        try {

            jsonObject1.put("Id", id);

            jsonObject1.put("BasementType", basement_type);

            jsonObject1.put("TotalBasement", Integer.valueOf(no_of_count_basement.getText().toString()));

            JSONArray jsonTowerFloor = new JSONArray();

            for (int i = 0; i < towerList.size(); i++) {
                JSONObject jsonObjectTowerList = new JSONObject();
                jsonObjectTowerList.put("Name", towerList.get(i).getName());
                //jsonObjectFloorList.put("FloorNumber", floorModelList.get(i).getFloorNumber());
                jsonTowerFloor.put(jsonObjectTowerList);
            }

            jsonObject1.put("Towers", jsonTowerFloor);

            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }

    private String getAddTowersApi() {

        String input = "";

        towerList.clear();

        int mineId = 0;

        for (int i = 0; i < Project_name.proCatTypesArrayList.size(); i++) {

            if (Project_name.proCatTypesArrayList.get(i).getCategoryType().equalsIgnoreCase("Flat/Apartment")) {

                mineId = Project_name.proCatTypesArrayList.get(i).getId();
            }

        }
        for (int i = 1; i <= Integer.valueOf(no_of_count.getText().toString()); i++) {

            TowerModel towerModel = new TowerModel();
            towerModel.setName("Tower" + " " + i);
            towerList.add(towerModel);

        }

        JSONObject jsonObject1 = new JSONObject();
        try {

            jsonObject1.put("Id", "1");
            jsonObject1.put("ProjectCategoryTypeId", mineId);

            jsonObject1.put("TotalBasement", Integer.valueOf(no_of_count_basement.getText().toString()));

            JSONArray jsonTowerFloor = new JSONArray();

            for (int i = 0; i < towerList.size(); i++) {
                JSONObject jsonObjectTowerList = new JSONObject();
                jsonObjectTowerList.put("Name", towerList.get(i).getName());
                //jsonObjectFloorList.put("FloorNumber", floorModelList.get(i).getFloorNumber());
                jsonTowerFloor.put(jsonObjectTowerList);
            }

            jsonObject1.put("Towers", jsonTowerFloor);

            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }

    private void setAdapter() {

        accomoAdapter = new AccomoAdapter(getActivity(), accomodationList);
        rv_accomo_detail.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_accomo_detail.setAdapter(accomoAdapter);
        accomoAdapter.notifyDataSetChanged();
    }

    private void getAccomoListing() {

        int mineId = 0;

        for (int i = 0; i < Project_name.proCatTypesArrayList.size(); i++) {

            if (Project_name.proCatTypesArrayList.get(i).getCategoryType().equalsIgnoreCase("Flat/Apartment")) {

                mineId = Project_name.proCatTypesArrayList.get(i).getId();
            }

        }


        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_LISTING + mineId,
                "",
                "Loading..",
                this,
                Urls.URL_PROJECT_LISTING,
                Constants.GET).execute();

    }

    private void getTowerListing() {
        int id = 0;
        for (int i = 0; i < Project_name.proCatTypesArrayList.size(); i++) {

            if (Project_name.proCatTypesArrayList.get(i).getCategoryType().equalsIgnoreCase("Flat/Apartment")) {

                id = Project_name.proCatTypesArrayList.get(i).getId();
            }

        }
        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_TOWER_LISTING + String.valueOf(id)
                ,
                "",
                "Loading..",
                this,
                Urls.URL_TOWER_LISTING,
                Constants.GET).execute();

    }

    private void getAccomoRoomListing() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ACCOMODATION_ROOM_LISTING + "/1/0/room",
                "",
                "Loading..",
                this,
                Urls.URL_ACCOMODATION_ROOM_LISTING,
                Constants.GET).execute();

    }

    public void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {
            // if already permition granted
            selectImage();

        }
    }

    public void checkRunTimePermissionAccomo() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11);
        } else {
            // if already permition granted
            selectImageAccomo();

        }
    }


    public void addTowersPopup(int pos) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_tower);

        TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        TextView tv_done = (TextView) dialog.findViewById(R.id.tv_done);

        MultiSelectionSpinner multiselectSpinner = (MultiSelectionSpinner) dialog.findViewById(R.id.multiselectSpinner);
        tower_type = (Spinner) dialog.findViewById(R.id.tower_type);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        RecyclerView rv_towerList = (RecyclerView) dialog.findViewById(R.id.rv_towerList);
        final RecyclerView rv_towerListSelection = (RecyclerView) dialog.findViewById(R.id.rv_towerListSelection);
//        towerPopupListSelected.clear();

        // updated
        TextView tv_towerselected_name = (TextView) dialog.findViewById(R.id.tv_towerselected_name);
        tv_towerselected_name.setText(towerList.get(pos).getName());
        LinearLayout ll_opendropdpwn = (LinearLayout) dialog.findViewById(R.id.ll_opendropdpwn);
        ll_opendropdpwn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (open) {

                    rv_towerListSelection.setVisibility(View.VISIBLE);
                    open = false;
                } else {

                    rv_towerListSelection.setVisibility(View.GONE);
                    open = true;
                }


            }
        });

        TowerPopUpAdapterSelection towerPopUpAdapterSelection = new TowerPopUpAdapterSelection(getActivity(), towerListArrayLIst, new TowerPopUpAdapterSelection.Callback() {
            @Override
            public void onItemClicked(int position, String selection, ArrayList<TowerListModel> towerListModels) {

                if (selection.equalsIgnoreCase("checked")) {

                    if (towerPopupListSelected.contains(towerListArrayLIst.get(position))) {
                        towerPopupListSelected.set(position, towerListArrayLIst.get(position));
                    } else {
                        towerPopupListSelected.add(towerListArrayLIst.get(position));
                    }


//                       towerListAdapter = new TowerListAdapter(getActivity(), towerListArrayLIst, towerPopupListSelected,accomodationList);
//                      tower_list.setAdapter(towerListAdapter);
//                    towerListAdapter.notifyDataSetChanged();

                } else if (selection.equalsIgnoreCase("unchecked")) {

                    towerPopupListSelected.remove(towerListArrayLIst.get(position));
//                      towerListAdapter = new TowerListAdapter(getActivity(), towerListArrayLIst, towerPopupListSelected,accomodationList);
//                     tower_list.setAdapter(towerListAdapter);
//                    towerListAdapter.notifyDataSetChanged();

                }

            }
        });
        towerPopUpAdapter = new TowerPopUpAdapter(getActivity(), towerPopupList);
        linearLayoutManager1 = new LinearLayoutManager(getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rv_towerList.setLayoutManager(linearLayoutManager1);
        rv_towerList.setAdapter(towerPopUpAdapter);
        rv_towerListSelection.setLayoutManager(linearLayoutManager);
        rv_towerListSelection.setAdapter(towerPopUpAdapterSelection);

        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                towerListAdapter.notifyDataSetChanged();
                dialog.dismiss();

            }
        });
//      multiselectSpinner.setItems(towerPopupList);


        adapterTower = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, towerPopupList);
        tower_type.setAdapter(adapterTower);

        //String[] countries = getResources().getStringArray(R.array.TowerTypes);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        dialog.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean openActivityOnce = true;
        boolean openDialogOnce = true;
        if (requestCode == 11111) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView();
                        }
                    }
                }
            }

            selectImage();

        } else if (requestCode == 11) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView();
                        }
                    }
                }
            }

            selectImageAccomo();

        }

    }

    private void alertView() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.AppTheme);

        dialog.setTitle("Permission Denied")
                .setInverseBackgroundForced(true)
                //.setIcon(R.drawable.ic_info_black_24dp)
                .setMessage("Without those permission the app is unable to save your profile. App needs to save profile image in your external storage and also need to get profile image from camera or external storage.Are you sure you want to deny this permission?")

                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }
                })
                .setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        checkRunTimePermission();

                    }
                }).show();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.bnhabitat.utils.Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectImageAccomo() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.bnhabitat.utils.Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntentAccomo();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntentAccomo();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void cameraIntentAccomo() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA_ACCOMO);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void galleryIntentAccomo() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE_ACCOOMO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == SELECT_FILE_ACCOOMO)
                onSelectFromGalleryResultAccomo(data);

            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);

            else if (requestCode == REQUEST_CAMERA_ACCOMO)
                onCaptureImageResultAccomo(data);

        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodedImageData = getEncoded64ImageStringFromBitmap(bm);
                postPropertyImages();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ivImage.setImageBitmap(bm);
    }

    private void onSelectFromGalleryResultAccomo(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodedImageData = getEncoded64ImageStringFromBitmap(bm);
                postPropertyImagesAccomo();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ivImage.setImageBitmap(bm);
    }

    private void postPropertyImages() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_PHOTOS_PROJECT,
                photoInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_PHOTOS_PROJECT,
                Constants.POST).execute();

    }

    private void postPropertyImagesAccomo() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_PHOTOS_PROJECT_ACCOMO,
                photoInputAccomo(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_PHOTOS_PROJECT_ACCOMO,
                Constants.POST).execute();

    }

    private String photoInput() {
        String inputStr = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("CompanyId", "1");
            jsonObject1.put("Name", "mine");
            jsonObject1.put("Type", "jpg");
            jsonObject1.put("ImageCode", encodedImageData);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputStr = jsonArray.toString();
        return inputStr;
    }

    private String photoInputAccomo() {
        String inputStr = "";
        accomo = true;

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("CompanyId", "1");
            jsonObject1.put("Name", "mine");
            jsonObject1.put("Type", "jpg");
            jsonObject1.put("ImageCode", encodedImageData);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputStr = jsonArray.toString();
        return inputStr;
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
    //    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");

        File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encodedImageData = getEncoded64ImageStringFromBitmap(thumbnail);
        postPropertyImages();
//      ivImage.setImageBitmap(thumbnail);
    }

    private void onCaptureImageResultAccomo(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
       // File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encodedImageData = getEncoded64ImageStringFromBitmap(thumbnail);
        postPropertyImagesAccomo();
//      ivImage.setImageBitmap(thumbnail);
    }

    @Override
    public void onResultListener(String result, String which) {

        if (which.equalsIgnoreCase(Urls.URL_PROPERTY_UNITS + "/length")) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray propertySize = jsonObject.getJSONArray("Result");

                    if (propertySize.length() == 0) {
//                  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < propertySize.length(); index++) {
                            JSONObject jsonObject1 = propertySize.getJSONObject(index);

                            PropertyUnitsModel propertyUnitsModel = new PropertyUnitsModel();

                            propertyUnitsModel.setId(jsonObject1.getString("Id"));
                            propertyUnitsModel.setSizeUnitId(jsonObject1.getString("SizeUnit"));
                            propertyUnitsModels.add(propertyUnitsModel);

                        }
                        property_units_id.clear();

                        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
                        for (int i = 0; i < propertyUnitsModels.size(); i++) {
                            property_units_id.add(propertyUnitsModels.get(i).getSizeUnitId());
                        }

                        adapter1.addAll(property_units_id);
                        adapter1.add(getString(R.string.select_type));

                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_DELETE_ROOM_OR_BALCONY_ACCOMODATION)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        //Utils.showSuccessMessage("Success", message, "Ok", getActivity());
//                        AccommodationId=0;
//                        rv_floor_names.setVisibility(View.GONE);
                        getAccomoListing();

                    }


                }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_DELETE_TOWERS)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        //Utils.showSuccessMessage("Success", message, "Ok", getActivity());
                        // AccommodationId = 0;
                        //   rv_floor_names.setVisibility(View.GONE);
                        getTowerListing();

                    }


                }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_DELETE_ACCOMODATION)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        //Utils.showSuccessMessage("Success", message, "Ok", getActivity());
                        AccommodationId = 0;
                        rv_floor_names.setVisibility(View.GONE);
                        getAccomoListing();

                    }


                }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_TOWER_LISTING)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray towerArray = jsonObject.getJSONArray("Result");

                    if (towerArray.length() == 0) {

                        towerListArrayLIst.clear();

                    } else {

                        towerListArrayLIst.clear();
                        towerPopupList.clear();

                        for (int index = 0; index < towerArray.length(); index++) {
                            JSONObject jsonObject1 = towerArray.getJSONObject(index);

                            TowerListModel towerListModel = new TowerListModel();

                            towerListModel.setId(jsonObject1.getInt("Id"));
                            towerListModel.setProjectCategoryTypeId(jsonObject1.getInt("ProjectCategoryTypeId"));
                            towerListModel.setName(jsonObject1.getString("Name"));
                            towerPopupList.add(jsonObject1.getString("Name"));

                            // ArrayList<AccomodationModel.AccomodationFilesModel> accomodationFilesModels = new ArrayList<>();
/*
                            if (jsonObject1.getJSONArray("AccomodationFiles") != null) {
                                JSONArray accomodationFiles = jsonObject1.getJSONArray("AccomodationFiles");

                                for (int i = 0; i < accomodationFiles.length(); i++) {
                                    JSONObject object = accomodationFiles.getJSONObject(i);
                                    AccomodationModel.AccomodationFilesModel accomodationFilesModel = new AccomodationModel.AccomodationFilesModel();

                                    accomodationFilesModel.setId(object.getInt("Id"));
                                    accomodationFilesModel.setAccomodationId(object.getString("AccomodationId"));
                                    accomodationFilesModel.setFileId(object.getString("FileId"));
                                    accomodationFilesModel.setCategory(object.getString("Category"));

                                    JSONObject jsonObject2 = accomodationFiles.getJSONObject(i).getJSONObject("File");
                                    AccomodationModel.FileModel fileModel = new AccomodationModel.FileModel();
                                    fileModel.setId(jsonObject2.getInt("Id"));
                                    fileModel.setName(jsonObject2.getString("Name"));
                                    fileModel.setUrl(jsonObject2.getString("Url"));
                                    fileModel.setType(jsonObject2.getString("Type"));
                                    accomodationFilesModel.setFileModel(fileModel);

                                    accomodationFilesModels.add(accomodationFilesModel);

                                }
                                accomodationModel.setAccomodationFilesModels(accomodationFilesModels);
                            }
                    */


                            //        setAdapter();
                            towerListArrayLIst.add(towerListModel);

                        }

                        towerListAdapter = new TowerListAdapter(getActivity(), towerListArrayLIst, towerPopupListSelected, accomodationList, photosModels, Flat_apartment_fragment.this);
                        tower_list.setLayoutManager(new LinearLayoutManager(getActivity()));
                        tower_list.setAdapter(towerListAdapter);
                        towerListAdapter.notifyDataSetChanged();
                        towerPopUpAdapter.notifyDataSetChanged();

                    }

                    setAdapter();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROJECT_LISTING)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray accomoArray = jsonObject.getJSONArray("Result");

                    if (accomoArray.length() == 0) {

                        accomodationList.clear();

                    } else {

                        accomodationList.clear();
                        for (int index = 0; index < accomoArray.length(); index++) {
                            JSONObject jsonObject1 = accomoArray.getJSONObject(index);

                            AccomodationModel accomodationModel = new AccomodationModel();

                            accomodationModel.setId(jsonObject1.getInt("Id"));
                            accomodationModel.setProjectCategoryTypeId(jsonObject1.getInt("ProjectCategoryTypeId"));
                            accomodationModel.setTitle(jsonObject1.getString("Title"));
                            if (!jsonObject1.getString("TotalArea").equalsIgnoreCase("null")) {
                                accomodationModel.setTotalArea(jsonObject1.getInt("TotalArea"));

                            }

//                          accomodationModel.setTotalAreaSizeUnitId(jsonObject1.getInt("TotalAreaSizeUnitId"));
                            accomodationModel.setBedrooms(jsonObject1.getInt("Bedrooms"));
                            accomodationModel.setFloors(jsonObject1.getInt("Floors"));

                            if (!jsonObject1.get("CoverAreaSize").toString().equalsIgnoreCase("null"))

                                accomodationModel.setCoverAreaSize(jsonObject1.getDouble("CoverAreaSize"));
                            accomodationModel.setCoverAreaSizeUnitId(jsonObject1.getInt("CoverAreaSizeUnitId"));

                            if (!jsonObject1.get("SuperAreaSize").toString().equalsIgnoreCase("null"))

                                accomodationModel.setSuperAreaSize(jsonObject1.getInt("SuperAreaSize"));
                            accomodationModel.setSuperAreaSizeUnitId(jsonObject1.getInt("SuperAreaSizeUnitId"));
                            accomodationModel.setSuperAreaSizeUnitId(jsonObject1.getInt("SuperAreaSizeUnitId"));

                            if (!jsonObject1.get("BuiltUpAreaSize").toString().equalsIgnoreCase("null"))
                                accomodationModel.setBuiltUpAreaSize(jsonObject1.getInt("BuiltUpAreaSize"));
                            accomodationModel.setBuiltUpAreaSizeUnitId(jsonObject1.getInt("BuiltUpAreaSizeUnitId"));

                            if (!jsonObject1.get("CarpetAreaSize").toString().equalsIgnoreCase("null"))

                                accomodationModel.setCarpetAreaSize(jsonObject1.getDouble("CarpetAreaSize"));
                            accomodationModel.setCarpetAreaSizeUnitId(jsonObject1.getInt("CarpetAreaSizeUnitId"));
//                            accomodationModel.setParkings(Utils.getFilteredValueInt(jsonObject1.getInt("Parkings")));
                            ArrayList<AccomodationModel.AccomodationFilesModel> accomodationFilesModels = new ArrayList<>();
                            if (jsonObject1.getJSONArray("AccomodationFiles") != null) {
                                JSONArray accomodationFiles = jsonObject1.getJSONArray("AccomodationFiles");

                                for (int i = 0; i < accomodationFiles.length(); i++) {
                                    JSONObject object = accomodationFiles.getJSONObject(i);
                                    AccomodationModel.AccomodationFilesModel accomodationFilesModel = new AccomodationModel.AccomodationFilesModel();

                                    accomodationFilesModel.setId(object.getInt("Id"));
                                    accomodationFilesModel.setAccomodationId(object.getString("AccomodationId"));
                                    accomodationFilesModel.setFileId(object.getString("FileId"));
                                    accomodationFilesModel.setCategory(object.getString("Category"));

                                    JSONObject jsonObject2 = accomodationFiles.getJSONObject(i).getJSONObject("File");
                                    AccomodationModel.FileModel fileModel = new AccomodationModel.FileModel();
                                    fileModel.setId(jsonObject2.getInt("Id"));
                                    fileModel.setName(jsonObject2.getString("Name"));
                                    fileModel.setUrl(jsonObject2.getString("Url"));
                                    fileModel.setType(jsonObject2.getString("Type"));
                                    accomodationFilesModel.setFileModel(fileModel);

                                    accomodationFilesModels.add(accomodationFilesModel);

                                }
                                accomodationModel.setAccomodationFilesModels(accomodationFilesModels);
                            }
                            ArrayList<AccomodationModel.AccommodationFloors> AccommodationFloorModels = new ArrayList<>();
                            if (jsonObject1.getJSONArray("AccommodationFloors") != null) {
                                JSONArray AccommodationRooms = jsonObject1.getJSONArray("AccommodationFloors");

                                for (int i = 0; i < AccommodationRooms.length(); i++) {
                                    JSONObject object = AccommodationRooms.getJSONObject(i);
                                    AccomodationModel.AccommodationFloors accomodationFloorModel = new AccomodationModel.AccommodationFloors();

                                    accomodationFloorModel.setId(object.getInt("Id"));
                                    accomodationFloorModel.setFloorNumber(object.getString("FloorNumber"));
                                    accomodationFloorModel.setAccommodationId(object.getInt("AccommodationId"));


                                    AccommodationFloorModels.add(accomodationFloorModel);

                                }

                            }
                            ArrayList<AccomodationModel.AccommodationRooms> AccommodationRoomsModels = new ArrayList<>();
                            ArrayList<AccomodationModel.AccommodationRooms> AccommodationBalconyModels = new ArrayList<>();
                            if (jsonObject1.getJSONArray("AccommodationRooms") != null) {
                                JSONArray AccommodationRooms = jsonObject1.getJSONArray("AccommodationRooms");

                                for (int i = 0; i < AccommodationRooms.length(); i++) {
                                    JSONObject object = AccommodationRooms.getJSONObject(i);
                                    AccomodationModel.AccommodationRooms accomodationFloorModel = new AccomodationModel.AccommodationRooms();

                                    accomodationFloorModel.setId(object.getInt("Id"));
                                    if (object.has("AccomodationId"))
                                        accomodationFloorModel.setFloorNumber(Utils.getFilteredValue(object.getString("AccomodationId")));
                                    accomodationFloorModel.setBedRoomName(object.getString("BedRoomName"));
                                    accomodationFloorModel.setCategory(object.getString("Category"));
                                    accomodationFloorModel.setLength(object.getString("Length"));
                                    accomodationFloorModel.setWidth(object.getString("Width"));
                                    accomodationFloorModel.setFloorSize(object.getString("BedRoomSize"));
                                    accomodationFloorModel.setAccommodationFloorId(object.getInt("AccommodationFloorId"));

                                    if (object.getString("Category").equalsIgnoreCase("balcony")) {
                                        AccommodationBalconyModels.add(accomodationFloorModel);
                                    } else {
                                        AccommodationRoomsModels.add(accomodationFloorModel);
                                    }


                                }

                            }
                            accomodationModel.setAccomodationFilesModels(accomodationFilesModels);
                            accomodationModel.setAccommodationFloorsModels(AccommodationFloorModels);
                            accomodationModel.setAccommodationRoomsModels(AccommodationRoomsModels);
                            accomodationModel.setAccommodationBalconyModels(AccommodationBalconyModels);

                            accomodationList.add(accomodationModel);

                        }


                    }

                    setAdapter();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_ACCOMODATION_ROOM_LISTING)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray accomoArray = jsonObject.getJSONArray("Result");
                    ArrayList<AccomodationModel.AccommodationRooms> AccommodationRoomsModels = new ArrayList<>();

                    if (accomoArray.length() == 0) {

//                        accomodationList.clear();

                    } else {

//                        accomodationList.clear();
                        for (int index = 0; index < accomoArray.length(); index++) {
                            JSONObject jsonObject1 = accomoArray.getJSONObject(index);


//                            if (jsonObject1.getJSONArray("AccommodationRooms") != null) {
//                                JSONArray AccommodationRooms = jsonObject1.getJSONArray("AccommodationRooms");

                            for (int i = 0; i < accomoArray.length(); i++) {
                                JSONObject object = accomoArray.getJSONObject(i);
                                AccomodationModel.AccommodationRooms accomodationFloorModel = new AccomodationModel.AccommodationRooms();

                                accomodationFloorModel.setId(object.getInt("Id"));
                                accomodationFloorModel.setFloorNumber(object.getString("AccomodationId"));
                                accomodationFloorModel.setBedRoomName(object.getString("BedRoomName"));
                                accomodationFloorModel.setCategory(object.getString("Category"));


                                AccommodationRoomsModels.add(accomodationFloorModel);

                            }
                            accomodationList.get(3).setAccommodationRoomsModels(AccommodationRoomsModels);
//                          AccomoFloorAdapter accomoFloorAdapter = new AccomoFloorAdapter(getActivity(), accomodationList.get(3).getAccommodationFloorsModels());
//                          rv_floor_names.setAdapter(accomoFloorAdapter);
                            accomoAdapter.notifyDataSetChanged();

//                            }


                        }


                    }

                    setAdapter();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_UNITS + "/area")) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray propertySize = jsonObject.getJSONArray("Result");

                    if (propertySize.length() == 0) {
//                  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < propertySize.length(); index++) {
                            JSONObject jsonObject1 = propertySize.getJSONObject(index);

                            PropertyUnitsModel propertyUnitsModel = new PropertyUnitsModel();

                            propertyUnitsModel.setId(jsonObject1.getString("Id"));
                            propertyUnitsModel.setSizeUnitId(jsonObject1.getString("SizeUnit"));
                            propertyUnitsModels.add(propertyUnitsModel);

                        }

                        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
                        for (int i = 0; i < propertyUnitsModels.size(); i++) {
                            property_area_id.add(propertyUnitsModels.get(i).getSizeUnitId());
                        }

                        adapter.addAll(property_area_id);
                        adapter.add(getString(R.string.select_type));

                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_PHOTOS_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {


                    JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                    JSONArray propertyPhotos = jsonObject_photo.getJSONArray("Result");

                    if (propertyPhotos.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < propertyPhotos.length(); index++) {
                            JSONObject jsonObject1 = propertyPhotos.getJSONObject(index);

                            PhotosModel photosModel = new PhotosModel();

                            photosModel.setId(jsonObject1.getString("Id"));
                            photosModel.setName(jsonObject1.getString("Name"));
                            photosModel.setImageUrl(jsonObject1.getString("Url"));
                            photosModels.add(photosModel);


                        }

                        if (accomo == true) {

                            imageinflateview();

                            accomo = false;

                        } else {
//                            towerListAdapter.imageinflateview(photosModels);
                            towerListAdapter.notifyDataSetChanged();
                        }

                    }
                }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_PHOTOS_PROJECT_ACCOMO)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {


                    JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                    JSONArray propertyPhotos = jsonObject_photo.getJSONArray("Result");

                    if (propertyPhotos.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < propertyPhotos.length(); index++) {
                            JSONObject jsonObject1 = propertyPhotos.getJSONObject(index);

                            PhotosModel photosModel = new PhotosModel();

                            photosModel.setId(jsonObject1.getString("Id"));
                            photosModel.setName(jsonObject1.getString("Name"));
                            photosModel.setImageUrl(jsonObject1.getString("Url"));
                            photosModels.add(photosModel);


                        }

                        imageinflateview();

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {


                    JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");

                }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_ADD_BEDROOM_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");

                        Toast.makeText(getActivity(), "Accomodation is added successfully.", Toast.LENGTH_SHORT).show();
                        rv_floor_names.setVisibility(View.GONE);
                        getAccomoListing();
                    }


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_ADD_ACCOMODATION_BEDROOM)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");

                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        if (dialog != null)
                            dialog.dismiss();
//                        getAccomoRoomListing();
                        getAccomoListing();
                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (which.equalsIgnoreCase(Urls.URL_ADD_BASEMENT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");


                        if (clickedFromNext) {

                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                            tower_list_lay.setVisibility(View.VISIBLE);
                        } else {

//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                            getActivity().finish();

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(this.getString(R.string.message))
                                    .setContentText(this.getString(R.string.tapping_yes))
                                    .setConfirmText(this.getString(R.string.yes))
                                    .setCancelText(this.getString(R.string.cancel))
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismissWithAnimation();
                                        }
                                    })

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            getActivity().finish();

                                        }
                                    })
                                    .show();


                        }
                        //  getTowerListing();

                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (which.equalsIgnoreCase(Urls.URL_ADD_TOWER_INFORMATION)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");

                        if (clickedFromNext) {

                            SM.sendData("Flat/Apartment", 0);
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                        } else {

//                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                            getActivity().finish();

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(this.getString(R.string.message))
                                    .setContentText(this.getString(R.string.tapping_yes))
                                    .setConfirmText(this.getString(R.string.yes))
                                    .setCancelText(this.getString(R.string.cancel))
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismissWithAnimation();
                                        }
                                    })

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            getActivity().finish();

                                        }
                                    })
                                    .show();


                        }


                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void onEvent(StatusModel statusModel) {
        if (statusModel != null) {

            if (statusModel.getOpen().equalsIgnoreCase("yes"))
                addTowersPopup(statusModel.getPos());

        }

    }

    private void imageinflateview() {
        image_view_infate.removeAllViews();

        for (int i = 0; i < photosModels.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   id=(Integer)view.getTag();

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(photosModels.get(id).getId());
                                    photosModels.remove(id);
                                    imageinflateview();
                                }
                            })
                            .show();

                }
            });

            image_view_infate.addView(view);
        }
    }

    public void deletePropertyImages(String position) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT,
                deletephotoInput(position),
                "Loading..",
                this,
                Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT,
                Constants.POST).execute();

    }

    private String deletephotoInput(String id) {
        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }

    public class AccomoAdapter extends RecyclerView.Adapter<AccomoAdapter.ViewHolder> {

        Context context;
        ArrayList<AccomodationModel> floorNumberList = new ArrayList<>();

        public AccomoAdapter(Context context, ArrayList<AccomodationModel> accomodationList) {
            this.context = context;
            this.floorNumberList = accomodationList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_accomodation, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tv_name.setText(floorNumberList.get(position).getTitle());
            holder.tv_floor_no.setText(String.valueOf(floorNumberList.get(position).getFloors()));

            proCatTypeId = floorNumberList.get(position).getProjectCategoryTypeId();

            if (!accomodationList.get(position).getAccommodationFloorsModels().isEmpty()) {

                holder.tv_floor_no.setText(String.valueOf(floorNumberList.get(position).getFloors()) + " Floors");
            }

            holder.iv_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    accomoDeleteId = accomodationList.get(position).getId();
                                    deleteAccomo();
                                }
                            })
                            .show();

                }
            });

            holder.iv_copy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    postAddCopy(position);

                }
            });

            if (AccommodationId != 0) {
                rv_floor_names.setVisibility(View.VISIBLE);
                accomoFloorAdapter = new AccomoFloorAdapter(getActivity(), accomodationList.get(accomoId).getAccommodationFloorsModels());
                rv_floor_names.setLayoutManager(new LinearLayoutManager(getActivity()));
                rv_floor_names.setAdapter(accomoFloorAdapter);

            }

            holder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!accomodationList.get(position).getAccommodationFloorsModels().isEmpty()) {
                        accomoId = position;
                        rv_floor_names.setVisibility(View.VISIBLE);
                        accomoFloorAdapter = new AccomoFloorAdapter(getActivity(), accomodationList.get(position).getAccommodationFloorsModels());
                        if (accomodationList.get(position).getAccommodationFloorsModels().get(0).getAccommodationId() != 0)
                            AccommodationId = accomodationList.get(position).getAccommodationFloorsModels().get(0).getAccommodationId();

                        rv_floor_names.setLayoutManager(new LinearLayoutManager(getActivity()));
                        rv_floor_names.setAdapter(accomoFloorAdapter);
                    } else {
                        Toast.makeText(context, "No floors available", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            holder.iv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   editRoomDetaildialog(floorNumberList.get(position));
                    //   quantityFloor = 0;
                    roomDetaildialog(position);

                }
            });


        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            EditText floor_size, floor_number;
            ImageView floor_minus, iv_del, iv_edit, iv_copy;
            TextView tv_name, tv_floor_no;

            public ViewHolder(View itemView) {
                super(itemView);
                floor_size = (EditText) itemView.findViewById(R.id.floor_size);
                tv_name = (TextView) itemView.findViewById(R.id.tv_name);
                iv_del = (ImageView) itemView.findViewById(R.id.iv_del);
                iv_edit = (ImageView) itemView.findViewById(R.id.iv_edit);
                iv_copy = (ImageView) itemView.findViewById(R.id.iv_copy);
                tv_floor_no = (TextView) itemView.findViewById(R.id.tv_floor_no);

            }


        }
    }

    public class AccomoFloorAdapter extends RecyclerView.Adapter<AccomoFloorAdapter.ViewHolder> {

        Context context;
        ArrayList<AccomodationModel.AccommodationFloors> floorNumberList = new ArrayList<>();

        public AccomoFloorAdapter(Context context, ArrayList<AccomodationModel.AccommodationFloors> accomodationList) {
            this.context = context;
            this.floorNumberList = accomodationList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_accomodation_floor, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tv_name.setText("Type of Accomodation of " + floorNumberList.get(position).getFloorNumber() + " (Dynamic)");
            AccomoFloorRoomsAdapter accomoFloorRoomsAdapter;
            AccomoFloorRoomsAdapter accomoFloorRoomsAdapter1;
//          proCatTypeId = floorNumberList.get(position).getProjectCategoryTypeId();
            ArrayList<AccomodationModel.AccommodationRooms> AccommodationRoomsModels = new ArrayList<>();
            ArrayList<AccomodationModel.AccommodationRooms> AccommodationBalconyModels = new ArrayList<>();
            for (int i = 0; i < accomodationList.get(accomoId).getAccommodationRoomsModels().size(); i++) {

                if (floorNumberList.get(position).getId() == accomodationList.get(accomoId).getAccommodationRoomsModels().get(i).getAccommodationFloorId())
                    AccommodationRoomsModels.add(accomodationList.get(accomoId).getAccommodationRoomsModels().get(i));


            }
            accomoFloorRoomsAdapter = new AccomoFloorRoomsAdapter(context, AccommodationRoomsModels);
            holder.rv_rooms.setLayoutManager(new LinearLayoutManager(getActivity()));
            holder.rv_rooms.setAdapter(accomoFloorRoomsAdapter);

            for (int i = 0; i < accomodationList.get(accomoId).getAccommodationBalconyModels().size(); i++) {


                if (floorNumberList.get(position).getId() == accomodationList.get(accomoId).getAccommodationBalconyModels().get(i).getAccommodationFloorId())

//                    if(accomodationList.get(accomoId).getAccommodationRoomsModels().get(i).getCategory().equalsIgnoreCase("balcony"))
                    AccommodationBalconyModels.add(accomodationList.get(accomoId).getAccommodationBalconyModels().get(i));


            }


            accomoFloorRoomsAdapter1 = new AccomoFloorRoomsAdapter(context, AccommodationBalconyModels);
            holder.rv_balcony.setLayoutManager(new LinearLayoutManager(getActivity()));
            holder.rv_balcony.setAdapter(accomoFloorRoomsAdapter1);

            holder.rl_add_more_accomo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addRoomId = floorNumberList.get(position).getId();
                    addRoomDetaildialog();
//                    Toast.makeText(context, "Add More Rooms", Toast.LENGTH_SHORT).show();
                }
            });
            holder.rl_add_more_b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addRoomId = floorNumberList.get(position).getId();
                    addBalconyDetaildialog();
//                    Toast.makeText(context, "Add More Balcony", Toast.LENGTH_SHORT).show();
                }
            });


        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            EditText floor_size, floor_number;
            ImageView floor_minus, iv_del, iv_edit;
            TextView tv_name, tv_add_more_room, tv_add_more_balcony;
            RecyclerView rv_balcony, rv_rooms;
            RelativeLayout rl_add_more_accomo, rl_add_more_b;

            public ViewHolder(View itemView) {
                super(itemView);
                floor_size = (EditText) itemView.findViewById(R.id.floor_size);
                tv_name = (TextView) itemView.findViewById(R.id.tv_name);
                tv_add_more_room = (TextView) itemView.findViewById(R.id.tv_add_more_room);
                tv_add_more_balcony = (TextView) itemView.findViewById(R.id.tv_add_more_balcony);
                iv_del = (ImageView) itemView.findViewById(R.id.iv_del);
//                iv_edit = (ImageView) itemView.findViewById(R.id.iv_edit);
                rv_balcony = (RecyclerView) itemView.findViewById(R.id.rv_balcony);
                rv_rooms = (RecyclerView) itemView.findViewById(R.id.rv_rooms);
                rl_add_more_accomo = (RelativeLayout) itemView.findViewById(R.id.rl_add_more_accomo);
                rl_add_more_b = (RelativeLayout) itemView.findViewById(R.id.rl_add_more_b);
            }


        }
    }

    private void roomDetaildialog(final int pos) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_add_accomo);
        spinner_accomo_unit = (Spinner) dialog.findViewById(R.id.spinner_accomo_unit);
        TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
        ImageView minus_btn_a = (ImageView) dialog.findViewById(R.id.minus_btn_a);
        final TextView no_of_count_a = (TextView) dialog.findViewById(R.id.no_of_count_a);
        ImageView plus_btn_a = (ImageView) dialog.findViewById(R.id.plus_btn_a);
        LinearLayout lnrEmbededVideo = (LinearLayout) dialog.findViewById(R.id.lnrEmbededVideo);
        final LinearLayout ll_other_info = (LinearLayout) dialog.findViewById(R.id.ll_other_info);
        final LinearLayout upload_photos = (LinearLayout) dialog.findViewById(R.id.upload_photos);
        final EditText et_vidoes = (EditText) dialog.findViewById(R.id.et_vidoes);
        final TextView tv_add_embeded = (TextView) dialog.findViewById(R.id.tv_add_embeded);
        image_view_infate = (LinearLayout) dialog.findViewById(R.id.image_view_infate);
        et_title = (EditText) dialog.findViewById(R.id.et_title);
        et_unit = (EditText) dialog.findViewById(R.id.et_unit);

        TextView tv_done = (TextView) dialog.findViewById(R.id.tv_done);
        ImageView minus_btn_b = (ImageView) dialog.findViewById(R.id.minus_btn_b);
        final TextView no_of_count_b = (TextView) dialog.findViewById(R.id.no_of_count_b);
        ImageView plus_btn_b = (ImageView) dialog.findViewById(R.id.plus_btn_b);
        final CheckBox cb_other_info = (CheckBox) dialog.findViewById(R.id.cb_other_info);

        if (!String.valueOf(((int) accomodationList.get(pos).getBuiltUpAreaSize())).equalsIgnoreCase("")
                || String.valueOf(((int) accomodationList.get(pos).getCarpetAreaSize())).equalsIgnoreCase("")
                || String.valueOf(((int) accomodationList.get(pos).getCoverAreaSize())).equalsIgnoreCase("")
                || String.valueOf(((int) accomodationList.get(pos).getBuiltUpAreaSize())).equalsIgnoreCase("")) {
            cb_other_info.setChecked(true);
        } else {

            cb_other_info.setChecked(false);
        }

        minus_btn_b.setVisibility(View.GONE);
        et_cover_area = (EditText) dialog.findViewById(R.id.et_cover_area);
        et_buildup_area = (EditText) dialog.findViewById(R.id.et_buildup_area);
        et_super_area = (EditText) dialog.findViewById(R.id.et_super_area);
        et_carpet_area = (EditText) dialog.findViewById(R.id.et_carpet_area);
        et_cover_area.setText(String.valueOf(((int) accomodationList.get(pos).getCoverAreaSize())));

        et_buildup_area.setText(String.valueOf(((int) accomodationList.get(pos).getBuiltUpAreaSize())));
        et_carpet_area.setText(String.valueOf(((int) accomodationList.get(pos).getCarpetAreaSize())));
        et_super_area.setText(String.valueOf(((int) accomodationList.get(pos).getSuperAreaSize())));

        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);

        et_title.setText(accomodationList.get(pos).getTitle());
        et_unit.setText(String.valueOf(accomodationList.get(pos).getTotalArea()));
        //   spinner_accomo_unit.setSelection(accomodationList.get(pos).getTotalAreaSizeUnitId());
        no_of_count_a.setText(String.valueOf(accomodationList.get(pos).getBedrooms()));
        no_of_count_b.setText(String.valueOf(accomodationList.get(pos).getFloors()));
        final RecyclerView rv_embeded_video = (RecyclerView) dialog.findViewById(R.id.rv_embeded_video);
        embededVideoUrls.clear();
        embededVideoUrls.add("");
        rv_embeded_video.setLayoutManager(new LinearLayoutManager(getActivity()));
        EmbededVideoAdapter embededVideoAdapter = new EmbededVideoAdapter(getActivity(), embededVideoUrls);
        rv_embeded_video.setAdapter(embededVideoAdapter);

        imageinflateview(photosModels);

        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_title.getText().toString().equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please add title name", Toast.LENGTH_SHORT).show();
                } else {

                    postAddRoomEdit(accomodationList.get(pos));
                    dialog.dismiss();

                }

            }
        });

        tv_add_embeded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                embededVideoUrls.add("");
                rv_embeded_video.setLayoutManager(new LinearLayoutManager(getActivity()));
                EmbededVideoAdapter embededVideoAdapter = new EmbededVideoAdapter(getActivity(), embededVideoUrls);
                rv_embeded_video.setAdapter(embededVideoAdapter);
            }
        });

        upload_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);*/

                checkRunTimePermissionAccomo();

            }
        });


        cb_other_info.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    ll_other_info.setVisibility(View.VISIBLE);
                } else {

                    ll_other_info.setVisibility(View.GONE);

                }
            }
        });


        spinner_accomo_unit.setAdapter(adapter1);
        spinner_accomo_unit.setSelection(adapter1.getCount());

        spinner_accomo_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_accomo_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_accomo_text = spinner_accomo_unit.getSelectedItem().toString();
                    spinner_select_id = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

         spinner_cover_area = (Spinner) dialog.findViewById(R.id.spinner_cover_area);

        spinner_cover_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_cover_area.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_cover = spinner_cover_area.getSelectedItem().toString();
                    spinner_select_cover = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

         spinner_super_area = (Spinner) dialog.findViewById(R.id.spinner_super_area);

        spinner_super_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_super_area.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_super = spinner_super_area.getSelectedItem().toString();
                    spinner_select_super = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

       spinner_built_up = (Spinner) dialog.findViewById(R.id.spinner_built_up);

       spinner_carpet_area = (Spinner) dialog.findViewById(R.id.spinner_carpet_area);

        spinner_carpet_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_carpet_area.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_carpet = spinner_carpet_area.getSelectedItem().toString();
                    spinner_select_carpet = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        if (accomoFirst) {

            spinner_cover_area.setAdapter(adapter);
            // spinner_cover_area.setSelection(adapter.getCount());
            spinner_super_area.setAdapter(adapter);
            // spinner_super_area.setSelection(adapter.getCount());
            spinner_built_up.setAdapter(adapter);
            //  spinner_built_up.setSelection(adapter.getCount());
            spinner_carpet_area.setAdapter(adapter);
            //   spinner_carpet_area.setSelection(adapter.getCount());

        }

        int carpetArea=0;
        int coverArea=0;
        int BuiltUpArea=0;
        int superArea=0;


        for(int i=0;i<propertyUnitsModels.size();i++){

            if(propertyUnitsModels.get(i).getId().equalsIgnoreCase(String.valueOf(accomodationList.get(pos).getCarpetAreaSizeUnitId()))){

                carpetArea=i;
//                return;
            }

            if(propertyUnitsModels.get(i).getId().equalsIgnoreCase(String.valueOf(accomodationList.get(pos).getCoverAreaSizeUnitId()))){

                coverArea=i;
//                return;
            }
            if(propertyUnitsModels.get(i).getId().equalsIgnoreCase(String.valueOf(accomodationList.get(pos).getBuiltUpAreaSizeUnitId()))){

                BuiltUpArea=i;
//                return;
            }
            if(propertyUnitsModels.get(i).getId().equalsIgnoreCase(String.valueOf(accomodationList.get(pos).getSuperAreaSizeUnitId()))){

                superArea=i;
//                return;
            }
        }
        spinner_cover_area.setSelection(coverArea);
        spinner_super_area.setSelection(superArea);
        spinner_built_up.setSelection(BuiltUpArea);
        spinner_carpet_area.setSelection(carpetArea);


/*
        spinner_built_up.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_built_up.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    spinner_built = spinner_built_up.getSelectedItem().toString();
                    spinner_select_built = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
*/

        minus_btn_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityRoom = Integer.parseInt(no_of_count_a.getText().toString());

                if (quantityRoom > 0)
                    quantityRoom--;
                no_of_count_a.setText(String.valueOf(quantityRoom));

            }
        });

        minus_btn_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityFloor = Integer.parseInt(no_of_count_b.getText().toString());

                if (quantityFloor > 0)
                    quantityFloor--;
                no_of_count_b.setText(String.valueOf(quantityFloor));

            }
        });

        plus_btn_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityRoom = Integer.parseInt(no_of_count_a.getText().toString());

                quantityRoom++;
                no_of_count_a.setText(String.valueOf(quantityRoom));
            }
        });

        plus_btn_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                quantityFloor = Integer.parseInt(no_of_count_b.getText().toString());

                quantityFloor++;
                no_of_count_b.setText(String.valueOf(quantityFloor));
            }
        });

        //room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void imageinflateview(final ArrayList<PhotosModel> photosModels) {
        image_view_infate.removeAllViews();

        for (int i = 0; i < photosModels.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   id=(Integer)view.getTag();

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(photosModels.get(id).getId());
                                    photosModels.remove(id);
                                    imageinflateview();
                                }
                            })
                            .show();

                }
            });

            image_view_infate.addView(view);
        }
    }


    private void postAddRoomEdit(AccomodationModel accomodationModel) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ADD_BEDROOM_PROJECT,
                getAddinputEdit(accomodationModel),
                "Loading..",
                this,
                Urls.URL_PROPERTY_ADD_BEDROOM_PROJECT,
                Constants.POST).execute();

    }

    private String getAddinputEdit(AccomodationModel accomodationModel) {
        floorModelList.clear();
        int accomofloorSize = accomodationModel.getAccommodationFloorsModels().size();

        for (int i = 0; i < quantityFloor; i++) {


            if (i == 0) {
                FloorModel floorModel = new FloorModel();
                if (!accomodationModel.getAccommodationFloorsModels().isEmpty()) {
                    floorModel.setId(accomodationModel.getAccommodationFloorsModels().get(0).getId());
                    accomofloorSize--;
                } else {
                    floorModel.setId(0);
                }

                floorModel.setFloorNumber("Ground Floor");
                floorModelList.add(floorModel);
            } else {
                FloorModel floorModel = new FloorModel();
                if (!accomodationModel.getAccommodationFloorsModels().isEmpty()) {
                    if (accomofloorSize != 0) {
                        floorModel.setId(accomodationModel.getAccommodationFloorsModels().get(i).getId());
                        accomofloorSize--;
                    } else {
                        floorModel.setId(0);
                    }

                } else {
                    floorModel.setId(0);
                }

                floorModel.setFloorNumber(i + " " + "Floor");
                floorModelList.add(floorModel);
            }

        }
        String input = "";
        int mineId = 0;

        for (int i = 0; i < Project_name.proCatTypesArrayList.size(); i++) {

            if (Project_name.proCatTypesArrayList.get(i).getCategoryType().equalsIgnoreCase("Flat/Apartment")) {

                mineId = Project_name.proCatTypesArrayList.get(i).getId();
            }

        }
        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {

            for(int i=0;i<propertyUnitsModels.size();i++){

                if(propertyUnitsModels.get(i).getSizeUnitId().equalsIgnoreCase(spinner_carpet_area.getSelectedItem().toString())){

                    jsonObject1.put("CarpetAreaSizeUnitId", propertyUnitsModels.get(i).getId());
                }
                if(propertyUnitsModels.get(i).getSizeUnitId().equalsIgnoreCase(spinner_cover_area.getSelectedItem().toString())){

                    jsonObject1.put("CoverAreaSizeUnitId", propertyUnitsModels.get(i).getId());
                }
                if(propertyUnitsModels.get(i).getSizeUnitId().equalsIgnoreCase(spinner_super_area.getSelectedItem().toString())){

                    jsonObject1.put("SuperAreaSizeUnitId", propertyUnitsModels.get(i).getId());
                }
                if(propertyUnitsModels.get(i).getSizeUnitId().equalsIgnoreCase(spinner_built_up.getSelectedItem().toString())){

                    jsonObject1.put("BuiltUpAreaSizeUnitId", propertyUnitsModels.get(i).getId());
                }
//                else{
//                    jsonObject1.put("CarpetAreaSizeUnitId", 1);
//                }
            }


            jsonObject1.put("Id", accomodationModel.getId());
            jsonObject1.put("ProjectCategoryTypeId", mineId);
            jsonObject1.put("Title", et_title.getText().toString());
            jsonObject1.put("TotalArea", et_unit.getText().toString());
            jsonObject1.put("Bedrooms", quantityRoom);
            jsonObject1.put("Floors", quantityFloor);
            jsonObject1.put("CoverAreaSize", et_cover_area.getText().toString());
//            jsonObject1.put("CoverAreaSizeUnitId", 1);
            jsonObject1.put("SuperAreaSize", et_super_area.getText().toString());
//            jsonObject1.put("SuperAreaSizeUnitId", 1);
            jsonObject1.put("BuiltUpAreaSize", et_buildup_area.getText().toString());
//            jsonObject1.put("BuiltUpAreaSizeUnitId", 1);
            jsonObject1.put("CarpetAreaSize", et_carpet_area.getText().toString());
//            jsonObject1.put("CarpetAreaSizeUnitId", 1);
            jsonObject1.put("Parkings", 0);
//            jsonObject1.put("IsModify", true);


            JSONArray jsonArrayFloor = new JSONArray();

            for (int i = 0; i < floorModelList.size(); i++) {
                JSONObject jsonObjectFloorList = new JSONObject();
                jsonObjectFloorList.put("Id", floorModelList.get(i).getId());
                jsonObjectFloorList.put("FloorNumber", floorModelList.get(i).getFloorNumber());
                jsonArrayFloor.put(jsonObjectFloorList);
                Log.e("FloorNumber", "FloorNumber" + floorModelList.get(i).getFloorNumber());
            }

//          jsonObject1.put("AccomodationFiles", jsonArray1);
            jsonObject1.put("AccommodationFloors", jsonArrayFloor);

            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }

    public class AccomoFloorRoomsAdapter extends RecyclerView.Adapter<AccomoFloorRoomsAdapter.ViewHolder> {

        Context context;
        ArrayList<AccomodationModel.AccommodationRooms> floorNumberList = new ArrayList<>();

        public AccomoFloorRoomsAdapter(Context context, ArrayList<AccomodationModel.AccommodationRooms> accomodationList) {
            this.context = context;
            this.floorNumberList = accomodationList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_accomodation_room, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tv_name.setText(floorNumberList.get(position).getBedRoomName());

//          proCatTypeId = floorNumberList.get(position).getProjectCategoryTypeId();

            holder.iv_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                    deleteRoomOrBalconyId = floorNumberList.get(position).getId();
                                    deleteRoomOrBalcony();
                                }
                            })
                            .show();

                }
            });

            holder.iv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (floorNumberList.get(position).getCategory().equalsIgnoreCase("balcony")) {
                        EditRoomOrBalconyId = floorNumberList.get(position).getId();
                        EditBalconyDetaildialog(floorNumberList.get(position));
                    } else {
                        EditRoomOrBalconyId = floorNumberList.get(position).getId();
                        EditRoomDetaildialog(floorNumberList.get(position));
                    }
                }
            });


        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            EditText floor_size, floor_number;
            ImageView floor_minus, iv_del, iv_edit;
            TextView tv_name;
            RecyclerView rv_balcony, rv_rooms;

            public ViewHolder(View itemView) {
                super(itemView);
                floor_size = (EditText) itemView.findViewById(R.id.floor_size);
                tv_name = (TextView) itemView.findViewById(R.id.tv_name);
                iv_del = (ImageView) itemView.findViewById(R.id.iv_del);
                iv_edit = (ImageView) itemView.findViewById(R.id.iv_edit);
                rv_balcony = (RecyclerView) itemView.findViewById(R.id.rv_balcony);
                rv_rooms = (RecyclerView) itemView.findViewById(R.id.rv_rooms);
            }


        }
    }

    public class EmbededVideoAdapter extends RecyclerView.Adapter<EmbededVideoAdapter.ViewHolder> {

        Context context;
        ArrayList<String> floorNumberList = new ArrayList<>();

        public EmbededVideoAdapter(Context context, ArrayList<String> accomodationList) {
            this.context = context;
            this.floorNumberList = accomodationList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_accomodation_embeded_video, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.et_vidoes.setText(embededVideoUrls.get(position));

            holder.et_vidoes.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    embededVideoUrls.remove(position);
                    embededVideoUrls.add(position, holder.et_vidoes.getText().toString());
                    //   notifyDataSetChanged();

                }
            });

            if (position == 0) {

                holder.imgCloseAddMore.setVisibility(View.GONE);
            } else {

                holder.imgCloseAddMore.setVisibility(View.VISIBLE);

            }

            holder.imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String text = holder.et_vidoes.getText().toString();

                    embededVideoUrls.remove(position);
                    notifyDataSetChanged();


                }
            });

        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            EditText et_vidoes;
            ImageView imgCloseAddMore;

            public ViewHolder(View itemView) {
                super(itemView);
                et_vidoes = (EditText) itemView.findViewById(R.id.et_vidoes);
                imgCloseAddMore = (ImageView) itemView.findViewById(R.id.imgCloseAddMore);

            }

        }
    }

    private void deleteAccomo() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_DELETE_ACCOMODATION,
                deleteInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_DELETE_ACCOMODATION,
                Constants.POST).execute();

    }

    public void deleteTower(int id) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_DELETE_TOWERS,
                deleteTowerInput(id),
                "Loading..",
                this,
                Urls.URL_PROPERTY_DELETE_TOWERS,
                Constants.POST).execute();

    }

    private void deleteRoomOrBalcony() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_DELETE_ROOM_OR_BALCONY_ACCOMODATION,
                deleteRoomOrBalconyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_DELETE_ROOM_OR_BALCONY_ACCOMODATION,
                Constants.POST).execute();

    }

    private String deleteInput() {
        String input = "";

        JSONArray jsonArray = new JSONArray();
        try {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("Id", accomoDeleteId);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        input = jsonArray.toString();

        return input;
    }

    private String deleteTowerInput(int id) {
        String input = "";

        JSONArray jsonArray = new JSONArray();
        try {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("Id", id);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        input = jsonArray.toString();

        return input;
    }

    private String deleteRoomOrBalconyInput() {
        String input = "";

        JSONArray jsonArray = new JSONArray();
        try {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("Id", deleteRoomOrBalconyId);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        input = jsonArray.toString();

        return input;
    }

    private void postAddCopy(int pos) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ADD_BEDROOM_PROJECT,
                getAddinputCopy(pos),
                "Loading..",
                this,
                Urls.URL_PROPERTY_ADD_BEDROOM_PROJECT,
                Constants.POST).execute();

    }

    private String getAddinputCopy(int pos) {
        floorModelList.clear();
        for (int i = 0; i < accomodationList.get(pos).getFloors(); i++) {


            if (i == 0) {
                FloorModel floorModel = new FloorModel();
                floorModel.setId(0);
                floorModel.setFloorNumber("Ground Floor");
                floorModelList.add(floorModel);
            } else {
                FloorModel floorModel = new FloorModel();
                floorModel.setId(0);
                floorModel.setFloorNumber(i + " " + "Floor");
                floorModelList.add(floorModel);
            }

        }
        String input = "";

        int mineId = 0;

        for (int i = 0; i < Project_name.proCatTypesArrayList.size(); i++) {

            if (Project_name.proCatTypesArrayList.get(i).getCategoryType().equalsIgnoreCase("Flat/Apartment")) {

                mineId = Project_name.proCatTypesArrayList.get(i).getId();
            }

        }
        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {

            jsonObject1.put("Id", 0);
            jsonObject1.put("ProjectCategoryTypeId", mineId);
            jsonObject1.put("Title", accomodationList.get(pos).getTitle());
            jsonObject1.put("TotalArea", accomodationList.get(pos).getTotalArea());
            jsonObject1.put("Bedrooms", quantityRoom);
            jsonObject1.put("Floors", accomodationList.get(pos).getFloors());
            jsonObject1.put("CoverAreaSize", et_cover_area.getText().toString());
            jsonObject1.put("CoverAreaSizeUnitId", 1);
            jsonObject1.put("SuperAreaSize", et_super_area.getText().toString());
            jsonObject1.put("SuperAreaSizeUnitId", 1);
            jsonObject1.put("BuiltUpAreaSize", et_buildup_area.getText().toString());
            jsonObject1.put("BuiltUpAreaSizeUnitId", 1);
            jsonObject1.put("CarpetAreaSize", et_carpet_area.getText().toString());
            jsonObject1.put("CarpetAreaSizeUnitId", 1);
            jsonObject1.put("Parkings", "");
            jsonObject1.put("IsModify", false);


            JSONArray jsonArrayFloor = new JSONArray();

            for (int i = 0; i < floorModelList.size(); i++) {
                JSONObject jsonObjectFloorList = new JSONObject();
                jsonObjectFloorList.put("Id", floorModelList.get(i).getId());
                jsonObjectFloorList.put("FloorNumber", floorModelList.get(i).getFloorNumber());
                jsonArrayFloor.put(jsonObjectFloorList);
                Log.e("FloorNumber", "FloorNumber" + floorModelList.get(i).getFloorNumber());
            }

            // jsonObject1.put("AccomodationFiles", jsonArray1);
            jsonObject1.put("AccommodationFloors", jsonArrayFloor);

            input = jsonObject1.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }


        return input;

    }

    @Override
    public void onResume() {
        super.onResume();


        EventBus.getDefault().register(this);

    }

    @Override
    public void onPause() {
        super.onPause();

        EventBus.getDefault().unregister(this);

    }

}
