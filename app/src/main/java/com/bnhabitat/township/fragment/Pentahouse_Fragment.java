package com.bnhabitat.township.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.FloorCalculationModel;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.township.adapter.PentaHouseDuplexAdapter;
import com.bnhabitat.township.adapter.PlotLotUnitAdapter;
import com.bnhabitat.township.adapter.ResidentialFloorAdapter;
import com.bnhabitat.township.model.FloorUnitModel;
import com.bnhabitat.township.model.ResidentialFloorModel;
import com.bnhabitat.township.model.SizeUnitModel;
import com.bnhabitat.township.townactivities.TabChangeInterface;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.ui.fragments.LocationFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.kyleduo.switchbutton.SwitchButton;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class Pentahouse_Fragment extends Fragment implements CommonAsync.OnAsyncResultListener {

    View view;
    LinearLayout add_more;
    RecyclerView pentahouse_view;
    RecyclerView rowhouse_recycler_view;
    ArrayList<ResidentialFloorModel> arrayList = new ArrayList<>();
    ArrayList<String> listUnit = new ArrayList<>();
    static int flagTower1 = 0;
    static int flagTower2 = 0;
    static int flagTower3 = 0;
    Button next;
    private JSONObject resObj;
    //PentaHouseDuplexAdapter pentaHouseDuplexAdapter;
    Button done;
    boolean clickedFromNext = true;
    TabChangeInterface tabChangeInterface;
    String unit_from, unit_to, street = "";
    int areaUnitId;
    float area;
    ArrayList<PropertyUnitsModel> propertyUnitsModels = new ArrayList<>();
    String userChoosenTask;
    private static int REQUEST_CAMERA = 101;
    private static int REQUEST_CAMERA_ACCOMO = 1011;
    private static int SELECT_FILE = 102;
    private static int SELECT_FILE_ACCOOMO = 1012;
    private String encodedImageData;
    private ArrayList<String> embededVideoUrls = new ArrayList<>();
    private boolean isPermitted = false;
    EditText et_floorno, et_floorsize;
    ArrayList<FloorCalculationModel> floorNumberList = new ArrayList<>();
    private FloornumberAdapter floornumberAdapter;
    RecyclerView floor_number_list, rv_floor, room_detail_list, balcony_detail_list;
    public Floor flooradapter;
    private int selectedFloorId = 0;
    ArrayList<ResidentialFloorModel.Properties.PhotosModel> photosModels = new ArrayList<>();
    RowHouseAdapter rowHouseAdapter;
    private int floorId;
    int rowhouselistPosition = 0;
    SpinnerAdapter adapter, adapter1;
    int floor_id = 1;
    int floorNumber;
    double floorSize;
    TextView update_more_floor;
    LinearLayout ll_detail;
    NestedScrollView scroll_view;
    LinearLayout ll_main;

    private Dialog dialog, dialog1;
    private EditText length;
    private EditText width;
    private EditText room_size;
    private EditText room_name;
    private String type = "";
    ArrayList<String> property_units_id = new ArrayList<>();
    private String room_type = "";
    private int AccommodationId = 0;
    ArrayList<PropertyBedroomModel> room_list = new ArrayList<>();
    int selectedRowHousePos = 0;
    private boolean isShown = true;
    private boolean first = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.penta_house_duplex_fragment, null);
        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);
        ll_main = (LinearLayout) view.findViewById(R.id.ll_main);

        init();
        onClicks();
        //   initialization();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {

            if (isShown) {
                getAllSizeUnits(Constants.AREA);
                if (arrayList.size() == 0) {
                    initialization();

                }
                isShown = false;

            } else {
                isShown = false;
            }


        }
    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    private void getPropertylengthSizes() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/length",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_UNITS + "/length",
                Constants.GET).execute();

    }

    private void init() {
        rowhouse_recycler_view = (RecyclerView) view.findViewById(R.id.pentahouse_view);
        add_more = (LinearLayout) view.findViewById(R.id.add_more);
        next = (Button) view.findViewById(R.id.next);
        tabChangeInterface = (TabChangeInterface) getActivity();
        done = (Button) view.findViewById(R.id.done);
    }

    public void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {
            // if already permition granted
            selectImage();

        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.bnhabitat.utils.Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void cameraIntentAccomo() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA_ACCOMO);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void galleryIntentAccomo() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE_ACCOOMO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
          /*  else if (requestCode == SELECT_FILE_ACCOOMO)
                onSelectFromGalleryResultAccomo(data);
*/
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);

         /*   else if (requestCode == REQUEST_CAMERA_ACCOMO)
                onCaptureImageResultAccomo(data);*/

        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
    //    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encodedImageData = getEncoded64ImageStringFromBitmap(thumbnail);
        postPropertyImages();
//      ivImage.setImageBitmap(thumbnail);
    }

    private void postPropertyImages() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_PHOTOS_PROJECT,
                photoInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_PHOTOS_PROJECT,
                Constants.POST).execute();

    }

    private String photoInput() {
        String inputStr = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("CompanyId", "1");
            jsonObject1.put("Name", "mine");
            jsonObject1.put("Type", "jpg");
            jsonObject1.put("ImageCode", encodedImageData);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputStr = jsonArray.toString();
        return inputStr;
    }

/*
    private void postPropertyImagesAccomo() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_PHOTOS_PROJECT_ACCOMO,
                photoInputAccomo(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_PHOTOS_PROJECT_ACCOMO,
                Constants.POST).execute();

    }
*/

    private void onCaptureImageResultAccomo(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
     //   File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encodedImageData = getEncoded64ImageStringFromBitmap(thumbnail);
        //  postPropertyImagesAccomo();
//      ivImage.setImageBitmap(thumbnail);
    }

/*
    private String photoInputAccomo() {
        String inputStr = "";
        accomo = true;

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("CompanyId", "1");
            jsonObject1.put("Name", "mine");
            jsonObject1.put("Type", "jpg");
            jsonObject1.put("ImageCode", encodedImageData);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputStr = jsonArray.toString();
        return inputStr;
    }
*/

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    private void onSelectFromGalleryResultAccomo(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodedImageData = getEncoded64ImageStringFromBitmap(bm);
                //    postPropertyImagesAccomo();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ivImage.setImageBitmap(bm);
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodedImageData = getEncoded64ImageStringFromBitmap(bm);
                postPropertyImages();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ivImage.setImageBitmap(bm);
    }

    private void onClicks() {

/*
        ll_main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideSoftKeyboard(getActivity());
                return true;
            }
        });
*/

        add_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  if (street.equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please fill street name", Toast.LENGTH_SHORT).show();

                } else {


                }*/

                initialization();

                sendResidentialData();
                scroll_view.scrollTo(10, 10);

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* if (street.equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please fill street name", Toast.LENGTH_SHORT).show();

                } else {

                }*/

                sendResidentialData();
                tabChangeInterface.onTabChange(1);

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickedFromNext = false;

              /*  if (street.equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please fill street name", Toast.LENGTH_SHORT).show();

                } else {



                }*/

                sendResidentialData();

            }
        });


    }

    public void onEvent(DialogModel dialogModel) {

        unit_from = dialogModel.getUnit_from();
        unit_to = dialogModel.getUnit_to();
        street = dialogModel.getName();
        area = dialogModel.getArea();
        areaUnitId = dialogModel.getPosSpinner();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean openActivityOnce = true;
        boolean openDialogOnce = true;
        if (requestCode == 11111) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView();
                        }
                    }
                }
            }

            selectImage();

        } else if (requestCode == 11) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView();
                        }
                    }
                }
            }

            selectImageAccomo();

        }

    }

    @Override
    public void onResume() {
        super.onResume();


        EventBus.getDefault().register(this);

    }

    private void selectImageAccomo() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.bnhabitat.utils.Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntentAccomo();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntentAccomo();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onPause() {
        super.onPause();

        EventBus.getDefault().unregister(this);

    }

    private void alertView() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.AppTheme);

        dialog.setTitle("Permission Denied")
                .setInverseBackgroundForced(true)
                //.setIcon(R.drawable.ic_info_black_24dp)
                .setMessage("Without those permission the app is unable to save your profile. App needs to save profile image in your external storage and also need to get profile image from camera or external storage.Are you sure you want to deny this permission?")

                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }
                })
                .setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        checkRunTimePermission();

                    }
                }).show();
    }


    public void sendResidentialData() {

        try {

            // plot/lot/land request
            resObj = new JSONObject();
            resObj.put("ProjectCategoryTypeId", "4");
            resObj.put("NeighborhoodName", street);
            resObj.put("Area", "");
            resObj.put("AreaUnitId", " 1");
            resObj.put("UnitFrom", "");
            resObj.put("UnitTo", "");

            JSONArray propertyArray = new JSONArray();

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("PropertyTypeId", "1");
            jsonObject1.put("CompanyId", "");
            jsonObject1.put("CreatedById", " ");
            jsonObject1.put("UpdatedById", "");
            jsonObject1.put("NoOfParkings", "");

            JSONArray propLocationArray = new JSONArray();

/*
            for (int i = 0; i < PlotLotUnitAdapter.unitModelArrayList.size(); i++) {

                JSONObject jsonObjectLoc = new JSONObject();

                jsonObjectLoc.put("ProjectId", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("ProjectId", ""));
                jsonObjectLoc.put("UnitNo", PlotLotUnitAdapter.unitModelArrayList.get(i).getValue());

                propLocationArray.put(jsonObjectLoc);

            }
*/

            jsonObject1.put("PropertyLocations", propLocationArray);

            propertyArray.put(jsonObject1);

            resObj.put("Properties", propertyArray);

            // detail frag data

            postLandData();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void postLandData() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_NEIGHBORHOOD,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_NEIGHBORHOOD,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = resObj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return inputStr;
    }


    private void initialization() {

        ResidentialFloorModel residentialFloorModel = new ResidentialFloorModel();
        residentialFloorModel.setProjectCategoryTypeId("2");
        residentialFloorModel.setNeighborhoodName("");
        residentialFloorModel.setArea("");
        residentialFloorModel.setUnitFrom("000");
        residentialFloorModel.setUnitTo("000");

        ArrayList<ResidentialFloorModel.Properties> propertiesArrayList = new ArrayList<>();
        ResidentialFloorModel.Properties properties = new ResidentialFloorModel.Properties();
        properties.setPropertyTypeId("2");
        properties.setCompanyId("1");
        properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
        properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
        properties.setNoOfBedrooms("0");
        properties.setNoOfFloors("0");
        properties.setNoOfParkings("0");

        ArrayList<ResidentialFloorModel.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
        ResidentialFloorModel.Properties.PropertyAreas propertyAreas = new ResidentialFloorModel.Properties.PropertyAreas();
        propertyAreas.setFrontSize("");
        propertyAreas.setDepthSize("");
        propertyAreasArrayList.add(propertyAreas);

        properties.setPropertyAreas(propertyAreasArrayList);
        propertiesArrayList.add(properties);

        residentialFloorModel.setProperties(propertiesArrayList);

        arrayList.add(residentialFloorModel);
    }

    private void getAllSizeUnits(String type) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/" + type,
                "",
                "Loading..",
                this,
                Constants.GET_ALL_SIZE_UNITS_API,
                Constants.GET).execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (which.equalsIgnoreCase(Constants.GET_ALL_SIZE_UNITS_API)) {
            SizeUnitModel sizeUnitModel = Utils.getGsonObject().fromJson(result, SizeUnitModel.class);
            if (sizeUnitModel != null) {

                listUnit.add(0, getString(R.string.select));
                ArrayList<SizeUnitModel.Result> result1 = sizeUnitModel.getResult();
                if (result1.size() != 0) {

                    for (int i = 0; i < result1.size(); i++) {

                        String strUnits = result1.get(i).getSizeUnit();
                        listUnit.add(strUnits);
                    }

                }

                getPropertylengthSizes();


            } else {
                Utils.showErrorMessage(R.string.something_wrong, getActivity());
            }

        } else if (which.equalsIgnoreCase(Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {


                    JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");

                }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_PHOTOS_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {


                    JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                    JSONArray propertyPhotos = jsonObject_photo.getJSONArray("Result");

                    if (propertyPhotos.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < propertyPhotos.length(); index++) {
                            JSONObject jsonObject1 = propertyPhotos.getJSONObject(index);

                            ResidentialFloorModel.Properties.PhotosModel photosModel = new ResidentialFloorModel.Properties.PhotosModel();

                            photosModel.setId(jsonObject1.getString("Id"));
                            photosModel.setName(jsonObject1.getString("Name"));
                            photosModel.setImageUrl(jsonObject1.getString("Url"));
                            photosModel.setRandomId(new Random().toString());
                            photosModel.setSelectedId(selectedRowHousePos);
                            photosModels.add(photosModel);


                        }

                        ResidentialFloorModel residentialFloorModel = new ResidentialFloorModel();
                        residentialFloorModel.setProjectCategoryTypeId(arrayList.get(selectedRowHousePos).getProjectCategoryTypeId());
                        residentialFloorModel.setNeighborhoodName("");
                        residentialFloorModel.setArea("");
                        residentialFloorModel.setUnitFrom("000");
                        residentialFloorModel.setUnitTo("000");
                        residentialFloorModel.setId(String.valueOf(floorId));


                        ArrayList<ResidentialFloorModel.Properties> propertiesArrayList = new ArrayList<>();
                        ResidentialFloorModel.Properties properties = new ResidentialFloorModel.Properties();
                        properties.setPropertyTypeId("2");
                        properties.setCompanyId("1");
                        properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                        properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                        properties.setNoOfBedrooms("0");
                        properties.setNoOfFloors("0");
                        properties.setNoOfParkings("0");

                        ArrayList<ResidentialFloorModel.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
                        ResidentialFloorModel.Properties.PropertyAreas propertyAreas = new ResidentialFloorModel.Properties.PropertyAreas();
                        propertyAreas.setFrontSize("");
                        propertyAreas.setDepthSize("");
                        propertyAreasArrayList.add(propertyAreas);
                        properties.setPropertyAreas(propertyAreasArrayList);
                        propertiesArrayList.add(properties);
                        properties.setPhotosModels(photosModels);
                        properties.setPropertyBedrooms(room_list);
                        residentialFloorModel.setProperties(propertiesArrayList);

                        arrayList.set(selectedRowHousePos, residentialFloorModel);

                        rowHouseAdapter.notifyDataSetChanged();


                    }
                }

//           InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//           imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }/*
        if (which.equalsIgnoreCase(Urls.URL_PROPERTY_UNITS + "/area")) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray propertySize = jsonObject.getJSONArray("Result");

                    if (propertySize.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < propertySize.length(); index++) {
                            JSONObject jsonObject1 = propertySize.getJSONObject(index);

                            PropertyUnitsModel propertyUnitsModel = new PropertyUnitsModel();

                            propertyUnitsModel.setId(jsonObject1.getString("Id"));
                            propertyUnitsModel.setSizeUnitId(jsonObject1.getString("SizeUnit"));
                            propertyUnitsModels.add(propertyUnitsModel);


                        }


                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
*/

        if (which.equalsIgnoreCase(Urls.URL_PROPERTY_UNITS + "/length")) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray propertySize = jsonObject.getJSONArray("Result");

                    if (propertySize.length() == 0) {
//                  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < propertySize.length(); index++) {
                            JSONObject jsonObject1 = propertySize.getJSONObject(index);

                            PropertyUnitsModel propertyUnitsModel = new PropertyUnitsModel();

                            propertyUnitsModel.setId(jsonObject1.getString("Id"));
                            propertyUnitsModel.setSizeUnitId(jsonObject1.getString("SizeUnit"));
                            propertyUnitsModels.add(propertyUnitsModel);
                            //     pentaHouseDuplexAdapter.notifyDataSetChanged();

                        }

                        rowHouseAdapter = new RowHouseAdapter(getContext(), arrayList, listUnit, Pentahouse_Fragment.this, propertyUnitsModels);
                        rowhouse_recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
                        rowhouse_recycler_view.setAdapter(rowHouseAdapter);
                        rowHouseAdapter.notifyDataSetChanged();

                       /* adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
                        for (int i = 0; i < propertyUnitsModels.size(); i++) {
                            property_units_id.add(propertyUnitsModels.get(i).getSizeUnitId());
                        }

                        adapter1.addAll(property_units_id);
                        adapter1.add(getString(R.string.select_type));*/

                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (which.equalsIgnoreCase(Urls.URL_PROJECT_NEIGHBOURHOODS)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {
                    JSONObject jsonResult = jsonObject.getJSONObject("Result");

                    String message = jsonResult.getString("Message");

                    //  jsonResult.getJSONObject("Result").get("Id")
                    JSONArray jsonArray = jsonResult.getJSONObject("Result").getJSONArray("Result");


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject dataObj = jsonArray.getJSONObject(i);
                        ResidentialFloorModel residentialFloorModel = new ResidentialFloorModel();
                        floorId = jsonResult.getJSONObject("Result").getInt("Id");
                        residentialFloorModel.setProjectCategoryTypeId(jsonResult.getJSONObject("Result").getString("NeighborhoodName"));
                        residentialFloorModel.setNeighborhoodName("");
                        residentialFloorModel.setArea("");
                        residentialFloorModel.setUnitFrom("000");
                        residentialFloorModel.setUnitTo("000");
                        residentialFloorModel.setId(String.valueOf(floorId));


                        ArrayList<ResidentialFloorModel.Properties> propertiesArrayList = new ArrayList<>();
                        ResidentialFloorModel.Properties properties = new ResidentialFloorModel.Properties();
                        properties.setPropertyTypeId("2");
                        properties.setCompanyId("1");
                        properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                        properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                        properties.setNoOfBedrooms("0");
                        properties.setNoOfFloors("0");
                        properties.setNoOfParkings("0");

//                        ArrayList<ResidentialFloorModel.PropertyBedrooms> propertyBedrooms = new ArrayList<>();
//                        ResidentialFloorModel.PropertyBedrooms propertyBedrooms1 = new ResidentialFloorModel.PropertyBedrooms();
//                        propertyBedrooms1.setBedRoomName("");
//                        propertyBedrooms1.setCategory("room");


                        ArrayList<ResidentialFloorModel.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
                        ResidentialFloorModel.Properties.PropertyAreas propertyAreas = new ResidentialFloorModel.Properties.PropertyAreas();
                        propertyAreas.setFrontSize("");
                        propertyAreas.setDepthSize("");
                        propertyAreasArrayList.add(propertyAreas);
//                        properties.setPropertyBedrooms(propertyBedrooms);
                        properties.setPropertyAreas(propertyAreasArrayList);
                        propertiesArrayList.add(properties);

                        residentialFloorModel.setProperties(propertiesArrayList);


                        arrayList.set(rowhouselistPosition, residentialFloorModel);
                    }

                    rowhouselistPosition++;
                    if (clickedFromNext) {

                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        getActivity().finish();

                    }

                    // setPlotAdapter();
                    rowHouseAdapter.notifyDataSetChanged();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_NEIGHBORHOOD)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {
                    JSONObject jsonResult = jsonObject.getJSONObject("Result");

                    String message = jsonResult.getString("Message");

                    //  jsonResult.getJSONObject("Result").get("Id")

                    floorId = jsonResult.getJSONObject("Result").getInt("Id");
                    ResidentialFloorModel residentialFloorModel = new ResidentialFloorModel();
                    residentialFloorModel.setProjectCategoryTypeId("2");
                    residentialFloorModel.setNeighborhoodName("");
                    residentialFloorModel.setArea("");
                    residentialFloorModel.setUnitFrom("000");
                    residentialFloorModel.setUnitTo("000");
                    residentialFloorModel.setId(String.valueOf(floorId));


                    ArrayList<ResidentialFloorModel.Properties> propertiesArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties properties = new ResidentialFloorModel.Properties();
                    properties.setPropertyTypeId("2");
                    properties.setCompanyId("1");
                    properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setNoOfBedrooms("0");
                    properties.setNoOfFloors("0");
                    properties.setNoOfParkings("0");

                    ArrayList<ResidentialFloorModel.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties.PropertyAreas propertyAreas = new ResidentialFloorModel.Properties.PropertyAreas();
                    propertyAreas.setFrontSize("");
                    propertyAreas.setDepthSize("");
                    propertyAreasArrayList.add(propertyAreas);

                    properties.setPropertyAreas(propertyAreasArrayList);
                    propertiesArrayList.add(properties);

                    residentialFloorModel.setProperties(propertiesArrayList);

                    arrayList.set(rowhouselistPosition, residentialFloorModel);
                    rowhouselistPosition++;
                    if (clickedFromNext) {

                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        getActivity().finish();

                    }
                    rowHouseAdapter.notifyDataSetChanged();
//                    getAllProjectList(floorId);
                    // setPlotAdapter();
//                    rowHouseAdapter.notifyDataSetChanged();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {


                    JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_ADD_BEDROOM_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");

                        Toast.makeText(getActivity(), "Accomodation is added successfully.", Toast.LENGTH_SHORT).show();
//                        rv_floor_names.setVisibility(View.GONE);
//                        getFloorList();
//                        getAllProjectList(0);

                        getBedroomList();
                        getBalconyList();
                    }


                }
            } catch (Exception e) {

            }
        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_ALL_BEDROOM_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray propertySize = jsonObject.getJSONArray("Result");

                    if (propertySize.length() == 0) {
                        room_list.clear();
//                   Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {

                        room_list.clear();
                        for (int index = 0; index < propertySize.length(); index++) {
                            JSONObject jsonObject1 = propertySize.getJSONObject(index);

                            PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();

                            propertyBedroomModel.setId(jsonObject1.getString("Id"));
                            propertyBedroomModel.setFloor(jsonObject1.getString("FloorNo"));
                            propertyBedroomModel.setRoom_name(jsonObject1.getString("BedRoomName"));
                            propertyBedroomModel.setGoundtype(jsonObject1.getString("GroundType"));
                            propertyBedroomModel.setRoom_type(jsonObject1.getString("BedRoomType"));
                            propertyBedroomModel.setRoom_size(jsonObject1.getString("BedRoomSize"));
                            propertyBedroomModel.setLength(jsonObject1.getString("Length"));
                            propertyBedroomModel.setWidth(jsonObject1.getString("Width"));
                            propertyBedroomModel.setLengthWidthUnitId(jsonObject1.getString("LengthWidthUnitId"));
                            propertyBedroomModel.setCategory(jsonObject1.getString("Category"));

                            try {
                                JSONObject jsonObject11 = jsonObject1.getJSONObject("PropertySizeUnit");
                                propertyBedroomModel.setSizeUnit(jsonObject11.getString("SizeUnit"));

                            } catch (Exception e) {

                            }
                            JSONArray jsonArray = jsonObject1.getJSONArray("PropertyBedroomAttachWiths");
                            ArrayList<PropertyBedroomModel.AttachPropertyBedroomId> attachPropertyBedroomIds = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                PropertyBedroomModel.AttachPropertyBedroomId attachPropertyBedroomId = propertyBedroomModel.new AttachPropertyBedroomId();

                                attachPropertyBedroomId.setAttachPropertyBedroomId(object.getString("AttachPropertyBedroomId"));
                                attachPropertyBedroomId.setAttachPropertyBedroomName(object.getString("AttachPropertyBedroomName"));
                                attachPropertyBedroomIds.add(attachPropertyBedroomId);

                            }
                            propertyBedroomModel.setAttachPropertyBedroomIds(attachPropertyBedroomIds);

                            room_list.add(propertyBedroomModel);


//                            roomListAdapter = new RoomListAdapter(getActivity(), room_list);
//
//                            room_detail_list.setAdapter(roomListAdapter);

                        }


                        rowHouseAdapter.notifyDataSetChanged();

                    }

//                    roomListAdapter = new RoomListAdapter(getActivity(), room_list);
//                    room_detail_list.setAdapter(roomListAdapter);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_ALL_BEDROOM_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray propertySize = jsonObject.getJSONArray("Result");

                    if (propertySize.length() == 0) {
                        room_list.clear();
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {

                        room_list.clear();
                        for (int index = 0; index < propertySize.length(); index++) {
                            JSONObject jsonObject1 = propertySize.getJSONObject(index);

                            PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();

                            propertyBedroomModel.setId(jsonObject1.getString("Id"));
                            propertyBedroomModel.setFloor(jsonObject1.getString("FloorNo"));
                            propertyBedroomModel.setRoom_name(jsonObject1.getString("BedRoomName"));
                            propertyBedroomModel.setGoundtype(jsonObject1.getString("GroundType"));
                            propertyBedroomModel.setRoom_type(jsonObject1.getString("BedRoomType"));
                            propertyBedroomModel.setRoom_size(jsonObject1.getString("BedRoomSize"));
                            propertyBedroomModel.setLength(jsonObject1.getString("Length"));
                            propertyBedroomModel.setWidth(jsonObject1.getString("Width"));
                            propertyBedroomModel.setLengthWidthUnitId(jsonObject1.getString("LengthWidthUnitId"));
                            propertyBedroomModel.setCategory(jsonObject1.getString("Category"));

                            try {
                                JSONObject jsonObject11 = jsonObject1.getJSONObject("PropertySizeUnit");
                                propertyBedroomModel.setSizeUnit(jsonObject11.getString("SizeUnit"));

                            } catch (Exception e) {

                            }
                            JSONArray jsonArray = jsonObject1.getJSONArray("PropertyBedroomAttachWiths");
                            ArrayList<PropertyBedroomModel.AttachPropertyBedroomId> attachPropertyBedroomIds = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                PropertyBedroomModel.AttachPropertyBedroomId attachPropertyBedroomId = propertyBedroomModel.new AttachPropertyBedroomId();

                                attachPropertyBedroomId.setAttachPropertyBedroomId(object.getString("AttachPropertyBedroomId"));
                                attachPropertyBedroomId.setAttachPropertyBedroomName(object.getString("AttachPropertyBedroomName"));
                                attachPropertyBedroomIds.add(attachPropertyBedroomId);

                            }
                            propertyBedroomModel.setAttachPropertyBedroomIds(attachPropertyBedroomIds);

                            room_list.add(propertyBedroomModel);

//                            roomListAdapter = new RoomListAdapter(getActivity(), room_list);
//                            room_detail_list.setAdapter(roomListAdapter);

                        }
                        rowHouseAdapter.notifyDataSetChanged();
                    }

//                    roomListAdapter = new RoomListAdapter(getActivity(), room_list);
//                    room_detail_list.setAdapter(roomListAdapter);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_REMOVE_FLOOR)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {
                    JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                    String success = jsonObject1.getString("Success");
                    if (success.equals("true")) {

//                        getAllProjectList(0);

                    } else {

                        Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (which.equalsIgnoreCase(Urls.FLOOR_LIST_NEIGHBOURBOOD)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray result1 = jsonObject.getJSONArray("Result");
                    floorNumberList.clear();
                    for (int i = 0; i < result1.length(); i++) {
                        JSONObject jsonObject2 = result1.getJSONObject(i);

                        final FloorCalculationModel floorCalculationModel = new FloorCalculationModel();
                        floorCalculationModel.setId((Integer) jsonObject2.get("Id"));

                        floorCalculationModel.setFloorNumber(jsonObject2.getInt("FloorNumber"));
                        floorCalculationModel.setFloorSize(jsonObject2.getDouble("FloorSize"));
                        floorNumberList.add(floorCalculationModel);
                    }

//                    getAllProjectList(0);

//                    flooradapter = new Floor(floorNumberList);
//                    final LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
//                    layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
//                    rv_floor.setLayoutManager(layoutManager1);
//                    rv_floor.setAdapter(flooradapter);
//                    floor_number_list.setVisibility(View.VISIBLE);
//                    rv_floor.setVisibility(View.VISIBLE);
//                    floornumberAdapter.notifyDataSetChanged();
//                    flooradapter.notifyDataSetChanged();
//                    update_more_floor.setVisibility(View.GONE);
                   /* et_floorsize.setText("");
                    et_floorno.setText("");*/
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_FLOOR_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {
                    JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                    String success = jsonObject1.getString("Success");
                    if (success.equals("true")) {


                        getFloorList();

                    } else {

                        Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (which.equalsIgnoreCase(Urls.URL_PROJECT_ADD_BEDROOM)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject addJson = jsonObject.getJSONObject("Result");

                    boolean success = addJson.getBoolean("Success");
                    if (success) {
                        String message = addJson.getString("Message");

                        if (type.equalsIgnoreCase("room")) {
                            Toast.makeText(getActivity(), "Room added successfully", Toast.LENGTH_SHORT).show();

                        } else {

                            Toast.makeText(getActivity(), "Balcony added successfully", Toast.LENGTH_SHORT).show();
                        }

                        if (dialog != null)
                            dialog.dismiss();

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (which.equalsIgnoreCase(Urls.URL_PROPERTY_UNITS + "/length")) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray propertySize = jsonObject.getJSONArray("Result");

                    if (propertySize.length() == 0) {
//                  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < propertySize.length(); index++) {
                            JSONObject jsonObject1 = propertySize.getJSONObject(index);

                            PropertyUnitsModel propertyUnitsModel = new PropertyUnitsModel();

                            propertyUnitsModel.setId(jsonObject1.getString("Id"));
                            propertyUnitsModel.setSizeUnitId(jsonObject1.getString("SizeUnit"));
                            propertyUnitsModels.add(propertyUnitsModel);
                            property_units_id.clear();
                            for (int i = 0; i < propertyUnitsModels.size(); i++) {
                                property_units_id.add(propertyUnitsModels.get(i).getSizeUnitId());
                            }

//                            adapter1.addAll(property_units_id);
//                            adapter1.add(getString(R.string.select_type));
                            //     pentaHouseDuplexAdapter.notifyDataSetChanged();

                        }

                        rowHouseAdapter = new RowHouseAdapter(getContext(), arrayList, listUnit, Pentahouse_Fragment.this, propertyUnitsModels);
                        rowhouse_recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
                        rowhouse_recycler_view.setAdapter(rowHouseAdapter);
                        rowHouseAdapter.notifyDataSetChanged();

                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }


    public void deletePropertyImages(String position) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT,
                deletephotoInput(position),
                "Loading..",
                this,
                Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT,
                Constants.POST).execute();

    }

    private String deletephotoInput(String id) {
        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }


    private void getFloorList() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.FLOOR_LIST_NEIGHBOURBOOD + floorId,
                "",
                "Loading..",
                this,
                Urls.FLOOR_LIST_NEIGHBOURBOOD,
                Constants.GET).execute();

    }

    public class FloornumberAdapter extends RecyclerView.Adapter<FloornumberAdapter.ViewHolder> {

        Context context;
        ArrayList<FloorCalculationModel> floorNumberList = new ArrayList<>();

        String number_count;
        private String select_unit_txt, select_unit_id;

        public FloornumberAdapter(Context context, ArrayList<FloorCalculationModel> floorNumberList) {
            this.context = context;
            this.floorNumberList = floorNumberList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.floor_layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.floor_minus.setTag(position);
            holder.floor_number.setText(String.valueOf(floorNumberList.get(position).getFloorNumber()));
            holder.floor_size.setText(String.valueOf(floorNumberList.get(position).getFloorSize()));
            if (floorNumberList.size() > 1) {

                holder.floor_minus.setVisibility(View.VISIBLE);
            } else {

                holder.floor_minus.setVisibility(View.GONE);
            }

            holder.select_floor_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    // TODO Auto-generated method stub

                    if (holder.select_floor_unit.getSelectedItem() == getActivity().getString(R.string.select_type)) {

                        //Do nothing.
                    } else {

                        select_unit_txt = holder.select_floor_unit.getSelectedItem().toString();
                        select_unit_id = propertyUnitsModels.get(position).getId();

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

            holder.select_floor_unit.setAdapter(adapter1);

            holder.floor_size.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    floorNumber = Integer.parseInt(holder.floor_number.getText().toString());
                    floorSize = Double.parseDouble(holder.floor_size.getText().toString());
//                    et_floorno.setText(holder.floor_number.getText().toString());
//                    et_floorsize.setText(holder.floor_size.getText().toString());
                    selectedFloorId = floorNumberList.get(position).getId();
                    update_more_floor.setVisibility(View.VISIBLE);
                }
            });
            holder.floor_number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    floorNumber = Integer.parseInt(holder.floor_number.getText().toString());
                    floorSize = Double.parseDouble(holder.floor_size.getText().toString());
                    selectedFloorId = floorNumberList.get(position).getId();
                    update_more_floor.setVisibility(View.VISIBLE);
                }
            });

            holder.floor_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                   /* int pos = position;
                                    FloorCalculationModel floorCalculationModel = new FloorCalculationModel();
                                    floorCalculationModel.setDeletePos(pos);
                                    floorCalculationModel.setDelete("Yes");
                                    EventBus.getDefault().post(floorCalculationModel);*/
                                    floorNumberList.remove(position);
                                    notifyDataSetChanged();
                                    flooradapter.notifyDataSetChanged();
//                                    removeFloor(floorNumberList.get(position).getId());

                                }
                            })
                            .show();


                }
            });
        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView floor_size, floor_number;
            ImageView floor_minus;
            Spinner select_floor_unit;

            public ViewHolder(View itemView) {
                super(itemView);
                floor_size = (TextView) itemView.findViewById(R.id.floor_size);
                floor_minus = (ImageView) itemView.findViewById(R.id.floor_minus);
                floor_number = (TextView) itemView.findViewById(R.id.floor_number);
                select_floor_unit = (Spinner) itemView.findViewById(R.id.select_floor_unit);

            }


        }
    }

    private void removeFloor(int id) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REMOVE_FLOOR,
                getRemoveFloor(id),
                "Loading..",
                this,
                Urls.URL_REMOVE_FLOOR,
                Constants.POST).execute();

    }

    private String getRemoveFloor(int id) {
        String input = "";


        JSONObject jsonObjectRoom = new JSONObject();
        // JSONArray jsonArray1 = new JSONArray();
        try {
            jsonObjectRoom.put("Id", id);


            input = jsonObjectRoom.toString();

        } catch (Exception e) {

            e.printStackTrace();
        }
        return input;

    }

    public class Floor extends RecyclerView.Adapter<Floor.ViewHolder> {
        ArrayList<FloorCalculationModel> floorNumberList;
        int selected_position = -1;
        private int row_index = -1;

        public Floor(ArrayList<FloorCalculationModel> floorNumberList) {

            this.floorNumberList = floorNumberList;

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.floor_bedroom_inflate_view, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.number.setText(floorNumberList.get(position).getCount());

            holder.number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    row_index = position;
                    floor_id = floorNumberList.get(position).getId();
                    getBedroomList();
                    getBalconyList();
//                    ll_detail.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();
                }
            });

            if (row_index == position) {
                holder.number.setTextColor(Color.parseColor("#2C8BCE"));
            } else {
                holder.number.setTextColor(Color.parseColor("#727272"));

            }

            holder.no_room_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    floor_id = floorNumberList.get(position).getId();
                    selected_position = position;
                    notifyDataSetChanged();

                }
            });

        }


        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView number;
            LinearLayout no_room_layout;

            public ViewHolder(View itemView) {
                super(itemView);

                number = (TextView) itemView.findViewById(R.id.number);
                no_room_layout = (LinearLayout) itemView.findViewById(R.id.no_room_layout);

            }

        }
    }

    private void getBalconyList() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ALL_BEDROOM_PROJECT + "/" + floorId + "/" + floor_id + "/balcony",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_ALL_BEDROOM_PROJECT,
                Constants.GET).execute();

    }

    private void getBedroomList() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ALL_BEDROOM_PROJECT + "/" + floorId + "/" + floor_id + "/room",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_ALL_BEDROOM_PROJECT,
                Constants.GET).execute();

    }

    public class RowHouseAdapter extends RecyclerView.Adapter<RowHouseAdapter.ViewHolder> {

        Context context;
        ArrayList<ResidentialFloorModel> residentialFloorModels = new ArrayList<>();
        LinearLayout layoutZoingImages, lnrEmbededVideo, lnrAddEmbdedVideo;
        EditText edtAddMore;
        ImageView imgCloseAddMore;
        int id = 0;
        Pentahouse_Fragment rowHouse_fragment;
        ArrayList<String> embedVideo1 = new ArrayList<>();
        ArrayList<String> listUnit = new ArrayList<>();

        String strTower1;
        String strTower2;
        String strTower3;
        private ArrayList<FloorUnitModel> plotlotUnitModels = new ArrayList<>();
        private PlotLotUnitAdapter plotLotUnitAdapter;
        int minVal = 0;
        private int range;
        SpinnerAdapter adapter1;
        private String edit_property;
        ArrayList<String> property_units_id = new ArrayList<>();
        ArrayList<PropertyUnitsModel> propertyUnitsModels = new ArrayList<>();
        private String floorSizeText, floorSizeUnitId;


        public RowHouseAdapter(Context context, ArrayList<ResidentialFloorModel> residentialFloorModels, ArrayList<String> listUnit, Pentahouse_Fragment pentahouse_fragment, ArrayList<PropertyUnitsModel> propertyUnitsModels) {
            this.context = context;
            this.residentialFloorModels = residentialFloorModels;
            this.listUnit = listUnit;
            this.rowHouse_fragment = rowHouse_fragment;
            this.propertyUnitsModels = propertyUnitsModels;
            property_units_id.clear();

            for (int i = 0; i < this.propertyUnitsModels.size(); i++) {
                property_units_id.add(this.propertyUnitsModels.get(i).getSizeUnitId());
            }

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.residential_floor_layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            embededVideoUrls.clear();
            embededVideoUrls.add("");
            holder.rv_embeded_video.setLayoutManager(new LinearLayoutManager(context));
            final EmbededVideoAdapter embededVideoAdapter = new EmbededVideoAdapter(context, embededVideoUrls);
            holder.rv_embeded_video.setAdapter(embededVideoAdapter);
            holder.lnrAddEmbdedVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (embededVideoUrls.contains("")) {

                        Toast.makeText(context, "Please add embed youtube/vimeo code", Toast.LENGTH_SHORT).show();

                    } else {

                        embededVideoUrls.add("");
                        embededVideoAdapter.notifyDataSetChanged();
                    }

                }
            });

            if (residentialFloorModels.get(position).getId() != null) {

                holder.ll_unit_dependent.setVisibility(View.VISIBLE);
            } else {
                holder.ll_unit_dependent.setVisibility(View.GONE);
            }

            holder.street.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    DialogModel dialogModel = new DialogModel();
                    dialogModel.setUnit_to(holder.unit_to.getText().toString());
                    dialogModel.setUnit_from(holder.unit_from.getText().toString());
                    dialogModel.setName(holder.street.getText().toString());
                    dialogModel.setPosSpinner(holder.selectSQFT.getSelectedItemPosition());
                    if (!holder.area.getText().toString().equalsIgnoreCase(""))
                        dialogModel.setArea(Float.parseFloat(holder.area.getText().toString()));

                    EventBus.getDefault().post(dialogModel);

                }
            });

            holder.unit_to.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                            && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                        if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                                || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                            plotlotUnitModels.clear();

                            plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                            holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
//                            holder.ll_unit_dependent.setVisibility(View.GONE);
                            holder.ll_unit.setVisibility(View.GONE);


                        } else {

                            if (Integer.parseInt(holder.unit_to.getText().toString())

                                    >= Integer.parseInt(holder.unit_from.getText().toString())) {

                                minVal = Integer.parseInt(holder.unit_from.getText().toString());

                                range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                        Integer.parseInt(holder.unit_from.getText().toString());
                                plotlotUnitModels.clear();

                                if (range >= 0) {
                                    for (int i = 0; i <= range; i++) {
                                        FloorUnitModel floorUnitModel = new FloorUnitModel();
                                        floorUnitModel.setValue(minVal + i);
                                        plotlotUnitModels.add(floorUnitModel);
                                    }

                                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                                    holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
//                                    holder.ll_unit_dependent.setVisibility(View.VISIBLE);
                                    holder.ll_unit.setVisibility(View.VISIBLE);

                                }
                            } else {

                                Toast.makeText(context, "Unit To value must be greater than Unit From.", Toast.LENGTH_LONG).show();

                            }

                        }


                    } else {
                        plotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                        holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                        holder.ll_unit_dependent.setVisibility(View.GONE);
                        holder.ll_unit.setVisibility(View.GONE);

                    }

                }
            });

            holder.unit_from.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (!holder.unit_to.getText().toString().equalsIgnoreCase("")
                            && !holder.unit_from.getText().toString().equalsIgnoreCase("")) {

                        if (holder.unit_from.getText().toString().equalsIgnoreCase("")
                                || holder.unit_to.getText().toString().equalsIgnoreCase("")) {
                            plotlotUnitModels.clear();

                            plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                            holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                            holder.ll_unit_dependent.setVisibility(View.GONE);
                            holder.ll_unit.setVisibility(View.GONE);


                        } else {

                            if (Integer.parseInt(holder.unit_to.getText().toString())

                                    >= Integer.parseInt(holder.unit_from.getText().toString())) {

                                minVal = Integer.parseInt(holder.unit_from.getText().toString());

                                range = Integer.parseInt(holder.unit_to.getText().toString()) -
                                        Integer.parseInt(holder.unit_from.getText().toString());
                                plotlotUnitModels.clear();

                                if (range >= 0) {
                                    for (int i = 0; i <= range; i++) {
                                        FloorUnitModel floorUnitModel = new FloorUnitModel();
                                        floorUnitModel.setValue(minVal + i);
                                        plotlotUnitModels.add(floorUnitModel);
                                    }

                                    plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                                    holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                                    holder.ll_unit_dependent.setVisibility(View.VISIBLE);
                                    holder.ll_unit.setVisibility(View.VISIBLE);

                                }
                            } else {

                                Toast.makeText(context, "Unit To value must be greater than Unit From.", Toast.LENGTH_LONG).show();

                            }

                        }


                    } else {

                        plotlotUnitModels.clear();

                        plotLotUnitAdapter = new PlotLotUnitAdapter(context, plotlotUnitModels);
                        holder.rv_rowhouse_unit.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        holder.rv_rowhouse_unit.setAdapter(plotLotUnitAdapter);
                        holder.ll_unit_dependent.setVisibility(View.GONE);
                        holder.ll_unit.setVisibility(View.GONE);

                    }
                }
            });

            final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                    context, R.layout.custom_select_spinner, listUnit) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        // Disable the first item from Spinner
                        // First item will be use for hint
                        return false;
                    } else {
                        return true;
                    }
                }

                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the hint text color gray

                        tv.setTextColor(context.getResources().getColor(R.color.black));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //   tv.setPadding(10, 5, 10, 5);
                        tv.setLayoutParams(params);
                        // tv.setBackgroundColor(getResources().getColor(R.color.black));

                    } else {
                        tv.setTextColor(context.getResources().getColor(R.color.black));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //   tv.setPadding(10, 5, 10, 5);
                        tv.setLayoutParams(params);
                        // tv.setBackgroundColor(getResources().getColor(R.color.black));
                    }
                    return view;
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        // Set the hint text color gray
                        tv.setTextColor(context.getResources().getColor(R.color.black));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //   tv.setPadding(10, 5, 10, 5);
                        tv.setLayoutParams(params);

                    } else {
                        tv.setTextColor(context.getResources().getColor(R.color.black));
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        //   tv.setPadding(10, 5, 10, 5);
                        tv.setLayoutParams(params);

                    }
                    return view;
                }
            };

            spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

            spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

            if (first) {
                holder.spnrTotalArea.setAdapter(spinnerArrayMs);
                holder.selectSQFT.setAdapter(spinnerArrayMs);
                holder.snrCoverArea.setAdapter(spinnerArrayMs);
                holder.spnrSuperArea.setAdapter(spinnerArrayMs);
                holder.spnrBuildArea.setAdapter(spinnerArrayMs);
                holder.spnrCarpetArea.setAdapter(spinnerArrayMs);


                first = false;

            }


            embedVideo1.clear();
            embedVideo1.add("");
            addEmbededVideo(embedVideo1);

            adapter1 = new SpinnerAdapter(context, R.layout.new_spinner_item);
            adapter1.addAll(property_units_id);
            adapter1.add(context.getString(R.string.select_type));
            holder.spinner_select_floor.setAdapter(adapter1);

            holder.spinner_select_floor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    // TODO Auto-generated method stub

                    if (holder.spinner_select_floor.getSelectedItem() == context.getString(R.string.select_type)) {

                        //Do nothing.
                    } else {

                        floorSizeText = holder.spinner_select_floor.getSelectedItem().toString();
                        floorSizeUnitId = propertyUnitsModels.get(position).getId();

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

            holder.upload_pics.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        holder.add_layout.setVisibility(View.VISIBLE);

                    } else
                        holder.add_layout.setVisibility(View.GONE);
                }
            });

            holder.other_info.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        holder.other_info_lay.setVisibility(View.VISIBLE);
                    else
                        holder.other_info_lay.setVisibility(View.GONE);
                }

            });

            holder.uploadZoingImages.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    selectedRowHousePos = position;
                    checkRunTimePermission();

                }
            });

            holder.dec_floor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flagTower1 > 0)
                        flagTower1--;
                    strTower1 = "TotalRooms%20eq%20" + flagTower1 + "%20and%20";
                    holder.floor_count.setText(String.valueOf(flagTower1));

                }
            });

            holder.inc_floor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    flagTower1++;
                    strTower1 = "TotalRooms%20eq%20" + flagTower1 + "%20and%20";
                    holder.floor_count.setText(String.valueOf(flagTower1));
                }
            });

            holder.dec_bedroom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flagTower2 > 0)
                        flagTower2--;
                    strTower2 = "TotalRooms%20eq%20" + flagTower2 + "%20and%20";
                    holder.bedroom_count.setText(String.valueOf(flagTower2));

                }
            });

            holder.inc_bedroom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    flagTower2++;
                    strTower2 = "TotalRooms%20eq%20" + flagTower2 + "%20and%20";
                    holder.bedroom_count.setText(String.valueOf(flagTower2));
                }
            });

            holder.dec_carParking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flagTower3 > 0)
                        flagTower3--;
                    strTower3 = "TotalRooms%20eq%20" + flagTower3 + "%20and%20";
                    holder.carParking_count.setText(String.valueOf(flagTower3));

                }
            });

            holder.inc_carParking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    flagTower3++;
                    strTower3 = "TotalRooms%20eq%20" + flagTower3 + "%20and%20";
                    holder.carParking_count.setText(String.valueOf(flagTower3));
                }
            });

            holder.add_more_floor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                floornumberAdapter.notifyDataSetChanged();
//                flooradapter.notifyDataSetChanged();
                    if (holder.et_floorno.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(context, "Please add floor number", Toast.LENGTH_SHORT).show();
                    } else if (holder.et_floorsize.getText().toString().equalsIgnoreCase("")) {

                        Toast.makeText(context, "Please add floor Size", Toast.LENGTH_SHORT).show();

                    } else {
//                        FloorCalculationModel floorCalculationModel1 = new FloorCalculationModel();
//                        floorCalculationModel1.setCount(holder.et_floorno.getText().toString());
//                        floorCalculationModel1.setFloorNumber(Integer.parseInt(holder.et_floorno.getText().toString()));
//                        floorCalculationModel1.setFloorSize(Double.parseDouble(holder.et_floorsize.getText().toString()));
//                        floorNumberList.add(floorCalculationModel1);

                        addFloor();
                        holder.et_floorno.setText("");
                        holder.et_floorsize.setText("");

                    }

//                    if(room_list.)


//                    addFloor();


                }
            });

            holder.add_more_room.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addRoomDetaildialog();
                    AccommodationId = Integer.parseInt(arrayList.get(position).getId());
                    type = "room";
                    selectedRowHousePos = position;
//                    floorId = jsonResult.getJSONObject("Result").getInt("Id");

//                    getBedroomList();

                }
            });

            holder.add_more_balcony.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AccommodationId = Integer.parseInt(arrayList.get(position).getId());
                    addBalconyDetaildialog();
                    type = "balcony";
                    selectedRowHousePos = position;
//                    Toast.makeText(context, "Add More Balcony", Toast.LENGTH_SHORT).show();
                }
            });

            flooradapter = new Floor(floorNumberList);
            final LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity());
            layoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
            holder.floor_number_list.setLayoutManager(layoutManager1);
            holder.floor_number_list.setAdapter(flooradapter);

            floornumberAdapter = new FloornumberAdapter(getActivity(), floorNumberList);
            final LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
            layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            holder.rv_floorlist.setLayoutManager(layoutManager2);
            holder.rv_floorlist.setAdapter(floornumberAdapter);

            ArrayList<PropertyBedroomModel> room_lists = new ArrayList<>();
            ArrayList<PropertyBedroomModel> balcony_lists = new ArrayList<>();


            for (int i = 0; i < arrayList.get(position).getProperties().get(0).getPropertyBedrooms().size(); i++) {

                if (arrayList.get(position).getProperties().get(0).getPropertyBedrooms().get(i).getCategory().equalsIgnoreCase("room")) {


                    if (position == Integer.valueOf(arrayList.get(position).getProperties().get(0).getPropertyBedrooms().get(i).getId()))
                        room_lists.add(arrayList.get(position).getProperties().get(0).getPropertyBedrooms().get(i));

                } else {
                    if (position == Integer.valueOf(arrayList.get(position).getProperties().get(0).getPropertyBedrooms().get(i).getId()))
                        balcony_lists.add(arrayList.get(position).getProperties().get(0).getPropertyBedrooms().get(i));
                }
            }

            final LinearLayoutManager layoutManager3 = new LinearLayoutManager(getActivity());
            RoomListAdapter roomListAdapter = new RoomListAdapter(getActivity(), room_lists, holder);
            holder.room_detail_list.setLayoutManager(layoutManager3);
            holder.room_detail_list.setAdapter(roomListAdapter);

            BalconyListAdapter balconyListAdapter = new BalconyListAdapter(getActivity(), balcony_lists, holder);
            holder.balcony_detail_list.setLayoutManager(new LinearLayoutManager(getActivity()));
            holder.balcony_detail_list.setAdapter(balconyListAdapter);
//            AccomoFloorRoomsAdapter  accomoFloorRoomsAdapter = new AccomoFloorRoomsAdapter(context, arrayList.get(position).getProperties().get(0).getPropertyBedrooms());
//            holder.room_detail_list.setLayoutManager(new LinearLayoutManager(getActivity()));
//            holder.room_detail_list.setAdapter(accomoFloorRoomsAdapter);

            ArrayList<ResidentialFloorModel.Properties.PhotosModel> photosModelsArray = new ArrayList<>();

            for (int i = 0; i < arrayList.get(position).getProperties().get(0).getPhotosModels().size(); i++) {

                if (arrayList.get(position).getProperties().get(0).getPhotosModels().get(i).getSelectedId() == position) {

                    photosModelsArray.add(arrayList.get(position).getProperties().get(0).getPhotosModels().get(i));

                }

            }
            final LinearLayoutManager layoutManager4 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

            ImagesAdapter imagesAdapter = new ImagesAdapter(photosModelsArray, holder);
            holder.rv_images.setLayoutManager(layoutManager4);
            holder.rv_images.setAdapter(imagesAdapter);


//            rowHouseAdapter.imageinflateview(arrayList.get(position).getProperties().get(0).getPhotosModels(), holder);

        }


        @Override
        public int getItemCount() {
            return residentialFloorModels.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CheckBox upload_pics, other_info;
            LinearLayout other_info_lay, add_layout, uploadZoingImages, ll_unit, ll_unit_dependent, ll_detail;
            Spinner spnrTotalArea, selectSQFT, snrCoverArea, spnrSuperArea, spnrBuildArea, spnrCarpetArea;
            ImageView dec_floor, inc_floor, dec_bedroom, inc_bedroom, dec_carParking, inc_carParking;
            TextView floor_count, bedroom_count, carParking_count;
            RecyclerView rv_rowhouse_unit, rv_floorlist, floor_number_list, balcony_detail_list, room_detail_list;
            EditText unit_from, unit_to, street, area;
            Spinner spinner_select_floor;
            TextView add_more_floor, et_floorno, et_floorsize, add_more_room, add_more_balcony;
            LinearLayout ClusterImg;
            RecyclerView rv_embeded_video, rv_images;
            LinearLayout lnrAddEmbdedVideo;

            public ViewHolder(View itemView) {
                super(itemView);
                rv_embeded_video = (RecyclerView) itemView.findViewById(R.id.rv_embeded_video);
                rv_images = (RecyclerView) itemView.findViewById(R.id.rv_images);

                lnrAddEmbdedVideo = (LinearLayout) itemView.findViewById(R.id.lnrAddEmbdedVideo);

                upload_pics = (CheckBox) itemView.findViewById(R.id.upload_pics);
                other_info = (CheckBox) itemView.findViewById(R.id.other_info);
                other_info_lay = (LinearLayout) itemView.findViewById(R.id.other_info_lay);
                add_layout = (LinearLayout) itemView.findViewById(R.id.add_layout);
                uploadZoingImages = (LinearLayout) itemView.findViewById(R.id.uploadZoingImages);
                layoutZoingImages = (LinearLayout) itemView.findViewById(R.id.layoutZoingImages);
                lnrEmbededVideo = (LinearLayout) itemView.findViewById(R.id.lnrEmbededVideo);
                lnrAddEmbdedVideo = (LinearLayout) itemView.findViewById(R.id.lnrAddEmbdedVideo);
                spnrTotalArea = (Spinner) itemView.findViewById(R.id.spnrTotalArea);
                selectSQFT = (Spinner) itemView.findViewById(R.id.selectSQFT);
                snrCoverArea = (Spinner) itemView.findViewById(R.id.snrCoverArea);
                spnrSuperArea = (Spinner) itemView.findViewById(R.id.spnrSuperArea);
                spnrBuildArea = (Spinner) itemView.findViewById(R.id.spnrBuildArea);
                spnrCarpetArea = (Spinner) itemView.findViewById(R.id.spnrCarpetArea);
                dec_floor = (ImageView) itemView.findViewById(R.id.dec_floor);
                inc_floor = (ImageView) itemView.findViewById(R.id.inc_floor);
                dec_bedroom = (ImageView) itemView.findViewById(R.id.dec_bedroom);
                inc_bedroom = (ImageView) itemView.findViewById(R.id.inc_bedroom);
                dec_carParking = (ImageView) itemView.findViewById(R.id.dec_carParking);
                inc_carParking = (ImageView) itemView.findViewById(R.id.inc_carParking);
                floor_count = (TextView) itemView.findViewById(R.id.floor_count);
                bedroom_count = (TextView) itemView.findViewById(R.id.bedroom_count);
                carParking_count = (TextView) itemView.findViewById(R.id.carParking_count);
                add_more_room = (TextView) itemView.findViewById(R.id.add_more);
                add_more_balcony = (TextView) itemView.findViewById(R.id.add_more_balcony);
                rv_rowhouse_unit = (RecyclerView) itemView.findViewById(R.id.rv_rowhouse_unit);
                rv_floorlist = (RecyclerView) itemView.findViewById(R.id.rv_floorlist);

                unit_from = (EditText) itemView.findViewById(R.id.unit_from);
                unit_to = (EditText) itemView.findViewById(R.id.unit_to);
                spinner_select_floor = (Spinner) itemView.findViewById(R.id.spinner_select_floor);
                add_more_floor = (TextView) itemView.findViewById(R.id.add_more_floor);
                et_floorno = (EditText) itemView.findViewById(R.id.et_floorno);
                et_floorsize = (EditText) itemView.findViewById(R.id.et_floorsize);
                floor_number_list = (RecyclerView) itemView.findViewById(R.id.floor_number_list);
                rv_floor = (RecyclerView) itemView.findViewById(R.id.rv_floor);
                rv_floorlist = (RecyclerView) itemView.findViewById(R.id.rv_floorlist);
                update_more_floor = (TextView) itemView.findViewById(R.id.update_more_floor);
                ll_detail = (LinearLayout) itemView.findViewById(R.id.ll_detail);
                room_detail_list = (RecyclerView) itemView.findViewById(R.id.room_detail_list);
                balcony_detail_list = (RecyclerView) itemView.findViewById(R.id.balcony_detail_list);
                ll_unit = (LinearLayout) itemView.findViewById(R.id.ll_unit);
                ll_unit_dependent = (LinearLayout) itemView.findViewById(R.id.ll_unit_dependent);
                ll_detail = (LinearLayout) itemView.findViewById(R.id.ll_detail);
                street = (EditText) itemView.findViewById(R.id.street);
                area = (EditText) itemView.findViewById(R.id.area);
                et_floorno = (EditText) itemView.findViewById(R.id.et_floorno);
                et_floorsize = (EditText) itemView.findViewById(R.id.et_floorsize);
                ClusterImg = (LinearLayout) itemView.findViewById(R.id.layoutZoingImages);

            }


        }


        public void addEmbededVideo(final ArrayList<String> list) {

            lnrEmbededVideo.removeAllViews();
            for (int i = 0; i < list.size(); i++) {

                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                final View view = inflater.inflate(R.layout.custom_add_more, null);
                edtAddMore = (EditText) view.findViewById(R.id.edtAddMore);
                edtAddMore.setText(list.get(i));

                imgCloseAddMore = (ImageView) view.findViewById(R.id.imgCloseAddMore);
                imgCloseAddMore.setId(i);

                edtAddMore.setId(i);

                if (i == 0) {
                    imgCloseAddMore.setVisibility(View.GONE);
                    if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {
                        edtAddMore.setText(list.get(i));
                    } else
                        edtAddMore.setHint(context.getResources().getString(R.string.add_embed_youtube));
                }

                final int finalI = i;

                imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int id1 = finalI;

                        EditText t = (EditText) view.findViewById(id1);
                        String text = t.getText().toString();

                        if (!text.equalsIgnoreCase("")) {
                            if (!text.equalsIgnoreCase(t.getText().toString())) {
                                list.add(list.size() - 1, text);
                            }
                        }


                        int id = finalI;
                        list.remove(id);

                        addEmbededVideo(list);
                    }
                });

                lnrEmbededVideo.addView(view);
            }

/*
            lnrAddEmbdedVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {

                        if (!list.get(list.size() - 1).equalsIgnoreCase("")) {

                            list.add("");

                        } else {
                            list.add(list.size() - 1, edtAddMore.getText().toString());
                        }

                        addEmbededVideo(list);
                    } else {
                        Utils.showErrorMessage("Please add url", context);
                    }
                }
            });
*/

        }

    }

    public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

        Context context;
        ArrayList<ResidentialFloorModel.Properties.PhotosModel> photosModelsArray = new ArrayList<>();
        RowHouseAdapter.ViewHolder vh;


        public ImagesAdapter(final ArrayList<ResidentialFloorModel.Properties.PhotosModel> photosModelsArray, final RowHouseAdapter.ViewHolder vh) {
            this.context = getActivity();
            this.photosModelsArray = photosModelsArray;
            this.vh = vh;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.image_layout_inflate, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModelsArray.get(position).getImageUrl(), holder.imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModelsArray.get(position).getImageUrl());

            holder.cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    for (int i = 0; i < arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPhotosModels().size(); i++) {

                                        if (arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPhotosModels().get(i).getRandomId()
                                                .equalsIgnoreCase(photosModelsArray.get(position).getRandomId())) {
                                            arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPhotosModels().remove(i);
                                            deletePropertyImages(photosModelsArray.get(position).getId());
                                            for (int j = 0; j < photosModels.size(); j++) {


                                                if (photosModels.get(j).getRandomId().equalsIgnoreCase(photosModelsArray.get(position).getRandomId())) {
                                                    photosModels.remove(j);
                                                }
                                            }
                                            rowHouseAdapter.notifyDataSetChanged();
                                            return;
                                        }


                                    }


//                                        imageinflateview(photosModels);
                                }
                            })
                            .show();

                    //   Toast.makeText(context, "cancel is clicked", Toast.LENGTH_SHORT).show();

                }
            });


        }

        @Override
        public int getItemCount() {
            return photosModelsArray.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ImageView imageView;
            ImageView cross;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView = (ImageView) itemView.findViewById(R.id.image_view);
                cross = (ImageView) itemView.findViewById(R.id.cross);

            }

        }
    }

    public class EmbededVideoAdapter extends RecyclerView.Adapter<EmbededVideoAdapter.ViewHolder> {

        Context context;
        ArrayList<String> floorNumberList = new ArrayList<>();

        public EmbededVideoAdapter(Context context, ArrayList<String> accomodationList) {
            this.context = context;
            this.floorNumberList = accomodationList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_accomodation_embeded_video, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.et_vidoes.setText(embededVideoUrls.get(position));

            holder.et_vidoes.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    embededVideoUrls.remove(position);
                    embededVideoUrls.add(position, holder.et_vidoes.getText().toString());
                    //   notifyDataSetChanged();

                }
            });

            if (position == 0) {

                holder.imgCloseAddMore.setVisibility(View.GONE);
            } else {

                holder.imgCloseAddMore.setVisibility(View.VISIBLE);

            }

            holder.imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String text = holder.et_vidoes.getText().toString();

                    embededVideoUrls.remove(position);
                    notifyDataSetChanged();


                }
            });

        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            EditText et_vidoes;
            ImageView imgCloseAddMore;

            public ViewHolder(View itemView) {
                super(itemView);
                et_vidoes = (EditText) itemView.findViewById(R.id.et_vidoes);
                imgCloseAddMore = (ImageView) itemView.findViewById(R.id.imgCloseAddMore);

            }

        }
    }

    private void addFloor() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_FLOOR_PROJECT,
                getPropertyFloor(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_FLOOR_PROJECT,
                Constants.POST).execute();

    }

    private String getPropertyFloor() {
        String input = "";


        JSONObject jsonObjectRoom = new JSONObject();
        // JSONArray jsonArray1 = new JSONArray();
        try {
            //  jsonObject1.put("PropertyId", pid);

            jsonObjectRoom.put("Id", 0);

            jsonObjectRoom.put("ProjectNeighborhoodId", floorId);
            jsonObjectRoom.put("FloorNumber", "123");
            jsonObjectRoom.put("FloorSize", "12344");
            jsonObjectRoom.put("FloorSizeUnitId", 1);

            input = jsonObjectRoom.toString();

        } catch (Exception e) {

            e.printStackTrace();
        }


        return input;

    }

    /*
     * room Add
     * */

    public void addRoomDetaildialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.room_detail_popup);
        TextView room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        final Spinner room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        final Spinner select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        final Spinner select_unit_room = (Spinner) dialog.findViewById(R.id.select_unit_room);

        RecyclerView room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        LinearLayout attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        Button save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));

        String[] countries = getResources().getStringArray(R.array.RoomTypes);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, countries);

        room_type.setAdapter(adapter);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        room_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                same = false;
//
//            }
//        });

//        room_name.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                roomName = room_name.getText().toString().trim();
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

/*
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!room_name.getText().toString().equalsIgnoreCase("") && !room_size.getText().toString().equalsIgnoreCase("")) {

                    if (!add_room.equalsIgnoreCase("")) {
                        postAddRoom();
                    } else {
                        editRoom();
                    }
                    dialog.dismiss();
//

                } else {
                    if (category.equalsIgnoreCase("balcony")) {
                        Utils.showWarningErrorMessage("Warning", "Please fill Balcony Name and Room size", "Ok", getActivity());
                    } else {
                        Utils.showWarningErrorMessage("Warning", "Please fill Room Name and Room size", "Ok", getActivity());

                    }

                }
            }
        });
*/
        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add


                if (room_name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter room name", Toast.LENGTH_SHORT).show();
                } else if (length.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter length", Toast.LENGTH_SHORT).show();
                } else if (width.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter width", Toast.LENGTH_SHORT).show();
                } else {
                    type = "room";
                    addRoomApi();


                    PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();

                    propertyBedroomModel.setId(String.valueOf(selectedRowHousePos));

                    propertyBedroomModel.setRoom_name(room_name.getText().toString());
                    propertyBedroomModel.setGoundtype("");
                    propertyBedroomModel.setRoom_type(new Random().toString());
                    propertyBedroomModel.setRoom_size(room_size.getText().toString());
                    propertyBedroomModel.setLength(length.getText().toString());
                    propertyBedroomModel.setWidth(width.getText().toString());
                    propertyBedroomModel.setBedRoomSizeUnitId(String.valueOf(room_type.getSelectedItemPosition()));
                    propertyBedroomModel.setLengthWidthUnitId(String.valueOf(select_unit_room.getSelectedItemPosition()));
                    propertyBedroomModel.setSizeUnit(String.valueOf(select_unit.getSelectedItemPosition()));
                    propertyBedroomModel.setCategory("room");
                    room_list.add(propertyBedroomModel);


                    ResidentialFloorModel residentialFloorModel = new ResidentialFloorModel();

                    residentialFloorModel.setProjectCategoryTypeId(arrayList.get(selectedRowHousePos).getProjectCategoryTypeId());
                    residentialFloorModel.setNeighborhoodName(arrayList.get(selectedRowHousePos).getNeighborhoodName());
                    residentialFloorModel.setArea("");
                    residentialFloorModel.setUnitFrom("000");
                    residentialFloorModel.setUnitTo("000");
                    residentialFloorModel.setId(arrayList.get(0).getId());


                    ArrayList<ResidentialFloorModel.Properties> propertiesArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties properties = new ResidentialFloorModel.Properties();
                    properties.setPropertyTypeId("2");
                    properties.setCompanyId("1");
                    properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setNoOfBedrooms("0");
                    properties.setNoOfFloors("0");
                    properties.setNoOfParkings("0");

//                        ArrayList<ResidentialFloorModel.PropertyBedrooms> propertyBedrooms = new ArrayList<>();
//                        ResidentialFloorModel.PropertyBedrooms propertyBedrooms1 = new ResidentialFloorModel.PropertyBedrooms();
//                        propertyBedrooms1.setBedRoomName("");
//                        propertyBedrooms1.setCategory("room");


                    ArrayList<ResidentialFloorModel.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties.PropertyAreas propertyAreas = new ResidentialFloorModel.Properties.PropertyAreas();
                    propertyAreas.setFrontSize("");
                    propertyAreas.setDepthSize("");
                    propertyAreasArrayList.add(propertyAreas);
                    properties.setPropertyBedrooms(room_list);
                    properties.setPhotosModels(photosModels);
                    properties.setPropertyAreas(propertyAreasArrayList);
                    propertiesArrayList.add(properties);

                    residentialFloorModel.setProperties(propertiesArrayList);


                    arrayList.set(selectedRowHousePos, residentialFloorModel);

                    rowHouseAdapter.notifyDataSetChanged();
                    dialog.cancel();
                }


            }
        });

        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        select_unit_room.setAdapter(adapter1);
        select_unit_room.setSelection(adapter1.getCount());
       /* room_type.setAdapter(adapter);
        room_type.setSelection(adapter.getCount());*/
        dialog.show();
//
    }

    private void EditRoomDetaildialog(final PropertyBedroomModel room_dataArray, final RowHouseAdapter.ViewHolder vh) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.room_detail_popup);
        TextView room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        final Spinner room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        final Spinner select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        final Spinner select_unit_room = (Spinner) dialog.findViewById(R.id.select_unit_room);

        RecyclerView room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        LinearLayout attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        Button save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));

        room_name.setText(room_dataArray.getRoom_name());
        length.setText(Utils.getFilteredValue(room_dataArray.getLength()));
        width.setText(Utils.getFilteredValue(room_dataArray.getWidth()));
        room_size.setText(Utils.getFilteredValue(room_dataArray.getRoom_size()));



//        room_data.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
//        if (category.equals("room")) {
//            room_balcony_txt.setText("Room Name*");
//            lnrRoomType.setVisibility(View.VISIBLE);
//        } else {
//            room_balcony_txt.setText("Balcony Name*");
//            lnrRoomType.setVisibility(View.GONE);
//        }
//        if (!room_list.isEmpty()) {
//            attached_layout.setVisibility(View.VISIBLE);
//        } else {
//            attached_layout.setVisibility(View.GONE);
//        }

        String[] countries = getResources().getStringArray(R.array.RoomTypes);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, countries);

        room_type.setAdapter(adapter);

        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        select_unit_room.setAdapter(adapter1);
        select_unit_room.setSelection(adapter1.getCount());

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        if(!room_dataArray.getBedRoomSizeUnitId().equalsIgnoreCase("")){

            room_type.setSelection(Integer.parseInt(room_dataArray.getBedRoomSizeUnitId()));
        }
        if(!room_dataArray.getLengthWidthUnitId().equalsIgnoreCase("")){

            select_unit_room.setSelection(Integer.parseInt(room_dataArray.getLengthWidthUnitId()));
        }
        if(!room_dataArray.getSizeUnit().equalsIgnoreCase("")){

            select_unit.setSelection(Integer.parseInt(room_dataArray.getSizeUnit()));
        }

//        roomAttachedDataAdapter = new RoomAttachedDataAdapter(getActivity(), room_list);
//
//        room_data.setAdapter(roomAttachedDataAdapter);

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        room_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                same = false;
//
//            }
//        });

//        room_name.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                roomName = room_name.getText().toString().trim();
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

/*
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!room_name.getText().toString().equalsIgnoreCase("") && !room_size.getText().toString().equalsIgnoreCase("")) {

                    if (!add_room.equalsIgnoreCase("")) {
                        postAddRoom();
                    } else {
                        editRoom();
                    }
                    dialog.dismiss();
//

                } else {
                    if (category.equalsIgnoreCase("balcony")) {
                        Utils.showWarningErrorMessage("Warning", "Please fill Balcony Name and Room size", "Ok", getActivity());
                    } else {
                        Utils.showWarningErrorMessage("Warning", "Please fill Room Name and Room size", "Ok", getActivity());

                    }

                }
            }
        });
*/
        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add


                if (room_name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter room name", Toast.LENGTH_SHORT).show();
                } else if (length.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter length", Toast.LENGTH_SHORT).show();
                } else if (width.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter width", Toast.LENGTH_SHORT).show();
                } else {
                    type = "room";
                    EditRoomApi();

                    int editpos = 0;
                    for (int i = 0; i < arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().size(); i++) {

                        if (arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().get(i).getRoom_type()
                                .equalsIgnoreCase(room_dataArray.getRoom_type())) {
                            editpos = i;
                        }

                    }

                    PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();

                    propertyBedroomModel.setId(String.valueOf(selectedRowHousePos));

                    propertyBedroomModel.setRoom_name(room_name.getText().toString());
                    propertyBedroomModel.setGoundtype("");
                    propertyBedroomModel.setCategory("room");
                    propertyBedroomModel.setRoom_type(room_dataArray.getRoom_type());
                    propertyBedroomModel.setRoom_size(room_size.getText().toString());
                    propertyBedroomModel.setLength(length.getText().toString());
                    propertyBedroomModel.setWidth(width.getText().toString());
                    propertyBedroomModel.setBedRoomSizeUnitId(String.valueOf(room_type.getSelectedItemPosition()));
                    propertyBedroomModel.setLengthWidthUnitId(String.valueOf(select_unit_room.getSelectedItemPosition()));
                    propertyBedroomModel.setSizeUnit(String.valueOf(select_unit.getSelectedItemPosition()));
                    room_list.set(editpos, propertyBedroomModel);


                    ResidentialFloorModel residentialFloorModel = new ResidentialFloorModel();

                    residentialFloorModel.setProjectCategoryTypeId(arrayList.get(selectedRowHousePos).getProjectCategoryTypeId());
                    residentialFloorModel.setNeighborhoodName(arrayList.get(selectedRowHousePos).getNeighborhoodName());
                    residentialFloorModel.setArea("");
                    residentialFloorModel.setUnitFrom("000");
                    residentialFloorModel.setUnitTo("000");
                    residentialFloorModel.setId(arrayList.get(0).getId());


                    ArrayList<ResidentialFloorModel.Properties> propertiesArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties properties = new ResidentialFloorModel.Properties();
                    properties.setPropertyTypeId("2");
                    properties.setCompanyId("1");
                    properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setNoOfBedrooms("0");
                    properties.setNoOfFloors("0");
                    properties.setNoOfParkings("0");

//                        ArrayList<ResidentialFloorModel.PropertyBedrooms> propertyBedrooms = new ArrayList<>();
//                        ResidentialFloorModel.PropertyBedrooms propertyBedrooms1 = new ResidentialFloorModel.PropertyBedrooms();
//                        propertyBedrooms1.setBedRoomName("");
//                        propertyBedrooms1.setCategory("room");


                    ArrayList<ResidentialFloorModel.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties.PropertyAreas propertyAreas = new ResidentialFloorModel.Properties.PropertyAreas();
                    propertyAreas.setFrontSize("");
                    propertyAreas.setDepthSize("");
                    propertyAreasArrayList.add(propertyAreas);
                    properties.setPropertyBedrooms(room_list);
                    properties.setPhotosModels(photosModels);
                    properties.setPropertyAreas(propertyAreasArrayList);
                    propertiesArrayList.add(properties);

                    residentialFloorModel.setProperties(propertiesArrayList);


                    arrayList.set(vh.getAdapterPosition(), residentialFloorModel);

                    rowHouseAdapter.notifyDataSetChanged();
                }


            }
        });


       /* room_type.setAdapter(adapter);
        room_type.setSelection(adapter.getCount());*/
        dialog.show();
//
    }

    private void addBalconyDetaildialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.balcony_detail_popup);
        TextView room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        final Spinner room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        final Spinner select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        RecyclerView room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        LinearLayout attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        final EditText room_name = (EditText) dialog.findViewById(R.id.room_name);
        Button save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);
        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final Spinner select_unit_room = (Spinner) dialog.findViewById(R.id.select_unit_balcony);

        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        String[] countries = getResources().getStringArray(R.array.RoomTypes);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, countries);

        room_type.setAdapter(adapter);

        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        select_unit_room.setAdapter(adapter1);
        select_unit_room.setSelection(adapter1.getCount());

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//
//                    select_unit_txt = select_unit.getSelectedItem().toString();
//                    select_unit_id = propertyUnitsModels.get(position).getId();
////
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.

                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

//        roomAttachedDataAdapter = new RoomAttachedDataAdapter(getActivity(), room_list);
//
//        room_data.setAdapter(roomAttachedDataAdapter);

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        room_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                same = false;
//
//            }
//        });

//        room_name.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                roomName = room_name.getText().toString().trim();
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

/*
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!room_name.getText().toString().equalsIgnoreCase("") && !room_size.getText().toString().equalsIgnoreCase("")) {

                    if (!add_room.equalsIgnoreCase("")) {
                        postAddRoom();
                    } else {
                        editRoom();
                    }
                    dialog.dismiss();
//

                } else {
                    if (category.equalsIgnoreCase("balcony")) {
                        Utils.showWarningErrorMessage("Warning", "Please fill Balcony Name and Room size", "Ok", getActivity());
                    } else {
                        Utils.showWarningErrorMessage("Warning", "Please fill Room Name and Room size", "Ok", getActivity());

                    }

                }
            }
        });
*/
        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add


                if (room_name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter room name", Toast.LENGTH_SHORT).show();
                } else if (length.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter length", Toast.LENGTH_SHORT).show();
                } else if (width.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter width", Toast.LENGTH_SHORT).show();
                } else {
                    type = "balcony";
                    addRoomApi();

                    PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();
                    propertyBedroomModel.setId(String.valueOf(selectedRowHousePos));
                    propertyBedroomModel.setRoom_name(room_name.getText().toString());
                    propertyBedroomModel.setGoundtype("");

                    propertyBedroomModel.setCategory("balcony");
                    propertyBedroomModel.setRoom_type(new Random().toString());
                    propertyBedroomModel.setRoom_size(room_size.getText().toString());
                    propertyBedroomModel.setLength(length.getText().toString());
                    propertyBedroomModel.setWidth(width.getText().toString());
                    propertyBedroomModel.setLengthWidthUnitId(String.valueOf(select_unit_room.getSelectedItemPosition()));
                    propertyBedroomModel.setSizeUnit(String.valueOf(select_unit.getSelectedItemPosition()));
                    room_list.add(propertyBedroomModel);


                    ResidentialFloorModel residentialFloorModel = new ResidentialFloorModel();

                    residentialFloorModel.setProjectCategoryTypeId(arrayList.get(selectedRowHousePos).getProjectCategoryTypeId());
                    residentialFloorModel.setNeighborhoodName(arrayList.get(selectedRowHousePos).getNeighborhoodName());
                    residentialFloorModel.setArea("");
                    residentialFloorModel.setUnitFrom("000");
                    residentialFloorModel.setUnitTo("000");
                    residentialFloorModel.setId(arrayList.get(selectedRowHousePos).getId());


                    ArrayList<ResidentialFloorModel.Properties> propertiesArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties properties = new ResidentialFloorModel.Properties();
                    properties.setPropertyTypeId("2");
                    properties.setCompanyId("1");
                    properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setNoOfBedrooms("0");
                    properties.setNoOfFloors("0");
                    properties.setNoOfParkings("0");

//                        ArrayList<ResidentialFloorModel.PropertyBedrooms> propertyBedrooms = new ArrayList<>();
//                        ResidentialFloorModel.PropertyBedrooms propertyBedrooms1 = new ResidentialFloorModel.PropertyBedrooms();
//                        propertyBedrooms1.setBedRoomName("");
//                        propertyBedrooms1.setCategory("room");


                    ArrayList<ResidentialFloorModel.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties.PropertyAreas propertyAreas = new ResidentialFloorModel.Properties.PropertyAreas();
                    propertyAreas.setFrontSize("");
                    propertyAreas.setDepthSize("");
                    propertyAreasArrayList.add(propertyAreas);
                    properties.setPropertyBedrooms(room_list);
                    properties.setPhotosModels(photosModels);
                    properties.setPropertyAreas(propertyAreasArrayList);
                    propertiesArrayList.add(properties);

                    residentialFloorModel.setProperties(propertiesArrayList);


                    arrayList.set(selectedRowHousePos, residentialFloorModel);

                    rowHouseAdapter.notifyDataSetChanged();
                    dialog.cancel();
                }


            }
        });


        dialog.show();

    }

    private void EditBalconyDetaildialog(final PropertyBedroomModel room_dataArray, final RowHouseAdapter.ViewHolder vh) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.balcony_detail_popup);
        TextView room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        final Spinner room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        final Spinner select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        final Spinner select_unit_balcony = (Spinner) dialog.findViewById(R.id.select_unit_balcony);

        RecyclerView room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        LinearLayout attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        Button save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units_id);
        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        room_name.setText(room_dataArray.getRoom_name());
        length.setText(Utils.getFilteredValue(room_dataArray.getLength()));
        width.setText(Utils.getFilteredValue(room_dataArray.getWidth()));
        room_size.setText(Utils.getFilteredValue(room_dataArray.getRoom_size()));

        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });

        String[] countries = getResources().getStringArray(R.array.RoomTypes);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.text, countries);

        room_type.setAdapter(adapter);

        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        select_unit_balcony.setAdapter(adapter1);
        select_unit_balcony.setSelection(adapter1.getCount());
        room_type.setAdapter(adapter1);
        room_type.setSelection(adapter1.getCount());


        if(!room_dataArray.getLengthWidthUnitId().equalsIgnoreCase("")){

            select_unit_balcony.setSelection(Integer.parseInt(room_dataArray.getLengthWidthUnitId()));
        }
        if(!room_dataArray.getSizeUnit().equalsIgnoreCase("")){

            select_unit.setSelection(Integer.parseInt(room_dataArray.getSizeUnit()));
        }

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        select_unit_balcony.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit_balcony.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.

                } else {


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add

                if (room_name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter room name", Toast.LENGTH_SHORT).show();
                } else if (length.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter length", Toast.LENGTH_SHORT).show();
                } else if (width.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter width", Toast.LENGTH_SHORT).show();
                } else {
                    type = "balcony";
                    EditRoomApi();


                    int editpos = 0;
                    for (int i = 0; i < arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().size(); i++) {

                        if (arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().get(i).getRoom_type()
                                .equalsIgnoreCase(room_dataArray.getRoom_type())) {
                            editpos = i;
                        }

                    }

                    PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();

                    propertyBedroomModel.setId(String.valueOf(selectedRowHousePos));

                    propertyBedroomModel.setRoom_name(room_name.getText().toString());
                    propertyBedroomModel.setGoundtype("");
                    propertyBedroomModel.setRoom_type("");
                    propertyBedroomModel.setCategory(type);
                    propertyBedroomModel.setRoom_type(room_dataArray.getRoom_type());
                    propertyBedroomModel.setRoom_size(room_size.getText().toString());
                    propertyBedroomModel.setLength(length.getText().toString());
                    propertyBedroomModel.setWidth(width.getText().toString());
                    propertyBedroomModel.setLengthWidthUnitId(String.valueOf(select_unit_balcony.getSelectedItemPosition()));
                    propertyBedroomModel.setSizeUnit(String.valueOf(select_unit.getSelectedItemPosition()));
                    room_list.set(editpos, propertyBedroomModel);


                    ResidentialFloorModel residentialFloorModel = new ResidentialFloorModel();

                    residentialFloorModel.setProjectCategoryTypeId(arrayList.get(selectedRowHousePos).getProjectCategoryTypeId());
                    residentialFloorModel.setNeighborhoodName(arrayList.get(selectedRowHousePos).getNeighborhoodName());
                    residentialFloorModel.setArea("");
                    residentialFloorModel.setUnitFrom("000");
                    residentialFloorModel.setUnitTo("000");
                    residentialFloorModel.setId(arrayList.get(0).getId());


                    ArrayList<ResidentialFloorModel.Properties> propertiesArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties properties = new ResidentialFloorModel.Properties();
                    properties.setPropertyTypeId("2");
                    properties.setCompanyId("1");
                    properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
                    properties.setNoOfBedrooms("0");
                    properties.setNoOfFloors("0");
                    properties.setNoOfParkings("0");

                    ArrayList<ResidentialFloorModel.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
                    ResidentialFloorModel.Properties.PropertyAreas propertyAreas = new ResidentialFloorModel.Properties.PropertyAreas();
                    propertyAreas.setFrontSize("");
                    propertyAreas.setDepthSize("");
                    propertyAreasArrayList.add(propertyAreas);
                    properties.setPropertyBedrooms(room_list);
                    properties.setPhotosModels(photosModels);
                    properties.setPropertyAreas(propertyAreasArrayList);
                    propertiesArrayList.add(properties);

                    residentialFloorModel.setProperties(propertiesArrayList);


                    arrayList.set(vh.getAdapterPosition(), residentialFloorModel);

                    rowHouseAdapter.notifyDataSetChanged();
                }


            }
        });


      /*room_type.setAdapter(adapter);
        room_type.setSelection(adapter.getCount());*/
        dialog.show();

    }


    /*
     * Add Room APi
     * */

    private void addRoomApi() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_ADD_BEDROOM,
                getAddRoominput(),
                "Loading..",
                this,
                Urls.URL_PROJECT_ADD_BEDROOM,
                Constants.POST).execute();

    }

    private String getAddRoominput() {

        String input = "";

        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {

            jsonObject1.put("Id", 0);
            jsonObject1.put("ProjectNeighborhoodId", AccommodationId);
            jsonObject1.put("BedRoomName", room_name.getText().toString());
            jsonObject1.put("BedRoomType", room_type);
            jsonObject1.put("BedRoomSize", room_size.getText().toString());
            jsonObject1.put("BedRoomSizeUnitId", 1);
            jsonObject1.put("Length", length.getText().toString());
            jsonObject1.put("Width", width.getText().toString());
            jsonObject1.put("LengthWidthUnitId", 1);
            jsonObject1.put("PropertyFloorId", floor_id);
            jsonObject1.put("Category", type);


            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }

    private void EditRoomApi() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_ADD_BEDROOM,
                getEditRoominput(),
                "Loading..",
                this,
                Urls.URL_PROJECT_ADD_BEDROOM,
                Constants.POST).execute();

    }

    private String getEditRoominput() {

        String input = "";

        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {

            jsonObject1.put("Id", AccommodationId);
            jsonObject1.put("AccommodationId", AccommodationId);
            jsonObject1.put("BedRoomName", room_name.getText().toString());
            jsonObject1.put("BedRoomType", room_type);
            jsonObject1.put("BedRoomSize", room_size.getText().toString());
            jsonObject1.put("BedRoomSizeUnitId", 1);
            jsonObject1.put("Length", length.getText().toString());
            jsonObject1.put("Width", width.getText().toString());
            jsonObject1.put("LengthWidthUnitId", 1);
            jsonObject1.put("Category", type);
            jsonObject1.put("PropertyFloorId", 1);
            jsonObject1.put("AccommodationFloorId", 1);

            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }


    public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.ViewHolder> {
        Context context;
        private ArrayList<PropertyBedroomModel> room_data = new ArrayList();
        private ArrayList<ProjectsModel> tempArraylist;
        int id, position1;
        boolean ispublished = false;
        RowHouseAdapter.ViewHolder vh;

        public RoomListAdapter(Context context, ArrayList<PropertyBedroomModel> room_data, RowHouseAdapter.ViewHolder vh) {
            this.context = context;
            this.room_data = room_data;
            this.vh = vh;


        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.room_data_show, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.room_name_txt.setText(room_data.get(position).getRoom_name());

            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    for (int i = 0; i < arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().size(); i++) {

                                        if (arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().get(i).getRoom_type()
                                                .equalsIgnoreCase(room_data.get(position).getRoom_type())) {
                                            arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().remove(i);
                                            rowHouseAdapter.notifyDataSetChanged();
                                            return;
                                        }

                                    }
//                                    sDialog.dismissWithAnimation();
//                                    StatusModel statusModel=new StatusModel();
//                                    statusModel.setId(room_data.get(position).getId());
//                                    statusModel.setStatus("delete");
//                                    EventBus.getDefault().post(statusModel);
                                }
                            })
                            .show();

                }
            });
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    StatusModel statusModel=new StatusModel();
//                    statusModel.setId(room_data.get(position).getId());
//                    statusModel.setPos(position);
//                    statusModel.setStatus("edit");
//                    EventBus.getDefault().post(statusModel);

                    EditRoomDetaildialog(room_data.get(position), vh);
                }
            });

        }

        @Override
        public int getItemCount() {
            return room_data.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView delete, edit;
            TextView room_size_txt, room_name_txt;
            SwitchButton dots;

            public ViewHolder(View itemView) {
                super(itemView);
                delete = (ImageView) itemView.findViewById(R.id.delete);
                edit = (ImageView) itemView.findViewById(R.id.edit_room);
                room_name_txt = (TextView) itemView.findViewById(R.id.room_name_txt);

            }


        }

        private void showPublishedDialog() {

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.published_dioalog);
            dialog.show();

            final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.checked);
            final Button done = (Button) dialog.findViewById(R.id.done);

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (checkBox.isChecked()) {
                        ispublished = true;
                    } else {
                        ispublished = false;
                    }
                }
            });

        }

    }

    public class BalconyListAdapter extends RecyclerView.Adapter<BalconyListAdapter.ViewHolder> {
        Context context;
        private ArrayList<PropertyBedroomModel> room_data = new ArrayList();
        private ArrayList<ProjectsModel> tempArraylist;
        int id, position1;
        boolean ispublished = false;
        RowHouseAdapter.ViewHolder vh;

        public BalconyListAdapter(Context context, ArrayList<PropertyBedroomModel> room_data, RowHouseAdapter.ViewHolder vh) {
            this.context = context;
            this.room_data = room_data;
            this.vh = vh;


        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.room_data_show, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.room_name_txt.setText(room_data.get(position).getRoom_name());

            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                    for (int i = 0; i < arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().size(); i++) {

                                        if (arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().get(i).getRoom_type()
                                                .equalsIgnoreCase(room_data.get(position).getRoom_type())) {
                                            arrayList.get(vh.getAdapterPosition()).getProperties().get(0).getPropertyBedrooms().remove(i);
                                            rowHouseAdapter.notifyDataSetChanged();
                                            return;
                                        }

                                    }


//                                    sDialog.dismissWithAnimation();
//                                    StatusModel statusModel=new StatusModel();
//                                    statusModel.setId(room_data.get(position).getId());
//                                    statusModel.setStatus("delete");
//                                    EventBus.getDefault().post(statusModel);
                                }
                            })
                            .show();

                }
            });
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    EditBalconyDetaildialog(room_data.get(position), vh);
                }
            });

        }

        @Override
        public int getItemCount() {
            return room_data.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView delete, edit;
            TextView room_size_txt, room_name_txt;
            SwitchButton dots;

            public ViewHolder(View itemView) {
                super(itemView);
                delete = (ImageView) itemView.findViewById(R.id.delete);
                edit = (ImageView) itemView.findViewById(R.id.edit_room);
                room_name_txt = (TextView) itemView.findViewById(R.id.room_name_txt);

            }


        }

        private void showPublishedDialog() {

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.published_dioalog);
            dialog.show();

            final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.checked);
            final Button done = (Button) dialog.findViewById(R.id.done);

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (checkBox.isChecked()) {
                        ispublished = true;
                    } else {
                        ispublished = false;
                    }
                }
            });

        }

    }

}
