package com.bnhabitat.township.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.sizeunit.UnitsModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.township.adapter.PlotLandLotAdapter;
import com.bnhabitat.township.model.FloorUnitModel;
import com.bnhabitat.township.model.FloorUnitModelUnit;
import com.bnhabitat.township.model.PlotLandLot;
import com.bnhabitat.township.model.PropertiesModel;
import com.bnhabitat.township.model.SizeUnitModel;
import com.bnhabitat.township.model.TownshipLocalitiesModel;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.township.townactivities.Project_name;
import com.bnhabitat.township.townactivities.TabChangeInterface;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

import static android.app.Activity.RESULT_OK;
import static com.bnhabitat.ui.fragments.LocationFragment.inventoryModelProp;

public class Plot_lotLand_Fragment extends Fragment implements CommonAsync.OnAsyncResultListener {

    private static Activity activity;
    View view;
    LinearLayout add_more;
    RecyclerView plot_land_lot;
    Button next, done;
    PlotLandLotAdapter plotLandLotAdapter;
    ArrayList<PlotLandLot> plotLandLotArrayList = new ArrayList<>();
    public static final int REQUEST_CAMERA = 5;
    public static final int SELECT_FILE = 3;
    String encodedImage = "";
    String name = "";
    ArrayList<PhotosModel> photosModels = new ArrayList<>();
    String userChoosenTask;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private boolean isPermitted = false;
    ArrayList<String> listUnit = new ArrayList<>();
    ArrayList<UnitsModel> listUnits = new ArrayList<>();
    private JSONObject plotObj;
    String unit_from, unit_to, street = "";
    int areaUnitId;
    float area;
    ArrayList<PropertiesModel> propertiesModels = new ArrayList<>();
    TabChangeInterface tabChangeInterface;
    boolean clickedFromNext = true;
    NestedScrollView scroll_view;
    LinearLayout ll_main;
    private boolean isShown = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.plot_land_lot, null);

        initialize(view);

        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);
        ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
        onClicks(view);

        return view;

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {


            if (isShown) {

                getAllSizeUnits(Constants.AREA);
                // plotLandLotArrayList.clear();
                if (plotLandLotArrayList.size() == 0) {
                    initPlot();
                }
                isShown=false;

        } else {
                isShown = false;
            }

        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        Plot_lotLand_Fragment.activity = activity;
        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    private void onClicks(View view) {

/*
        ll_main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideSoftKeyboard(getActivity());
                return true;
            }
        });
*/

        add_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


              /*  if (street.equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please fill street name", Toast.LENGTH_SHORT).show();

                } else {



                }*/

                initPlot();

                sendPlotData();
                scroll_view.scrollTo(10, 10);

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  if (street.equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please fill street name", Toast.LENGTH_SHORT).show();

                } else {

                }*/

                sendPlotData();
                tabChangeInterface.onTabChange(1);

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickedFromNext = false;

               /* if (street.equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please fill street name", Toast.LENGTH_SHORT).show();

                } else {

                }*/
                sendPlotData();

            }
        });

    }

    public void sendPlotData() {

        try {

            // plot/lot/land request
            plotObj = new JSONObject();
            plotObj.put("ProjectCategoryTypeId", "1");
            plotObj.put("NeighborhoodName", street);
            plotObj.put("Area", area);
            plotObj.put("AreaUnitId", listUnits.get(areaUnitId).getUnitId());
            plotObj.put("UnitFrom", unit_from);
            plotObj.put("UnitTo", unit_to);

            JSONArray propertyArray = new JSONArray();

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("PropertyTypeId", "1");
            jsonObject1.put("CompanyId", "");
            jsonObject1.put("CreatedById", " ");
            jsonObject1.put("UpdatedById", "");
            jsonObject1.put("NoOfParkings", "");

            JSONArray propLocationArray = new JSONArray();

            // plotList

         /*   for (int i = 0; i < unitModelArrayList.size(); i++) {

                JSONObject jsonObjectLoc = new JSONObject();

                jsonObjectLoc.put("ProjectId", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("ProjectId", ""));
                jsonObjectLoc.put("UnitNo", unitModelArrayList.get(i).getValue());

                propLocationArray.put(jsonObjectLoc);

            }*/

            jsonObject1.put("PropertyLocations", propLocationArray);

            propertyArray.put(jsonObject1);

            plotObj.put("Properties", propertyArray);

            // detail frag data

            postLandData();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void postLandData() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_NEIGHBORHOOD,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_NEIGHBORHOOD,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = plotObj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return inputStr;
    }

    public void onEvent(DialogModel dialogModel) {

        unit_from = dialogModel.getUnit_from();
        unit_to = dialogModel.getUnit_to();
        street = dialogModel.getName();
        area = dialogModel.getArea();
        areaUnitId = dialogModel.getPosSpinner();
    }

    private void initPlot() {
        PlotLandLot plotLandLot = new PlotLandLot();
        plotLandLot.setProjectCategoryTypeId("2");
        plotLandLot.setNeighborhoodName("");
        plotLandLot.setArea("");
        plotLandLot.setUnitFrom("000");
        plotLandLot.setUnitTo("000");

        ArrayList<PlotLandLot.Properties> propertiesArrayList = new ArrayList<>();
        PlotLandLot.Properties properties = new PlotLandLot.Properties();
        properties.setPropertyTypeId("2");
        properties.setCompanyId("1");
        properties.setCreatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
        properties.setUpdatedById(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));

        ArrayList<PlotLandLot.Properties.PropertyAreas> propertyAreasArrayList = new ArrayList<>();
        PlotLandLot.Properties.PropertyAreas propertyAreas = new PlotLandLot.Properties.PropertyAreas();
        propertyAreas.setFrontSize("");
        propertyAreas.setDepthSize("");
        propertyAreasArrayList.add(propertyAreas);

        properties.setPropertyAreas(propertyAreasArrayList);
        propertiesArrayList.add(properties);

        plotLandLot.setProperties(propertiesArrayList);

        plotLandLotArrayList.add(plotLandLot);
    }

    private void initialize(View view) {
        add_more = (LinearLayout) view.findViewById(R.id.add_more);
        plot_land_lot = (RecyclerView) view.findViewById(R.id.plot_land_lot);
        next = (Button) view.findViewById(R.id.next);
        tabChangeInterface = (TabChangeInterface) getActivity();

        done = (Button) view.findViewById(R.id.done);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean openActivityOnce = true;
        boolean openDialogOnce = true;
        if (requestCode == 786) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView();
                        }
                    }
                }
            }

            selectImage();

        }
    }

    private void alertView() {
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.AppTheme);

        dialog.setTitle("Permission Denied")
                .setInverseBackgroundForced(true)
                //.setIcon(R.drawable.ic_info_black_24dp)
                .setMessage("Without those permission the app is unable to save your profile. App needs to save profile image in your external storage and also need to get profile image from camera or external storage.Are you sure you want to deny this permission?")

                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }
                })
                .setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        checkRunTimePermission();

                    }
                }).show();
    }

    public void onEvent(SizeUnitModel sizeUnitModel) {
        if (sizeUnitModel != null) {

            ArrayList<String> listUnit = new ArrayList<>();
            listUnit.add(0, getString(R.string.select));
            ArrayList<SizeUnitModel.Result> result = sizeUnitModel.getResult();
            if (result.size() != 0) {

                for (int i = 0; i < result.size(); i++) {

                    String strUnits = result.get(i).getSizeUnit();
                    listUnit.add(strUnits);
                }

            }

        } else {
            Utils.showErrorMessage(R.string.something_wrong, getActivity());
        }
    }

    @Override
    public void onResultListener(String result, String which) {

        if (which.equalsIgnoreCase(Constants.GET_ALL_SIZE_UNITS_API)) {
            SizeUnitModel sizeUnitModel = Utils.getGsonObject().fromJson(result, SizeUnitModel.class);
            if (sizeUnitModel != null) {

                listUnit.add(0, getString(R.string.select));
                ArrayList<SizeUnitModel.Result> result1 = sizeUnitModel.getResult();
                if (result1.size() != 0) {

                    for (int i = 0; i < result1.size(); i++) {
                        UnitsModel unitsModel = new UnitsModel();

                        String strUnits = result1.get(i).getSizeUnit();
                        int id = Integer.parseInt(result1.get(i).getId());
                        unitsModel.setUnitId(id);
                        unitsModel.setUnitName(strUnits);
                        listUnit.add(strUnits);
                        listUnits.add(unitsModel);
                    }

                }
                ArrayList<FloorUnitModel> localPlotlotUnitModels = new ArrayList<>();
                ArrayList<FloorUnitModelUnit> finalplotlotUnitModels = new ArrayList<>();
                FloorUnitModelUnit floorUnitModelUnit = new FloorUnitModelUnit();
                floorUnitModelUnit.setValue(0);
                floorUnitModelUnit.setPlotlotUnitModels(localPlotlotUnitModels);
                finalplotlotUnitModels.add(floorUnitModelUnit);
                plotLandLotAdapter = new PlotLandLotAdapter(getActivity(), plotLandLotArrayList, listUnit, finalplotlotUnitModels, Plot_lotLand_Fragment.this, photosModels);
                plot_land_lot.setLayoutManager(new LinearLayoutManager(getContext()));
                plot_land_lot.setAdapter(plotLandLotAdapter);
//              plot_land_lot.setNestedScrollingEnabled(false);
                plotLandLotAdapter.notifyDataSetChanged();

            } else {
                Utils.showErrorMessage(R.string.something_wrong, getActivity());
            }

        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_PHOTOS_NEIGHBOURHOOD)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {

                    JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");

                    JSONArray propertyPhotos = jsonObject_photo.getJSONArray("Result");

                    if (propertyPhotos.length() == 0) {
                    } else {

                        for (int index = 0; index < propertyPhotos.length(); index++) {
                            JSONObject jsonObject1 = propertyPhotos.getJSONObject(index);
                            PhotosModel photosModel = new PhotosModel();
                            photosModel.setId(jsonObject1.getString("Id"));
                            photosModel.setName(jsonObject1.getString("Name"));
                            photosModel.setImageUrl(jsonObject1.getString("Url"));
                            photosModels.add(photosModel);
                        }
//                        plotLandLotAdapter.imageinflateview(photosModels);
                        plotLandLotAdapter.notifyDataSetChanged();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {
                    JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_NEIGHBORHOOD)) {

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.getInt("StatusCode") == 200) {
                    JSONObject jsonResult = jsonObject.getJSONObject("Result");

                    String message = jsonResult.getString("Message");
                    //  street = "";

                    if (clickedFromNext) {

                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } else {

//                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                        getActivity().finish();

                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText(this.getString(R.string.message))
                                .setContentText(this.getString(R.string.tapping_yes))
                                .setConfirmText(this.getString(R.string.yes))
                                .setCancelText(this.getString(R.string.cancel))
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })

                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        getActivity().finish();

                                    }
                                })
                                .show();


                    }


                    // setPlotAdapter();
                    plotLandLotAdapter.notifyDataSetChanged();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }

    @Override
    public void onResume() {
        super.onResume();

        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        EventBus.getDefault().unregister(this);

    }

//    private void setPlotAdapter() {
//
//        plotLandLotAdapter = new PlotLandLotAdapter(getActivity(), plotLandLotArrayList, listUnit, Plot_lotLand_Fragment.this);
//        plot_land_lot.setLayoutManager(new LinearLayoutManager(getContext()));
//        plot_land_lot.setAdapter(plotLandLotAdapter);
//        plotLandLotAdapter.notifyDataSetChanged();
//    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.bnhabitat.utils.Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {
            // if already permition granted
            selectImage();

        }
    }

    private void getAllSizeUnits(String type) {

        new CommonAsync(getContext(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/" + type,
                "",
                "Loading..",
                this,
                Constants.GET_ALL_SIZE_UNITS_API,
                Constants.GET).execute();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodedImage = getEncoded64ImageStringFromBitmap(bm);
                //  name = data.getData().getLastPathSegment();
                postPropertyImages();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//      ivImage.setImageBitmap(bm);
    }

    private void postPropertyImages() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_PHOTOS_NEIGHBOURHOOD,
                photoInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_PHOTOS_NEIGHBOURHOOD,
                Constants.POST).execute();

    }

    private String photoInput() {
        String inputStr = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("CompanyId", "1");
            jsonObject1.put("Name", "test");
            jsonObject1.put("Type", "jpg");
            jsonObject1.put("ImageCode", encodedImage);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsonArray.toString();
        return inputStr;
    }

    public void deletePropertyImages(String position) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT,
                deletephotoInput(position),
                "Loading..",
                this,
                Urls.URL_REMOVE_PROPERTY_PHOTOS_PROJECT,
                Constants.POST).execute();

    }

    private String deletephotoInput(String id) {
        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
      //  File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encodedImage = getEncoded64ImageStringFromBitmap(thumbnail);
        //  name = data.getData().getLastPathSegment();
        postPropertyImages();
    }

    public void checkDrawerPermision() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_WRITE_PERMISSION);

        } else {
            onPickImageCamera();
        }
    }

    public void onPickImageCamera() {


        final CharSequence[] items = {"Take Photo", "Select photo from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select your option");
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    cameraIntent();
                } else if (items[item].equals("Select photo from gallery")) {
                    userChoosenTask = "Choose from Library";
                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(intent, SELECT_FILE);
    }

}


