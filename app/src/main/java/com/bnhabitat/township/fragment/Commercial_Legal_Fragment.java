package com.bnhabitat.township.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.township.model.TownshipFileModel;
import com.bnhabitat.township.townactivities.Add_Project;
import com.bnhabitat.ui.activities.NewDashboardActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.app.Activity.RESULT_OK;

public class Commercial_Legal_Fragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener {

    LinearLayout lnrEmbededVideo1, lnrAddEmbdedVideo1, lnrEmbededVideo2, lnrAddEmbdedVideo2, lnrEmbededVideo3, lnrAddEmbdedVideo3;

    ImageView imgCloseAddMore, imgCloseAddMore1, imgCloseAddMore2;

    EditText edtAddMore, edtAddMore1, edtAddMore2;

    ArrayList<String> embedVideo1 = new ArrayList<>();

    ArrayList<String> embedVideo2 = new ArrayList<>();

    ArrayList<String> embedVideo3 = new ArrayList<>();

    LinearLayout lnrZoningPlanImg, lnrZoningImg, lnrZoningPlanImg2, lnrZoningImg2, lnrZoningPlanImg3, lnrZoningImg3;

    static int uploadImageType = 1;

    public static final int REQUEST_CAMERA = 5;

    public static final int SELECT_FILE = 0x3;

    private static final int REQUEST_WRITE_PERMISSION = 786;

    String encodedImage = "";
    String strDeveloperLogo;
    int id = 0;

    ArrayList<TownshipFileModel> listTownshipModel = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelDevelop = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelZoning = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelClub = new ArrayList<>();

    RadioButton radio_free_hold, radio_lease_hold;
    EditText date,et_dateOfCompletion;

    Calendar calendar;

    ImageView next_legal_btn;
    EditText pull_from_txt;
    RadioGroup ownership_status;
    View view;

    TabChangeListener tabChangeListener;
    String selectedtext = "Free Hold";
    RadioButton radio_gpa, radio_share, radio_builder_buyer, radio_convence_deed, radio_registery;

    RadioButton radio_pul_registery,radio_builder_transfer,radio_share_transfer,radio_pull_gpa,

    radio_company,radio_build_developer,radio_rwa;
    ScrollView scroll_view;
    LinearLayout ll_main;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.commercial_legal_fragment, container, false);
        scroll_view = (ScrollView) view.findViewById(R.id.scroll_view);
        ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
        init();
        onClicks();

        lnrEmbededVideo1 = (LinearLayout) view.findViewById(R.id.lnrEmbededVideo1);
        lnrAddEmbdedVideo1 = (LinearLayout) view.findViewById(R.id.lnrAddEmbdedVideo1);
        lnrEmbededVideo2 = (LinearLayout) view.findViewById(R.id.lnrEmbededVideo2);
        lnrAddEmbdedVideo2 = (LinearLayout) view.findViewById(R.id.lnrAddEmbdedVideo2);
        lnrEmbededVideo3 = (LinearLayout) view.findViewById(R.id.lnrEmbededVideo3);
        lnrAddEmbdedVideo3 = (LinearLayout) view.findViewById(R.id.lnrAddEmbdedVideo3);
        et_dateOfCompletion = (EditText) view.findViewById(R.id.et_dateOfCompletion);

        lnrZoningPlanImg = (LinearLayout) view.findViewById(R.id.lnrZoningPlanImg);
        lnrZoningImg = (LinearLayout) view.findViewById(R.id.lnrZoningImg);
        lnrZoningPlanImg2 = (LinearLayout) view.findViewById(R.id.lnrZoningPlanImg2);
        lnrZoningImg2 = (LinearLayout) view.findViewById(R.id.lnrZoningImg2);
        lnrZoningPlanImg3 = (LinearLayout) view.findViewById(R.id.lnrZoningPlanImg3);
        lnrZoningImg3 = (LinearLayout) view.findViewById(R.id.lnrZoningImg3);

        next_legal_btn = (ImageView) view.findViewById(R.id.next_legal_btn);

        next_legal_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addLegalInformation();
                scroll_view.scrollTo(10, 10);

            }
        });


        pull_from_txt = (EditText) view.findViewById(R.id.pull_from_txt);
        ownership_status = (RadioGroup) view.findViewById(R.id.ownership_status);

        date = (EditText) view.findViewById(R.id.date);
        calendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        final DatePickerDialog.OnDateSetListener datePicker1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel1();
            }

        };

        date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), datePicker, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        et_dateOfCompletion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), datePicker1, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        embedVideo1.add("");
        addEmbededVideo(embedVideo1);

        embedVideo2.add("");
        addEmbededVideo1(embedVideo2);

        embedVideo3.add("");
        addEmbededVideo2(embedVideo3);

        lnrZoningPlanImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageType = 1;
                checkDrawerPermision();
            }
        });

        lnrZoningPlanImg2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageType = 2;
                checkDrawerPermision();
            }
        });

        lnrZoningPlanImg3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImageType = 3;
                checkDrawerPermision();
            }
        });

        return view;
    }
    private void updateLabel1() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        et_dateOfCompletion.setText(sdf.format(calendar.getTime()));
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        date.setText(sdf.format(calendar.getTime()));
    }
    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }


    private void onClicks() {

/*
        ll_main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideSoftKeyboard(getActivity());
                return true;
            }
        });
*/

        radio_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_share.isChecked()) {
                    radio_gpa.setChecked(false);
                    radio_builder_buyer.setChecked(false);
                    radio_convence_deed.setChecked(false);
                    radio_registery.setChecked(false);
                }
            }
        });
        radio_builder_buyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_builder_buyer.isChecked()) {
                    radio_gpa.setChecked(false);
                    radio_share.setChecked(false);
                    radio_convence_deed.setChecked(false);
                    radio_registery.setChecked(false);
                }
            }
        });

        radio_convence_deed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_convence_deed.isChecked()) {
                    radio_gpa.setChecked(false);
                    radio_share.setChecked(false);
                    radio_builder_buyer.setChecked(false);
                    radio_registery.setChecked(false);
                }
            }
        });

        radio_registery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_registery.isChecked()) {
                    radio_gpa.setChecked(false);
                    radio_share.setChecked(false);
                    radio_builder_buyer.setChecked(false);
                    radio_convence_deed.setChecked(false);
                }
            }
        });

        /////////////////////////////////////////////////////

        radio_pul_registery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_pul_registery.isChecked()) {
                    radio_builder_transfer.setChecked(false);
                    radio_share_transfer.setChecked(false);
                    radio_pull_gpa.setChecked(false);
                }
            }
        });

        radio_builder_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_builder_transfer.isChecked()) {
                    radio_builder_transfer.setChecked(false);
                    radio_pul_registery.setChecked(false);
                    radio_pull_gpa.setChecked(false);
                }
            }
        });

        radio_share_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_share_transfer.isChecked()) {
                    radio_builder_transfer.setChecked(false);
                    radio_pul_registery.setChecked(false);
                    radio_pull_gpa.setChecked(false);
                }
            }
        });
        radio_pull_gpa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_pull_gpa.isChecked()) {
                    radio_builder_transfer.setChecked(false);
                    radio_share_transfer.setChecked(false);
                    radio_pul_registery.setChecked(false);
                }
            }
        });

        ////////////////////////////

        radio_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_company.isChecked()) {
                    radio_build_developer.setChecked(false);
                    radio_rwa.setChecked(false);
                }
            }
        });
        radio_build_developer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_build_developer.isChecked()) {
                    radio_company.setChecked(false);
                    radio_rwa.setChecked(false);
                }
            }
        });
        radio_rwa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_rwa.isChecked()) {
                    radio_company.setChecked(false);
                    radio_build_developer.setChecked(false);
                }
            }
        });


    }

    public void init() {

        radio_free_hold = (RadioButton) view.findViewById(R.id.radio_free_hold);
        radio_lease_hold = (RadioButton) view.findViewById(R.id.radio_lease_hold);
        radio_share = (RadioButton) view.findViewById(R.id.radio_share);
        radio_builder_buyer = (RadioButton) view.findViewById(R.id.radio_builder_buyer);
        radio_convence_deed = (RadioButton) view.findViewById(R.id.radio_convence_deed);
        radio_registery = (RadioButton) view.findViewById(R.id.radio_registery);
        radio_gpa = (RadioButton) view.findViewById(R.id.radio_gpa);

        radio_pul_registery = (RadioButton) view.findViewById(R.id.radio_pul_registery);
        radio_builder_transfer = (RadioButton) view.findViewById(R.id.radio_builder_transfer);
        radio_share_transfer = (RadioButton) view.findViewById(R.id.radio_share_transfer);
        radio_pull_gpa = (RadioButton) view.findViewById(R.id.radio_pull_gpa);

        radio_company = (RadioButton) view.findViewById(R.id.radio_company);
        radio_build_developer = (RadioButton) view.findViewById(R.id.radio_build_developer);
        radio_rwa = (RadioButton) view.findViewById(R.id.radio_rwa);

    }

    public void addEmbededVideo(final ArrayList<String> list) {

        lnrEmbededVideo1.removeAllViews();
        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View view = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore = (EditText) view.findViewById(R.id.edtAddMore);
            edtAddMore.setText(list.get(i));

            imgCloseAddMore = (ImageView) view.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore.setId(i);

            edtAddMore.setId(i);

            if (i == 0) {
                imgCloseAddMore.setVisibility(View.GONE);
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore.setText(list.get(i));
                } else
                    edtAddMore.setHint(getResources().getString(R.string.add_embed_youtube));
            }

            final int finalI = i;

            imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id1 = finalI;

                    EditText t = (EditText) view.findViewById(id1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            list.add(list.size() - 1, text);
                        }
                    }

                    int id = finalI;
                    list.remove(id);

                    addEmbededVideo(list);
                }
            });

            lnrEmbededVideo1.addView(view);
        }

        lnrAddEmbdedVideo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).equalsIgnoreCase("")) {

                        list.add("");

                    } else {
                        list.add(list.size() - 1, edtAddMore.getText().toString());
                    }

                    addEmbededVideo(list);
                } else {
                    Utils.showErrorMessage("Please add url", getActivity());
                }
            }
        });

    }

    private void addLegalInformation() {


        new CommonAsync(getActivity(), Urls.LEGAL_INFO_URL,
                getInputLegalJson(),
                "Loading..",
                this,
                Constants.LEGAL_INFO_URL,
                Constants.POST).execute();

    }

    private String getInputLegalJson() {
        String strREq = "";
        if (radio_free_hold.isChecked())
            selectedtext = "Free Hold";
        else
            selectedtext = "Lease Hold";

        // JSONArray jsonArray = null;
        JSONObject jsonObject = null;

        jsonObject = new JSONObject();
        // jsonArray = new JSONArray();
        try {
            int radioButtonID = ownership_status.getCheckedRadioButtonId();

            jsonObject.put("ProjectId", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("ProjectId", ""));
            jsonObject.put("Category", "Residential");
            jsonObject.put("TransferByWayOf", pull_from_txt.getText().toString().trim());
            jsonObject.put("OwnershipStatus", selectedtext);
            jsonObject.put("TransferRestriction", "");
            jsonObject.put("OwnershipType", "");
            jsonObject.put("ManagementUnder", "");
            jsonObject.put("Cam", "");
            jsonObject.put("DateOfCompliationCertificate", null);
            jsonObject.put("DateOfOccupassionCertificate", null);
            //  jsonArray.put(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        strREq = jsonObject.toString();

        return strREq;

    }

    public void addEmbededVideo1(final ArrayList<String> list) {

        lnrEmbededVideo2.removeAllViews();
        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View view = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore1 = (EditText) view.findViewById(R.id.edtAddMore);
            edtAddMore1.setText(list.get(i));

            imgCloseAddMore1 = (ImageView) view.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore1.setId(i);

            edtAddMore1.setId(i);

            if (i == 0) {
                imgCloseAddMore1.setVisibility(View.GONE);
                if (!edtAddMore1.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore1.setText(list.get(i));
                } else
                    edtAddMore1.setHint(getResources().getString(R.string.add_embed_youtube));
            }

            final int finalI = i;

            imgCloseAddMore1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id1 = finalI;

                    EditText t = (EditText) view.findViewById(id1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            list.add(list.size() - 1, text);
                        }
                    }

                    int id = finalI;
                    list.remove(id);

                    addEmbededVideo1(list);
                }
            });

            lnrEmbededVideo2.addView(view);
        }

        lnrAddEmbdedVideo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtAddMore1.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).equalsIgnoreCase("")) {

                        list.add("");

                    } else {
                        list.add(list.size() - 1, edtAddMore1.getText().toString());
                    }

                    addEmbededVideo1(list);
                } else {
                    Utils.showErrorMessage("Please add url", getActivity());
                }
            }
        });

    }

    public void addEmbededVideo2(final ArrayList<String> list) {

        lnrEmbededVideo3.removeAllViews();
        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View view = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore2 = (EditText) view.findViewById(R.id.edtAddMore);
            edtAddMore2.setText(list.get(i));

            imgCloseAddMore2 = (ImageView) view.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore2.setId(i);

            edtAddMore2.setId(i);

            if (i == 0) {
                imgCloseAddMore2.setVisibility(View.GONE);
                if (!edtAddMore2.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore2.setText(list.get(i));
                } else
                    edtAddMore2.setHint(getResources().getString(R.string.add_embed_youtube));
            }

            final int finalI = i;

            imgCloseAddMore2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id1 = finalI;

                    EditText t = (EditText) view.findViewById(id1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            list.add(list.size() - 1, text);
                        }
                    }

                    int id = finalI;
                    list.remove(id);

                    addEmbededVideo2(list);
                }
            });

            lnrEmbededVideo3.addView(view);
        }

        lnrAddEmbdedVideo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtAddMore2.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).equalsIgnoreCase("")) {

                        list.add("");

                    } else {
                        list.add(list.size() - 1, edtAddMore2.getText().toString());
                    }

                    addEmbededVideo2(list);
                } else {
                    Utils.showErrorMessage("Please add url", getActivity());
                }
            }
        });

    }

    private void checkDrawerPermision() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_WRITE_PERMISSION);

        } else {
            onPickImageCamera();
        }


    }


    public void onPickImageCamera() {


        final CharSequence[] items = {"Take Photo", "Select photo from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select your option");
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Select photo from gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            String encodedImage = Utils.encodeImage(thumbnail);

            uploadFile(encodedImage);

        } else if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {


            Uri selectedImageUri = data.getData();


            try {
                InputStream imageStream = getActivity().getContentResolver().openInputStream(selectedImageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                encodedImage = Utils.encodeImage(selectedImage);

                uploadFile(encodedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPickImageCamera();
        }

    }

    private void uploadFile(String urlImage) {


        new CommonAsync(getActivity(), Urls.UPLOAD_IMAGE,
                getInputJson(urlImage),
                "Loading..",
                this,
                Constants.UPLOAD_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJson(String urlImage) {
        String strREq = "";
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;

        jsonObject = new JSONObject();
        jsonArray = new JSONArray();
        try {

            jsonObject.put(Constants.CompanyId, 1);
            jsonObject.put(Constants.Name, "township");
            jsonObject.put(Constants.Type, Constants.jpg);
            jsonObject.put(Constants.ImageCode, urlImage);
            jsonArray.put(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        strREq = jsonArray.toString();

        return strREq;
    }

    private void removeFile(String id) {


        new CommonAsync(getActivity(), Urls.REMOVE_IMAGE,
                getInputJsonId(id),
                "Loading..",
                this,
                Constants.REMOVE_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJsonId(String id) {

        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }

    @Override
    public void onResultListener(String result, String which) {

        String w = which;

        if (which != null) {
            if (which.equalsIgnoreCase(Constants.UPLOAD_IMAGE_API)) {
                try {
                    JSONObject obj = new JSONObject(result);
                    int statusCode = obj.getInt("StatusCode");
                    if (statusCode == 200) {

                        JSONObject Result = obj.getJSONObject("Result");
                        JSONArray jsonArray = Result.getJSONArray("Result");

                        if (jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                strDeveloperLogo = object.getString("Url");
                                Log.d("TAG", "onResultListener: " + strDeveloperLogo);
                                String Id = object.getString("Id");


                                if (uploadImageType == 1) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "developer_logo", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelDevelop.add(townshipFileModel);
                                    notifyChangeImages1();

                                } else if (uploadImageType == 2) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "zoningPlan", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelZoning.add(townshipFileModel);

                                    notifyChangeImages();

                                } else if (uploadImageType == 3) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "clubAminity", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelClub.add(townshipFileModel);

                                    notifyChangeImages2();

                                }

                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.REMOVE_IMAGE_API)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        Toast.makeText(getActivity(), "Legal information added successfully", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Constants.LEGAL_INFO_URL)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                      //  Utils.showSuccessErrorMessage("Success", "Legal information added successfully", "Ok", getActivity());

                        Toast.makeText(getActivity(), "Legal information added successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), NewDashboardActivity.class);
                        startActivity(intent);

                    } else if (jsonObject.getInt("StatusCode") == 500) {
                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        Toast.makeText(getActivity(), "Legal information added successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), NewDashboardActivity.class);
                        startActivity(intent);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }


        Log.e("which", "" + which);


    }

    public void notifyChangeImages() {

        lnrZoningImg2.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelZoning.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getActivity().getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);


            Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelZoning.get(i).getUrl()).into(imgEvents);

            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelZoning.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelZoning.get(id).getFileId());
                                    listTownshipModelZoning.remove(id);
                                    notifyChangeImages();
                                }
                            })
                            .show();

                }
            });

            lnrZoningImg2.addView(v);

        }


    }

    public void notifyChangeImages1() {

        lnrZoningImg.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelDevelop.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getActivity().getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);

            String strurl = Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl();


            Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl()).into(imgEvents);

            Log.e("TAG", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelDevelop.get(id).getFileId());
                                    listTownshipModelDevelop.remove(id);
                                    notifyChangeImages1();
                                }
                            })
                            .show();
                }
            });


            lnrZoningImg.addView(v);

        }

    }

    public void notifyChangeImages2() {

        lnrZoningImg3.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelClub.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getActivity().getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);

            Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelClub.get(i).getUrl()).into(imgEvents);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelClub.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelClub.get(id).getFileId());
                                    listTownshipModelClub.remove(id);
                                    notifyChangeImages2();
                                }
                            })
                            .show();
                }
            });

            lnrZoningImg3.addView(v);

        }

    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}

