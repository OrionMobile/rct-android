package com.bnhabitat.township.townactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ProCategoryTypes;
import com.bnhabitat.township.fragment.Flat_apartment_fragment;
import com.bnhabitat.township.fragment.Pentahouse_Fragment;
import com.bnhabitat.township.fragment.Plot_lotLand_Fragment;
import com.bnhabitat.township.fragment.Residential_fragment;
import com.bnhabitat.township.fragment.RowHouse_Fragment;
import com.bnhabitat.township.fragment.ScoScf_fragment;
import com.bnhabitat.township.model.PropertyTypeModel;
import com.bnhabitat.ui.fragments.DetailFragment;
import com.bnhabitat.ui.fragments.FinancialandBankLoanFragment;
import com.bnhabitat.ui.fragments.LegalStatusFragment;
import com.bnhabitat.ui.fragments.LocationFragment;
import com.bnhabitat.ui.fragments.OtherExpensesFragment;
import com.bnhabitat.ui.fragments.OtherQuestionsFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class ProjectNameSepecification extends FragmentActivity implements TabChangeInterface {

    @OnClick(R.id.imgBack)
    public void onClickBack() {

        if (count > 0) {

            count--;


            tabLayout.getTabAt(count);
            viewpager.setCurrentItem(count);
        } else {

            onBackPressed();
        }
    }

    String proName = "";

    //  @BindView(R.id.tabs_projectname)
    //  TabLayout tabs_projectname;
    public TabLayout tabLayout;

    ViewPager viewpager;

    TextView tv_title;

    public ArrayList<String> strings = new ArrayList<>();

    ArrayList<ProCategoryTypes> myList = new ArrayList<>();

    ArrayList<PropertyTypeModel> propertyTypeSelectedArray = new ArrayList<>();

    TabLayout.Tab Tab1, Tab2, Tab3, Tab4, Tab5, Tab6, Tab7, Tab8, Tab9, Tab10, Tab11, Tab12, Tab13, Tab14, Tab15, Tab16;

    int count = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project_name_specifications);

        myList = (ArrayList<ProCategoryTypes>) getIntent().getSerializableExtra("fabList");
        propertyTypeSelectedArray = (ArrayList<PropertyTypeModel>) getIntent().getSerializableExtra("selectedList");
        tabLayout = (TabLayout) findViewById(R.id.tabs_projectname);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        proName = getIntent().getStringExtra("ProjectName");
        tv_title = (TextView) findViewById(R.id.tv_title);

        if (!proName.equalsIgnoreCase(""))
            tv_title.setText(proName);
        if (propertyTypeSelectedArray != null)
            fetchStringFromList();

        ButterKnife.bind(this);


        setUpFrag();


    }

    private void setUpFrag() {

        Tab1 = tabLayout.newTab().setText("Flat/Apartment");
        Tab2 = tabLayout.newTab().setText("Plot/Lot/Land");
        Tab3 = tabLayout.newTab().setText("Row House/Villa Independent House");
        Tab4 = tabLayout.newTab().setText("Penthouse/Duplex");
        Tab5 = tabLayout.newTab().setText("Residential Floor");
        Tab6 = tabLayout.newTab().setText("Studio Apartment");
        Tab7 = tabLayout.newTab().setText("Farm House");
        Tab8 = tabLayout.newTab().setText("SCO Shop Cum Office");
        Tab9 = tabLayout.newTab().setText("SCS Shop Cum Shop");
        Tab10 = tabLayout.newTab().setText("SCF Shop Cum Flat");
        Tab11 = tabLayout.newTab().setText("DSS Double Story Shop");
        Tab12 = tabLayout.newTab().setText("Booth/ Kiosk");
        Tab13 = tabLayout.newTab().setText("Office Space in Commercial Building");
        Tab14 = tabLayout.newTab().setText("Office in a Mall");
        Tab15 = tabLayout.newTab().setText("Shop in a Shopping Complex");
        Tab16 = tabLayout.newTab().setText("Commercial Plot");

        if (strings.contains("Flat/Apartment"))
            tabLayout.addTab(Tab1);
        else if (strings.contains("Plot/Lot/Land"))
            tabLayout.addTab(Tab2);
        else if (strings.contains("Row House/Villa Independent House"))
            tabLayout.addTab(Tab3);
        else if (strings.contains("Penthouse/Duplex"))
            tabLayout.addTab(Tab4);
        else if (strings.contains("Residential Floor"))
            tabLayout.addTab(Tab5);
        else if (strings.contains("Studio Apartment"))
            tabLayout.addTab(Tab6);
        else if (strings.contains("Farm House"))
            tabLayout.addTab(Tab7);
        else if (strings.contains("SCO Shop Cum Office"))
            tabLayout.addTab(Tab8);
        else if (strings.contains("SCS Shop Cum Shop"))
            tabLayout.addTab(Tab9);
        else if (strings.contains("SCF Shop Cum Flat"))
            tabLayout.addTab(Tab10);
        else if (strings.contains("DSS Double Story Shop"))
            tabLayout.addTab(Tab11);
        else if (strings.contains("Booth/ Kiosk"))
            tabLayout.addTab(Tab12);
        else if (strings.contains("Office Space in Commercial Building"))
            tabLayout.addTab(Tab13);
        else if (strings.contains("Office in a Mall"))
            tabLayout.addTab(Tab14);
        else if (strings.contains("Shop in a Shopping Complex"))
            tabLayout.addTab(Tab15);
        else if (strings.contains("Commercial Plot"))
            tabLayout.addTab(Tab16);

        Adapter adapter = new Adapter(getSupportFragmentManager());

        viewpager.setAdapter(adapter);
        viewpager.setOffscreenPageLimit(strings.size());
        tabLayout.setupWithViewPager(viewpager);

        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }

        final View touchView = findViewById(R.id.viewpager);
        touchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        bindWidgetsWithAnEvent();

    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                PreferenceConnector.getInstance(ProjectNameSepecification.this).savePreferences(Constants.FRAGMENT_Name, tab.getText().toString());

                EventBus.getDefault().post(tab.getText().toString());

//                setCurrentTabFragment(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void fetchStringFromList() {

        strings.clear();

        for (int i = 0; i < propertyTypeSelectedArray.size(); i++) {

            strings.add(propertyTypeSelectedArray.get(i).getName());
        }
    }

    @Override
    public void onTabChange(int pos) {

        count++;
        if (strings.size() > count) {


            tabLayout.getTabAt(count);
            viewpager.setCurrentItem(count);
        } else {
//          PreferenceConnector.getInstance(this).savePreferences(Constants.FRAGMENT_Name,strings.get(count));

            Intent intent = new Intent(ProjectNameSepecification.this, LegalActivity.class);
            startActivity(intent);
            // finish();
        }
    }

    @Override
    public void onTabChangeback(int pos) {
        count++;

    }

    class Adapter extends FragmentPagerAdapter {

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {


            switch (strings.get(position)) {
                case "Flat/Apartment":
                    return new Flat_apartment_fragment();
                case "Plot/Lot/Land":
                    return new Plot_lotLand_Fragment();
                case "Row House/Villa Independent House":
                    return new RowHouse_Fragment();
                case "Penthouse/Duplex":
                    return new Pentahouse_Fragment();
                case "Residential Floor":
                    return new Residential_fragment();
                case "Studio Apartment":
                    return new Residential_fragment();
                case "Farm House":
                    return new Residential_fragment();
                case "SCO Shop Cum Office":
                    return new Residential_fragment();
                case "SCS Shop Cum Shop":
                    return new Residential_fragment();
                case "SCF Shop Cum Flat":
                    return new Residential_fragment();
                case "DSS Double Story Shop":
                    return new Residential_fragment();
                case "Booth/ Kiosk":
                    return new Residential_fragment();
                case "Office Space in Commercial Building":
                    return new Residential_fragment();
                case "Office in a Mall":
                    return new Residential_fragment();
                case "Shop in a Shopping Complex":
                    return new Residential_fragment();
                case "Commercial Plot":
                    return new Residential_fragment();


            }
            return null;
        }

        @Override
        public int getCount() {
            return strings.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            title = strings.get(position);

            return title;
        }


    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (strings.get(tabPosition)) {

            case "Flat/Apartment":
                Flat_apartment_fragment locationFragment = new Flat_apartment_fragment();
                Bundle locationbundle = new Bundle();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, locationFragment).commit();
                locationFragment.setArguments(locationbundle);
                break;
            case "Plot/Lot/Land":
                Plot_lotLand_Fragment plot_lotLand_Fragment = new Plot_lotLand_Fragment();
                Bundle bundle = new Bundle();
//                    bundle.putSerializable("propertyunits", propertyUnitsModels);
//                    bundle.putSerializable("propertyunits_length", propertyUnitsModels_length);
////                bundle.putString("jsonObject", json.toString());
//                    if (EditId != null && !EditId.equalsIgnoreCase(""))
//                        bundle.putString("editId", EditId);
//                    bundle.putString("pid", property_pid);
//                    bundle.putString("edit", edit_property);
//                    bundle.putString("PropertyTypeId", PropertyTypeId);
//
//                    bundle.putSerializable("json_Data", json_Data);
                plot_lotLand_Fragment.setArguments(bundle);
                //Inflate the fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, plot_lotLand_Fragment).commit();
                break;
            case "Row House/Villa Independent House":
                try {
                    RowHouse_Fragment rowHouse_Fragment = new RowHouse_Fragment();
                    Bundle bundle1 = new Bundle();
                    rowHouse_Fragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, rowHouse_Fragment).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                try {
                    Residential_fragment residential_fragment = new Residential_fragment();
                    Bundle bundle1 = new Bundle();
                    residential_fragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, residential_fragment).commit();


                } catch (Exception e) {
                    e.printStackTrace();
                }

//            case "Pentahouse Duplex":
//                return new Pentahouse_Fragment();
//            case "Residential Floor":
//                return new Residential_fragment();
//            case "Studio Apartment":
//                return new Residential_fragment();
//            case "Farm House":
//                return new Residential_fragment();
//            case "SCO Shop Cum Office":
//                return new Residential_fragment();
//            case "SCS Shop Cum Shop":
//                return new Residential_fragment();
//            case "SCF Shop Cum Flat":
//                return new Residential_fragment();
//            case "DSS Double Story Shop":
//                return new Residential_fragment();
//            case "Booth/Kiosk":
//                return new Residential_fragment();
//            case "Office Space in Commercial Building":
//                return new Residential_fragment();
//            case "Office in a Mall":
//                return new Residential_fragment();
//            case "Shop in a Shopping Complex":
//                return new Residential_fragment();
//            case "Commercial Plot":
//                return new Residential_fragment();
//
//

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


    @Subscribe
    public void onEvent(int pos) {

    }

}