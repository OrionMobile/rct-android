package com.bnhabitat.township.townactivities;

public interface TabChangeInterface {

    public void onTabChange(int pos);

    public void onTabChangeback(int pos);

}
