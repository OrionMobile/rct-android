package com.bnhabitat.township.townactivities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.ProCategoryTypes;
import com.bnhabitat.township.adapter.PropertyTypeAdapter;
import com.bnhabitat.township.adapter.SelectTypeAdapter;
import com.bnhabitat.township.model.ClubAmenitiesModel;
import com.bnhabitat.township.model.LengthUnitModel;
import com.bnhabitat.township.model.ProCatTypes;
import com.bnhabitat.township.model.ProjectAmenityModel;
import com.bnhabitat.township.model.ProjectCategoryTypesModel;
import com.bnhabitat.township.model.PropertyTypeModel;
import com.bnhabitat.township.model.SizeUnitModel;
import com.bnhabitat.township.model.TownshipFile;
import com.bnhabitat.township.model.TownshipFileModel;
import com.bnhabitat.township.model.TownshipLocalitiesModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class Project_name extends Activity implements CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener {

    String[] strListPropertyType = {"Residential", "Commercial", "Industrial", "Agriculture", "Institutional"};

    public LinearLayout residential_type_lay, commercial_type_lay;

    String encodedImage = "";

    boolean clickedFromNext = true;
    Button done;
    public static String type = "";

    public static ArrayList<ProCatTypes> proCatTypesArrayList = new ArrayList<>();

    int[] arrayPropertyType = {
            R.drawable.residentiall,
            R.drawable.commerciall,
            R.drawable.industriall,
            R.drawable.agriculture,
            R.drawable.institutionall,
    };

    int[] arrayPropertyTypeSlct = {
            R.drawable.residential_active,
            R.drawable.commercial_active,
            R.drawable.industrial_active,
            R.drawable.agriculture_active,
            R.drawable.institutional_active,
    };

    static int slctArea = 1, slctCoverArea = 1, slctSuperArea = 1, slctBuildUpArea = 1, slctCarperarea = 1;

    static int slctBusStand = 1, slctRailway = 1, slctHospital = 1, slctDispensary = 1, slctPoliceStation = 1, slctAirport = 1, slctStaticHighway = 1, slctNationalHighway = 1;

    @BindView(R.id.recyclerPropertyType)
    RecyclerView recyclerPropertyType;

    @BindView(R.id.recyclerResidentialType)
    public RecyclerView recyclerResidentialType;

    @BindView(R.id.recyclerCommercialType)
    public RecyclerView recyclerCommercialType;

    @BindView(R.id.lnrAddSports)
    LinearLayout lnrAddSports;

    @BindView(R.id.lnrAddAmussment)
    LinearLayout lnrAddAmussment;

    @BindView(R.id.lnrAddGreen)
    LinearLayout lnrAddGreen;

    @BindView(R.id.lnrSportsFclts)
    LinearLayout lnrSportsFclts;

    @BindView(R.id.lnrAmusmentPark)
    LinearLayout lnrAmusmentPark;

    @BindView(R.id.lnrGreenPark)
    LinearLayout lnrGreenPark;

    @BindView(R.id.lnrEmbeded)
    LinearLayout lnrEmbeded;

    @BindView(R.id.lnrAddEmbdedVideo)
    LinearLayout lnrAddEmbdedVideo;

    @BindView(R.id.lnrClubFclts)
    LinearLayout lnrClubFclts;

    @BindView(R.id.lnrAddMorClub)
    LinearLayout lnrAddMorClub;

    @BindView(R.id.lnrEmbdedClub)
    LinearLayout lnrEmbdedClub;

    @BindView(R.id.lnrAddEmbededClub)
    LinearLayout lnrAddEmbededClub;

    @BindView(R.id.lnrKeyDistance)
    LinearLayout lnrKeyDistance;

    @BindView(R.id.lnrEmbededDistance)
    LinearLayout lnrEmbededDistance;

    @BindView(R.id.lnrAddEmbdedVDistance)
    LinearLayout lnrAddEmbdedVDistance;

    @BindView(R.id.uploadZoningPlan)
    LinearLayout uploadZoningPlan;

    @BindView(R.id.lnrClubImg)
    LinearLayout lnrClubImg;

    @BindView(R.id.lnrProjectImg)
    LinearLayout lnrProjectImg;

    static int uploadImageType = 1;

    @OnClick(R.id.lnrUploadDvlprLogo)
    public void uploadDevlprLogo() {

        uploadImageType = 1;

        checkDrawerPermision();

    }

    @OnClick(R.id.lnrClubImages)
    public void uploadClubLogo() {

        uploadImageType = 2;

        checkDrawerPermision();

    }

    @OnClick(R.id.lnrProjectImages)
    public void uploadProjectLogo() {

        uploadImageType = 3;

        checkDrawerPermision();

    }

    EditText edtAddMore, edtAddMore2, edtAddMore3, edtAddMore4, edtAddMore5, edtAddMore6, edtAddMore7, edtAddMore8;
    ImageView imgCloseAddMore, imgCloseAddMore2, imgCloseAddMore3, imgCloseAddMore4, imgCloseAddMore5, imgCloseAddMore6, imgCloseAddMore7, imgCloseAddMore8;

    @OnClick(R.id.next)
    public void onClickNext() {

        for (int i = 0; i < listTownshipFile.size(); i++) {
            if (!edtAddMore.getText().toString().trim().equalsIgnoreCase(listTownshipFile.get(i).getFileUrl()) && !edtAddMore.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore.getText().toString().trim().equalsIgnoreCase(null)) {
                TownshipFile townshipFile = new TownshipFile();
                townshipFile.setUrl(edtAddMore.getText().toString().trim());
                listTownshipFile.add(townshipFile);
            }
        }

        for (int i = 0; i < listClubFile.size(); i++) {
            if (!edtAddMore2.getText().toString().trim().equalsIgnoreCase(listClubFile.get(i).getFileUrl()) && !edtAddMore2.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore2.getText().toString().trim().equalsIgnoreCase(null)) {
                TownshipFile townshipFile = new TownshipFile();
                townshipFile.setUrl(edtAddMore2.getText().toString().trim());
                listClubFile.add(townshipFile);
            }
        }


        for (int i = 0; i < listClubAlmetiesModel.size(); i++) {
            if (!edtAddMore3.getText().toString().trim().equalsIgnoreCase(listClubAlmetiesModel.get(i).getValue()) && !edtAddMore3.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore3.getText().toString().trim().equalsIgnoreCase(null)) {
                ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();
                clubAmenitiesModel.setValue(edtAddMore3.getText().toString().trim());
                listClubAlmetiesModel.add(clubAmenitiesModel);
            }
        }

        for (int i = 0; i < listZoningFile.size(); i++) {
            if (!edtAddMore4.getText().toString().trim().equalsIgnoreCase(listZoningFile.get(i).getFileUrl()) && !edtAddMore4.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore4.getText().toString().trim().equalsIgnoreCase(null)) {
                TownshipFile townshipFile = new TownshipFile();
                townshipFile.setUrl(edtAddMore4.getText().toString().trim());
                listZoningFile.add(townshipFile);
            }
        }

        for (int i = 0; i < listSportFile.size(); i++) {
            if (!edtAddMore5.getText().toString().trim().equalsIgnoreCase(listSportFile.get(i).getValue()) && !edtAddMore5.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore5.getText().toString().trim().equalsIgnoreCase(null)) {
                ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                projectAmenityModel.setValue(edtAddMore5.getText().toString().trim());
                listSportFile.remove(projectAmenityModel.getValue().equalsIgnoreCase(""));
                listSportFile.add(projectAmenityModel);
            }
        }

        for (int i = 0; i < listAmusementFile.size(); i++) {
            if (!edtAddMore6.getText().toString().trim().equalsIgnoreCase(listAmusementFile.get(i).getValue()) && !edtAddMore6.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore6.getText().toString().trim().equalsIgnoreCase(null)) {
                ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                projectAmenityModel.setValue(edtAddMore6.getText().toString().trim());
                listAmusementFile.add(projectAmenityModel);
            }
        }

        for (int i = 0; i < listGreenParkFile.size(); i++) {
            if (!edtAddMore7.getText().toString().trim().equalsIgnoreCase(listGreenParkFile.get(i).getValue()) && !edtAddMore7.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore7.getText().toString().trim().equalsIgnoreCase(null)) {
                ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                projectAmenityModel.setValue(edtAddMore7.getText().toString().trim());
                listGreenParkFile.add(projectAmenityModel);
            }
        }

        if (!propertyTypeModelArrayList.isEmpty()) {

            clickedFromNext = true;

            addProject();
        } else {

            Toast.makeText(this, "Please select Residential type/ Commercial property", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.imgBack)
    public void onClickBack() {
        onBackPressed();
    }

    public static final int REQUEST_CAMERA = 5;

    public static final int SELECT_FILE = 0x3;

    private static final int REQUEST_WRITE_PERMISSION = 786;

    String strDeveloperLogo;

    ArrayList<TownshipFileModel> listTownshipModel = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelZoning = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelClub = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelProject = new ArrayList<>();

    ArrayList<ClubAmenitiesModel> listClubAlmetiesModel = new ArrayList<>();

    ArrayList<PropertyTypeModel> propertyTypeModelArrayListResidential = new ArrayList<>();

    ArrayList<PropertyTypeModel> propertyTypeModelArrayListCommercial = new ArrayList<>();

    public static ArrayList<PropertyTypeModel> combiResComTypesArray = new ArrayList<>();

    int id = 0;

    @BindView(R.id.snrCoverArea)
    Spinner snrCoverArea;

    @BindView(R.id.spnrArea)
    Spinner spnrArea;

    @BindView(R.id.spnrBuildArea)
    Spinner spnrBuildArea;

    @BindView(R.id.spnrSuperArea)
    Spinner spnrSuperArea;

    @BindView(R.id.spnrCarpetArea)
    Spinner spnrCarpetArea;

    @BindView(R.id.edtCarpetArea)
    EditText edtCarpetArea;

    @BindView(R.id.edtBuildUpArea)
    EditText edtBuildUpArea;

    @BindView(R.id.edtSuperArea)
    EditText edtSuperArea;

    @BindView(R.id.edtCoverArea)
    EditText edtCoverArea;

    @BindView(R.id.edtClubNm)
    EditText edtClubNm;

    @BindView(R.id.editArea)
    EditText editArea;

    @BindView(R.id.key_distance)
    TextView key_distance;

    @BindView(R.id.busStand)
    TextView busStand;

    @BindView(R.id.edtBusStandLocation)
    EditText edtBusStandLocation;

    @BindView(R.id.edtBusStandDistance)
    EditText edtBusStandDistance;

    @BindView(R.id.snrBusStand)
    Spinner snrBusStand;

    @BindView(R.id.railwayStation)
    TextView railwayStation;

    @BindView(R.id.edtRailwayStationLocation)
    EditText edtRailwayStationLocation;

    @BindView(R.id.edtRailwayStationDistance)
    EditText edtRailwayStationDistance;

    @BindView(R.id.snrRailwayStation)
    Spinner snrRailwayStation;

    @BindView(R.id.hospital)
    TextView hospital;

    @BindView(R.id.edtHospitalLocation)
    EditText edtHospitalLocation;

    @BindView(R.id.edtHospitalDistance)
    EditText edtHospitalDistance;

    @BindView(R.id.snrHospital)
    Spinner snrHospital;

    @BindView(R.id.dispensary)
    TextView dispensary;

    @BindView(R.id.edtDispensaryLocation)
    EditText edtDispensaryLocation;

    @BindView(R.id.edtDispensaryDistance)
    EditText edtDispensaryDistance;

    @BindView(R.id.snrDispensary)
    Spinner snrDispensary;

    @BindView(R.id.policeStation)
    TextView policeStation;

    @BindView(R.id.edtPoliceStationLocation)
    EditText edtPoliceStationLocation;

    @BindView(R.id.edtPoliceStationDistance)
    EditText edtPoliceStationDistance;

    @BindView(R.id.snrPoliceStation)
    Spinner snrPoliceStation;

    @BindView(R.id.airport)
    TextView airport;

    @BindView(R.id.edtAirportLocation)
    EditText edtAirportLocation;

    @BindView(R.id.edtAirportDistance)
    EditText edtAirportDistance;

    @BindView(R.id.snrAirport)
    Spinner snrAirport;

    @BindView(R.id.stateHighway)
    TextView stateHighway;

    @BindView(R.id.edtStateHighwayLocation)
    EditText edtStateHighwayLocation;

    @BindView(R.id.edtStateHighwayDistance)
    EditText edtStateHighwayDistance;

    @BindView(R.id.snrStateHighway)
    Spinner snrStateHighway;

    @BindView(R.id.nationalHighway)
    TextView nationalHighway;

    @BindView(R.id.edtNationalHighwayLocation)
    EditText edtNationalHighwayLocation;

    @BindView(R.id.edtNationalHighwayDistance)
    EditText edtNationalHighwayDistance;

    @BindView(R.id.snrNationalHighway)
    Spinner snrNationalHighway;

    @BindView(R.id.about_project)
    EditText about_project;

    @BindView(R.id.edtNegativeMark)
    EditText edtNegativeMark;

    @BindView(R.id.isLayoutVisible)
    RadioGroup isLayoutVisible;

    @BindView(R.id.yes)
    RadioButton yes;

    @BindView(R.id.no)
    RadioButton no;

    @BindView(R.id.clubData)
    LinearLayout clubData;

    @BindView(R.id.recreational_area)
    TextView recreational_area;

    @BindView(R.id.sports_facility)
    TextView sports_facility;

    @BindView(R.id.recreational)
    TextView recreational;

    @BindView(R.id.amusement_park)
    TextView amusement_park;

    @BindView(R.id.green_park)
    TextView green_park;

    @BindView(R.id.amphitheatre)
    TextView amphitheatre;

    @BindView(R.id.edtEmpTheatr)
    EditText edtEmpTheatr;

    ArrayList<ProjectCategoryTypesModel> arrayList = new ArrayList<>();

    ArrayList<ProjectAmenityModel> listSportFile = new ArrayList<>();
    ArrayList<ProjectAmenityModel> listAmusementFile = new ArrayList<>();
    ArrayList<ProjectAmenityModel> listGreenParkFile = new ArrayList<>();

    ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
    ProjectAmenityModel projectAmenityModel2 = new ProjectAmenityModel();
    ProjectAmenityModel projectAmenityModel3 = new ProjectAmenityModel();

    ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();

    ArrayList<TownshipFile> listTownshipFile = new ArrayList<>();
    ArrayList<TownshipFile> listClubFile = new ArrayList<>();
    ArrayList<TownshipFile> listZoningFile = new ArrayList<>();

    TownshipFile townshipFile = new TownshipFile();

    ArrayList<TownshipFile> listAddMore1 = new ArrayList<>();
    ArrayList<TownshipFile> listAddMore2 = new ArrayList<>();
    ArrayList<TownshipFile> listAddMore3 = new ArrayList<>();
    ArrayList<ClubAmenitiesModel> listAddMore4 = new ArrayList<>();
    ArrayList<ProjectAmenityModel> listAddMore5 = new ArrayList<>();
    ArrayList<ProjectAmenityModel> listAddMore6 = new ArrayList<>();
    ArrayList<ProjectAmenityModel> listAddMore7 = new ArrayList<>();

    ArrayList<PropertyTypeModel> propertyTypeModelArrayList = new ArrayList<>();

    String proName = "";
    TextView tv_title;
    NestedScrollView scroll_view;
    LinearLayout ll_main;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project_name);
        ButterKnife.bind(this);
        tv_title = (TextView) findViewById(R.id.tv_title);
        scroll_view = (NestedScrollView) findViewById(R.id.scroll_view);
        ll_main = (LinearLayout) findViewById(R.id.ll_main);

        done = (Button) findViewById(R.id.done);
        proName = getIntent().getStringExtra("ProjectName");
        ll_main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideSoftKeyboard(Project_name.this);
                return true;
            }
        });

        if (!proName.equalsIgnoreCase(""))
            tv_title.setText(proName);

        onCLicks();

        residential_type_lay = (LinearLayout) findViewById(R.id.residential_type_lay);
        commercial_type_lay = (LinearLayout) findViewById(R.id.commercial_type_lay);

        getAllSizeUnitsLength(Constants.LENGTH);
        getAllSizeUnits(Constants.AREA);

        GridLayoutManager gridlayoutManager1 = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        PropertyTypeAdapter propertyTypeAdapter = new PropertyTypeAdapter(this, arrayPropertyType, arrayPropertyTypeSlct, strListPropertyType, arrayList);
        recyclerPropertyType.setLayoutManager(gridlayoutManager1);
        recyclerPropertyType.setAdapter(propertyTypeAdapter);

        //listSportsFclts.add("");
        projectAmenityModel.setType(recreational_area.getText().toString());
        projectAmenityModel.setTitle(sports_facility.getText().toString());
        projectAmenityModel.setValue("");
        listAddMore5.add(projectAmenityModel);
        addSportsFclts(listAddMore5);

        projectAmenityModel2.setType(recreational_area.getText().toString());
        projectAmenityModel2.setTitle(amusement_park.getText().toString());
        projectAmenityModel2.setValue("");
        listAddMore6.add(projectAmenityModel2);
        addAmusment(listAddMore6);

        projectAmenityModel3.setType(recreational_area.getText().toString());
        projectAmenityModel3.setTitle(green_park.getText().toString());
        projectAmenityModel3.setValue("");
        listAddMore7.add(projectAmenityModel3);
        addGreenPark(listAddMore7);

        townshipFile.setUrl("");
        listAddMore1.add(townshipFile);
        addZoningEmbededVideo(listAddMore1);

        clubAmenitiesModel.setValue("");
        listAddMore4.add(clubAmenitiesModel);
        addClubFlcts(listAddMore4);

        townshipFile.setUrl("");
        listAddMore2.add(townshipFile);
        addEmbdedClub(listAddMore2);

        townshipFile.setUrl("");
        listAddMore3.add(townshipFile);
        addEmbdedProject(listAddMore3);

        yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    clubData.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    clubData.setVisibility(View.GONE);
                    yes.setChecked(false);
                }
            }
        });

    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    private void onCLicks() {

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < listTownshipFile.size(); i++) {
                    if (!edtAddMore.getText().toString().trim().equalsIgnoreCase(listTownshipFile.get(i).getFileUrl()) && !edtAddMore.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore.getText().toString().trim().equalsIgnoreCase(null)) {
                        TownshipFile townshipFile = new TownshipFile();
                        townshipFile.setUrl(edtAddMore.getText().toString().trim());
                        listTownshipFile.add(townshipFile);
                    }
                }

                for (int i = 0; i < listClubFile.size(); i++) {
                    if (!edtAddMore2.getText().toString().trim().equalsIgnoreCase(listClubFile.get(i).getFileUrl()) && !edtAddMore2.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore2.getText().toString().trim().equalsIgnoreCase(null)) {
                        TownshipFile townshipFile = new TownshipFile();
                        townshipFile.setUrl(edtAddMore2.getText().toString().trim());
                        listClubFile.add(townshipFile);
                    }
                }


                for (int i = 0; i < listClubAlmetiesModel.size(); i++) {
                    if (!edtAddMore3.getText().toString().trim().equalsIgnoreCase(listClubAlmetiesModel.get(i).getValue()) && !edtAddMore3.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore3.getText().toString().trim().equalsIgnoreCase(null)) {
                        ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();
                        clubAmenitiesModel.setValue(edtAddMore3.getText().toString().trim());
                        listClubAlmetiesModel.add(clubAmenitiesModel);
                    }
                }

                for (int i = 0; i < listZoningFile.size(); i++) {
                    if (!edtAddMore4.getText().toString().trim().equalsIgnoreCase(listZoningFile.get(i).getFileUrl()) && !edtAddMore4.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore4.getText().toString().trim().equalsIgnoreCase(null)) {
                        TownshipFile townshipFile = new TownshipFile();
                        townshipFile.setUrl(edtAddMore4.getText().toString().trim());
                        listZoningFile.add(townshipFile);
                    }
                }

                for (int i = 0; i < listSportFile.size(); i++) {
                    if (!edtAddMore5.getText().toString().trim().equalsIgnoreCase(listSportFile.get(i).getValue()) && !edtAddMore5.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore5.getText().toString().trim().equalsIgnoreCase(null)) {
                        ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                        projectAmenityModel.setValue(edtAddMore5.getText().toString().trim());
                        listSportFile.remove(projectAmenityModel.getValue().equalsIgnoreCase(""));
                        listSportFile.add(projectAmenityModel);
                    }
                }

                for (int i = 0; i < listAmusementFile.size(); i++) {
                    if (!edtAddMore6.getText().toString().trim().equalsIgnoreCase(listAmusementFile.get(i).getValue()) && !edtAddMore6.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore6.getText().toString().trim().equalsIgnoreCase(null)) {
                        ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                        projectAmenityModel.setValue(edtAddMore6.getText().toString().trim());
                        listAmusementFile.add(projectAmenityModel);
                    }
                }

                for (int i = 0; i < listGreenParkFile.size(); i++) {
                    if (!edtAddMore7.getText().toString().trim().equalsIgnoreCase(listGreenParkFile.get(i).getValue()) && !edtAddMore7.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore7.getText().toString().trim().equalsIgnoreCase(null)) {
                        ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                        projectAmenityModel.setValue(edtAddMore7.getText().toString().trim());
                        listGreenParkFile.add(projectAmenityModel);
                    }
                }
                if (!propertyTypeModelArrayList.isEmpty()) {

                    clickedFromNext = false;

                    addProject();
                } else {
                    scroll_view.scrollTo(10, 10);

                    Toast.makeText(Project_name.this, "Please select atleast one property", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    //Project Embed Videos
    public void addEmbdedProject(final ArrayList<TownshipFile> list) {

        lnrEmbededDistance.removeAllViews();
        listTownshipFile = list;

        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore.setText(list.get(i).getFileUrl());

            imgCloseAddMore = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore.setId(i);

            edtAddMore.setId(i);

            if (i == 0) {
                imgCloseAddMore.setVisibility(View.GONE);
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore.setText(list.get(i).getFileUrl());
                }

            }

            final int finalI = i;

            imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            TownshipFile townshipFile = new TownshipFile();
                            townshipFile.setUrl(text);
                            list.add(list.size() - 1, townshipFile);
                        }
                        /*if (!list.contains(text)) {

                            list.add(list.size() - 1, text);
                        }*/
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addEmbdedProject(list);
                }
            });

            lnrEmbededDistance.addView(v);
        }

        lnrAddEmbdedVDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TownshipFile townshipFile = new TownshipFile();
                if (!edtAddMore.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getFileUrl().trim().equalsIgnoreCase("")) {

                        townshipFile.setUrl("");
                        list.add(townshipFile);

                    } else {
                        townshipFile.setUrl(edtAddMore.getText().toString());
                        list.add(list.size() - 1, townshipFile);
                    }

                    addEmbdedProject(list);
                } else {
                    Utils.showErrorMessage("Please add Embded Videos", Project_name.this);
                }
            }
        });


    }

    //Club Embed Videos
    public void addEmbdedClub(final ArrayList<TownshipFile> list) {

        lnrEmbdedClub.removeAllViews();
        listClubFile = list;

        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore2 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore2.setText(list.get(i).getFileUrl());

            imgCloseAddMore2 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore2.setId(i);

            edtAddMore2.setId(i);

            if (i == 0) {
                imgCloseAddMore2.setVisibility(View.GONE);
                if (!edtAddMore2.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore2.setText(list.get(i).getFileUrl());
                }

            }

            final int finalI = i;

            imgCloseAddMore2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            TownshipFile townshipFile = new TownshipFile();
                            townshipFile.setUrl(text);
                            list.add(list.size() - 1, townshipFile);
                        }
                        /*if (!list.contains(text)) {

                            list.add(list.size() - 1, text);
                        }*/
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addEmbdedClub(list);
                }
            });

            lnrEmbdedClub.addView(v);
        }


        lnrAddEmbededClub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TownshipFile townshipFile = new TownshipFile();
                if (!edtAddMore2.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getFileUrl().trim().equalsIgnoreCase("")) {
                        townshipFile.setUrl("");
                        list.add(list.size() - 1, townshipFile);

                    } else {
                        townshipFile.setUrl(edtAddMore2.getText().toString());
                        list.add(list.size() - 1, townshipFile);
                    }

                    addEmbdedClub(list);
                } else {
                    Utils.showErrorMessage("Please add Embded Videos", Project_name.this);
                }
            }
        });


    }

    //Club Facilities
    public void addClubFlcts(final ArrayList<ClubAmenitiesModel> list) {

        lnrClubFclts.removeAllViews();
        listClubAlmetiesModel = list;

        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore3 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore3.setText(list.get(i).getValue());

            imgCloseAddMore3 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore3.setId(i);

            edtAddMore3.setId(i);

            if (i == 0) {
                imgCloseAddMore3.setVisibility(View.GONE);
                if (!edtAddMore3.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore3.setText(list.get(i).getValue());
                } else
                    edtAddMore3.setHint("");
            }

            final int finalI = i;

            imgCloseAddMore3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();
                            clubAmenitiesModel.setValue(text);
                            list.add(list.size() - 1, clubAmenitiesModel);
                        }
                        /*if (!list.contains(text)) {
                            list.add(list.size() - 1, text);
                        }*/
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addClubFlcts(list);
                }
            });

            lnrClubFclts.addView(v);
        }

        lnrAddMorClub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();

                if (!edtAddMore3.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getValue().equalsIgnoreCase("")) {
                        clubAmenitiesModel.setValue("");
                        list.add(clubAmenitiesModel);

                    } else {
                        clubAmenitiesModel.setValue(edtAddMore3.getText().toString());
                        list.add(list.size() - 1, clubAmenitiesModel);
                    }

                    addClubFlcts(list);
                } else {
                    Utils.showErrorMessage("Please add Club facilities", Project_name.this);
                }
            }
        });


    }

    //Zoning Embed Videos
    public void addZoningEmbededVideo(final ArrayList<TownshipFile> list) {

        lnrEmbeded.removeAllViews();
        listZoningFile = list;
        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore4 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore4.setText(list.get(i).getFileUrl());

            imgCloseAddMore4 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore4.setId(i);

            edtAddMore4.setId(i);

            if (i == 0) {
                imgCloseAddMore4.setVisibility(View.GONE);
                if (!edtAddMore4.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore4.setText(list.get(i).getFileUrl());
                }

            }

            final int finalI = i;

            imgCloseAddMore4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            TownshipFile townshipFile = new TownshipFile();
                            townshipFile.setUrl(text);
                            list.add(list.size() - 1, townshipFile);
                        }
                        /*if (!list.contains(text)) {

                            list.add(list.size() - 1, text);
                        }*/
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addZoningEmbededVideo(list);
                }
            });

            lnrEmbeded.addView(v);
        }

        lnrAddEmbdedVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TownshipFile townshipFile = new TownshipFile();
                if (!edtAddMore4.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getFileUrl().trim().equalsIgnoreCase("")) {
                        townshipFile.setUrl("");
                        list.add(townshipFile);

                    } else {
                        townshipFile.setUrl(edtAddMore4.getText().toString());
                        list.add(list.size() - 1, townshipFile);
                    }

                    addZoningEmbededVideo(list);
                } else {
                    Utils.showErrorMessage("Please add embded video", Project_name.this);
                }
            }
        });


    }

    //Sports Facilities
    public void addSportsFclts(final ArrayList<ProjectAmenityModel> list) {

        lnrSportsFclts.removeAllViews();
        listSportFile = list;

        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore5 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore5.setText(list.get(i).getValue());

            imgCloseAddMore5 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore5.setId(i);

            edtAddMore5.setId(i);

            if (i == 0) {
                imgCloseAddMore5.setVisibility(View.GONE);
                if (!edtAddMore5.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore5.setText(list.get(i).getValue());
                }

            }

            final int finalI = i;

            imgCloseAddMore5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                            projectAmenityModel.setValue("");
                            list.add(list.size() - 1, projectAmenityModel);
                        }
                        /*if (!list.contains(text)) {

                            list.add(list.size() - 1, text);
                        }*/
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addSportsFclts(list);
                }
            });

            lnrSportsFclts.addView(v);
        }

        lnrAddSports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                if (!edtAddMore5.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getValue().trim().equalsIgnoreCase("")) {

                        projectAmenityModel.setValue("");
                        list.add(projectAmenityModel);

                    } else {
                        projectAmenityModel.setValue(edtAddMore5.getText().toString());
                        list.add(list.size() - 1, projectAmenityModel);
                    }

                    addSportsFclts(list);
                } else {
                    Utils.showErrorMessage("Please add Sport Facilities", Project_name.this);
                }
            }
        });


    }

    //Amusement Park
    public void addAmusment(final ArrayList<ProjectAmenityModel> list) {

        lnrAmusmentPark.removeAllViews();
        listAmusementFile = list;

        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore6 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore6.setText(list.get(i).getValue());

            imgCloseAddMore6 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore6.setId(i);

            edtAddMore6.setId(i);

            if (i == 0) {
                imgCloseAddMore6.setVisibility(View.GONE);
                if (!edtAddMore6.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore6.setText(list.get(i).getValue());
                }

            }

            final int finalI = i;

            imgCloseAddMore6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                            projectAmenityModel.setValue("");
                            list.add(list.size() - 1, projectAmenityModel);
                        }
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addAmusment(list);
                }
            });

            lnrAmusmentPark.addView(v);
        }

        lnrAddAmussment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                if (!edtAddMore6.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getValue().trim().equalsIgnoreCase("")) {

                        projectAmenityModel.setValue("");
                        list.add(projectAmenityModel);

                    } else {
                        projectAmenityModel.setValue(edtAddMore6.getText().toString());
                        list.add(list.size() - 1, projectAmenityModel);
                    }

                    addAmusment(list);
                } else {
                    Utils.showErrorMessage("Please add Amusment Parks", Project_name.this);
                }
            }
        });


    }

    //Green Parks
    public void addGreenPark(final ArrayList<ProjectAmenityModel> list) {

        lnrGreenPark.removeAllViews();
        listGreenParkFile = list;
        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore7 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore7.setText(list.get(i).getValue());

            imgCloseAddMore7 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore7.setId(i);

            edtAddMore7.setId(i);

            if (i == 0) {
                imgCloseAddMore7.setVisibility(View.GONE);
                if (!edtAddMore7.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore7.setText(list.get(i).getValue());
                }

            }

            final int finalI = i;

            imgCloseAddMore7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();
                            projectAmenityModel.setValue("");
                            list.add(list.size() - 1, projectAmenityModel);
                        }
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addGreenPark(list);
                }
            });

            lnrGreenPark.addView(v);
        }

        lnrAddGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectAmenityModel projectAmenityModel = new ProjectAmenityModel();

                if (!edtAddMore7.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getValue().trim().equalsIgnoreCase("")) {
                        projectAmenityModel.setValue("");
                        list.add(projectAmenityModel);

                    } else {
                        projectAmenityModel.setValue(edtAddMore7.getText().toString());
                        list.add(list.size() - 1, projectAmenityModel);
                    }

                    addGreenPark(list);
                } else {
                    Utils.showErrorMessage("Please add Green Parks", Project_name.this);
                }
            }
        });


    }

    //Amphitheatre
    public void addAmphitheatre(final ArrayList<String> list) {

        lnrGreenPark.removeAllViews();

        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore8 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore8.setText(list.get(i));

            imgCloseAddMore8 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore8.setId(i);

            edtAddMore8.setId(i);

            if (i == 0) {
                imgCloseAddMore8.setVisibility(View.GONE);
                if (!edtAddMore8.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore8.setText(list.get(i));
                }

            }

            final int finalI = i;

            imgCloseAddMore8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        /*if(!text.equalsIgnoreCase(t.getText().toString())) {
                            list.add(list.size() - 1, text);
                        }*/
                        if (!list.contains(text)) {

                            list.add(list.size() - 1, text);
                        }
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addAmphitheatre(list);
                }
            });

            lnrGreenPark.addView(v);
        }

        lnrAddGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edtAddMore8.getText().toString().trim().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).trim().equalsIgnoreCase("")) {


                        list.add("");

                    } else {

                        list.add(list.size() - 1, edtAddMore8.getText().toString());
                    }

                    addAmphitheatre(list);
                } else {
                    Utils.showErrorMessage("Please add Green Parks", Project_name.this);
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void checkDrawerPermision() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_WRITE_PERMISSION);

        } else {
            onPickImageCamera();
        }


    }

    public void onPickImageCamera() {


        final CharSequence[] items = {"Take Photo", "Select photo from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select your option");
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Select photo from gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private void uploadFile(String urlImage) {


        new CommonAsync(this, Urls.UPLOAD_IMAGE,
                getInputJson(urlImage),
                "Loading..",
                this,
                Constants.UPLOAD_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJson(String urlImage) {
        String strREq = "";
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;

        jsonObject = new JSONObject();
        jsonArray = new JSONArray();
        try {

            jsonObject.put(Constants.CompanyId, 1);
            jsonObject.put(Constants.Name, "project");
            jsonObject.put(Constants.Type, Constants.jpg);
            jsonObject.put(Constants.ImageCode, urlImage);
            jsonArray.put(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        strREq = jsonArray.toString();

        return strREq;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPickImageCamera();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            String encodedImage = Utils.encodeImage(thumbnail);
            uploadFile(encodedImage);

        } else if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {


            Uri selectedImageUri = data.getData();


            try {
                InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                encodedImage = Utils.encodeImage(selectedImage);

                uploadFile(encodedImage);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }

    private void removeFile(String id) {


        new CommonAsync(this, Urls.REMOVE_IMAGE,
                getInputJsonId(id),
                "Loading..",
                this,
                Constants.REMOVE_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJsonId(String id) {

        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }

    public void propertyData(String proprtyType) {
        new CommonAsync(this, Urls.PROPERTY_LAY_DISPLAY + proprtyType,
                "",
                "Loading..",
                this,
                Constants.PROPERTY_LAY_DISPLAY,
                Constants.GET).execute();
    }

    private void getAllSizeUnitsLength(String type) {


        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/" + type,
                "",
                "Loading..",
                this,
                Constants.GET_ALL_SIZE_UNITS_LENGTH_API,
                Constants.GET).execute();

    }

    private void getAllSizeUnits(String type) {


        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/" + type,
                "",
                "Loading..",
                this,
                Constants.GET_ALL_SIZE_UNITS_API,
                Constants.GET).execute();

    }

    public void onEvent(LengthUnitModel sizeUnitModel) {
        if (sizeUnitModel != null) {

            ArrayList<String> listUnit = new ArrayList<>();
            listUnit.add(0, getString(R.string.select));
            ArrayList<LengthUnitModel.Result> result = sizeUnitModel.getResult();
            if (result.size() != 0) {

                for (int i = 0; i < result.size(); i++) {

                    String strUnits = result.get(i).getSizeUnit();
                    listUnit.add(strUnits);
                }

                final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                        this, R.layout.custom_select_spinner, listUnit) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            // Disable the first item from Spinner
                            // First item will be use for hint
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray

                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));
                        }
                        return view;
                    }

                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        }
                        return view;
                    }
                };

                spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                snrBusStand.setAdapter(spinnerArrayMs);
                snrRailwayStation.setAdapter(spinnerArrayMs);
                snrHospital.setAdapter(spinnerArrayMs);
                snrDispensary.setAdapter(spinnerArrayMs);
                snrPoliceStation.setAdapter(spinnerArrayMs);
                snrAirport.setAdapter(spinnerArrayMs);
                snrStateHighway.setAdapter(spinnerArrayMs);
                snrNationalHighway.setAdapter(spinnerArrayMs);


            }

        } else {
            Utils.showErrorMessage(R.string.something_wrong, this);
        }
    }

    public void onEvent(SizeUnitModel sizeUnitModel) {
        if (sizeUnitModel != null) {

            ArrayList<String> listUnit = new ArrayList<>();
            listUnit.add(0, getString(R.string.select));
            ArrayList<SizeUnitModel.Result> result = sizeUnitModel.getResult();
            if (result.size() != 0) {

                for (int i = 0; i < result.size(); i++) {

                    String strUnits = result.get(i).getSizeUnit();
                    listUnit.add(strUnits);
                }

                final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                        this, R.layout.custom_select_spinner, listUnit) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            // Disable the first item from Spinner
                            // First item will be use for hint
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray

                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));
                        }
                        return view;
                    }

                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        }
                        return view;
                    }
                };

                spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                snrCoverArea.setAdapter(spinnerArrayMs);
                spnrArea.setAdapter(spinnerArrayMs);
                spnrBuildArea.setAdapter(spinnerArrayMs);
                spnrSuperArea.setAdapter(spinnerArrayMs);
                spnrCarpetArea.setAdapter(spinnerArrayMs);


            }

        } else {
            Utils.showErrorMessage(R.string.something_wrong, this);
        }
    }

    private void addProject() {
        new CommonAsync(this, Urls.ADD_PROJECT,
                getInputJson(),
                "Loading...", this,
                Constants.ADD_PROJECT_API,
                Constants.POST).execute();
    }

    private String getInputJson() {

        slctArea = spnrArea.getSelectedItemPosition() + 1;
        slctCoverArea = snrCoverArea.getSelectedItemPosition() + 1;
        slctSuperArea = spnrSuperArea.getSelectedItemPosition() + 1;
        slctBuildUpArea = spnrBuildArea.getSelectedItemPosition() + 1;
        slctCarperarea = spnrCarpetArea.getSelectedItemPosition() + 1;

        slctBusStand = snrBusStand.getSelectedItemPosition() + 1;
        slctRailway = snrRailwayStation.getSelectedItemPosition() + 1;
        slctHospital = snrHospital.getSelectedItemPosition() + 1;
        slctDispensary = snrDispensary.getSelectedItemPosition() + 1;
        slctPoliceStation = snrPoliceStation.getSelectedItemPosition() + 1;
        slctAirport = snrAirport.getSelectedItemPosition() + 1;
        slctStaticHighway = snrStateHighway.getSelectedItemPosition() + 1;
        slctNationalHighway = snrNationalHighway.getSelectedItemPosition() + 1;

        JSONObject jsonObject = null;
        JSONObject jsonClub = null;
        JSONArray arrayClubFiles = null;
        JSONArray ProjectCategoryTypes = null;
        JSONArray ProjectFiles = null;
        JSONArray ProjectAmenities = null;
        JSONArray arrayClubAmeties = null;

        try {
            jsonObject = new JSONObject();
            jsonClub = new JSONObject();
            arrayClubFiles = new JSONArray();
            ProjectCategoryTypes = new JSONArray();
            ProjectFiles = new JSONArray();
            ProjectAmenities = new JSONArray();
            arrayClubAmeties = new JSONArray();

            jsonObject.put(Constants.Id, PreferenceConnector.getInstance(Project_name.this).loadSavedPreferences("ProjectId", ""));
            jsonObject.put(Constants.TownshipId, 0);
            jsonObject.put(Constants.Name, PreferenceConnector.getInstance(this).loadSavedPreferences("ProjectName", ""));
            jsonObject.put(Constants.TotalArea, PreferenceConnector.getInstance(this).loadSavedPreferences("TotalArea", ""));
            jsonObject.put(Constants.TotalAreaSizeUnitId, PreferenceConnector.getInstance(this).loadSavedPreferences("TotalAreaSizeUnitId", 0));
            jsonObject.put(Constants.AboutProject, about_project.getText().toString());
            jsonObject.put(Constants.AreaNagetiveMark, edtNegativeMark.getText().toString());

            jsonObject.put(Constants.TownshipName, PreferenceConnector.getInstance(this).loadSavedPreferences("TownshipName", ""));
            jsonObject.put(Constants.ZipCode, PreferenceConnector.getInstance(this).loadSavedPreferences("ZipCode", ""));
            jsonObject.put(Constants.StateName, PreferenceConnector.getInstance(this).loadSavedPreferences("StateName", ""));
            jsonObject.put(Constants.DistrictName, PreferenceConnector.getInstance(this).loadSavedPreferences("DistrictName", ""));
            jsonObject.put(Constants.TehsilName, PreferenceConnector.getInstance(this).loadSavedPreferences("TehsilName", ""));
            jsonObject.put(Constants.TownName, PreferenceConnector.getInstance(this).loadSavedPreferences("TownName", ""));
            jsonObject.put(Constants.LocalBodyType, PreferenceConnector.getInstance(this).loadSavedPreferences("LocalBodyType", ""));
            jsonObject.put(Constants.LocalBodyName, PreferenceConnector.getInstance(this).loadSavedPreferences("LocalBodyName", ""));
            jsonObject.put(Constants.DeveloperCompanyName, PreferenceConnector.getInstance(this).loadSavedPreferences("DeveloperCompanyName", ""));
            jsonObject.put(Constants.DeveloperCompanyLogo, PreferenceConnector.getInstance(this).loadSavedPreferences("DeveloperCompanyLogo", ""));
            jsonObject.put(Constants.CompanyId, PreferenceConnector.getInstance(this).loadSavedPreferences("CompanyId", 0));
            jsonObject.put(Constants.CreatedById, PreferenceConnector.getInstance(this).loadSavedPreferences("CreatedById", ""));
            jsonObject.put(Constants.UpdatedById, PreferenceConnector.getInstance(this).loadSavedPreferences("UpdatedById", ""));

            //ProjectCategoryTypes
            for (int i = 0; i < propertyTypeModelArrayList.size(); i++) {
                JSONObject object = new JSONObject();
                object.put(Constants.Id, 0);
                if (propertyTypeModelArrayList.get(i).getId() != "0")
                    object.put(Constants.CategoryTypeId, propertyTypeModelArrayList.get(i).getId());
                ProjectCategoryTypes.put(object);
            }

            jsonObject.put(Constants.ProjectCategoryTypes, ProjectCategoryTypes);

            //ProjectFiles
            if (!listTownshipModelZoning.isEmpty()) {
                for (int i = 0; i < listTownshipModelZoning.size(); i++) {
                    JSONObject object = new JSONObject();
                    object.put(Constants.Id, listTownshipModelZoning.get(i).getId());
                    object.put(Constants.FileId, listTownshipModelZoning.get(i).getFileId());
                    object.put(Constants.Category, listTownshipModelZoning.get(i).getCategory());
                    ProjectFiles.put(object);
                }
            }

            if (!listTownshipModelProject.isEmpty()) {
                for (int i = 0; i < listTownshipModelProject.size(); i++) {
                    JSONObject object = new JSONObject();
                    object.put(Constants.Id, listTownshipModelProject.get(i).getId());
                    object.put(Constants.FileId, listTownshipModelProject.get(i).getFileId());
                    object.put(Constants.Category, listTownshipModelProject.get(i).getCategory());
                    ProjectFiles.put(object);
                }
            }

            if (!listZoningFile.isEmpty()) {
                for (int i = 0; i < listZoningFile.size(); i++) {
                    JSONObject object = new JSONObject();
                    object.put(Constants.Category, "zoningVideos");
                    object.put(Constants.FileUrl, listZoningFile.get(i).getFileUrl());
                    ProjectFiles.put(object);
                }
            }

            if (!listTownshipFile.isEmpty()) {
                for (int i = 0; i < listTownshipFile.size(); i++) {
                    JSONObject object = new JSONObject();
                    object.put(Constants.Category, "ProjectVideos");
                    object.put(Constants.FileUrl, listTownshipFile.get(i).getFileUrl());
                    ProjectFiles.put(object);
                }
            }

            jsonObject.put(Constants.ProjectFiles, ProjectFiles);

            //ProjectAmenities
            if (!listSportFile.isEmpty()) {
                for (int i = 0; i < listSportFile.size(); i++) {
                    JSONObject object = new JSONObject();

                    JSONObject baseObject = new JSONObject();
                    object.put(Constants.Type, "Recreational Area");
                    object.put(Constants.Title, "Sports Facilities");
                    object.put(Constants.Value, listSportFile.get(i).getValue());
                    baseObject.put(Constants.Amenity, object);
                    ProjectAmenities.put(baseObject);
                }

            }

            if (!listAmusementFile.isEmpty()) {
                for (int i = 0; i < listAmusementFile.size(); i++) {
                    JSONObject baseObject = new JSONObject();
                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, "Recreational Area");
                    object.put(Constants.Title, "Amusement Parks");
                    object.put(Constants.Value, listAmusementFile.get(i).getValue());
                    baseObject.put(Constants.Amenity, object);
                    ProjectAmenities.put(baseObject);
                }
            }

            if (!listGreenParkFile.isEmpty()) {
                for (int i = 0; i < listGreenParkFile.size(); i++) {
                    JSONObject baseObject = new JSONObject();
                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, "Recreational Area");
                    object.put(Constants.Title, "Green Parks");
                    object.put(Constants.Value, listGreenParkFile.get(i).getValue());
                    baseObject.put(Constants.Amenity, object);
                    ProjectAmenities.put(baseObject);
                }
            }

            if (!edtEmpTheatr.getText().toString().equalsIgnoreCase("") && !edtEmpTheatr.getText().toString().equalsIgnoreCase(null)) {
                JSONObject baseObject = new JSONObject();
                JSONObject object = new JSONObject();
                object.put(Constants.Type, "Recreational Area");
                object.put(Constants.Title, "Amphitheatre");
                object.put(Constants.Value, edtEmpTheatr.getText().toString());
                baseObject.put(Constants.Amenity, object);
                ProjectAmenities.put(baseObject);

            }

            if (!edtBusStandLocation.getText().toString().equalsIgnoreCase("") && !edtBusStandLocation.getText().toString().equalsIgnoreCase(null)) {
                {
                    JSONObject jsonObject1 = new JSONObject();

                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, key_distance.getText().toString());
                    object.put(Constants.Title, busStand.getText().toString());
                    object.put(Constants.Value, edtBusStandLocation.getText().toString());
                    object.put(Constants.Size, edtBusStandDistance.getText().toString());
                    object.put(Constants.SizeUnitId, slctBusStand);
                    jsonObject1.put(Constants.Amenity, object);
                    ProjectAmenities.put(jsonObject1);
                }
            }

            if (!edtRailwayStationLocation.getText().toString().equalsIgnoreCase("") && !edtRailwayStationLocation.getText().toString().equalsIgnoreCase(null)) {
                {
                    JSONObject jsonObject1 = new JSONObject();

                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, key_distance.getText().toString());
                    object.put(Constants.Title, railwayStation.getText().toString());
                    object.put(Constants.Value, edtRailwayStationLocation.getText().toString());
                    object.put(Constants.Size, edtRailwayStationDistance.getText().toString());
                    object.put(Constants.SizeUnitId, slctRailway);
                    jsonObject1.put(Constants.Amenity, object);
                    ProjectAmenities.put(jsonObject1);
                }
            }

            if (!edtHospitalLocation.getText().toString().equalsIgnoreCase("") && !edtHospitalLocation.getText().toString().equalsIgnoreCase(null)) {
                {
                    JSONObject jsonObject1 = new JSONObject();

                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, key_distance.getText().toString());
                    object.put(Constants.Title, hospital.getText().toString());
                    object.put(Constants.Value, edtHospitalLocation.getText().toString());
                    object.put(Constants.Size, edtHospitalDistance.getText().toString());
                    object.put(Constants.SizeUnitId, slctHospital);
                    jsonObject1.put(Constants.Amenity, object);
                    ProjectAmenities.put(jsonObject1);
                }
            }

            if (!edtDispensaryLocation.getText().toString().equalsIgnoreCase("") && !edtDispensaryLocation.getText().toString().equalsIgnoreCase(null)) {
                {
                    JSONObject jsonObject1 = new JSONObject();

                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, key_distance.getText().toString());
                    object.put(Constants.Title, dispensary.getText().toString());
                    object.put(Constants.Value, edtDispensaryLocation.getText().toString());
                    object.put(Constants.Size, edtDispensaryDistance.getText().toString());
                    object.put(Constants.SizeUnitId, slctDispensary);
                    jsonObject1.put(Constants.Amenity, object);
                    ProjectAmenities.put(jsonObject1);
                }
            }

            if (!edtPoliceStationLocation.getText().toString().equalsIgnoreCase("") && !edtPoliceStationLocation.getText().toString().equalsIgnoreCase(null)) {
                {
                    JSONObject jsonObject1 = new JSONObject();

                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, key_distance.getText().toString());
                    object.put(Constants.Title, policeStation.getText().toString());
                    object.put(Constants.Value, edtPoliceStationLocation.getText().toString());
                    object.put(Constants.Size, edtPoliceStationDistance.getText().toString());
                    object.put(Constants.SizeUnitId, slctPoliceStation);
                    jsonObject1.put(Constants.Amenity, object);
                    ProjectAmenities.put(jsonObject1);
                }
            }

            if (!edtAirportLocation.getText().toString().equalsIgnoreCase("") && !edtAirportLocation.getText().toString().equalsIgnoreCase(null)) {
                {
                    JSONObject jsonObject1 = new JSONObject();

                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, key_distance.getText().toString());
                    object.put(Constants.Title, airport.getText().toString());
                    object.put(Constants.Value, edtAirportLocation.getText().toString());
                    object.put(Constants.Size, edtAirportDistance.getText().toString());
                    object.put(Constants.SizeUnitId, slctAirport);
                    jsonObject1.put(Constants.Amenity, object);
                    ProjectAmenities.put(jsonObject1);
                }
            }

            if (!edtStateHighwayLocation.getText().toString().equalsIgnoreCase("") && !edtStateHighwayLocation.getText().toString().equalsIgnoreCase(null)) {
                {
                    JSONObject jsonObject1 = new JSONObject();

                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, key_distance.getText().toString());
                    object.put(Constants.Title, stateHighway.getText().toString());
                    object.put(Constants.Value, edtStateHighwayLocation.getText().toString());
                    object.put(Constants.Size, edtStateHighwayDistance.getText().toString());
                    object.put(Constants.SizeUnitId, slctStaticHighway);
                    jsonObject1.put(Constants.Amenity, object);
                    ProjectAmenities.put(jsonObject1);
                }
            }

            if (!edtNationalHighwayLocation.getText().toString().equalsIgnoreCase("") && !edtNationalHighwayLocation.getText().toString().equalsIgnoreCase(null)) {
                {
                    JSONObject jsonObject1 = new JSONObject();

                    JSONObject object = new JSONObject();
                    object.put(Constants.Type, key_distance.getText().toString());
                    object.put(Constants.Title, nationalHighway.getText().toString());
                    object.put(Constants.Value, edtNationalHighwayLocation.getText().toString());
                    object.put(Constants.Size, edtNationalHighwayDistance.getText().toString());
                    object.put(Constants.SizeUnitId, slctNationalHighway);
                    jsonObject1.put(Constants.Amenity, object);
                    ProjectAmenities.put(jsonObject1);
                }
            }

            jsonObject.put(Constants.ProjectAmenities, ProjectAmenities);

            //club
            if (yes.isChecked()) {

                jsonClub.put(Constants.Name, edtClubNm.getText().toString());
                jsonClub.put(Constants.Description, "");
                jsonClub.put(Constants.Area, editArea.getText().toString());
                jsonClub.put(Constants.AreaSizeUnitId, slctArea);
                jsonClub.put(Constants.CoverAreaSize, edtCoverArea.getText().toString());
                jsonClub.put(Constants.CoverAreaSizeUnitId, slctCoverArea);
                jsonClub.put(Constants.SuperAreaSize, edtSuperArea.getText().toString());
                jsonClub.put(Constants.SuperAreaSizeUnitId, slctSuperArea);
                jsonClub.put(Constants.BuiltUpAreaSize, edtBuildUpArea.getText().toString());
                jsonClub.put(Constants.BuiltUpAreaSizeUnitId, slctBuildUpArea);
                jsonClub.put(Constants.CarpetAreaSize, edtCarpetArea.getText().toString());
                jsonClub.put(Constants.CarpetAreaSizeUnitId, slctCarperarea);

                for (int i = 0; i < listClubAlmetiesModel.size(); i++) {
                    JSONObject objAmenity = new JSONObject();
                    JSONObject obj = new JSONObject();

                    obj.put(Constants.Type, listClubAlmetiesModel.get(i).getType());
                    obj.put(Constants.Title, listClubAlmetiesModel.get(i).getTitle());
                    obj.put(Constants.Value, listClubAlmetiesModel.get(i).getValue());
                    obj.put(Constants.Size, listClubAlmetiesModel.get(i).getSize());
                    obj.put(Constants.SizeUnitId, listClubAlmetiesModel.get(i).getSizeUnitId());

                    objAmenity.put(Constants.Amenity, obj);
                    arrayClubAmeties.put(objAmenity);

                }

                jsonClub.put(Constants.ClubAmenities, arrayClubAmeties);

                for (int i = 0; i < listTownshipModelClub.size(); i++) {

                    JSONObject obj = new JSONObject();
                    obj.put(Constants.Id, listTownshipModelClub.get(i).getId());
                    obj.put(Constants.FileId, listTownshipModelClub.get(i).getFileId());
                    obj.put(Constants.Category, listTownshipModelClub.get(i).getCategory());

                    arrayClubFiles.put(obj);
                }

                for (int i = 0; i < listClubFile.size(); i++) {

                    JSONObject obj = new JSONObject();
                    obj.put(Constants.Category, "videos");
                    obj.put(Constants.FileUrl, listClubFile.get(i).getFileUrl());
                    arrayClubFiles.put(obj);
                }

                jsonClub.put(Constants.ClubFiles, arrayClubFiles);
                jsonObject.put(Constants.Club, jsonClub);

            } else {
                jsonObject.put(Constants.Club, jsonClub);
            }

            jsonObject.put(Constants.IsDeletable, false);
            jsonObject.put(Constants.IsModify, false);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject.toString();
    }

    public void onEvent(ArrayList<PropertyTypeModel> propertyTypeModels) {

        //propertyTypeModelArrayList.clear();

        PropertyTypeModel propertyTypeModel = new PropertyTypeModel();

        for (int i = 0; i < propertyTypeModels.size(); i++) {


            if (propertyTypeModels.get(i).getDelete().equalsIgnoreCase("yes")) {

                propertyTypeModel.setId(propertyTypeModels.get(i).getId());
                int pos = Integer.parseInt(propertyTypeModels.get(i).getId());

                //  propertyTypeModelArrayList.remove(pos - 1);
                propertyTypeModelArrayList.remove(id);

            } else {

                if (propertyTypeModels.get(i).getName().equalsIgnoreCase("") || propertyTypeModels.get(i).getName().equalsIgnoreCase(null)) {

                } else {
                    propertyTypeModel.setId(propertyTypeModels.get(i).getId());
                    propertyTypeModel.setName(propertyTypeModels.get(i).getName());


                    propertyTypeModelArrayList.add(propertyTypeModel);


                }
            }

            Log.e("PropList", "" + propertyTypeModelArrayList.size());
        }
    }

    @Override
    public void onResultListener(String result, String which) {
        if (which != null) {
            if (which.equalsIgnoreCase(Constants.UPLOAD_IMAGE_API)) {
                try {
                    JSONObject obj = new JSONObject(result);
                    int statusCode = obj.getInt("StatusCode");
                    if (statusCode == 200) {

                        JSONObject Result = obj.getJSONObject("Result");
                        JSONArray jsonArray = Result.getJSONArray("Result");

                        if (jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                strDeveloperLogo = object.getString("Url");
                                Log.d("TAG", "onResultListener: " + strDeveloperLogo);
                                String Id = object.getString("Id");


                                if (uploadImageType == 1) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "zoningPlan", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelZoning.add(townshipFileModel);
                                    notifyChangeImages1();

                                } else if (uploadImageType == 2) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "clubAminity", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelClub.add(townshipFileModel);

                                    notifyChangeImages();

                                } else if (uploadImageType == 3) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "images", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelProject.add(townshipFileModel);

                                    notifyChangeImages2();

                                }


                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.REMOVE_IMAGE_API)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        Utils.showSuccessErrorMessage("Success", "Image deleted successfully", "Ok", Project_name.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (which.equalsIgnoreCase(Constants.PROPERTY_LAY_DISPLAY)) {
                try {

                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.getJSONArray("Result");

                    if (type.equalsIgnoreCase("Residential")) {

                        propertyTypeModelArrayListResidential.clear();

                        for (int index = 0; index < jsonArray.length(); index++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(index);
                            PropertyTypeModel propertyTypeModel = new PropertyTypeModel();

                            propertyTypeModel.setId(jsonObject1.getString("Id"));
                            propertyTypeModel.setName(jsonObject1.getString("Name"));
                            propertyTypeModel.setType(jsonObject1.getString("Type"));
                            propertyTypeModel.setIsActive(jsonObject1.getString("IsActive"));

                            if (propertyTypeModel.getType().equalsIgnoreCase("Residential")) {

                                propertyTypeModelArrayListResidential.add(propertyTypeModel);
                                combiResComTypesArray.add(propertyTypeModel);
                                Log.e("names", propertyTypeModel.getName());
                            }
                        }

                    } else {

                        propertyTypeModelArrayListCommercial.clear();

                        for (int index = 0; index < jsonArray.length(); index++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(index);
                            PropertyTypeModel propertyTypeModel = new PropertyTypeModel();

                            propertyTypeModel.setId(jsonObject1.getString("Id"));
                            propertyTypeModel.setName(jsonObject1.getString("Name"));
                            propertyTypeModel.setType(jsonObject1.getString("Type"));
                            propertyTypeModel.setIsActive(jsonObject1.getString("IsActive"));

                            if (propertyTypeModel.getType().equalsIgnoreCase("Commercial")) {

                                propertyTypeModelArrayListCommercial.add(propertyTypeModel);
                                combiResComTypesArray.add(propertyTypeModel);
                                Log.e("names", propertyTypeModel.getName());
                            }


                        }

                    }


                    setResidentialAdapt();

                    setCommercial();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.GET_ALL_SIZE_UNITS_API)) {
                SizeUnitModel sizeUnitModel = Utils.getGsonObject().fromJson(result, SizeUnitModel.class);
                if (sizeUnitModel != null) {

                    ArrayList<String> listUnit = new ArrayList<>();
                    listUnit.add(0, getString(R.string.select));
                    ArrayList<SizeUnitModel.Result> results = sizeUnitModel.getResult();
                    if (results.size() != 0) {

                        for (int i = 0; i < results.size(); i++) {

                            String strUnits = results.get(i).getSizeUnit();
                            listUnit.add(strUnits);
                        }

                        final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                                this, R.layout.custom_select_spinner, listUnit) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @NonNull
                            @Override
                            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray

                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);
                                    // tv.setBackgroundColor(getResources().getColor(R.color.black));

                                } else {
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);
                                    // tv.setBackgroundColor(getResources().getColor(R.color.black));
                                }
                                return view;
                            }

                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);

                                } else {
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);

                                }
                                return view;
                            }
                        };

                        spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                        snrCoverArea.setAdapter(spinnerArrayMs);
                        spnrArea.setAdapter(spinnerArrayMs);
                        spnrBuildArea.setAdapter(spinnerArrayMs);
                        spnrSuperArea.setAdapter(spinnerArrayMs);
                        spnrCarpetArea.setAdapter(spinnerArrayMs);
                    }

                } else {
                    Utils.showErrorMessage(R.string.something_wrong, this);
                }

            } else if (which.equalsIgnoreCase(Constants.GET_ALL_SIZE_UNITS_LENGTH_API)) {
                SizeUnitModel sizeUnitModel = Utils.getGsonObject().fromJson(result, SizeUnitModel.class);
                if (sizeUnitModel != null) {

                    ArrayList<String> listUnit = new ArrayList<>();
                    listUnit.add(0, getString(R.string.select));
                    ArrayList<SizeUnitModel.Result> results = sizeUnitModel.getResult();
                    if (results.size() != 0) {

                        for (int i = 0; i < results.size(); i++) {

                            String strUnits = results.get(i).getSizeUnit();
                            listUnit.add(strUnits);
                        }

                        final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                                this, R.layout.custom_select_spinner, listUnit) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @NonNull
                            @Override
                            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray

                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);
                                    // tv.setBackgroundColor(getResources().getColor(R.color.black));

                                } else {
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);
                                    // tv.setBackgroundColor(getResources().getColor(R.color.black));
                                }
                                return view;
                            }

                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);

                                } else {
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);

                                }
                                return view;
                            }
                        };

                        spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                        snrBusStand.setAdapter(spinnerArrayMs);
                        snrRailwayStation.setAdapter(spinnerArrayMs);
                        snrHospital.setAdapter(spinnerArrayMs);
                        snrDispensary.setAdapter(spinnerArrayMs);
                        snrPoliceStation.setAdapter(spinnerArrayMs);
                        snrAirport.setAdapter(spinnerArrayMs);
                        snrStateHighway.setAdapter(spinnerArrayMs);
                        snrNationalHighway.setAdapter(spinnerArrayMs);
                    }

                } else {
                    Utils.showErrorMessage(R.string.something_wrong, this);
                }

            } else if (which.equalsIgnoreCase(Constants.ADD_PROJECT_API)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        String success = resultObject.getString("Success");
                        if (success.equals("true")) {
                            String message = resultObject.getString("Message");

                            JSONArray jsonArray = resultObject.getJSONObject("Result").getJSONArray("ProjectCategoryTypes");

                            proCatTypesArrayList.clear();


                            for (int i = 0; i < jsonArray.length(); i++) {

                                ProCategoryTypes proCategoryTypes = new ProCategoryTypes();

                                JSONObject dataObj = jsonArray.getJSONObject(i);
                                ProCatTypes proCatTypes = new ProCatTypes();
                                proCatTypes.setId(dataObj.getInt("Id"));
                                proCatTypes.setCategoryType(dataObj.getString("CategoryType"));
                                proCatTypes.setType(dataObj.getString("Type"));
                                proCategoryTypes.setCategoryType(dataObj.getString("CategoryType"));
                                proCatTypesArrayList.add(proCatTypes);

                            }

                            if (clickedFromNext) {

                                Utils.showSuccessErrorMessage("Success", message, "Ok", Project_name.this);
                            } else {

//                                Toast.makeText(this, "Project Added Successfully.", Toast.LENGTH_SHORT).show();
//                                finish();

                                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(this.getString(R.string.message))
                                        .setContentText(this.getString(R.string.tapping_yes))
                                        .setConfirmText(this.getString(R.string.yes))
                                        .setCancelText(this.getString(R.string.cancel))
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismissWithAnimation();
                                            }
                                        })

                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                finish();

                                            }
                                        })
                                        .show();


                            }

                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", Project_name.this);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void setCommercial() {
        propertyTypeModelArrayList.clear();
        GridLayoutManager gridlayoutManager3 = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        SelectTypeAdapter selectTypeAdapterCmcl = new SelectTypeAdapter(this, propertyTypeModelArrayListCommercial);
        recyclerCommercialType.setLayoutManager(gridlayoutManager3);
        recyclerCommercialType.setAdapter(selectTypeAdapterCmcl);
        selectTypeAdapterCmcl.notifyDataSetChanged();
    }

    private void setResidentialAdapt() {
        propertyTypeModelArrayList.clear();
        GridLayoutManager gridlayoutManager2 = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        SelectTypeAdapter slctRsdnlType = new SelectTypeAdapter(this, propertyTypeModelArrayListResidential);
        recyclerResidentialType.setLayoutManager(gridlayoutManager2);
        recyclerResidentialType.setAdapter(slctRsdnlType);
        slctRsdnlType.notifyDataSetChanged();
    }

    public void notifyChangeImages() {

        lnrClubImg.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelClub.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);


            Picasso.with(Project_name.this).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelClub.get(i).getUrl()).into(imgEvents);

            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelClub.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(Project_name.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(Project_name.this.getString(R.string.message))
                            .setContentText(Project_name.this.getString(R.string.are_you_sure))
                            .setConfirmText(Project_name.this.getString(R.string.yes))
                            .setCancelText(Project_name.this.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelClub.get(id).getFileId());
                                    listTownshipModelClub.remove(id);
                                    notifyChangeImages();
                                }
                            })
                            .show();

                }
            });

            lnrClubImg.addView(v);

        }


    }

    public void notifyChangeImages1() {

        uploadZoningPlan.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelZoning.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);

            String strurl = Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelZoning.get(i).getUrl();


            Picasso.with(Project_name.this).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelZoning.get(i).getUrl()).into(imgEvents);

            Log.e("TAG", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelZoning.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(Project_name.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(Project_name.this.getString(R.string.message))
                            .setContentText(Project_name.this.getString(R.string.are_you_sure))
                            .setConfirmText(Project_name.this.getString(R.string.yes))
                            .setCancelText(Project_name.this.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelZoning.get(id).getFileId());
                                    listTownshipModelZoning.remove(id);
                                    notifyChangeImages1();
                                }
                            })
                            .show();
                }
            });


            uploadZoningPlan.addView(v);

        }

    }

    public void notifyChangeImages2() {

        lnrProjectImg.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelProject.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);

            Picasso.with(Project_name.this).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelProject.get(i).getUrl()).into(imgEvents);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelProject.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(Project_name.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(Project_name.this.getString(R.string.message))
                            .setContentText(Project_name.this.getString(R.string.are_you_sure))
                            .setConfirmText(Project_name.this.getString(R.string.yes))
                            .setCancelText(Project_name.this.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelProject.get(id).getFileId());
                                    listTownshipModelProject.remove(id);
                                    notifyChangeImages2();
                                }
                            })
                            .show();
                }
            });

            lnrProjectImg.addView(v);

        }

    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent it = new Intent(this, ProjectNameSepecification.class);
            it.putExtra("fabList", propertyTypeModelArrayList);
            it.putExtra("selectedList", propertyTypeModelArrayList);
            it.putExtra("ProjectName", proName);

            startActivity(it);


        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        EventBus.getDefault().register(this);


    }

    @Override
    protected void onPause() {
        super.onPause();

        EventBus.getDefault().unregister(this);

    }

}
