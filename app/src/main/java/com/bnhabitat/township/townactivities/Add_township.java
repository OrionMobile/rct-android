package com.bnhabitat.township.townactivities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.township.adapter.AddMoreLocalityAdapter;
import com.bnhabitat.township.adapter.ClubFacilityAdapter;
import com.bnhabitat.township.adapter.EmbedClubVideoAdapter;
import com.bnhabitat.township.adapter.EmbedZoningVideoAdapter;
import com.bnhabitat.township.model.ClubAmenitiesModel;
import com.bnhabitat.township.model.ClubFilesModel;
import com.bnhabitat.township.model.GetLocationModel;
import com.bnhabitat.township.model.SizeUnitModel;
import com.bnhabitat.township.model.TownshipFile;
import com.bnhabitat.township.model.TownshipFileModel;
import com.bnhabitat.township.model.TownshipLocalitiesModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class Add_township extends Activity implements CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener {
    boolean istextChange = true;
    String encodedImage = "";

    @OnClick(R.id.imgBack)
    public void onImageBackClick() {
        finish();
    }

    @BindView(R.id.scrollView)
    ScrollView scrollView;

  /*  @BindView(R.id.mainLayout)
    LinearLayout mainLayout;
*/
    @OnClick(R.id.next)
    public void onNextClick() {

        if (isValid()) {

            if (!edtAddMore2.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore2.getText().toString().trim().equalsIgnoreCase(null)) {
                TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();
                townshipLocalitiesModel.setLocalityName(edtAddMore2.getText().toString());
                listTownshipLocalitiesModel.add(townshipLocalitiesModel);
            }

            if (!edtAddMore.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore.getText().toString().trim().equalsIgnoreCase(null)) {
                TownshipFile townshipFile = new TownshipFile();
                townshipFile.setUrl(edtAddMore.getText().toString().trim());
                listTownshipFile.add(townshipFile);
            }

            if (!edtAddMore3.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore3.getText().toString().trim().equalsIgnoreCase(null)) {
                ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();
                clubAmenitiesModel.setValue(edtAddMore3.getText().toString().trim());
                listClubAlmetiesModel.add(clubAmenitiesModel);
            }

            if (!edtAddMore4.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore4.getText().toString().trim().equalsIgnoreCase(null)) {
                TownshipFile townshipFile = new TownshipFile();
                townshipFile.setUrl(edtAddMore4.getText().toString().trim());
                listClubFile.add(townshipFile);
            }

            addTownship();
        } else {
            scrollView.scrollTo(10, 10);
        }
    }

    int id = 0;

    String encodedImageFormat;

    @BindView(R.id.checkLocalBody1)
    CheckBox checkLocalBody1;

    @BindView(R.id.checkLocalBody2)
    CheckBox checkLocalBody2;

    @BindView(R.id.checkLocalBody3)
    CheckBox checkLocalBody3;

    @BindView(R.id.checkLocalBody4)
    CheckBox checkLocalBody4;

    @BindView(R.id.edtNmName)
    EditText edtNmName;

    @BindView(R.id.edtDAuthorityNm)
    EditText edtDAuthorityNm;

    @BindView(R.id.edtHousingNm)
    EditText edtHousingNm;

    @BindView(R.id.edtCdmName)
    EditText edtCdmName;

    @BindView(R.id.edtDeveloperNm)
    EditText edtDeveloperNm;

    @BindView(R.id.edtClubNm)
    EditText edtClubNm;

    @BindView(R.id.lnrBodyType1)
    LinearLayout lnrBodyType1;

    @BindView(R.id.lnrBodyType2)
    LinearLayout lnrBodyType2;

    @BindView(R.id.lnrBodyType3)
    LinearLayout lnrBodyType3;

    @BindView(R.id.lnrBodyType4)
    LinearLayout lnrBodyType4;

    @BindView(R.id.lnrAddMore1)
    LinearLayout lnrAddMore1;

    @BindView(R.id.lnrAddMore3)
    LinearLayout lnrAddMore3;

    @BindView(R.id.lnrAddMore4)
    LinearLayout lnrAddMore4;

    @BindView(R.id.lnrClubFcltsAdded)
    LinearLayout lnrClubFcltsAdded;

    @BindView(R.id.lnrAddedEmbded)
    LinearLayout lnrAddedEmbded;

    @BindView(R.id.edtTownshipNm)
    EditText edtTownshipNm;

    @BindView(R.id.edtNegativeMark)
    EditText edtNegativeMark;

    @BindView(R.id.edtTotalArea)
    EditText edtTotalArea;

    @BindView(R.id.lnrSclLcltyArea)
    LinearLayout lnrSclLcltyArea;

    @BindView(R.id.lnrZoningImg)
    LinearLayout lnrZoningImg;

    @BindView(R.id.DevelopLogoImg)
    LinearLayout developLogoimg;

    ArrayList<TownshipLocalitiesModel> listAddMore1 = new ArrayList<>();
    ArrayList<TownshipFile> listAddMore2 = new ArrayList<>();

    ArrayList<ClubAmenitiesModel> listAddMore3 = new ArrayList<>();
    ArrayList<TownshipFile> listAddMore4 = new ArrayList<>();


    public static final int REQUEST_CAMERA = 5;

    public static final int SELECT_FILE = 0x3;
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    private static final int REQUEST_WRITE_PERMISSION = 786;
    private String pictureImagePath = "";

    @BindView(R.id.lnrAddEmbdedVideo)
    LinearLayout lnrAddEmbdedVideo;

    @BindView(R.id.lnrEmbededVideo)
    LinearLayout lnrEmbededVideo;

    @BindView(R.id.txtPin_zip)
    AutoCompleteTextView txtPin_zip;

    @BindView(R.id.spnrTotalArea)
    Spinner spnrTotalArea;

    @BindView(R.id.snrCoverArea)
    Spinner snrCoverArea;

    @BindView(R.id.spnrArea)
    Spinner spnrArea;

    @BindView(R.id.spnrBuildArea)
    Spinner spnrBuildArea;

    @BindView(R.id.spnrSuperArea)
    Spinner spnrSuperArea;

    @BindView(R.id.spnrCarpetArea)
    Spinner spnrCarpetArea;

    @BindView(R.id.edtCarpetArea)
    EditText edtCarpetArea;

    @BindView(R.id.edtBuildUpArea)
    EditText edtBuildUpArea;

    @BindView(R.id.edtSuperArea)
    EditText edtSuperArea;

    @BindView(R.id.edtCoverArea)
    EditText edtCoverArea;

    @BindView(R.id.editArea)
    EditText editArea;

    static int uploadImageType = 1;

    @BindView(R.id.lnrClubImg)
    LinearLayout lnrClubImg;

    @OnClick(R.id.lnrUploadDvlprLogo)
    public void uploadDevlprLogo() {

        if (listTownshipModelDevelop.size() < 1) {
            uploadImageType = 1;

            checkDrawerPermision();
        } else {
            Utils.showErrorMessage("You need to delete the previous image if you want to upload new image", Add_township.this);
        }
    }


    @OnClick(R.id.lnrZoningPlanImg)
    public void uploadZoningPlan() {
        uploadImageType = 2;
        checkDrawerPermision();
    }

    @OnClick(R.id.lnrClubImages)
    public void uploadClubImages() {
        uploadImageType = 3;
        checkDrawerPermision();
    }


    @BindView(R.id.txtstate)
    EditText txtstate;

    @BindView(R.id.txtDistrict)
    EditText txtDistrict;

    @BindView(R.id.txtTehsil)
    EditText txtTehsil;

    @BindView(R.id.isLayoutVisible)
    RadioGroup isLayoutVisible;

    @BindView(R.id.yes)
    RadioButton yes;

    @BindView(R.id.no)
    RadioButton no;

    @BindView(R.id.clubData)
    LinearLayout clubData;

    ArrayList<String> listSector = new ArrayList<>();

    ArrayList<TownshipFile> listTownshipFile = new ArrayList<>();

    ArrayList<TownshipFile> listClubFile = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModel = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelDevelop = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelZoning = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelClub = new ArrayList<>();

    ArrayList<ClubAmenitiesModel> listClubAlmetiesModel = new ArrayList<>();

    ArrayList<TownshipLocalitiesModel> listTownshipLocalitiesModel = new ArrayList<>();

    AddMoreLocalityAdapter addMoreLocalityAdapter;
    EmbedZoningVideoAdapter embedZoningVideoAdapter;
    ClubFacilityAdapter clubFacilityAdapter;
    EmbedClubVideoAdapter embedClubVideoAdapter;
    LinearLayoutManager linearLayoutManager;

    TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();
    TownshipFile townshipFile = new TownshipFile();
    ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();


    ArrayList<ClubFilesModel> listClubFilesModel = new ArrayList<>();

    static int slctAreaTownship = 1, slctArea = 1, slctCoverArea = 1, slctSuperArea = 1, slctBuildUpArea = 1, slctCarperarea = 1;

    String strLocalBodyName, strBodyType, strDeveloperLogo;

    EditText edtAddMore, edtAddMore2, edtAddMore3, edtAddMore4;
    ImageView imgCloseAddMore, imgCloseAddMore2, imgCloseAddMore3, imgCloseAddMore4;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_township);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        ButterKnife.bind(this);

        townshipLocalitiesModel.setLocalityName("");
        listAddMore1.add(townshipLocalitiesModel);
        addStrLcttyArea(listAddMore1);

        townshipFile.setUrl("");
        listAddMore2.add(townshipFile);
        addEmbededVideo(listAddMore2);

        clubAmenitiesModel.setValue("");
        listAddMore3.add(clubAmenitiesModel);
        addClubFacilities(listAddMore3);

        townshipFile.setUrl("");
        listAddMore4.add(townshipFile);
        addClubEmbededVideo(listAddMore4);

        strBodyType = getResources().getString(R.string.muncipal_corporation);

        //    getAllSizeUnits(Constants.AREA);
        //   authenticateUser();

        String email = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        String password = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PASSWORD, "");

        loginUser(email, password);

        getInputJson();

        txtPin_zip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (txtPin_zip.getText().length() == 6) {
                    getLocationByPin(txtPin_zip.getText().toString());
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        checkLocalBody1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.VISIBLE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody2.setChecked(false);
                    checkLocalBody3.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.muncipal_corporation);
                }

            }
        });


        checkLocalBody2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.VISIBLE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody3.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.development_auth);
                }

            }
        });


        checkLocalBody3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.VISIBLE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody2.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.housing_board);
                }

            }
        });


        checkLocalBody4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.VISIBLE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody2.setChecked(false);
                    checkLocalBody3.setChecked(false);

                    strBodyType = getResources().getString(R.string.cantonment_development);
                }

            }
        });

        lnrBodyType1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtNmName.setVisibility(View.VISIBLE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(true);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.muncipal_corporation);


            }
        });

        lnrBodyType2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.VISIBLE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(true);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.development_auth);
            }
        });

        lnrBodyType3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.VISIBLE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(true);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.housing_board);
            }
        });

        lnrBodyType4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.VISIBLE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(true);

                strBodyType = getResources().getString(R.string.cantonment_development);
            }
        });

        yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    clubData.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    clubData.setVisibility(View.GONE);
                    yes.setChecked(false);
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        EventBus.getDefault().register(this);


    }

    @Override
    protected void onPause() {
        super.onPause();

        EventBus.getDefault().unregister(this);

    }

    public void addStrLcttyArea(final ArrayList<TownshipLocalitiesModel> list) {

        lnrSclLcltyArea.removeAllViews();

        listTownshipLocalitiesModel = list;

        for (int i = 0; i < list.size(); i++) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore2 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore2.setText(list.get(i).getLocalityName());

            imgCloseAddMore2 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore2.setId(i);

            edtAddMore2.setId(i);

            if (i == 0) {
                imgCloseAddMore2.setVisibility(View.GONE);
                if (!edtAddMore2.getText().toString().equalsIgnoreCase(""))
                    edtAddMore2.setText(list.get(i).getLocalityName());
                else
                    edtAddMore2.setHint("");
            }

            final int finalI = i;

            imgCloseAddMore2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();
                            townshipLocalitiesModel.setLocalityName(text);
                            list.add(list.size() - 1, townshipLocalitiesModel);
                        }
                    }

                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);
                    addStrLcttyArea(list);
                }
            });

            lnrSclLcltyArea.addView(v);

        }

        lnrAddMore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();

                if (!edtAddMore2.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getLocalityName().equalsIgnoreCase("")) {
                        townshipLocalitiesModel.setLocalityName("");
                        list.add(townshipLocalitiesModel);
                    } else {
                        townshipLocalitiesModel.setLocalityName(edtAddMore2.getText().toString().trim());
                        list.add(list.size() - 1, townshipLocalitiesModel);
                    }
                    addStrLcttyArea(list);
                } else {
                    Utils.showErrorMessage("Please add locality", Add_township.this);
                }
            }
        });
    }


    public void addEmbededVideo(final ArrayList<TownshipFile> list) {

        lnrEmbededVideo.removeAllViews();
        listTownshipFile = list;
        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore.setText(list.get(i).getFileUrl());

            imgCloseAddMore = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore.setId(i);

            edtAddMore.setId(i);

            if (i == 0) {
                imgCloseAddMore.setVisibility(View.GONE);
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore.setText(list.get(i).getFileUrl());
                } else
                    edtAddMore.setHint(getResources().getString(R.string.add_embed_youtube));
            }

            final int finalI = i;

            imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();

                    if (!text.equalsIgnoreCase("")) {
                        /*if(!text.equalsIgnoreCase(t.getText().toString())) {
                            list.add(list.size() - 1, text);
                        }*/
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            TownshipFile townshipFile = new TownshipFile();
                            townshipFile.setUrl(text);
                            list.add(list.size() - 1, townshipFile);
                        }
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addEmbededVideo(list);
                }
            });

            lnrEmbededVideo.addView(v);
        }

        lnrAddEmbdedVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = edtAddMore.getText().toString().trim();
                TownshipFile townshipFile = new TownshipFile();
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getFileUrl().equalsIgnoreCase("")) {

                        townshipFile.setUrl("");
                        list.add(townshipFile);

                    } else {
                        townshipFile.setUrl(edtAddMore.getText().toString());
                        list.add(list.size() - 1, townshipFile);
                    }

                    addEmbededVideo(list);
                } else {
                    Utils.showErrorMessage("Please add url", Add_township.this);
                }
            }
        });

    }


    public void addClubFacilities(final ArrayList<ClubAmenitiesModel> list) {
        lnrClubFcltsAdded.removeAllViews();
        listClubAlmetiesModel = list;

        for (int i = 0; i < list.size(); i++) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore3 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore3.setText(list.get(i).getValue());
            imgCloseAddMore3 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore3.setId(i);

            edtAddMore3.setId(i);

            if (i == 0) {
                imgCloseAddMore3.setVisibility(View.GONE);
                if (!edtAddMore3.getText().toString().equalsIgnoreCase(""))
                    edtAddMore3.setText(list.get(i).getValue());
                else
                    edtAddMore3.setHint("");
            }

            final int finalI = i;
            imgCloseAddMore3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();
                    if (!text.equalsIgnoreCase("")) {

                        if (!text.equalsIgnoreCase(t.getText().toString())) {

                            ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();
                            clubAmenitiesModel.setValue(text);
                            list.add(list.size() - 1, clubAmenitiesModel);
                        }
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);
                    addClubFacilities(list);
                }
            });

            lnrClubFcltsAdded.addView(v);

        }

        lnrAddMore3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClubAmenitiesModel clubAmenitiesModel = new ClubAmenitiesModel();

                if (!edtAddMore3.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getValue().equalsIgnoreCase("")) {
                        clubAmenitiesModel.setValue("");
                        list.add(clubAmenitiesModel);
                    } else {
                        clubAmenitiesModel.setValue(edtAddMore3.getText().toString().trim());
                        list.add(list.size() - 1, clubAmenitiesModel);
                    }
                    addClubFacilities(list);
                } else {
                    Utils.showErrorMessage("Please add facility", Add_township.this);
                }
            }
        });
    }

    public void addClubEmbededVideo(final ArrayList<TownshipFile> list) {

        lnrAddedEmbded.removeAllViews();
        listClubFile = list;

        for (int i = 0; i < list.size(); i++) {
            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore4 = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore4.setText(list.get(i).getFileUrl());

            imgCloseAddMore4 = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore4.setId(i);

            edtAddMore4.setId(i);

            if (i == 0) {
                imgCloseAddMore4.setVisibility(View.GONE);
                if (!edtAddMore4.getText().toString().equalsIgnoreCase(""))
                    edtAddMore4.setText(list.get(i).getFileUrl());
                else
                    edtAddMore4.setHint(getResources().getString(R.string.add_embed_youtube));
            }


            final int finalI = i;

            imgCloseAddMore4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();
                    if (!text.equalsIgnoreCase("")) {

                        if (!text.equalsIgnoreCase(t.getText().toString())) {

                            TownshipFile townshipFile = new TownshipFile();
                            townshipFile.setUrl(text);
                            list.add(list.size() - 1, townshipFile);

                        }
                    }
                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);
                    addClubEmbededVideo(list);
                }
            });

            lnrAddedEmbded.addView(v);

        }

        lnrAddMore4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TownshipFile townshipFile = new TownshipFile();
                if (!edtAddMore4.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getFileUrl().equalsIgnoreCase("")) {
                        townshipFile.setUrl("");
                        list.add(list.size() - 1, townshipFile);

                    } else {
                        townshipFile.setUrl(edtAddMore4.getText().toString());
                        list.add(list.size() - 1, townshipFile);
                    }
                    addClubEmbededVideo(list);
                } else {
                    Utils.showErrorMessage("Please add Url", Add_township.this);
                }
            }
        });

    }

    private void checkDrawerPermision() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_WRITE_PERMISSION);

//            if (checkSelfPermission(Manifest.permission.CAMERA)
//                    != PackageManager.PERMISSION_GRANTED) {
//                requestPermissions(new String[]{Manifest.permission.CAMERA},
//                        MY_CAMERA_REQUEST_CODE);
        } else {
            onPickImageCamera();
        }


    }


    public void onPickImageCamera() {


        final CharSequence[] items = {"Take Photo", "Select photo from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select your option");
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Select photo from gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            String encodedImage = Utils.encodeImage(thumbnail);

            uploadFile(encodedImage);

        } else if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {


            Uri selectedImageUri = data.getData();


            try {
                InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                encodedImage = Utils.encodeImage(selectedImage);

                uploadFile(encodedImage);

//                if (uploadImageType == 1) {
//                    zoningPlanImage.setVisibility(View.VISIBLE);
//                    zoningPlanImage.setImageBitmap(selectedImage);
//
//                    uploadFile(encodedImage);
//                }
//

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPickImageCamera();
        }

    }


    private void getLocationByPin(String strValue) {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PIN_DETAIL + strValue.trim(),
                "",
                "Loading..",
                this,
                Constants.GET_LOCATION_API,
                Constants.GET).execute();
    }

    public void onEvent(final GetLocationModel getLocationModel) {
        if (getLocationModel != null) {

            GetLocationModel.Result result = getLocationModel.getResult();
            if (result != null) {

                txtstate.setText(result.getStateName());
                txtDistrict.setText(result.getDistrictName());

            }

        } else {
            Utils.showErrorMessage(R.string.something_wrong, this);
        }
    }

    private void getAllSizeUnits(String type) {


        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/" + type,
                "",
                "Loading..",
                this,
                Constants.GET_ALL_SIZE_UNITS_API,
                Constants.GET).execute();

    }

    public void onEvent(SizeUnitModel sizeUnitModel) {
        if (sizeUnitModel != null) {

            ArrayList<String> listUnit = new ArrayList<>();
            listUnit.add(0, getString(R.string.select));
            ArrayList<SizeUnitModel.Result> result = sizeUnitModel.getResult();
            if (result.size() != 0) {

                for (int i = 0; i < result.size(); i++) {

                    String strUnits = result.get(i).getSizeUnit();
                    listUnit.add(strUnits);
                }

                final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                        this, R.layout.custom_select_spinner, listUnit) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            // Disable the first item from Spinner
                            // First item will be use for hint
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray

                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));
                        }
                        return view;
                    }

                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        }
                        return view;
                    }
                };

                spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                spnrTotalArea.setAdapter(spinnerArrayMs);
                snrCoverArea.setAdapter(spinnerArrayMs);
                spnrArea.setAdapter(spinnerArrayMs);
                spnrBuildArea.setAdapter(spinnerArrayMs);
                spnrSuperArea.setAdapter(spinnerArrayMs);
                spnrCarpetArea.setAdapter(spinnerArrayMs);


            }

        } else {
            Utils.showErrorMessage(R.string.something_wrong, this);
        }
    }

    private void addTownship() {

        new CommonAsync(this, Urls.ADD_TOWNSHIP,
                getInputJson(),
                "Loading..",
                this,
                Constants.ADD_TOWNSHIP_API,
                Constants.POST).execute();

    }

    private String getInputJson() {


        slctAreaTownship = spnrTotalArea.getSelectedItemPosition() + 1;
        slctArea = spnrArea.getSelectedItemPosition() + 1;
        slctCoverArea = snrCoverArea.getSelectedItemPosition() + 1;
        slctSuperArea = spnrSuperArea.getSelectedItemPosition() + 1;
        slctBuildUpArea = spnrBuildArea.getSelectedItemPosition() + 1;
        slctCarperarea = spnrCarpetArea.getSelectedItemPosition() + 1;


        JSONObject jsonObject = null;
        JSONArray arrayTownship = null;
        JSONArray arrayClubAmeties = null;
        JSONArray arrayClubFiles = null;
        JSONArray arrayTownshipLocalities = null;
        JSONObject jsonClub = null;

        try {
            jsonObject = new JSONObject();
            arrayTownship = new JSONArray();
            arrayClubAmeties = new JSONArray();
            arrayClubFiles = new JSONArray();
            arrayTownshipLocalities = new JSONArray();
            jsonClub = new JSONObject();

            jsonObject.put(Constants.Id, 0);
            jsonObject.put(Constants.Name, edtTownshipNm.getText().toString());
            jsonObject.put(Constants.TotalArea, edtTotalArea.getText().toString());
            jsonObject.put(Constants.TotalAreaSizeUnitId, slctAreaTownship);
            jsonObject.put(Constants.ZipCode, txtPin_zip.getText().toString());
            jsonObject.put(Constants.StateName, txtstate.getText().toString());
            jsonObject.put(Constants.DistrictName, txtDistrict.getText().toString());
            jsonObject.put(Constants.TehsilName, txtTehsil.getText().toString());
            jsonObject.put(Constants.LocalBodyType, strBodyType);
            jsonObject.put(Constants.LocalBodyName, strLocalBodyName);
            jsonObject.put(Constants.DeveloperCompanyName, edtDeveloperNm.getText().toString());
            if (listTownshipModelDevelop.isEmpty())
                jsonObject.put(Constants.DeveloperCompanyLogo, "");
            else
                jsonObject.put(Constants.DeveloperCompanyLogo, listTownshipModelDevelop.get(0).getUrl());

            jsonObject.put(Constants.AreaNegativeMark, edtNegativeMark.getText().toString());
            jsonObject.put(Constants.CompanyId, 1);
            jsonObject.put(Constants.CreatedById, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put(Constants.UpdatedById, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));

            for (int i = 0; i < listTownshipModelZoning.size(); i++) {
                JSONObject obj = new JSONObject();
                obj.put(Constants.Id, listTownshipModelZoning.get(i).getId());
                obj.put(Constants.FileId, listTownshipModelZoning.get(i).getFileId());
                obj.put(Constants.Category, listTownshipModelZoning.get(i).getCategory());
                arrayTownship.put(obj);
            }


            for (int i = 0; i < listTownshipFile.size(); i++) {

                JSONObject obj = new JSONObject();
                obj.put(Constants.Category, "videos");
                obj.put(Constants.FileUrl, listTownshipFile.get(i).getFileUrl());
                arrayTownship.put(obj);


            }

            jsonObject.put(Constants.TownshipFiles, arrayTownship);

            for (int i = 0; i < listTownshipLocalitiesModel.size(); i++) {


                JSONObject obj = new JSONObject();
                obj.put(Constants.Id, listTownshipLocalitiesModel.get(i).getId());
                obj.put(Constants.LocalityName, listTownshipLocalitiesModel.get(i).getLocalityName());

                arrayTownshipLocalities.put(obj);

            }

            jsonObject.put(Constants.TownshipLocalities, arrayTownshipLocalities);

            if (yes.isChecked()) {

                jsonClub.put(Constants.Name, edtClubNm.getText().toString());
                jsonClub.put(Constants.Description, "");
                jsonClub.put(Constants.Area, editArea.getText().toString());
                jsonClub.put(Constants.AreaSizeUnitId, slctArea);
                jsonClub.put(Constants.CoverAreaSize, edtCoverArea.getText().toString());
                jsonClub.put(Constants.CoverAreaSizeUnitId, slctCoverArea);
                jsonClub.put(Constants.SuperAreaSize, edtSuperArea.getText().toString());
                jsonClub.put(Constants.SuperAreaSizeUnitId, slctSuperArea);
                jsonClub.put(Constants.BuiltUpAreaSize, edtBuildUpArea.getText().toString());
                jsonClub.put(Constants.BuiltUpAreaSizeUnitId, slctBuildUpArea);
                jsonClub.put(Constants.CarpetAreaSize, edtCarpetArea.getText().toString());
                jsonClub.put(Constants.CarpetAreaSizeUnitId, slctCarperarea);

                for (int i = 0; i < listClubAlmetiesModel.size(); i++) {


                    JSONObject objAmenity = new JSONObject();

                    JSONObject obj = new JSONObject();

                    obj.put(Constants.Type, listClubAlmetiesModel.get(i).getType());
                    obj.put(Constants.Title, listClubAlmetiesModel.get(i).getTitle());
                    obj.put(Constants.Value, listClubAlmetiesModel.get(i).getValue());
                    obj.put(Constants.Size, listClubAlmetiesModel.get(i).getSize());
                    obj.put(Constants.SizeUnitId, listClubAlmetiesModel.get(i).getSizeUnitId());

                    objAmenity.put(Constants.Amenity, obj);
                    arrayClubAmeties.put(objAmenity);

                }

                jsonClub.put(Constants.ClubAmenities, arrayClubAmeties);

                for (int i = 0; i < listTownshipModelClub.size(); i++) {

                    JSONObject obj = new JSONObject();
                    obj.put(Constants.Id, listTownshipModelClub.get(i).getId());
                    obj.put(Constants.FileId, listTownshipModelClub.get(i).getFileId());
                    obj.put(Constants.Category, listTownshipModelClub.get(i).getCategory());

                    arrayClubFiles.put(obj);
                }

                for (int i = 0; i < listClubFile.size(); i++) {

                    JSONObject obj = new JSONObject();
                    obj.put(Constants.Category, "videos");
                    obj.put(Constants.FileUrl, listClubFile.get(i).getFileUrl());
                    arrayClubFiles.put(obj);
                }

                jsonClub.put(Constants.ClubFiles, arrayClubFiles);
                jsonObject.put(Constants.Club, jsonClub);


            } else {
                jsonObject.put(Constants.Club, "");

            }

            jsonObject.put(Constants.IsDeletable, false);
            jsonObject.put(Constants.IsModify, false);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject.toString();
    }


    public boolean isValid() {

        boolean isValid = false;

        if (!edtNmName.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtNmName.getText().toString();
        } else if (!edtDAuthorityNm.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtDAuthorityNm.getText().toString();
        } else if (!edtHousingNm.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtHousingNm.getText().toString();
        } else if (!edtCdmName.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtCdmName.getText().toString();
        } else {
            strLocalBodyName = edtNmName.getText().toString();
        }

        if (txtPin_zip.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_zip), txtPin_zip, this);
            return false;
        }
        if (txtstate.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_state), txtstate, this);
            return false;
        }
        if (txtDistrict.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_district), txtDistrict, this);
            return false;
        }

        if (checkLocalBody1.isChecked()) {

            if (edtNmName.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_mc_name), edtNmName, this);
                return false;
            }
        }

        if (checkLocalBody2.isChecked()) {

            if (edtDAuthorityNm.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_development_auth_nm), edtDAuthorityNm, this);
                return false;
            }
        }

        if (checkLocalBody3.isChecked()) {

            if (edtHousingNm.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_housing_board_nm), edtHousingNm, this);
                return false;
            }
        }

        if (checkLocalBody4.isChecked()) {

            if (edtCdmName.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_cantonment_nm), edtCdmName, this);
                return false;
            }
        }

        if (edtDeveloperNm.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_developer_name), edtDeveloperNm, this);
            return false;
        }

        if (edtTownshipNm.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_township_name), edtTownshipNm, this);
            return false;
        } else if (strLocalBodyName.equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_body_nm), this);
            return false;
        }

        return true;


    }

    private void uploadFile(String urlImage) {


        new CommonAsync(this, Urls.UPLOAD_IMAGE,
                getInputJson(urlImage),
                "Loading..",
                this,
                Constants.UPLOAD_IMAGE_API,
                Constants.POST).execute();

    }

    private void removeFile(String id) {


        new CommonAsync(this, Urls.REMOVE_IMAGE,
                getInputJsonId(id),
                "Loading..",
                this,
                Constants.REMOVE_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJsonId(String id) {

        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }


    private void authenticateUser(String authKey) {


        new CommonSyncwithoutstatus(this, Urls.AUTHENTICATE_USER,
                getInputJson(0, authKey),
                "Loading..",
                this,
                Constants.AUTHENTICATE_USER_API,
                Constants.POST).execute();

    }


    private void loginUser(String email, String password) {

        new CommonAsync(this, Urls.LOGIN_USER,
                getInputJson(email, password),
                "Loading..",
                this,
                Constants.LOGIN_USER_API,
                Constants.POST).execute();

    }

    private String getInputJson(String email, String password) {

        JSONObject jsonObject = null;

        jsonObject = new JSONObject();

        try {

            jsonObject.put("EmailOrPhone", email);
            jsonObject.put("Password", password);
            jsonObject.put("DeviceId", Utils.getDeviceId(this));
            jsonObject.put("RegistrationId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.REGISTRATION_ID, ""));
            jsonObject.put("DeviceName", "Android");
//
            jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonObject.put("Platform", "Android");
            jsonObject.put("AppId", Integer.parseInt("1"));

        } catch (Exception e) {

        }

        return jsonObject.toString();

    }


    private String getInputJson(int val, String authKey) {

        JSONObject jsonObject = null;

        jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put(Constants.AUTH_KEY, authKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    private String getInputJson(String urlImage) {
        String strREq = "";
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;

        jsonObject = new JSONObject();
        jsonArray = new JSONArray();
        try {

            jsonObject.put(Constants.CompanyId, 1);
            jsonObject.put(Constants.Name, "township");
            jsonObject.put(Constants.Type, Constants.jpg);
            jsonObject.put(Constants.ImageCode, urlImage);
            jsonArray.put(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        strREq = jsonArray.toString();
        //strREq = strREq.replaceAll("\\\\", "\\\\\\");

        return strREq;
    }

    @Override
    public void onResultListener(String output, String which) {

        String w = which;

        if (which != null) {
            if (which.equalsIgnoreCase(Constants.LOGIN_USER_API)) {
                try {

                    JSONObject jsonObject = new JSONObject(output);

                    JSONObject obj = jsonObject.getJSONObject("Result");
                    JSONObject UserDetail = obj.getJSONObject("UserDetail");

                    String user_Id = UserDetail.getJSONObject("UserDetail").getString("Id");
                    String authKey = UserDetail.getJSONObject("UserDetail").getString("AuthKey");

                    PreferenceConnector.getInstance(this).savePreferences(Constants.USER_ID, user_Id);

                    if (authKey != null) {
                        authenticateUser(authKey);

                    } else {
                        Utils.showErrorMessage(getString(R.string.something_wrong), this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Constants.AUTHENTICATE_USER_API)) {
                try {

                    JSONObject jsonObject = new JSONObject(output);
                    String AuthToken = jsonObject.getString("AuthToken");
                    PreferenceConnector.getInstance(this).savePreferences(Constants.AUTHENTICATION_KEY, AuthToken);
                    getAllSizeUnits(Constants.AREA);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.GET_ALL_SIZE_UNITS_API)) {
                SizeUnitModel sizeUnitModel = Utils.getGsonObject().fromJson(output, SizeUnitModel.class);
                if (sizeUnitModel != null) {

                    ArrayList<String> listUnit = new ArrayList<>();
                    listUnit.add(0, getString(R.string.select));
                    ArrayList<SizeUnitModel.Result> result = sizeUnitModel.getResult();
                    if (result.size() != 0) {

                        for (int i = 0; i < result.size(); i++) {

                            String strUnits = result.get(i).getSizeUnit();
                            listUnit.add(strUnits);
                        }

                        final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                                this, R.layout.custom_select_spinner, listUnit) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @NonNull
                            @Override
                            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray

                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);
                                    // tv.setBackgroundColor(getResources().getColor(R.color.black));

                                } else {
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);
                                    // tv.setBackgroundColor(getResources().getColor(R.color.black));
                                }
                                return view;
                            }

                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);

                                } else {
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);

                                }
                                return view;
                            }
                        };

                        spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                        spnrTotalArea.setAdapter(spinnerArrayMs);
                        snrCoverArea.setAdapter(spinnerArrayMs);
                        spnrArea.setAdapter(spinnerArrayMs);
                        spnrBuildArea.setAdapter(spinnerArrayMs);
                        spnrSuperArea.setAdapter(spinnerArrayMs);
                        spnrCarpetArea.setAdapter(spinnerArrayMs);


                    }

                } else {
                    Utils.showErrorMessage(R.string.something_wrong, this);
                }

            } else if (which.equalsIgnoreCase(Constants.UPLOAD_IMAGE_API)) {
                try {
                    JSONObject obj = new JSONObject(output);
                    int statusCode = obj.getInt("StatusCode");
                    if (statusCode == 200) {

                        JSONObject Result = obj.getJSONObject("Result");
                        JSONArray jsonArray = Result.getJSONArray("Result");

                        if (jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                strDeveloperLogo = object.getString("Url");
                                Log.d("TAG", "onResultListener: " + strDeveloperLogo);
                                String Id = object.getString("Id");


                                if (uploadImageType == 1) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "developer_logo", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelDevelop.add(townshipFileModel);
                                    notifyChangeImages1();

                                } else if (uploadImageType == 2) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "zoningPlan", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelZoning.add(townshipFileModel);

                                    notifyChangeImages();

                                } else if (uploadImageType == 3) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "clubAminity", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelClub.add(townshipFileModel);

                                    notifyChangeImages2();

                                }

                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.REMOVE_IMAGE_API)) {

                try {
                    JSONObject jsonObject = new JSONObject(output);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        Utils.showSuccessErrorMessage("Success", "Image deleted successfully", "Ok", Add_township.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Constants.ADD_TOWNSHIP_API)) {
                try {
                    JSONObject jsonObject = new JSONObject(output);
                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        String success = resultObject.getString("Success");
                        if (success.equals("true")) {
                            String message = resultObject.getString("Message");
                            Utils.showSuccessErrorMessage("Success", message, "Ok", Add_township.this);
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", Add_township.this);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }


        Log.e("which", "" + which);


    }


    public void onEvent(String authKey) {
        if (authKey != null) {
            authenticateUser(authKey);

        } else {
            Utils.showErrorMessage(getString(R.string.something_wrong), this);
        }
    }

    public void onEvent(JSONObject jsonObject) {
        if (jsonObject != null) {
            String AuthToken = null;
            try {
                AuthToken = jsonObject.getString("AuthToken");
                PreferenceConnector.getInstance(this).savePreferences(Constants.AUTHENTICATION_KEY, AuthToken);
                getAllSizeUnits(Constants.AREA);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(Add_township.this, Add_Project.class);
            startActivity(intent);
            finish();
        }
    }

    public void notifyChangeImages() {

        lnrZoningImg.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelZoning.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);


            Picasso.with(Add_township.this).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelZoning.get(i).getUrl()).into(imgEvents);

            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelZoning.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(Add_township.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(Add_township.this.getString(R.string.message))
                            .setContentText(Add_township.this.getString(R.string.are_you_sure))
                            .setConfirmText(Add_township.this.getString(R.string.yes))
                            .setCancelText(Add_township.this.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelZoning.get(id).getFileId());
                                    listTownshipModelZoning.remove(id);
                                    notifyChangeImages();
                                }
                            })
                            .show();

                }
            });

            lnrZoningImg.addView(v);

        }


    }

    public void notifyChangeImages1() {

        developLogoimg.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelDevelop.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);

            String strurl = Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl();


            Picasso.with(Add_township.this).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl()).into(imgEvents);

            Log.e("TAG", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(Add_township.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(Add_township.this.getString(R.string.message))
                            .setContentText(Add_township.this.getString(R.string.are_you_sure))
                            .setConfirmText(Add_township.this.getString(R.string.yes))
                            .setCancelText(Add_township.this.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelDevelop.get(id).getFileId());
                                    listTownshipModelDevelop.remove(id);
                                    notifyChangeImages1();
                                }
                            })
                            .show();
                }
            });


            developLogoimg.addView(v);

        }

    }

    public void notifyChangeImages2() {

        lnrClubImg.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelClub.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);

            Picasso.with(Add_township.this).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelClub.get(i).getUrl()).into(imgEvents);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelClub.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(Add_township.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(Add_township.this.getString(R.string.message))
                            .setContentText(Add_township.this.getString(R.string.are_you_sure))
                            .setConfirmText(Add_township.this.getString(R.string.yes))
                            .setCancelText(Add_township.this.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelClub.get(id).getFileId());
                                    listTownshipModelClub.remove(id);
                                    notifyChangeImages2();
                                }
                            })
                            .show();
                }
            });

            lnrClubImg.addView(v);

        }

    }

    private boolean isValid(String urlString) {
        try {
            URL url = new URL(urlString);
            return URLUtil.isValidUrl(urlString) && Patterns.WEB_URL.matcher(urlString).matches();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

}
