package com.bnhabitat.township.townactivities;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.township.fragment.Commercial_Legal_Fragment;
import com.bnhabitat.township.fragment.Industrial_Legal_Fragment;
import com.bnhabitat.township.fragment.Residential_Legal_Fragment;

import java.util.ArrayList;

public class LegalActivity extends AppCompatActivity implements TabChangeListener{

    TabLayout.Tab Tab1, Tab2, Tab3;
    TabLayout tabLayout;
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        Tab1 = tabLayout.newTab().setText("Residential");
        Tab2 = tabLayout.newTab().setText("Commercial");
      //  Tab3 = tabLayout.newTab().setText("Industrial");
        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
      //  tabLayout.addTab(Tab3);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Residential_Legal_Fragment residential_legal_fragment = new Residential_Legal_Fragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_legal, residential_legal_fragment).commit();
        imgBack= (ImageView) findViewById(R.id.imgBack);
        tabLayout.getTabAt(0);
        Tab1.select();


        bindWidgetsWithAnEvent();

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });
    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:

                Residential_Legal_Fragment residential_legal_fragment = new Residential_Legal_Fragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_legal, residential_legal_fragment).commit();

                break;
            case 1:
                Commercial_Legal_Fragment commercial_legal_fragment = new Commercial_Legal_Fragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_legal, commercial_legal_fragment).commit();

                break;
          /*  case 2:
                Industrial_Legal_Fragment industrial_legal_fragment = new Industrial_Legal_Fragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_legal, industrial_legal_fragment).commit();

                break;*/

        }
    }

    @Override
    public void onChangeTab(int position) {

        tabLayout.getTabAt(1);
        Tab2.select();
        setCurrentTabFragment(1);
    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

}
