package com.bnhabitat.township.townactivities;

public interface Demo {

    void testFunctionOne();

    void testFunctionTwo();
}
