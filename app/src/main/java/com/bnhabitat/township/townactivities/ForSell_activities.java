package com.bnhabitat.township.townactivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bnhabitat.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForSell_activities extends Activity{

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.lnr1)
    LinearLayout lnr1;

    @BindView(R.id.lnr2)
    LinearLayout lnr2;

    @BindView(R.id.lnr3)
    LinearLayout lnr3;

    @OnClick(R.id.lnr1)
    public void onClick1(){
        Intent it = new Intent(this,Add_township.class);
        startActivity(it);
    }

    @OnClick(R.id.lnr2)
    public void onClick2(){
        Intent it = new Intent(this,Add_Project.class);
        startActivity(it);
    }

    @OnClick(R.id.lnr3)
    public void onClick3(){

    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.for_sell);
        ButterKnife.bind(this);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
