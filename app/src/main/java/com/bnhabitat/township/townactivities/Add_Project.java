package com.bnhabitat.township.townactivities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.connection.ConnectionManager;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.township.adapter.AddMoreLocalityAdapter;
import com.bnhabitat.township.adapter.ClubFacilityAdapter;
import com.bnhabitat.township.adapter.EmbedClubVideoAdapter;
import com.bnhabitat.township.adapter.EmbedZoningVideoAdapter;
import com.bnhabitat.township.model.ClubAmenitiesModel;
import com.bnhabitat.township.model.ClubFilesModel;
import com.bnhabitat.township.model.ProCatTypes;
import com.bnhabitat.township.model.SizeUnitModel;
import com.bnhabitat.township.model.TownshipFile;
import com.bnhabitat.township.model.TownshipFileModel;
import com.bnhabitat.township.model.TownshipLocalitiesModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class Add_Project extends Activity implements CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener {

    String encodedImage = "";

    public ArrayList<ProCatTypes> proCatTypesArrayList = new ArrayList<>();

    boolean clickedFromNext = true;

    @OnClick(R.id.imgBack)
    public void onImageBackClick() {
        finish();
    }

    @OnClick(R.id.next)
    public void onNextClick() {

        addProject();
        scroll_view.scrollTo(10, 10);
      /*  if (isValid()) {

            if (!edtAddMore.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore.getText().toString().trim().equalsIgnoreCase(null)) {
                TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();
                townshipLocalitiesModel.setLocalityName(edtAddMore.getText().toString());
                listTownshipLocalitiesModel.add(townshipLocalitiesModel);
            }

            addProject();
        }

        else {
            scroll_view.scrollTo(10, 10);
        }*/
    }

    int id = 0;

    ArrayList<TownshipFile> listProjectFile = new ArrayList<>();


    @BindView(R.id.checkLocalBody1)
    CheckBox checkLocalBody1;

    @BindView(R.id.checkLocalBody2)
    CheckBox checkLocalBody2;

    @BindView(R.id.checkLocalBody3)
    CheckBox checkLocalBody3;

    @BindView(R.id.checkLocalBody4)
    CheckBox checkLocalBody4;

    @BindView(R.id.edtNmName)
    EditText edtNmName;


    @BindView(R.id.edtDAuthorityNm)
    EditText edtDAuthorityNm;


    @BindView(R.id.edtHousingNm)
    EditText edtHousingNm;

    @BindView(R.id.edtCdmName)
    EditText edtCdmName;

    @BindView(R.id.edtDeveloperNm)
    EditText edtDeveloperNm;

    @BindView(R.id.edtProjectNm)
    EditText edtProjectNm;


    @BindView(R.id.lnrBodyType1)
    LinearLayout lnrBodyType1;

    @BindView(R.id.lnrBodyType2)
    LinearLayout lnrBodyType2;

    @BindView(R.id.lnrBodyType3)
    LinearLayout lnrBodyType3;

    @BindView(R.id.lnrBodyType4)
    LinearLayout lnrBodyType4;

    @BindView(R.id.lnrAddMore1)
    LinearLayout lnrAddMore1;

    @BindView(R.id.DevelopLogoImg)
    LinearLayout developLogoimg;


    @BindView(R.id.edtTownshipName)
    EditText edtTownshipNm;


    @BindView(R.id.edtTotalArea)
    EditText edtTotalArea;

    @BindView(R.id.lnrSclLcltyArea)
    LinearLayout lnrSclLcltyArea;


    public static final int REQUEST_CAMERA = 5;

    public static final int SELECT_FILE = 0x3;

    private static final int REQUEST_WRITE_PERMISSION = 786;

    ArrayList<TownshipFileModel> listTownshipModel = new ArrayList<>();

    @BindView(R.id.txtPin_zip)
    AutoCompleteTextView txtPin_zip;

    @BindView(R.id.spnrTotalArea)
    Spinner spnrTotalArea;


    @BindView(R.id.edtLocalityArea)
    EditText edtLocalityArea;
    Button done;
    static int uploadImageType = 1;

    @OnClick(R.id.lnrUploadDvlprLogo)
    public void uploadDevlprLogo() {

        if (listTownshipModelDevelop.size() < 1) {
            uploadImageType = 1;

            checkDrawerPermision();
        } else {
            Utils.showErrorMessage("You need to delete the previous image if you want to upload new image", Add_Project.this);
        }
    }


    @BindView(R.id.txtstate)
    EditText txtstate;

    @BindView(R.id.txtDistrict)
    EditText txtDistrict;

    @BindView(R.id.txtTehsil)
    EditText txtTehsil;

    @BindView(R.id.txtCityTown)
    EditText txtCityTown;

    ArrayList<TownshipFileModel> listTownshipModelDevelop = new ArrayList<>();

    ArrayList<TownshipLocalitiesModel> listTownshipLocalitiesModel = new ArrayList<>();

    static int slctAreaTownship = 1;

    String strLocalBodyName, strBodyType, strDeveloperLogo;

    ArrayList<TownshipLocalitiesModel> listSectorLocality = new ArrayList<>();

    TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();
    double aDouble;

    EditText edtAddMore;
    ImageView imgCloseAddMore;

    ScrollView scroll_view;
    LinearLayout ll_main;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_project);

        init();
        onCLicks();

        ButterKnife.bind(this);


        getAllSizeUnits(Constants.AREA);

        townshipLocalitiesModel.setLocalityName("");
        listSectorLocality.add(townshipLocalitiesModel);
        addsSector_locality(listSectorLocality);

        strBodyType = getResources().getString(R.string.muncipal_corporation);

        txtPin_zip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (txtPin_zip.getText().length() == 6) {
                    getLocationByPin(txtPin_zip.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        checkLocalBody1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.VISIBLE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody2.setChecked(false);
                    checkLocalBody3.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.muncipal_corporation);
                }

            }
        });

        checkLocalBody2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.VISIBLE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody3.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.development_auth);
                }

            }
        });


        checkLocalBody3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.VISIBLE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody2.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.housing_board);
                }

            }
        });


        checkLocalBody4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.VISIBLE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody2.setChecked(false);
                    checkLocalBody3.setChecked(false);

                    strBodyType = getResources().getString(R.string.cantonment_development);
                }

            }
        });

        lnrBodyType1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtNmName.setVisibility(View.VISIBLE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(true);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.muncipal_corporation);


            }
        });

        lnrBodyType2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.VISIBLE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(true);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.development_auth);
            }
        });

        lnrBodyType3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.VISIBLE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(true);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.housing_board);
            }
        });

        lnrBodyType4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.VISIBLE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(true);

                strBodyType = getResources().getString(R.string.cantonment_development);
            }
        });


    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    private void onCLicks() {

/*
        ll_main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideSoftKeyboard(Add_Project.this);
                return true;
            }
        });
*/

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedFromNext = false;
                addProject();

             /*   if (isValid()) {

                    if (!edtAddMore.getText().toString().trim().equalsIgnoreCase("") && !edtAddMore.getText().toString().trim().equalsIgnoreCase(null)) {
                        TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();
                        townshipLocalitiesModel.setLocalityName(edtAddMore.getText().toString());
                        listTownshipLocalitiesModel.add(townshipLocalitiesModel);
                    }

                    clickedFromNext = false;

                    addProject();

                }*/

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        try {
            if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPickImageCamera();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            String encodedImage = Utils.encodeImage(thumbnail);
            uploadFile(encodedImage);

        } else if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {


            Uri selectedImageUri = data.getData();


            try {
                InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                encodedImage = Utils.encodeImage(selectedImage);

                uploadFile(encodedImage);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }


    @Override
    public void onResultListener(String output, String which) {

        if (which != null) {
            if (which.equalsIgnoreCase(Constants.GET_ALL_SIZE_UNITS_API)) {
                SizeUnitModel sizeUnitModel = Utils.getGsonObject().fromJson(output, SizeUnitModel.class);
                if (sizeUnitModel != null) {

                    ArrayList<String> listUnit = new ArrayList<>();
                    listUnit.add(0, getString(R.string.select));
                    ArrayList<SizeUnitModel.Result> result = sizeUnitModel.getResult();
                    if (result.size() != 0) {

                        for (int i = 0; i < result.size(); i++) {

                            String strUnits = result.get(i).getSizeUnit();
                            listUnit.add(strUnits);
                        }

                        final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                                this, R.layout.custom_select_spinner, listUnit) {
                            @Override
                            public boolean isEnabled(int position) {
                                if (position == 0) {
                                    // Disable the first item from Spinner
                                    // First item will be use for hint
                                    return false;
                                } else {
                                    return true;
                                }
                            }

                            @NonNull
                            @Override
                            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray

                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);
                                    // tv.setBackgroundColor(getResources().getColor(R.color.black));

                                } else {
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);
                                    // tv.setBackgroundColor(getResources().getColor(R.color.black));
                                }
                                return view;
                            }

                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if (position == 0) {
                                    // Set the hint text color gray
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);

                                } else {
                                    tv.setTextColor(getResources().getColor(R.color.black));
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    //   tv.setPadding(10, 5, 10, 5);
                                    tv.setLayoutParams(params);

                                }
                                return view;
                            }
                        };

                        spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                        spnrTotalArea.setAdapter(spinnerArrayMs);


                    }

                } else {
                    Utils.showErrorMessage(R.string.something_wrong, this);
                }

            } else if (which.equalsIgnoreCase(Constants.UPLOAD_IMAGE_API)) {
                try {
                    JSONObject obj = new JSONObject(output);
                    int statusCode = obj.getInt("StatusCode");
                    if (statusCode == 200) {

                        JSONObject Result = obj.getJSONObject("Result");
                        JSONArray jsonArray = Result.getJSONArray("Result");

                        if (jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                strDeveloperLogo = object.getString("Url");
                                Log.d("TAG", "onResultListener: " + strDeveloperLogo);
                                String Id = object.getString("Id");


                                if (uploadImageType == 1) {

                                    TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "developer_logo", strDeveloperLogo);
                                    listTownshipModel.add(townshipFileModel);

                                    listTownshipModelDevelop.add(townshipFileModel);
                                    notifyChangeImages1();

                                }


                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.REMOVE_IMAGE_API)) {

                try {
                    JSONObject jsonObject = new JSONObject(output);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        Utils.showSuccessErrorMessage("Success", "Image deleted successfully", "Ok", Add_Project.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (which.equalsIgnoreCase(Constants.ADD_PROJECT_API)) {
                try {
                    JSONObject jsonObject = new JSONObject(output);
                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        String success = resultObject.getString("Success");
                        if (success.equals("true")) {
                            String message = resultObject.getString("Message");
                            JSONObject jsonObject1 = resultObject.getJSONObject("Result");
                            String projectId = jsonObject1.getString("Id");
                            PreferenceConnector.getInstance(this).savePreferences("ProjectId", projectId);
                            proCatTypesArrayList.clear();

                            JSONArray projectCatTypesArray = jsonObject1.getJSONArray("ProjectCategoryTypes");

                            for (int abc = 0; abc < projectCatTypesArray.length(); abc++) {

                                JSONObject jsonObject2 = projectCatTypesArray.getJSONObject(abc);

                                ProCatTypes proCatTypes = new ProCatTypes();
                                proCatTypes.setId(jsonObject2.getInt("Id"));
                                proCatTypes.setCategoryType(jsonObject2.getString("CategoryType"));
                                proCatTypes.setType(jsonObject2.getString("Type"));

                                proCatTypesArrayList.add(proCatTypes);

                            }

                            if (clickedFromNext) {

                                Utils.showSuccessErrorMessage("Success", message, "Ok", Add_Project.this);
                            } else {
                                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText(this.getString(R.string.message))
                                        .setContentText(this.getString(R.string.tapping_yes))
                                        .setConfirmText(this.getString(R.string.yes))
                                        .setCancelText(this.getString(R.string.cancel))
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismissWithAnimation();
                                            }
                                        })

                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                finish();

                                            }
                                        })
                                        .show();


                            }


                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", Add_Project.this);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(Add_Project.this, Project_name.class);
            intent.putExtra("ProjectName", edtProjectNm.getText().toString());
            startActivity(intent);

        }
    }

    public void init() {

        done = (Button) findViewById(R.id.done);
        scroll_view = (ScrollView) findViewById(R.id.scroll_view);
        ll_main = (LinearLayout) findViewById(R.id.ll_main);

    }

    public boolean isValid() {

        if (!edtNmName.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtNmName.getText().toString();
        } else if (!edtDAuthorityNm.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtDAuthorityNm.getText().toString();
        } else if (!edtHousingNm.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtHousingNm.getText().toString();
        } else if (!edtCdmName.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtCdmName.getText().toString();
        } else {
            strLocalBodyName = edtNmName.getText().toString();
        }

        if (txtPin_zip.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_zip), txtPin_zip, this);
            return false;
        }
        if (txtstate.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_state), txtstate, this);
            return false;
        }
        if (txtDistrict.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_district), txtDistrict, this);
            return false;
        }

        if (checkLocalBody1.isChecked()) {

            if (edtNmName.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_mc_name), edtNmName, this);
                return false;
            }
        }

        if (checkLocalBody2.isChecked()) {

            if (edtDAuthorityNm.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_development_auth_nm), edtDAuthorityNm, this);
                return false;
            }
        }

        if (checkLocalBody3.isChecked()) {

            if (edtHousingNm.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_housing_board_nm), edtHousingNm, this);
                return false;
            }
        }

        if (checkLocalBody4.isChecked()) {

            if (edtCdmName.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_cantonment_nm), edtCdmName, this);
                return false;
            }
        }

        if (edtTownshipNm.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_township_name), edtTownshipNm, this);
            return false;
        }


        if (edtDeveloperNm.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_developer_name), edtDeveloperNm, this);
            return false;
        }


        if (edtProjectNm.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_project_name), edtProjectNm, this);
            return false;
        } else if (strLocalBodyName.equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_body_nm), this);
            return false;
        }

        return true;

    }

    private boolean isValid(String urlString) {
        try {
            URL url = new URL(urlString);
            return URLUtil.isValidUrl(urlString) && Patterns.WEB_URL.matcher(urlString).matches();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void checkDrawerPermision() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_WRITE_PERMISSION);

        } else {
            onPickImageCamera();
        }


    }

    public void onPickImageCamera() {


        final CharSequence[] items = {"Take Photo", "Select photo from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select your option");
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Select photo from gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private void getLocationByPin(String strValue) {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PIN_DETAIL + strValue.trim(),
                "",
                "Loading..",
                this,
                Constants.GET_LOCATION_API,
                Constants.GET).execute();
    }

    private void addProject() {
        new CommonAsync(this, Urls.ADD_PROJECT,
                getInputJson(),
                "Loading...", this,
                Constants.ADD_PROJECT_API,
                Constants.POST).execute();
    }

    private String getInputJson() {

        slctAreaTownship = spnrTotalArea.getSelectedItemPosition() + 1;


        JSONObject jsonObject = null;
        JSONArray arrayTownshipLocalities = null;

        try {
            jsonObject = new JSONObject();
            arrayTownshipLocalities = new JSONArray();

            jsonObject.put(Constants.Id, 0);
            PreferenceConnector.getInstance(this).savePreferences("ProjectName", edtProjectNm.getText().toString());
            jsonObject.put(Constants.Name, edtProjectNm.getText().toString());
            PreferenceConnector.getInstance(this).savePreferences("TownshipName", edtTownshipNm.getText().toString());
            jsonObject.put(Constants.TownshipName, edtTownshipNm.getText().toString());
            PreferenceConnector.getInstance(this).savePreferences("TotalArea", edtTotalArea.getText().toString());
            jsonObject.put(Constants.TotalArea, edtTotalArea.getText().toString());
            PreferenceConnector.getInstance(this).savePreferences("TotalAreaSizeUnitId", slctAreaTownship);
            jsonObject.put(Constants.TotalAreaSizeUnitId, slctAreaTownship);
            PreferenceConnector.getInstance(this).savePreferences("ZipCode", txtPin_zip.getText().toString());
            jsonObject.put(Constants.ZipCode, txtPin_zip.getText().toString());
            PreferenceConnector.getInstance(this).savePreferences("StateName", txtstate.getText().toString());
            jsonObject.put(Constants.StateName, txtstate.getText().toString());
            PreferenceConnector.getInstance(this).savePreferences("DistrictName", txtDistrict.getText().toString());
            jsonObject.put(Constants.DistrictName, txtDistrict.getText().toString());
            PreferenceConnector.getInstance(this).savePreferences("TehsilName", txtTehsil.getText().toString());
            jsonObject.put(Constants.TehsilName, txtTehsil.getText().toString());
            PreferenceConnector.getInstance(this).savePreferences("TownName", txtCityTown.getText().toString());
            jsonObject.put(Constants.TownName, txtCityTown.getText().toString());
            PreferenceConnector.getInstance(this).savePreferences("LocalBodyType", strBodyType);
            jsonObject.put(Constants.LocalBodyType, strBodyType);
            PreferenceConnector.getInstance(this).savePreferences("LocalBodyName", strLocalBodyName);
            jsonObject.put(Constants.LocalBodyName, strLocalBodyName);
            PreferenceConnector.getInstance(this).savePreferences("DeveloperCompanyName", edtDeveloperNm.getText().toString());
            jsonObject.put(Constants.DeveloperCompanyName, edtDeveloperNm.getText().toString());
            if (listTownshipModelDevelop.isEmpty()) {
                PreferenceConnector.getInstance(this).savePreferences("DeveloperCompanyLogo", "");
                jsonObject.put(Constants.DeveloperCompanyLogo, "");
            } else {
                PreferenceConnector.getInstance(this).savePreferences("DeveloperCompanyLogo", listTownshipModelDevelop.get(0).getUrl());
                jsonObject.put(Constants.DeveloperCompanyLogo, listTownshipModelDevelop.get(0).getUrl());
            }


            PreferenceConnector.getInstance(this).savePreferences("CompanyId", 1);
            jsonObject.put(Constants.CompanyId, 1);
            PreferenceConnector.getInstance(this).savePreferences("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put(Constants.CreatedById, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            PreferenceConnector.getInstance(this).savePreferences("UpdatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put(Constants.UpdatedById, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));

            for (int i = 0; i < listTownshipLocalitiesModel.size(); i++) {

                JSONObject obj = new JSONObject();
                obj.put(Constants.Id, listTownshipLocalitiesModel.get(i).getId());
                obj.put(Constants.LocalityName, listTownshipLocalitiesModel.get(i).getLocalityName());

                arrayTownshipLocalities.put(obj);

            }

            jsonObject.put(Constants.ProjectLocalities, arrayTownshipLocalities);

            jsonObject.put(Constants.IsDeletable, false);
            jsonObject.put(Constants.IsModify, false);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject.toString();
    }

    public void addsSector_locality(final ArrayList<TownshipLocalitiesModel> list) {

        lnrSclLcltyArea.removeAllViews();
        listTownshipLocalitiesModel = list;

        for (int i = 0; i < list.size(); i++) {

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.custom_add_more, null);
            edtAddMore = (EditText) v.findViewById(R.id.edtAddMore);
            edtAddMore.setText(list.get(i).getLocalityName());

            imgCloseAddMore = (ImageView) v.findViewById(R.id.imgCloseAddMore);
            imgCloseAddMore.setId(i);

            edtAddMore.setId(i);

            if (i == 0) {
                imgCloseAddMore.setVisibility(View.GONE);
                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {
                    edtAddMore.setText(list.get(i).getLocalityName());
                } else {
                    edtAddMore.setHint("");
                }

            }

            final int finalI = i;

            imgCloseAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int id1 = finalI;
                    EditText t = (EditText) findViewById(list.size() - 1);
                    String text = t.getText().toString();
                    TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();


                    if (!text.equalsIgnoreCase("")) {
                        if (!text.equalsIgnoreCase(t.getText().toString())) {
                            townshipLocalitiesModel.setLocalityName(text);
                            list.add(list.size() - 1, townshipLocalitiesModel);
                        }
                    }

                    if (list.contains(""))
                        list.remove("");

                    int id = finalI;
                    list.remove(id);

                    addsSector_locality(list);
                }
            });

            lnrSclLcltyArea.addView(v);
        }

        lnrAddMore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TownshipLocalitiesModel townshipLocalitiesModel = new TownshipLocalitiesModel();

                if (!edtAddMore.getText().toString().equalsIgnoreCase("")) {

                    if (!list.get(list.size() - 1).getLocalityName().equalsIgnoreCase("")) {

                        townshipLocalitiesModel.setLocalityName("");
                        list.add(townshipLocalitiesModel);

                    } else {
                        townshipLocalitiesModel.setLocalityName(edtAddMore.getText().toString().trim());
                        list.add(list.size() - 1, townshipLocalitiesModel);
                    }

                    addsSector_locality(list);
                } else {
                    Utils.showErrorMessage("Please add Sector/Locality/Area", Add_Project.this);
                }
            }
        });

    }

    private void uploadFile(String urlImage) {

        new CommonAsync(this, Urls.UPLOAD_IMAGE,
                getInputJson(urlImage),
                "Loading..",
                this,
                Constants.UPLOAD_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJson(String urlImage) {
        String strREq = "";
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;

        jsonObject = new JSONObject();
        jsonArray = new JSONArray();
        try {

            jsonObject.put(Constants.CompanyId, 1);
            jsonObject.put(Constants.Name, "project");
            jsonObject.put(Constants.Type, Constants.jpg);
            jsonObject.put(Constants.ImageCode, urlImage);
            jsonArray.put(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        strREq = jsonArray.toString();

        return strREq;
    }

    public void notifyChangeImages1() {

        developLogoimg.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelDevelop.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);


            Picasso.with(Add_Project.this).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl()).into(imgEvents);

            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(Add_Project.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(Add_Project.this.getString(R.string.message))
                            .setContentText(Add_Project.this.getString(R.string.are_you_sure))
                            .setConfirmText(Add_Project.this.getString(R.string.yes))
                            .setCancelText(Add_Project.this.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelDevelop.get(id).getFileId());
                                    listTownshipModelDevelop.remove(id);
                                    notifyChangeImages1();
                                }
                            })
                            .show();
                }
            });


            developLogoimg.addView(v);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        EventBus.getDefault().register(this);


    }

    @Override
    protected void onPause() {
        super.onPause();

        EventBus.getDefault().unregister(this);

    }

    private void removeFile(String id) {

        new CommonAsync(this, Urls.REMOVE_IMAGE,
                getInputJsonId(id),
                "Loading..",
                this,
                Constants.REMOVE_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJsonId(String id) {

        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();

        return inputStr;
    }

    public void onEvent(SizeUnitModel sizeUnitModel) {
        if (sizeUnitModel != null) {

            ArrayList<String> listUnit = new ArrayList<>();
            listUnit.add(0, getString(R.string.select));
            ArrayList<SizeUnitModel.Result> result = sizeUnitModel.getResult();
            if (result.size() != 0) {

                for (int i = 0; i < result.size(); i++) {

                    String strUnits = result.get(i).getSizeUnit();
                    listUnit.add(strUnits);
                }

                final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                        this, R.layout.custom_select_spinner, listUnit) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            // Disable the first item from Spinner
                            // First item will be use for hint
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray

                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));
                        }
                        return view;
                    }

                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        }
                        return view;
                    }
                };

                spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                spnrTotalArea.setAdapter(spinnerArrayMs);


            }

        } else {
            Utils.showErrorMessage(R.string.something_wrong, this);
        }
    }

    private void getAllSizeUnits(String type) {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/" + type,
                "",
                "Loading..",
                this,
                Constants.GET_ALL_SIZE_UNITS_API,
                Constants.GET).execute();

    }

}
