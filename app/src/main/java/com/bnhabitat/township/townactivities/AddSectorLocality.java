package com.bnhabitat.township.townactivities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.township.adapter.AddMoreLocalityAdapter;
import com.bnhabitat.township.adapter.ClubFacilityAdapter;
import com.bnhabitat.township.adapter.EmbedClubVideoAdapter;
import com.bnhabitat.township.adapter.EmbedZoningVideoAdapter;
import com.bnhabitat.township.model.ClubAmenitiesModel;
import com.bnhabitat.township.model.ClubFilesModel;
import com.bnhabitat.township.model.SizeUnitModel;
import com.bnhabitat.township.model.TownshipFile;
import com.bnhabitat.township.model.TownshipFileModel;
import com.bnhabitat.township.model.TownshipLocalitiesModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddSectorLocality extends AppCompatActivity implements CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener {

    boolean istextChange = true;
    String encodedImage = "";

    @OnClick(R.id.imgBack)
    public void onImageBackClick() {
        finish();
    }

    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @BindView(R.id.mainLayout)
    LinearLayout mainLayout;

    int id = 0;

    @BindView(R.id.checkLocalBody1)
    CheckBox checkLocalBody1;

    @BindView(R.id.checkLocalBody2)
    CheckBox checkLocalBody2;

    @BindView(R.id.checkLocalBody3)
    CheckBox checkLocalBody3;

    @BindView(R.id.checkLocalBody4)
    CheckBox checkLocalBody4;

    @BindView(R.id.edtNmName)
    EditText edtNmName;

    @BindView(R.id.edtDAuthorityNm)
    EditText edtDAuthorityNm;

    @BindView(R.id.edtHousingNm)
    EditText edtHousingNm;

    @BindView(R.id.edtCdmName)
    EditText edtCdmName;

    @BindView(R.id.edtDeveloperNm)
    EditText edtDeveloperNm;

    @BindView(R.id.lnrBodyType1)
    LinearLayout lnrBodyType1;

    @BindView(R.id.lnrBodyType2)
    LinearLayout lnrBodyType2;

    @BindView(R.id.lnrBodyType3)
    LinearLayout lnrBodyType3;

    @BindView(R.id.lnrBodyType4)
    LinearLayout lnrBodyType4;

    @BindView(R.id.edtTotalArea)
    EditText edtTotalArea;

    @BindView(R.id.DevelopLogoImg)
    LinearLayout developLogoimg;

    public static final int REQUEST_CAMERA = 5;

    public static final int SELECT_FILE = 0x3;

    private static final int REQUEST_WRITE_PERMISSION = 786;

    @BindView(R.id.txtPin_zip)
    AutoCompleteTextView txtPin_zip;

    @BindView(R.id.spnrTotalArea)
    Spinner spnrTotalArea;

    static int uploadImageType = 1;

    @OnClick(R.id.lnrUploadDvlprLogo)
    public void uploadDevlprLogo() {
        if (listTownshipModelDevelop.size() < 1) {
            uploadImageType = 1;

            checkDrawerPermision();
        } else {
            Utils.showErrorMessage("You need to delete the previous image if you want to upload new image", AddSectorLocality.this);
        }
    }


    @BindView(R.id.txtstate)
    EditText txtstate;

    @BindView(R.id.txtDistrict)
    EditText txtDistrict;

    @BindView(R.id.txtCityTown)
    EditText txtCityTown;

    @BindView(R.id.edtProjectNm)
    EditText edtProjectNm;

    @BindView(R.id.txtTehsil)
    EditText txtTehsil;

    @BindView(R.id.txtSectorLocality)
    EditText txtSectorLocality;

    ArrayList<TownshipFileModel> listTownshipModel = new ArrayList<>();

    ArrayList<TownshipFileModel> listTownshipModelDevelop = new ArrayList<>();

    static int slctAreaTownship = 1;

    String strLocalBodyName, strBodyType, strDeveloperLogo;

    @OnClick(R.id.next)
    public void onNextClick() {

        if (isValid()) {
            Toast.makeText(this, "All Validations are OK", Toast.LENGTH_SHORT).show();
        }
        else{
            scrollView.scrollTo(10, 10);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sector_locality);

        ButterKnife.bind(this);

        String email = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        String password = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PASSWORD, "");

        loginUser(email, password);

        checkLocalBody1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.VISIBLE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody2.setChecked(false);
                    checkLocalBody3.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.muncipal_corporation);
                }

            }
        });


        checkLocalBody2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.VISIBLE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody3.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.development_auth);
                }

            }
        });


        checkLocalBody3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.VISIBLE);
                    edtCdmName.setVisibility(View.GONE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody2.setChecked(false);
                    checkLocalBody4.setChecked(false);

                    strBodyType = getResources().getString(R.string.housing_board);
                }

            }
        });


        checkLocalBody4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    edtNmName.setVisibility(View.GONE);
                    edtDAuthorityNm.setVisibility(View.GONE);
                    edtHousingNm.setVisibility(View.GONE);
                    edtCdmName.setVisibility(View.VISIBLE);

                    checkLocalBody1.setChecked(false);
                    checkLocalBody2.setChecked(false);
                    checkLocalBody3.setChecked(false);

                    strBodyType = getResources().getString(R.string.cantonment_development);
                }

            }
        });

        lnrBodyType1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtNmName.setVisibility(View.VISIBLE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(true);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.muncipal_corporation);


            }
        });

        lnrBodyType2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.VISIBLE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(true);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.development_auth);
            }
        });

        lnrBodyType3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.VISIBLE);
                edtCdmName.setVisibility(View.GONE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(true);
                checkLocalBody4.setChecked(false);

                strBodyType = getResources().getString(R.string.housing_board);
            }
        });

        lnrBodyType4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtNmName.setVisibility(View.GONE);
                edtDAuthorityNm.setVisibility(View.GONE);
                edtHousingNm.setVisibility(View.GONE);
                edtCdmName.setVisibility(View.VISIBLE);

                checkLocalBody1.setChecked(false);
                checkLocalBody2.setChecked(false);
                checkLocalBody3.setChecked(false);
                checkLocalBody4.setChecked(true);

                strBodyType = getResources().getString(R.string.cantonment_development);
            }
        });
    }

    private void checkDrawerPermision() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, REQUEST_WRITE_PERMISSION);

        } else {
            onPickImageCamera();
        }
    }

    public void onPickImageCamera() {

        final CharSequence[] items = {"Take Photo", "Select photo from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select your option");
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Select photo from gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            String encodedImage = Utils.encodeImage(thumbnail);

            uploadFile(encodedImage);

        } else if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {


            Uri selectedImageUri = data.getData();


            try {
                InputStream imageStream = getContentResolver().openInputStream(selectedImageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                encodedImage = Utils.encodeImage(selectedImage);

                uploadFile(encodedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onPickImageCamera();
        }

    }

    private void loginUser(String email, String password) {

        new CommonAsync(this, Urls.LOGIN_USER,
                getInputJson(email, password),
                "Loading..",
                this,
                Constants.LOGIN_USER_API,
                Constants.POST).execute();

    }

    private String getInputJson(String email, String password) {

        JSONObject jsonObject = null;

        jsonObject = new JSONObject();

        try {

            jsonObject.put("EmailOrPhone", email);
            jsonObject.put("Password", password);
            jsonObject.put("DeviceId", Utils.getDeviceId(this));
            jsonObject.put("RegistrationId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.REGISTRATION_ID, ""));
            jsonObject.put("DeviceName", "Android");
//
            jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonObject.put("Platform", "Android");
            jsonObject.put("AppId", Integer.parseInt("1"));

        } catch (Exception e) {

        }

        return jsonObject.toString();

    }

    private void authenticateUser(String authKey) {


        new CommonSyncwithoutstatus(this, Urls.AUTHENTICATE_USER,
                getInputJson(0, authKey),
                "Loading..",
                this,
                Constants.AUTHENTICATE_USER_API,
                Constants.POST).execute();

    }

    private void getAllSizeUnits(String type) {


        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/" + type,
                "",
                "Loading..",
                this,
                Constants.GET_ALL_SIZE_UNITS_API,
                Constants.GET).execute();

    }

    private String getInputJson(int val, String authKey) {

        JSONObject jsonObject = null;

        jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put(Constants.AUTH_KEY, authKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    private void uploadFile(String urlImage) {


        new CommonAsync(this, Urls.UPLOAD_IMAGE,
                getInputJson(urlImage),
                "Loading..",
                this,
                Constants.UPLOAD_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJson(String urlImage) {
        String strREq = "";
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;

        jsonObject = new JSONObject();
        jsonArray = new JSONArray();
        try {

            jsonObject.put(Constants.CompanyId, 1);
            jsonObject.put(Constants.Name, "township");
            jsonObject.put(Constants.Type, Constants.jpg);
            jsonObject.put(Constants.ImageCode, urlImage);
            jsonArray.put(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        strREq = jsonArray.toString();

        return strREq;
    }

    private void removeFile(String id) {


        new CommonAsync(this, Urls.REMOVE_IMAGE,
                getInputJsonId(id),
                "Loading..",
                this,
                Constants.REMOVE_IMAGE_API,
                Constants.POST).execute();

    }

    private String getInputJsonId(String id) {

        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }


    public void onEvent(SizeUnitModel sizeUnitModel) {
        if (sizeUnitModel != null) {

            ArrayList<String> listUnit = new ArrayList<>();
            listUnit.add(0, getString(R.string.select));
            ArrayList<SizeUnitModel.Result> result = sizeUnitModel.getResult();
            if (result.size() != 0) {

                for (int i = 0; i < result.size(); i++) {

                    String strUnits = result.get(i).getSizeUnit();
                    listUnit.add(strUnits);
                }

                final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                        this, R.layout.custom_select_spinner, listUnit) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            // Disable the first item from Spinner
                            // First item will be use for hint
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray

                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);
                            // tv.setBackgroundColor(getResources().getColor(R.color.black));
                        }
                        return view;
                    }

                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        } else {
                            tv.setTextColor(getResources().getColor(R.color.black));
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                            //   tv.setPadding(10, 5, 10, 5);
                            tv.setLayoutParams(params);

                        }
                        return view;
                    }
                };

                spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                spnrTotalArea.setAdapter(spinnerArrayMs);


            }

        } else {
            Utils.showErrorMessage(R.string.something_wrong, this);
        }
    }

    public void onEvent(String authKey) {
        if (authKey != null) {
            authenticateUser(authKey);

        } else {
            Utils.showErrorMessage(getString(R.string.something_wrong), this);
        }
    }

    public void onEvent(JSONObject jsonObject) {
        if (jsonObject != null) {
            String AuthToken = null;
            try {
                AuthToken = jsonObject.getString("AuthToken");
                PreferenceConnector.getInstance(this).savePreferences(Constants.AUTHENTICATION_KEY, AuthToken);
                getAllSizeUnits(Constants.AREA);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    public boolean isValid() {

        if (!edtNmName.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtNmName.getText().toString();
        } else if (!edtDAuthorityNm.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtDAuthorityNm.getText().toString();
        } else if (!edtHousingNm.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtHousingNm.getText().toString();
        } else if (!edtCdmName.getText().toString().equalsIgnoreCase("")) {
            strLocalBodyName = edtCdmName.getText().toString();
        } else {
            strLocalBodyName = edtNmName.getText().toString();
        }

        if (txtPin_zip.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_zip), txtPin_zip, this);
            return false;
        } if (txtstate.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_state), txtstate, this);
            return false;
        } if (txtDistrict.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_district), txtDistrict,this);
            return false;
        }

        if (checkLocalBody1.isChecked()) {

            if (edtNmName.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_mc_name), edtNmName,this);
                return false;
            }
        }

        if (checkLocalBody2.isChecked()) {

            if (edtDAuthorityNm.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_development_auth_nm), edtDAuthorityNm,this);
                return false;
            }
        }

        if (checkLocalBody3.isChecked()) {

            if (edtHousingNm.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_housing_board_nm), edtHousingNm,this);
                return false;
            }
        }

        if (checkLocalBody4.isChecked()) {

            if (edtCdmName.getText().toString().equalsIgnoreCase("")) {
                Utils.showErrorMessage(getString(R.string.enter_cantonment_nm), edtCdmName,this);
                return false;
            }
        }

        if (txtSectorLocality.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_sector_locality), txtSectorLocality,this);
            return false;
        }

        if (edtDeveloperNm.getText().toString().equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_developer_name), edtDeveloperNm,this);
            return false;
        }

        /*else if (strLocalBodyName.equalsIgnoreCase("")) {
            Utils.showErrorMessage(getString(R.string.enter_body_nm), this);
            return false;
        }*/

        return true;

    }

    @Override
    public void onResultListener(String result, String which) {

        String w = which;

            if (which != null) {

                if (which.equalsIgnoreCase(Constants.LOGIN_USER_API)) {
                    try {

                        JSONObject jsonObject = new JSONObject(result);

                        JSONObject obj = jsonObject.getJSONObject("Result");
                        JSONObject UserDetail = obj.getJSONObject("UserDetail");

                        String user_Id = UserDetail.getJSONObject("UserDetail").getString("Id");
                        String authKey = UserDetail.getJSONObject("UserDetail").getString("AuthKey");

                        PreferenceConnector.getInstance(this).savePreferences(Constants.USER_ID, user_Id);

                        if (authKey != null) {
                            authenticateUser(authKey);

                        } else {
                            Utils.showErrorMessage(getString(R.string.something_wrong), this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else if (which.equalsIgnoreCase(Constants.AUTHENTICATE_USER_API)) {
                    try {

                        JSONObject jsonObject = new JSONObject(result);
                        String AuthToken = jsonObject.getString("AuthToken");
                        PreferenceConnector.getInstance(this).savePreferences(Constants.AUTHENTICATION_KEY, AuthToken);
                        getAllSizeUnits(Constants.AREA);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else
                if (which.equalsIgnoreCase(Constants.GET_ALL_SIZE_UNITS_API)) {
                    SizeUnitModel sizeUnitModel = Utils.getGsonObject().fromJson(result, SizeUnitModel.class);
                    if (sizeUnitModel != null) {

                        ArrayList<String> listUnit = new ArrayList<>();
                        listUnit.add(0, getString(R.string.select));
                        ArrayList<SizeUnitModel.Result> results = sizeUnitModel.getResult();
                        if (results.size() != 0) {

                            for (int i = 0; i < results.size(); i++) {

                                String strUnits = results.get(i).getSizeUnit();
                                listUnit.add(strUnits);
                            }

                            final ArrayAdapter<String> spinnerArrayMs = new ArrayAdapter<String>(
                                    this, R.layout.custom_select_spinner, listUnit) {
                                @Override
                                public boolean isEnabled(int position) {
                                    if (position == 0) {
                                        // Disable the first item from Spinner
                                        // First item will be use for hint
                                        return false;
                                    } else {
                                        return true;
                                    }
                                }

                                @NonNull
                                @Override
                                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                    View view = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) view;
                                    if (position == 0) {
                                        // Set the hint text color gray

                                        tv.setTextColor(getResources().getColor(R.color.black));
                                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                        //   tv.setPadding(10, 5, 10, 5);
                                        tv.setLayoutParams(params);
                                        // tv.setBackgroundColor(getResources().getColor(R.color.black));

                                    } else {
                                        tv.setTextColor(getResources().getColor(R.color.black));
                                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                        //   tv.setPadding(10, 5, 10, 5);
                                        tv.setLayoutParams(params);
                                        // tv.setBackgroundColor(getResources().getColor(R.color.black));
                                    }
                                    return view;
                                }

                                @Override
                                public View getDropDownView(int position, View convertView,
                                                            ViewGroup parent) {
                                    View view = super.getDropDownView(position, convertView, parent);
                                    TextView tv = (TextView) view;
                                    if (position == 0) {
                                        // Set the hint text color gray
                                        tv.setTextColor(getResources().getColor(R.color.black));
                                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                        //   tv.setPadding(10, 5, 10, 5);
                                        tv.setLayoutParams(params);

                                    } else {
                                        tv.setTextColor(getResources().getColor(R.color.black));
                                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                        //   tv.setPadding(10, 5, 10, 5);
                                        tv.setLayoutParams(params);

                                    }
                                    return view;
                                }
                            };

                            spinnerArrayMs.setDropDownViewResource(R.layout.custom_select_spinner);

                            spnrTotalArea.setAdapter(spinnerArrayMs);

                    } else {
                        Utils.showErrorMessage(R.string.something_wrong, this);
                    }
                }
            } else if (which.equalsIgnoreCase(Constants.UPLOAD_IMAGE_API)) {
                    try {
                        JSONObject obj = new JSONObject(result);
                        int statusCode = obj.getInt("StatusCode");
                        if (statusCode == 200) {

                            JSONObject Result = obj.getJSONObject("Result");
                            JSONArray jsonArray = Result.getJSONArray("Result");

                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    strDeveloperLogo = object.getString("Url");
                                    Log.d("TAG", "onResultListener: " + strDeveloperLogo);
                                    String Id = object.getString("Id");


                                    if (uploadImageType == 1) {

                                        TownshipFileModel townshipFileModel = new TownshipFileModel(0, Id, "developer_logo", strDeveloperLogo);
                                        listTownshipModel.add(townshipFileModel);

                                        listTownshipModelDevelop.add(townshipFileModel);
                                        notifyChangeImages1();

                                    }

                                }
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (which.equalsIgnoreCase(Constants.REMOVE_IMAGE_API)) {

                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (jsonObject.getInt("StatusCode") == 200) {


                            JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                            Utils.showSuccessErrorMessage("Success", "Image deleted successfully", "Ok", AddSectorLocality.this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
        }
        Log.e("which", "" + which);

    }

    public void notifyChangeImages1() {

        developLogoimg.removeAllViews();

        int i1;
        for (i1 = 0; i1 < listTownshipModelDevelop.size(); i1++) {

            final int i = i1;

            LayoutInflater inflate = getLayoutInflater();
            View v = inflate.inflate(R.layout.custom_images, null);

            final ImageView imgEvents = (ImageView) v.findViewById(R.id.imgEvents);
            final ImageView imgDelete = (ImageView) v.findViewById(R.id.imgDelete);

            String strurl=    Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl();


            Picasso.with(AddSectorLocality.this).load(Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl()).into(imgEvents);

            Log.e("TAG", "" + Urls.BASE_CONTACT_IMAGE_URL + listTownshipModelDevelop.get(i).getUrl());
            imgEvents.setTag(i);
            imgDelete.setId(i);

            imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SweetAlertDialog(AddSectorLocality.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(AddSectorLocality.this.getString(R.string.message))
                            .setContentText(AddSectorLocality.this.getString(R.string.are_you_sure))
                            .setConfirmText(AddSectorLocality.this.getString(R.string.yes))
                            .setCancelText(AddSectorLocality.this.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    id = imgDelete.getId();
                                    removeFile(listTownshipModelDevelop.get(id).getFileId());
                                    listTownshipModelDevelop.remove(id);
                                    notifyChangeImages1();
                                }
                            })
                            .show();
                }
            });


            developLogoimg.addView(v);

        }

    }

}
