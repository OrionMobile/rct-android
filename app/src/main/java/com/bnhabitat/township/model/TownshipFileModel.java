package com.bnhabitat.township.model;

public class TownshipFileModel {

    //int Id;
    int Id;
    String FileId;
    String Category;
    String url;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFileId() {
        return FileId;
    }

    public void setFileId(String fileId) {
        FileId = fileId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public TownshipFileModel(int id, String fileId, String category, String url) {
        Id = id;
        FileId = fileId;
        Category = category;
        this.url = url;
    }
}
