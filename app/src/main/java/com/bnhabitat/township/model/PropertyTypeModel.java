package com.bnhabitat.township.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PropertyTypeModel implements Serializable {

    public PropertyTypeModel(){

    }

        String Id;
        String Name;
        String Type;
        String IsActive;
        String delete;

    public String getDelete() {
        return delete;
    }

    public void setDelete(String delete) {
        this.delete = delete;
    }

    public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getIsActive() {
            return IsActive;
        }

        public void setIsActive(String isActive) {
            IsActive = isActive;
        }

}
