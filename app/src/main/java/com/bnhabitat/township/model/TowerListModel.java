package com.bnhabitat.township.model;

import java.util.ArrayList;

public class TowerListModel {
    int Id;
    int ProjectCategoryTypeId;
    int CompanyId;
    String Name;
    int TotalFloor;
    int TotalPassengerLift;
    int TotalCargoLift;
    int TotalStaircase;
    int TotalBasements;
    int GroundTypeStilt;
    int GroundTypeApartment;
    int GroundTypeOther;
    int TowerGroupId;
    String selected;
    ArrayList<ProjectCategoryTypesModel> projectCategoryTypesModels= new ArrayList<>();

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getProjectCategoryTypeId() {
        return ProjectCategoryTypeId;
    }

    public void setProjectCategoryTypeId(int projectCategoryTypeId) {
        ProjectCategoryTypeId = projectCategoryTypeId;
    }

    public int getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(int companyId) {
        CompanyId = companyId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getTotalFloor() {
        return TotalFloor;
    }

    public void setTotalFloor(int totalFloor) {
        TotalFloor = totalFloor;
    }

    public int getTotalPassengerLift() {
        return TotalPassengerLift;
    }

    public void setTotalPassengerLift(int totalPassengerLift) {
        TotalPassengerLift = totalPassengerLift;
    }

    public int getTotalCargoLift() {
        return TotalCargoLift;
    }

    public void setTotalCargoLift(int totalCargoLift) {
        TotalCargoLift = totalCargoLift;
    }

    public int getTotalStaircase() {
        return TotalStaircase;
    }

    public void setTotalStaircase(int totalStaircase) {
        TotalStaircase = totalStaircase;
    }

    public int getTotalBasements() {
        return TotalBasements;
    }

    public void setTotalBasements(int totalBasements) {
        TotalBasements = totalBasements;
    }

    public int getGroundTypeStilt() {
        return GroundTypeStilt;
    }

    public void setGroundTypeStilt(int groundTypeStilt) {
        GroundTypeStilt = groundTypeStilt;
    }

    public int getGroundTypeApartment() {
        return GroundTypeApartment;
    }

    public void setGroundTypeApartment(int groundTypeApartment) {
        GroundTypeApartment = groundTypeApartment;
    }

    public int getGroundTypeOther() {
        return GroundTypeOther;
    }

    public void setGroundTypeOther(int groundTypeOther) {
        GroundTypeOther = groundTypeOther;
    }

    public int getTowerGroupId() {
        return TowerGroupId;
    }

    public void setTowerGroupId(int towerGroupId) {
        TowerGroupId = towerGroupId;
    }

    public ArrayList<ProjectCategoryTypesModel> getProjectCategoryTypesModels() {
        return projectCategoryTypesModels;
    }

    public void setProjectCategoryTypesModels(ArrayList<ProjectCategoryTypesModel> projectCategoryTypesModels) {
        this.projectCategoryTypesModels = projectCategoryTypesModels;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }
}
