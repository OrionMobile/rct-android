package com.bnhabitat.township.model;

import java.util.ArrayList;

/**
 * Created by epi-i5-gold on 12/21/2018.
 */

public class FloorListModel {

    String name;
    ArrayList<AccomodationModel> accomodationList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<AccomodationModel> getAccomodationList() {
        return accomodationList;
    }

    public void setAccomodationList(ArrayList<AccomodationModel> accomodationList) {
        this.accomodationList = accomodationList;
    }
}
