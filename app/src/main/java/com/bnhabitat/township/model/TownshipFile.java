package com.bnhabitat.township.model;

public class TownshipFile {

    int Id;
    String Category;
    String FileUrl;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }


    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getFileUrl() {
        return FileUrl;
    }

    public void setUrl(String FileUrl) {
        this.FileUrl = FileUrl;
    }

    public TownshipFile(int id, String category, String FileUrl) {
        Id = id;
        Category = category;
        this.FileUrl = FileUrl;
    }

    public TownshipFile(){

    }
}
