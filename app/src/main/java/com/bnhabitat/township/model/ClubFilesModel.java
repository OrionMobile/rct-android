package com.bnhabitat.township.model;

public class ClubFilesModel {


    String Id;
    String FileId;
    String Category;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFileId() {
        return FileId;
    }

    public void setFileId(String fileId) {
        FileId = fileId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public ClubFilesModel(String id, String fileId, String category) {
        Id = id;
        FileId = fileId;
        Category = category;
    }
}
