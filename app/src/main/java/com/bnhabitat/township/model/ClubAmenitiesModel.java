package com.bnhabitat.township.model;

public class ClubAmenitiesModel {


    String Type;
    String Title;
    String Value;
    double Size;
    int SizeUnitId;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public double getSize() {
        return Size;
    }

    public void setSize(double size) {
        Size = size;
    }

    public int getSizeUnitId() {
        return SizeUnitId;
    }

    public void setSizeUnitId(int sizeUnitId) {
        SizeUnitId = sizeUnitId;
    }

    public ClubAmenitiesModel(String type, String title, String value, double size, int sizeUnitId) {
        Type = type;
        Title = title;
        Value = value;
        Size = size;
        SizeUnitId = sizeUnitId;
    }

    public ClubAmenitiesModel(){

    }
}
