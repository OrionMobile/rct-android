package com.bnhabitat.township.model;

import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.models.PropertyBedroomModel;

import java.io.Serializable;
import java.util.ArrayList;

public class ResidentialFloorModel implements Serializable {
    String ProjectCategoryTypeId;
    String NeighborhoodName;
    String Area;
    String AreaUnitId;
    String UnitFrom;
    String UnitTo;
    String AccommodationId;
    String ProjectFloorId;
    String id;
    String length;
    String width;

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    ArrayList<Properties> Properties = new ArrayList<>();

    public ResidentialFloorModel() {
    }

    public String getProjectCategoryTypeId() {
        return ProjectCategoryTypeId;
    }

    public void setProjectCategoryTypeId(String projectCategoryTypeId) {
        ProjectCategoryTypeId = projectCategoryTypeId;
    }

    public String getNeighborhoodName() {
        return NeighborhoodName;
    }

    public void setNeighborhoodName(String neighborhoodName) {
        NeighborhoodName = neighborhoodName;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getAreaUnitId() {
        return AreaUnitId;
    }

    public void setAreaUnitId(String areaUnitId) {
        AreaUnitId = areaUnitId;
    }

    public String getUnitFrom() {
        return UnitFrom;
    }

    public void setUnitFrom(String unitFrom) {
        UnitFrom = unitFrom;
    }

    public String getUnitTo() {
        return UnitTo;
    }

    public void setUnitTo(String unitTo) {
        UnitTo = unitTo;
    }

    public String getAccommodationId() {
        return AccommodationId;
    }

    public void setAccommodationId(String accommodationId) {
        AccommodationId = accommodationId;
    }

    public String getProjectFloorId() {
        return ProjectFloorId;
    }

    public void setProjectFloorId(String projectFloorId) {
        ProjectFloorId = projectFloorId;
    }

    public ArrayList<Properties> getProperties() {
        return Properties;
    }

    public void setProperties(ArrayList<Properties> properties) {
        Properties = properties;
    }

    public static class Properties implements Serializable {
        String PropertyTypeId;
        String CompanyId;
        String CreatedById;
        String UpdatedById;
        String NoOfFloors;
        String NoOfBedrooms;
        String NoOfParkings;
        ArrayList<PropertyLocations> PropertyLocations = new ArrayList<>();
        ArrayList<PropertyAreas> PropertyAreas = new ArrayList<>();
        ArrayList<PropertyImages> PropertyImages = new ArrayList<>();
        ArrayList<PropertyBedroomModel> PropertyBedrooms = new ArrayList<>();
        ArrayList<PhotosModel> photosModels = new ArrayList<>();


        public ArrayList<PhotosModel> getPhotosModels() {
            return photosModels;
        }

        public void setPhotosModels(ArrayList<PhotosModel> photosModels) {
            this.photosModels = photosModels;
        }

        public ArrayList<PropertyBedroomModel> getPropertyBedrooms() {
            return PropertyBedrooms;
        }

        public void setPropertyBedrooms(ArrayList<PropertyBedroomModel> propertyBedrooms) {
            PropertyBedrooms = propertyBedrooms;
        }

        public Properties() {
        }

        public String getPropertyTypeId() {
            return PropertyTypeId;
        }

        public void setPropertyTypeId(String propertyTypeId) {
            PropertyTypeId = propertyTypeId;
        }

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String companyId) {
            CompanyId = companyId;
        }

        public String getCreatedById() {
            return CreatedById;
        }

        public void setCreatedById(String createdById) {
            CreatedById = createdById;
        }

        public String getUpdatedById() {
            return UpdatedById;
        }

        public void setUpdatedById(String updatedById) {
            UpdatedById = updatedById;
        }

        public String getNoOfFloors() {
            return NoOfFloors;
        }

        public void setNoOfFloors(String noOfFloors) {
            NoOfFloors = noOfFloors;
        }

        public String getNoOfBedrooms() {
            return NoOfBedrooms;
        }

        public void setNoOfBedrooms(String noOfBedrooms) {
            NoOfBedrooms = noOfBedrooms;
        }

        public String getNoOfParkings() {
            return NoOfParkings;
        }

        public void setNoOfParkings(String noOfParkings) {
            NoOfParkings = noOfParkings;
        }

        public ArrayList<PropertyLocations> getPropertyLocations() {
            return PropertyLocations;
        }

        public void setPropertyLocations(ArrayList<PropertyLocations> propertyLocations) {
            PropertyLocations = propertyLocations;
        }

        public ArrayList<PropertyAreas> getPropertyAreas() {
            return PropertyAreas;
        }

        public void setPropertyAreas(ArrayList<PropertyAreas> propertyAreas) {
            PropertyAreas = propertyAreas;
        }

        public ArrayList<PropertyImages> getPropertyImages() {
            return PropertyImages;
        }

        public void setPropertyImages(ArrayList<PropertyImages> propertyImages) {
            PropertyImages = propertyImages;
        }

        public static class PropertyLocations implements Serializable {
            String ProjectId;
            String UnitNo;

            public PropertyLocations() {
            }

            public String getProjectId() {
                return ProjectId;
            }

            public void setProjectId(String projectId) {
                ProjectId = projectId;
            }

            public String getUnitNo() {
                return UnitNo;
            }

            public void setUnitNo(String unitNo) {
                UnitNo = unitNo;
            }
        }

        public static class PropertyAreas implements Serializable {
            String FrontSize;
            String FrontSizeUnitId;
            String DepthSize;
            String DepthSizeUnitId;
            String PlotOrCoverArea;
            String PlotOrCoverAreaUnitId;
            String SuperArea;
            String SuperAreaUnitId;
            String BuiltUpArea;
            String BuiltUpAreaUnitId;
            String CarpetArea;
            String CarpetAreaUnitId;

            public PropertyAreas() {
            }

            public String getFrontSize() {
                return FrontSize;
            }

            public void setFrontSize(String frontSize) {
                FrontSize = frontSize;
            }

            public String getFrontSizeUnitId() {
                return FrontSizeUnitId;
            }

            public void setFrontSizeUnitId(String frontSizeUnitId) {
                FrontSizeUnitId = frontSizeUnitId;
            }

            public String getDepthSize() {
                return DepthSize;
            }

            public void setDepthSize(String depthSize) {
                DepthSize = depthSize;
            }

            public String getDepthSizeUnitId() {
                return DepthSizeUnitId;
            }

            public void setDepthSizeUnitId(String depthSizeUnitId) {
                DepthSizeUnitId = depthSizeUnitId;
            }

            public String getPlotOrCoverArea() {
                return PlotOrCoverArea;
            }

            public void setPlotOrCoverArea(String plotOrCoverArea) {
                PlotOrCoverArea = plotOrCoverArea;
            }

            public String getPlotOrCoverAreaUnitId() {
                return PlotOrCoverAreaUnitId;
            }

            public void setPlotOrCoverAreaUnitId(String plotOrCoverAreaUnitId) {
                PlotOrCoverAreaUnitId = plotOrCoverAreaUnitId;
            }

            public String getSuperArea() {
                return SuperArea;
            }

            public void setSuperArea(String superArea) {
                SuperArea = superArea;
            }

            public String getSuperAreaUnitId() {
                return SuperAreaUnitId;
            }

            public void setSuperAreaUnitId(String superAreaUnitId) {
                SuperAreaUnitId = superAreaUnitId;
            }

            public String getBuiltUpArea() {
                return BuiltUpArea;
            }

            public void setBuiltUpArea(String builtUpArea) {
                BuiltUpArea = builtUpArea;
            }

            public String getBuiltUpAreaUnitId() {
                return BuiltUpAreaUnitId;
            }

            public void setBuiltUpAreaUnitId(String builtUpAreaUnitId) {
                BuiltUpAreaUnitId = builtUpAreaUnitId;
            }

            public String getCarpetArea() {
                return CarpetArea;
            }

            public void setCarpetArea(String carpetArea) {
                CarpetArea = carpetArea;
            }

            public String getCarpetAreaUnitId() {
                return CarpetAreaUnitId;
            }

            public void setCarpetAreaUnitId(String carpetAreaUnitId) {
                CarpetAreaUnitId = carpetAreaUnitId;
            }
        }

        public static class PropertyImages implements Serializable {
            String Id;
            String IsDeletable;

            public PropertyImages() {
            }

            public String getId() {
                return Id;
            }

            public void setId(String id) {
                Id = id;
            }

            public String getIsDeletable() {
                return IsDeletable;
            }

            public void setIsDeletable(String isDeletable) {
                IsDeletable = isDeletable;
            }
        }

        public static class PhotosModel {
            String id;
            String name;
            String imageUrl;
            private String randomId;
            int selectedId;


            public String getRandomId() {
                return randomId;
            }

            public int getSelectedId() {
                return selectedId;
            }

            public void setSelectedId(int selectedId) {
                this.selectedId = selectedId;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImageUrl() {
                return imageUrl;
            }

            public void setImageUrl(String imageUrl) {
                this.imageUrl = imageUrl;
            }

            public void setRandomId(String randomId) {
                this.randomId = randomId;
            }
        }


    }



//    public class PropertyBedroomModel {
//
//        String id;
//        String PropertyId;
//        String goundtype;
//        String floor;
//        String room_name;
//        String room_type;
//        String length;
//        String width;
//        String room_size;
//        String BedRoomSizeUnitId;
//        String LengthWidthUnitId;
//        String Category;
//        String SizeUnit;
//        ArrayList<com.bnhabitat.models.PropertyBedroomModel.AttachPropertyBedroomId> attachPropertyBedroomIds = new ArrayList<>();
//
//        public ArrayList<com.bnhabitat.models.PropertyBedroomModel.AttachPropertyBedroomId> getAttachPropertyBedroomIds() {
//            return attachPropertyBedroomIds;
//        }
//
//        public void setAttachPropertyBedroomIds(ArrayList<com.bnhabitat.models.PropertyBedroomModel.AttachPropertyBedroomId> attachPropertyBedroomIds) {
//            this.attachPropertyBedroomIds = attachPropertyBedroomIds;
//        }
//
//        public String getSizeUnit() {
//            return SizeUnit;
//        }
//
//        public void setSizeUnit(String sizeUnit) {
//            SizeUnit = sizeUnit;
//        }
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getPropertyId() {
//            return PropertyId;
//        }
//
//        public void setPropertyId(String propertyId) {
//            PropertyId = propertyId;
//        }
//
//        public String getBedRoomSizeUnitId() {
//            return BedRoomSizeUnitId;
//        }
//
//        public void setBedRoomSizeUnitId(String bedRoomSizeUnitId) {
//            BedRoomSizeUnitId = bedRoomSizeUnitId;
//        }
//
//        public String getLengthWidthUnitId() {
//            return LengthWidthUnitId;
//        }
//
//        public void setLengthWidthUnitId(String lengthWidthUnitId) {
//            LengthWidthUnitId = lengthWidthUnitId;
//        }
//
//        public String getCategory() {
//            return Category;
//        }
//
//        public void setCategory(String category) {
//            Category = category;
//        }
//
//        public String getGoundtype() {
//            return goundtype;
//        }
//
//        public void setGoundtype(String goundtype) {
//            this.goundtype = goundtype;
//        }
//
//        public String getFloor() {
//            return floor;
//        }
//
//        public void setFloor(String floor) {
//            this.floor = floor;
//        }
//
//        public String getRoom_name() {
//            return room_name;
//        }
//
//        public void setRoom_name(String room_name) {
//            this.room_name = room_name;
//        }
//
//        public String getRoom_type() {
//            return room_type;
//        }
//
//        public void setRoom_type(String room_type) {
//            this.room_type = room_type;
//        }
//
//        public String getLength() {
//            return length;
//        }
//
//        public void setLength(String length) {
//            this.length = length;
//        }
//
//        public String getWidth() {
//            return width;
//        }
//
//        public void setWidth(String width) {
//            this.width = width;
//        }
//
//        public String getRoom_size() {
//            return room_size;
//        }
//
//        public void setRoom_size(String room_size) {
//            this.room_size = room_size;
//        }
//
//        public class AttachPropertyBedroomId implements Serializable {
//            String AttachPropertyBedroomId;
//            String AttachPropertyBedroomName;
//
//            public String getAttachPropertyBedroomName() {
//                return AttachPropertyBedroomName;
//            }
//
//            public void setAttachPropertyBedroomName(String attachPropertyBedroomName) {
//                AttachPropertyBedroomName = attachPropertyBedroomName;
//            }
//
//            public String getAttachPropertyBedroomId() {
//                return AttachPropertyBedroomId;
//            }
//
//            public void setAttachPropertyBedroomId(String attachPropertyBedroomId) {
//                AttachPropertyBedroomId = attachPropertyBedroomId;
//            }
//        }
//    }

}

