package com.bnhabitat.township.model;

public class GetLocationModel {

    String Version;
    String StatusCode;
    String Message;

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    Result Result;

    public GetLocationModel.Result getResult() {
        return Result;
    }

    public void setResult(GetLocationModel.Result result) {
        Result = result;
    }

    public class Result {
        String Id;
        String OfficeName;
        String PinCode;
        String OfficeType;
        String DeliveryStatus;
        String DivisionName;
        String RegionName;
        String CircleName;
        String Taluk;
        String DistrictName;
        String StateName;
        String Telephone;
        String RelatedSubOffice;
        String RelatedHeadOffice;
        String Longitude;
        String Latitude;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getOfficeName() {
            return OfficeName;
        }

        public void setOfficeName(String officeName) {
            OfficeName = officeName;
        }

        public String getPinCode() {
            return PinCode;
        }

        public void setPinCode(String pinCode) {
            PinCode = pinCode;
        }

        public String getOfficeType() {
            return OfficeType;
        }

        public void setOfficeType(String officeType) {
            OfficeType = officeType;
        }

        public String getDeliveryStatus() {
            return DeliveryStatus;
        }

        public void setDeliveryStatus(String deliveryStatus) {
            DeliveryStatus = deliveryStatus;
        }

        public String getDivisionName() {
            return DivisionName;
        }

        public void setDivisionName(String divisionName) {
            DivisionName = divisionName;
        }

        public String getRegionName() {
            return RegionName;
        }

        public void setRegionName(String regionName) {
            RegionName = regionName;
        }

        public String getCircleName() {
            return CircleName;
        }

        public void setCircleName(String circleName) {
            CircleName = circleName;
        }

        public String getTaluk() {
            return Taluk;
        }

        public void setTaluk(String taluk) {
            Taluk = taluk;
        }

        public String getDistrictName() {
            return DistrictName;
        }

        public void setDistrictName(String districtName) {
            DistrictName = districtName;
        }

        public String getStateName() {
            return StateName;
        }

        public void setStateName(String stateName) {
            StateName = stateName;
        }

        public String getTelephone() {
            return Telephone;
        }

        public void setTelephone(String telephone) {
            Telephone = telephone;
        }

        public String getRelatedSubOffice() {
            return RelatedSubOffice;
        }

        public void setRelatedSubOffice(String relatedSubOffice) {
            RelatedSubOffice = relatedSubOffice;
        }

        public String getRelatedHeadOffice() {
            return RelatedHeadOffice;
        }

        public void setRelatedHeadOffice(String relatedHeadOffice) {
            RelatedHeadOffice = relatedHeadOffice;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }
    }
}
