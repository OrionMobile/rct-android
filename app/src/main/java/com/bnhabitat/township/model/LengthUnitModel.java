package com.bnhabitat.township.model;

import java.util.ArrayList;

public class LengthUnitModel {

    String Version;
    String StatusCode;
    String Message;



    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }


    ArrayList<Result> Result = new ArrayList<>();

    public ArrayList<Result> getResult() {
        return Result;
    }

    public void setResult(ArrayList<Result> result) {
        Result = result;
    }

    public class Result {
        String Id;
        String SizeUnit;
        String IsActive;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getSizeUnit() {
            return SizeUnit;
        }

        public void setSizeUnit(String sizeUnit) {
            SizeUnit = sizeUnit;
        }

        public String getIsActive() {
            return IsActive;
        }

        public void setIsActive(String isActive) {
            IsActive = isActive;
        }
    }
}
