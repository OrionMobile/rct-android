package com.bnhabitat.township.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ProjectCategoryTypesModel implements Serializable {

    int ProjectId,TotalBasement,PropertyType;
    String CategoryType,Type,BasementType,Project;

    ArrayList<AccomodationModel> accomodationModels= new ArrayList<>();


    public ProjectCategoryTypesModel() {
    }

    String Id, CategoryTypeId;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCategoryTypeId() {
        return CategoryTypeId;
    }

    public void setCategoryTypeId(String categoryTypeId) {
        CategoryTypeId = categoryTypeId;
    }

}
