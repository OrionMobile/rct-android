package com.bnhabitat.township.model;

public class ImageUploadModel {

    String Id;
    String Name;
    String Url;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public ImageUploadModel(String id, String name, String url) {
        Id = id;
        Name = name;
        Url = url;
    }
}
