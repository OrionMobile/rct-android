package com.bnhabitat.township.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PlotLandLot implements Serializable {
    String ProjectCategoryTypeId;
    String NeighborhoodName;
    String Area;
    String AreaUnitId;
    String UnitFrom;
    String UnitTo;
    String AccommodationId;
    String ProjectFloorId;

    ArrayList<Properties> Properties = new ArrayList<>();

    public PlotLandLot() {
    }

    public String getProjectCategoryTypeId() {
        return ProjectCategoryTypeId;
    }

    public void setProjectCategoryTypeId(String projectCategoryTypeId) {
        ProjectCategoryTypeId = projectCategoryTypeId;
    }

    public String getNeighborhoodName() {
        return NeighborhoodName;
    }

    public void setNeighborhoodName(String neighborhoodName) {
        NeighborhoodName = neighborhoodName;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getAreaUnitId() {
        return AreaUnitId;
    }

    public void setAreaUnitId(String areaUnitId) {
        AreaUnitId = areaUnitId;
    }

    public String getUnitFrom() {
        return UnitFrom;
    }

    public void setUnitFrom(String unitFrom) {
        UnitFrom = unitFrom;
    }

    public String getUnitTo() {
        return UnitTo;
    }

    public void setUnitTo(String unitTo) {
        UnitTo = unitTo;
    }

    public String getAccommodationId() {
        return AccommodationId;
    }

    public void setAccommodationId(String accommodationId) {
        AccommodationId = accommodationId;
    }

    public String getProjectFloorId() {
        return ProjectFloorId;
    }

    public void setProjectFloorId(String projectFloorId) {
        ProjectFloorId = projectFloorId;
    }

    public ArrayList<Properties> getProperties() {
        return Properties;
    }

    public void setProperties(ArrayList<Properties> properties) {
        Properties = properties;
    }

    public static class Properties implements Serializable{
        String PropertyTypeId;
        String CompanyId;
        String CreatedById;
        String UpdatedById;
        String NoOfFloors;
        String NoOfBedrooms;
        String NoOfParkings;
        ArrayList<PropertyLocations> PropertyLocations = new ArrayList<>();
        ArrayList<PropertyAreas> PropertyAreas = new ArrayList<>();
        ArrayList<PropertyImages> PropertyImages = new ArrayList<>();

        public Properties() {
        }

        public String getPropertyTypeId() {
            return PropertyTypeId;
        }

        public void setPropertyTypeId(String propertyTypeId) {
            PropertyTypeId = propertyTypeId;
        }

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String companyId) {
            CompanyId = companyId;
        }

        public String getCreatedById() {
            return CreatedById;
        }

        public void setCreatedById(String createdById) {
            CreatedById = createdById;
        }

        public String getUpdatedById() {
            return UpdatedById;
        }

        public void setUpdatedById(String updatedById) {
            UpdatedById = updatedById;
        }

        public String getNoOfFloors() {
            return NoOfFloors;
        }

        public void setNoOfFloors(String noOfFloors) {
            NoOfFloors = noOfFloors;
        }

        public String getNoOfBedrooms() {
            return NoOfBedrooms;
        }

        public void setNoOfBedrooms(String noOfBedrooms) {
            NoOfBedrooms = noOfBedrooms;
        }

        public String getNoOfParkings() {
            return NoOfParkings;
        }

        public void setNoOfParkings(String noOfParkings) {
            NoOfParkings = noOfParkings;
        }

        public ArrayList<PropertyLocations> getPropertyLocations() {
            return PropertyLocations;
        }

        public void setPropertyLocations(ArrayList<PropertyLocations> propertyLocations) {
            PropertyLocations = propertyLocations;
        }

        public ArrayList<PropertyAreas> getPropertyAreas() {
            return PropertyAreas;
        }

        public void setPropertyAreas(ArrayList<PropertyAreas> propertyAreas) {
            PropertyAreas = propertyAreas;
        }

        public ArrayList<PropertyImages> getPropertyImages() {
            return PropertyImages;
        }

        public void setPropertyImages(ArrayList<PropertyImages> propertyImages) {
            PropertyImages = propertyImages;
        }

        public static class PropertyLocations implements Serializable{
            String ProjectId;
            String UnitNo;

            public PropertyLocations() {
            }

            public String getProjectId() {
                return ProjectId;
            }

            public void setProjectId(String projectId) {
                ProjectId = projectId;
            }

            public String getUnitNo() {
                return UnitNo;
            }

            public void setUnitNo(String unitNo) {
                UnitNo = unitNo;
            }
        }

        public static class PropertyAreas implements Serializable{
            String FrontSize;
            String FrontSizeUnitId;
            String DepthSize;
            String DepthSizeUnitId;
            String PlotOrCoverArea;
            String PlotOrCoverAreaUnitId;
            String SuperArea;
            String SuperAreaUnitId;
            String BuiltUpArea;
            String BuiltUpAreaUnitId;
            String CarpetArea;
            String CarpetAreaUnitId;

            public PropertyAreas() {
            }

            public String getFrontSize() {
                return FrontSize;
            }

            public void setFrontSize(String frontSize) {
                FrontSize = frontSize;
            }

            public String getFrontSizeUnitId() {
                return FrontSizeUnitId;
            }

            public void setFrontSizeUnitId(String frontSizeUnitId) {
                FrontSizeUnitId = frontSizeUnitId;
            }

            public String getDepthSize() {
                return DepthSize;
            }

            public void setDepthSize(String depthSize) {
                DepthSize = depthSize;
            }

            public String getDepthSizeUnitId() {
                return DepthSizeUnitId;
            }

            public void setDepthSizeUnitId(String depthSizeUnitId) {
                DepthSizeUnitId = depthSizeUnitId;
            }

            public String getPlotOrCoverArea() {
                return PlotOrCoverArea;
            }

            public void setPlotOrCoverArea(String plotOrCoverArea) {
                PlotOrCoverArea = plotOrCoverArea;
            }

            public String getPlotOrCoverAreaUnitId() {
                return PlotOrCoverAreaUnitId;
            }

            public void setPlotOrCoverAreaUnitId(String plotOrCoverAreaUnitId) {
                PlotOrCoverAreaUnitId = plotOrCoverAreaUnitId;
            }

            public String getSuperArea() {
                return SuperArea;
            }

            public void setSuperArea(String superArea) {
                SuperArea = superArea;
            }

            public String getSuperAreaUnitId() {
                return SuperAreaUnitId;
            }

            public void setSuperAreaUnitId(String superAreaUnitId) {
                SuperAreaUnitId = superAreaUnitId;
            }

            public String getBuiltUpArea() {
                return BuiltUpArea;
            }

            public void setBuiltUpArea(String builtUpArea) {
                BuiltUpArea = builtUpArea;
            }

            public String getBuiltUpAreaUnitId() {
                return BuiltUpAreaUnitId;
            }

            public void setBuiltUpAreaUnitId(String builtUpAreaUnitId) {
                BuiltUpAreaUnitId = builtUpAreaUnitId;
            }

            public String getCarpetArea() {
                return CarpetArea;
            }

            public void setCarpetArea(String carpetArea) {
                CarpetArea = carpetArea;
            }

            public String getCarpetAreaUnitId() {
                return CarpetAreaUnitId;
            }

            public void setCarpetAreaUnitId(String carpetAreaUnitId) {
                CarpetAreaUnitId = carpetAreaUnitId;
            }
        }

        public static class PropertyImages implements Serializable{
            String Id;
            String IsDeletable;

            public PropertyImages() {
            }

            public String getId() {
                return Id;
            }

            public void setId(String id) {
                Id = id;
            }

            public String getIsDeletable() {
                return IsDeletable;
            }

            public void setIsDeletable(String isDeletable) {
                IsDeletable = isDeletable;
            }
        }

    }
}
