package com.bnhabitat.township.model;

import java.util.ArrayList;

public class FloorUnitModelUnit {

    int value;
    private ArrayList<FloorUnitModel> plotlotUnitModels = new ArrayList<>();

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ArrayList<FloorUnitModel> getPlotlotUnitModels() {
        return plotlotUnitModels;
    }

    public void setPlotlotUnitModels(ArrayList<FloorUnitModel> plotlotUnitModels) {
        this.plotlotUnitModels = plotlotUnitModels;
    }
}
