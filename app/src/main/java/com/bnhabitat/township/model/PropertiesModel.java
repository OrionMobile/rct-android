package com.bnhabitat.township.model;

import java.util.ArrayList;

public class PropertiesModel {

    int Id;
    int ProjectCategoryTypeId;
    String NeighborhoodName;
    int Area;
    int AreaUnitId;
    String UnitFrom;
    String UnitTo;
    int AccommodationId;
    int ProjectFloorId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getProjectCategoryTypeId() {
        return ProjectCategoryTypeId;
    }

    public void setProjectCategoryTypeId(int projectCategoryTypeId) {
        ProjectCategoryTypeId = projectCategoryTypeId;
    }

    public String getNeighborhoodName() {
        return NeighborhoodName;
    }

    public void setNeighborhoodName(String neighborhoodName) {
        NeighborhoodName = neighborhoodName;
    }

    public int getArea() {
        return Area;
    }

    public void setArea(int area) {
        Area = area;
    }

    public int getAreaUnitId() {
        return AreaUnitId;
    }

    public void setAreaUnitId(int areaUnitId) {
        AreaUnitId = areaUnitId;
    }

    public String getUnitFrom() {
        return UnitFrom;
    }

    public void setUnitFrom(String unitFrom) {
        UnitFrom = unitFrom;
    }

    public String getUnitTo() {
        return UnitTo;
    }

    public void setUnitTo(String unitTo) {
        UnitTo = unitTo;
    }

    public int getAccommodationId() {
        return AccommodationId;
    }

    public void setAccommodationId(int accommodationId) {
        AccommodationId = accommodationId;
    }

    public int getProjectFloorId() {
        return ProjectFloorId;
    }

    public void setProjectFloorId(int projectFloorId) {
        ProjectFloorId = projectFloorId;
    }

    public ArrayList<PropertyLocationsModels> getPropertiesModels() {
        return propertiesModels;
    }

    public void setPropertiesModels(ArrayList<PropertyLocationsModels> propertiesModels) {
        this.propertiesModels = propertiesModels;
    }

    ArrayList<PropertyLocationsModels> propertiesModels = new ArrayList<>();

}


