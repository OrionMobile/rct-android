package com.bnhabitat.township.model;

import java.io.Serializable;

public class RowHouse_Villa_Model implements Serializable {

    String streetName, area, spnrArea, unit_from, unit_to, unit_lay_text, width, height, spnrUnit, floor_text, bedroom_text, carP_text;
    String coverArea, spnrCover, superArea, spnrSuper, buildArea, spnrBuild, carpetArea, spnrCarpet;

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSpnrArea() {
        return spnrArea;
    }

    public void setSpnrArea(String spnrArea) {
        this.spnrArea = spnrArea;
    }

    public String getUnit_from() {
        return unit_from;
    }

    public void setUnit_from(String unit_from) {
        this.unit_from = unit_from;
    }

    public String getUnit_to() {
        return unit_to;
    }

    public void setUnit_to(String unit_to) {
        this.unit_to = unit_to;
    }

    public String getUnit_lay_text() {
        return unit_lay_text;
    }

    public void setUnit_lay_text(String unit_lay_text) {
        this.unit_lay_text = unit_lay_text;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getSpnrUnit() {
        return spnrUnit;
    }

    public void setSpnrUnit(String spnrUnit) {
        this.spnrUnit = spnrUnit;
    }

    public String getFloor_text() {
        return floor_text;
    }

    public void setFloor_text(String floor_text) {
        this.floor_text = floor_text;
    }

    public String getBedroom_text() {
        return bedroom_text;
    }

    public void setBedroom_text(String bedroom_text) {
        this.bedroom_text = bedroom_text;
    }

    public String getCarP_text() {
        return carP_text;
    }

    public void setCarP_text(String carP_text) {
        this.carP_text = carP_text;
    }

    public String getCoverArea() {
        return coverArea;
    }

    public void setCoverArea(String coverArea) {
        this.coverArea = coverArea;
    }

    public String getSpnrCover() {
        return spnrCover;
    }

    public void setSpnrCover(String spnrCover) {
        this.spnrCover = spnrCover;
    }

    public String getSuperArea() {
        return superArea;
    }

    public void setSuperArea(String superArea) {
        this.superArea = superArea;
    }

    public String getSpnrSuper() {
        return spnrSuper;
    }

    public void setSpnrSuper(String spnrSuper) {
        this.spnrSuper = spnrSuper;
    }

    public String getBuildArea() {
        return buildArea;
    }

    public void setBuildArea(String buildArea) {
        this.buildArea = buildArea;
    }

    public String getSpnrBuild() {
        return spnrBuild;
    }

    public void setSpnrBuild(String spnrBuild) {
        this.spnrBuild = spnrBuild;
    }

    public String getCarpetArea() {
        return carpetArea;
    }

    public void setCarpetArea(String carpetArea) {
        this.carpetArea = carpetArea;
    }

    public String getSpnrCarpet() {
        return spnrCarpet;
    }

    public void setSpnrCarpet(String spnrCarpet) {
        this.spnrCarpet = spnrCarpet;
    }
}
