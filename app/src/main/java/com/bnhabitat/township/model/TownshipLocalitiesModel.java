package com.bnhabitat.township.model;

public class TownshipLocalitiesModel {

    int Id;
   String LocalityName;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getLocalityName() {
        return LocalityName;
    }

    public void setLocalityName(String localityName) {
        LocalityName = localityName;
    }

    public TownshipLocalitiesModel(int id, String localityName) {
        Id = id;
        LocalityName = localityName;
    }

    public TownshipLocalitiesModel(){

    }
}
