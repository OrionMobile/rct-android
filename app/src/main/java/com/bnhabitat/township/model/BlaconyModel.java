package com.bnhabitat.township.model;

import com.bnhabitat.models.PropertyBedroomModel;

import java.io.Serializable;
import java.util.ArrayList;

public class BlaconyModel {
    int Id;
    int AccommodationId;
    String BedRoomName;
    String BedRoomType;
    int BedRoomSize;
    int BedRoomSizeUnitId;
    int Length;
    int Width;
    int LengthWidthUnitId;
    String Category;
    int AccommodationFloorId;
    boolean IsDeletable;
    boolean IsModify;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getAccommodationId() {
        return AccommodationId;
    }

    public void setAccommodationId(int accommodationId) {
        AccommodationId = accommodationId;
    }

    public String getBedRoomName() {
        return BedRoomName;
    }

    public void setBedRoomName(String bedRoomName) {
        BedRoomName = bedRoomName;
    }

    public String getBedRoomType() {
        return BedRoomType;
    }

    public void setBedRoomType(String bedRoomType) {
        BedRoomType = bedRoomType;
    }

    public int getBedRoomSize() {
        return BedRoomSize;
    }

    public void setBedRoomSize(int bedRoomSize) {
        BedRoomSize = bedRoomSize;
    }

    public int getBedRoomSizeUnitId() {
        return BedRoomSizeUnitId;
    }

    public void setBedRoomSizeUnitId(int bedRoomSizeUnitId) {
        BedRoomSizeUnitId = bedRoomSizeUnitId;
    }

    public int getLength() {
        return Length;
    }

    public void setLength(int length) {
        Length = length;
    }

    public int getWidth() {
        return Width;
    }

    public void setWidth(int width) {
        Width = width;
    }

    public int getLengthWidthUnitId() {
        return LengthWidthUnitId;
    }

    public void setLengthWidthUnitId(int lengthWidthUnitId) {
        LengthWidthUnitId = lengthWidthUnitId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public int getAccommodationFloorId() {
        return AccommodationFloorId;
    }

    public void setAccommodationFloorId(int accommodationFloorId) {
        AccommodationFloorId = accommodationFloorId;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }

    public ArrayList<AccommodationBedroomAttachWith> getAttachWithArrayList() {
        return attachWithArrayList;
    }

    public void setAttachWithArrayList(ArrayList<AccommodationBedroomAttachWith> attachWithArrayList) {
        this.attachWithArrayList = attachWithArrayList;
    }

    ArrayList<AccommodationBedroomAttachWith> attachWithArrayList=new ArrayList<>();

    public class AccommodationBedroomAttachWith implements Serializable{

        int Id;
        int AccommodationRoomId;
        int AttachAccommodationRoomId;
        String AttachAccommodationRoomName;
        boolean IsDeletable;
        boolean IsModify;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getAccommodationRoomId() {
            return AccommodationRoomId;
        }

        public void setAccommodationRoomId(int accommodationRoomId) {
            AccommodationRoomId = accommodationRoomId;
        }

        public int getAttachAccommodationRoomId() {
            return AttachAccommodationRoomId;
        }

        public void setAttachAccommodationRoomId(int attachAccommodationRoomId) {
            AttachAccommodationRoomId = attachAccommodationRoomId;
        }

        public String getAttachAccommodationRoomName() {
            return AttachAccommodationRoomName;
        }

        public void setAttachAccommodationRoomName(String attachAccommodationRoomName) {
            AttachAccommodationRoomName = attachAccommodationRoomName;
        }

        public boolean isDeletable() {
            return IsDeletable;
        }

        public void setDeletable(boolean deletable) {
            IsDeletable = deletable;
        }

        public boolean isModify() {
            return IsModify;
        }

        public void setModify(boolean modify) {
            IsModify = modify;
        }
    }

}
