package com.bnhabitat.township.model;

import java.io.Serializable;

public class ProjectAmenityModel implements Serializable {

    String Type;
    String Title;
    String Value;

    public ProjectAmenityModel() {

    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
