package com.bnhabitat.township.model;

public class FloorUnitModel {

    int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
