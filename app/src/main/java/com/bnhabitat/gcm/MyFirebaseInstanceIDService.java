package com.bnhabitat.gcm;

import android.util.Log;

import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by gourav on 11/17/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    private static final String TAG = "MyFirebaseInstance";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
 public void sendRegistrationToServer(String token){
     Log.d(TAG, "Refreshed token: " + token);
     PreferenceConnector.getInstance(this).savePreferences(Constants.REGISTRATION_ID,token);

 }
}
