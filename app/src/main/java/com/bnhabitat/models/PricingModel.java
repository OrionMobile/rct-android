package com.bnhabitat.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by developer15 on 3/11/2016.
 */
public class PricingModel implements Serializable{

    int sizeId;
    ArrayList<PriceDetails> priceDetails = new ArrayList<>();

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public ArrayList<PriceDetails> getPriceDetails() {
        return priceDetails;
    }

    public void setPriceDetails(ArrayList<PriceDetails> priceDetails) {
        this.priceDetails = priceDetails;
    }

    public class PriceDetails implements Serializable{

        int projprprtycomponentID;
        String name;
        String description;
        boolean isActive;
        int priority;
        String paymentOptions;
        double serviceTax;
        boolean applyMultipleSubComponents;


        public double getServiceTax() {
            return serviceTax;
        }

        public void setServiceTax(double serviceTax) {
            this.serviceTax = serviceTax;
        }

        ArrayList<ProjectPropertySubComponents> projectPropertySubComponents = new ArrayList<>();

        public ArrayList<ProjectPropertySubComponents> getProjectPropertySubComponents() {
            return projectPropertySubComponents;
        }

        public void setProjectPropertySubComponents(ArrayList<ProjectPropertySubComponents> projectPropertySubComponents) {
            this.projectPropertySubComponents = projectPropertySubComponents;
        }

        public int getProjprprtycomponentID() {
            return projprprtycomponentID;
        }

        public void setProjprprtycomponentID(int projprprtycomponentID) {
            this.projprprtycomponentID = projprprtycomponentID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isActive() {
            return isActive;
        }

        public void setActive(boolean active) {
            isActive = active;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public String getPaymentOptions() {
            return paymentOptions;
        }

        public void setPaymentOptions(String paymentOptions) {
            this.paymentOptions = paymentOptions;
        }

        public boolean isApplyMultipleSubComponents() {
            return applyMultipleSubComponents;
        }

        public void setApplyMultipleSubComponents(boolean applyMultipleSubComponents) {
            this.applyMultipleSubComponents = applyMultipleSubComponents;
        }


        public class ProjectPropertySubComponents implements Serializable{

            int componentId;
            int id;
            String title;
            String description;
            boolean active;
            boolean floor;
            String paymentOption;
            boolean compulsory;
            String amount;
            int noOfParking;
            String options;
            double serviceTax;

            public double getServiceTax() {
                return serviceTax;
            }

            public void setServiceTax(double serviceTax) {
                this.serviceTax = serviceTax;
            }

            public String getOptions() {
                return options;
            }

            public void setOptions(String options) {
                this.options = options;
            }

            public int getComponentId() {
                return componentId;
            }

            public void setComponentId(int componentId) {
                this.componentId = componentId;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public boolean isActive() {
                return active;
            }

            public void setActive(boolean active) {
                this.active = active;
            }

            public boolean isFloor() {
                return floor;
            }

            public void setFloor(boolean floor) {
                this.floor = floor;
            }

            public String getPaymentOption() {
                return paymentOption;
            }

            public void setPaymentOption(String paymentOption) {
                this.paymentOption = paymentOption;
            }

            public boolean isCompulsory() {
                return compulsory;
            }

            public void setCompulsory(boolean compulsory) {
                this.compulsory = compulsory;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public int getNoOfParking() {
                return noOfParking;
            }

            public void setNoOfParking(int noOfParking) {
                this.noOfParking = noOfParking;
            }
        }
    }

}
