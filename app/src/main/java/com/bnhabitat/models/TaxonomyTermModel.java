package com.bnhabitat.models;

import java.util.ArrayList;

/**
 * Created by gourav on 12/12/2017.
 */

public class TaxonomyTermModel {
   String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    ArrayList<TermData> Terms;

    public ArrayList<TermData> getTerms() {
        return Terms;
    }

    public void setTerms(ArrayList<TermData> terms) {
        Terms = terms;
    }

    public class TermData{
   String Id;
   String Name;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}

}
