package com.bnhabitat.models;

/**
 * Created by gourav on 3/22/2018.
 */

public class PropertyBedroomType {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
