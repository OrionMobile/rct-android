package com.bnhabitat.models;

public class AccomDetailModel {

    String Name;
    int Id;
    String Type;
    int TotalCount;
    boolean IsDeletable;
    boolean IsModify;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(int totalCount) {
        TotalCount = totalCount;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }
}
