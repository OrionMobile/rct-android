package com.bnhabitat.models;

import java.math.BigDecimal;

/**
 * Created by Android on 5/23/2017.
 */

public class CommonDialogModel {
    String id;
    String title;
    double areaSize;
    double currencyDPrice;
    String price="";
    BigDecimal totalAmount;
    String superArea;
    String carpetArea;
    String builtupArea;
    String type="";
    String unitName="";
    int actualPrice;
    int propertyTypeId;
    int sizeUnitId;

    String DefaultUnitID;
    String MinUnitRange;
    String MaxUnitRange;

    public String getDefaultUnitID() {
        return DefaultUnitID;
    }

    public void setDefaultUnitID(String defaultUnitID) {
        DefaultUnitID = defaultUnitID;
    }

    public String getMinUnitRange() {
        return MinUnitRange;
    }

    public void setMinUnitRange(String minUnitRange) {
        MinUnitRange = minUnitRange;
    }

    public String getMaxUnitRange() {
        return MaxUnitRange;
    }

    public void setMaxUnitRange(String maxUnitRange) {
        MaxUnitRange = maxUnitRange;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getCurrencyDPrice() {
        return currencyDPrice;
    }

    public void setCurrencyDPrice(double currencyDPrice) {
        this.currencyDPrice = currencyDPrice;
    }

    public int getSizeUnitId() {
        return sizeUnitId;
    }

    public void setSizeUnitId(int sizeUnitId) {
        this.sizeUnitId = sizeUnitId;
    }

    public int getPropertyTypeId() {
        return propertyTypeId;
    }

    public void setPropertyTypeId(int propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    public int getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(int actualPrice) {
        this.actualPrice = actualPrice;
    }


    public double getAreaSize() {
        return areaSize;
    }

    public void setAreaSize(double areaSize) {
        this.areaSize = areaSize;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getSuperArea() {
        return superArea;
    }

    public void setSuperArea(String superArea) {
        this.superArea = superArea;
    }

    public String getCarpetArea() {
        return carpetArea;
    }

    public void setCarpetArea(String carpetArea) {
        this.carpetArea = carpetArea;
    }

    public String getBuiltupArea() {
        return builtupArea;
    }

    public void setBuiltupArea(String builtupArea) {
        this.builtupArea = builtupArea;
    }
}
