package com.bnhabitat.models;

import android.nfc.Tag;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Android on 1/31/2017.
 */
public class ContactsModel implements Serializable {


    private ArrayList<NotesModel> notesModelArrayList;

    public ContactsModel(String name,
                         String phonenumber, String email) {
        this.name = name;
        this.phonenumber = phonenumber;
        this.email = email;

    }

    public ContactsModel() {


    }

    //    ArrayList<Result> results=new ArrayList<>();
    String name;
    String phonenumber;
    String email;
    String group_id;
    String group_name;
    String dob;
    String Prefix;
    String MiddleName;
    String Sufix;
    String MaritalStatus;
    String MarriageAnniversaryDate;
    String MarriageAnniversaryDateString;
    String SourceName;
    String Campaign;
    String SourceDate;
    String OwnerShip;
    String OwnerUserId;
    String IndustryName;
    String Designation;
    String Profession;
    String Religion;
    String AnnualIncome;
    String CompanyId;
    String ExistingImageUrl;
    String ImageUrl;
    String Gender;
    ArrayList<String> tags = new ArrayList<>();
    public String getImageUrl() {
        return ImageUrl;
    }


    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getPrefix() {
        return Prefix;
    }

    public void setPrefix(String prefix) {
        Prefix = prefix;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getSufix() {
        return Sufix;
    }

    public void setSufix(String sufix) {
        Sufix = sufix;
    }

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public String getMarriageAnniversaryDate() {
        return MarriageAnniversaryDate;
    }

    public void setMarriageAnniversaryDate(String marriageAnniversaryDate) {
        MarriageAnniversaryDate = marriageAnniversaryDate;
    }

    public String getMarriageAnniversaryDateString() {
        return MarriageAnniversaryDateString;
    }

    public void setMarriageAnniversaryDateString(String marriageAnniversaryDateString) {
        MarriageAnniversaryDateString = marriageAnniversaryDateString;
    }

    public String getSourceName() {
        return SourceName;
    }

    public void setSourceName(String sourceName) {
        SourceName = sourceName;
    }

    public String getCampaign() {
        return Campaign;
    }

    public void setCampaign(String campaign) {
        Campaign = campaign;
    }

    public String getSourceDate() {
        return SourceDate;
    }

    public void setSourceDate(String sourceDate) {
        SourceDate = sourceDate;
    }

    public String getOwnerShip() {
        return OwnerShip;
    }

    public void setOwnerShip(String ownerShip) {
        OwnerShip = ownerShip;
    }

    public String getOwnerUserId() {
        return OwnerUserId;
    }

    public void setOwnerUserId(String ownerUserId) {
        OwnerUserId = ownerUserId;
    }

    public String getIndustryName() {
        return IndustryName;
    }

    public void setIndustryName(String industryName) {
        IndustryName = industryName;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public String getProfession() {
        return Profession;
    }

    public void setProfession(String profession) {
        Profession = profession;
    }

    public String getReligion() {
        return Religion;
    }

    public void setReligion(String religion) {
        Religion = religion;
    }

    public String getAnnualIncome() {
        return AnnualIncome;
    }

    public void setAnnualIncome(String annualIncome) {
        AnnualIncome = annualIncome;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getExistingImageUrl() {
        return ExistingImageUrl;
    }

    public void setExistingImageUrl(String existingImageUrl) {
        ExistingImageUrl = existingImageUrl;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    String Id;
    String FirstName;
    String LastName;

    String Email;
    String CompanyName;
    String CityName;
    int StateId;
    State state;
    Country country;
    Company company;
    String CreatedById;
    String UpdatedById;
    String CreatedOn;
    String UpdatedOn;
    String DateOfBirthStr;
    String TotalRecords;

    public String getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        TotalRecords = totalRecords;
    }

    public String getDateOfBirthStr() {
        return DateOfBirthStr;
    }

    public void setDateOfBirthStr(String dateOfBirthStr) {
        DateOfBirthStr = dateOfBirthStr;
    }

    ArrayList<AddressModel> addresses = new ArrayList<>();
    ArrayList<Email> emails = new ArrayList<>();
    ArrayList<Phones> phones = new ArrayList<>();
    ArrayList<ContactFiles> contactFiles = new ArrayList<>();
    ArrayList<ContactAttributes> contactAttributes = new ArrayList<>();
    ArrayList<ContactTerms> contactTermses = new ArrayList<>();
    ArrayList<Groups> groupses = new ArrayList<>();
    ArrayList<ContactRelations> contactRelationses = new ArrayList<>();
    ArrayList<ContactNotes> contactNotes = new ArrayList<>();
    ArrayList<ContactSocialDetails> contactSocialDetails = new ArrayList<>();

    public ArrayList<ContactSocialDetails> getContactSocialDetails() {
        return contactSocialDetails;
    }

    public void setContactSocialDetails(ArrayList<ContactSocialDetails> contactSocialDetails) {
        this.contactSocialDetails = contactSocialDetails;
    }

    public ArrayList<ContactNotes> getContactNotes() {
        return contactNotes;
    }

    public void setContactNotes(ArrayList<ContactNotes> contactNotes) {
        this.contactNotes = contactNotes;
    }

    public ArrayList<ContactRelations> getContactRelationses() {
        return contactRelationses;
    }

    public void setContactRelationses(ArrayList<ContactRelations> contactRelationses) {
        this.contactRelationses = contactRelationses;
    }

    public ArrayList<Groups> getGroupses() {
        return groupses;
    }

    public void setGroupses(ArrayList<Groups> groupses) {
        this.groupses = groupses;
    }

    public ArrayList<ContactTerms> getContactTermses() {
        return contactTermses;
    }

    public void setContactTermses(ArrayList<ContactTerms> contactTermses) {
        this.contactTermses = contactTermses;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public int getStateId() {
        return StateId;
    }

    public void setStateId(int stateId) {
        StateId = stateId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCreatedById() {
        return CreatedById;
    }

    public void setCreatedById(String createdById) {
        CreatedById = createdById;
    }

    public String getUpdatedById() {
        return UpdatedById;
    }

    public void setUpdatedById(String updatedById) {
        UpdatedById = updatedById;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getUpdatedOn() {
        return UpdatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        UpdatedOn = updatedOn;
    }

    public ArrayList<AddressModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<AddressModel> addresses) {
        this.addresses = addresses;
    }

    public ArrayList<Phones> getPhones() {
        return phones;
    }

    public void setPhones(ArrayList<Phones> phones) {
        this.phones = phones;
    }

    public ArrayList<ContactFiles> getContactFiles() {
        return contactFiles;
    }

    public void setContactFiles(ArrayList<ContactFiles> contactFiles) {
        this.contactFiles = contactFiles;
    }

    public ArrayList<ContactAttributes> getContactAttributes() {
        return contactAttributes;
    }

    public void setContactAttributes(ArrayList<ContactAttributes> contactAttributes) {
        this.contactAttributes = contactAttributes;
    }

    public void setNotesModelArrayList(ArrayList<NotesModel> notesModelArrayList) {
        this.notesModelArrayList = notesModelArrayList;
    }

    public ArrayList<NotesModel> getNotesModelArrayList() {
        return notesModelArrayList;
    }

    public class State implements Serializable {

        String id;
        String CountryId;
        String Name;
        String Description;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCountryId() {
            return CountryId;
        }

        public void setCountryId(String countryId) {
            CountryId = countryId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }
    }


    public class Country implements Serializable {

        String Id;
        String Name;
        String Description;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }
    }

    public class Company implements Serializable {

        String Id;
        String Name;
        String Description;


        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }
    }

    public class Addresses implements Serializable {

        String AddressType;
        String CountryId;
        String StateName;
        String DistrictName;
        String TehsilName;
        String TownName;
        String VillageName;
        String TownshipName;
        String DeveloperName;
        String ProjectName;
        String LocalityName;
        String ZipCode;;
        String SubAreaName;
        String Tower;
        String Floor;
        String Id;
        String FirstName;
        String LastName;
        String Email;
        String Company;
        String CountryName;
        String CityName;
        String Address1;
        String Address2;
        String Address3;
        String UnitName;
        String GooglePlusCode;
        String UnitNo;
        String ZipPostalCode;
        String PhoneNumber;
        String FaxNumber;
        String ContactId;
        String CreatedOn;
        String UpdatedOn;
        String Landmark;


        public String getAddressType() {
            return AddressType;
        }

        public void setAddressType(String addressType) {
            AddressType = addressType;
        }

        public String getCountryId() {
            return CountryId;
        }

        public void setCountryId(String countryId) {
            CountryId = countryId;
        }

        public String getDistrictName() {
            return DistrictName;
        }

        public void setDistrictName(String districtName) {
            DistrictName = districtName;
        }

        public String getTehsilName() {
            return TehsilName;
        }

        public void setTehsilName(String tehsilName) {
            TehsilName = tehsilName;
        }

        public String getTownName() {
            return TownName;
        }

        public void setTownName(String townName) {
            TownName = townName;
        }

        public String getVillageName() {
            return VillageName;
        }

        public void setVillageName(String villageName) {
            VillageName = villageName;
        }

        public String getTownshipName() {
            return TownshipName;
        }

        public void setTownshipName(String townshipName) {
            TownshipName = townshipName;
        }

        public String getProjectName() {
            return ProjectName;
        }

        public void setProjectName(String projectName) {
            ProjectName = projectName;
        }

        public String getLocalityName() {
            return LocalityName;
        }

        public void setLocalityName(String localityName) {
            LocalityName = localityName;
        }

        public String getZipCode() {
            return ZipCode;
        }

        public void setZipCode(String zipCode) {
            ZipCode = zipCode;
        }

        public String getSubAreaName() {
            return SubAreaName;
        }

        public void setSubAreaName(String subAreaName) {
            SubAreaName = subAreaName;
        }

        public String getUnitName() {
            return UnitName;
        }

        public void setUnitName(String unitName) {
            UnitName = unitName;
        }

        public String getGooglePlusCode() {
            return GooglePlusCode;
        }

        public void setGooglePlusCode(String googlePlusCode) {
            GooglePlusCode = googlePlusCode;
        }

        public String getLandmark() {
            return Landmark;
        }

        public void setLandmark(String landmark) {
            Landmark = landmark;
        }

        public String getAddress3() {
            return Address3;
        }

        public void setAddress3(String address3) {
            Address3 = address3;
        }

        public String getDeveloperName() {
            return DeveloperName;
        }

        public void setDeveloperName(String developerName) {
            DeveloperName = developerName;
        }

        public String getTower() {
            return Tower;
        }

        public void setTower(String tower) {
            Tower = tower;
        }

        public String getFloor() {
            return Floor;
        }

        public void setFloor(String floor) {
            Floor = floor;
        }

        public String getUnitNo() {
            return UnitNo;
        }

        public void setUnitNo(String unitNo) {
            UnitNo = unitNo;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getCompany() {
            return Company;
        }

        public void setCompany(String company) {
            Company = company;
        }

        public String getCountryName() {
            return CountryName;
        }

        public void setCountryName(String CountryName) {
            this.CountryName = CountryName;
        }

        public String getStateName() {
            return StateName;
        }

        public void setStateName(String StateName) {
            this.StateName = StateName;
        }

        public String getCityName() {
            return CityName;
        }

        public void setCityName(String cityName) {
            CityName = cityName;
        }

        public String getAddress1() {
            return Address1;
        }

        public void setAddress1(String address1) {
            Address1 = address1;
        }

        public String getAddress2() {
            return Address2;
        }

        public void setAddress2(String address2) {
            Address2 = address2;
        }

        public String getZipPostalCode() {
            return ZipPostalCode;
        }

        public void setZipPostalCode(String zipPostalCode) {
            ZipPostalCode = zipPostalCode;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            PhoneNumber = phoneNumber;
        }

        public String getFaxNumber() {
            return FaxNumber;
        }

        public void setFaxNumber(String faxNumber) {
            FaxNumber = faxNumber;
        }

        public String getContactId() {
            return ContactId;
        }

        public void setContactId(String contactId) {
            ContactId = contactId;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String createdOn) {
            CreatedOn = createdOn;
        }

        public String getUpdatedOn() {
            return UpdatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            UpdatedOn = updatedOn;
        }
    }


    public class Phones implements Serializable {

        String Id;
        String ContactId;
        String PhoneNumber;
        String PhoneCode;
        String PhoneType;
        String IsPrimary;


        public String getPhoneCode() {
            return PhoneCode;
        }

        public void setPhoneCode(String phoneCode) {
            PhoneCode = phoneCode;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getContactId() {
            return ContactId;
        }

        public void setContactId(String contactId) {
            ContactId = contactId;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            PhoneNumber = phoneNumber;
        }

        public String getPhoneType() {
            return PhoneType;
        }

        public void setPhoneType(String phoneType) {
            PhoneType = phoneType;
        }

        public String getIsPrimary() {
            return IsPrimary;
        }

        public void setIsPrimary(String isPrimary) {
            IsPrimary = isPrimary;
        }
    }

    public class Email implements Serializable {

        String Id;
        String ContactId;
        String Address;
        String Label;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getContactId() {
            return ContactId;
        }

        public void setContactId(String contactId) {
            ContactId = contactId;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getLabel() {
            return Label;
        }

        public void setLabel(String label) {
            Label = label;
        }
    }

    public class ContactFiles implements Serializable {

        String Id;
        String ContactId;
        String Name;
        String Type;
        String Path;
        String CompanyId;
        String IsSecure;


        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getContactId() {
            return ContactId;
        }

        public void setContactId(String contactId) {
            ContactId = contactId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getPath() {
            return Path;
        }

        public void setPath(String path) {
            Path = path;
        }

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String companyId) {
            CompanyId = companyId;
        }

        public String getIsSecure() {
            return IsSecure;
        }

        public void setIsSecure(String isSecure) {
            IsSecure = isSecure;
        }
    }

    public class ContactNotes implements Serializable {

        String Note;
        String CreatedDate;

        public String getNote() {
            return Note;
        }

        public void setNote(String note) {
            Note = note;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String createdDate) {
            CreatedDate = createdDate;
        }
    }


    public class ContactRelations implements Serializable {

        String Relation;
        String RelationFirstName;
        String RelationLastName;
        int ContactId;
        int Id;
        boolean IsModify;

        public boolean isModify() {
            return IsModify;
        }

        public void setModify(boolean modify) {
            IsModify = modify;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getRelation() {
            return Relation;
        }

        public void setRelation(String relation) {
            Relation = relation;
        }

        public String getRelationFirstName() {
            return RelationFirstName;
        }

        public void setRelationFirstName(String relationFirstName) {
            RelationFirstName = relationFirstName;
        }

        public String getRelationLastName() {
            return RelationLastName;
        }

        public void setRelationLastName(String relationLastName) {
            RelationLastName = relationLastName;
        }

        public int getContactId() {
            return ContactId;
        }

        public void setContactId(int contactId) {
            ContactId = contactId;
        }
    }

    public class Groups implements Serializable {

        String Id;
        String GroupName;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getGroupName() {
            return GroupName;
        }

        public void setGroupName(String groupName) {
            GroupName = groupName;
        }
    }

    public class ContactTerms implements Serializable {

        String Id;
        String TermId;
        Term term;

        public Term getTerm() {
            return term;
        }

        public void setTerm(Term term) {
            this.term = term;
        }

        public class Term implements Serializable {

            String Name;
            String Description;
            String TaxonomyId;
            String CompanyId;

            public String getName() {
                return Name;
            }

            public void setName(String name) {
                Name = name;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String description) {
                Description = description;
            }

            public String getTaxonomyId() {
                return TaxonomyId;
            }

            public void setTaxonomyId(String taxonomyId) {
                TaxonomyId = taxonomyId;
            }

            public String getCompanyId() {
                return CompanyId;
            }

            public void setCompanyId(String companyId) {
                CompanyId = companyId;
            }
        }


        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getTermId() {
            return TermId;
        }

        public void setTermId(String termId) {
            TermId = termId;
        }


    }

    public class ContactAttributes implements Serializable {

        String Id;
        String EntityId;
        String KeyGroup;
        String Key;
        String Value;
        String CompanyId;


        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getEntityId() {
            return EntityId;
        }

        public void setEntityId(String entityId) {
            EntityId = entityId;
        }

        public String getKeyGroup() {
            return KeyGroup;
        }

        public void setKeyGroup(String keyGroup) {
            KeyGroup = keyGroup;
        }

        public String getKey() {
            return Key;
        }

        public void setKey(String key) {
            Key = key;
        }

        public String getValue() {
            return Value;
        }

        public void setValue(String value) {
            Value = value;
        }

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String companyId) {
            CompanyId = companyId;
        }
    }

    public ArrayList<ContactsModel.Email> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<ContactsModel.Email> emails) {
        this.emails = emails;
    }
}
