package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by gourav on 2/1/2018.
 */

public class PropertyUnitsModel implements Serializable{

    String Id;
    String SizeUnitId;
    private int priceUnitId;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSizeUnitId() {
        return SizeUnitId;
    }

    public void setSizeUnitId(String sizeUnitId) {
        SizeUnitId = sizeUnitId;
    }

    public int getPriceUnitId() {
        return priceUnitId;
    }
}
