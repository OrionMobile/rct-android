package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by developer15 on 4/13/2016.
 */
public class GalleryModel implements Serializable {

    String title;
    String name;
    String imagePath;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
