package com.bnhabitat.models;

import java.util.ArrayList;

public class PropAreaModel {

    int Id;
    String Cam;
    String PlotShape;
    int PlotArea;
    int PlotAreaUnitId;
    String FrontSize;
    int FrontSizeUnitId;
    String DepthSize;
    String DepthSizeUnitId;
    String StreetOrRoadType;
    int RoadWidth;
    String RoadWidthUnitId;;
    String EnteranceDoorFacing;
    boolean IsDeletable;
    boolean IsModify;
    int PropertyId;
    String Name;
    String Description;
    String Category;
    String Capacity;
    String PropertyBedrooms;

    int BankId;
    int LoanAmount;
    int LoanTenure;
    boolean FullDisbursed;
    String LastInstallmentPaidOn;
    int TotalOutstandingAfterLastInstallment;

    String SuperArea;
    int SuperAreaUnitId;
    String BuiltUpArea;
    int BuiltUpAreaUnitId;
    String CarpetArea;
    int CarpetAreaUnitId;

    public int getBuiltUpAreaUnitId() {
        return BuiltUpAreaUnitId;
    }

    public void setBuiltUpAreaUnitId(int builtUpAreaUnitId) {
        BuiltUpAreaUnitId = builtUpAreaUnitId;
    }

    public String getCarpetArea() {
        return CarpetArea;
    }

    public void setCarpetArea(String carpetArea) {
        CarpetArea = carpetArea;
    }

    public int getCarpetAreaUnitId() {
        return CarpetAreaUnitId;
    }

    public void setCarpetAreaUnitId(int carpetAreaUnitId) {
        CarpetAreaUnitId = carpetAreaUnitId;
    }

    public String getSuperArea() {
        return SuperArea;
    }

    public void setSuperArea(String superArea) {
        SuperArea = superArea;
    }

    public int getSuperAreaUnitId() {
        return SuperAreaUnitId;
    }

    public void setSuperAreaUnitId(int superAreaUnitId) {
        SuperAreaUnitId = superAreaUnitId;
    }

    public String getBuiltUpArea() {
        return BuiltUpArea;
    }

    public void setBuiltUpArea(String builtUpArea) {
        BuiltUpArea = builtUpArea;
    }

    String Title;;
    int Value;

    String LastPaidDate;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getCam() {
        return Cam;
    }

    public void setCam(String cam) {
        Cam = cam;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }

    public String getLastPaidDate() {
        return LastPaidDate;
    }

    public void setLastPaidDate(String lastPaidDate) {
        LastPaidDate = lastPaidDate;
    }

    public int getBankId() {
        return BankId;
    }

    public void setBankId(int bankId) {
        BankId = bankId;
    }

    public int getLoanAmount() {
        return LoanAmount;
    }

    public void setLoanAmount(int loanAmount) {
        LoanAmount = loanAmount;
    }

    public int getLoanTenure() {
        return LoanTenure;
    }

    public void setLoanTenure(int loanTenure) {
        LoanTenure = loanTenure;
    }

    public boolean isFullDisbursed() {
        return FullDisbursed;
    }

    public void setFullDisbursed(boolean fullDisbursed) {
        FullDisbursed = fullDisbursed;
    }

    public String getLastInstallmentPaidOn() {
        return LastInstallmentPaidOn;
    }

    public void setLastInstallmentPaidOn(String lastInstallmentPaidOn) {
        LastInstallmentPaidOn = lastInstallmentPaidOn;
    }

    public int getTotalOutstandingAfterLastInstallment() {
        return TotalOutstandingAfterLastInstallment;
    }

    public void setTotalOutstandingAfterLastInstallment(int totalOutstandingAfterLastInstallment) {
        TotalOutstandingAfterLastInstallment = totalOutstandingAfterLastInstallment;
    }

    public int getDemandPrice() {
        return DemandPrice;
    }

    public void setDemandPrice(int demandPrice) {
        DemandPrice = demandPrice;
    }

    public boolean isInclusivePrice() {
        return InclusivePrice;
    }

    public void setInclusivePrice(boolean inclusivePrice) {
        InclusivePrice = inclusivePrice;
    }

    public boolean isNegociable() {
        return IsNegociable;
    }

    public void setNegociable(boolean negociable) {
        IsNegociable = negociable;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getPriceUnitId() {
        return PriceUnitId;
    }

    public void setPriceUnitId(int priceUnitId) {
        PriceUnitId = priceUnitId;
    }

    String PropertySpecificationRoomMapping;
    int DemandPrice;
    boolean InclusivePrice;
    boolean IsNegociable;
    int Price;
    int PriceUnitId;


    public int getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(int propertyId) {
        PropertyId = propertyId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        Capacity = capacity;
    }

    public String getPropertyBedrooms() {
        return PropertyBedrooms;
    }

    public void setPropertyBedrooms(String propertyBedrooms) {
        PropertyBedrooms = propertyBedrooms;
    }

    public String getPropertySpecificationRoomMapping() {
        return PropertySpecificationRoomMapping;
    }

    public void setPropertySpecificationRoomMapping(String propertySpecificationRoomMapping) {
        PropertySpecificationRoomMapping = propertySpecificationRoomMapping;
    }

    ArrayList<PicsModel> picsModelArrayList = new ArrayList<>();
    ArrayList<ImageModel> imgArrayList = new ArrayList<>();
    ArrayList<AccomDetailModel> accomDetailModelsList = new ArrayList<>();
    ArrayList<PropAreaModel> propSpecific = new ArrayList<>();
    ArrayList<PropAreaModel> propFinance = new ArrayList<>();
    ArrayList<PropAreaModel> propLoans = new ArrayList<>();
    ArrayList<PropAreaModel> propPtherExpenses = new ArrayList<>();
    ArrayList<PropAreaModel> propOwnership = new ArrayList<>();

    public ArrayList<PropAreaModel> getPropOwnership() {
        return propOwnership;
    }

    public void setPropOwnership(ArrayList<PropAreaModel> propOwnership) {
        this.propOwnership = propOwnership;
    }

    public ArrayList<PropAreaModel> getPropPtherExpenses() {
        return propPtherExpenses;
    }

    public void setPropPtherExpenses(ArrayList<PropAreaModel> propPtherExpenses) {
        this.propPtherExpenses = propPtherExpenses;
    }

    public ArrayList<PropAreaModel> getPropFinance() {
        return propFinance;
    }

    public void setPropFinance(ArrayList<PropAreaModel> propFinance) {
        this.propFinance = propFinance;
    }

    public ArrayList<PropAreaModel> getPropLoans() {
        return propLoans;
    }

    public void setPropLoans(ArrayList<PropAreaModel> propLoans) {
        this.propLoans = propLoans;
    }

    public ArrayList<PropAreaModel> getPropSpecific() {
        return propSpecific;
    }

    public void setPropSpecific(ArrayList<PropAreaModel> propSpecific) {
        this.propSpecific = propSpecific;
    }

    ArrayList<PropBedroomModel> propBedList = new ArrayList<>();
    ArrayList<PropAreaModel> PropAreas = new ArrayList<>();

    public ArrayList<PropAreaModel> getPropAreas() {
        return PropAreas;
    }

    public void setPropAreas(ArrayList<PropAreaModel> propAreas) {
        PropAreas = propAreas;
    }

    public ArrayList<PropBedroomModel> getPropBedList() {
        return propBedList;
    }


    public void setPropBedList(ArrayList<PropBedroomModel> propBedList) {
        this.propBedList = propBedList;
    }

    public ArrayList<AccomDetailModel> getAccomDetailModelsList() {
        return accomDetailModelsList;
    }


    public void setAccomDetailModelsList(ArrayList<AccomDetailModel> accomDetailModelsList) {
        this.accomDetailModelsList = accomDetailModelsList;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public ArrayList<PicsModel> getPicsModelArrayList() {
        return picsModelArrayList;
    }

    public void setPicsModelArrayList(ArrayList<PicsModel> picsModelArrayList) {
        this.picsModelArrayList = picsModelArrayList;
    }

    public Boolean getDeletable() {
        return IsDeletable;
    }

    public void setDeletable(Boolean deletable) {
        IsDeletable = deletable;
    }

    public Boolean getModify() {
        return IsModify;
    }

    public void setModify(Boolean modify) {
        IsModify = modify;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getPlotShape() {
        return PlotShape;
    }

    public void setPlotShape(String plotShape) {
        PlotShape = plotShape;
    }

    public int getPlotArea() {
        return PlotArea;
    }

    public void setPlotArea(int plotArea) {
        PlotArea = plotArea;
    }

    public int getPlotAreaUnitId() {
        return PlotAreaUnitId;
    }

    public void setPlotAreaUnitId(int plotAreaUnitId) {
        PlotAreaUnitId = plotAreaUnitId;
    }

    public String getFrontSize() {
        return FrontSize;
    }

    public void setFrontSize(String frontSize) {
        FrontSize = frontSize;
    }

    public int getFrontSizeUnitId() {
        return FrontSizeUnitId;
    }

    public void setFrontSizeUnitId(int frontSizeUnitId) {
        FrontSizeUnitId = frontSizeUnitId;
    }

    public String getDepthSize() {
        return DepthSize;
    }

    public void setDepthSize(String depthSize) {
        DepthSize = depthSize;
    }

    public String getDepthSizeUnitId() {
        return DepthSizeUnitId;
    }

    public void setDepthSizeUnitId(String depthSizeUnitId) {
        DepthSizeUnitId = depthSizeUnitId;
    }

    public String getStreetOrRoadType() {
        return StreetOrRoadType;
    }

    public void setStreetOrRoadType(String streetOrRoadType) {
        StreetOrRoadType = streetOrRoadType;
    }

    public int getRoadWidth() {
        return RoadWidth;
    }

    public void setRoadWidth(int roadWidth) {
        RoadWidth = roadWidth;
    }

    public String getRoadWidthUnitId() {
        return RoadWidthUnitId;
    }

    public void setRoadWidthUnitId(String roadWidthUnitId) {
        RoadWidthUnitId = roadWidthUnitId;
    }

    public String getEnteranceDoorFacing() {
        return EnteranceDoorFacing;
    }

    public void setEnteranceDoorFacing(String enteranceDoorFacing) {
        EnteranceDoorFacing = enteranceDoorFacing;
    }

    public boolean isDeletable(boolean isDeletable) {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify(boolean isModify) {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }

    public ArrayList<ImageModel> getImgArrayList() {
        return imgArrayList;
    }

    public void setImgArrayList(ArrayList<ImageModel> imgArrayList) {
        this.imgArrayList = imgArrayList;
    }
}
