package com.bnhabitat.models;

public class PicsModel {

    int Id;
    String Name;
    boolean IsDeletable;
    boolean IsModify;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean isDeletable(boolean isDeletable) {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify(boolean isModify) {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }
}
