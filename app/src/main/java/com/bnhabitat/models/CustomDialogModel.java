package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by Android on 5/16/2017.
 */
public class CustomDialogModel implements Serializable {
    String name;
    String details;
    String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
