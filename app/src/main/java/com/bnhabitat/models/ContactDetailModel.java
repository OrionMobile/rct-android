package com.bnhabitat.models;

/**
 * Created by gourav on 12/15/2017.
 */

public class ContactDetailModel {
    Results Result;

    public Results getResult() {
        return Result;
    }

    public void setResult(Results result) {
        Result = result;
    }

    public class Results{
        String Id;
        String FirstName;
        String LastName;
        String Prefix;
        String MiddleName;
        String Sufix;
        String MaritalStatus;
        String MarriageAnniversaryDate;
        String MarriageAnniversaryDateString;
        String SourceName;
        String Campaign;
        String SourceDate;
        String OwnerShip;
        String OwnerUserId;
        String IndustryName;
        String Designation;
        String Profession;
        String Religion;
        String AnnualIncome;
        String CompanyId;
        String CompanyName;
        String DateOfBirthStr;
        String Gender;

        public String getReligion() {
            return Religion;
        }

        public void setReligion(String religion) {
            Religion = religion;
        }

        public String getProfession() {
            return Profession;
        }

        public void setProfession(String profession) {
            Profession = profession;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getPrefix() {
            return Prefix;
        }

        public void setPrefix(String prefix) {
            Prefix = prefix;
        }

        public String getMiddleName() {
            return MiddleName;
        }

        public void setMiddleName(String middleName) {
            MiddleName = middleName;
        }

        public String getSufix() {
            return Sufix;
        }

        public void setSufix(String sufix) {
            Sufix = sufix;
        }

        public String getMaritalStatus() {
            return MaritalStatus;
        }

        public void setMaritalStatus(String maritalStatus) {
            MaritalStatus = maritalStatus;
        }

        public String getMarriageAnniversaryDate() {
            return MarriageAnniversaryDate;
        }

        public void setMarriageAnniversaryDate(String marriageAnniversaryDate) {
            MarriageAnniversaryDate = marriageAnniversaryDate;
        }

        public String getMarriageAnniversaryDateString() {
            return MarriageAnniversaryDateString;
        }

        public void setMarriageAnniversaryDateString(String marriageAnniversaryDateString) {
            MarriageAnniversaryDateString = marriageAnniversaryDateString;
        }

        public String getSourceName() {
            return SourceName;
        }

        public void setSourceName(String sourceName) {
            SourceName = sourceName;
        }

        public String getCampaign() {
            return Campaign;
        }

        public void setCampaign(String campaign) {
            Campaign = campaign;
        }

        public String getSourceDate() {
            return SourceDate;
        }

        public void setSourceDate(String sourceDate) {
            SourceDate = sourceDate;
        }

        public String getOwnerShip() {
            return OwnerShip;
        }

        public void setOwnerShip(String ownerShip) {
            OwnerShip = ownerShip;
        }

        public String getOwnerUserId() {
            return OwnerUserId;
        }

        public void setOwnerUserId(String ownerUserId) {
            OwnerUserId = ownerUserId;
        }

        public String getIndustryName() {
            return IndustryName;
        }

        public void setIndustryName(String industryName) {
            IndustryName = industryName;
        }

        public String getDesignation() {
            return Designation;
        }

        public void setDesignation(String designation) {
            Designation = designation;
        }

        public String getAnnualIncome() {
            return AnnualIncome;
        }

        public void setAnnualIncome(String annualIncome) {
            AnnualIncome = annualIncome;
        }

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String companyId) {
            CompanyId = companyId;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String companyName) {
            CompanyName = companyName;
        }

        public String getDateOfBirthStr() {
            return DateOfBirthStr;
        }

        public void setDateOfBirthStr(String dateOfBirthStr) {
            DateOfBirthStr = dateOfBirthStr;
        }

        public String getGender() {
            return Gender;
        }

        public void setGender(String gender) {
            Gender = gender;
        }
    }
}
