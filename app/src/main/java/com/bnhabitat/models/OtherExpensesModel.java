package com.bnhabitat.models;

public class OtherExpensesModel {

    String Id;
    String Title;
    String  Value;
    String LastPaidDate;
    boolean IsDeletable;
    boolean IsModify;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getLastPaidDate() {
        return LastPaidDate;
    }

    public void setLastPaidDate(String lastPaidDate) {
        LastPaidDate = lastPaidDate;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }
}
