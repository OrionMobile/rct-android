package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by developer15 on 9/7/2016.
 */
public class ComponentModel implements Serializable {

    double price;
    String type;
    String unitName;


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
