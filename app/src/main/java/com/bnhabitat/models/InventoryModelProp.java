package com.bnhabitat.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryModelProp {

    private Integer id;
    private Integer propertyId;
    private Object developer;
    private Object developerId;
    private Object project;
    private Object projectId;
    private String zipCode;
    private Integer stateId;
    private Integer districtId;
    private Integer TownId;
    private String TownName;
    private Integer LocalityId;
    private String Sector;
    private Object tehsilId;
    private Integer subDivisionId;
    private Object subAreaId;
    private String state;
    private String district;
    private String subDivision;
    private Object subArea;
    private Object unitNo;
    private String towerName;
    private String floorNo;
    private String googlePlusCode;
    private Object areaNegativeMark;
    private boolean isDeletable;
    private boolean isModify;

    public boolean isDeletable(boolean isDeletable) {
        return this.isDeletable;
    }

    public void setDeletable(boolean deletable) {
        isDeletable = deletable;
    }

    public boolean isModify(boolean isModify) {
        return this.isModify;
    }

    public void setModify(boolean modify) {
        isModify = modify;
    }

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Object getDeveloper() {
        return developer;
    }

    public void setDeveloper(Object developer) {
        this.developer = developer;
    }

    public Object getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(Object developerId) {
        this.developerId = developerId;
    }

    public Object getProject() {
        return project;
    }

    public void setProject(Object project) {
        this.project = project;
    }

    public Object getProjectId() {
        return projectId;
    }

    public void setProjectId(Object projectId) {
        this.projectId = projectId;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getTownId() {
        return TownId;
    }

    public void setTownId(Integer townId) {
        TownId = townId;
    }

    public String getTownName() {
        return TownName;
    }

    public void setTownName(String townName) {
        TownName = townName;
    }

    public Integer getLocalityId() {
        return LocalityId;
    }

    public void setLocalityId(Integer localityId) {
        LocalityId = localityId;
    }

    public String getSector() {
        return Sector;
    }

    public void setSector(String sector) {
        Sector = sector;
    }

    public Object getTehsilId() {
        return tehsilId;
    }

    public void setTehsilId(Object tehsilId) {
        this.tehsilId = tehsilId;
    }

    public Integer getSubDivisionId() {
        return subDivisionId;
    }

    public void setSubDivisionId(Integer subDivisionId) {
        this.subDivisionId = subDivisionId;
    }

    public Object getSubAreaId() {
        return subAreaId;
    }

    public void setSubAreaId(Object subAreaId) {
        this.subAreaId = subAreaId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getSubDivision() {
        return subDivision;
    }

    public void setSubDivision(String subDivision) {
        this.subDivision = subDivision;
    }

    public Object getSubArea() {
        return subArea;
    }

    public void setSubArea(Object subArea) {
        this.subArea = subArea;
    }

    public Object getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(Object unitNo) {
        this.unitNo = unitNo;
    }

    public String getTowerName() {
        return towerName;
    }

    public void setTowerName(String towerName) {
        this.towerName = towerName;
    }

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public String getGooglePlusCode() {
        return googlePlusCode;
    }

    public void setGooglePlusCode(String googlePlusCode) {
        this.googlePlusCode = googlePlusCode;
    }

    public Object getAreaNegativeMark() {
        return areaNegativeMark;
    }

    public void setAreaNegativeMark(Object areaNegativeMark) {
        this.areaNegativeMark = areaNegativeMark;
    }

    public Boolean getIsDeletable() {
        return isDeletable;
    }

    public void setIsDeletable(Boolean isDeletable) {
        this.isDeletable = isDeletable;
    }

    public Boolean getIsModify() {
        return isModify;
    }

    public void setIsModify(Boolean isModify) {
        this.isModify = isModify;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


}
