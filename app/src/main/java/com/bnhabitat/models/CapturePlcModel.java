package com.bnhabitat.models;

public class CapturePlcModel {

    String Id;
    String Name;
    boolean IsDeletable;
    boolean IsModify;
    boolean IsChecked ;

    public boolean isChecked() {
        return IsChecked;
    }

    public void setChecked(boolean checked) {
        IsChecked = checked;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }
}
