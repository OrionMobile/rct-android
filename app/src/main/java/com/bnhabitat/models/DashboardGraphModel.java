package com.bnhabitat.models;

import java.util.ArrayList;

public class DashboardGraphModel {

    String TotalContacts,ContactsLast7days,ContactsLast30days,TotalUsers;
    AreaLineData areaLineData;
    SimpleBarData simpleBarData;
    SimplePieData simplePieData;


    public String getTotalContacts() {
        return TotalContacts;
    }

    public void setTotalContacts(String totalContacts) {
        TotalContacts = totalContacts;
    }

    public String getContactsLast7days() {
        return ContactsLast7days;
    }

    public void setContactsLast7days(String contactsLast7days) {
        ContactsLast7days = contactsLast7days;
    }

    public String getContactsLast30days() {
        return ContactsLast30days;
    }

    public void setContactsLast30days(String contactsLast30days) {
        ContactsLast30days = contactsLast30days;
    }

    public String getTotalUsers() {
        return TotalUsers;
    }

    public void setTotalUsers(String totalUsers) {
        TotalUsers = totalUsers;
    }

    public AreaLineData getAreaLineData() {
        return areaLineData;
    }

    public void setAreaLineData(AreaLineData areaLineData) {
        this.areaLineData = areaLineData;
    }

    public SimpleBarData getSimpleBarData() {
        return simpleBarData;
    }

    public void setSimpleBarData(SimpleBarData simpleBarData) {
        this.simpleBarData = simpleBarData;
    }

    public SimplePieData getSimplePieData() {
        return simplePieData;
    }

    public void setSimplePieData(SimplePieData simplePieData) {
        this.simplePieData = simplePieData;
    }

    public class AreaLineData {

        ArrayList<String> dayses=new ArrayList<>();

        ArrayList<String> contactses=new ArrayList<>();


        public ArrayList<String> getDayses() {
            return dayses;
        }

        public void setDayses(ArrayList<String> dayses) {
            this.dayses = dayses;
        }

        public ArrayList<String> getContactses() {
            return contactses;
        }

        public void setContactses(ArrayList<String> contactses) {
            this.contactses = contactses;
        }
    }

    public class SimpleBarData {

        ArrayList<String> scoreValues=new ArrayList<>();

        public ArrayList<String> getScoreValues() {
            return scoreValues;
        }

        public void setScoreValues(ArrayList<String> scoreValues) {
            this.scoreValues = scoreValues;
        }
    }

    public class SimplePieData {


        ArrayList<String> scoreValues=new ArrayList<>();
        ArrayList<String> sourceValues=new ArrayList<>();


        public ArrayList<String> getScoreValues() {
            return scoreValues;
        }

        public void setScoreValues(ArrayList<String> scoreValues) {
            this.scoreValues = scoreValues;
        }

        public ArrayList<String> getSourceValues() {
            return sourceValues;
        }

        public void setSourceValues(ArrayList<String> sourceValues) {
            this.sourceValues = sourceValues;
        }
    }
}
