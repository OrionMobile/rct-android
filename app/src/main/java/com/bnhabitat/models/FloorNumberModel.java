package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by gourav on 4/27/2018.
 */

public class FloorNumberModel implements Serializable {
    String floornumber;
    String floorsize;

    public String getFloornumber() {
        return floornumber;
    }

    public void setFloornumber(String floornumber) {
        this.floornumber = floornumber;
    }

    public String getFloorsize() {
        return floorsize;
    }

    public void setFloorsize(String floorsize) {
        this.floorsize = floorsize;
    }
}
