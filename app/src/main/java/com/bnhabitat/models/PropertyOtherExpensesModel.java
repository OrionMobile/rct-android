package com.bnhabitat.models;

/**
 * Created by gourav on 2/6/2018.
 */

public class PropertyOtherExpensesModel {
    String Title;
    String Value;
    String LastPaidDate;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getLastPaidDate() {
        return LastPaidDate;
    }

    public void setLastPaidDate(String lastPaidDate) {
        LastPaidDate = lastPaidDate;
    }
}
