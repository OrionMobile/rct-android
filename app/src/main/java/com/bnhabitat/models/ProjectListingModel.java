package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by Android on 5/16/2017.
 */

public class ProjectListingModel implements Serializable {

    String title;
    String addressDetail;
    String Price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }
}
