package com.bnhabitat.models;

import java.util.ArrayList;

public class LegalModel {

    int Id;
    int CompanyId;
    String FirstName;
    String LastName;
    String PhoneNumber;
    String Email;
    String PanNo;
    String AdhaarCardNo;
    String Address;
    String Occupation;
    String CompanyType;
    String CompanyName;
    ArrayList<PropertyOwnerShipProofs> propertyOwnerShipProofs = new ArrayList<>();

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(int companyId) {
        CompanyId = companyId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPanNo() {
        return PanNo;
    }

    public void setPanNo(String panNo) {
        PanNo = panNo;
    }

    public String getAdhaarCardNo() {
        return AdhaarCardNo;
    }

    public void setAdhaarCardNo(String adhaarCardNo) {
        AdhaarCardNo = adhaarCardNo;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String occupation) {
        Occupation = occupation;
    }

    public String getCompanyType() {
        return CompanyType;
    }

    public void setCompanyType(String companyType) {
        CompanyType = companyType;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public ArrayList<PropertyOwnerShipProofs> getPropertyOwnerShipProofs() {
        return propertyOwnerShipProofs;
    }

    public void setPropertyOwnerShipProofs(ArrayList<PropertyOwnerShipProofs> propertyOwnerShipProofs) {
        this.propertyOwnerShipProofs = propertyOwnerShipProofs;
    }
}
