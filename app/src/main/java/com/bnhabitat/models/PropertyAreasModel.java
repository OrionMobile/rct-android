package com.bnhabitat.models;

public class PropertyAreasModel {

    String Id;
    String PropertyId;
    String PlotShape;
    String PlotArea;
    String PlotAreaUnitId;
    String FrontSize;
    String FrontSizeUnitId;
    String DepthSize;
    String DepthSizeUnitId;
    String StreetOrRoadType;
    String RoadWidth;
    String RoadWidthUnitId;;
    String EnteranceDoorFacing;
    boolean ParkingInFront;
    boolean HaveWalkingPath ;
    String PlotOrCoverArea ;
    String PlotOrCoverAreaUnitId  ;
    String SuperArea;
    String SuperAreaUnitId;
    String BuiltUpArea;
    String BuiltUpAreaUnitId;
    String CarpetArea;
    String CarpetAreaUnitId;
    boolean IsDeletable;
    boolean IsModify;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(String propertyId) {
        PropertyId = propertyId;
    }

    public String getPlotShape() {
        return PlotShape;
    }

    public void setPlotShape(String plotShape) {
        PlotShape = plotShape;
    }

    public String getPlotArea() {
        return PlotArea;
    }

    public void setPlotArea(String plotArea) {
        PlotArea = plotArea;
    }

    public String getPlotAreaUnitId() {
        return PlotAreaUnitId;
    }

    public void setPlotAreaUnitId(String plotAreaUnitId) {
        PlotAreaUnitId = plotAreaUnitId;
    }

    public String getFrontSize() {
        return FrontSize;
    }

    public void setFrontSize(String frontSize) {
        FrontSize = frontSize;
    }

    public String getFrontSizeUnitId() {
        return FrontSizeUnitId;
    }

    public void setFrontSizeUnitId(String frontSizeUnitId) {
        FrontSizeUnitId = frontSizeUnitId;
    }

    public String getDepthSize() {
        return DepthSize;
    }

    public void setDepthSize(String depthSize) {
        DepthSize = depthSize;
    }

    public String getDepthSizeUnitId() {
        return DepthSizeUnitId;
    }

    public void setDepthSizeUnitId(String depthSizeUnitId) {
        DepthSizeUnitId = depthSizeUnitId;
    }

    public String getStreetOrRoadType() {
        return StreetOrRoadType;
    }

    public void setStreetOrRoadType(String streetOrRoadType) {
        StreetOrRoadType = streetOrRoadType;
    }

    public String getRoadWidth() {
        return RoadWidth;
    }

    public void setRoadWidth(String roadWidth) {
        RoadWidth = roadWidth;
    }

    public String getRoadWidthUnitId() {
        return RoadWidthUnitId;
    }

    public void setRoadWidthUnitId(String roadWidthUnitId) {
        RoadWidthUnitId = roadWidthUnitId;
    }

    public String getEnteranceDoorFacing() {
        return EnteranceDoorFacing;
    }

    public void setEnteranceDoorFacing(String enteranceDoorFacing) {
        EnteranceDoorFacing = enteranceDoorFacing;
    }

    public boolean isParkingInFront() {
        return ParkingInFront;
    }

    public void setParkingInFront(boolean parkingInFront) {
        ParkingInFront = parkingInFront;
    }

    public boolean isHaveWalkingPath() {
        return HaveWalkingPath;
    }

    public void setHaveWalkingPath(boolean haveWalkingPath) {
        HaveWalkingPath = haveWalkingPath;
    }

    public String getPlotOrCoverArea() {
        return PlotOrCoverArea;
    }

    public void setPlotOrCoverArea(String plotOrCoverArea) {
        PlotOrCoverArea = plotOrCoverArea;
    }

    public String getPlotOrCoverAreaUnitId() {
        return PlotOrCoverAreaUnitId;
    }

    public void setPlotOrCoverAreaUnitId(String plotOrCoverAreaUnitId) {
        PlotOrCoverAreaUnitId = plotOrCoverAreaUnitId;
    }

    public String getSuperArea() {
        return SuperArea;
    }

    public void setSuperArea(String superArea) {
        SuperArea = superArea;
    }

    public String getSuperAreaUnitId() {
        return SuperAreaUnitId;
    }

    public void setSuperAreaUnitId(String superAreaUnitId) {
        SuperAreaUnitId = superAreaUnitId;
    }

    public String getBuiltUpArea() {
        return BuiltUpArea;
    }

    public void setBuiltUpArea(String builtUpArea) {
        BuiltUpArea = builtUpArea;
    }

    public String getBuiltUpAreaUnitId() {
        return BuiltUpAreaUnitId;
    }

    public void setBuiltUpAreaUnitId(String builtUpAreaUnitId) {
        BuiltUpAreaUnitId = builtUpAreaUnitId;
    }

    public String getCarpetArea() {
        return CarpetArea;
    }

    public void setCarpetArea(String carpetArea) {
        CarpetArea = carpetArea;
    }

    public String getCarpetAreaUnitId() {
        return CarpetAreaUnitId;
    }

    public void setCarpetAreaUnitId(String carpetAreaUnitId) {
        CarpetAreaUnitId = carpetAreaUnitId;
    }

    public boolean isDeletable(boolean isDeletable) {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify(boolean isModify) {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }
}
