package com.bnhabitat.models;

/**
 * Created by gourav on 12/27/2017.
 */

public class PermissionsModel {
    String Id;
    String PermissionName;
    String GroupId;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPermissionName() {
        return PermissionName;
    }

    public void setPermissionName(String permissionName) {
        PermissionName = permissionName;
    }

    public String getGroupId() {
        return GroupId;
    }

    public void setGroupId(String groupId) {
        GroupId = groupId;
    }
}
