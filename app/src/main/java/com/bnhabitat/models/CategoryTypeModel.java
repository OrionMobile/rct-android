package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by developer15 on 3/28/2016.
 */
public class CategoryTypeModel implements Serializable {

    String area1;
    String area2;
    String area3;
    String area4;


    public String getArea1() {
        return area1;
    }

    public void setArea1(String area1) {
        this.area1 = area1;
    }

    public String getArea2() {
        return area2;
    }

    public void setArea2(String area2) {
        this.area2 = area2;
    }

    public String getArea3() {
        return area3;
    }

    public void setArea3(String area3) {
        this.area3 = area3;
    }

    public String getArea4() {
        return area4;
    }

    public void setArea4(String area4) {
        this.area4 = area4;
    }
}
