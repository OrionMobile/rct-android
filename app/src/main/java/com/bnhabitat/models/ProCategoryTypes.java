package com.bnhabitat.models;

import java.io.Serializable;

public class ProCategoryTypes implements Serializable {

    int Id;
    int ProjectId;
    int CategoryTypeId;
    String CategoryType;
    String Type;
    String BasementType;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getProjectId() {
        return ProjectId;
    }

    public void setProjectId(int projectId) {
        ProjectId = projectId;
    }

    public int getCategoryTypeId() {
        return CategoryTypeId;
    }

    public void setCategoryTypeId(int categoryTypeId) {
        CategoryTypeId = categoryTypeId;
    }

    public String getCategoryType() {
        return CategoryType;
    }

    public void setCategoryType(String categoryType) {
        CategoryType = categoryType;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getBasementType() {
        return BasementType;
    }

    public void setBasementType(String basementType) {
        BasementType = basementType;
    }
}
