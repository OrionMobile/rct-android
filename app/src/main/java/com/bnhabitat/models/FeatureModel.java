package com.bnhabitat.models;

public class FeatureModel {

    String Id;
    String PropertyId;
    String Name;
    String Type;
    String TotalCount;
    boolean IsDeletable;
    boolean IsModify;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(String propertyId) {
        PropertyId = propertyId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(String totalCount) {
        TotalCount = totalCount;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }
}
