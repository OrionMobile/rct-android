package com.bnhabitat.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by developer15 on 3/13/2016.
 */
public class PaymentPlanModel implements Serializable {


    int id;
    String name;
    String disclaimer;
    int planTypeId;
    public ArrayList<PropertySizes> propertySizes = new ArrayList<>();
    public ArrayList<Milestones> milestones = new ArrayList<>();

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public class PropertySizes implements Serializable{

        int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public class Milestones implements Serializable{

        int id;
        int planTypeId;
        String milestoneNumber;
        boolean lumsum;
        boolean inclusive;
        public ArrayList<PaymentPlanMilestoneComponents> paymentPlanMilestoneComponentses = new ArrayList<>();

        public class PaymentPlanMilestoneComponents implements Serializable{


            int componentId;
            int subComponentId;
            double milestoneValue;
            int propertyPaymentPlan_MileStoneId;
            boolean discountApplicable;

            public int getComponentId() {
                return componentId;
            }

            public void setComponentId(int componentId) {
                this.componentId = componentId;
            }

            public int getSubComponentId() {
                return subComponentId;
            }

            public void setSubComponentId(int subComponentId) {
                this.subComponentId = subComponentId;
            }

            public int getPropertyPaymentPlan_MileStoneId() {
                return propertyPaymentPlan_MileStoneId;
            }

            public void setPropertyPaymentPlan_MileStoneId(int propertyPaymentPlan_MileStoneId) {
                this.propertyPaymentPlan_MileStoneId = propertyPaymentPlan_MileStoneId;
            }

            public double getMilestoneValue() {
                return milestoneValue;
            }

            public void setMilestoneValue(double milestoneValue) {
                this.milestoneValue = milestoneValue;
            }

            public boolean isDiscountApplicable() {
                return discountApplicable;
            }

            public void setDiscountApplicable(boolean discountApplicable) {
                this.discountApplicable = discountApplicable;
            }
        }



        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMilestoneNumber() {
            return milestoneNumber;
        }

        public void setMilestoneNumber(String milestoneNumber) {
            this.milestoneNumber = milestoneNumber;
        }

        public int getPlanTypeId() {
            return planTypeId;
        }

        public void setPlanTypeId(int planTypeId) {
            this.planTypeId = planTypeId;
        }

        public boolean isLumsum() {
            return lumsum;
        }

        public void setLumsum(boolean lumsum) {
            this.lumsum = lumsum;
        }

        public boolean isInclusive() {
            return inclusive;
        }

        public void setInclusive(boolean inclusive) {
            this.inclusive = inclusive;
        }

        public ArrayList<PaymentPlanMilestoneComponents> getPaymentPlanMilestoneComponentses() {
            return paymentPlanMilestoneComponentses;
        }

        public void setPaymentPlanMilestoneComponentses(ArrayList<PaymentPlanMilestoneComponents> paymentPlanMilestoneComponentses) {
            this.paymentPlanMilestoneComponentses = paymentPlanMilestoneComponentses;
        }
    }


    Discount discount = new Discount();

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public class Discount implements Serializable{

        int id;
        String discountName;
        String discountValue;
        String paymentOption;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDiscountName() {
            return discountName;
        }

        public void setDiscountName(String discountName) {
            this.discountName = discountName;
        }

        public String getDiscountValue() {
            return discountValue;
        }

        public void setDiscountValue(String discountValue) {
            this.discountValue = discountValue;
        }

        public String getPaymentOption() {
            return paymentOption;
        }

        public void setPaymentOption(String paymentOption) {
            this.paymentOption = paymentOption;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlanTypeId() {
        return planTypeId;
    }

    public void setPlanTypeId(int planTypeId) {
        this.planTypeId = planTypeId;
    }

    public ArrayList<PropertySizes> getPropertySizes() {
        return propertySizes;
    }

    public void setPropertySizes(ArrayList<PropertySizes> propertySizes) {
        this.propertySizes = propertySizes;
    }

    public ArrayList<Milestones> getMilestones() {
        return milestones;
    }

    public void setMilestones(ArrayList<Milestones> milestones) {
        this.milestones = milestones;
    }
}
