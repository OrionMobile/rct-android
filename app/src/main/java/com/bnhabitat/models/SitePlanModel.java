package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by gourav on 6/2/2017.
 */

public class SitePlanModel implements Serializable {
    String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
