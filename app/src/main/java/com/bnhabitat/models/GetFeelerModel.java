package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by Android on 5/19/2017.
 */
public class GetFeelerModel implements Serializable {

    String ProjectName;

    public String getProjectName() {
        return ProjectName;
    }

    public void setProjectName(String projectName) {
        ProjectName = projectName;
    }
}
