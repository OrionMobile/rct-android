package com.bnhabitat.models;

/**
 * Created by gourav on 1/30/2018.
 */

public class AreaEntitiyModel {
    String Id;
    String Name;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
