package com.bnhabitat.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProjectsModel implements Serializable {

    String id;
    String title;
    String imagePath;
    String logoimage;
    String locality;
    String minPrice;
    String maxPrice;
    String projectStatus;
    String region;
    String propertyTitles;
    String topImage1;
    String topImage2;
    String topImage3;
    String CompanyLogo;

    String specifications;
    String builderCompanyId;
    String builderCompanyName;
    String CityName;
    String StateName;
    String UpdatedOn;
    String Area;
    String AreaUnitId;

    String TypeOfProject;
    String projectDetailJson;
    String NumberOfTowers;
    String aboutSubProject;
    String PossessionMonth;
    String PossessionYear;
    String NumberOfParking;
    String CurrentOffers;


    public String getCurrentOffers() {
        return CurrentOffers;
    }

    public void setCurrentOffers(String currentOffers) {
        CurrentOffers = currentOffers;
    }

    public String getAreaUnitId() {
        return AreaUnitId;
    }

    public void setAreaUnitId(String areaUnitId) {
        AreaUnitId = areaUnitId;
    }

    public String getNumberOfParking() {
        return NumberOfParking;
    }

    public void setNumberOfParking(String numberOfParking) {
        NumberOfParking = numberOfParking;
    }
    public String getCompanyLogo() {
        return CompanyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        CompanyLogo = companyLogo;
    }

    public String getPossessionYear() {
        return PossessionYear;
    }

    public void setPossessionYear(String possessionYear) {
        PossessionYear = possessionYear;
    }

    public String getPossessionMonth() {
        return PossessionMonth;
    }

    public void setPossessionMonth(String possessionMonth) {
        PossessionMonth = possessionMonth;
    }

    public String getNumberOfTowers() {
        return NumberOfTowers;
    }

    public void setNumberOfTowers(String numberOfTowers) {
        NumberOfTowers = numberOfTowers;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getUpdatedOn() {
        return UpdatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        UpdatedOn = updatedOn;
    }

    ArrayList<PropertySize> propertySizes=new ArrayList<>();

    ArrayList<ProjectRelationshipManagers> projectRelationshipManagerses = new ArrayList<>();
    ArrayList<Towers> towerses = new ArrayList<>();

    public ProjectsModel(String locality,String image) {
        this.CityName = locality;
        this.topImage1 = image;
    }

    public ProjectsModel() {

    }


    public ArrayList<PropertySize> getPropertySizes() {
        return propertySizes;
    }

    public void setPropertySizes(ArrayList<PropertySize> propertySizes) {
        this.propertySizes = propertySizes;
    }

    public ArrayList<ProjectRelationshipManagers> getProjectRelationshipManagerses() {
        return projectRelationshipManagerses;
    }

    public void setProjectRelationshipManagerses(ArrayList<ProjectRelationshipManagers> projectRelationshipManagerses) {
        this.projectRelationshipManagerses = projectRelationshipManagerses;
    }

    public ArrayList<Towers> getTowerses() {
        return towerses;
    }

    public void setTowerses(ArrayList<Towers> towerses) {
        this.towerses = towerses;
    }

    public class ProjectRelationshipManagers implements Serializable{
        String Id;
        String Name;
        String Email;
        String PhoneNumber;
        String Photo;
        String Designation;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            PhoneNumber = phoneNumber;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String photo) {
            Photo = photo;
        }

        public String getDesignation() {
            return Designation;
        }

        public void setDesignation(String designation) {
            Designation = designation;
        }
    }

    public class PropertySize implements Serializable {
        String Id;
        String title;
        String applicationForm;
        String property_typeIdlevel1;
        String property_typeIdlevel2;
        String property_typeIdlevel3;
        String floorPlan;
        String size;
        String sizeUnit;
        String builtarea;
        String carpetarea;
        String sizeUnit2;
        String AboutProperty;
        String Length;
        String Breadth;
        String SizeUnit3;
        String ExtraArea;
        String MinimumPrice;
        String MaximumPrice;
        String BathRooms;
        String BedRooms;
        String TowerId;
        String isCostAppliedCarpetArea;
        String ExtraAreaLabel;
        String Discounts;
        String LayoutPlans;
        String DiscountGroups;
        String Towers;
        ArrayList<Specifications> specificationses = new ArrayList<>();

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getApplicationForm() {
            return applicationForm;
        }

        public void setApplicationForm(String applicationForm) {
            this.applicationForm = applicationForm;
        }

        public String getProperty_typeIdlevel1() {
            return property_typeIdlevel1;
        }

        public void setProperty_typeIdlevel1(String property_typeIdlevel1) {
            this.property_typeIdlevel1 = property_typeIdlevel1;
        }

        public String getProperty_typeIdlevel2() {
            return property_typeIdlevel2;
        }

        public void setProperty_typeIdlevel2(String property_typeIdlevel2) {
            this.property_typeIdlevel2 = property_typeIdlevel2;
        }

        public String getProperty_typeIdlevel3() {
            return property_typeIdlevel3;
        }

        public void setProperty_typeIdlevel3(String property_typeIdlevel3) {
            this.property_typeIdlevel3 = property_typeIdlevel3;
        }

        public String getFloorPlan() {
            return floorPlan;
        }

        public void setFloorPlan(String floorPlan) {
            this.floorPlan = floorPlan;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSizeUnit() {
            return sizeUnit;
        }

        public void setSizeUnit(String sizeUnit) {
            this.sizeUnit = sizeUnit;
        }

        public String getBuiltarea() {
            return builtarea;
        }

        public void setBuiltarea(String builtarea) {
            this.builtarea = builtarea;
        }

        public String getCarpetarea() {
            return carpetarea;
        }

        public void setCarpetarea(String carpetarea) {
            this.carpetarea = carpetarea;
        }

        public String getSizeUnit2() {
            return sizeUnit2;
        }

        public void setSizeUnit2(String sizeUnit2) {
            this.sizeUnit2 = sizeUnit2;
        }

        public String getAboutProperty() {
            return AboutProperty;
        }

        public void setAboutProperty(String aboutProperty) {
            AboutProperty = aboutProperty;
        }

        public String getLength() {
            return Length;
        }

        public void setLength(String length) {
            Length = length;
        }

        public String getBreadth() {
            return Breadth;
        }

        public void setBreadth(String breadth) {
            Breadth = breadth;
        }

        public String getSizeUnit3() {
            return SizeUnit3;
        }

        public void setSizeUnit3(String sizeUnit3) {
            SizeUnit3 = sizeUnit3;
        }

        public String getExtraArea() {
            return ExtraArea;
        }

        public void setExtraArea(String extraArea) {
            ExtraArea = extraArea;
        }

        public String getMinimumPrice() {
            return MinimumPrice;
        }

        public void setMinimumPrice(String minimumPrice) {
            MinimumPrice = minimumPrice;
        }

        public String getMaximumPrice() {
            return MaximumPrice;
        }

        public void setMaximumPrice(String maximumPrice) {
            MaximumPrice = maximumPrice;
        }

        public String getBathRooms() {
            return BathRooms;
        }

        public void setBathRooms(String bathRooms) {
            BathRooms = bathRooms;
        }

        public String getBedRooms() {
            return BedRooms;
        }

        public void setBedRooms(String bedRooms) {
            BedRooms = bedRooms;
        }

        public String getTowerId() {
            return TowerId;
        }

        public void setTowerId(String towerId) {
            TowerId = towerId;
        }

        public String getIsCostAppliedCarpetArea() {
            return isCostAppliedCarpetArea;
        }

        public void setIsCostAppliedCarpetArea(String isCostAppliedCarpetArea) {
            this.isCostAppliedCarpetArea = isCostAppliedCarpetArea;
        }

        public String getExtraAreaLabel() {
            return ExtraAreaLabel;
        }

        public void setExtraAreaLabel(String extraAreaLabel) {
            ExtraAreaLabel = extraAreaLabel;
        }

        public String getDiscounts() {
            return Discounts;
        }

        public void setDiscounts(String discounts) {
            Discounts = discounts;
        }

        public String getLayoutPlans() {
            return LayoutPlans;
        }

        public void setLayoutPlans(String layoutPlans) {
            LayoutPlans = layoutPlans;
        }

        public String getDiscountGroups() {
            return DiscountGroups;
        }

        public void setDiscountGroups(String discountGroups) {
            DiscountGroups = discountGroups;
        }

        public String getTowers() {
            return Towers;
        }

        public void setTowers(String towers) {
            Towers = towers;
        }

        public ArrayList<Specifications> getSpecificationses() {
            return specificationses;
        }

        public void setSpecificationses(ArrayList<Specifications> specificationses) {
            this.specificationses = specificationses;
        }

        public class Specifications {

        }
    }
    public class Towers implements Serializable{

        String TowerId;
        String TowerName;
        String Stories;
        String Basement;
        String ProjectCategoryId;
        String PropertyTypeId;
        String floorType;

        public String getTowerId() {
            return TowerId;
        }

        public void setTowerId(String towerId) {
            TowerId = towerId;
        }

        public String getTowerName() {
            return TowerName;
        }

        public void setTowerName(String towerName) {
            TowerName = towerName;
        }

        public String getStories() {
            return Stories;
        }

        public void setStories(String stories) {
            Stories = stories;
        }

        public String getBasement() {
            return Basement;
        }

        public void setBasement(String basement) {
            Basement = basement;
        }

        public String getProjectCategoryId() {
            return ProjectCategoryId;
        }

        public void setProjectCategoryId(String projectCategoryId) {
            ProjectCategoryId = projectCategoryId;
        }

        public String getPropertyTypeId() {
            return PropertyTypeId;
        }

        public void setPropertyTypeId(String propertyTypeId) {
            PropertyTypeId = propertyTypeId;
        }

        public String getFloorType() {
            return floorType;
        }

        public void setFloorType(String floorType) {
            this.floorType = floorType;
        }
    }



    public String getProjectDetailJson() {
        return projectDetailJson;
    }

    public void setProjectDetailJson(String projectDetailJson) {
        this.projectDetailJson = projectDetailJson;
    }

    public String getTopImage1() {
        return topImage1;
    }

    public void setTopImage1(String topImage1) {
        this.topImage1 = topImage1;
    }

    public String getPropertyTitles() {
        return propertyTitles;
    }

    public void setPropertyTitles(String propertyTitles) {
        this.propertyTitles = propertyTitles;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getLogoimage() {
        return logoimage;
    }

    public void setLogoimage(String logoimage) {
        this.logoimage = logoimage;
    }

    public String getTopImage2() {
        return topImage2;
    }

    public void setTopImage2(String topImage2) {
        this.topImage2 = topImage2;
    }

    public String getTopImage3() {
        return topImage3;
    }

    public void setTopImage3(String topImage3) {
        this.topImage3 = topImage3;
    }

    public String getAboutSubProject() {
        return aboutSubProject;
    }

    public void setAboutSubProject(String aboutSubProject) {
        this.aboutSubProject = aboutSubProject;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getBuilderCompanyId() {
        return builderCompanyId;
    }

    public void setBuilderCompanyId(String builderCompanyId) {
        this.builderCompanyId = builderCompanyId;
    }

    public String getBuilderCompanyName() {
        return builderCompanyName;
    }

    public void setBuilderCompanyName(String builderCompanyName) {
        this.builderCompanyName = builderCompanyName;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getTypeOfProject() {
        return TypeOfProject;
    }

    public void setTypeOfProject(String typeOfProject) {
        TypeOfProject = typeOfProject;
    }
}


