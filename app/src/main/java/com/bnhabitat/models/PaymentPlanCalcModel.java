package com.bnhabitat.models;

import java.io.Serializable;

/**
 * Created by developer15 on 3/17/2016.
 */
public class PaymentPlanCalcModel implements Serializable {

    int subCompoId;
    int price;
    int serviceTax;
    String componentName;

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(int serviceTax) {
        this.serviceTax = serviceTax;
    }

    public int getSubCompoId() {
        return subCompoId;
    }

    public void setSubCompoId(int subCompoId) {
        this.subCompoId = subCompoId;
    }
}
