package com.bnhabitat.models;

/**
 * Created by gourav on 8/4/2017.
 */

public class PhoneModel {
    String id;
    String phone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
