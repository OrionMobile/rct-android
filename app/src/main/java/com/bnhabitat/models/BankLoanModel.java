package com.bnhabitat.models;

/**
 * Created by gourav on 2/5/2018.
 */

public class BankLoanModel {
    String bank_id;
    String loan_amount;
    String loan_tenure;
    String last_instalment;
    String disbursed;
    String total_outstanding;

    public String getBank_id() {
        return bank_id;
    }

    public void setBank_id(String bank_id) {
        this.bank_id = bank_id;
    }

    public String getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(String loan_amount) {
        this.loan_amount = loan_amount;
    }

    public String getLoan_tenure() {
        return loan_tenure;
    }

    public void setLoan_tenure(String loan_tenure) {
        this.loan_tenure = loan_tenure;
    }

    public String getLast_instalment() {
        return last_instalment;
    }

    public void setLast_instalment(String last_instalment) {
        this.last_instalment = last_instalment;
    }

    public String getDisbursed() {
        return disbursed;
    }

    public void setDisbursed(String disbursed) {
        this.disbursed = disbursed;
    }

    public String getTotal_outstanding() {
        return total_outstanding;
    }

    public void setTotal_outstanding(String total_outstanding) {
        this.total_outstanding = total_outstanding;
    }
}
