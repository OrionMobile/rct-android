package com.bnhabitat.models;

public class FloorCalculationModel {


    String Count;
    int position;
    int deletePos;
    String Delete;
    int Id;
    int FloorNumber;
    double FloorSize;
    int FloorSizeUnitId;

    public int getFloorSizeUnitId() {
        return FloorSizeUnitId;
    }

    public void setFloorSizeUnitId(int floorSizeUnitId) {
        FloorSizeUnitId = floorSizeUnitId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getFloorNumber() {
        return FloorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        FloorNumber = floorNumber;
    }

    public double getFloorSize() {
        return FloorSize;
    }

    public void setFloorSize(double floorSize) {
        FloorSize = floorSize;
    }

    public String getDelete() {
        return Delete;
    }

    public void setDelete(String delete) {
        Delete = delete;
    }

    public int getDeletePos() {
        return deletePos;
    }

    public void setDeletePos(int deletePos) {
        this.deletePos = deletePos;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
