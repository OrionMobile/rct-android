package com.bnhabitat.models;

import java.util.ArrayList;

public class PropBedroomModel {

    int Id;
    int PropertyId;
    String GroundType;
    String FloorNo;
    String BedRoomName;
    String BedRoomType;
    String BedRoomSize;
    int BedRoomSizeUnitId;
    int Length;
    int Width;
    int LengthWidthUnitId;
    String Category;
    ArrayList<AttachedModel> PropertyBedroomAttachWiths = new ArrayList();

    boolean IsDeletable;
    boolean IsModify;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(int propertyId) {
        PropertyId = propertyId;
    }

    public String getGroundType() {
        return GroundType;
    }

    public void setGroundType(String groundType) {
        GroundType = groundType;
    }

    public String getFloorNo() {
        return FloorNo;
    }

    public void setFloorNo(String floorNo) {
        FloorNo = floorNo;
    }

    public String getBedRoomName() {
        return BedRoomName;
    }

    public void setBedRoomName(String bedRoomName) {
        BedRoomName = bedRoomName;
    }

    public String getBedRoomType() {
        return BedRoomType;
    }

    public void setBedRoomType(String bedRoomType) {
        BedRoomType = bedRoomType;
    }

    public String getBedRoomSize() {
        return BedRoomSize;
    }

    public void setBedRoomSize(String bedRoomSize) {
        BedRoomSize = bedRoomSize;
    }

    public int getBedRoomSizeUnitId() {
        return BedRoomSizeUnitId;
    }

    public void setBedRoomSizeUnitId(int bedRoomSizeUnitId) {
        BedRoomSizeUnitId = bedRoomSizeUnitId;
    }

    public int getLength() {
        return Length;
    }

    public void setLength(int length) {
        Length = length;
    }

    public int getWidth() {
        return Width;
    }

    public void setWidth(int width) {
        Width = width;
    }

    public int getLengthWidthUnitId() {
        return LengthWidthUnitId;
    }

    public void setLengthWidthUnitId(int lengthWidthUnitId) {
        LengthWidthUnitId = lengthWidthUnitId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public ArrayList<AttachedModel> getPropertyBedroomAttachWiths() {
        return PropertyBedroomAttachWiths;
    }

    public void setPropertyBedroomAttachWiths(ArrayList<AttachedModel> propertyBedroomAttachWiths) {
        PropertyBedroomAttachWiths = propertyBedroomAttachWiths;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }

    public static class AttachedModel {

        int AttachPropertyBedroomId;
        int Id;

        public int getAttachPropertyBedroomId() {
            return AttachPropertyBedroomId;
        }

        public void setAttachPropertyBedroomId(int attachPropertyBedroomId) {
            AttachPropertyBedroomId = attachPropertyBedroomId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }
    }
}
