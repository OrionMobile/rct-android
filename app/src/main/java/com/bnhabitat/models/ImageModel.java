package com.bnhabitat.models;

public class ImageModel {

    int Id;
    boolean IsDeletable;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }
}
