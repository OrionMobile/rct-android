package com.bnhabitat.models;

import java.io.Serializable;
import java.util.ArrayList;

public class PropertyBedroomModel {

    String id;
    String PropertyId;
    String goundtype;
    String floor;
    String room_name;
    String room_type;
    String length;
    String width;
    String room_size;
    String BedRoomSizeUnitId;
    String LengthWidthUnitId;
    String Category;
    String SizeUnit;
    ArrayList<AttachPropertyBedroomId>attachPropertyBedroomIds=new ArrayList<>();

    public ArrayList<AttachPropertyBedroomId> getAttachPropertyBedroomIds() {
        return attachPropertyBedroomIds;
    }

    public void setAttachPropertyBedroomIds(ArrayList<AttachPropertyBedroomId> attachPropertyBedroomIds) {
        this.attachPropertyBedroomIds = attachPropertyBedroomIds;
    }

    public String getSizeUnit() {
        return SizeUnit;
    }

    public void setSizeUnit(String sizeUnit) {
        SizeUnit = sizeUnit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(String propertyId) {
        PropertyId = propertyId;
    }

    public String getBedRoomSizeUnitId() {
        return BedRoomSizeUnitId;
    }

    public void setBedRoomSizeUnitId(String bedRoomSizeUnitId) {
        BedRoomSizeUnitId = bedRoomSizeUnitId;
    }

    public String getLengthWidthUnitId() {
        return LengthWidthUnitId;
    }

    public void setLengthWidthUnitId(String lengthWidthUnitId) {
        LengthWidthUnitId = lengthWidthUnitId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getGoundtype() {
        return goundtype;
    }

    public void setGoundtype(String goundtype) {
        this.goundtype = goundtype;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getRoom_size() {
        return room_size;
    }

    public void setRoom_size(String room_size) {
        this.room_size = room_size;
    }

public class AttachPropertyBedroomId implements Serializable{
    String AttachPropertyBedroomId;
    String AttachPropertyBedroomName;

    public String getAttachPropertyBedroomName() {
        return AttachPropertyBedroomName;
    }

    public void setAttachPropertyBedroomName(String attachPropertyBedroomName) {
        AttachPropertyBedroomName = attachPropertyBedroomName;
    }

    public String getAttachPropertyBedroomId() {
        return AttachPropertyBedroomId;
    }

    public void setAttachPropertyBedroomId(String attachPropertyBedroomId) {
        AttachPropertyBedroomId = attachPropertyBedroomId;
    }
}
}
