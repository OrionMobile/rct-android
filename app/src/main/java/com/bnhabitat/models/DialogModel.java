package com.bnhabitat.models;

import java.util.ArrayList;

/**
 * Created by developer15 on 8/18/2016.
 */
public class DialogModel {

    String clickedOn;

    String unit_to;

    String unit_from;

    float area;
    String name;
    int posSpinner;


    public int getPosSpinner() {
        return posSpinner;
    }

    public void setPosSpinner(int posSpinner) {
        this.posSpinner = posSpinner;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit_to() {
        return unit_to;
    }

    public void setUnit_to(String unit_to) {
        this.unit_to = unit_to;
    }

    public String getUnit_from() {
        return unit_from;
    }

    public void setUnit_from(String unit_from) {
        this.unit_from = unit_from;
    }

    public String getClickedOn() {
        return clickedOn;
    }

    public void setClickedOn(String clickedOn) {
        this.clickedOn = clickedOn;
    }
}
