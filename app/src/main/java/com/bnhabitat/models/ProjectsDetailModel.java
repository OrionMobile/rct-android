package com.bnhabitat.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProjectsDetailModel implements Serializable {

    int id;
    int builderProjectId;
    String title;
    String logoImage;
    int minPrice;
    int maxPrice;
    String topImage1;
    String topImage2;
    String topImage3;
    String aboutSubProject;
    String specifications;
    boolean isDeleted;
    String createdOn;
    boolean isPublished;
    int builderCompanyId;
    String builderCompanyName;
    String pageTitle;
    String metadescription;
    String metaKeywords;
    int subProjectFlagId;
    String disclaimer1;
    String disclaimer2;
    int countryId;
    String countryName;
    int stateId;
    String stateName;
    int cityId;
    String cityName;
    int areaUnit;
    String area;
    String region;
    String locality;
    String tagline;
    int typeofProject;
    int paymentPlanId;
    String updatedOn;
    String projectStatus;
    int numberOfParking;
    int possessionMonth;
    int possessionYear;
    String propertyTitles;
    String NoOfUnits;
    String NumberOfTowers;
    String BedRooms;
    String TypeOfProject;
    String CompanyLogo;

    String CurrentOffers;

    String AreaUnitId;
    String ImagePath;

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getAreaUnitId() {
        return AreaUnitId;
    }

    public void setAreaUnitId(String areaUnitId) {
        AreaUnitId = areaUnitId;
    }

    public String getCurrentOffers() {
        return CurrentOffers;
    }

    public void setCurrentOffers(String currentOffers) {
        CurrentOffers = currentOffers;
    }

    public String getCompanyLogo() {
        return CompanyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        CompanyLogo = companyLogo;
    }

    public String getTypeOfProject() {
        return TypeOfProject;
    }

    public void setTypeOfProject(String typeOfProject) {
        TypeOfProject = typeOfProject;
    }

    public String getBedRooms() {
        return BedRooms;
    }

    public void setBedRooms(String bedRooms) {
        BedRooms = bedRooms;
    }

    public String getNumberOfTowers() {
        return NumberOfTowers;
    }

    public void setNumberOfTowers(String numberOfTowers) {
        NumberOfTowers = numberOfTowers;
    }

    public String getNoOfUnits() {
        return NoOfUnits;
    }

    public void setNoOfUnits(String noOfUnits) {
        NoOfUnits = noOfUnits;
    }

    public String getPropertyTitles() {
        return propertyTitles;
    }

    public void setPropertyTitles(String propertyTitles) {
        this.propertyTitles = propertyTitles;
    }

    ArrayList<PropertySize> propertySizes = new ArrayList<PropertySize>();
    ArrayList<KeyDistances> keyDistances = new ArrayList<KeyDistances>();
    ArrayList<Amenities> amenities = new ArrayList<Amenities>();
    ArrayList<SpecialFeatures> specialFeatures = new ArrayList<SpecialFeatures>();
    ArrayList<FAQs> faqs = new ArrayList<FAQs>();
    ArrayList<Towers> towers = new ArrayList<Towers>();
    ArrayList<ProjectRelationshipManagers> projectRelationshipManagers = new ArrayList<ProjectRelationshipManagers>();
    ArrayList<ProjectLenders> projectLenders = new ArrayList<ProjectLenders>();

    ArrayList<PricingModel> pricingModels = new ArrayList<>();
    ArrayList<PaymentPlanModel> paymentPlanModels = new ArrayList<>();


    public ArrayList<Towers> getTowers() {
        return towers;
    }

    public void setTowers(ArrayList<Towers> towers) {
        this.towers = towers;
    }

    public ArrayList<PricingModel> getPricingModels() {
        return pricingModels;
    }


    public ArrayList<ProjectRelationshipManagers> getProjectRelationshipManagers() {
        return projectRelationshipManagers;
    }

    public void setProjectRelationshipManagers(ArrayList<ProjectRelationshipManagers> projectRelationshipManagers) {
        this.projectRelationshipManagers = projectRelationshipManagers;
    }

    public void setPricingModels(ArrayList<PricingModel> pricingModels) {
        this.pricingModels = pricingModels;
    }

    public ArrayList<PaymentPlanModel> getPaymentPlanModels() {
        return paymentPlanModels;
    }

    public void setPaymentPlanModels(ArrayList<PaymentPlanModel> paymentPlanModels) {
        this.paymentPlanModels = paymentPlanModels;
    }

    public ArrayList<FAQs> getFaqs() {
        return faqs;
    }

    public void setFaqs(ArrayList<FAQs> faqs) {
        this.faqs = faqs;
    }

    public ArrayList<SpecialFeatures> getSpecialFeatures() {
        return specialFeatures;
    }

    public void setSpecialFeatures(ArrayList<SpecialFeatures> specialFeatures) {
        this.specialFeatures = specialFeatures;
    }

    public ArrayList<Amenities> getAmenities() {
        return amenities;
    }

    public void setAmenities(ArrayList<Amenities> amenities) {
        this.amenities = amenities;
    }

    public ArrayList<KeyDistances> getKeyDistances() {
        return keyDistances;
    }

    public void setKeyDistances(ArrayList<KeyDistances> keyDistances) {
        this.keyDistances = keyDistances;
    }

    public ArrayList<PropertySize> getPropertySizes() {
        return propertySizes;
    }

    public void setPropertySizes(ArrayList<PropertySize> propertySizes) {
        this.propertySizes = propertySizes;
    }

    public ArrayList<ProjectLenders> getProjectLenders() {
        return projectLenders;
    }

    public void setProjectLenders(ArrayList<ProjectLenders> projectLenders) {
        this.projectLenders = projectLenders;
    }

    public class Amenities implements Serializable {
        int amenityId;
        String amenity;
        String image;
        String createDate;
        String modifiedDate;
        String description;
        int subProjectId;
        int townshipId;

        public int getAmenityId() {
            return amenityId;
        }

        public void setAmenityId(int amenityId) {
            this.amenityId = amenityId;
        }

        public String getAmenity() {
            return amenity;
        }

        public void setAmenity(String amenity) {
            this.amenity = amenity;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getSubProjectId() {
            return subProjectId;
        }

        public void setSubProjectId(int subProjectId) {
            this.subProjectId = subProjectId;
        }

        public int getTownshipId() {
            return townshipId;
        }

        public void setTownshipId(int townshipId) {
            this.townshipId = townshipId;
        }


    }


    public class SpecialFeatures implements Serializable {

        int id;
        String featureName;
        int projectId;
        String description;
        String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFeatureName() {
            return featureName;
        }

        public void setFeatureName(String featureName) {
            this.featureName = featureName;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }


    }


    public class ProjectRelationshipManagers implements Serializable {

        int Id;
        String Name;
        String Email;
        String PhoneNumber;
        String Photo;
        String Designation;


        public String getDesignation() {
            return Designation;
        }

        public void setDesignation(String designation) {
            Designation = designation;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            PhoneNumber = phoneNumber;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String photo) {
            Photo = photo;
        }
    }


    public class ProjectLenders implements Serializable {

        int LenderId;
        String Name;
        String Logo;
        String Website;
        String Address;
        String ContactDetails;
        String Description;

        public int getLenderId() {
            return LenderId;
        }

        public void setLenderId(int lenderId) {
            LenderId = lenderId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String logo) {
            Logo = logo;
        }

        public String getWebsite() {
            return Website;
        }

        public void setWebsite(String website) {
            Website = website;
        }

        public String getContactDetails() {
            return ContactDetails;
        }

        public void setContactDetails(String contactDetails) {
            ContactDetails = contactDetails;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }
    }

    public class FAQs implements Serializable {

        int id;
        int projectId;
        String question;
        String answer;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }


    }


    public class KeyDistances implements Serializable {

        String keyDistanceId;
        String nameOfLocation;
        String distance;
        String image;
        String unitName;
        String distanceUnit;
        int subProjectId;

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public String getKeyDistanceId() {
            return keyDistanceId;
        }

        public void setKeyDistanceId(String keyDistanceId) {
            this.keyDistanceId = keyDistanceId;
        }

        public String getNameOfLocation() {
            return nameOfLocation;
        }

        public void setNameOfLocation(String nameOfLocation) {
            this.nameOfLocation = nameOfLocation;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDistanceUnit() {
            return distanceUnit;
        }

        public void setDistanceUnit(String distanceUnit) {
            this.distanceUnit = distanceUnit;
        }

        public int getSubProjectId() {
            return subProjectId;
        }

        public void setSubProjectId(int subProjectId) {
            this.subProjectId = subProjectId;
        }


    }


    public class Towers implements Serializable {

        int towerId;
        String towerName;
        int stories;
        int basement;
        int projectCategoryId;
        int propertyTypeId;
        String floorType;


        public int getTowerId() {
            return towerId;
        }

        public void setTowerId(int towerId) {
            this.towerId = towerId;
        }

        public String getTowerName() {
            return towerName;
        }

        public void setTowerName(String towerName) {
            this.towerName = towerName;
        }

        public int getStories() {
            return stories;
        }

        public void setStories(int stories) {
            this.stories = stories;
        }

        public int getBasement() {
            return basement;
        }

        public void setBasement(int basement) {
            this.basement = basement;
        }

        public int getProjectCategoryId() {
            return projectCategoryId;
        }

        public void setProjectCategoryId(int projectCategoryId) {
            this.projectCategoryId = projectCategoryId;
        }

        public int getPropertyTypeId() {
            return propertyTypeId;
        }

        public void setPropertyTypeId(int propertyTypeId) {
            this.propertyTypeId = propertyTypeId;
        }

        public String getFloorType() {
            return floorType;
        }

        public void setFloorType(String floorType) {
            this.floorType = floorType;
        }
    }


    public class PropertySize implements Serializable {

        int id;
        String title;
        String applicationForm;
        int property_typeIdlevel1;
        int property_typeIdlevel2;
        int property_typeIdlevel3;
        String floorPlan;
        String size;
        int sizeUnit;
        String builtarea;
        int sizeUnit2;
        String aboutProperty;
        String length;
        String breadth;
        int sizeUnit3;
        String carpetarea;
        String extraArea;
        long minimumPrice;
        long maximumPrice;
        int bathRooms;
        int bedRooms;
        boolean isApplicableOnCarpetArea;
        String DiscountGroups;
        String isCostAppliedCarpetArea;
        String ExtraAreaLabel;
        String Towers;
        String Discounts;
        String TowerId;

        public String getIsCostAppliedCarpetArea() {
            return isCostAppliedCarpetArea;
        }

        public void setIsCostAppliedCarpetArea(String isCostAppliedCarpetArea) {
            this.isCostAppliedCarpetArea = isCostAppliedCarpetArea;
        }

        public String getDiscountGroups() {
            return DiscountGroups;
        }

        public void setDiscountGroups(String discountGroups) {
            DiscountGroups = discountGroups;
        }

        public String getExtraAreaLabel() {
            return ExtraAreaLabel;
        }

        public void setExtraAreaLabel(String extraAreaLabel) {
            ExtraAreaLabel = extraAreaLabel;
        }

        public void setTowers(String towers) {
            Towers = towers;
        }

        public void setDiscounts(String discounts) {
            Discounts = discounts;
        }

        public String getTowerId() {
            return TowerId;
        }

        public void setTowerId(String towerId) {
            TowerId = towerId;
        }

        ArrayList<Integer> towers = new ArrayList<>();
        ArrayList<Specifications> specifications = new ArrayList<Specifications>();
        ArrayList<Discounts> discounts = new ArrayList<>();


        public class Specifications implements Serializable {

            String BSPType;
            String AreaName;
            String Title;
            String Description;

            public String getBSPType() {
                return BSPType;
            }

            public void setBSPType(String bSPType) {
                BSPType = bSPType;
            }

            public String getAreaName() {
                return AreaName;
            }

            public void setAreaName(String areaName) {
                AreaName = areaName;
            }

            public String getTitle() {
                return Title;
            }

            public void setTitle(String title) {
                Title = title;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String description) {
                Description = description;
            }
        }

        ArrayList<LayoutPlans> layoutPlans = new ArrayList<>();


        public class LayoutPlans implements Serializable {

            int id;
            String title;
            String bigImage;
            String description;
            String type;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getBigImage() {
                return bigImage;
            }

            public void setBigImage(String bigImage) {
                this.bigImage = bigImage;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }


        public class Discounts implements Serializable {

            int id;
            String discountName;
            String discountValue;
            String discountType;
            String paymentOption;
            String StartDate;
            String EndDate;

            ArrayList<Components> components = new ArrayList<>();

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public ArrayList<Components> getComponents() {
                return components;
            }

            public void setComponents(ArrayList<Components> componentses) {
                this.components = componentses;
            }

            public String getEndDate() {
                return EndDate;
            }

            public void setEndDate(String endDate) {
                EndDate = endDate;
            }

            public String getStartDate() {
                return StartDate;
            }

            public void setStartDate(String startDate) {
                StartDate = startDate;
            }

            public String getPaymentOption() {
                return paymentOption;
            }

            public void setPaymentOption(String paymentOption) {
                this.paymentOption = paymentOption;
            }

            public String getDiscountType() {
                return discountType;
            }

            public void setDiscountType(String discountType) {
                this.discountType = discountType;
            }

            public String getDiscountValue() {
                return discountValue;
            }

            public void setDiscountValue(String discountValue) {
                this.discountValue = discountValue;
            }

            public String getDiscountName() {
                return discountName;
            }

            public void setDiscountName(String discountName) {
                this.discountName = discountName;
            }

            public class Components implements Serializable{
                int id;
                ArrayList<Integer> componentIds = new ArrayList<>();

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public ArrayList<Integer> getComponentIds() {
                    return componentIds;
                }

                public void setComponentIds(ArrayList<Integer> componentIds) {
                    this.componentIds = componentIds;
                }
            }
        }

        public boolean isApplicableOnCarpetArea() {
            return isApplicableOnCarpetArea;
        }

        public void setApplicableOnCarpetArea(boolean applicableOnCarpetArea) {
            isApplicableOnCarpetArea = applicableOnCarpetArea;
        }

        public ArrayList<Integer> getTowers() {
            return towers;
        }

        public void setTowers(ArrayList<Integer> towers) {
            this.towers = towers;
        }

        public ArrayList<LayoutPlans> getLayoutPlans() {
            return layoutPlans;
        }

        public void setLayoutPlans(ArrayList<LayoutPlans> layoutPlans) {
            this.layoutPlans = layoutPlans;
        }

        public String getCarpetarea() {
            return carpetarea;
        }

        public void setCarpetarea(String carpetarea) {
            this.carpetarea = carpetarea;
        }

        public ArrayList<Discounts> getDiscounts() {
            return discounts;
        }

        public void setDiscounts(ArrayList<Discounts> discounts) {
            this.discounts = discounts;
        }

        public ArrayList<Specifications> getSpecifications() {
            return specifications;
        }


        public void setSpecifications(ArrayList<Specifications> specifications) {
            this.specifications = specifications;
        }


        public int getId() {
            return id;
        }


        public void setId(int id) {
            this.id = id;
        }


        public String getTitle() {
            return title;
        }


        public void setTitle(String title) {
            this.title = title;
        }


        public String getApplicationForm() {
            return applicationForm;
        }


        public void setApplicationForm(String applicationForm) {
            this.applicationForm = applicationForm;
        }


        public int getProperty_typeIdlevel1() {
            return property_typeIdlevel1;
        }


        public void setProperty_typeIdlevel1(int property_typeIdlevel1) {
            this.property_typeIdlevel1 = property_typeIdlevel1;
        }


        public int getProperty_typeIdlevel2() {
            return property_typeIdlevel2;
        }


        public void setProperty_typeIdlevel2(int property_typeIdlevel2) {
            this.property_typeIdlevel2 = property_typeIdlevel2;
        }


        public int getProperty_typeIdlevel3() {
            return property_typeIdlevel3;
        }


        public void setProperty_typeIdlevel3(int property_typeIdlevel3) {
            this.property_typeIdlevel3 = property_typeIdlevel3;
        }


        public String getFloorPlan() {
            return floorPlan;
        }


        public void setFloorPlan(String floorPlan) {
            this.floorPlan = floorPlan;
        }


        public String getSize() {
            return size;
        }


        public void setSize(String size) {
            this.size = size;
        }


        public int getSizeUnit() {
            return sizeUnit;
        }


        public void setSizeUnit(int sizeUnit) {
            this.sizeUnit = sizeUnit;
        }


        public String getBuiltarea() {
            return builtarea;
        }


        public void setBuiltarea(String builtarea) {
            this.builtarea = builtarea;
        }


        public int getSizeUnit2() {
            return sizeUnit2;
        }


        public void setSizeUnit2(int sizeUnit2) {
            this.sizeUnit2 = sizeUnit2;
        }


        public String getAboutProperty() {
            return aboutProperty;
        }


        public void setAboutProperty(String aboutProperty) {
            this.aboutProperty = aboutProperty;
        }


        public String getLength() {
            return length;
        }


        public void setLength(String length) {
            this.length = length;
        }


        public String getBreadth() {
            return breadth;
        }


        public void setBreadth(String breadth) {
            this.breadth = breadth;
        }


        public int getSizeUnit3() {
            return sizeUnit3;
        }


        public void setSizeUnit3(int sizeUnit3) {
            this.sizeUnit3 = sizeUnit3;
        }


        public String getExtraArea() {
            return extraArea;
        }


        public void setExtraArea(String extraArea) {
            this.extraArea = extraArea;
        }


        public long getMinimumPrice() {
            return minimumPrice;
        }


        public void setMinimumPrice(long minimumPrice) {
            this.minimumPrice = minimumPrice;
        }


        public long getMaximumPrice() {
            return maximumPrice;
        }


        public void setMaximumPrice(long maximumPrice) {
            this.maximumPrice = maximumPrice;
        }


        public int getBathRooms() {
            return bathRooms;
        }


        public void setBathRooms(int bathRooms) {
            this.bathRooms = bathRooms;
        }


        public int getBedRooms() {
            return bedRooms;
        }


        public void setBedRooms(int bedRooms) {
            this.bedRooms = bedRooms;
        }


    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBuilderProjectId() {
        return builderProjectId;
    }

    public void setBuilderProjectId(int builderProjectId) {
        this.builderProjectId = builderProjectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogoImage() {
        return logoImage;
    }

    public void setLogoImage(String logoImage) {
        this.logoImage = logoImage;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getTopImage1() {
        return topImage1;
    }

    public void setTopImage1(String topImage1) {
        this.topImage1 = topImage1;
    }

    public String getTopImage2() {
        return topImage2;
    }

    public void setTopImage2(String topImage2) {
        this.topImage2 = topImage2;
    }

    public String getTopImage3() {
        return topImage3;
    }

    public void setTopImage3(String topImage3) {
        this.topImage3 = topImage3;
    }

    public String getAboutSubProject() {
        return aboutSubProject;
    }

    public void setAboutSubProject(String aboutSubProject) {
        this.aboutSubProject = aboutSubProject;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public boolean isPublished() {
        return isPublished;
    }

    public void setPublished(boolean isPublished) {
        this.isPublished = isPublished;
    }

    public int getBuilderCompanyId() {
        return builderCompanyId;
    }

    public void setBuilderCompanyId(int builderCompanyId) {
        this.builderCompanyId = builderCompanyId;
    }

    public String getBuilderCompanyName() {
        return builderCompanyName;
    }

    public void setBuilderCompanyName(String builderCompanyName) {
        this.builderCompanyName = builderCompanyName;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getMetadescription() {
        return metadescription;
    }

    public void setMetadescription(String metadescription) {
        this.metadescription = metadescription;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public int getSubProjectFlagId() {
        return subProjectFlagId;
    }

    public void setSubProjectFlagId(int subProjectFlagId) {
        this.subProjectFlagId = subProjectFlagId;
    }

    public String getDisclaimer1() {
        return disclaimer1;
    }

    public void setDisclaimer1(String disclaimer1) {
        this.disclaimer1 = disclaimer1;
    }

    public String getDisclaimer2() {
        return disclaimer2;
    }

    public void setDisclaimer2(String disclaimer2) {
        this.disclaimer2 = disclaimer2;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getAreaUnit() {
        return areaUnit;
    }

    public void setAreaUnit(int areaUnit) {
        this.areaUnit = areaUnit;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public int getTypeofProject() {
        return typeofProject;
    }

    public void setTypeofProject(int typeofProject) {
        this.typeofProject = typeofProject;
    }

    public int getPaymentPlanId() {
        return paymentPlanId;
    }

    public void setPaymentPlanId(int paymentPlanId) {
        this.paymentPlanId = paymentPlanId;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public int getNumberOfParking() {
        return numberOfParking;
    }

    public void setNumberOfParking(int numberOfParking) {
        this.numberOfParking = numberOfParking;
    }

    public int getPossessionMonth() {
        return possessionMonth;
    }

    public void setPossessionMonth(int possessionMonth) {
        this.possessionMonth = possessionMonth;
    }

    public int getPossessionYear() {
        return possessionYear;
    }

    public void setPossessionYear(int possessionYear) {
        this.possessionYear = possessionYear;
    }


    ArrayList<LayoutPlans> layoutPlans = new ArrayList<>();


    public class LayoutPlans implements Serializable {

        int id;
        String title;
        String bigImage;
        String description;
        String type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBigImage() {
            return bigImage;
        }

        public void setBigImage(String bigImage) {
            this.bigImage = bigImage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public ArrayList<LayoutPlans> getLayoutPlans() {
        return layoutPlans;
    }

    public void setLayoutPlans(ArrayList<LayoutPlans> layoutPlans) {
        this.layoutPlans = layoutPlans;
    }
}
