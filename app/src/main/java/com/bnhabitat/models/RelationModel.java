package com.bnhabitat.models;

import java.io.Serializable;

public class RelationModel implements Serializable {

    int Id;
    String Relation;
    String RelationFirstName;
    String RelationLastName;
    int ContactId;
    int RelationContactId;
    boolean IsDeletable;
    boolean IsModify;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public String getRelationFirstName() {
        return RelationFirstName;
    }

    public void setRelationFirstName(String relationFirstName) {
        RelationFirstName = relationFirstName;
    }

    public String getRelationLastName() {
        return RelationLastName;
    }

    public void setRelationLastName(String relationLastName) {
        RelationLastName = relationLastName;
    }

    public int getContactId() {
        return ContactId;
    }

    public void setContactId(int contactId) {
        ContactId = contactId;
    }

    public int getRelationContactId() {
        return RelationContactId;
    }

    public void setRelationContactId(int relationContactId) {
        RelationContactId = relationContactId;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }
}
