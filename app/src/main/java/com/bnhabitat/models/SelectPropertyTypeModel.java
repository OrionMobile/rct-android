package com.bnhabitat.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by gourav on 1/30/2018.
 */

public class SelectPropertyTypeModel implements Serializable {


       String Id;
       String Name;
       String Type;

       public String getId() {
           return Id;
       }

       public void setId(String id) {
           Id = id;
       }

       public String getName() {
           return Name;
       }

       public void setName(String name) {
           Name = name;
       }

       public String getType() {
           return Type;
       }

       public void setType(String type) {
           Type = type;
       }


}
