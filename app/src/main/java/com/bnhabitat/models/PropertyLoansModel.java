package com.bnhabitat.models;

public class PropertyLoansModel {

    String Id;
    String PropertyId;
    String BankId;
    String LoanAmount;
    String LoanTenure;
    boolean FullDisbursed;
    String LastInstallmentPaidOn;
    String TotalOutstandingAfterLastInstallment;
    boolean IsDeletable;
    boolean IsModify;

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(String propertyId) {
        PropertyId = propertyId;
    }

    public String getBankId() {
        return BankId;
    }

    public void setBankId(String bankId) {
        BankId = bankId;
    }

    public String getLoanAmount() {
        return LoanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        LoanAmount = loanAmount;
    }

    public String getLoanTenure() {
        return LoanTenure;
    }

    public void setLoanTenure(String loanTenure) {
        LoanTenure = loanTenure;
    }

    public boolean getFullDisbursed() {
        return FullDisbursed;
    }

    public void setFullDisbursed(boolean fullDisbursed) {
        FullDisbursed = fullDisbursed;
    }

    public String getLastInstallmentPaidOn() {
        return LastInstallmentPaidOn;
    }

    public void setLastInstallmentPaidOn(String lastInstallmentPaidOn) {
        LastInstallmentPaidOn = lastInstallmentPaidOn;
    }

    public String getTotalOutstandingAfterLastInstallment() {
        return TotalOutstandingAfterLastInstallment;
    }

    public void setTotalOutstandingAfterLastInstallment(String totalOutstandingAfterLastInstallment) {
        TotalOutstandingAfterLastInstallment = totalOutstandingAfterLastInstallment;
    }
}
