package com.bnhabitat.models;

/**
 * Created by gourav on 1/30/2018.
 */

public class PincodeDetailModel {
    String DistrictName;
    String StateName;

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }
}
