package com.bnhabitat.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by gourav on 4/11/2018.
 */

public class InventoryModel_Area implements  Serializable {
    String Id;
    String propertyobjectId;
    String Nameobject;
    String Typeobject;
    String PropertyTypeId;
    String PropertyId;
    String CompanyId;
    String Cam;
    String Title;
    String CreatedById;
    boolean IsDraft;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public boolean isDraft() {
        return IsDraft;
    }

    public void setDraft(boolean draft) {
        IsDraft = draft;
    }

    public String getPropertyobjectId() {
        return propertyobjectId;
    }

    public void setPropertyobjectId(String propertyobjectId) {
        this.propertyobjectId = propertyobjectId;
    }

    public String getNameobject() {
        return Nameobject;
    }

    public void setNameobject(String nameobject) {
        Nameobject = nameobject;
    }

    public String getTypeobject() {
        return Typeobject;
    }

    public void setTypeobject(String typeobject) {
        Typeobject = typeobject;
    }

    int TotalRooms ;
    int TotalBathrooms ;
    int TotalParkings ;
    int TotalKitchen ;

    public int getTotalRooms() {
        return TotalRooms;
    }

    public void setTotalRooms(int totalRooms) {
        TotalRooms = totalRooms;
    }

    public int getTotalBathrooms() {
        return TotalBathrooms;
    }

    public void setTotalBathrooms(int totalBathrooms) {
        TotalBathrooms = totalBathrooms;
    }

    public int getTotalParkings() {
        return TotalParkings;
    }

    public void setTotalParkings(int totalParkings) {
        TotalParkings = totalParkings;
    }

    public int getTotalKitchen() {
        return TotalKitchen;
    }

    public void setTotalKitchen(int totalKitchen) {
        TotalKitchen = totalKitchen;
    }

    ArrayList<PropertyOtherQuestions>propertyOtherQuestionses=new ArrayList<>();
    ArrayList<PropertyLoans>propertyLoanses=new ArrayList<>();
    ArrayList<PropertyOwners> propertyOwnerses=new ArrayList<>();
    ArrayList<PropertyPlc>propertyPlcs=new ArrayList<>();
    ArrayList<PropertyOtherExpences>propertyOtherExpences=new ArrayList<>();
    ArrayList<PropertyArea>propertyAreas=new ArrayList<>();
    ArrayList<PropertyFinancials>propertyFinancialses=new ArrayList<>();
    ArrayList<PropertyImage>propertyImages=new ArrayList<>();
    ArrayList<PropertyLocation>propertyLocations=new ArrayList<>();
    ArrayList<PropertyLegalStatus>propertyLegalStatuses=new ArrayList<>();
    ArrayList<PropertyAccommodationDetails>propertyAccommodationDetailses=new ArrayList<>();
    ArrayList<PropertyBedrooms>propertyBedroomses=new ArrayList<>();
    ArrayList<PropertySpecifications>propertySpecificationses=new ArrayList<>();

    public ArrayList<PropertySpecifications> getPropertySpecificationses() {
        return propertySpecificationses;
    }

    public void setPropertySpecificationses(ArrayList<PropertySpecifications> propertySpecificationses) {
        this.propertySpecificationses = propertySpecificationses;
    }

    public ArrayList<PropertyBedrooms> getPropertyBedroomses() {
        return propertyBedroomses;
    }

    public void setPropertyBedroomses(ArrayList<PropertyBedrooms> propertyBedroomses) {
        this.propertyBedroomses = propertyBedroomses;
    }

    public ArrayList<PropertyAccommodationDetails> getPropertyAccommodationDetailses() {
        return propertyAccommodationDetailses;
    }

    public void setPropertyAccommodationDetailses(ArrayList<PropertyAccommodationDetails> propertyAccommodationDetailses) {
        this.propertyAccommodationDetailses = propertyAccommodationDetailses;
    }

    public ArrayList<PropertyLegalStatus> getPropertyLegalStatuses() {
        return propertyLegalStatuses;
    }

    public void setPropertyLegalStatuses(ArrayList<PropertyLegalStatus> propertyLegalStatuses) {
        this.propertyLegalStatuses = propertyLegalStatuses;
    }

    public ArrayList<PropertyLocation> getPropertyLocations() {
        return propertyLocations;
    }

    public void setPropertyLocations(ArrayList<PropertyLocation> propertyLocations) {
        this.propertyLocations = propertyLocations;
    }

    public ArrayList<PropertyOtherQuestions> getPropertyOtherQuestionses() {
        return propertyOtherQuestionses;
    }

    public void setPropertyOtherQuestionses(ArrayList<PropertyOtherQuestions> propertyOtherQuestionses) {
        this.propertyOtherQuestionses = propertyOtherQuestionses;
    }

    public ArrayList<PropertyLoans> getPropertyLoanses() {
        return propertyLoanses;
    }

    public void setPropertyLoanses(ArrayList<PropertyLoans> propertyLoanses) {
        this.propertyLoanses = propertyLoanses;
    }

    public ArrayList<PropertyOwners> getPropertyOwnerses() {
        return propertyOwnerses;
    }

    public void setPropertyOwnerses(ArrayList<PropertyOwners> propertyOwnerses) {
        this.propertyOwnerses = propertyOwnerses;
    }

    public ArrayList<PropertyPlc> getPropertyPlcs() {
        return propertyPlcs;
    }

    public void setPropertyPlcs(ArrayList<PropertyPlc> propertyPlcs) {
        this.propertyPlcs = propertyPlcs;
    }

    public ArrayList<PropertyOtherExpences> getPropertyOtherExpences() {
        return propertyOtherExpences;
    }

    public void setPropertyOtherExpences(ArrayList<PropertyOtherExpences> propertyOtherExpences) {
        this.propertyOtherExpences = propertyOtherExpences;
    }

    public ArrayList<PropertyArea> getPropertyAreas() {
        return propertyAreas;
    }

    public void setPropertyAreas(ArrayList<PropertyArea> propertyAreas) {
        this.propertyAreas = propertyAreas;
    }

    public ArrayList<PropertyFinancials> getPropertyFinancialses() {
        return propertyFinancialses;
    }

    public void setPropertyFinancialses(ArrayList<PropertyFinancials> propertyFinancialses) {
        this.propertyFinancialses = propertyFinancialses;
    }

    public ArrayList<PropertyImage> getPropertyImages() {
        return propertyImages;
    }

    public void setPropertyImages(ArrayList<PropertyImage> propertyImages) {
        this.propertyImages = propertyImages;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getCam() {
        return Cam;
    }

    public void setCam(String cam) {
        Cam = cam;
    }

    public String getCreatedById() {
        return CreatedById;
    }

    public void setCreatedById(String createdById) {
        CreatedById = createdById;
    }

    public class PropertyLegalStatus implements Serializable {
        String Id;
        String PropertyId;
        String TransferRestriction;
        String OwnerShipStatus;
        String OwnerShipType;
        String TransferByWayOf;
        String OwnedBy;
        String PossessionDate;
        String DateFrom;
        String NoOfTennure;
        String LeaseAlignmentPaid;
        String LastPaidDate;
        String LeaseAmountPayable;
        String PaymentCircle;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getTransferRestriction() {
            return TransferRestriction;
        }

        public void setTransferRestriction(String transferRestriction) {
            TransferRestriction = transferRestriction;
        }

        public String getOwnerShipStatus() {
            return OwnerShipStatus;
        }

        public void setOwnerShipStatus(String ownerShipStatus) {
            OwnerShipStatus = ownerShipStatus;
        }

        public String getOwnerShipType() {
            return OwnerShipType;
        }

        public void setOwnerShipType(String ownerShipType) {
            OwnerShipType = ownerShipType;
        }

        public String getTransferByWayOf() {
            return TransferByWayOf;
        }

        public void setTransferByWayOf(String transferByWayOf) {
            TransferByWayOf = transferByWayOf;
        }

        public String getOwnedBy() {
            return OwnedBy;
        }

        public void setOwnedBy(String ownedBy) {
            OwnedBy = ownedBy;
        }

        public String getPossessionDate() {
            return PossessionDate;
        }

        public void setPossessionDate(String possessionDate) {
            PossessionDate = possessionDate;
        }

        public String getDateFrom() {
            return DateFrom;
        }

        public void setDateFrom(String dateFrom) {
            DateFrom = dateFrom;
        }

        public String getNoOfTennure() {
            return NoOfTennure;
        }

        public void setNoOfTennure(String noOfTennure) {
            NoOfTennure = noOfTennure;
        }

        public String getLeaseAlignmentPaid() {
            return LeaseAlignmentPaid;
        }

        public void setLeaseAlignmentPaid(String leaseAlignmentPaid) {
            LeaseAlignmentPaid = leaseAlignmentPaid;
        }

        public String getLastPaidDate() {
            return LastPaidDate;
        }

        public void setLastPaidDate(String lastPaidDate) {
            LastPaidDate = lastPaidDate;
        }

        public String getLeaseAmountPayable() {
            return LeaseAmountPayable;
        }

        public void setLeaseAmountPayable(String leaseAmountPayable) {
            LeaseAmountPayable = leaseAmountPayable;
        }

        public String getPaymentCircle() {
            return PaymentCircle;
        }

        public void setPaymentCircle(String paymentCircle) {
            PaymentCircle = paymentCircle;
        }
    }

    public class PropertySpecifications implements Serializable {
        String Id;
        String Name;
        String PropertyId;

        String Category;
        String Description;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getCategory() {
            return Category;
        }

        public void setCategory(String category) {
            Category = category;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }
    }
    public class PropertyAccommodationDetails implements Serializable {
        String Id;
        String Name;
        String PropertyId;
        String Type;
        String TotalCount;



        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getTotalCount() {
            return TotalCount;
        }

        public void setTotalCount(String totalCount) {
            TotalCount = totalCount;
        }
    }
    public class PropertyBedrooms implements Serializable {
        String Id;
        String GroundType;
        String PropertyId;
        String FloorNo;
        String BedRoomName;
        String BedRoomType;
        String Category;

        public String getCategory() {
            return Category;
        }

        public void setCategory(String category) {
            Category = category;
        }

        private ArrayList<PropertySpecifications> childDataItems;

        public ArrayList<PropertySpecifications> getChildDataItems() {
            return childDataItems;
        }

        public void setChildDataItems(ArrayList<PropertySpecifications> childDataItems) {
            this.childDataItems = childDataItems;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getGroundType() {
            return GroundType;
        }

        public void setGroundType(String groundType) {
            GroundType = groundType;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getFloorNo() {
            return FloorNo;
        }

        public void setFloorNo(String floorNo) {
            FloorNo = floorNo;
        }

        public String getBedRoomName() {
            return BedRoomName;
        }

        public void setBedRoomName(String bedRoomName) {
            BedRoomName = bedRoomName;
        }

        public String getBedRoomType() {
            return BedRoomType;
        }

        public void setBedRoomType(String bedRoomType) {
            BedRoomType = bedRoomType;
        }
    }
    public class PropertyOtherQuestions implements Serializable {
        String Id;
        String PropertyId;
        String ContactId;
        public Contact Contact;
        String ContactNo;
        String FirstName;
        String LastName;
        String Email;
        String SubmitOfBehalfOf;
        String Relation;
        String AgentId;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getContactId() {
            return ContactId;
        }

        public void setContactId(String contactId) {
            ContactId = contactId;
        }

        public Contact getContact() {
            return Contact;
        }

        public void setContact(Contact contact) {
            Contact = contact;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String contactNo) {
            ContactNo = contactNo;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getSubmitOfBehalfOf() {
            return SubmitOfBehalfOf;
        }

        public void setSubmitOfBehalfOf(String submitOfBehalfOf) {
            SubmitOfBehalfOf = submitOfBehalfOf;
        }

        public String getRelation() {
            return Relation;
        }

        public void setRelation(String relation) {
            Relation = relation;
        }

        public String getAgentId() {
            return AgentId;
        }

        public void setAgentId(String agentId) {
            AgentId = agentId;
        }

        public class Contact{
            String Prefix, FirstName, MiddleName, LastName;
            String Designation;
            ArrayList<Addresses> Addresses = new ArrayList<>();
            ArrayList<Phones> Phones = new ArrayList<>();
            ArrayList<EmailAddresses> EmailAddresses = new ArrayList<>();

            public Contact() {
            }

            public String getPrefix() {
                return Prefix;
            }

            public void setPrefix(String prefix) {
                Prefix = prefix;
            }

            public String getFirstName() {
                return FirstName;
            }

            public void setFirstName(String firstName) {
                FirstName = firstName;
            }

            public String getMiddleName() {
                return MiddleName;
            }

            public void setMiddleName(String middleName) {
                MiddleName = middleName;
            }

            public String getLastName() {
                return LastName;
            }

            public void setLastName(String lastName) {
                LastName = lastName;
            }

            public String getDesignation() {
                return Designation;
            }

            public void setDesignation(String designation) {
                Designation = designation;
            }

            public ArrayList<Addresses> getAddresses() {
                return Addresses;
            }

            public void setAddresses(ArrayList<Addresses> addresses) {
                Addresses = addresses;
            }

            public ArrayList<Phones> getPhones() {
                return Phones;
            }

            public void setPhones(ArrayList<Phones> phones) {
                Phones = phones;
            }

            public ArrayList<EmailAddresses> getEmailAddresses() {
                return EmailAddresses;
            }

            public void setEmailAddresses(ArrayList<EmailAddresses> emailAddresses) {
                EmailAddresses = emailAddresses;
            }

            public class Addresses{
                String UnitName, TowerName, SubAreaName, LocalityName, VillageName, TehsilName, TownName, DistrictName, StateName, CountryName, ZipCode;

                public Addresses() {
                }

                public String getUnitName() {
                    return UnitName;
                }

                public void setUnitName(String unitName) {
                    UnitName = unitName;
                }

                public String getTowerName() {
                    return TowerName;
                }

                public void setTowerName(String towerName) {
                    TowerName = towerName;
                }

                public String getSubAreaName() {
                    return SubAreaName;
                }

                public void setSubAreaName(String subAreaName) {
                    SubAreaName = subAreaName;
                }

                public String getLocalityName() {
                    return LocalityName;
                }

                public void setLocalityName(String localityName) {
                    LocalityName = localityName;
                }

                public String getVillageName() {
                    return VillageName;
                }

                public void setVillageName(String villageName) {
                    VillageName = villageName;
                }

                public String getTehsilName() {
                    return TehsilName;
                }

                public void setTehsilName(String tehsilName) {
                    TehsilName = tehsilName;
                }

                public String getTownName() {
                    return TownName;
                }

                public void setTownName(String townName) {
                    TownName = townName;
                }

                public String getDistrictName() {
                    return DistrictName;
                }

                public void setDistrictName(String districtName) {
                    DistrictName = districtName;
                }

                public String getStateName() {
                    return StateName;
                }

                public void setStateName(String stateName) {
                    StateName = stateName;
                }

                public String getCountryName() {
                    return CountryName;
                }

                public void setCountryName(String countryName) {
                    CountryName = countryName;
                }

                public String getZipCode() {
                    return ZipCode;
                }

                public void setZipCode(String zipCode) {
                    ZipCode = zipCode;
                }
            }

            public class Phones{
                String PhoneCode, PhoneNumber;

                public Phones() {
                }

                public String getPhoneCode() {
                    return PhoneCode;
                }

                public void setPhoneCode(String phoneCode) {
                    PhoneCode = phoneCode;
                }

                public String getPhoneNumber() {
                    return PhoneNumber;
                }

                public void setPhoneNumber(String phoneNumber) {
                    PhoneNumber = phoneNumber;
                }
            }

            public class EmailAddresses{
                String Address;

                public EmailAddresses() {
                }

                public String getAddress() {
                    return Address;
                }

                public void setAddress(String address) {
                    Address = address;
                }
            }
        }
    }


    public class PropertyOwners implements Serializable {
        String Id;
        String PropertyId;
        String ContactId;
        String CompanyId;
        String PanNo;
        String AdhaarCardNo;
        String Address;
        String FirstName;
        String LastName;
        String Email;
        String PhoneNumber;
        String Occupation;
        String CompanyName;
        String CompanyType;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getContactId() {
            return ContactId;
        }

        public void setContactId(String contactId) {
            ContactId = contactId;
        }

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String companyId) {
            CompanyId = companyId;
        }

        public String getPanNo() {
            return PanNo;
        }

        public void setPanNo(String panNo) {
            PanNo = panNo;
        }

        public String getAdhaarCardNo() {
            return AdhaarCardNo;
        }

        public void setAdhaarCardNo(String adhaarCardNo) {
            AdhaarCardNo = adhaarCardNo;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            PhoneNumber = phoneNumber;
        }

        public String getOccupation() {
            return Occupation;
        }

        public void setOccupation(String occupation) {
            Occupation = occupation;
        }

        public String getCompanyName() {
            return CompanyName;
        }

        public void setCompanyName(String companyName) {
            CompanyName = companyName;
        }

        public String getCompanyType() {
            return CompanyType;
        }

        public void setCompanyType(String companyType) {
            CompanyType = companyType;
        }
    }
    public class PropertyOtherExpences implements Serializable {
        String Id;
        String PropertyId;
        String Title;
        String Value;
        String LastPaidDate;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getValue() {
            return Value;
        }

        public void setValue(String value) {
            Value = value;
        }

        public String getLastPaidDate() {
            return LastPaidDate;
        }

        public void setLastPaidDate(String lastPaidDate) {
            LastPaidDate = lastPaidDate;
        }
    }
    public class PropertyLoans implements Serializable {
        String Id;
        String PropertyId;
        String BankId;
        String BankName;
        String LoanAmount;
        String LoanTenure;
        String FullDisbursed;
        String LastInstallmentPaidOn;
        String TotalOutstandingAfterLastInstallment;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getBankId() {
            return BankId;
        }

        public void setBankId(String bankId) {
            BankId = bankId;
        }

        public String getBankName() {
            return BankName;
        }

        public void setBankName(String bankName) {
            BankName = bankName;
        }

        public String getLoanAmount() {
            return LoanAmount;
        }

        public void setLoanAmount(String loanAmount) {
            LoanAmount = loanAmount;
        }

        public String getLoanTenure() {
            return LoanTenure;
        }

        public void setLoanTenure(String loanTenure) {
            LoanTenure = loanTenure;
        }

        public String getFullDisbursed() {
            return FullDisbursed;
        }

        public void setFullDisbursed(String fullDisbursed) {
            FullDisbursed = fullDisbursed;
        }

        public String getLastInstallmentPaidOn() {
            return LastInstallmentPaidOn;
        }

        public void setLastInstallmentPaidOn(String lastInstallmentPaidOn) {
            LastInstallmentPaidOn = lastInstallmentPaidOn;
        }

        public String getTotalOutstandingAfterLastInstallment() {
            return TotalOutstandingAfterLastInstallment;
        }

        public void setTotalOutstandingAfterLastInstallment(String totalOutstandingAfterLastInstallment) {
            TotalOutstandingAfterLastInstallment = totalOutstandingAfterLastInstallment;
        }
    }
    public class PropertyFinancials implements Serializable {
        String Id;
        String PropertyId;
        String DemandPrice;
        String InclusivePrice;
        String IsNegociable;
        String Price;
        String PriceUnitId;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getDemandPrice() {
            return DemandPrice;
        }

        public void setDemandPrice(String demandPrice) {
            DemandPrice = demandPrice;
        }

        public String getInclusivePrice() {
            return InclusivePrice;
        }

        public void setInclusivePrice(String inclusivePrice) {
            InclusivePrice = inclusivePrice;
        }

        public String getIsNegociable() {
            return IsNegociable;
        }

        public void setIsNegociable(String isNegociable) {
            IsNegociable = isNegociable;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            Price = price;
        }

        public String getPriceUnitId() {
            return PriceUnitId;
        }

        public void setPriceUnitId(String priceUnitId) {
            PriceUnitId = priceUnitId;
        }
    }
    public class PropertySides implements Serializable{
        String Id;
        String PropertyId;
        String Name;
        String Length;
        String LengthUnitId;
        String Width;
        String WidthUnitId;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getLength() {
            return Length;
        }

        public void setLength(String length) {
            Length = length;
        }

        public String getLengthUnitId() {
            return LengthUnitId;
        }

        public void setLengthUnitId(String lengthUnitId) {
            LengthUnitId = lengthUnitId;
        }

        public String getWidth() {
            return Width;
        }

        public void setWidth(String width) {
            Width = width;
        }

        public String getWidthUnitId() {
            return WidthUnitId;
        }

        public void setWidthUnitId(String widthUnitId) {
            WidthUnitId = widthUnitId;
        }
    }
    public class PropertyImage implements Serializable{
        String Id;
        String PropertyId;
        String Name;
        String Type;
        String Url;
        String ImageCode;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }

        public String getImageCode() {
            return ImageCode;
        }

        public void setImageCode(String imageCode) {
            ImageCode = imageCode;
        }
    }
    public class PropertyPlc implements Serializable{
        String Id;
        String PropertyId;
        String Name;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
    public class PropertyArea implements Serializable{
        String Id;
        String PropertyId;
        String PlotShape;
        String PlotArea;
        String PlotAreaUnitId;
        String FrontSize;
        String FrontSizeUnitId;
        String DepthSize;
        String DepthSizeUnitId;
        String StreetOrRoadType;
        String RoadWidth;
        String RoadWidthUnitId;
        String EnteranceDoorFacing;
        String ParkingInFront;
        String HaveWalkingPath;
        String PlotOrCoverArea;
        String PlotOrCoverAreaUnitId;
        String SuperArea;
        String SuperAreaUnitId;
        String BuiltUpArea;
        String BuiltUpAreaUnitId;
        String CarpetArea;
        String CarpetAreaUnitId;
        String PropertySizeUnit4;

        public String getPropertySizeUnit4() {
            return PropertySizeUnit4;
        }

        public void setPropertySizeUnit4(String propertySizeUnit4) {
            PropertySizeUnit4 = propertySizeUnit4;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getPlotShape() {
            return PlotShape;
        }

        public void setPlotShape(String plotShape) {
            PlotShape = plotShape;
        }

        public String getPlotArea() {
            return PlotArea;
        }

        public void setPlotArea(String plotArea) {
            PlotArea = plotArea;
        }

        public String getPlotAreaUnitId() {
            return PlotAreaUnitId;
        }

        public void setPlotAreaUnitId(String plotAreaUnitId) {
            PlotAreaUnitId = plotAreaUnitId;
        }

        public String getFrontSize() {
            return FrontSize;
        }

        public void setFrontSize(String frontSize) {
            FrontSize = frontSize;
        }

        public String getFrontSizeUnitId() {
            return FrontSizeUnitId;
        }

        public void setFrontSizeUnitId(String frontSizeUnitId) {
            FrontSizeUnitId = frontSizeUnitId;
        }

        public String getDepthSize() {
            return DepthSize;
        }

        public void setDepthSize(String depthSize) {
            DepthSize = depthSize;
        }

        public String getDepthSizeUnitId() {
            return DepthSizeUnitId;
        }

        public void setDepthSizeUnitId(String depthSizeUnitId) {
            DepthSizeUnitId = depthSizeUnitId;
        }

        public String getStreetOrRoadType() {
            return StreetOrRoadType;
        }

        public void setStreetOrRoadType(String streetOrRoadType) {
            StreetOrRoadType = streetOrRoadType;
        }

        public String getRoadWidth() {
            return RoadWidth;
        }

        public void setRoadWidth(String roadWidth) {
            RoadWidth = roadWidth;
        }

        public String getRoadWidthUnitId() {
            return RoadWidthUnitId;
        }

        public void setRoadWidthUnitId(String roadWidthUnitId) {
            RoadWidthUnitId = roadWidthUnitId;
        }

        public String getEnteranceDoorFacing() {
            return EnteranceDoorFacing;
        }

        public void setEnteranceDoorFacing(String enteranceDoorFacing) {
            EnteranceDoorFacing = enteranceDoorFacing;
        }

        public String getParkingInFront() {
            return ParkingInFront;
        }

        public void setParkingInFront(String parkingInFront) {
            ParkingInFront = parkingInFront;
        }

        public String getHaveWalkingPath() {
            return HaveWalkingPath;
        }

        public void setHaveWalkingPath(String haveWalkingPath) {
            HaveWalkingPath = haveWalkingPath;
        }

        public String getPlotOrCoverArea() {
            return PlotOrCoverArea;
        }

        public void setPlotOrCoverArea(String plotOrCoverArea) {
            PlotOrCoverArea = plotOrCoverArea;
        }

        public String getPlotOrCoverAreaUnitId() {
            return PlotOrCoverAreaUnitId;
        }

        public void setPlotOrCoverAreaUnitId(String plotOrCoverAreaUnitId) {
            PlotOrCoverAreaUnitId = plotOrCoverAreaUnitId;
        }

        public String getSuperArea() {
            return SuperArea;
        }

        public void setSuperArea(String superArea) {
            SuperArea = superArea;
        }

        public String getSuperAreaUnitId() {
            return SuperAreaUnitId;
        }

        public void setSuperAreaUnitId(String superAreaUnitId) {
            SuperAreaUnitId = superAreaUnitId;
        }

        public String getBuiltUpArea() {
            return BuiltUpArea;
        }

        public void setBuiltUpArea(String builtUpArea) {
            BuiltUpArea = builtUpArea;
        }

        public String getBuiltUpAreaUnitId() {
            return BuiltUpAreaUnitId;
        }

        public void setBuiltUpAreaUnitId(String builtUpAreaUnitId) {
            BuiltUpAreaUnitId = builtUpAreaUnitId;
        }

        public String getCarpetArea() {
            return CarpetArea;
        }

        public void setCarpetArea(String carpetArea) {
            CarpetArea = carpetArea;
        }

        public String getCarpetAreaUnitId() {
            return CarpetAreaUnitId;
        }

        public void setCarpetAreaUnitId(String carpetAreaUnitId) {
            CarpetAreaUnitId = carpetAreaUnitId;
        }
    }
    public class PropertyLocation implements Serializable{
        String Id;
        String PropertyId;
        String Developer;
        String DeveloperLogo;
        String Project;
        String ZipCode;
        String State;
        String District;
        String SubArea;
        String TownName;
        String Sector;
        String SubDivision;
        String UnitNo;
        String TowerName;
        String FloorNo;
        String GooglePlusCode;
        String AreaAttributeValue;
        String AreaAttributeValue1;
        String AreaAttributeValue2;
        String AreaAttributeValue3;
        String AreaAttributeValue4;
        String UnitName;

        public String getUnitName() {
            return UnitName;
        }

        public void setUnitName(String unitName) {
            UnitName = unitName;
        }

        public String getAreaAttributeValue() {
            return AreaAttributeValue;
        }

        public void setAreaAttributeValue(String areaAttributeValue) {
            AreaAttributeValue = areaAttributeValue;
        }

        public String getAreaAttributeValue1() {
            return AreaAttributeValue1;
        }

        public void setAreaAttributeValue1(String areaAttributeValue1) {
            AreaAttributeValue1 = areaAttributeValue1;
        }

        public String getAreaAttributeValue2() {
            return AreaAttributeValue2;
        }

        public void setAreaAttributeValue2(String areaAttributeValue2) {
            AreaAttributeValue2 = areaAttributeValue2;
        }

        public String getAreaAttributeValue3() {
            return AreaAttributeValue3;
        }

        public void setAreaAttributeValue3(String areaAttributeValue3) {
            AreaAttributeValue3 = areaAttributeValue3;
        }

        public String getAreaAttributeValue4() {
            return AreaAttributeValue4;
        }

        public void setAreaAttributeValue4(String areaAttributeValue4) {
            AreaAttributeValue4 = areaAttributeValue4;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getPropertyId() {
            return PropertyId;
        }

        public void setPropertyId(String propertyId) {
            PropertyId = propertyId;
        }

        public String getDeveloper() {
            return Developer;
        }

        public void setDeveloper(String developer) {
            Developer = developer;
        }

        public String getDeveloperLogo() {
            return DeveloperLogo;
        }

        public void setDeveloperLogo(String developerLogo) {
            DeveloperLogo = developerLogo;
        }

        public String getProject() {
            return Project;
        }

        public void setProject(String project) {
            Project = project;
        }

        public String getZipCode() {
            return ZipCode;
        }

        public void setZipCode(String zipCode) {
            ZipCode = zipCode;
        }

        public String getState() {
            return State;
        }

        public void setState(String state) {
            State = state;
        }

        public String getDistrict() {
            return District;
        }

        public void setDistrict(String district) {
            District = district;
        }

        public String getSubArea() {
            return SubArea;
        }

        public void setSubArea(String subArea) {
            SubArea = subArea;
        }

        public String getTownName() {
            return TownName;
        }

        public void setTownName(String townName) {
            TownName = townName;
        }

        public String getSector() {
            return Sector;
        }

        public void setSector(String sector) {
            Sector = sector;
        }

        public String getSubDivision() {
            return SubDivision;
        }

        public void setSubDivision(String subDivision) {
            SubDivision = subDivision;
        }

        public String getUnitNo() {
            return UnitNo;
        }

        public void setUnitNo(String unitNo) {
            UnitNo = unitNo;
        }

        public String getTowerName() {
            return TowerName;
        }

        public void setTowerName(String towerName) {
            TowerName = towerName;
        }

        public String getFloorNo() {
            return FloorNo;
        }

        public void setFloorNo(String floorNo) {
            FloorNo = floorNo;
        }

        public String getGooglePlusCode() {
            return GooglePlusCode;
        }

        public void setGooglePlusCode(String googlePlusCode) {
            GooglePlusCode = googlePlusCode;
        }
    }
    public String getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(String propertyId) {
        PropertyId = propertyId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPropertyTypeId() {
        return PropertyTypeId;
    }

    public void setPropertyTypeId(String propertyTypeId) {
        PropertyTypeId = propertyTypeId;
    }


}
