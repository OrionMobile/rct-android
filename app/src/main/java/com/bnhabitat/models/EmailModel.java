package com.bnhabitat.models;

/**
 * Created by gourav on 8/18/2017.
 */

public class EmailModel {
    String id;
    String address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
