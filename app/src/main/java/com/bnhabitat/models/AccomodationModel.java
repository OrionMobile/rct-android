package com.bnhabitat.models;

import java.util.ArrayList;

public class AccomodationModel {

    public int Id;
    int ProjectCategoryTypeId;
    String Title;
    double TotalArea;
    int TotalAreaSizeUnitId;
    int Bedrooms;
    int Floors;
    double CoverAreaSize;
    int CoverAreaSizeUnitId;
    double SuperAreaSize;
    int SuperAreaSizeUnitId;
    double BuiltUpAreaSize;
    int BuiltUpAreaSizeUnitId;
    double CarpetAreaSize;
    int CarpetAreaSizeUnitId;
    int Parkings;

    ArrayList<AccomodationFilesModel> accomodationFilesModels = new ArrayList<>();

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getProjectCategoryTypeId() {
        return ProjectCategoryTypeId;
    }

    public void setProjectCategoryTypeId(int projectCategoryTypeId) {
        ProjectCategoryTypeId = projectCategoryTypeId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public double getTotalArea() {
        return TotalArea;
    }

    public void setTotalArea(double totalArea) {
        TotalArea = totalArea;
    }

    public int getTotalAreaSizeUnitId() {
        return TotalAreaSizeUnitId;
    }

    public void setTotalAreaSizeUnitId(int totalAreaSizeUnitId) {
        TotalAreaSizeUnitId = totalAreaSizeUnitId;
    }

    public int getBedrooms() {
        return Bedrooms;
    }

    public void setBedrooms(int bedrooms) {
        Bedrooms = bedrooms;
    }

    public int getFloors() {
        return Floors;
    }

    public void setFloors(int floors) {
        Floors = floors;
    }

    public double getCoverAreaSize() {
        return CoverAreaSize;
    }

    public void setCoverAreaSize(double coverAreaSize) {
        CoverAreaSize = coverAreaSize;
    }

    public int getCoverAreaSizeUnitId() {
        return CoverAreaSizeUnitId;
    }

    public void setCoverAreaSizeUnitId(int coverAreaSizeUnitId) {
        CoverAreaSizeUnitId = coverAreaSizeUnitId;
    }

    public double getSuperAreaSize() {
        return SuperAreaSize;
    }

    public void setSuperAreaSize(double superAreaSize) {
        SuperAreaSize = superAreaSize;
    }

    public int getSuperAreaSizeUnitId() {
        return SuperAreaSizeUnitId;
    }

    public void setSuperAreaSizeUnitId(int superAreaSizeUnitId) {
        SuperAreaSizeUnitId = superAreaSizeUnitId;
    }

    public double getBuiltUpAreaSize() {
        return BuiltUpAreaSize;
    }

    public void setBuiltUpAreaSize(double builtUpAreaSize) {
        BuiltUpAreaSize = builtUpAreaSize;
    }

    public int getBuiltUpAreaSizeUnitId() {
        return BuiltUpAreaSizeUnitId;
    }

    public void setBuiltUpAreaSizeUnitId(int builtUpAreaSizeUnitId) {
        BuiltUpAreaSizeUnitId = builtUpAreaSizeUnitId;
    }

    public double getCarpetAreaSize() {
        return CarpetAreaSize;
    }

    public void setCarpetAreaSize(double carpetAreaSize) {
        CarpetAreaSize = carpetAreaSize;
    }

    public int getCarpetAreaSizeUnitId() {
        return CarpetAreaSizeUnitId;
    }

    public void setCarpetAreaSizeUnitId(int carpetAreaSizeUnitId) {
        CarpetAreaSizeUnitId = carpetAreaSizeUnitId;
    }

    public int getParkings() {
        return Parkings;
    }

    public void setParkings(int parkings) {
        Parkings = parkings;
    }

    public ArrayList<AccomodationFilesModel> getAccomodationFilesModels() {
        return accomodationFilesModels;
    }

    public void setAccomodationFilesModels(ArrayList<AccomodationFilesModel> accomodationFilesModels) {
        this.accomodationFilesModels = accomodationFilesModels;
    }

    public static class AccomodationFilesModel {

        public int Id;
        public String AccomodationId;
        String FileId;
        String Category;
        String Accomodation;
        public FileModel fileModel;

        public String getFileId() {
            return FileId;
        }

        public FileModel getFileModel() {
            return fileModel;
        }

        public void setFileModel(FileModel fileModel) {
            this.fileModel = fileModel;
        }

        public String getAccomodationId() {
            return AccomodationId;
        }

        public void setAccomodationId(String accomodationId) {
            AccomodationId = accomodationId;
        }

        public void setFileId(String fileId) {
            FileId = fileId;
        }

        public String getAccomodation() {
            return Accomodation;
        }

        public void setAccomodation(String accomodation) {
            Accomodation = accomodation;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getCategory() {
            return Category;
        }

        public void setCategory(String category) {
            Category = category;
        }
    }

    public static class FileModel {

        int Id;
        public String Name;
        int ImageCode;
        String Url;
        String Type;
        String OriginalFileName;
        int CompanyId;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public int getImageCode() {
            return ImageCode;
        }

        public void setImageCode(int imageCode) {
            ImageCode = imageCode;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getOriginalFileName() {
            return OriginalFileName;
        }

        public void setOriginalFileName(String originalFileName) {
            OriginalFileName = originalFileName;
        }

        public int getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(int companyId) {
            CompanyId = companyId;
        }
    }
}
