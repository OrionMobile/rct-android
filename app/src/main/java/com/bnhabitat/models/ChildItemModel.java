package com.bnhabitat.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by gourav on 5/15/2018.
 */

public class ChildItemModel implements Parcelable {

    private String name;
    private String Description;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public static Creator<ChildItemModel> getCREATOR() {
        return CREATOR;
    }

    public ChildItemModel(Parcel in) {
        name = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChildItemModel(String name) {
        this.name = name;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChildItemModel> CREATOR = new Creator<ChildItemModel>() {
        @Override
        public ChildItemModel createFromParcel(Parcel in) {
            return new ChildItemModel(in);
        }

        @Override
        public ChildItemModel[] newArray(int size) {
            return new ChildItemModel[size];
        }
    };
//    private String name;
//    private String Description;
//
//
//
//
//
//
//
//    public String getName() {
//        return name;
//    }
//    public ChildItemModel(Parcel in) {
//        name = in.readString();
//    }
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getDescription() {
//        return Description;
//    }
//
//    public void setDescription(String description) {
//        Description = description;
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(name);
//    }
////
//    public static final Creator<ChildItemModel> CREATOR = new Creator<ChildItemModel>() {
//        @Override
//        public ChildItemModel createFromParcel(Parcel in) {
//            return new ChildItemModel(in);
//        }
//
//        @Override
//        public ChildItemModel[] newArray(int size) {
//            return new ChildItemModel[size];
//        }
//    };
}
