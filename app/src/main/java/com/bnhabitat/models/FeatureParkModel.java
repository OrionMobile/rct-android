package com.bnhabitat.models;

/**
 * Created by gourav on 3/26/2018.
 */

public class FeatureParkModel {
    String name;
    String type;
    String totalcount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(String totalcount) {
        this.totalcount = totalcount;
    }
}
