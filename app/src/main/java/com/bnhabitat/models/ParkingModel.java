package com.bnhabitat.models;

public class ParkingModel {

    String Id;
    String Name;
    String Type;
    int TotalCount;
    boolean IsDeletable;
    boolean IsModify;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(int totalCount) {
        TotalCount = totalCount;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }
}
