package com.bnhabitat.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gourav on 7/3/2017.
 */

public class StatusModel implements Serializable{
    String status;
    String id;
    String name;
    String description_name;
    String category_name;
    String capacity_size;
    String room_id;
    int pos;
    ArrayList<String >ids;
    List<String > room_ids;
    String firstname,lastname,phone_number,email;
    String open ;

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getCapacity_size() {
        return capacity_size;
    }

    public void setCapacity_size(String capacity_size) {
        this.capacity_size = capacity_size;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public List<String> getRoom_ids() {
        return room_ids;
    }

    public void setRoom_ids(List<String> room_ids) {
        this.room_ids = room_ids;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getDescription_name() {
        return description_name;
    }

    public void setDescription_name(String description_name) {
        this.description_name = description_name;
    }

    public ArrayList<String> getIds() {
        return ids;
    }

    public void setIds(ArrayList<String> ids) {
        this.ids = ids;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
