package com.bnhabitat.models;

/**
 * Created by gourav on 2/1/2018.
 */

public class TabFragment {
    String tabfragment;

    public String getTabfragment() {
        return tabfragment;
    }

    public void setTabfragment(String tabfragment) {
        this.tabfragment = tabfragment;
    }
}
