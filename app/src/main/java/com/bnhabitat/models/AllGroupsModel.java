package com.bnhabitat.models;

/**
 * Created by Android on 12/26/2017.
 */

public class AllGroupsModel {


    String Id;
    String name;
    String ParentGroupId;
    String IsPrivate;
    String GroupName;
    String Description;
    String GroupDetail;
    String OwnerUserId;

    OwnerUser ownerUser;


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentGroupId() {
        return ParentGroupId;
    }

    public void setParentGroupId(String parentGroupId) {
        ParentGroupId = parentGroupId;
    }

    public String getIsPrivate() {
        return IsPrivate;
    }

    public void setIsPrivate(String isPrivate) {
        IsPrivate = isPrivate;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getGroupDetail() {
        return GroupDetail;
    }

    public void setGroupDetail(String groupDetail) {
        GroupDetail = groupDetail;
    }

    public String getOwnerUserId() {
        return OwnerUserId;
    }

    public void setOwnerUserId(String ownerUserId) {
        OwnerUserId = ownerUserId;
    }

    public OwnerUser getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(OwnerUser ownerUser) {
        this.ownerUser = ownerUser;
    }

    public class OwnerUser {

        String Id;
        String UserName;
        String FirstName;
        String LastName;
        String Email;
        String Company;
        String ExistingImageUrl;
        String CompanyLogoUrl;
        String CompanyId;
        String Password;
        String IsLocked;
        String CreatedOn;
        String CreatedOnString;
        String UpdatedOn;
        String UpdatedBy;
        String IsDeleted;
        String UserGroups;
        String GroupId;
        String CompanyUser;


        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getCompany() {
            return Company;
        }

        public void setCompany(String company) {
            Company = company;
        }

        public String getExistingImageUrl() {
            return ExistingImageUrl;
        }

        public void setExistingImageUrl(String existingImageUrl) {
            ExistingImageUrl = existingImageUrl;
        }

        public String getCompanyLogoUrl() {
            return CompanyLogoUrl;
        }

        public void setCompanyLogoUrl(String companyLogoUrl) {
            CompanyLogoUrl = companyLogoUrl;
        }

        public String getCompanyId() {
            return CompanyId;
        }

        public void setCompanyId(String companyId) {
            CompanyId = companyId;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String password) {
            Password = password;
        }

        public String getIsLocked() {
            return IsLocked;
        }

        public void setIsLocked(String isLocked) {
            IsLocked = isLocked;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String createdOn) {
            CreatedOn = createdOn;
        }

        public String getCreatedOnString() {
            return CreatedOnString;
        }

        public void setCreatedOnString(String createdOnString) {
            CreatedOnString = createdOnString;
        }

        public String getUpdatedOn() {
            return UpdatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            UpdatedOn = updatedOn;
        }

        public String getUpdatedBy() {
            return UpdatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            UpdatedBy = updatedBy;
        }

        public String getIsDeleted() {
            return IsDeleted;
        }

        public void setIsDeleted(String isDeleted) {
            IsDeleted = isDeleted;
        }

        public String getUserGroups() {
            return UserGroups;
        }

        public void setUserGroups(String userGroups) {
            UserGroups = userGroups;
        }

        public String getGroupId() {
            return GroupId;
        }

        public void setGroupId(String groupId) {
            GroupId = groupId;
        }

        public String getCompanyUser() {
            return CompanyUser;
        }

        public void setCompanyUser(String companyUser) {
            CompanyUser = companyUser;
        }
    }
}
