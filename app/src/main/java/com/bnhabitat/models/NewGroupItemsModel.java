 package com.bnhabitat.models;

import java.io.Serializable;

 /**
  * Created by Android on 2/3/2017.
  */
 public class NewGroupItemsModel implements Serializable {

     String Id;

     String Name;

     String Description;

     String Slug;

     String TaxonomyId;


     public String getId() {
         return Id;
     }

     public void setId(String id) {
         Id = id;
     }

     public String getName() {
         return Name;
     }

     public void setName(String name) {
         Name = name;
     }

     public String getDescription() {
         return Description;
     }

     public void setDescription(String description) {
         Description = description;
     }

     public String getSlug() {
         return Slug;
     }

     public void setSlug(String slug) {
         Slug = slug;
     }

     public String getTaxonomyId() {
         return TaxonomyId;
     }

     public void setTaxonomyId(String taxonomyId) {
         TaxonomyId = taxonomyId;
     }
 }
