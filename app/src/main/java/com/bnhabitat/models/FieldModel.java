package com.bnhabitat.models;

/**
 * Created by gourav on 12/20/2017.
 */

public class FieldModel  {
    ResultData Result;

    public ResultData getResult() {
        return Result;
    }

    public void setResult(ResultData result) {
        Result = result;
    }

  public  class ResultData{
        String id;
        String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
