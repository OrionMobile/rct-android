package com.bnhabitat.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Result_ {
        private Integer id;
        private Object title;
        private Integer propertyTypeId;
        private Integer companyId;
        private Object cam;
        private Object isDraft;
        private Integer createdById;
        private Integer updatedById;
        private String createdOn;
        private String updatedOn;
        private List<InventoryModelProp> propertyLocations = null;
        private Object propertyType;
        private List<Object> propertyAreas = null;
        private List<Object> propertyPlcs = null;
        private List<Object> propertyImages = null;
        private List<Object> propertySides = null;
        private List<Object> propertyFinancials = null;
        private List<Object> propertyLoans = null;
        private List<Object> propertyOtherExpences = null;
        private List<Object> propertyOwners = null;
        private List<Object> propertyOtherQuestions = null;
        private List<Object> propertyLegalStatus = null;
        private List<Object> propertySpecifications = null;
        private List<Object> propertyBedrooms = null;
        private Integer totalRooms;
        private Integer totalBathrooms;
        private Integer totalParkings;
        private Integer totalKitchen;
        private List<Object> propertyAccommodationDetails = null;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();
        private ArrayList <PropertyAreasModel> propAreasList = new ArrayList<>();
        public Integer getId() {
            return id;
        }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public ArrayList<PropertyAreasModel> getPropAreasList() {
        return propAreasList;
    }

    public void setPropAreasList(ArrayList<PropertyAreasModel> propAreasList) {
        this.propAreasList = propAreasList;
    }

    public void setId(Integer id) {
            this.id = id;
        }

        public Object getTitle() {
            return title;
        }

        public void setTitle(Object title) {
            this.title = title;
        }

        public Integer getPropertyTypeId() {
            return propertyTypeId;
        }

        public void setPropertyTypeId(Integer propertyTypeId) {
            this.propertyTypeId = propertyTypeId;
        }

        public Integer getCompanyId() {
            return companyId;
        }

        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        public Object getCam() {
            return cam;
        }

        public void setCam(Object cam) {
            this.cam = cam;
        }

        public Object getIsDraft() {
            return isDraft;
        }

        public void setIsDraft(Object isDraft) {
            this.isDraft = isDraft;
        }

        public Integer getCreatedById() {
            return createdById;
        }

        public void setCreatedById(Integer createdById) {
            this.createdById = createdById;
        }

        public Integer getUpdatedById() {
            return updatedById;
        }

        public void setUpdatedById(Integer updatedById) {
            this.updatedById = updatedById;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public List<InventoryModelProp> getPropertyLocations() {
            return propertyLocations;
        }

        public void setPropertyLocations(List<InventoryModelProp> propertyLocations) {
            this.propertyLocations = propertyLocations;
        }

        public Object getPropertyType() {
            return propertyType;
        }

        public void setPropertyType(Object propertyType) {
            this.propertyType = propertyType;
        }

        public List<Object> getPropertyAreas() {
            return propertyAreas;
        }

        public void setPropertyAreas(List<Object> propertyAreas) {
            this.propertyAreas = propertyAreas;
        }

        public List<Object> getPropertyPlcs() {
            return propertyPlcs;
        }

        public void setPropertyPlcs(List<Object> propertyPlcs) {
            this.propertyPlcs = propertyPlcs;
        }

        public List<Object> getPropertyImages() {
            return propertyImages;
        }

        public void setPropertyImages(List<Object> propertyImages) {
            this.propertyImages = propertyImages;
        }

        public List<Object> getPropertySides() {
            return propertySides;
        }

        public void setPropertySides(List<Object> propertySides) {
            this.propertySides = propertySides;
        }

        public List<Object> getPropertyFinancials() {
            return propertyFinancials;
        }

        public void setPropertyFinancials(List<Object> propertyFinancials) {
            this.propertyFinancials = propertyFinancials;
        }

        public List<Object> getPropertyLoans() {
            return propertyLoans;
        }

        public void setPropertyLoans(List<Object> propertyLoans) {
            this.propertyLoans = propertyLoans;
        }

        public List<Object> getPropertyOtherExpences() {
            return propertyOtherExpences;
        }

        public void setPropertyOtherExpences(List<Object> propertyOtherExpences) {
            this.propertyOtherExpences = propertyOtherExpences;
        }

        public List<Object> getPropertyOwners() {
            return propertyOwners;
        }

        public void setPropertyOwners(List<Object> propertyOwners) {
            this.propertyOwners = propertyOwners;
        }

        public List<Object> getPropertyOtherQuestions() {
            return propertyOtherQuestions;
        }

        public void setPropertyOtherQuestions(List<Object> propertyOtherQuestions) {
            this.propertyOtherQuestions = propertyOtherQuestions;
        }

        public List<Object> getPropertyLegalStatus() {
            return propertyLegalStatus;
        }

        public void setPropertyLegalStatus(List<Object> propertyLegalStatus) {
            this.propertyLegalStatus = propertyLegalStatus;
        }

        public List<Object> getPropertySpecifications() {
            return propertySpecifications;
        }

        public void setPropertySpecifications(List<Object> propertySpecifications) {
            this.propertySpecifications = propertySpecifications;
        }

        public List<Object> getPropertyBedrooms() {
            return propertyBedrooms;
        }

        public void setPropertyBedrooms(List<Object> propertyBedrooms) {
            this.propertyBedrooms = propertyBedrooms;
        }

        public Integer getTotalRooms() {
            return totalRooms;
        }

        public void setTotalRooms(Integer totalRooms) {
            this.totalRooms = totalRooms;
        }

        public Integer getTotalBathrooms() {
            return totalBathrooms;
        }

        public void setTotalBathrooms(Integer totalBathrooms) {
            this.totalBathrooms = totalBathrooms;
        }

        public Integer getTotalParkings() {
            return totalParkings;
        }

        public void setTotalParkings(Integer totalParkings) {
            this.totalParkings = totalParkings;
        }

        public Integer getTotalKitchen() {
            return totalKitchen;
        }

        public void setTotalKitchen(Integer totalKitchen) {
            this.totalKitchen = totalKitchen;
        }

        public List<Object> getPropertyAccommodationDetails() {
            return propertyAccommodationDetails;
        }

        public void setPropertyAccommodationDetails(List<Object> propertyAccommodationDetails) {
            this.propertyAccommodationDetails = propertyAccommodationDetails;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
