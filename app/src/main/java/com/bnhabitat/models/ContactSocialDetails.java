package com.bnhabitat.models;

import java.io.Serializable;

public class ContactSocialDetails implements Serializable {

        int Id;
        int ContactId;
        String Detail;
        String Type;
        boolean IsDeletable;
        boolean IsModify;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getContactId() {
            return ContactId;
        }

        public void setContactId(int contactId) {
            ContactId = contactId;
        }

        public String getDetail() {
            return Detail;
        }

        public void setDetail(String detail) {
            Detail = detail;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public boolean isDeletable() {
            return IsDeletable;
        }

        public void setDeletable(boolean deletable) {
            IsDeletable = deletable;
        }

        public boolean isModify() {
            return IsModify;
        }

        public void setModify(boolean modify) {
            IsModify = modify;
        }


}
