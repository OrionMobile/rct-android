package com.bnhabitat.models;

import java.util.ArrayList;

public class SpecificationListModel {

    public int Id;
    public String Name;
    public String Description;
    public String Category;
    public String Capacity;
    public boolean IsDeletable;
    public boolean IsModify;

    public ArrayList<PropertyBedroom> propertyBedrooms = new ArrayList<>() ;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        Capacity = capacity;
    }

    public boolean isDeletable() {
        return IsDeletable;
    }

    public void setDeletable(boolean deletable) {
        IsDeletable = deletable;
    }

    public boolean isModify() {
        return IsModify;
    }

    public void setModify(boolean modify) {
        IsModify = modify;
    }

    public ArrayList<PropertyBedroom> getPropertyBedrooms() {
        return propertyBedrooms;
    }

    public void setPropertyBedrooms(ArrayList<PropertyBedroom> propertyBedrooms) {
        this.propertyBedrooms = propertyBedrooms;
    }

    public static class PropertyBedroom {

        public String Id;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }
    }
}

