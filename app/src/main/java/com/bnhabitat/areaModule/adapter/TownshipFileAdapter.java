package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.ViewPagerListener;
import com.bnhabitat.areaModule.model.ImageModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.areaModule.model.TownshipFilesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TownshipFileAdapter extends RecyclerView.Adapter<TownshipFileAdapter.ViewHolder> {

    Context context;
    ViewPagerListener mListener;

    ArrayList<TownshipFilesModel> pictures = new ArrayList<>();
    ArrayList<ImageModel> imageModels = new ArrayList<>();


    public TownshipFileAdapter(Context context, ArrayList<TownshipFilesModel> pictures, ViewPagerListener mListener) {
        this.context = context;
        this.pictures = pictures;
        this.mListener = mListener;
    }

    @Override
    public TownshipFileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.image_video_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TownshipFileAdapter.ViewHolder holder, final int position) {

        Picasso.with(context).load(pictures.get(position).getFile().getUrl().toString()).into(holder.img_video);
        holder.img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.setViewPager(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_video;

        public ViewHolder(View itemView) {
            super(itemView);

            img_video = (ImageView) itemView.findViewById(R.id.img_video);
        }
    }

}

