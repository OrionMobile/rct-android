package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.Developers;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Developer_list extends RecyclerView.Adapter<Developer_list.ViewHolder> {

    Context context;
    ArrayList<Developers> developer_list = new ArrayList<>();
    String view_data;

    public Developer_list(Context context, ArrayList<Developers> developer_list, String view_data) {
        this.context = context;
        this.developer_list = developer_list;
        this.view_data = view_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.developer_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.devNaming.setText(developer_list.get(position).getName());
        if(!developer_list.get(position).getLogoUrl().isEmpty())
            Picasso.with(context).load(developer_list.get(position).getLogoUrl()).into(holder.logoImage);
        else
            holder.logoImage.setImageResource(R.drawable.clipboard);
    }

    @Override
    public int getItemCount() {
        return developer_list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView developerCardView;
        TextView devNaming;
        ImageView logoImage;
        public ViewHolder(View itemView) {
            super(itemView);

            developerCardView = (CardView) itemView.findViewById(R.id.developerCardView);
            devNaming = (TextView) itemView.findViewById(R.id.devNaming);
            logoImage = (ImageView) itemView.findViewById(R.id.logoImage);
        }
    }
}
