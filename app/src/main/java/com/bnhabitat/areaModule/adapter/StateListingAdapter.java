package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.State_Info;
import com.bnhabitat.areaModule.model.StateListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class StateListingAdapter extends RecyclerView.Adapter<StateListingAdapter.ViewHolder>{

    private Context context;
    ArrayList<StateListModel> stateList = new ArrayList<>();

    public StateListingAdapter(Context context, ArrayList<StateListModel> stateList) {
        this.context = context;
        this.stateList = stateList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.area_state_design, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.stateNaming.setText(stateList.get(position).getName().toString().trim());
        if(stateList.get(position).getPictures().size()>0 && !stateList.get(position).getPictures().get(0).getUrl().equalsIgnoreCase(""))
            Picasso.with(context).load(stateList.get(position).getPictures().get(0).getUrl().toString()).into(holder.stateImage);
        else
            holder.stateImage.setImageResource(R.drawable.image_logo);
        holder.cityNumber.setText(stateList.get(position).getDistrictCount().toString().trim());
        holder.propertyNumber.setText(stateList.get(position).getTotalProperties().toString().trim());
        holder.stateCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, State_Info.class);
                intent.putExtra("Id", stateList.get(position).getId().toString());
                intent.putExtra("Name", stateList.get(position).getName().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stateList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView stateCardView;
        ImageView stateImage;
        TextView stateNaming, cityNumber, propertyNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            stateCardView = (CardView) itemView.findViewById(R.id.stateCardView);
            stateImage = (ImageView) itemView.findViewById(R.id.stateImage);
            stateNaming = (TextView) itemView.findViewById(R.id.stateNaming);
            cityNumber = (TextView) itemView.findViewById(R.id.cityNumber);
            propertyNumber = (TextView) itemView.findViewById(R.id.propertyNumber);
        }
    }
}
