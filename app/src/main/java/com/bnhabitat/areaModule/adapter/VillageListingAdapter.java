package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.VillageDetail;
import com.bnhabitat.areaModule.model.VillageListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VillageListingAdapter extends RecyclerView.Adapter<VillageListingAdapter.ViewHolder> {

    private Context context;
    ArrayList<VillageListModel> districtList = new ArrayList<>();

    public VillageListingAdapter(Context context, ArrayList<VillageListModel> districtList) {
        this.context = context;
        this.districtList = districtList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.area_district_design, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.districtNaming.setText(districtList.get(position).getName().toString().trim());
        if(districtList.get(position).getPictures().size()>0 && !districtList.get(position).getPictures().get(0).getUrl().equalsIgnoreCase("")){
            Picasso.with(context).load(districtList.get(position).getPictures().get(0).getUrl().toString()).into(holder.districtImage);
        }else{
            holder.districtImage.setImageResource(R.drawable.image_logo);
        }

        holder.districtCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, VillageDetail.class);
                intent.putExtra("VillageId", districtList.get(position).getId());
                intent.putExtra("VillageName", districtList.get(position).getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return districtList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView districtCardView;
        ImageView districtImage;
        TextView districtNaming, townshipNumber, localityNumber, projectNumber, propertyNumber;
        TextView township, locality, project, property;

        public ViewHolder(View itemView) {
            super(itemView);
            districtCardView = (CardView) itemView.findViewById(R.id.districtCardView);
            districtImage = (ImageView) itemView.findViewById(R.id.districtImage);
            districtNaming = (TextView) itemView.findViewById(R.id.districtNaming);
            townshipNumber = (TextView) itemView.findViewById(R.id.townshipNumber);
            townshipNumber.setVisibility(View.GONE);
            localityNumber = (TextView) itemView.findViewById(R.id.localityNumber);
            localityNumber.setVisibility(View.GONE);
            projectNumber = (TextView) itemView.findViewById(R.id.projectNumber);
            projectNumber.setVisibility(View.GONE);
            propertyNumber = (TextView) itemView.findViewById(R.id.propertyNumber);
            propertyNumber.setVisibility(View.GONE);
            township = (TextView) itemView.findViewById(R.id.township);
            township.setVisibility(View.GONE);
            locality = (TextView) itemView.findViewById(R.id.locality);
            locality.setVisibility(View.GONE);
            project = (TextView) itemView.findViewById(R.id.project);
            project.setVisibility(View.GONE);
            property = (TextView) itemView.findViewById(R.id.property);
            property.setVisibility(View.GONE);
        }
    }
}
