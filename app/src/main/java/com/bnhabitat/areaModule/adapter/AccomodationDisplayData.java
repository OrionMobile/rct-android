package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.ProjectCategoryTypeModel;

import java.util.ArrayList;

public class AccomodationDisplayData extends RecyclerView.Adapter<AccomodationDisplayData.ViewHolder> {

    Context context;
    ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationRooms> rooms = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationFloors> floors = new ArrayList<>();

    public AccomodationDisplayData(Context context, ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationRooms> rooms) {
        this.context = context;
        this.rooms = rooms;
    }

    public AccomodationDisplayData( ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationFloors> floors, Context context) {
        this.context = context;
        this.floors = floors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.accomodation_room_floor, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(floors.size()!=0){
            holder.name.setText(floors.get(position).getFloorNumber());
            holder.category.setVisibility(View.GONE);
        }
        else if(rooms.size()!=0){
            holder.name.setText(rooms.get(position).getBedRoomName());
            holder.category.setText(rooms.get(position).getCategory());
        }
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(rooms.size()!=0)
            size = rooms.size();
        else
            size = floors.size();
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, category;
        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            category = (TextView) itemView.findViewById(R.id.category);
        }
    }
}
