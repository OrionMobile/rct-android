package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;

import java.util.ArrayList;

public class LandAdapter extends RecyclerView.Adapter<LandAdapter.ViewHolder> {
    Context context;
    ArrayList<String> landname = new ArrayList<>();
    ArrayList<String> landunits = new ArrayList<>();

    public LandAdapter(Context context, ArrayList<String> landname, ArrayList<String> landunits) {
        this.context = context;
        this.landname = landname;
        this.landunits = landunits;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.land_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.land.setText(landname.get(position));
        holder.units.setText(landunits.get(position));
    }

    @Override
    public int getItemCount() {
        return landname.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView land, units;
        public ViewHolder(View itemView) {
            super(itemView);

            land = (TextView) itemView.findViewById(R.id.land);
            units = (TextView) itemView.findViewById(R.id.units);
        }
    }
}
