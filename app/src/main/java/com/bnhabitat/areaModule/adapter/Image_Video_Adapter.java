package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.TownshipDetail;
import com.bnhabitat.areaModule.areaActivities.ViewPagerListener;
import com.bnhabitat.areaModule.model.ImageModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Image_Video_Adapter extends RecyclerView.Adapter<Image_Video_Adapter.ViewHolder> {

    Context context;
    ViewPagerListener mListener;

    ArrayList<StateListModel.Pictures> pictures = new ArrayList<>();
    ArrayList<ImageModel> pictures1 = new ArrayList<>();

    public Image_Video_Adapter(Context context, ArrayList<StateListModel.Pictures> pictures, ViewPagerListener mListener) {
        this.context = context;
        this.pictures = pictures;
        this.mListener = mListener;
    }

    public Image_Video_Adapter(Context context, ViewPagerListener mListener, ArrayList<ImageModel> pictures1) {
        this.context = context;
        this.mListener = mListener;
        this.pictures1 = pictures1;
    }

    @Override
    public Image_Video_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.image_video_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Image_Video_Adapter.ViewHolder holder, final int position) {
        if(pictures.size()!=0) {
            for (int i = 0; i < pictures.size(); i++)
                Picasso.with(context).load(pictures.get(position).getUrl().toString()).into(holder.img_video);
        }
        else if(pictures1.size()!=0){
            for(int i = 0;i <pictures1.size(); i++)
                Picasso.with(context).load(pictures1.get(position).getFileUrl().toString()).into(holder.img_video);

        }
        holder.img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.setViewPager(position);
            }
        });
    }

    @Override
    public int getItemCount() {

        int size = 0;
        if(pictures.size()!=0)
            size = pictures.size();
        else
            size = pictures1.size();

        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_video;

        public ViewHolder(View itemView) {
            super(itemView);

            img_video = (ImageView) itemView.findViewById(R.id.img_video);
        }
    }
    
}
