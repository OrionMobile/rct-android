package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.DistrictDetailing;
import com.bnhabitat.areaModule.model.DistrictListModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DistrictListingAdapter extends RecyclerView.Adapter<DistrictListingAdapter.ViewHolder> {

    private Context context;
    ArrayList<DistrictListModel> districtList = new ArrayList<>();

    public DistrictListingAdapter(Context context, ArrayList<DistrictListModel> districtList) {
        this.context = context;
        this.districtList = districtList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.area_district_design, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.districtNaming.setText(districtList.get(position).getDistrict().toString().trim());
        if(districtList.get(position).getPictures().size()>0 && !districtList.get(position).getPictures().get(0).getUrl().equalsIgnoreCase("")){
            Picasso.with(context).load(districtList.get(position).getPictures().get(0).getUrl().toString()).into(holder.districtImage);
        }else{
            holder.districtImage.setImageResource(R.drawable.image_logo);
        }
        holder.townshipNumber.setText(districtList.get(position).getTownships().toString());
        holder.localityNumber.setText(districtList.get(position).getLocalities().toString());
        holder.propertyNumber.setText(districtList.get(position).getProperties().toString());
        holder.projectNumber.setText(districtList.get(position).getProjects().toString());

        holder.districtCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DistrictDetailing.class);
                intent.putExtra("stateId", districtList.get(position).getStateId());
                intent.putExtra("districtId", districtList.get(position).getId());
                intent.putExtra("Name", districtList.get(position).getDistrict().toString().trim());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return districtList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView districtCardView;
        ImageView districtImage;
        TextView districtNaming, townshipNumber, localityNumber, projectNumber, propertyNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            districtCardView = (CardView) itemView.findViewById(R.id.districtCardView);
            districtImage = (ImageView) itemView.findViewById(R.id.districtImage);
            districtNaming = (TextView) itemView.findViewById(R.id.districtNaming);
            townshipNumber = (TextView) itemView.findViewById(R.id.townshipNumber);
            localityNumber = (TextView) itemView.findViewById(R.id.localityNumber);
            projectNumber = (TextView) itemView.findViewById(R.id.projectNumber);
            propertyNumber = (TextView) itemView.findViewById(R.id.propertyNumber);
        }
    }
}
