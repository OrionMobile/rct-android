package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bnhabitat.areaModule.model.ImageModel;
import com.bnhabitat.areaModule.model.TownshipFilesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TownshipImageAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<TownshipFilesModel> image_list = new ArrayList<>();

    public TownshipImageAdapter(Context mContext, ArrayList<TownshipFilesModel> image_list) {
        this.context = mContext;
        this.image_list = image_list;
    }

    @Override
    public int getCount() {
        return image_list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        Picasso.with(context).load(image_list.get(position).getFile().getUrl().toString()).into(imageView);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}
