package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;

import java.util.ArrayList;

public class Gallery_Image_Adapter extends RecyclerView.Adapter<Gallery_Image_Adapter.ViewHolder> {

    Context context;
    ArrayList<String> gallery_naming = new ArrayList<>();

    public Gallery_Image_Adapter(Context context, ArrayList<String> gallery_naming) {
        this.context = context;
        this.gallery_naming = gallery_naming;
    }

    public Gallery_Image_Adapter() {
    }

    @Override
    public Gallery_Image_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.gallery_list_township, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Gallery_Image_Adapter.ViewHolder holder, int position) {
        holder.planNaming.setText(gallery_naming.get(position).toString().trim());
    }

    @Override
    public int getItemCount() {
        return gallery_naming.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView planNaming;

        public ViewHolder(View itemView) {
            super(itemView);
            planNaming = (TextView) itemView.findViewById(R.id.planNaming);
        }
    }
}
