package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.TownshipDetail;
import com.bnhabitat.areaModule.fragment.Township_Detail;
import com.bnhabitat.areaModule.model.TownshipListingModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TownshipListingAdapter extends RecyclerView.Adapter<TownshipListingAdapter.ViewHolder> {

    Context context;
    ArrayList<TownshipListingModel> townshipListingArrayList = new ArrayList<>();
    int projectCount = 0;
    int propertyCount = 0;

    public TownshipListingAdapter(Context context, ArrayList<TownshipListingModel> townshipListingArrayList) {
        this.context = context;
        this.townshipListingArrayList = townshipListingArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_row_township_list, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.townshipNaming.setText(townshipListingArrayList.get(position).getName());

        projectCount = 0;
        propertyCount = 0;
        for(int i = 0; i< townshipListingArrayList.get(position).getProjects().size(); i++){
            projectCount = townshipListingArrayList.get(position).getProjects().size();

            if(projectCount>0)
                propertyCount = propertyCount + townshipListingArrayList.get(position).getProjects().get(i).getProjectCategoryTypes().size();
            else
                propertyCount = 0;
        }

        holder.projectNumber.setText(String.valueOf(projectCount));
        holder.propertyNumber.setText(String.valueOf(propertyCount));

        for(int i = 0; i < townshipListingArrayList.get(position).getTownshipFiles().size(); i++){
            if(townshipListingArrayList.get(position).getTownshipFiles().get(i).getCategory().equalsIgnoreCase("Photos")) {
                Picasso.with(context).load(townshipListingArrayList.get(position).getTownshipFiles().get(i).getFiles().getUrl()).into(holder.townshipImage);
                break;
            }
            else
               holder.townshipImage.setImageResource(R.drawable.image_logo);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TownshipDetail.class);
                intent.putExtra("TownshipId", townshipListingArrayList.get(position).getId());
                intent.putExtra("TownshipName", townshipListingArrayList.get(position).getName());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return townshipListingArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView townshipImage;
        TextView townshipNaming, projectNumber, propertyNumber;

        public ViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.townshipCardView);
            townshipImage = (ImageView) itemView.findViewById(R.id.townshipImage);
            townshipNaming = (TextView) itemView.findViewById(R.id.townshipNaming);
            projectNumber = (TextView) itemView.findViewById(R.id.projectNumber);
            propertyNumber = (TextView) itemView.findViewById(R.id.propertyNumber);
        }
    }
}
