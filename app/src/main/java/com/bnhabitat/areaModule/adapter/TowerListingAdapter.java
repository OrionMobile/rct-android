package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;

import java.util.ArrayList;

public class TowerListingAdapter extends RecyclerView.Adapter<TowerListingAdapter.ViewHolder> {

    Context context;
    ArrayList<String> towerList = new ArrayList<>();
    ArrayList<String> floorListing = new ArrayList<>();
    ArrayList<String> roomListing = new ArrayList<>();

    public TowerListingAdapter(Context context, ArrayList<String> towerList, ArrayList<String> floorListing, ArrayList<String> roomListing) {
        this.context = context;
        this.towerList = towerList;
        this.floorListing = floorListing;
        this.roomListing = roomListing;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.tower_listing_area_module, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.towerName.setText(towerList.get(position));
        holder.expandLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.expandLayout.setVisibility(View.GONE);
                holder.contractLayout.setVisibility(View.VISIBLE);
                holder.towerDetail.setVisibility(View.VISIBLE);
                holder.towerName.setTextColor(ContextCompat.getColor(context, R.color.black_dark));

            }
        });

        holder.contractLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.contractLayout.setVisibility(View.GONE);
                holder.expandLayout.setVisibility(View.VISIBLE);
                holder.towerDetail.setVisibility(View.GONE);
                holder.towerName.setTextColor(ContextCompat.getColor(context, R.color.blue));


            }
        });

        FloorListingAdapter floorListingAdapter = new FloorListingAdapter(context, floorListing, roomListing);
        holder.floorList.setLayoutManager(new LinearLayoutManager(context));
        holder.floorList.setAdapter(floorListingAdapter);
        floorListingAdapter.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return towerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView expandLayout, contractLayout, location;
        TextView towerName;
        RecyclerView floorList;
        LinearLayout towerDetail;

        public ViewHolder(View itemView) {
            super(itemView);
            expandLayout = (ImageView) itemView.findViewById(R.id.expandLayout);
            contractLayout = (ImageView) itemView.findViewById(R.id.contractLayout);
            location = (ImageView) itemView.findViewById(R.id.location);
            towerName =(TextView) itemView.findViewById(R.id.towerName);
            floorList =(RecyclerView) itemView.findViewById(R.id.floorList);
            towerDetail =(LinearLayout) itemView.findViewById(R.id.towerDetail);
        }
    }
}
