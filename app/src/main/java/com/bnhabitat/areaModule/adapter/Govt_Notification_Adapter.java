package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;

import java.util.ArrayList;

public class Govt_Notification_Adapter extends RecyclerView.Adapter<Govt_Notification_Adapter.ViewHolder>{

    Context context;
    ArrayList<String> govt_notify = new ArrayList<>();

    public Govt_Notification_Adapter(Context context, ArrayList<String> govt_notify) {
        this.context = context;
        this.govt_notify = govt_notify;
    }

    @Override
    public Govt_Notification_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.govt_notification_design_township, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Govt_Notification_Adapter.ViewHolder holder, int position) {
        holder.govt_notification_rep.setText(govt_notify.get(position));
    }

    @Override
    public int getItemCount() {
        return govt_notify.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView govt_notification_rep;
        public ViewHolder(View itemView) {
            super(itemView);

            govt_notification_rep = (TextView) itemView.findViewById(R.id.govt_notification_rep);
        }
    }
}
