package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.PropertyDetail;
import com.bnhabitat.models.InventoryModel_Area;
import com.bnhabitat.ui.activities.PropertyDetailPageActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PropertyListingAdapter extends RecyclerView.Adapter<PropertyListingAdapter.ViewHolder> {

    Context context;
    private ArrayList<InventoryModel_Area> inventoryModels = new ArrayList();
    private ArrayList<InventoryModel_Area.PropertyLocation> propertyLocations = new ArrayList();
    String view_data;
    int room_count = 0, kitchen_count = 0;

    String FloorNo = "", TowerName = "", State = "", UnitNo = "";

    public PropertyListingAdapter(Context context, ArrayList<InventoryModel_Area> inventoryModels, String view_data) {
        this.context = context;
        this.inventoryModels = inventoryModels;
        this.view_data = view_data;
    }

    @Override
    public PropertyListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (view_data.equalsIgnoreCase("")) {
            view = LayoutInflater.from(context).inflate(R.layout.for_you_list_items, parent, false);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.for_you_list_view_all, parent, false);
        }

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PropertyListingAdapter.ViewHolder holder, final int position) {
        propertyLocations = inventoryModels.get(position).getPropertyLocations();

        if(propertyLocations.get(0).getFloorNo().equalsIgnoreCase("null"))
            FloorNo = "";
        else
            FloorNo = propertyLocations.get(0).getFloorNo();

        if(propertyLocations.get(0).getState().equalsIgnoreCase("null"))
            State = "";
        else
            State = propertyLocations.get(0).getState();

        if(propertyLocations.get(0).getTowerName().equalsIgnoreCase("null"))
            TowerName = "";
        else
            TowerName = propertyLocations.get(0).getTowerName();

        if(propertyLocations.get(0).getUnitName().equalsIgnoreCase("null"))
            UnitNo = "";
        else
            UnitNo = propertyLocations.get(0).getUnitName();

        holder.property_type.setText(inventoryModels.get(position).getTypeobject() + " " + inventoryModels.get(position).getNameobject());

        Picasso.with(context).load(inventoryModels.get(position).getPropertyLocations().get(0).getDeveloperLogo()).into(holder.logo);
        holder.project_name.setText(inventoryModels.get(position).getPropertyLocations().get(0).getDeveloper());

        holder.locality.setText(UnitNo + " " + FloorNo + " " + TowerName + " " + State);

        for (int i = 0; i < inventoryModels.get(position).getPropertyBedroomses().size(); i++) {
            if (inventoryModels.get(position).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("room")) {
                room_count = room_count + 1;
            }
            if (inventoryModels.get(position).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("kitchen")) {
                kitchen_count = kitchen_count + 1;
            }
        }

        try {
            holder.bedroom.setText("" + room_count);
            holder.kitchen.setText("" + kitchen_count);

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            for (int index = 0; index < inventoryModels.get(position).getPropertyAccommodationDetailses().size(); index++) {
                if (inventoryModels.get(position).getPropertyAccommodationDetailses().get(index).getType().equalsIgnoreCase("Parking")) {
                    holder.parking.setText(inventoryModels.get(position).getPropertyAccommodationDetailses().get(index).getTotalCount());

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            double price_txt = Double.parseDouble(String.valueOf(inventoryModels.get(position).getPropertyFinancialses().get(0).getDemandPrice()));
            holder.price.setText(Utils.getConvertedPrice((long) price_txt, context));

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            holder.area_unit.setText(inventoryModels.get(position).getPropertyAreas().get(0).getPlotArea() + " " + inventoryModels.get(position).getPropertyAreas().get(0).getPropertySizeUnit4());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();

        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }

        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();

        if (!inventoryModels.get(position).getPropertyImages().isEmpty())
            imageLoader.displayImage(inventoryModels.get(position).getPropertyImages().get(0).getUrl(), holder.project_image, options);

        holder.full_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PropertyDetail.class);
                intent.putExtra(Constants.PROPERTY_ID, inventoryModels.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return inventoryModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView project_name, property_type, area_unit, price, locality, parking, kitchen, bedroom, bathroom;
        ImageView edit_property, more_details, share_property, project_image, logo;
        RelativeLayout full_lay;

        public ViewHolder(View itemView) {
            super(itemView);

            property_type = (TextView) itemView.findViewById(R.id.property_type);
            area_unit = (TextView) itemView.findViewById(R.id.area_unit);
            price = (TextView) itemView.findViewById(R.id.price);
            project_name = (TextView) itemView.findViewById(R.id.project_name);
            locality = (TextView) itemView.findViewById(R.id.locality);
            parking = (TextView) itemView.findViewById(R.id.parking);
            kitchen = (TextView) itemView.findViewById(R.id.kitchen);
            bedroom = (TextView) itemView.findViewById(R.id.bedroom);
            bathroom = (TextView) itemView.findViewById(R.id.bathroom);
            project_image = (ImageView) itemView.findViewById(R.id.project_image);
            edit_property = (ImageView) itemView.findViewById(R.id.edit_property);
            more_details = (ImageView) itemView.findViewById(R.id.more_details);
            logo = (ImageView) itemView.findViewById(R.id.logo);
            share_property = (ImageView) itemView.findViewById(R.id.share_property);
            full_lay = (RelativeLayout) itemView.findViewById(R.id.full_lay);
        }
    }
}
