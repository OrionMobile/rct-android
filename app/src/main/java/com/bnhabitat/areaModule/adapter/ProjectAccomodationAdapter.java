package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.ProjectCategoryTypeModel;

import java.util.ArrayList;

public class ProjectAccomodationAdapter extends RecyclerView.Adapter<ProjectAccomodationAdapter.ViewHolder> {

    Context context;
    ArrayList<ProjectCategoryTypeModel.Accomodations> accomodationsArrayList = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationRooms> accommodationRooms = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationFloors> accommodationFloors = new ArrayList<>();

    public ProjectAccomodationAdapter(Context context, ArrayList<ProjectCategoryTypeModel.Accomodations> accomodationsArrayList, ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationRooms> accommodationRooms, ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationFloors> accommodationFloors) {
        this.context = context;
        this.accomodationsArrayList = accomodationsArrayList;
        this.accommodationRooms = accommodationRooms;
        this.accommodationFloors = accommodationFloors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.accomodation_view_project, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        if(accomodationsArrayList.size()==0){
//            holder.no_data.setVisibility(View.VISIBLE);
//            holder.accomodationRoom.setVisibility(View.GONE);
//            holder.accomodationFloor.setVisibility(View.GONE);
//        }
//        else {
//            holder.no_data.setVisibility(View.GONE);

            if (accommodationRooms.size() != 0) {
                holder.accomodationRoom.setVisibility(View.VISIBLE);
                AccomodationDisplayData accomodationDisplayData = new AccomodationDisplayData(context, accomodationsArrayList.get(position).getAccommodationRooms());
                holder.accomodation_room.setLayoutManager(new LinearLayoutManager(context));
                holder.accomodation_room.setAdapter(accomodationDisplayData);
                accomodationDisplayData.notifyDataSetChanged();
            } else
                holder.accomodationRoom.setVisibility(View.GONE);

            if (accommodationFloors.size() != 0) {
                holder.accomodationFloor.setVisibility(View.VISIBLE);
                AccomodationDisplayData accomodationDisplayData = new AccomodationDisplayData(accommodationFloors, context);
                holder.accomodation_floors.setLayoutManager(new LinearLayoutManager(context));
                holder.accomodation_floors.setAdapter(accomodationDisplayData);
                accomodationDisplayData.notifyDataSetChanged();
            } else
                holder.accomodationFloor.setVisibility(View.GONE);
//        }
    }

    @Override
    public int getItemCount() {
        return accomodationsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout accomodationRoom, accomodationFloor;
        RecyclerView accomodation_room, accomodation_floors;
        TextView no_data;

        public ViewHolder(View itemView) {
            super(itemView);

            accomodationRoom = (LinearLayout) itemView.findViewById(R.id.accomodationRoom);
            accomodationFloor = (LinearLayout) itemView.findViewById(R.id.accomodationFloor);
            accomodation_room = (RecyclerView) itemView.findViewById(R.id.accomodation_room);
            accomodation_floors = (RecyclerView) itemView.findViewById(R.id.accomodation_floors);
            no_data= (TextView) itemView.findViewById(R.id.no_data);
        }
    }
}
