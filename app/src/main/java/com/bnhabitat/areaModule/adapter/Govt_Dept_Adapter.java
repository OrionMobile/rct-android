package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.Departments;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Govt_Dept_Adapter extends RecyclerView.Adapter<Govt_Dept_Adapter.ViewHolder>{

    Context context;
    ArrayList<Departments> govt_detailing_list = new ArrayList<>();
    String unitname, towername, subareaname, townname, localityname, township, villagename, tehsilname, zipcode, district, state, country;

    public Govt_Dept_Adapter(Context context, ArrayList<Departments> govt_detailing_list) {
        this.context = context;
        this.govt_detailing_list = govt_detailing_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.govt_developer_rep, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.devNaming.setText(govt_detailing_list.get(position).getName());
        if(!govt_detailing_list.get(position).getLogoUrl().isEmpty() && !govt_detailing_list.get(position).getLogoUrl().equalsIgnoreCase("")){
            Picasso.with(context).load(govt_detailing_list.get(position).getLogoUrl()).into(holder.logoImage);
        }
        else
            holder.logoImage.setImageResource(R.drawable.clipboard);
        if(!govt_detailing_list.get(position).getIndustry().equalsIgnoreCase("") && !govt_detailing_list.get(position).getIndustry().equalsIgnoreCase(null))
            holder.industry.setText("("+govt_detailing_list.get(position).getIndustry()+")");
        else
            holder.industry.setVisibility(View.GONE);
        holder.website_name.setText(govt_detailing_list.get(position).getWebsite());

        if(govt_detailing_list.get(position).getAddresses().get(0).getUnitName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getUnitName().equalsIgnoreCase(""))
            unitname = "";
        else
            unitname = govt_detailing_list.get(position).getAddresses().get(0).getUnitName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getTowerName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getTowerName().equalsIgnoreCase(""))
            towername = "";
        else
            towername = govt_detailing_list.get(position).getAddresses().get(0).getTowerName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getSubAreaName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getSubAreaName().equalsIgnoreCase(""))
            subareaname = "";
        else
            subareaname = govt_detailing_list.get(position).getAddresses().get(0).getSubAreaName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getTownName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getTownName().equalsIgnoreCase(""))
            townname = "";
        else
            townname = govt_detailing_list.get(position).getAddresses().get(0).getTownName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getLocalityName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getLocalityName().equalsIgnoreCase(""))
            localityname = "";
        else
            localityname = govt_detailing_list.get(position).getAddresses().get(0).getLocalityName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getTownshipName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getTownshipName().equalsIgnoreCase(""))
            township = "";
        else
            township = govt_detailing_list.get(position).getAddresses().get(0).getTownshipName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getVillageName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getVillageName().equalsIgnoreCase(""))
            villagename = "";
        else
            villagename = govt_detailing_list.get(position).getAddresses().get(0).getVillageName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getTehsilName().equalsIgnoreCase("null") ||
        govt_detailing_list.get(position).getAddresses().get(0).getTehsilName().equalsIgnoreCase(""))
            tehsilname = "";
        else
            tehsilname = govt_detailing_list.get(position).getAddresses().get(0).getTehsilName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getZipCode().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getZipCode().equalsIgnoreCase("null"))
            zipcode = "";
        else
            zipcode = govt_detailing_list.get(position).getAddresses().get(0).getZipCode();

        if(govt_detailing_list.get(position).getAddresses().get(0).getDistrictName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getDistrictName().equalsIgnoreCase("null"))
            district = "";
        else
            district = govt_detailing_list.get(position).getAddresses().get(0).getDistrictName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getStateName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getStateName().equalsIgnoreCase("null"))
            state = "";
        else
            state = govt_detailing_list.get(position).getAddresses().get(0).getStateName();

        if(govt_detailing_list.get(position).getAddresses().get(0).getCountryName().equalsIgnoreCase("null") ||
                govt_detailing_list.get(position).getAddresses().get(0).getCountryName().equalsIgnoreCase("null"))
            country = "";
        else
            country = govt_detailing_list.get(position).getAddresses().get(0).getCountryName();


        holder.locationName.setText(
        unitname + " " + towername + ",\n" +
        subareaname + ", " + townname + ", " + localityname + ",\n" +
        township + ", " + villagename +", " + tehsilname + ",\n" +
        district + ", " + state + " - " + zipcode + ",\n" +
        country);


    }

    @Override
    public int getItemCount() {
        return govt_detailing_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView govtCardView;
        TextView devNaming, locationName, website_name, industry;
        ImageView logoImage;

        public ViewHolder(View itemView) {
            super(itemView);
            govtCardView = (CardView) itemView.findViewById(R.id.govtCardView);
            devNaming = (TextView) itemView.findViewById(R.id.devNaming);
            locationName = (TextView) itemView.findViewById(R.id.locationName);
            website_name = (TextView) itemView.findViewById(R.id.website_name);
            industry = (TextView) itemView.findViewById(R.id.industry);
            logoImage = (ImageView) itemView.findViewById(R.id.logoImage);
        }
    }
}
