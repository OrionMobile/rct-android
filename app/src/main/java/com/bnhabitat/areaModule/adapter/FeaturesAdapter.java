package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.KeydistanceModel;
import com.bnhabitat.areaModule.model.ProjectAmenityModel;

import java.util.ArrayList;

public class FeaturesAdapter extends RecyclerView.Adapter<FeaturesAdapter.ViewHolder> {

    Context context;
    ArrayList<KeydistanceModel> features_data = new ArrayList<>();
    ArrayList<ProjectAmenityModel.Amenity> amenities = new ArrayList<>();
    String abc = "";

    public FeaturesAdapter(Context context, ArrayList<KeydistanceModel> features_data) {
        this.context = context;
        this.features_data = features_data;
    }

    public FeaturesAdapter(Context context, ArrayList<ProjectAmenityModel.Amenity> amenities, String abc) {
        this.context = context;
        this.amenities = amenities;
        this.abc = abc;
    }

    @Override
    public FeaturesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.feature_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FeaturesAdapter.ViewHolder holder, int position) {
        if(features_data.size()!=0) {
            holder.name.setText(features_data.get(position).getKey());
            holder.dist.setText(features_data.get(position).getValue());
            holder.dist_unit.setText(features_data.get(position).getValue1());
            holder.walk_time.setText(features_data.get(position).getValue2());
        } else if(amenities.size()!=0){
            holder.name.setText(amenities.get(position).getTitle());
            holder.dist.setText(amenities.get(position).getSize());
            holder.dist_unit.setText(amenities.get(position).getSizeUnit());
            holder.walkingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(features_data.size()!=0)
            size = features_data.size();
        if(amenities.size()!=0)
            size = amenities.size();
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout feature_layout;
        RelativeLayout walkingLayout;
        TextView name, dist, dist_unit, walk_time;

        public ViewHolder(View itemView) {
            super(itemView);

            feature_layout = (LinearLayout) itemView.findViewById(R.id.feature_layout);
            walkingLayout = (RelativeLayout) itemView.findViewById(R.id.walkingLayout);
            name = (TextView) itemView.findViewById(R.id.name);
            dist = (TextView) itemView.findViewById(R.id.dist);
            dist_unit = (TextView) itemView.findViewById(R.id.dist_unit);
            walk_time = (TextView) itemView.findViewById(R.id.walk_time);
        }
    }
}
