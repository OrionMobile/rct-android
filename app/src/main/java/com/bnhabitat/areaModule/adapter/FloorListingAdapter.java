package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;

import java.util.ArrayList;

public class FloorListingAdapter extends RecyclerView.Adapter<FloorListingAdapter.ViewHolder> {

    Context context;
    ArrayList<String> floorList = new ArrayList<>();
    ArrayList<String> roomListing = new ArrayList<>();

    public FloorListingAdapter(Context context, ArrayList<String> floorList, ArrayList<String> roomListing) {
        this.context = context;
        this.floorList = floorList;
        this.roomListing = roomListing;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.floor_listing_area_module, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.floorName.setText(floorList.get(position));
        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.less.setVisibility(View.VISIBLE);
                holder.more.setVisibility(View.GONE);
                holder.floorDetail.setVisibility(View.VISIBLE);
                holder.floorName.setTextColor(ContextCompat.getColor(context, R.color.orange));
            }
        });

        holder.less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.more.setVisibility(View.VISIBLE);
                holder.less.setVisibility(View.GONE);
                holder.floorDetail.setVisibility(View.GONE);
                holder.floorName.setTextColor(ContextCompat.getColor(context, R.color.blue));

            }
        });

        RoomListingAdapter roomListingAdapter = new RoomListingAdapter(context, roomListing);
        holder.roomList.setLayoutManager(new LinearLayoutManager(context));
        holder.roomList.setAdapter(roomListingAdapter);
        roomListingAdapter.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return floorList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView floorName;
        ImageView more, less;
        RecyclerView roomList;
        LinearLayout floorDetail;

        public ViewHolder(View itemView) {
            super(itemView);

            floorName = (TextView) itemView.findViewById(R.id.floorName);
            more = (ImageView) itemView.findViewById(R.id.more);
            less = (ImageView) itemView.findViewById(R.id.less);
            floorDetail = (LinearLayout) itemView.findViewById(R.id.floorDetail);
            roomList = (RecyclerView) itemView.findViewById(R.id.roomList);
        }
    }
}
