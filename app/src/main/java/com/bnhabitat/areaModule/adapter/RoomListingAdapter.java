package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;

import java.util.ArrayList;

public class RoomListingAdapter extends RecyclerView.Adapter<RoomListingAdapter.ViewHolder> {

    Context context;
    ArrayList<String> roomList = new ArrayList<>();

    public RoomListingAdapter(Context context, ArrayList<String> roomList) {
        this.context = context;
        this.roomList = roomList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.room_listing_area_module, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.roomName.setText(roomList.get(position));
        holder.expandLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.contractLayout.setVisibility(View.VISIBLE);
                holder.expandLayout.setVisibility(View.GONE);
                holder.roomDetail.setVisibility(View.VISIBLE);
                holder.roomName.setTextColor(ContextCompat.getColor(context, R.color.black_dark));
            }
        });

        holder.contractLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.contractLayout.setVisibility(View.GONE);
                holder.expandLayout.setVisibility(View.VISIBLE);
                holder.roomDetail.setVisibility(View.GONE);
                holder.roomName.setTextColor(ContextCompat.getColor(context, R.color.light_text_grey));
            }
        });
    }

    @Override
    public int getItemCount() {
        return roomList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout roomDetail;
        TextView roomName;
        ImageView expandLayout, contractLayout;
        public ViewHolder(View itemView) {
            super(itemView);

            roomDetail = (LinearLayout) itemView.findViewById(R.id.roomDetail);
            roomName = (TextView) itemView.findViewById(R.id.roomName);
            expandLayout = (ImageView) itemView.findViewById(R.id.expandLayout);
            contractLayout = (ImageView) itemView.findViewById(R.id.contractLayout);
        }
    }
}
