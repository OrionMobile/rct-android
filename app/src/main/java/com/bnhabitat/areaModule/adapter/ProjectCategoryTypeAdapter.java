package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.ProjectCategoryTypeModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

import lecho.lib.hellocharts.model.Line;

public class ProjectCategoryTypeAdapter extends RecyclerView.Adapter<ProjectCategoryTypeAdapter.ViewHolder> {

    Context context;
    ArrayList<ProjectCategoryTypeModel> projectCategoryTypeModelArrayList = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations> accomodationsArrayList = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationFloors> accommodationFloors = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationRooms> accommodationRooms = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Towers> towersArrayList = new ArrayList<>();
    ProjectAccomodationAdapter projectAccomodationAdapter;
    NewTowerDisplayAdapter towerDisplayAdapter;

    public ProjectCategoryTypeAdapter(Context context, ArrayList<ProjectCategoryTypeModel> projectCategoryTypeModelArrayList, ArrayList<ProjectCategoryTypeModel.Accomodations> accomodationsArrayList, ArrayList<ProjectCategoryTypeModel.Towers> towersArrayList, ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationRooms> accommodationRooms, ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationFloors> accommodationFloors) {
        this.context = context;
        this.projectCategoryTypeModelArrayList = projectCategoryTypeModelArrayList;
        this.accomodationsArrayList = accomodationsArrayList;
        this.towersArrayList = towersArrayList;
        this.accommodationRooms = accommodationRooms;
        this.accommodationFloors = accommodationFloors;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.project_category_type, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.heading.setText(projectCategoryTypeModelArrayList.get(position).getCategoryType());
        if(accomodationsArrayList.size()==0){
            holder.accomodation.setVisibility(View.GONE);
        }
        if(towersArrayList.size()==0){
            holder.towers.setVisibility(View.GONE);
        }

        holder.expandLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.accomodationView.setVisibility(View.VISIBLE);
                holder.contractLayout.setVisibility(View.VISIBLE);
                holder.expandLayout.setVisibility(View.GONE);
                projectAccomodationAdapter = new ProjectAccomodationAdapter(context, accomodationsArrayList, accommodationRooms, accommodationFloors);
                holder.accomodationView.setLayoutManager(new LinearLayoutManager(context));
                holder.accomodationView.setAdapter(projectAccomodationAdapter);
                projectAccomodationAdapter.notifyDataSetChanged();
            }
        });

        holder.contractLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.contractLayout.setVisibility(View.GONE);
                holder.expandLayout.setVisibility(View.VISIBLE);
                holder.accomodationView.setVisibility(View.GONE);
                projectAccomodationAdapter.notifyDataSetChanged();
            }
        });

        holder.expandLayout_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.towerView.setVisibility(View.VISIBLE);
                holder.contractLayout_1.setVisibility(View.VISIBLE);
                holder.expandLayout_1.setVisibility(View.GONE);
                towerDisplayAdapter = new NewTowerDisplayAdapter(context, towersArrayList);
                holder.towerView.setLayoutManager(new LinearLayoutManager(context));
                holder.towerView.setAdapter(towerDisplayAdapter);
                towerDisplayAdapter.notifyDataSetChanged();
            }
        });

        holder.contractLayout_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.contractLayout_1.setVisibility(View.GONE);
                holder.expandLayout_1.setVisibility(View.VISIBLE);
                holder.towerView.setVisibility(View.GONE);
                towerDisplayAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return projectCategoryTypeModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView heading;
        LinearLayout accomodation, towers;
        RecyclerView accomodationView, towerView;
        ImageView expandLayout, contractLayout, expandLayout_1, contractLayout_1;

        public ViewHolder(View itemView) {
            super(itemView);

            heading = (TextView) itemView.findViewById(R.id.heading);
            accomodation = (LinearLayout) itemView.findViewById(R.id.accomodation);
            towers = (LinearLayout) itemView.findViewById(R.id.towers);
            accomodationView = (RecyclerView) itemView.findViewById(R.id.accomodationView);
            towerView = (RecyclerView) itemView.findViewById(R.id.towerView);
            expandLayout = (ImageView) itemView.findViewById(R.id.expandLayout);
            contractLayout = (ImageView) itemView.findViewById(R.id.contractLayout);
            expandLayout_1 = (ImageView) itemView.findViewById(R.id.expandLayout_1);
            contractLayout_1 = (ImageView) itemView.findViewById(R.id.contractLayout_1);
        }
    }
}
