package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.ClubImageModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.areaModule.model.TownshipFilesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class StateImageAdapter extends RecyclerView.Adapter<StateImageAdapter.ViewHolder> {

    Context context;
    ArrayList<StateListModel.Pictures> stateImage = new ArrayList<>();
    ArrayList<TownshipFilesModel> stateImage1 = new ArrayList<>();
    ArrayList<ClubImageModel> clubImageModels = new ArrayList<>();

    public StateImageAdapter(Context context, ArrayList<StateListModel.Pictures> stateImage) {
        this.context = context;
        this.stateImage = stateImage;
    }

    public StateImageAdapter(Context context, ArrayList<TownshipFilesModel> stateImage1, String abc) {
        this.context = context;
        this.stateImage1 = stateImage1;
    }

    public StateImageAdapter(Context context, String abc, ArrayList<ClubImageModel> clubImageModels) {
        this.context = context;
        this.clubImageModels = clubImageModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.state_image, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(stateImage.size()!=0)
            Picasso.with(context).load(stateImage.get(position).getUrl().toString()).into(holder.img_video);
        else if(stateImage1.size()!=0)
            Picasso.with(context).load(stateImage1.get(position).getFile().getUrl().toString()).into(holder.img_video);
        else if(clubImageModels.size()!=0)
            Picasso.with(context).load(clubImageModels.get(position).getFileUrl().toString()).into(holder.img_video);


    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(stateImage.size()!=0)
            size = stateImage.size();
        else if(stateImage1.size()!=0)
            size = stateImage1.size();
        else
            size = clubImageModels.size();
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_video;

        public ViewHolder(View itemView) {
            super(itemView);

            img_video = (ImageView) itemView.findViewById(R.id.img_video);
        }
    }
}
