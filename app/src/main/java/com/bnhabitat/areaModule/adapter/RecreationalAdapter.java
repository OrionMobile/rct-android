package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.ClubFacilities;
import com.bnhabitat.areaModule.model.ProjectAmenityModel;
import com.bnhabitat.areaModule.model.RecreationalModel;

import java.util.ArrayList;

public class RecreationalAdapter extends RecyclerView.Adapter<RecreationalAdapter.ViewHolder> {

    Context context;
    ArrayList<RecreationalModel> recreationalModels = new ArrayList<>();
    ArrayList<ClubFacilities> clubFacilities = new ArrayList<>();
    ArrayList<ProjectAmenityModel.Amenity> amenities = new ArrayList<>();
    int size = 0;

    public RecreationalAdapter(Context context, ArrayList<RecreationalModel> recreationalModels) {
        this.context = context;
        this.recreationalModels = recreationalModels;
    }

    public RecreationalAdapter(Context context, ArrayList<ClubFacilities> clubFacilities, String abc) {
        this.context = context;
        this.clubFacilities = clubFacilities;
    }

    public RecreationalAdapter(Context context, String abc, ArrayList<ProjectAmenityModel.Amenity> amenities) {
        this.context = context;
        this.amenities = amenities;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recreational_area, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(recreationalModels.size()!=0) {
            holder.recreational_value.setText(recreationalModels.get(position).getValue());
            holder.recreational_title.setVisibility(View.GONE);
        }
        else if(clubFacilities.size()!=0) {
            holder.recreational_value.setText(clubFacilities.get(position).getAmenity().getValue());
            holder.recreational_title.setVisibility(View.GONE);
        }
        else if(amenities.size()!=0) {
            holder.recreational_value.setText(amenities.get(position).getValue());
            holder.recreational_title.setText(amenities.get(position).getTitle());
        }
    }

    @Override
    public int getItemCount() {
        if(recreationalModels.size()!=0)
            size = recreationalModels.size();
        else if(clubFacilities.size()!=0)
            size = clubFacilities.size();
        else if(amenities.size()!=0)
            size = amenities.size();
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView recreational_value, recreational_title;

        public ViewHolder(View itemView) {
            super(itemView);

            recreational_value = (TextView) itemView.findViewById(R.id.recreational_value);
            recreational_title = (TextView) itemView.findViewById(R.id.recreational_title);
        }
    }
}
