package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.bnhabitat.areaModule.model.ImageModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.areaModule.model.TownshipFilesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<StateListModel.Pictures> image_list = new ArrayList<>();
    private ArrayList<ImageModel> image_list1 = new ArrayList<>();
    String abc = "";

    public ImageAdapter(Context mContext, ArrayList<StateListModel.Pictures> image_list) {
        this.context = mContext;
        this.image_list = image_list;
    }

    public ImageAdapter(Context context, ArrayList<ImageModel> image_list1, String abc) {
        this.context = context;
        this.image_list1 = image_list1;
        this.abc = abc;
    }

    @Override
    public int getCount() {
        int size = 0;
        if(image_list.size()!=0)
            size = image_list.size();
        else if (image_list1.size()!=0)
            size = image_list1.size();
        return size;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        if(image_list.size()!=0) {
            for (int i = 0; i < image_list.size(); i++)
                Picasso.with(context).load(image_list.get(position).getUrl().toString()).into(imageView);
        }
        else if(image_list1.size()!=0){
            for(int i = 0 ;i < image_list1.size(); i++)
                Picasso.with(context).load(image_list1.get(position).getFileUrl().toString()).into(imageView);
        }
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}
