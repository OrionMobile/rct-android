package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.SectorDetail;
import com.bnhabitat.areaModule.model.SectorListingModel;

import java.util.ArrayList;

public class SubareaListingAdapter extends RecyclerView.Adapter<SubareaListingAdapter.ViewHolder> {

    Context context;
    ArrayList<SectorListingModel> subareaListingArrayList = new ArrayList<>();

    public SubareaListingAdapter(Context context, ArrayList<SectorListingModel> subareaListingArrayList) {
        this.context = context;
        this.subareaListingArrayList = subareaListingArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_row_subarea_list, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.subareaNaming.setText(subareaListingArrayList.get(position).getName());
        holder.subareaCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //context.startActivity(new Intent(context, SectorDetail.class));
                Intent intent = new Intent(context, SectorDetail.class);
                intent.putExtra("SectorId", subareaListingArrayList.get(position).getId());
                intent.putExtra("SectorName", subareaListingArrayList.get(position).getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subareaListingArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView subareaCardView;
        TextView subareaNaming;

        public ViewHolder(View itemView) {
            super(itemView);
            subareaCardView = (CardView) itemView.findViewById(R.id.subareaCardView);
            subareaNaming = (TextView) itemView.findViewById(R.id.subareaNaming);
        }
    }
}
