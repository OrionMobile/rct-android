package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.bnhabitat.models.InventoryModel;

import java.util.ArrayList;

public class AccomodationAdapter extends RecyclerView.Adapter<AccomodationAdapter.ViewHolder> {

    private Context context;
    ArrayList<InventoryModel.PropertyBedrooms> _listDataHeader = new ArrayList<>();// header titles
    ArrayList<InventoryModel.PropertyAccommodationDetails> _listDataHeader1 = new ArrayList<>();// header titles
    ArrayList<InventoryModel.PropertyBedrooms> propertyBedrooms1 = new ArrayList<>();
    ArrayList<InventoryModel.PropertyBedrooms> propertyBedrooms2 = new ArrayList<>();
    ArrayList<InventoryModel.PropertyAccommodationDetails> accommodationDetails1 = new ArrayList<>();
    ArrayList<InventoryModel.PropertyAccommodationDetails> accommodationDetails2 = new ArrayList<>();

    public AccomodationAdapter(Context context, ArrayList<InventoryModel.PropertyBedrooms> _listDataHeader, ArrayList<InventoryModel.PropertyAccommodationDetails> _listDataHeader1, ArrayList<InventoryModel.PropertyBedrooms> propertyBedrooms1, ArrayList<InventoryModel.PropertyBedrooms> propertyBedrooms2, ArrayList<InventoryModel.PropertyAccommodationDetails> accommodationDetails1, ArrayList<InventoryModel.PropertyAccommodationDetails> accommodationDetails2) {
        this.context = context;
        this._listDataHeader = _listDataHeader;
        this._listDataHeader1 = _listDataHeader1;
        this.propertyBedrooms1 = propertyBedrooms1;
        this.propertyBedrooms2 = propertyBedrooms2;
        this.accommodationDetails1 = accommodationDetails1;
        this.accommodationDetails2 = accommodationDetails2;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
