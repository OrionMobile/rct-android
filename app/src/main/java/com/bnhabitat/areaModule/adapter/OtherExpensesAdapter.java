/*
package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel_Area;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;

*/
/**
 * Created by gourav on 5/15/2018.
 *//*


public class OtherExpensesAdapter extends RecyclerView.Adapter<OtherExpensesAdapter.ViewHolder>  {

    private Context context;
    //
    ArrayList<InventoryModel_Area.PropertyOtherExpences> propertyOtherExpences=new ArrayList<>();// header titles

    public OtherExpensesAdapter(Context context,ArrayList<InventoryModel_Area.PropertyOtherExpences> propertyOtherExpences
    ) {
        this.context = context;
        this.propertyOtherExpences = propertyOtherExpences;


    }




    //
    @Override
    public OtherExpensesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.otherexpenses_listitems, parent, false);
        OtherExpensesAdapter.ViewHolder viewHolder = new OtherExpensesAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final OtherExpensesAdapter.ViewHolder holder, int position) {
        holder.department.setText(Utils.getEmptyValue(propertyOtherExpences.get(position).getTitle()));
        holder.amount.setText(Utils.getEmptyValue(propertyOtherExpences.get(position).getValue()));
        holder.last_paid.setText(Utils.getEmptyValue(propertyOtherExpences.get(position).getLastPaidDate()));

//
    }

    @Override
    public int getItemCount() {
        return propertyOtherExpences.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView department, amount, last_paid;


        public ViewHolder(View itemView) {
            super(itemView);
            department = (TextView) itemView
                    .findViewById(R.id.department);
            amount = (TextView) itemView
                    .findViewById(R.id.amount);
            last_paid = (TextView) itemView
                    .findViewById(R.id.last_paid);

//
//
        }


    }
}

*/
