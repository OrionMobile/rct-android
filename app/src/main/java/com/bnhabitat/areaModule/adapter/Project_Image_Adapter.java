package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;

import java.util.ArrayList;

public class Project_Image_Adapter extends RecyclerView.Adapter<Project_Image_Adapter.ViewHolder> {

    Context context;
    ArrayList<String> project_naming = new ArrayList<>();

    public Project_Image_Adapter() {
    }

    public Project_Image_Adapter(Context context, ArrayList<String> project_naming) {
        this.context = context;
        this.project_naming = project_naming;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.projects_list_township, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.projectNaming.setText(project_naming.get(position).toString().trim());
    }

    @Override
    public int getItemCount() {

        return project_naming.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView projectImage;
        TextView projectNaming;

        public ViewHolder(View itemView) {
            super(itemView);
            projectImage = (ImageView) itemView.findViewById(R.id.projectImage);
            projectNaming = (TextView) itemView.findViewById(R.id.projectNaming);

        }
    }
}
