package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.ProjectDetail;
import com.bnhabitat.areaModule.model.ProjectListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProjectListingAdapterNew extends RecyclerView.Adapter<ProjectListingAdapterNew.ViewHolder> {

    Context context;
    ArrayList<ProjectListModel> projectListingArrayList = new ArrayList<>();
    String Locality, tehsil, township, district, state, zip;
    int resCount = 0;
    int comCount = 0;
    String id = "";


    public ProjectListingAdapterNew(Context context, ArrayList<ProjectListModel> projectListingArrayList) {
        this.context = context;
        this.projectListingArrayList = projectListingArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_row_project_list, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.projectNaming.setText(projectListingArrayList.get(position).getName());

        //holder.projectImage.setImageResource(R.drawable.demo_project_image);
        if(projectListingArrayList.get(position).getLocalityName().equalsIgnoreCase("null"))
            Locality = "";
        else
            Locality = projectListingArrayList.get(position).getLocalityName();

        if(projectListingArrayList.get(position).getTehsilName().equalsIgnoreCase("null"))
            tehsil = "";
        else
            tehsil = projectListingArrayList.get(position).getTehsilName();

        if(projectListingArrayList.get(position).getTownName().equalsIgnoreCase("null"))
            township = "";
        else
            township = projectListingArrayList.get(position).getTownName();

        if(projectListingArrayList.get(position).getDistrictName().equalsIgnoreCase("null"))
            district = "";
        else
            district = projectListingArrayList.get(position).getDistrictName();

        if(projectListingArrayList.get(position).getStateName().equalsIgnoreCase("null"))
            state = "";
        else
            state = projectListingArrayList.get(position).getStateName();

        if(projectListingArrayList.get(position).getZipCode().equalsIgnoreCase("null"))
            zip = "";
        else
            zip = projectListingArrayList.get(position).getZipCode();

        holder.sectorNaming.setText(Locality + " " + tehsil + " " + township + " " + district + " " + state + " " + zip);

        resCount = 0; comCount = 0;
        for(int i = 0; i <projectListingArrayList.get(position).getProjectCategoryTypes().size() ; i++){
            if(projectListingArrayList.get(position).getProjectCategoryTypes().get(i).getType().equalsIgnoreCase("Residential"))
                resCount++;
            if(projectListingArrayList.get(position).getProjectCategoryTypes().get(i).getType().equalsIgnoreCase("Commercial"))
                comCount++;
        }

        holder.residentialtNumber.setText(String.valueOf(resCount));
        holder.commercialNumber.setText(String.valueOf(comCount));

        for(int i = 0 ; i < projectListingArrayList.get(position).getProjectFiles().size(); i++ ){
            if(projectListingArrayList.get(position).getProjectFiles().get(i).getCategory().equalsIgnoreCase("Photos"))
                Picasso.with(context).load(projectListingArrayList.get(position).getProjectFiles().get(0).getFileUrl()).into(holder.projectImage);
            else
                holder.projectImage.setImageResource(R.drawable.image_logo);
        }

        holder.projectCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* //context.startActivity(new Intent(context, ProjectDetail.class));
                Intent intent = new Intent(context, ProjectDetail.class);
                intent.putExtra("ProjectId", projectListingArrayList.get(position).getId());
                context.startActivity(intent);*/

                Toast.makeText(context, "Project details coming soon", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return projectListingArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView projectCardView;
        ImageView projectImage;
        TextView projectNaming, sectorNaming, residentialtNumber, commercialNumber;

        public ViewHolder(View itemView) {
            super(itemView);

            projectCardView = (CardView) itemView.findViewById(R.id.projectCardView);
            projectImage = (ImageView) itemView.findViewById(R.id.projectImage);
            projectNaming = (TextView) itemView.findViewById(R.id.projectNaming);
            sectorNaming = (TextView) itemView.findViewById(R.id.sectorNaming);
            residentialtNumber = (TextView) itemView.findViewById(R.id.residentialtNumber);
            commercialNumber = (TextView) itemView.findViewById(R.id.commercialNumber);
        }
    }

}
