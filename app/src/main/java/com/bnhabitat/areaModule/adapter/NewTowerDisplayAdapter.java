package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.ProjectCategoryTypeModel;

import java.util.ArrayList;

public class NewTowerDisplayAdapter extends RecyclerView.Adapter<NewTowerDisplayAdapter.ViewHolder> {
    Context context;
    ArrayList<ProjectCategoryTypeModel.Towers> towersArrayList = new ArrayList<>();

    public NewTowerDisplayAdapter(Context context, ArrayList<ProjectCategoryTypeModel.Towers> towersArrayList) {
        this.context = context;
        this.towersArrayList = towersArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.tower_data, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.towerName.setText(towersArrayList.get(position).getName());
        holder.floor_count.setText(towersArrayList.get(position).getTotalFloor());
        holder.passenger_lift_count.setText(towersArrayList.get(position).getTotalPassengerLift());
        holder.cargo_lift_count.setText(towersArrayList.get(position).getTotalCargoLift());
        holder.staircase_count.setText(towersArrayList.get(position).getTotalStaircase());
        holder.basement_count.setText(towersArrayList.get(position).getTotalBasements());
    }

    @Override
    public int getItemCount() {
        return towersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView towerName, floor_count, passenger_lift_count, cargo_lift_count, staircase_count, basement_count;

        public ViewHolder(View itemView) {
            super(itemView);

            towerName = (TextView) itemView.findViewById(R.id.towerName);
            floor_count = (TextView) itemView.findViewById(R.id.floor_count);
            passenger_lift_count = (TextView) itemView.findViewById(R.id.passenger_lift_count);
            cargo_lift_count = (TextView) itemView.findViewById(R.id.cargo_lift_count);
            staircase_count = (TextView) itemView.findViewById(R.id.staircase_count);
            basement_count = (TextView) itemView.findViewById(R.id.basement_count);
        }
    }
}
