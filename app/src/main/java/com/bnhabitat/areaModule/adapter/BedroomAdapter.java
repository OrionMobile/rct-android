package com.bnhabitat.areaModule.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.InventoryModel_Area;

import java.util.ArrayList;

public class BedroomAdapter extends RecyclerView.Adapter<BedroomAdapter.ViewHolder> {

    private Context context;
    ArrayList<InventoryModel_Area.PropertyBedrooms> bedrooms = new ArrayList<>();
    ArrayList<InventoryModel_Area.PropertyAccommodationDetails> accommodationDetails = new ArrayList<>();
    String str = "";
    int size = 0;

    public BedroomAdapter(Context context, ArrayList<InventoryModel_Area.PropertyBedrooms> bedrooms) {
        this.context = context;
        this.bedrooms = bedrooms;
    }

    public BedroomAdapter(Context context, ArrayList<InventoryModel_Area.PropertyAccommodationDetails> accommodationDetails, String str) {
        this.context = context;
        this.accommodationDetails = accommodationDetails;
        this.str = str;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.specification_list_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(bedrooms.size()!=0) {
            holder.item_name.setText(bedrooms.get(position).getBedRoomName());
            holder.item_description.setText(bedrooms.get(position).getBedRoomType());
            holder.floornumber.setText(bedrooms.get(position).getFloorNo());
        }
        else if(accommodationDetails.size()!=0){
            holder.item_name.setText(accommodationDetails.get(position).getName());
            holder.item_description.setText(accommodationDetails.get(position).getType());
            holder.floornumber.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if(bedrooms.size()!=0)
            size = bedrooms.size();
        if(accommodationDetails.size()!=0)
            size = accommodationDetails.size();
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView item_name, item_description, floornumber;

        public ViewHolder(View itemView) {
            super(itemView);
            item_name = (TextView) itemView.findViewById(R.id.item_name);
            item_description = (TextView) itemView.findViewById(R.id.item_description);
            floornumber = (TextView) itemView.findViewById(R.id.floornumber);
        }
    }
}
