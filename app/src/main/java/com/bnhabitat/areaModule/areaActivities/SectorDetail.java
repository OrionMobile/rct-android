package com.bnhabitat.areaModule.areaActivities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.FeaturesAdapter;
import com.bnhabitat.areaModule.adapter.Govt_Notification_Adapter;
import com.bnhabitat.areaModule.adapter.ImageAdapter;
import com.bnhabitat.areaModule.adapter.Image_Video_Adapter;
import com.bnhabitat.areaModule.adapter.ProjectListingAdapter;
import com.bnhabitat.areaModule.adapter.RecreationalAdapter;
import com.bnhabitat.areaModule.adapter.StateImageAdapter;
import com.bnhabitat.areaModule.model.KeydistanceModel;
import com.bnhabitat.areaModule.model.RecreationalModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.township.townactivities.AddSectorLocality;
import com.bnhabitat.township.townactivities.Add_Project;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SectorDetail extends AppCompatActivity implements ViewPager.OnPageChangeListener, ViewPagerListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener, View.OnClickListener {

    RecyclerView list_images_videos, mohali_photos, features, recreational_area;

    ArrayList<StateListModel.Pictures> pictures1 = new ArrayList<>();
    ArrayList<StateListModel.Pictures> pictures = new ArrayList<>();
    ArrayList<KeydistanceModel> featureData = new ArrayList<>();
    ArrayList<RecreationalModel> recreationalModels = new ArrayList<>();
    DrawerLayout drawer_layout;
    ScrollView scroll_filter;
    ViewPager viewPager;

    TextView project_count, residential_count, commercial_count;
    TextView sector_locality, district, state, total_area, total_area_units;
    TextView township_count, locality_count, property_count, totalTehsils_count, totalUrbanVillages_count, totalRuralVillages_count;
    TextView sector_locality_name, name_of_sector, about_sector, lineCount;

    private View actionB,action_A, action_c, action_d, action_e, action_f, action_g;
    String Id, name;

    ImageAdapter imageAdapter;
    Image_Video_Adapter image_video_adapter;
    StateImageAdapter stateImageAdapter;
    FeaturesAdapter featuresAdapter;
    RecreationalAdapter recreationalAdapter;

    ImageView imgBack, imglocation, imgPrevious, imgNext, imgFilter;

    Dialog dialog, dialog1;
    ImageView userPic, msg, call;
    TextView first_last_name, professional_txt, submitReq, propertSell_Rent;
    String contact = "", email = "";
    private static final int REQUEST_PHONE_CALL = 198;
    ImageView close, img, close1, image;
    TextView project_name, project_name1, desc, description;
    RadioButton yes, no, buy, rent, yes1, no1, sell, rent1;
    LinearLayout property_detailing, property_detailing1;
    Spinner category_spinner, property_spinner, category_spinner1, property_spinner1;
    CheckBox check_studio, check_2bhk, check_3bhk, check_4bhk;
    EditText message, datePicker, media, tower_name_no, unit_no, message1, datePick, media1;
    Button submit, submit1;
    String name_number;
    FloatingActionsMenu floatingActionsMenu;
    TextView no_data, no_data1, no_data2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sector_detail);

        /*Intent intent = getIntent();
        Id = intent.getStringExtra("SectorId");
        name = intent.getStringExtra("SectorName");*/

        /*initialization();
        onClicks();*/



        //onSectorDetail();
        /*if(PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("") || PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("null"))
            name_number = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        else
            name_number = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "");
        first_last_name.setText(name_number);
        contact = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        email = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.EMAIL, "");

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (ContextCompat.checkSelfPermission(SectorDetail.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SectorDetail.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                } else {
                    callIntent.setData(Uri.parse("tel:" + contact));
                    startActivity(callIntent);
                }
            }
        });

        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
                startActivity(Intent.createChooser(intent, "Choose"));
            }
        });

        submitReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitReqPopUp(pictures.get(0).getUrl());
            }
        });

        propertSell_Rent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                propertySell_RentPopUp(pictures.get(0).getUrl());
            }
        });*/


        /*scroll_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(SectorDetail.this);
            }
        });*/



        /*action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();

            }
        });
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(SectorDetail.this, Add_Project.class);
                startActivity(intent);

            }
        });
        action_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(SectorDetail.this, Add_township.class);
                startActivity(intent);

            }
        });
        action_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(SectorDetail.this, AddSectorLocality.class);
                startActivity(intent);

            }
        });
        action_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();

            }
        });
        action_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();

            }
        });
        action_g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(SectorDetail.this, Submit_Requirement.class);
                startActivity(intent);

            }
        });*/


    }

    @Override
    protected void onResume() {
        onSectorDetail();
        super.onResume();
    }

    private void onClicks() {

        imgBack.setOnClickListener(this);
        imglocation.setOnClickListener(this);
        imgFilter.setOnClickListener(this);
        imgPrevious.setOnClickListener(this);
        imgNext.setOnClickListener(this);
        //scroll_filter.setOnClickListener(this);
        action_A.setOnClickListener(this);
        actionB.setOnClickListener(this);
        action_c.setOnClickListener(this);
        action_d.setOnClickListener(this);
        action_e.setOnClickListener(this);
        action_f.setOnClickListener(this);
        action_g.setOnClickListener(this);
        /*
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imglocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapPopup(pictures1);
            }
        });

        imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDrawer();
            }
        });

        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItemBack(viewPager.getCurrentItem())==0)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else if(getItemBack(viewPager.getCurrentItem())==pictures.size()-1)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else
                    viewPager.setCurrentItem(getItemBack(-1), false);

            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItem(+1) == pictures.size())
                    viewPager.setCurrentItem(getItem(0), true);
                else
                    viewPager.setCurrentItem(getItem(+1), false);

            }
        });*/
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.imgBack:
                finish();
                break;
            /*case R.id.imgFilter:
                openDrawer();
                break;*/
            case R.id.imglocation:
                mapPopup(pictures1);
                break;
            case R.id.scroll_filter:
                hideSoftKeyboard(SectorDetail.this);
                break;
            case R.id.action_a:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_b:
                floatingActionsMenu.collapse();
                intent = new Intent(SectorDetail.this, Add_Project.class);
                startActivity(intent);
                break;
            case R.id.action_c:
                floatingActionsMenu.collapse();
                intent = new Intent(SectorDetail.this, Add_township.class);
                startActivity(intent);
                break;
            case R.id.action_d:
                floatingActionsMenu.collapse();
                intent = new Intent(SectorDetail.this, AddSectorLocality.class);
                startActivity(intent);
                break;
            case R.id.action_e:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_f:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_g:
                floatingActionsMenu.collapse();
                intent = new Intent(SectorDetail.this, Submit_Requirement.class);
                startActivity(intent);
                break;
            case R.id.imgPrevious:
                if(getItemBack(viewPager.getCurrentItem())==0)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else if(getItemBack(viewPager.getCurrentItem())==pictures.size()-1)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else
                    viewPager.setCurrentItem(getItemBack(-1), false);
                break;
            case R.id.imgNext:
                if(getItem(+1) == pictures.size())
                    viewPager.setCurrentItem(getItem(0), true);
                else
                    viewPager.setCurrentItem(getItem(+1), false);
                break;

        }

    }

    @Override
    protected void onStart() {
        initialization();
        onClicks();

        Intent intent = getIntent();
        Id = intent.getStringExtra("SectorId");
        name = intent.getStringExtra("SectorName");

        sector_locality_name.setText(name);
        name_of_sector.setText(name);

        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.transparent));
            }

            @SuppressLint("ResourceType")
            @Override
            public void onMenuCollapsed() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.tran));

            }
        });

        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawer_layout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

                hideSoftKeyboard(SectorDetail.this);

            }
        });
        //set();
        super.onStart();
    }

    private void mapPopup(ArrayList<StateListModel.Pictures> pictures) {
        dialog = new Dialog(SectorDetail.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map_pop_up);
        close = (ImageView) dialog.findViewById(R.id.close);
        viewPager = (ViewPager) dialog.findViewById(R.id.map_images);
        imageAdapter = new ImageAdapter(this, pictures);
        viewPager.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

    }

    private void initialization() {
        floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        scroll_filter = (ScrollView) findViewById(R.id.scroll_filter);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imglocation = (ImageView) findViewById(R.id.imglocation);
        imgFilter = (ImageView) findViewById(R.id.imgFilter);
        project_count = (TextView) findViewById(R.id.project_count);
        residential_count = (TextView) findViewById(R.id.residential_count);
        commercial_count = (TextView) findViewById(R.id.commercial_count);

        no_data = (TextView) findViewById(R.id.no_data);
        no_data1 = (TextView) findViewById(R.id.no_data1);
        no_data2 = (TextView) findViewById(R.id.no_data2);
        sector_locality = (TextView) findViewById(R.id.sector_locality);
        district = (TextView) findViewById(R.id.district);
        state = (TextView) findViewById(R.id.state);
        total_area = (TextView) findViewById(R.id.total_area);
        total_area_units = (TextView) findViewById(R.id.total_area_units);
        total_area_units.setVisibility(View.GONE);
        township_count = (TextView) findViewById(R.id.township_count);
        locality_count = (TextView) findViewById(R.id.locality_count);
        property_count = (TextView) findViewById(R.id.property_count);
        totalTehsils_count = (TextView) findViewById(R.id.totalTehsils_count);
        totalUrbanVillages_count = (TextView) findViewById(R.id.totalUrbanVillages_count);
        totalRuralVillages_count = (TextView) findViewById(R.id.totalRuralVillages_count);

        sector_locality_name = (TextView) findViewById(R.id.sector_locality_name);
        name_of_sector = (TextView) findViewById(R.id.name_of_sector);
        about_sector = (TextView) findViewById(R.id.about_sector);
        lineCount = (TextView) findViewById(R.id.lineCount);

        viewPager = (ViewPager) findViewById(R.id.view_images);
        imgPrevious = (ImageView) findViewById(R.id.imgPrevious);
        imgNext = (ImageView) findViewById(R.id.imgNext);
        list_images_videos = (RecyclerView) findViewById(R.id.list_images_videos);
        mohali_photos = (RecyclerView) findViewById(R.id.mohali_photos);
        features = (RecyclerView) findViewById(R.id.features);
        recreational_area = (RecyclerView) findViewById(R.id.recreational_area);

        userPic = (ImageView) findViewById(R.id.userPic);
        msg = (ImageView) findViewById(R.id.msg);
        call = (ImageView) findViewById(R.id.call);

        first_last_name = (TextView) findViewById(R.id.first_last_name);
        professional_txt = (TextView) findViewById(R.id.professional_txt);
        submitReq = (TextView) findViewById(R.id.submitReq);
        propertSell_Rent = (TextView) findViewById(R.id.propertSell_Rent);

        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        action_c = findViewById(R.id.action_c);
        action_d = findViewById(R.id.action_d);
        action_e= findViewById(R.id.action_e);
        action_f = findViewById(R.id.action_f);
        action_g = findViewById(R.id.action_g);

    }

    private int getItem(int i) {
        if(viewPager.getCurrentItem() == pictures.size()-1)
            return 0;
        else
            return viewPager.getCurrentItem() + i;
    }

    private int getItemBack(int i) {

        if(viewPager.getCurrentItem() == pictures.size()-1)
            return pictures.size()-2;
        else if(viewPager.getCurrentItem() == 0)
            return pictures.size()-1;
        else
            return viewPager.getCurrentItem() + i;

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void setViewPager(int position) {
        viewPager.setCurrentItem(position, false);

    }

    public void onSectorDetail(){
        new CommonSyncwithoutstatus(this,
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_SECTOR_DETAIL +
                        Id ,
                "",
                "Loading...",this,
                Urls.URL_SECTOR_DETAIL,
                Constants.GET)
                .execute();
    }

    @Override
    public void onResultListener(String result, String which) {

        if(null != result && !result.equalsIgnoreCase("")){
            if(which.equalsIgnoreCase(Urls.URL_SECTOR_DETAIL)){
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    int statusCode = jsonObject.getInt("StatusCode");
                    if(statusCode == 200){
                        final JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        if(jsonObject1.getString("Name").equalsIgnoreCase("null"))
                            sector_locality.setText("N/A");
                        else
                            sector_locality.setText(jsonObject1.getString("Name"));
                        String sec = jsonObject1.getString("Name");
                        PreferenceConnector.getInstance(this).savePreferences("Locality", sec);
                        if(jsonObject1.getString("District").equalsIgnoreCase("null"))
                            district.setText("N/A");
                        else
                            district.setText(jsonObject1.getString("District"));
                        String dis = jsonObject1.getString("District");
                        PreferenceConnector.getInstance(this).savePreferences("Dist", dis);
                        if(jsonObject1.getString("State").equalsIgnoreCase("null"))
                            state.setText("N/A");
                        else
                            state.setText(jsonObject1.getString("State"));
                        String st = jsonObject1.getString("State");
                        PreferenceConnector.getInstance(this).savePreferences("Stating", st);
                        if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            total_area.setText("N/A");
                        else if(!jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            total_area.setText(jsonObject1.getString("TotalArea") + " N/A" );
                        else if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && !jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            total_area.setText("N/A " + jsonObject1.getString("TotalAreaSizeUnit"));
                        else
                            total_area.setText(jsonObject1.getString("TotalArea") + " " + jsonObject1.getString("TotalAreaSizeUnit"));
                        /*if(jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            total_area_units.setText("N/A");
                        else
                            total_area_units.setText(jsonObject1.getString("TotalAreaSizeUnit"));*/

                        residential_count.setText(jsonObject1.getString("TotalResidentialProperties"));
                        commercial_count.setText(jsonObject1.getString("TotalCommercialProperties"));

                        township_count.setText(jsonObject1.getString("Townships"));
                        locality_count.setText(jsonObject1.getString("Localities"));
                        property_count.setText(jsonObject1.getString("Properties"));
                        totalTehsils_count.setText(jsonObject1.getString("TotalTehsils"));
                        totalUrbanVillages_count.setText(jsonObject1.getString("TotalUrbanVillages"));
                        totalRuralVillages_count.setText(jsonObject1.getString("TotalRuralVillages"));
                        if(jsonObject1.getString("AboutArea").equalsIgnoreCase("null"))
                            about_sector.setText("N/A");
                        else
                            about_sector.setText(jsonObject1.getString("AboutArea"));

                        about_sector.post(new Runnable() {
                            @Override
                            public void run() {
                                final int lineCount1 = about_sector.getLineCount();
                                if(lineCount1 <= 5) {
                                    about_sector.setEllipsize(null);
                                    lineCount.setVisibility(View.GONE);
                                }
                                else{
                                    lineCount.setVisibility(View.VISIBLE);
                                    lineCount.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            lineCount.setVisibility(View.GONE);
                                            about_sector.setEllipsize(null);
                                            about_sector.setMaxLines(Integer.MAX_VALUE);
                                            try {
                                                about_sector.setText(jsonObject1.getString("AboutArea"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                                // Use lineCount here
                            }
                        });
                        JSONArray jsonArrayn = jsonObject1.getJSONArray("Projects");
                        project_count.setText(String.valueOf(jsonArrayn.length()));

                        JSONArray jsonArray = jsonObject1.getJSONArray("Pictures");
                        if(jsonArray.length()!=0){
                            for(int i = 0; i<jsonArray.length(); i++){
                                JSONObject object = jsonArray.getJSONObject(i);
                                StateListModel.Pictures stateListModel = new StateListModel.Pictures();
                                stateListModel.setId(object.getString("Id"));
                                stateListModel.setUrl(Urls.BASE_CONTACT_IMAGE_URL +object.getString("Url"));
                                pictures.add(stateListModel);
                            }
                        }

                        imageAdapter = new ImageAdapter(this, pictures);
                        viewPager.setAdapter(imageAdapter);
                        imageAdapter.notifyDataSetChanged();

                        image_video_adapter = new Image_Video_Adapter(this, pictures, this);
                        list_images_videos.setAdapter(image_video_adapter);
                        list_images_videos.setLayoutManager(new LinearLayoutManager(this,  LinearLayout.HORIZONTAL, false));
                        image_video_adapter.notifyDataSetChanged();

                        JSONArray jsonArray1 = jsonObject1.getJSONArray("UploadRevenueMapFile");
                        if(jsonArray1.length()!=0){
                            for(int i = 0; i<jsonArray1.length(); i++){
                                JSONObject object = jsonArray1.getJSONObject(i);
                                StateListModel.Pictures stateListModel = new StateListModel.Pictures();
                                stateListModel.setId(object.getString("Id"));
                                stateListModel.setUrl(Urls.BASE_CONTACT_IMAGE_URL +object.getString("Url"));
                                pictures1.add(stateListModel);
                            }
                        }

                        stateImageAdapter = new StateImageAdapter(this, pictures1);
                        mohali_photos.setAdapter(stateImageAdapter);
                        mohali_photos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                        stateImageAdapter.notifyDataSetChanged();

                        if (pictures1.size() == 0) {
                            mohali_photos.setVisibility(View.GONE);
                            no_data2.setVisibility(View.VISIBLE);
                        }else{
                            mohali_photos.setVisibility(View.VISIBLE);
                            no_data2.setVisibility(View.GONE);
                        }

                        JSONArray jsonArray2 = jsonObject1.getJSONArray("KeyDistances");
                        if(jsonArray2.length()!=0){
                            for(int i = 0; i<jsonArray2.length(); i++){
                                JSONObject object = jsonArray2.getJSONObject(i);
                                KeydistanceModel keydistanceModel = new KeydistanceModel();
                                keydistanceModel.setKey(object.getString("Key"));
                                keydistanceModel.setValue(object.getString("Value"));
                                keydistanceModel.setValue1(object.getString("Value1"));
                                keydistanceModel.setValue2(object.getString("Value2"));
                                featureData.add(keydistanceModel);
                            }
                        }

                        featuresAdapter = new FeaturesAdapter(this, featureData);
                        features.setAdapter(featuresAdapter);
                        features.setLayoutManager(new LinearLayoutManager(this));
                        featuresAdapter.notifyDataSetChanged();

                        if(featureData.size() == 0){
                            features.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                        }else{
                            features.setVisibility(View.VISIBLE);
                            no_data.setVisibility(View.GONE);
                        }

                        JSONArray jsonArray3 = jsonObject1.getJSONArray("RecreationalAreas");
                        if(jsonArray3.length() != 0){
                            for(int i = 0; i < jsonArray3.length(); i++){
                                JSONObject object = jsonArray3.getJSONObject(i);
                                RecreationalModel recreationalModel = new RecreationalModel();
                                recreationalModel.setId(object.getString("Id"));
                                recreationalModel.setKey(object.getString("Key"));
                                recreationalModel.setValue(object.getString("Value"));
                                recreationalModels.add(recreationalModel);
                            }
                        }

                        recreationalAdapter = new RecreationalAdapter(this, recreationalModels);
                        recreational_area.setAdapter(recreationalAdapter);
                        recreational_area.setLayoutManager(new GridLayoutManager(this, 2));
                        recreationalAdapter.notifyDataSetChanged();

                        if(recreationalModels.size() == 0){
                            recreational_area.setVisibility(View.GONE);
                            no_data1.setVisibility(View.VISIBLE);
                        }else{
                            recreational_area.setVisibility(View.VISIBLE);
                            no_data1.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void submitReqPopUp(String url){
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.submit_requirement_popup);
        close = (ImageView) dialog.findViewById(R.id.close);
        img = (ImageView) dialog.findViewById(R.id.img);
        Picasso.with(this).load(url).into(img);
        project_name = (TextView) dialog.findViewById(R.id.name);
        project_name.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Locality", ""));
        description = (TextView) dialog.findViewById(R.id.description);
        description.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Dist", "") + PreferenceConnector.getInstance(this).loadSavedPreferences("Stating", ""));
        yes = (RadioButton) dialog.findViewById(R.id.yes);
        no = (RadioButton) dialog.findViewById(R.id.no);
        buy = (RadioButton) dialog.findViewById(R.id.buy);
        rent = (RadioButton) dialog.findViewById(R.id.rent);
        property_detailing = (LinearLayout) dialog.findViewById(R.id.property_detailing);
        category_spinner = (Spinner) dialog.findViewById(R.id.category_spinner);
        property_spinner = (Spinner) dialog.findViewById(R.id.property_spinner);
        check_studio = (CheckBox) dialog.findViewById(R.id.check_studio);
        check_2bhk = (CheckBox) dialog.findViewById(R.id.check_2bhk);
        check_3bhk = (CheckBox) dialog.findViewById(R.id.check_3bhk);
        check_4bhk = (CheckBox) dialog.findViewById(R.id.check_4bhk);
        message = (EditText) dialog.findViewById(R.id.message);
        datePicker = (EditText) dialog.findViewById(R.id.datePicker);
        media = (EditText) dialog.findViewById(R.id.media);
        submit = (Button) dialog.findViewById(R.id.submit);

        yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    property_detailing.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    startActivity(new Intent(SectorDetail.this, Submit_Requirement.class));
                }
            }
        });

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }

    private void propertySell_RentPopUp(String url) {
        dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.property_sell_rent);
        close1 = (ImageView) dialog1.findViewById(R.id.close);
        image = (ImageView) dialog1.findViewById(R.id.image);
        Picasso.with(this).load(url).into(image);
        project_name = (TextView) dialog1.findViewById(R.id.project_name);
        project_name.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Locality", ""));
        desc = (TextView) dialog1.findViewById(R.id.desc);
        desc.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Dist", "") + PreferenceConnector.getInstance(this).loadSavedPreferences("Stating", ""));
        yes1 = (RadioButton) dialog1.findViewById(R.id.yes);
        no1 = (RadioButton) dialog1.findViewById(R.id.no);
        sell = (RadioButton) dialog1.findViewById(R.id.sell);
        rent1 = (RadioButton) dialog1.findViewById(R.id.rent);
        property_detailing1 = (LinearLayout) dialog1.findViewById(R.id.property_detailing);
        category_spinner1 = (Spinner) dialog1.findViewById(R.id.category_spinner);
        property_spinner1 = (Spinner) dialog1.findViewById(R.id.property_spinner);
        tower_name_no = (EditText) dialog1.findViewById(R.id.tower_name_no);
        unit_no = (EditText) dialog1.findViewById(R.id.unit_no);
        message1 = (EditText) dialog1.findViewById(R.id.message1);
        datePick = (EditText) dialog1.findViewById(R.id.datePick);
        media1 = (EditText) dialog1.findViewById(R.id.media);
        submit1 = (Button) dialog1.findViewById(R.id.submit);

        yes1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    property_detailing1.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    //startActivity(new Intent(getContext(), Submit_Requirement.class));

                }
            }
        });

        dialog1.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog1.show();

        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.cancel();
            }
        });
    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }


}
