package com.bnhabitat.areaModule.areaActivities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.bnhabitat.R;

public class BaseActivity_Area extends FragmentActivity { //changed from depricated ActionBarActivity
    private DrawerLayout mDrawerLayout;
    //  private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;


    private CharSequence mTitle;
    Toolbar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_filter_property);
        set();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getSupportMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        if (item.getItemId() == android.R.id.home) {
//            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
//                mDrawerLayout.closeDrawer(mDrawerList);
//            } else {
//                mDrawerLayout.openDrawer(mDrawerList);
//            }
//        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        // boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        // menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    void setupDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }



    public void closeDrawer(Activity activity) {
        // mDrawerLayout.closeDrawer(GravityCompat.END);

        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {

            mDrawerLayout.closeDrawers();

        } else {

            Intent it = new Intent(activity, Township_Subarea_Project.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(it);
            finishAffinity();
            // mDrawerLayout.openDrawer(Gravity.LEFT); //OPEN Nav Drawer!
        }


    }


    public void set() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        LinearLayout lnrCell1 =(LinearLayout) findViewById(R.id.lnrCell1);
        LinearLayout lnrCell2 =(LinearLayout) findViewById(R.id.lnrCell2);
        LinearLayout lnrCell3 =(LinearLayout) findViewById(R.id.lnrCell3);

        /*lnrCell1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(BaseActivity_Area.this,ForSell_activities.class);
                startActivity(it);

            }
        });*/

//        recyclerView = (RecyclerView) findViewById(R.id.drawerList);
//
//
//        adapter = new NavigationDrawerAdapter(this, getData());
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        setupDrawerToggle();

    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(Gravity.RIGHT);

        //   adapter.setMembershipValue(this);
    }

}
