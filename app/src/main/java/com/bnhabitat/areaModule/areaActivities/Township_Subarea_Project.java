package com.bnhabitat.areaModule.areaActivities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.ImageAdapter;
import com.bnhabitat.areaModule.fragment.Project_List_Fragment;
import com.bnhabitat.areaModule.fragment.Project_Listing_Fragment;
import com.bnhabitat.areaModule.fragment.Property_Listing_Fragment;
import com.bnhabitat.areaModule.fragment.Subarea_List_Fragment;
import com.bnhabitat.areaModule.fragment.Township_List_Fragment;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.township.townactivities.AddSectorLocality;
import com.bnhabitat.township.townactivities.Add_Project;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.utils.PreferenceConnector;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;


public class Township_Subarea_Project extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private View actionB,action_A, action_c, action_d, action_e, action_f, action_g;

    TabLayout.Tab Tab1, Tab2, Tab3, Tab4;
    TabLayout tabLayout;
    DrawerLayout drawer_layout;

    String townshipCount, sectorCount, projectCount, propertyCount;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    private GoogleApiClient mGoogleApiClient;
    //private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private Boolean mRequestingLocationUpdates;

    ImageView imgBack, iv_location, imgFilter, residential_drop, residential_up, commercial_drop, commercial_up, forSell_residential_drop, forSell_residential_up, forSell_drop, forSell_up;
    LinearLayout residential_lay, forSell_lay_res, commercial_lay, forSell_lay;
    ScrollView scroll_filter;
    static int flagCabin_res = 0;
    String strCabin_res;
    static int flagBathroom_res = 0;
    String strBathroom_res;
    static int flagPentry_res = 0;
    String strPentry_res;

    TextView txtMinValue_res, txtMaxValue_res, txtMinValueSquareFeetSelected_res, txtMaxValueSquareFeetSelected_res, txtMinValueLotSizeSelected_res, txtMaxValueLotSizeSelected_res;
    TextView txtMinValue_com, txtMaxValue_com, txtMinValueSquareFeetSelected_com, txtMaxValueSquareFeetSelected_com, txtMinValueLotSizeSelected_com, txtMaxValueLotSizeSelected_com;

    Number minCost=0, maxCost=1000000;
    Number minCost_sfs=500, maxCost_sfs=80000;
    Number minLot_res=500, maxLot_res=80000;

    static String strMin = "10000";
    static String strMax = "10000000";

    static String strMin_sfs = "500";
    static String strMax_sfs = "80000";

    static String strMin_lot_res = "500";
    static String strMax_lot_res = "80000";

    CrystalRangeSeekbar rangeSeekbar_res, squareFeetRate_res, LotSize_res;
    CrystalRangeSeekbar rangeSeekbar_com, squareFeetRate_com, LotSize_com;

    Number minCost_com=0, maxCost_com=1000000;
    Number minCost_sfs_com=500, maxCost_sfs_com=80000;
    Number minLot_com=500, maxLot_com=80000;

    static String strMin_com = "10000";
    static String strMax_com = "10000000";

    static String strMin_sfs_com = "500";
    static String strMax_sfs_com = "80000";

    static String strMin_lot_com = "500";
    static String strMax_lot_com = "80000";

    static int flagCabin = 0;
    String strCabin;
    static int flagBathroom = 0;
    String strBathroom;
    static int flagPentry = 0;
    String strPentry;

    Button decCabin_res, incCabin_res, decBathroom_res, incBathroom_res, decPentry_res, incPentry_res, decCabin, incCabin, decBathroom, incBathroom, decPentry, incPentry;
    TextView name_city, cabin_text_res, bathroom_text_res, pentry_text_res,cabin_text, bathroom_text, pentry_text;

    SwitchCompat switchButton1, switchButton2, switchButton3, switchButton4, switchButton5, switchforsell_residential, switchforsell;

    Button buy_res, rent_res, pg_res, any_res, freehold_res, lease_res;
    Button buy_com, id_lease_com, lease_com, any_com, freehold_com, lease_owner_com;

    LinearLayout submit_requirement_lay;

    String name, id, stateId, districtId;

    Dialog dialog;
    ImageView close;
    ViewPager viewPager;
    ImageAdapter imageAdapter;
    FloatingActionsMenu floatingActionsMenu;
    Township_List_Fragment township_list_fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_township__subarea__project);

        if(savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id= null;
                name= null;
                stateId = null;
                districtId = null;
            } else {
                id= extras.getString("CityId");
                name= extras.getString("CityName");
                stateId = extras.getString("StateId");
                districtId = extras.getString("DistrictId");
                townshipCount = extras.getString("TownshipCount");
                sectorCount = extras.getString("SectorCount");
                projectCount = extras.getString("ProjectCount");
                propertyCount = extras.getString("PropertiesCount");
            }
        } else {
            id= (String) savedInstanceState.getSerializable("CityId");
            name= (String) savedInstanceState.getSerializable("CityName");
            stateId= (String) savedInstanceState.getSerializable("StateId");
            districtId= (String) savedInstanceState.getSerializable("DistrictId");
        }

        initialization();
        Tab1 = tabLayout.newTab().setText("Township");
        Tab2 = tabLayout.newTab().setText("Locality");
        Tab3 = tabLayout.newTab().setText("Projects");
        Tab4 = tabLayout.newTab().setText("Properties");
        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
        tabLayout.addTab(Tab3);
        tabLayout.addTab(Tab4);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Bundle bundle = new Bundle();
        bundle.putString("CityId", id);
        bundle.putString("CityName", name);
        bundle.putString("StateId", stateId);
        bundle.putString("DistrictId", districtId);
        bundle.putString("townshipCount", townshipCount);
        bundle.putString("sectorCount", sectorCount);
        bundle.putString("projectCount", projectCount);
        bundle.putString("propertyCount", propertyCount);
        iv_location.setVisibility(View.GONE);

        township_list_fragment = new Township_List_Fragment();
        township_list_fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_area_listing, township_list_fragment).commit();

        tabLayout.getTabAt(0);
        Tab1.select();

        bindWidgetsWithAnEvent();

        /*action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });

        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(Township_Subarea_Project.this, Add_Project.class);
                startActivity(intent);

            }
        });

        action_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(Township_Subarea_Project.this, Add_township.class);
                startActivity(intent);

            }
        });

        action_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(Township_Subarea_Project.this, AddSectorLocality.class);
                startActivity(intent);

            }
        });

        action_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });

        action_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });

        action_g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(Township_Subarea_Project.this, Submit_Requirement.class);
                startActivity(intent);

            }
        });

        scroll_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(Township_Subarea_Project.this);
            }
        });

        switchButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    residential_drop.setVisibility(View.GONE);
                    residential_up.setVisibility(View.VISIBLE);
                    residential_lay.setVisibility(View.VISIBLE);
                }
                else{
                    residential_drop.setVisibility(View.VISIBLE);
                    residential_up.setVisibility(View.GONE);
                    residential_lay.setVisibility(View.GONE);
                }
            }
        });


        switchforsell_residential.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    forSell_residential_drop.setVisibility(View.GONE);
                    forSell_residential_up.setVisibility(View.VISIBLE);
                    forSell_lay_res.setVisibility(View.VISIBLE);
                }
                else{
                    forSell_residential_drop.setVisibility(View.VISIBLE);
                    forSell_residential_up.setVisibility(View.GONE);
                    forSell_lay_res.setVisibility(View.GONE);
                }
            }
        });

        decCabin_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagCabin_res > 0)
                    flagCabin_res--;
                strCabin_res = "TotalRooms%20eq%20" + flagCabin_res + "%20and%20";
                cabin_text_res.setText(String.valueOf(flagCabin_res));

            }
        });

        incCabin_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagCabin_res++;
                strCabin_res = "TotalRooms%20eq%20" + flagCabin_res + "%20and%20";
                cabin_text_res.setText(String.valueOf(flagCabin_res));
            }
        });

        decBathroom_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBathroom_res > 0)
                    flagBathroom_res--;
                strBathroom_res = "TotalRooms%20eq%20" + flagBathroom_res + "%20and%20";
                bathroom_text_res.setText(String.valueOf(flagBathroom_res));

            }
        });

        incBathroom_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBathroom_res++;
                strBathroom_res = "TotalRooms%20eq%20" + flagBathroom_res + "%20and%20";
                bathroom_text_res.setText(String.valueOf(flagBathroom_res));
            }
        });

        decPentry_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagPentry_res > 0)
                    flagPentry_res--;
                strPentry_res = "TotalRooms%20eq%20" + flagPentry_res + "%20and%20";
                pentry_text_res.setText(String.valueOf(flagPentry_res));

            }
        });

        incPentry_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagPentry_res++;
                strPentry_res = "TotalRooms%20eq%20" + flagPentry_res + "%20and%20";
                pentry_text_res.setText(String.valueOf(flagPentry_res));
            }
        });

        rangeSeekbar_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin = String.valueOf(minValue);
                strMax = String.valueOf(maxValue);

                txtMinValue_res.setText("$" + String.valueOf(minValue));
                txtMaxValue_res.setText("$" + String.valueOf(maxValue));

                minCost=minValue;
                maxCost=maxValue;

                Log.e("TAG", "minCost: "+minCost+ " maxCost: "+maxCost );

            }
        });

        squareFeetRate_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_sfs = String.valueOf(minValue);
                strMax_sfs = String.valueOf(maxValue);

                txtMinValueSquareFeetSelected_res.setText("$" + String.valueOf(minValue));
                txtMaxValueSquareFeetSelected_res.setText("$" + String.valueOf(maxValue));

                minCost_sfs=minValue;
                maxCost_sfs=maxValue;

                Log.e("TAG", "minCost: "+minCost_sfs+ " maxCost: "+maxCost_sfs );

            }
        });

        LotSize_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_lot_res = String.valueOf(minValue);
                strMax_lot_res = String.valueOf(maxValue);

                txtMinValueLotSizeSelected_res.setText("$" + String.valueOf(minValue));
                txtMaxValueLotSizeSelected_res.setText("$" + String.valueOf(maxValue));

                minLot_res=minValue;
                maxLot_res=maxValue;

                Log.e("TAG", "minCost: "+minLot_res+ " maxCost: "+maxLot_res );

            }
        });



        buy_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buy_res.setBackgroundResource(R.drawable.left_button_selected);
                buy_res.setTextColor(getResources().getColor(R.color.white));
                rent_res.setSelected(false);
                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
                pg_res.setSelected(false);
                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        rent_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rent_res.setBackgroundResource(R.drawable.center_button_selected);
                rent_res.setTextColor(getResources().getColor(R.color.white));
                buy_res.setSelected(false);
                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
                pg_res.setSelected(false);
                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        pg_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pg_res.setBackgroundResource(R.drawable.right_button_selected);
                pg_res.setTextColor(getResources().getColor(R.color.white));
                buy_res.setSelected(false);
                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
                rent_res.setSelected(false);
                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        any_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                any_res.setBackgroundResource(R.drawable.left_button_selected);
                any_res.setTextColor(getResources().getColor(R.color.white));
                freehold_res.setSelected(false);
                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
                lease_res.setSelected(false);
                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        freehold_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freehold_res.setBackgroundResource(R.drawable.center_button_selected);
                freehold_res.setTextColor(getResources().getColor(R.color.white));
                any_res.setSelected(false);
                any_res.setBackgroundResource(R.drawable.left_button_deselected);
                any_res.setTextColor(getResources().getColor(R.color.app_blue));
                lease_res.setSelected(false);
                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_res.setBackgroundResource(R.drawable.right_button_selected);
                lease_res.setTextColor(getResources().getColor(R.color.white));
                any_res.setSelected(false);
                any_res.setBackgroundResource(R.drawable.left_button_deselected);
                any_res.setTextColor(getResources().getColor(R.color.app_blue));
                freehold_res.setSelected(false);
                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });


        switchButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    commercial_drop.setVisibility(View.GONE);
                    commercial_up.setVisibility(View.VISIBLE);
                    commercial_lay.setVisibility(View.VISIBLE);
                }
                else{
                    commercial_drop.setVisibility(View.VISIBLE);
                    commercial_up.setVisibility(View.GONE);
                    commercial_lay.setVisibility(View.GONE);
                }
            }
        });



        switchforsell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    forSell_drop.setVisibility(View.GONE);
                    forSell_up.setVisibility(View.VISIBLE);
                    forSell_lay.setVisibility(View.VISIBLE);
                }
                else{
                    forSell_drop.setVisibility(View.VISIBLE);
                    forSell_up.setVisibility(View.GONE);
                    forSell_lay.setVisibility(View.GONE);
                }
            }
        });


        decCabin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagCabin > 0)
                    flagCabin--;
                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
                cabin_text.setText(String.valueOf(flagCabin));

            }
        });

        incCabin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagCabin++;
                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
                cabin_text.setText(String.valueOf(flagCabin));
            }
        });

        decBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBathroom > 0)
                    flagBathroom--;
                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
                bathroom_text.setText(String.valueOf(flagBathroom));

            }
        });

        incBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBathroom++;
                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
                bathroom_text.setText(String.valueOf(flagBathroom));
            }
        });



        decPentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagPentry > 0)
                    flagPentry--;
                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
                pentry_text.setText(String.valueOf(flagPentry));

            }
        });

        incPentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagPentry++;
                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
                pentry_text.setText(String.valueOf(flagPentry));
            }
        });



        rangeSeekbar_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_com = String.valueOf(minValue);
                strMax_com = String.valueOf(maxValue);

                txtMinValue_com.setText("$" + String.valueOf(minValue));
                txtMaxValue_com.setText("$" + String.valueOf(maxValue));

                minCost_com=minValue;
                maxCost_com=maxValue;

                Log.e("TAG", "minCost: "+minCost_com+ " maxCost: "+maxCost_com );

            }
        });


        squareFeetRate_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_sfs_com = String.valueOf(minValue);
                strMax_sfs_com = String.valueOf(maxValue);

                txtMinValueSquareFeetSelected_com.setText("$" + String.valueOf(minValue));
                txtMaxValueSquareFeetSelected_com.setText("$" + String.valueOf(maxValue));

                minCost_sfs_com=minValue;
                maxCost_sfs_com=maxValue;

                Log.e("TAG", "minCost: "+minCost_sfs_com+ " maxCost: "+maxCost_sfs_com );

            }
        });



        LotSize_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_lot_com = String.valueOf(minValue);
                strMax_lot_com = String.valueOf(maxValue);

                txtMinValueLotSizeSelected_com.setText("$" + String.valueOf(minValue));
                txtMaxValueLotSizeSelected_com.setText("$" + String.valueOf(maxValue));

                minLot_com=minValue;
                maxLot_com=maxValue;

                Log.e("TAG", "minCost: "+minLot_com+ " maxCost: "+maxLot_com );

            }
        });



        buy_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buy_com.setBackgroundResource(R.drawable.left_button_selected);
                buy_com.setTextColor(getResources().getColor(R.color.white));
                id_lease_com.setSelected(false);
                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_com.setSelected(false);
                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        id_lease_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id_lease_com.setBackgroundResource(R.drawable.center_button_selected);
                id_lease_com.setTextColor(getResources().getColor(R.color.white));
                buy_com.setSelected(false);
                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_com.setSelected(false);
                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_com.setBackgroundResource(R.drawable.right_button_selected);
                lease_com.setTextColor(getResources().getColor(R.color.white));
                buy_com.setSelected(false);
                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
                id_lease_com.setSelected(false);
                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });


        any_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                any_com.setBackgroundResource(R.drawable.left_button_selected);
                any_com.setTextColor(getResources().getColor(R.color.white));
                freehold_com.setSelected(false);
                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_owner_com.setSelected(false);
                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        freehold_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freehold_com.setBackgroundResource(R.drawable.center_button_selected);
                freehold_com.setTextColor(getResources().getColor(R.color.white));
                any_com.setSelected(false);
                any_com.setBackgroundResource(R.drawable.left_button_deselected);
                any_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_owner_com.setSelected(false);
                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_owner_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_owner_com.setBackgroundResource(R.drawable.right_button_selected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.white));
                any_com.setSelected(false);
                any_com.setBackgroundResource(R.drawable.left_button_deselected);
                any_com.setTextColor(getResources().getColor(R.color.app_blue));
                freehold_com.setSelected(false);
                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        submit_requirement_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Township_Subarea_Project.this, Submit_Requirement.class));
            }
        });*/








        /*imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDrawer();
            }
        });


        iv_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<StateListModel.Pictures> pictures = township_list_fragment.getCityArrayList();

                mapPopup(pictures);
            }
        });*/



    }

    @Override
    protected void onStart() {

        onClicks();
        name_city.setText(name);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.transparent));
                //floatingActionsMenu.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("ffffff")));
            }

            @SuppressLint("ResourceType")
            @Override
            public void onMenuCollapsed() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.tran));

            }
        });
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawer_layout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

                hideSoftKeyboard(Township_Subarea_Project.this);

            }
        });

        //set();
        super.onStart();
    }

    private void initialization() {
        name_city = (TextView) findViewById(R.id.name_city);
        floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        action_c = findViewById(R.id.action_c);
        action_d = findViewById(R.id.action_d);
        action_e= findViewById(R.id.action_e);
        action_f = findViewById(R.id.action_f);
        action_g = findViewById(R.id.action_g);
//        scroll_filter = (ScrollView) findViewById(R.id.scroll_filter);
//        residential_lay = (LinearLayout) findViewById(R.id.residential_lay);
//        residential_drop = (ImageView) findViewById(R.id.residential_drop);
//        residential_up = (ImageView) findViewById(R.id.residential_up);
//        switchButton1 = (SwitchCompat) findViewById(R.id.switchButton1);
//        forSell_lay_res = (LinearLayout) findViewById(R.id.forSell_lay_residential);
//        forSell_residential_drop = (ImageView) findViewById(R.id.forSell_residential_drop);
//        forSell_residential_up = (ImageView) findViewById(R.id.forSell_residential_up);
//        switchforsell_residential = (SwitchCompat) findViewById(R.id.switchforsell_residential);
//        decCabin_res = (Button) findViewById(R.id.decCabin_res);
//        incCabin_res = (Button) findViewById(R.id.incCabin_res);
//        cabin_text_res = (TextView) findViewById(R.id.cabin_text_res);
//        decBathroom_res = (Button) findViewById(R.id.decBathroom_res);
//        incBathroom_res = (Button) findViewById(R.id.incBathroom_res);
//        bathroom_text_res = (TextView) findViewById(R.id.bathroom_text_res);
//        decPentry_res = (Button) findViewById(R.id.decPentry_res);
//        incPentry_res = (Button) findViewById(R.id.incPentry_res);
//        pentry_text_res = (TextView) findViewById(R.id.pentry_text_res);
//        txtMinValue_res = (TextView) findViewById(R.id.txtMinValue_res);
//        txtMaxValue_res = (TextView) findViewById(R.id.txtMaxValue_res);
//        rangeSeekbar_res = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar_res);
//        txtMinValueSquareFeetSelected_res = (TextView) findViewById(R.id.txtMinValueSquareFeetSelected_res);
//        txtMaxValueSquareFeetSelected_res = (TextView) findViewById(R.id.txtMaxValueSquareFeetSelected_res);
//        squareFeetRate_res = (CrystalRangeSeekbar) findViewById(R.id.squareFeetRate_res);
//        any_res = (Button) findViewById(R.id.any_res);
//        freehold_res = (Button) findViewById(R.id.freehold_res);
//        lease_res = (Button) findViewById(R.id.lease_res);
//        buy_res = (Button) findViewById(R.id.buy_res);
//        rent_res = (Button) findViewById(R.id.rent_res);
//        pg_res = (Button) findViewById(R.id.pg_res);
//        txtMinValueLotSizeSelected_res = (TextView) findViewById(R.id.txtMinValueLotSizeSelected_res);
//        txtMaxValueLotSizeSelected_res = (TextView) findViewById(R.id.txtMaxValueLotSizeSelected_res);
//        LotSize_res = (CrystalRangeSeekbar) findViewById(R.id.LotSize_res);
//        commercial_lay = (LinearLayout) findViewById(R.id.commercial_lay);
//        commercial_drop = (ImageView) findViewById(R.id.commercial_drop);
//        commercial_up = (ImageView) findViewById(R.id.commercial_up);
//        switchButton2 = (SwitchCompat) findViewById(R.id.switchButton2);
//        forSell_lay = (LinearLayout) findViewById(R.id.forSell_lay);
//        forSell_drop = (ImageView) findViewById(R.id.forSell_drop);
//        forSell_up = (ImageView) findViewById(R.id.forSell_up);
//        switchforsell = (SwitchCompat) findViewById(R.id.switchforsell);
//        decCabin = (Button) findViewById(R.id.decCabin);
//        incCabin = (Button) findViewById(R.id.incCabin);
//        cabin_text = (TextView) findViewById(R.id.cabin_text);
//        decBathroom = (Button) findViewById(R.id.decBathroom);
//        incBathroom = (Button) findViewById(R.id.incBathroom);
//        bathroom_text = (TextView) findViewById(R.id.bathroom_text);
//        decPentry = (Button) findViewById(R.id.decPentry);
//        incPentry = (Button) findViewById(R.id.incPentry);
//        pentry_text = (TextView) findViewById(R.id.pentry_text);
//        txtMinValue_com = (TextView) findViewById(R.id.txtMinValue_com);
//        txtMaxValue_com = (TextView) findViewById(R.id.txtMaxValue_com);
//        rangeSeekbar_com = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar_com);
//        txtMinValueSquareFeetSelected_com = (TextView) findViewById(R.id.txtMinValueSquareFeetSelected_com);
//        txtMaxValueSquareFeetSelected_com = (TextView) findViewById(R.id.txtMaxValueSquareFeetSelected_com);
//        squareFeetRate_com = (CrystalRangeSeekbar) findViewById(R.id.squareFeetRate_com);
//        txtMinValueLotSizeSelected_com = (TextView) findViewById(R.id.txtMinValueLotSizeSelected_com);
//        txtMaxValueLotSizeSelected_com = (TextView) findViewById(R.id.txtMaxValueLotSizeSelected_com);
//        LotSize_com = (CrystalRangeSeekbar) findViewById(R.id.LotSize_com);
//        buy_com = (Button) findViewById(R.id.buy_com);
//        id_lease_com = (Button) findViewById(R.id.id_lease_com);
//        lease_com = (Button) findViewById(R.id.lease_com);
//        any_com = (Button) findViewById(R.id.any_com);
//        freehold_com = (Button) findViewById(R.id.freehold_com);
//        lease_owner_com = (Button) findViewById(R.id.lease_owner_com);
//        submit_requirement_lay = (LinearLayout) findViewById(R.id.submit_requirement_lay);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        iv_location = (ImageView) findViewById(R.id.iv_location);
        //iv_location.setVisibility(View.VISIBLE);
        imgFilter = (ImageView) findViewById(R.id.imgFilter);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
    }

    private void onClicks() {

        imgBack.setOnClickListener(this);

        //imgFilter.setOnClickListener(this);

        iv_location.setOnClickListener(this);

        action_A.setOnClickListener(this);

        actionB.setOnClickListener(this);

        action_c.setOnClickListener(this);

        action_d.setOnClickListener(this);

        action_e.setOnClickListener(this);

        action_f.setOnClickListener(this);

        action_g.setOnClickListener(this);

//        decCabin.setOnClickListener(this);
//
//        incCabin.setOnClickListener(this);
//
//        decBathroom.setOnClickListener(this);
//
//        incBathroom.setOnClickListener(this);
//
//        decPentry.setOnClickListener(this);
//
//        incPentry.setOnClickListener(this);
//
//        scroll_filter.setOnClickListener(this);
//
//        buy_com.setOnClickListener(this);
//
//        id_lease_com.setOnClickListener(this);
//
//        lease_com.setOnClickListener(this);
//
//        any_com.setOnClickListener(this);
//
//        freehold_com.setOnClickListener(this);
//
//        lease_owner_com.setOnClickListener(this);
//
//        submit_requirement_lay.setOnClickListener(this);
//
//        buy_res.setOnClickListener(this);
//
//        rent_res.setOnClickListener(this);
//
//        pg_res.setOnClickListener(this);
//
//        any_res.setOnClickListener(this);
//
//        freehold_res.setOnClickListener(this);
//
//        lease_res.setOnClickListener(this);
//
//        decCabin_res.setOnClickListener(this);
//
//        incCabin_res.setOnClickListener(this);
//
//        decBathroom_res.setOnClickListener(this);
//
//        incBathroom_res.setOnClickListener(this);
//
//        decPentry_res.setOnClickListener(this);
//
//        incPentry_res.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.imgBack:
                finish();
                break;
//            case R.id.imgFilter:
//                openDrawer();
//                break;
            case R.id.iv_location:
                ArrayList<StateListModel.Pictures> pictures = township_list_fragment.getCityArrayList();
                mapPopup(pictures);
                break;
            case R.id.action_a:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_b:
                floatingActionsMenu.collapse();
                intent = new Intent(Township_Subarea_Project.this, Add_Project.class);
                startActivity(intent);
                break;
            case R.id.action_c:
                floatingActionsMenu.collapse();
                intent = new Intent(Township_Subarea_Project.this, Add_township.class);
                startActivity(intent);
                break;
            case R.id.action_d:
                floatingActionsMenu.collapse();
                intent = new Intent(Township_Subarea_Project.this, AddSectorLocality.class);
                startActivity(intent);
                break;
            case R.id.action_e:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_f:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_g:
                floatingActionsMenu.collapse();
                intent = new Intent(Township_Subarea_Project.this, Submit_Requirement.class);
                startActivity(intent);
                break;
//            case R.id.decCabin:
//                if (flagCabin > 0)
//                    flagCabin--;
//                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
//                cabin_text.setText(String.valueOf(flagCabin));
//                break;
//            case R.id.incCabin:
//                flagCabin++;
//                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
//                cabin_text.setText(String.valueOf(flagCabin));
//                break;
//            case R.id.decBathroom:
//                if (flagBathroom > 0)
//                    flagBathroom--;
//                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
//                bathroom_text.setText(String.valueOf(flagBathroom));
//                break;
//            case R.id.incBathroom:
//                flagBathroom++;
//                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
//                bathroom_text.setText(String.valueOf(flagBathroom));
//                break;
//            case R.id.decPentry:
//                if (flagPentry > 0)
//                    flagPentry--;
//                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
//                pentry_text.setText(String.valueOf(flagPentry));
//                break;
//            case R.id.incPentry:
//                flagPentry++;
//                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
//                pentry_text.setText(String.valueOf(flagPentry));
//                break;
//            case R.id.scroll_filter:
//                hideSoftKeyboard(Township_Subarea_Project.this);
//                break;
//            case R.id.buy_com:
//                buy_com.setBackgroundResource(R.drawable.left_button_selected);
//                buy_com.setTextColor(getResources().getColor(R.color.white));
//                id_lease_com.setSelected(false);
//                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
//                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_com.setSelected(false);
//                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.id_lease_com:
//                id_lease_com.setBackgroundResource(R.drawable.center_button_selected);
//                id_lease_com.setTextColor(getResources().getColor(R.color.white));
//                buy_com.setSelected(false);
//                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
//                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_com.setSelected(false);
//                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.lease_com:
//                lease_com.setBackgroundResource(R.drawable.right_button_selected);
//                lease_com.setTextColor(getResources().getColor(R.color.white));
//                buy_com.setSelected(false);
//                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
//                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
//                id_lease_com.setSelected(false);
//                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
//                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.any_com:
//                any_com.setBackgroundResource(R.drawable.left_button_selected);
//                any_com.setTextColor(getResources().getColor(R.color.white));
//                freehold_com.setSelected(false);
//                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
//                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_owner_com.setSelected(false);
//                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.freehold_com:
//                freehold_com.setBackgroundResource(R.drawable.center_button_selected);
//                freehold_com.setTextColor(getResources().getColor(R.color.white));
//                any_com.setSelected(false);
//                any_com.setBackgroundResource(R.drawable.left_button_deselected);
//                any_com.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_owner_com.setSelected(false);
//                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.lease_owner_com:
//                lease_owner_com.setBackgroundResource(R.drawable.right_button_selected);
//                lease_owner_com.setTextColor(getResources().getColor(R.color.white));
//                any_com.setSelected(false);
//                any_com.setBackgroundResource(R.drawable.left_button_deselected);
//                any_com.setTextColor(getResources().getColor(R.color.app_blue));
//                freehold_com.setSelected(false);
//                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
//                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.submit_requirement_lay:
//                startActivity(new Intent(Township_Subarea_Project.this, Submit_Requirement.class));
//                break;
//            case R.id.buy_res:
//                buy_res.setBackgroundResource(R.drawable.left_button_selected);
//                buy_res.setTextColor(getResources().getColor(R.color.white));
//                rent_res.setSelected(false);
//                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
//                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
//                pg_res.setSelected(false);
//                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
//                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.rent_res:
//                rent_res.setBackgroundResource(R.drawable.center_button_selected);
//                rent_res.setTextColor(getResources().getColor(R.color.white));
//                buy_res.setSelected(false);
//                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
//                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
//                pg_res.setSelected(false);
//                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
//                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.pg_res:
//                pg_res.setBackgroundResource(R.drawable.right_button_selected);
//                pg_res.setTextColor(getResources().getColor(R.color.white));
//                buy_res.setSelected(false);
//                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
//                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
//                rent_res.setSelected(false);
//                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
//                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.any_res:
//                any_res.setBackgroundResource(R.drawable.left_button_selected);
//                any_res.setTextColor(getResources().getColor(R.color.white));
//                freehold_res.setSelected(false);
//                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
//                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_res.setSelected(false);
//                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.freehold_res:
//                freehold_res.setBackgroundResource(R.drawable.center_button_selected);
//                freehold_res.setTextColor(getResources().getColor(R.color.white));
//                any_res.setSelected(false);
//                any_res.setBackgroundResource(R.drawable.left_button_deselected);
//                any_res.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_res.setSelected(false);
//                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.lease_res:
//                lease_res.setBackgroundResource(R.drawable.right_button_selected);
//                lease_res.setTextColor(getResources().getColor(R.color.white));
//                any_res.setSelected(false);
//                any_res.setBackgroundResource(R.drawable.left_button_deselected);
//                any_res.setTextColor(getResources().getColor(R.color.app_blue));
//                freehold_res.setSelected(false);
//                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
//                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.decCabin_res:
//                if (flagCabin_res > 0)
//                    flagCabin_res--;
//                strCabin_res = "TotalRooms%20eq%20" + flagCabin_res + "%20and%20";
//                cabin_text_res.setText(String.valueOf(flagCabin_res));
//                break;
//            case R.id.incCabin_res:
//                flagCabin_res++;
//                strCabin_res = "TotalRooms%20eq%20" + flagCabin_res + "%20and%20";
//                cabin_text_res.setText(String.valueOf(flagCabin_res));
//                break;
//            case R.id.decBathroom_res:
//                if (flagBathroom_res > 0)
//                    flagBathroom_res--;
//                strBathroom_res = "TotalRooms%20eq%20" + flagBathroom_res + "%20and%20";
//                bathroom_text_res.setText(String.valueOf(flagBathroom_res));
//                break;
//            case R.id.incBathroom_res:
//                flagBathroom_res++;
//                strBathroom_res = "TotalRooms%20eq%20" + flagBathroom_res + "%20and%20";
//                bathroom_text_res.setText(String.valueOf(flagBathroom_res));
//                break;
//            case R.id.decPentry_res:
//                if (flagPentry_res > 0)
//                    flagPentry_res--;
//                strPentry_res = "TotalRooms%20eq%20" + flagPentry_res + "%20and%20";
//                pentry_text_res.setText(String.valueOf(flagPentry_res));
//                break;
//            case R.id.incPentry_res:
//                flagPentry_res++;
//                strPentry_res = "TotalRooms%20eq%20" + flagPentry_res + "%20and%20";
//                pentry_text_res.setText(String.valueOf(flagPentry_res));
//                break;

        }

    }

    private void mapPopup(ArrayList<StateListModel.Pictures> pictures) {
        dialog = new Dialog(Township_Subarea_Project.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map_pop_up);
        close = (ImageView) dialog.findViewById(R.id.close);
        viewPager = (ViewPager) dialog.findViewById(R.id.map_images);
        imageAdapter = new ImageAdapter(this, pictures);
        viewPager.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        Bundle bundle = new Bundle();
        bundle.putString("CityId", id);
        bundle.putString("CityName", name);
        bundle.putString("StateId", stateId);
        bundle.putString("DistrictId", districtId);
        bundle.putString("townshipCount", townshipCount);
        bundle.putString("sectorCount", sectorCount);
        bundle.putString("projectCount", projectCount);
        bundle.putString("propertyCount", propertyCount);
        switch (tabPosition) {
            case 0:
                //if(PreferenceConnector.getInstance(Township_Subarea_Project.this).loadSavedPreferences("FragmentClicked", "").equalsIgnoreCase("true"))
                    //iv_location.setVisibility(View.VISIBLE);
                /*else
                    iv_location.setVisibility(View.GONE);*/
                iv_location.setVisibility(View.GONE);
                Township_List_Fragment township_list_fragment = new Township_List_Fragment();
                township_list_fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_area_listing, township_list_fragment).commit();

                break;
            case 1:
                iv_location.setVisibility(View.GONE);
                Subarea_List_Fragment subarea_list_fragment = new Subarea_List_Fragment();
                subarea_list_fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_area_listing, subarea_list_fragment).commit();

                break;
            case 2:
                iv_location.setVisibility(View.GONE);
                Project_Listing_Fragment project_list_fragment = new Project_Listing_Fragment();
                project_list_fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_area_listing, project_list_fragment).commit();

                break;

            case 3:
                iv_location.setVisibility(View.GONE);
                Property_Listing_Fragment project_listing_fragment = new Property_Listing_Fragment();
                project_listing_fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_area_listing, project_listing_fragment).commit();
                break;

        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }
}
