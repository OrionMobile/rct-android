package com.bnhabitat.areaModule.areaActivities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.FeaturesAdapter;
import com.bnhabitat.areaModule.adapter.ImageAdapter;
import com.bnhabitat.areaModule.adapter.Image_Video_Adapter;
import com.bnhabitat.areaModule.adapter.ProjectCategoryTypeAdapter;
import com.bnhabitat.areaModule.adapter.ProjectListingAdapter;
import com.bnhabitat.areaModule.adapter.RecreationalAdapter;
import com.bnhabitat.areaModule.adapter.StateImageAdapter;
import com.bnhabitat.areaModule.adapter.TowerListingAdapter;
import com.bnhabitat.areaModule.adapter.TownshipFileAdapter;
import com.bnhabitat.areaModule.adapter.TownshipImageAdapter;
import com.bnhabitat.areaModule.model.ClubFacilities;
import com.bnhabitat.areaModule.model.ClubImageModel;
import com.bnhabitat.areaModule.model.ImageModel;
import com.bnhabitat.areaModule.model.ProjectAmenityModel;
import com.bnhabitat.areaModule.model.ProjectCategoryTypeModel;
import com.bnhabitat.areaModule.model.ProjectListModel;
import com.bnhabitat.areaModule.model.TownshipFilesModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.township.model.ProjectCategoryTypesModel;
import com.bnhabitat.township.townactivities.AddSectorLocality;
import com.bnhabitat.township.townactivities.Add_Project;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class ProjectDetail extends AppCompatActivity implements ViewPager.OnPageChangeListener, ViewPagerListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener {

    RecyclerView towerList;
    ArrayList<String> towerListing = new ArrayList<>();
    ArrayList<String> floorListing = new ArrayList<>();
    ArrayList<String> roomListing = new ArrayList<>();
    String id = "";
    ImageView imgBack, imgFilter, imgPrevious, imgNext, logo;
    TextView ProjectName, projectName, dev_name, first_last_name, professional_txt, submitReq, propertSell_Rent, project_count, residential_count, commercial_count;
    TextView project_name, city, township, sector_locality, tehsil, localBodyType, localBodyName;
    //Quick Facts
    TextView townshipName, totalLand, totalLandunits, areaNegativeMark, zipCode, stateName;
    TextView about_project, viewAll, clubName, clubDescription;
    ViewPager view_images;
    RecyclerView list_images_videos, propertyTypeView, clubFac, clubImg, key_distance_view, recreational_area_view;
    ImageAdapter imageAdapter;
    Image_Video_Adapter image_video_adapter;
    ArrayList<ImageModel> imageModels = new ArrayList<>();
    String name_number = "";
    String contact = "", email = "";
    ImageView userPic, msg, call;
    private static final int REQUEST_PHONE_CALL = 198;
    LinearLayout projects, clubfacilities, areas, area, coverArea, superArea, buildUpArea, carpetArea;
    TextView totalarea, totalareaunits, coverarea, coverareaunits, superarea, superareaunits, builduparea, buildupareaunits, carpetarea, carpetareaunits;
    ArrayList<ProjectCategoryTypeModel> resTypes  = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel> comTypes  = new ArrayList<>();
    ArrayList<String> localityArray = new ArrayList<>();
    ArrayList<ProjectAmenityModel.Amenity> recreationalArrayList = new ArrayList<>();
    ArrayList<ProjectAmenityModel.Amenity> keyDistanceArrayList = new ArrayList<>();
    FeaturesAdapter featuresAdapter;
    RecreationalAdapter recreationalAdapter;
    TextView key_distance, recreational_area;
    LinearLayout clubLayout, clubing, description, clubImages;
    StateImageAdapter stateImageAdapter;
    ArrayList<ProjectCategoryTypeModel> projectCategoryTypeModelArrayList = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations> accomodationsArrayList = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Towers> towersArrayList = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationFloors> floorsArrayList = new ArrayList<>();
    ArrayList<ProjectCategoryTypeModel.Accomodations.AccommodationRooms> roomsArrayList = new ArrayList<>();
    ProjectCategoryTypeAdapter projectCategoryTypeAdapter;
    private View actionB,action_A, action_c, action_d, action_e, action_f, action_g;
    ImageView share_property;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail2);
        share_property= (ImageView) findViewById(R.id.share_property);
        Intent intent = getIntent();
        id = intent.getStringExtra("ProjectId");

        initialization();
        onClicks();
        //onProjectDetail();

        final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        action_c = findViewById(R.id.action_c);
        action_d = findViewById(R.id.action_d);
        action_e= findViewById(R.id.action_e);
        action_f = findViewById(R.id.action_f);
        action_g = findViewById(R.id.action_g);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.transparent));
            }

            @SuppressLint("ResourceType")
            @Override
            public void onMenuCollapsed() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.tran));

            }
        });
        action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(ProjectDetail.this, Add_Project.class);
                startActivity(intent);

            }
        });
        action_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(ProjectDetail.this, Add_township.class);
                startActivity(intent);

            }
        });
        action_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(ProjectDetail.this, AddSectorLocality.class);
                startActivity(intent);

            }
        });
        action_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        action_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        action_g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(ProjectDetail.this, Submit_Requirement.class);
                startActivity(intent);

            }
        });

        if(PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("") || PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("null"))
            name_number = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        else
            name_number = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "");
        first_last_name.setText(name_number);
        professional_txt.setText("Developer");
        contact = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        email = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.EMAIL, "");

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (ContextCompat.checkSelfPermission(ProjectDetail.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ProjectDetail.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                } else {
                    callIntent.setData(Uri.parse("tel:" + contact));
                    startActivity(callIntent);
                }
            }
        });

        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text));
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
                startActivity(Intent.createChooser(intent, "Choose"));
            }
        });
    }

    private void onClicks() {

        share_property.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text));

                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
                startActivity(Intent.createChooser(intent, "Choose"));
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItemBack(view_images.getCurrentItem())==0)
                    view_images.setCurrentItem(getItemBack(imageModels.size()-1), true);
                else if(getItemBack(view_images.getCurrentItem())==imageModels.size()-1)
                    view_images.setCurrentItem(getItemBack(imageModels.size()-1), true);
                else
                    view_images.setCurrentItem(getItemBack(-1), false);

            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItem(+1) == imageModels.size())
                    view_images.setCurrentItem(getItem(0), true);
                else
                    view_images.setCurrentItem(getItem(+1), false);

            }
        });

        submitReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //submitReqPopUp(imageModels.get(0).getFileUrl());
            }
        });

        propertSell_Rent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //propertySell_RentPopUp(imageModels.get(0).getFileUrl());
            }
        });
    }

    public int getItem(int i) {
        if(view_images.getCurrentItem() == imageModels.size()-1)
            return 0;
        else
            return view_images.getCurrentItem() + i;
    }

    private int getItemBack(int i) {

        if(view_images.getCurrentItem() == imageModels.size()-1)
            return imageModels.size()-2;
        else if(view_images.getCurrentItem() == 0)
            return imageModels.size()-1;
        else
            return view_images.getCurrentItem() + i;

    }

    private void initialization() {

        projects = (LinearLayout) findViewById(R.id.projects);
        clubfacilities = (LinearLayout) findViewById(R.id.clubfacilities);
        projects.setVisibility(View.GONE);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgFilter = (ImageView) findViewById(R.id.imgFilter);
        imgPrevious = (ImageView) findViewById(R.id.imgPrevious);
        imgNext = (ImageView) findViewById(R.id.imgNext);
        logo = (ImageView) findViewById(R.id.logo);
        ProjectName = (TextView) findViewById(R.id.ProjectName);
        projectName = (TextView) findViewById(R.id.projectName);
        dev_name = (TextView) findViewById(R.id.dev_name);
        userPic = (ImageView) findViewById(R.id.userPic);
        msg = (ImageView) findViewById(R.id.msg);
        call = (ImageView) findViewById(R.id.call);
        first_last_name = (TextView) findViewById(R.id.first_last_name);
        professional_txt = (TextView) findViewById(R.id.professional_txt);
        submitReq = (TextView) findViewById(R.id.submitReq);
        propertSell_Rent = (TextView) findViewById(R.id.propertSell_Rent);
        project_count = (TextView) findViewById(R.id.project_count);
        residential_count = (TextView) findViewById(R.id.residential_count);
        commercial_count = (TextView) findViewById(R.id.commercial_count);
        project_name = (TextView) findViewById(R.id.project_name);
        city = (TextView) findViewById(R.id.city);
        township = (TextView) findViewById(R.id.township);
        sector_locality = (TextView) findViewById(R.id.sector_locality);
        tehsil = (TextView) findViewById(R.id.tehsil);
        localBodyType = (TextView) findViewById(R.id.localBodyType);
        localBodyName = (TextView) findViewById(R.id.localBodyName);
        about_project = (TextView) findViewById(R.id.about_project);
        viewAll = (TextView) findViewById(R.id.viewAll);
        townshipName = (TextView) findViewById(R.id.townshipName);
        totalLand = (TextView) findViewById(R.id.totalLand);
        totalLandunits = (TextView) findViewById(R.id.totalLandunits);
        totalLandunits.setVisibility(View.GONE);
        areaNegativeMark = (TextView) findViewById(R.id.areaNegativeMark);
        zipCode = (TextView) findViewById(R.id.zipCode);
        stateName = (TextView) findViewById(R.id.stateName);
        key_distance = (TextView) findViewById(R.id.key_distance);
        recreational_area = (TextView) findViewById(R.id.recreational_area);
        view_images = (ViewPager) findViewById(R.id.view_images);
        towerList = (RecyclerView) findViewById(R.id.towerList);
        list_images_videos = (RecyclerView) findViewById(R.id.list_images_videos);
        propertyTypeView = (RecyclerView) findViewById(R.id.propertyTypeView);
        recreational_area_view = (RecyclerView) findViewById(R.id.recreational_area_view);
        key_distance_view = (RecyclerView) findViewById(R.id.key_distance_view);
        clubLayout = (LinearLayout) findViewById(R.id.clubLayout);
        clubing = (LinearLayout) findViewById(R.id.clubing);
        description = (LinearLayout) findViewById(R.id.description);
        clubImages = (LinearLayout) findViewById(R.id.clubImages);
        areas = (LinearLayout) findViewById(R.id.areas);
        area = (LinearLayout) findViewById(R.id.area);
        coverArea = (LinearLayout) findViewById(R.id.coverArea);
        superArea = (LinearLayout) findViewById(R.id.superArea);
        buildUpArea = (LinearLayout) findViewById(R.id.buildUpArea);
        carpetArea = (LinearLayout) findViewById(R.id.carpetArea);
        clubFac = (RecyclerView) findViewById(R.id.clubFac);
        clubImg = (RecyclerView) findViewById(R.id.clubImg);
        clubName = (TextView) findViewById(R.id.clubName);
        clubDescription = (TextView) findViewById(R.id.clubDescription);
        totalarea = (TextView) findViewById(R.id.totalarea);
        totalareaunits = (TextView) findViewById(R.id.totalareaunits);
        coverarea = (TextView) findViewById(R.id.coverarea);
        coverareaunits = (TextView) findViewById(R.id.coverareaunits);
        superarea = (TextView) findViewById(R.id.superarea);
        superareaunits = (TextView) findViewById(R.id.superareaunits);
        builduparea = (TextView) findViewById(R.id.builduparea);
        buildupareaunits = (TextView) findViewById(R.id.buildupareaunits);
        carpetarea = (TextView) findViewById(R.id.carpetarea);
        carpetareaunits = (TextView) findViewById(R.id.carpetareaunits);

    }

    @Override
    protected void onResume() {
        onProjectDetail();
        super.onResume();
    }

    public void onProjectDetail(){
        new CommonSyncwithoutstatus(this,
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_PROJECT_DETAIL +
                        id ,
                "",
                "Loading...",this,
                Urls.URL_PROJECT_DETAIL,
                Constants.GET)
                .execute();
    }

    @Override
    public void onResultListener(String result, String which) {
        if(null != result && !result.equalsIgnoreCase("")){
            if(which.equalsIgnoreCase(Urls.URL_PROJECT_DETAIL)){
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    int statusCode = jsonObject.getInt("StatusCode");
                    if(statusCode == 200){
                        final JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        if(jsonObject1.getString("Name").equalsIgnoreCase("null"))
                            ProjectName.setText("N/A");
                        else
                            ProjectName.setText(jsonObject1.getString("Name"));
                        if(jsonObject1.getString("DeveloperCompanyName").equalsIgnoreCase("null"))
                            dev_name.setText("N/A");
                        else
                            dev_name.setText(jsonObject1.getString("DeveloperCompanyName"));
                        if(!jsonObject1.getString("DeveloperCompanyLogo").equalsIgnoreCase("") || !jsonObject1.getString("DeveloperCompanyLogo").equalsIgnoreCase("null"))
                            Picasso.with(this).load(Urls.BASE_CONTACT_IMAGE_URL + jsonObject1.getString("DeveloperCompanyLogo")).into(logo);

                        JSONArray jsonArray1 = jsonObject1.getJSONArray("ProjectCategoryTypes");
                        projectCategoryTypeModelArrayList.clear();
                        accomodationsArrayList.clear();
                        towersArrayList.clear();
                        roomsArrayList.clear();
                        floorsArrayList.clear();

                        for(int index = 0; index < jsonArray1.length(); index++){
                            JSONObject jsonObject2 = jsonArray1.getJSONObject(index);
                            ProjectCategoryTypeModel projectCategoryTypeModel = new ProjectCategoryTypeModel();
                            projectCategoryTypeModel.setType(jsonObject2.getString("Type"));
                            if(projectCategoryTypeModel.getType().equalsIgnoreCase("Residential"))
                                resTypes.add(projectCategoryTypeModel);

                            if(projectCategoryTypeModel.getType().equalsIgnoreCase("Commercial"))
                                comTypes.add(projectCategoryTypeModel);

                            projectCategoryTypeModel.setCategoryType(jsonObject2.getString("CategoryType"));
                            JSONArray accomodationArray = jsonObject2.getJSONArray("Accomodations");
                            if(accomodationArray.length()!=0){
                                for(int i1 = 0; i1 < accomodationArray.length(); i1++){
                                    JSONObject object1 = accomodationArray.getJSONObject(i1);
                                    ProjectCategoryTypeModel.Accomodations accomodations = new ProjectCategoryTypeModel.Accomodations();
                                    accomodations.setTitle(object1.getString("Title"));
                                    JSONArray floorArray = object1.getJSONArray("AccommodationFloors");
                                    if(floorArray.length()!=0){
                                        for(int i2 = 0; i2 < floorArray.length(); i2++){
                                            JSONObject floorObject = floorArray.getJSONObject(i2);
                                            ProjectCategoryTypeModel.Accomodations.AccommodationFloors accommodationFloors = new ProjectCategoryTypeModel.Accomodations.AccommodationFloors();
                                            accommodationFloors.setFloorNumber(floorObject.getString("FloorNumber"));
                                            floorsArrayList.add(accommodationFloors);
                                        }
                                    }
                                    accomodations.setAccommodationFloors(floorsArrayList);
                                    JSONArray roomArray = object1.getJSONArray("AccommodationRooms");
                                    if(roomArray.length()!=0){
                                        for(int i3 = 0; i3 < roomArray.length(); i3++){
                                            JSONObject roomObject = roomArray.getJSONObject(i3);
                                            ProjectCategoryTypeModel.Accomodations.AccommodationRooms accommodationRooms = new ProjectCategoryTypeModel.Accomodations.AccommodationRooms();
                                            accommodationRooms.setBedRoomName(roomObject.getString("BedRoomName"));
                                            accommodationRooms.setCategory(roomObject.getString("Category"));

                                            Log.e("RoomArray", "onResultListener: " + accommodationRooms.getBedRoomName() + "  " + accommodationRooms.getCategory() );
                                            roomsArrayList.add(accommodationRooms);
                                        }
                                    }
                                    accomodations.setAccommodationRooms(roomsArrayList);
                                    accomodationsArrayList.add(accomodations);

                                }
                            }
                            projectCategoryTypeModel.setAccomodations(accomodationsArrayList);
                            JSONArray towerArray = jsonObject2.getJSONArray("Towers");
                            if(towerArray.length()!=0){
                                for(int i1 = 0; i1 < towerArray.length(); i1++){
                                    JSONObject object1 = towerArray.getJSONObject(i1);
                                    ProjectCategoryTypeModel.Towers towers = new ProjectCategoryTypeModel.Towers();
                                    towers.setName(object1.getString("Name"));
                                    towers.setTotalFloor(object1.getString("TotalFloor"));
                                    towers.setTotalPassengerLift(object1.getString("TotalPassengerLift"));
                                    towers.setTotalCargoLift(object1.getString("TotalCargoLift"));
                                    towers.setTotalStaircase(object1.getString("TotalStaircase"));
                                    towers.setTotalBasements(object1.getString("TotalBasements"));

                                    ArrayList<ProjectCategoryTypeModel.Towers.ProjectFloors> projectFloorsArrayList = new ArrayList<>();
                                    projectFloorsArrayList.clear();
                                    JSONArray projectFloors = object1.getJSONArray("ProjectFloors");
                                    if(projectFloors.length()!=0){
                                        for(int i2 = 0; i2 < projectFloors.length(); i2++){
                                            JSONObject object2 = projectFloors.getJSONObject(i2);
                                            ProjectCategoryTypeModel.Towers.ProjectFloors projectFloors1 = new ProjectCategoryTypeModel.Towers.ProjectFloors();
                                            projectFloors1.setName(object2.getString("Name"));
                                            projectFloorsArrayList.add(projectFloors1);
                                        }
                                    }
                                    towers.setProjectFloors(projectFloorsArrayList);

                                    ArrayList<ProjectCategoryTypeModel.Towers.TowerAmenities> towerAmenitiesArrayList = new ArrayList<>();
                                    towerAmenitiesArrayList.clear();
                                    JSONArray towerAmenity = object1.getJSONArray("TowerAmenities");
                                    if(towerAmenity.length()!=0){
                                        for(int i3 = 0; i3 < towerAmenity.length(); i3++){
                                            JSONObject object2 = towerAmenity.getJSONObject(i3);
                                            ProjectCategoryTypeModel.Towers.TowerAmenities towerAmenities = new ProjectCategoryTypeModel.Towers.TowerAmenities();
                                            towerAmenities.setAmenityId(object2.getString("AmenityId"));
                                            JSONObject amenityObject = object2.getJSONObject("Amenity");
                                            ProjectCategoryTypeModel.Towers.TowerAmenities.Amenity amenity = new ProjectCategoryTypeModel.Towers.TowerAmenities.Amenity();
                                            amenity.setType(amenityObject.getString("Type"));
                                            amenity.setTitle(amenityObject.getString("Title"));
                                            amenity.setValue(amenityObject.getString("Value"));
                                            towerAmenities.setAmenity(amenity);
                                            towerAmenitiesArrayList.add(towerAmenities);
                                        }
                                    }
                                    towers.setTowerAmenities(towerAmenitiesArrayList);

                                    ArrayList<ProjectCategoryTypeModel.Towers.TowerBasementDetails> towerBasementDetailsArrayList = new ArrayList<>();
                                    towerBasementDetailsArrayList.clear();
                                    JSONArray basementDetail = object1.getJSONArray("TowerBasementDetails");
                                    if(basementDetail.length()!=0){
                                        for(int i4 = 0; i4 < basementDetail.length(); i4++){
                                            JSONObject basementObject = basementDetail.getJSONObject(i4);
                                            ProjectCategoryTypeModel.Towers.TowerBasementDetails towerBasementDetails = new ProjectCategoryTypeModel.Towers.TowerBasementDetails();
                                            towerBasementDetails.setTitle(basementObject.getString("Title"));
                                            towerBasementDetails.setValue(basementObject.getString("Value"));
                                            towerBasementDetailsArrayList.add(towerBasementDetails);
                                        }
                                    }
                                    towers.setTowerBasementDetails(towerBasementDetailsArrayList);
                                    towersArrayList.add(towers);
                                }
                            }
                            projectCategoryTypeModel.setTowers(towersArrayList);
                            projectCategoryTypeModelArrayList.add(projectCategoryTypeModel);

                        }

                        residential_count.setText(String.valueOf(resTypes.size()));
                        commercial_count.setText(String.valueOf(comTypes.size()));

                        projectCategoryTypeAdapter = new ProjectCategoryTypeAdapter(this, projectCategoryTypeModelArrayList, accomodationsArrayList, towersArrayList, roomsArrayList, floorsArrayList);
                        propertyTypeView.setLayoutManager(new LinearLayoutManager(this));
                        propertyTypeView.setAdapter(projectCategoryTypeAdapter);
                        projectCategoryTypeAdapter.notifyDataSetChanged();

                        if(jsonObject1.getString("TownshipName").equalsIgnoreCase("null"))
                            townshipName.setText("N/A");
                        else
                            townshipName.setText(jsonObject1.getString("TownshipName"));
                        if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            totalLand.setText("N/A");
                        else if(!jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            totalLand.setText(jsonObject1.getString("TotalArea") + " N/A" );
                        else if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && !jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            totalLand.setText("N/A " + jsonObject1.getString("TotalAreaSizeUnit"));
                        else
                            totalLand.setText(jsonObject1.getString("TotalArea") + " " + jsonObject1.getString("TotalAreaSizeUnit"));
                        //totalLandunits.setText(jsonObject1.getString("TotalAreaSizeUnit"));
                        if(jsonObject1.getString("AreaNagetiveMark").equalsIgnoreCase("null"))
                            areaNegativeMark.setText("N/A");
                        else
                            areaNegativeMark.setText(jsonObject1.getString("AreaNagetiveMark"));
                        if(jsonObject1.getString("ZipCode").equalsIgnoreCase("null"))
                            zipCode.setText("N/A");
                        else
                            zipCode.setText(jsonObject1.getString("ZipCode"));
                        if(jsonObject1.getString("StateName").equalsIgnoreCase("null"))
                            stateName.setText("N/A");
                        else
                            stateName.setText(jsonObject1.getString("StateName"));
                        if(jsonObject1.getString("Name").equalsIgnoreCase("null"))
                            projectName.setText("N/A");
                        else
                            projectName.setText(jsonObject1.getString("Name"));
                        if(jsonObject1.getString("AboutProject").equalsIgnoreCase("null"))
                            about_project.setText("N/A");
                        else
                            about_project.setText(jsonObject1.getString("AboutProject"));

                        about_project.post(new Runnable() {
                            @Override
                            public void run() {
                                final int lineCount1 = about_project.getLineCount();
                                Log.e("lineCount1", "lineCount1: "+ lineCount1);

                                if(lineCount1 <= 5) {
                                    about_project.setEllipsize(null);
                                    viewAll.setVisibility(View.GONE);
                                }
                                else{
                                    viewAll.setVisibility(View.VISIBLE);
                                    viewAll.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            viewAll.setVisibility(View.GONE);
                                            about_project.setEllipsize(null);
                                            about_project.setMaxLines(Integer.MAX_VALUE);
                                            try {
                                                about_project.setText(jsonObject1.getString("AboutProject"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                                // Use lineCount here
                            }
                        });

                        JSONArray jsonArray2 = jsonObject1.getJSONArray("ProjectFiles");
                        imageModels.clear();
                        if(jsonArray2.length()!=0){
                            for(int i = 0; i < jsonArray2.length(); i++){
                                JSONObject object = jsonArray2.getJSONObject(i);
                                ImageModel imageModel = new ImageModel();
                                imageModel.setCategory(object.getString("Category"));
                                imageModel.setFileUrl(Urls.BASE_CONTACT_IMAGE_URL + object.getString("FileUrl"));
                                if(imageModel.getCategory().equalsIgnoreCase("Photos"))
                                    imageModels.add(imageModel);
                            }
                        }
                        imageAdapter = new ImageAdapter(this, imageModels, "");
                        view_images.setAdapter(imageAdapter);
                        imageAdapter.notifyDataSetChanged();

                        image_video_adapter = new Image_Video_Adapter(this, this, imageModels);
                        list_images_videos.setAdapter(image_video_adapter);
                        list_images_videos.setLayoutManager(new LinearLayoutManager(this,  LinearLayout.HORIZONTAL, false));
                        image_video_adapter.notifyDataSetChanged();

                        project_name.setText(jsonObject1.getString("Name"));
                        city.setText(jsonObject1.getString("DistrictName"));
                        township.setText(jsonObject1.getString("TownName"));

                        JSONArray jsonArray = jsonObject1.getJSONArray("ProjectLocalities");
                        if(jsonArray.length()!=0){
                            for(int i = 0; i <jsonArray.length(); i++){
                                JSONObject localities = jsonArray.getJSONObject(i);
                                String loc = (String) localities.get("LocalityName");
                                localityArray.add(loc);

                            }
                            sector_locality.setText(TextUtils.join(", ",localityArray));
                        }else{
                            sector_locality.setText("N/A");

                        }
                        if(jsonObject1.getString("TehsilName").equalsIgnoreCase("null"))
                            tehsil.setText("N/A");
                        else
                            tehsil.setText(jsonObject1.getString("TehsilName"));
                        if(jsonObject1.getString("LocalBodyType").equalsIgnoreCase("null"))
                            localBodyType.setText("N/A");
                        else
                            localBodyType.setText(jsonObject1.getString("LocalBodyType"));
                        if(jsonObject1.getString("LocalBodyName").equalsIgnoreCase("null"))
                            localBodyName.setText("N/A");
                        else
                            localBodyName.setText(jsonObject1.getString("LocalBodyName"));

                        JSONObject club = jsonObject1.getJSONObject("Club");
                        if(club.length() != 0){
                            clubLayout.setVisibility(View.VISIBLE);
                        }

                        if(club.has("Name")) {
                            clubing.setVisibility(View.VISIBLE);
                            if(club.getString("Name").equalsIgnoreCase("null"))
                                clubName.setText("N/A");
                            else
                                clubName.setText(club.getString("Name"));
                        }
                        else
                            clubing.setVisibility(View.GONE);

                        if(club.has("Description")){
                            description.setVisibility(View.VISIBLE);
                            if(club.getString("Description").equalsIgnoreCase("null"))
                                clubDescription.setText("N/A");
                            else
                                clubDescription.setText(club.getString("Description"));
                        }
                        else
                            description.setVisibility(View.GONE);

                        if((club.has("Area") && club.has("AreaSizeUnit")) ||
                                (club.has("CoverAreaSize") && club.has("CoverAreaSizeUnit")) ||
                                (club.has("SuperAreaSize") && club.has("SuperAreaSizeUnit")) ||
                                (club.has("BuiltUpAreaSize") && club.has("BuiltUpAreaSizeUnit")) ||
                                (club.has("CarpetAreaSize") && club.has("CarpetAreaSizeUnit"))){
                            areas.setVisibility(View.VISIBLE);
                        }
                        else
                            areas.setVisibility(View.GONE);

                        if((club.has("Area") && club.has("AreaSizeUnit"))) {
                            area.setVisibility(View.VISIBLE);
                            if(club.getString("Area").equalsIgnoreCase("null"))
                                totalarea.setText("N/A" + " ");
                            else
                                totalarea.setText(club.getString("Area") + " ");
                            if(club.getString("AreaSizeUnit").equalsIgnoreCase("null"))
                                totalareaunits.setText("N/A");
                            else
                                totalareaunits.setText(club.getString("AreaSizeUnit"));
                        }
                        else
                            area.setVisibility(View.GONE);

                        if((club.has("CoverAreaSize") && club.has("CoverAreaSizeUnit"))) {
                            coverArea.setVisibility(View.VISIBLE);
                            if(club.getString("CoverAreaSize").equalsIgnoreCase("null"))
                                coverarea.setText("N/A ");
                            else
                                coverarea.setText(club.getString("CoverAreaSize") + " ");
                            if(club.getString("CoverAreaSizeUnit").equalsIgnoreCase("null"))
                                coverareaunits.setText("N/A");
                            else
                                coverareaunits.setText(club.getString("CoverAreaSizeUnit"));
                        }
                        else
                            coverArea.setVisibility(View.GONE);

                        if((club.has("SuperAreaSize") && club.has("SuperAreaSizeUnit"))) {
                            superArea.setVisibility(View.VISIBLE);
                            if(club.getString("SuperAreaSize").equalsIgnoreCase("null"))
                                superarea.setText("N/A ");
                            else
                                superarea.setText(club.getString("SuperAreaSize") + " ");
                            if(club.getString("SuperAreaSizeUnit").equalsIgnoreCase("null"))
                                superareaunits.setText("N/A");
                            else
                                superareaunits.setText(club.getString("SuperAreaSizeUnit"));
                        }
                        else
                            superArea.setVisibility(View.GONE);

                        if((club.has("BuiltUpAreaSize") && club.has("BuiltUpAreaSizeUnit"))) {
                            buildUpArea.setVisibility(View.VISIBLE);
                            if(club.getString("BuiltUpAreaSize").equalsIgnoreCase("null"))
                                builduparea.setText("N/A ");
                            else
                                builduparea.setText(club.getString("BuiltUpAreaSize") + " ");
                            if(club.getString("BuiltUpAreaSizeUnit").equalsIgnoreCase("null"))
                                buildupareaunits.setText("N/A");
                            else
                                buildupareaunits.setText(club.getString("BuiltUpAreaSizeUnit"));
                        }
                        else
                            buildUpArea.setVisibility(View.GONE);

                        if((club.has("CarpetAreaSize") && club.has("CarpetAreaSizeUnit"))) {
                            carpetArea.setVisibility(View.VISIBLE);
                            if(club.getString("CarpetAreaSize").equalsIgnoreCase("null"))
                                carpetarea.setText("N/A ");
                            else
                                carpetarea.setText(club.getString("CarpetAreaSize") + " ");
                            if(club.getString("CarpetAreaSizeUnit").equalsIgnoreCase("null"))
                                carpetareaunits.setText("N/A");
                            else
                                carpetareaunits.setText(club.getString("CarpetAreaSizeUnit"));
                        }
                        else
                            carpetArea.setVisibility(View.GONE);

                        JSONArray clubAmenities = club.optJSONArray("ClubAmenities");
                        if(clubAmenities.length()!=0){
                            clubfacilities.setVisibility(View.VISIBLE);
                        }
                        else
                            clubfacilities.setVisibility(View.GONE);
                        ArrayList<ClubFacilities> clubFacilitiesArrayList = new ArrayList<>();
                        clubFacilitiesArrayList.clear();

                        for(int i = 0; i < clubAmenities.length(); i++){
                            JSONObject jsonObject4 = clubAmenities.getJSONObject(i);
                            ClubFacilities clubFacilities = new ClubFacilities();
                            if(jsonObject4.getString("AmenityId").equalsIgnoreCase("null"))
                                clubFacilities.setAmenityId("N/A");
                            else
                                clubFacilities.setAmenityId(jsonObject4.getString("AmenityId"));
                            JSONObject jsonObject5 = jsonObject4.getJSONObject("Amenity");
                            ClubFacilities.Amenity amenity = new ClubFacilities.Amenity();
                            if(jsonObject5.getString("Value").equalsIgnoreCase("null"))
                                amenity.setValue("N/A");
                            else
                            amenity.setValue(jsonObject5.getString("Value"));

                            clubFacilities.setAmenity(amenity);
                            clubFacilitiesArrayList.add(clubFacilities);
                        }

                        recreationalAdapter = new RecreationalAdapter(this, clubFacilitiesArrayList, "");
                        clubFac.setAdapter(recreationalAdapter);
                        clubFac.setLayoutManager(new GridLayoutManager(this, 2));
                        recreationalAdapter.notifyDataSetChanged();

                        JSONArray jsonArray4 = club.optJSONArray("ClubFiles");
                        if(jsonArray4.length() != 0)
                            clubImages.setVisibility(View.VISIBLE);
                        else
                            clubImages.setVisibility(View.GONE);

                       ArrayList<ClubImageModel> clubImages = new ArrayList<>();
                       clubImages.clear();
                       if(jsonArray4.length()!=0){
                           for(int i = 0; i < jsonArray4.length(); i++){
                               JSONObject object = jsonArray4.getJSONObject(i);
                               ClubImageModel clubImageModel = new ClubImageModel();
                               if(object.getString("Id").equalsIgnoreCase("null"))
                                   clubImageModel.setId("N/A");
                               else
                                   clubImageModel.setId(object.getString("Id"));
                               if(object.getString("Category").equalsIgnoreCase("null"))
                                   clubImageModel.setCategory("N/A");
                               else
                                   clubImageModel.setCategory(object.getString("Category"));
                               clubImageModel.setFileUrl(Urls.BASE_CONTACT_IMAGE_URL + object.getString("FileUrl"));
                               clubImages.add(clubImageModel);
                           }
                       }

                        stateImageAdapter = new StateImageAdapter(this, "", clubImages);
                        clubImg.setAdapter(stateImageAdapter);
                        clubImg.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                        stateImageAdapter.notifyDataSetChanged();

                        JSONArray jsonArray3 = jsonObject1.getJSONArray("ProjectAmenities");
                        recreationalArrayList.clear();
                        keyDistanceArrayList.clear();
                        if(jsonArray3.length()!=0){
                            for(int i = 0; i < jsonArray3.length(); i++){
                                JSONObject object = jsonArray3.getJSONObject(i);
                                JSONObject object1 = object.getJSONObject("Amenity");
                                ProjectAmenityModel.Amenity amenity = new ProjectAmenityModel.Amenity();
                                amenity.setType(object1.getString("Type"));
                                if(amenity.getType().equalsIgnoreCase("RecreationalArea")){
                                    if(object1.getString("Title").equalsIgnoreCase("null"))
                                        amenity.setTitle("N/A");
                                    else
                                        amenity.setTitle(object1.getString("Title"));
                                    if(object1.getString("Value").equalsIgnoreCase("null"))
                                        amenity.setValue("N/A");
                                    else
                                        amenity.setValue(object1.getString("Value"));
                                    recreationalArrayList.add(amenity);
                                } else if(amenity.getType().equalsIgnoreCase("KeyDistances")){
                                    if(object1.getString("Title").equalsIgnoreCase("null"))
                                        amenity.setTitle("N/A");
                                    else
                                        amenity.setTitle(object1.getString("Title"));
                                    if(object1.getString("Value").equalsIgnoreCase("null"))
                                        amenity.setValue("N/A");
                                    else
                                        amenity.setValue(object1.getString("Value"));
                                    if(object1.getString("Size").equalsIgnoreCase("null"))
                                        amenity.setSize("N/A");
                                    else
                                        amenity.setSize(object1.getString("Size"));
                                    if(object1.getString("SizeUnit").equalsIgnoreCase("null"))
                                        amenity.setSizeUnit("N/A");
                                    else
                                        amenity.setSizeUnit(object1.getString("SizeUnit"));
                                    keyDistanceArrayList.add(amenity);
                                }
                            }
                            if(recreationalArrayList.size()!=0){
                                recreationalAdapter = new RecreationalAdapter(this,"", recreationalArrayList);
                                recreational_area_view.setLayoutManager(new LinearLayoutManager(this));
                                recreational_area_view.setAdapter(recreationalAdapter);
                                recreationalAdapter.notifyDataSetChanged();
                            }
                            else{
                                recreational_area_view.setVisibility(View.GONE);
                                recreational_area.setVisibility(View.GONE);
                            }

                            if(keyDistanceArrayList.size()!=0) {
                                featuresAdapter = new FeaturesAdapter(this, keyDistanceArrayList, "");
                                key_distance_view.setLayoutManager(new LinearLayoutManager(this));
                                key_distance_view.setAdapter(featuresAdapter);
                                featuresAdapter.notifyDataSetChanged();
                            }
                            else{
                                key_distance.setVisibility(View.GONE);
                                key_distance_view.setVisibility(View.GONE);
                            }


                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void setViewPager(int position) {

    }
}
