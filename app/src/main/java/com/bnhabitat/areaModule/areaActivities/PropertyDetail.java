package com.bnhabitat.areaModule.areaActivities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.fragment.AccomodationView;
import com.bnhabitat.areaModule.fragment.BuildView;
import com.bnhabitat.areaModule.fragment.FinancialBank;
import com.bnhabitat.areaModule.fragment.LegalStatus;
import com.bnhabitat.areaModule.fragment.OtherQuestion;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.InventoryModel_Area;
import com.bnhabitat.ui.adapters.GalleryListAdapter;
import com.bnhabitat.ui.fragments.BuiltViewFragment;
import com.bnhabitat.ui.fragments.OtherExpensesFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PropertyDetail extends AppCompatActivity implements CommonAsync.OnAsyncResultListener{

    RecyclerView gallery_list;
    TabLayout tab_layout_detail;
    TabLayout.Tab Tab1, Tab2, Tab3, Tab4, Tab5, Tab6, Tab7, Tab8;
    ImageView project_image, edit_property,imgShare;
    LinearLayoutManager layoutManager;
    ImageView back_btn, popup, iv_location;
    String property_id;
    TextView property_type, locality, area_unit, price, bedroom, kitchen, parking, bathroom,property_type_txt;
    int room_count=0,kitchen_count=0,bathroom_count=0,parking_count=0;
    ArrayList<InventoryModel_Area> property_data=new ArrayList<>();
    Dialog dialog;
    ImageView close, recommend_project_image;
    TextView recommend_property_type, recommend_property_locality;
    TabLayout tab_layout_map;
    static TabLayout.Tab Tab11, Tab22;
    String unitName = "", towerName = "", floorName = "", subArea = "", subDivision = "", district = "", state = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail);

        initialization();

        onClicks();

        property_id=getIntent().getStringExtra(Constants.PROPERTY_ID)==null?"":getIntent().getStringExtra(Constants.PROPERTY_ID);
        Tab1 = tab_layout_detail.newTab().setText("Built Up");
        Tab2 = tab_layout_detail.newTab().setText("Accomodation");
        Tab3 = tab_layout_detail.newTab().setText("Financial & Bank Loan's");
        Tab4 = tab_layout_detail.newTab().setText("Other Expenses");
        Tab5 = tab_layout_detail.newTab().setText("Legal Status");
        Tab6 = tab_layout_detail.newTab().setText("Other Questions");
        tab_layout_detail.addTab(Tab1);
        tab_layout_detail.addTab(Tab2);
        tab_layout_detail.addTab(Tab3);
        tab_layout_detail.addTab(Tab4);
        tab_layout_detail.addTab(Tab5);
        tab_layout_detail.addTab(Tab6);

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        gallery_list.setLayoutManager(layoutManager);

        BuildView builtViewFragment = new BuildView();
        Bundle builtViewbundle = new Bundle();
        builtViewbundle.putSerializable("property_data", property_data);
        builtViewFragment.setArguments(builtViewbundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, builtViewFragment).commit();
        tab_layout_detail.getTabAt(0);
        Tab1.select();

        //getPropertyDetail();

        bindWidgetsWithAnEvent();
    }

    @Override
    protected void onResume() {
        getPropertyDetail();
        super.onResume();
    }

    public void bindWidgetsWithAnEvent() {
        tab_layout_detail.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                BuildView builtViewFragment = new BuildView();
                Bundle builtViewbundle = new Bundle();

                builtViewbundle.putSerializable("property_data", property_data);
                builtViewFragment.setArguments(builtViewbundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, builtViewFragment).commit();


                break;
            case 1:
                AccomodationView accomodationViewFragment = new AccomodationView();
                Bundle bundle = new Bundle();

                bundle.putSerializable("property_data", property_data);
                accomodationViewFragment.setArguments(bundle);
                //Inflate the fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, accomodationViewFragment).commit();

                break;
            case 2:
                try {
                    FinancialBank financial_bank_viewFragment = new FinancialBank();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("property_data", property_data);

                    financial_bank_viewFragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, financial_bank_viewFragment).commit();


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 3:
                try {
                    OtherExpensesFragment otherexpensesViewFragment = new OtherExpensesFragment();
                    Bundle bundle1 = new Bundle();

                    bundle1.putSerializable("property_data", property_data);
                    otherexpensesViewFragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, otherexpensesViewFragment).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 4:
                try {
                    LegalStatus legalStatusViewFragment = new LegalStatus();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("property_data", property_data);
                    legalStatusViewFragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, legalStatusViewFragment).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
//
                break;
            case 5:
                try {
                    OtherQuestion otherQuestionViewFragment = new OtherQuestion();
                    Bundle bundle1 = new Bundle();

                    bundle1.putSerializable("property_data", property_data);
                    otherQuestionViewFragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.fragment_container_detail, otherQuestionViewFragment).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    private void onClicks() {

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text));
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_TEXT,"");
                startActivity(Intent.createChooser(intent, "Choose"));

            }
        });

        popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creating the instance of pop up
                PopupMenu popupMenu = new PopupMenu(PropertyDetail.this, popup);
                //Inflating the pop up using xml file
                popupMenu.getMenuInflater().inflate(R.menu.menu_pop_up, popupMenu.getMenu());

                //registering pop up with onMenuItemClickListener
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.edit:
                                break;
                            case R.id.recommendProperty:
                                recommendPropertyPopup();
                                break;
                            case R.id.assignOut:
                                break;
                            case R.id.lead:
                                break;
                            case R.id.delete:
                                break;
                        }
                        return true;
                    }
                });
                popupMenu.show();

            }

        });

        iv_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapPopup();
            }
        });

    }

    private void initialization() {
        gallery_list=(RecyclerView) findViewById(R.id.gallery_list);
        property_type = (TextView) findViewById(R.id.property_type);
        area_unit = (TextView)findViewById(R.id.area_unit);
        price = (TextView) findViewById(R.id.price);
        property_type_txt = (TextView) findViewById(R.id.property_type_txt);
        locality = (TextView) findViewById(R.id.locality);
        parking = (TextView) findViewById(R.id.parking);
        kitchen = (TextView) findViewById(R.id.kitchen);
        bedroom = (TextView) findViewById(R.id.bedroom);
        bathroom = (TextView) findViewById(R.id.bathroom);
        project_image = (ImageView) findViewById(R.id.project_image);
        edit_property = (ImageView) findViewById(R.id.edit_property);
        tab_layout_detail = (TabLayout) findViewById(R.id.tab_layout_detail);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        popup = (ImageView) findViewById(R.id.popup);
        iv_location = (ImageView) findViewById(R.id.iv_location);
        imgShare =(ImageView) findViewById(R.id.imgShare);
    }

    private void getPropertyDetail() {

        new CommonAsync(this,
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_PROPERTY_DETAILING+
                        property_id,
                "",
                "Loading...",
                this,
                Urls.URL_PROPERTY_DETAILING,
                Constants.GET).execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_DETAILING)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject propertySize = jsonObject.getJSONObject("Result");

                        if (propertySize.length() == 0) {
                        } else {

                            InventoryModel_Area inventoryModels = new InventoryModel_Area();

                            if(propertySize.getString("Id").equalsIgnoreCase("null"))
                                inventoryModels.setId("N/A");
                            else
                                inventoryModels.setId(propertySize.getString("Id"));
                            if(propertySize.getString("PropertyTypeId").equalsIgnoreCase("null"))
                                inventoryModels.setPropertyTypeId("N/A");
                            else
                                inventoryModels.setPropertyTypeId(propertySize.getString("PropertyTypeId"));
                            if(propertySize.getString("CompanyId").equalsIgnoreCase("null"))
                                inventoryModels.setCompanyId("N/A");
                            else
                                inventoryModels.setCompanyId(propertySize.getString("CompanyId"));
                            if(propertySize.getString("Cam").equalsIgnoreCase("null"))
                                inventoryModels.setCam("N/A");
                            else
                                inventoryModels.setCam(propertySize.getString("Cam"));
                            if(propertySize.getString("Title").equalsIgnoreCase("null"))
                                inventoryModels.setTitle("N/A");
                            else
                                inventoryModels.setTitle(propertySize.getString("Title"));
                            if(propertySize.getString("CreatedById").equalsIgnoreCase("null"))
                                inventoryModels.setCreatedById("N/A");
                            else
                                inventoryModels.setCreatedById(propertySize.getString("CreatedById"));

                            JSONObject objectPropertyType=propertySize.getJSONObject("PropertyType");
                            if(objectPropertyType.getString("Id").equalsIgnoreCase("null"))
                                inventoryModels.setPropertyobjectId("N/A");
                            else
                                inventoryModels.setPropertyobjectId(objectPropertyType.getString("Id"));
                            if(objectPropertyType.getString("Name").equalsIgnoreCase("null"))
                                inventoryModels.setNameobject("N/A");
                            else
                                inventoryModels.setNameobject(objectPropertyType.getString("Name"));
                            if(objectPropertyType.getString("Type").equalsIgnoreCase("null"))
                                inventoryModels.setTypeobject("N/A");
                            else
                                inventoryModels.setTypeobject(objectPropertyType.getString("Type"));

                            JSONArray jsonArray = propertySize.getJSONArray("PropertyLocations");
                            ArrayList<InventoryModel_Area.PropertyLocation> propertyLocations = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                InventoryModel_Area.PropertyLocation propertyLocation = inventoryModels.new PropertyLocation();
                                JSONObject jsonObjectPropertyLocations = jsonArray.getJSONObject(i);
                                if(jsonObjectPropertyLocations.getString("Id").equalsIgnoreCase("null"))
                                    propertyLocation.setId("N/A");
                                else
                                    propertyLocation.setId(jsonObjectPropertyLocations.getString("Id"));
                                if(jsonObjectPropertyLocations.getString("PropertyId").equalsIgnoreCase("null"))
                                    propertyLocation.setPropertyId("N/A");
                                else
                                    propertyLocation.setPropertyId(jsonObjectPropertyLocations.getString("PropertyId"));
                                if(jsonObjectPropertyLocations.getString("Developer").equalsIgnoreCase("null"))
                                    propertyLocation.setDeveloper("N/A");
                                else
                                    propertyLocation.setDeveloper(jsonObjectPropertyLocations.getString("Developer"));
                                if(jsonObjectPropertyLocations.getString("Project").equalsIgnoreCase("null"))
                                    propertyLocation.setProject("N/A");
                                else
                                    propertyLocation.setProject(jsonObjectPropertyLocations.getString("Project"));
                                if(jsonObjectPropertyLocations.getString("ZipCode").equalsIgnoreCase("null"))
                                    propertyLocation.setZipCode("N/A");
                                else
                                    propertyLocation.setZipCode(jsonObjectPropertyLocations.getString("ZipCode"));
                                if(jsonObjectPropertyLocations.getString("State").equalsIgnoreCase("null"))
                                    propertyLocation.setState("N/A");
                                else
                                    propertyLocation.setState(jsonObjectPropertyLocations.getString("State"));
                                if(jsonObjectPropertyLocations.getString("District").equalsIgnoreCase("null"))
                                    propertyLocation.setDistrict("N/A");
                                else
                                    propertyLocation.setDistrict(jsonObjectPropertyLocations.getString("District"));
                                if(jsonObjectPropertyLocations.getString("SubDivision").equalsIgnoreCase("null"))
                                    propertyLocation.setSubDivision("N/A");
                                else
                                    propertyLocation.setSubDivision(jsonObjectPropertyLocations.getString("SubDivision"));
                                if(jsonObjectPropertyLocations.getString("SubArea").equalsIgnoreCase("null"))
                                    propertyLocation.setSubArea("N/A");
                                else
                                    propertyLocation.setSubArea(jsonObjectPropertyLocations.getString("SubArea"));
                                if(jsonObjectPropertyLocations.getString("UnitNo").equalsIgnoreCase("null"))
                                    propertyLocation.setUnitName("N/A");
                                else
                                    propertyLocation.setUnitName(jsonObjectPropertyLocations.getString("UnitNo"));
                                if(jsonObjectPropertyLocations.getString("TowerName").equalsIgnoreCase("null"))
                                    propertyLocation.setTowerName("N/A");
                                else
                                    propertyLocation.setTowerName(jsonObjectPropertyLocations.getString("TowerName"));
                                if(jsonObjectPropertyLocations.getString("FloorNo").equalsIgnoreCase("null"))
                                    propertyLocation.setFloorNo("N/A");
                                else
                                    propertyLocation.setFloorNo(jsonObjectPropertyLocations.getString("FloorNo"));
                                if(jsonObjectPropertyLocations.getString("GooglePlusCode").equalsIgnoreCase("null"))
                                    propertyLocation.setGooglePlusCode("N/A");
                                else
                                    propertyLocation.setGooglePlusCode(jsonObjectPropertyLocations.getString("GooglePlusCode"));
                                propertyLocations.add(propertyLocation);


                            }
                            JSONArray jsonArrayPropertyAreas = propertySize.getJSONArray("PropertyAreas");
                            ArrayList<InventoryModel_Area.PropertyArea> propertyAreas = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyAreas.length(); k++) {
                                JSONObject jsonObjectPropertyLocations = jsonArrayPropertyAreas.getJSONObject(k);
                                InventoryModel_Area.PropertyArea propertyArea = inventoryModels.new PropertyArea();
                                if(jsonObjectPropertyLocations.getString("Id").equalsIgnoreCase("null"))
                                    propertyArea.setId("N/A");
                                else
                                    propertyArea.setId(jsonObjectPropertyLocations.getString("Id"));
                                if(jsonObjectPropertyLocations.getString("PropertyId").equalsIgnoreCase("null"))
                                    propertyArea.setPropertyId("N/A");
                                else
                                    propertyArea.setPropertyId(jsonObjectPropertyLocations.getString("PropertyId"));
                                if(jsonObjectPropertyLocations.getString("PlotShape").equalsIgnoreCase("null"))
                                    propertyArea.setPlotShape("N/A");
                                else
                                    propertyArea.setPlotShape(jsonObjectPropertyLocations.getString("PlotShape"));
                                if(jsonObjectPropertyLocations.getString("PlotArea").equalsIgnoreCase("null"))
                                    propertyArea.setPlotArea("N/A");
                                else
                                    propertyArea.setPlotArea(jsonObjectPropertyLocations.getString("PlotArea"));
                                if(jsonObjectPropertyLocations.getString("PlotAreaUnitId").equalsIgnoreCase("null"))
                                    propertyArea.setPlotAreaUnitId("N/A");
                                else
                                    propertyArea.setPlotAreaUnitId(jsonObjectPropertyLocations.getString("PlotAreaUnitId"));
                                if(jsonObjectPropertyLocations.getString("FrontSize").equalsIgnoreCase("null"))
                                    propertyArea.setFrontSize("N/A");
                                else
                                    propertyArea.setFrontSize(jsonObjectPropertyLocations.getString("FrontSize"));
                                if(jsonObjectPropertyLocations.getString("FrontSizeUnitId").equalsIgnoreCase("null"))
                                    propertyArea.setFrontSize("N/A");
                                else
                                    propertyArea.setFrontSizeUnitId(jsonObjectPropertyLocations.getString("FrontSizeUnitId"));
                                if(jsonObjectPropertyLocations.getString("DepthSize").equalsIgnoreCase("null"))
                                    propertyArea.setDepthSize("N/A");
                                else
                                    propertyArea.setDepthSize(jsonObjectPropertyLocations.getString("DepthSize"));
                                if(jsonObjectPropertyLocations.getString("DepthSizeUnitId").equalsIgnoreCase("null"))
                                    propertyArea.setDepthSizeUnitId("N/A");
                                else
                                    propertyArea.setDepthSizeUnitId(jsonObjectPropertyLocations.getString("DepthSizeUnitId"));
                                if(jsonObjectPropertyLocations.getString("StreetOrRoadType").equalsIgnoreCase("null"))
                                    propertyArea.setStreetOrRoadType("N/A");
                                else
                                    propertyArea.setStreetOrRoadType(jsonObjectPropertyLocations.getString("StreetOrRoadType"));
                                if(jsonObjectPropertyLocations.getString("RoadWidth").equalsIgnoreCase("null"))
                                    propertyArea.setRoadWidth("N/A");
                                else
                                    propertyArea.setRoadWidth(jsonObjectPropertyLocations.getString("RoadWidth"));
                                if(jsonObjectPropertyLocations.getString("RoadWidthUnitId").equalsIgnoreCase("null"))
                                    propertyArea.setRoadWidthUnitId("N/A");
                                else
                                    propertyArea.setRoadWidthUnitId(jsonObjectPropertyLocations.getString("RoadWidthUnitId"));
                                if(jsonObjectPropertyLocations.getString("EnteranceDoorFacing").equalsIgnoreCase("null"))
                                    propertyArea.setEnteranceDoorFacing("N/A");
                                else
                                    propertyArea.setEnteranceDoorFacing(jsonObjectPropertyLocations.getString("EnteranceDoorFacing"));
                                if(jsonObjectPropertyLocations.getString("ParkingInFront").equalsIgnoreCase("null"))
                                    propertyArea.setParkingInFront("N/A");
                                else
                                    propertyArea.setParkingInFront(jsonObjectPropertyLocations.getString("ParkingInFront"));
                                if(jsonObjectPropertyLocations.getString("HaveWalkingPath").equalsIgnoreCase("null"))
                                    propertyArea.setHaveWalkingPath("N/A");
                                else
                                    propertyArea.setHaveWalkingPath(jsonObjectPropertyLocations.getString("HaveWalkingPath"));
                                if(jsonObjectPropertyLocations.getString("PlotOrCoverArea").equalsIgnoreCase("null"))
                                    propertyArea.setPlotOrCoverArea("N/A");
                                else
                                    propertyArea.setPlotOrCoverArea(jsonObjectPropertyLocations.getString("PlotOrCoverArea"));
                                if(jsonObjectPropertyLocations.getString("PlotOrCoverAreaUnitId").equalsIgnoreCase("null"))
                                    propertyArea.setPlotOrCoverAreaUnitId("N/A");
                                else
                                    propertyArea.setPlotOrCoverAreaUnitId(jsonObjectPropertyLocations.getString("PlotOrCoverAreaUnitId"));
                                if(jsonObjectPropertyLocations.getString("SuperArea").equalsIgnoreCase("null"))
                                    propertyArea.setSuperArea("N/A");
                                else
                                    propertyArea.setSuperArea(jsonObjectPropertyLocations.getString("SuperArea"));
                                if(jsonObjectPropertyLocations.getString("SuperAreaUnitId").equalsIgnoreCase("null"))
                                    propertyArea.setSuperAreaUnitId("N/A");
                                else
                                    propertyArea.setSuperAreaUnitId(jsonObjectPropertyLocations.getString("SuperAreaUnitId"));
                                if(jsonObjectPropertyLocations.getString("BuiltUpArea").equalsIgnoreCase("null"))
                                    propertyArea.setBuiltUpArea("N/A");
                                else
                                    propertyArea.setBuiltUpArea(jsonObjectPropertyLocations.getString("BuiltUpArea"));
                                if(jsonObjectPropertyLocations.getString("BuiltUpAreaUnitId").equalsIgnoreCase("null"))
                                    propertyArea.setBuiltUpAreaUnitId("N/A");
                                else
                                    propertyArea.setBuiltUpAreaUnitId(jsonObjectPropertyLocations.getString("BuiltUpAreaUnitId"));
                                if(jsonObjectPropertyLocations.getString("CarpetArea").equalsIgnoreCase("null"))
                                    propertyArea.setCarpetArea("N/A");
                                else
                                    propertyArea.setCarpetArea(jsonObjectPropertyLocations.getString("CarpetArea"));
                                if(jsonObjectPropertyLocations.getString("CarpetAreaUnitId").equalsIgnoreCase("null"))
                                    propertyArea.setCarpetAreaUnitId("N/A");
                                else
                                    propertyArea.setCarpetAreaUnitId(jsonObjectPropertyLocations.getString("CarpetAreaUnitId"));
                                if(!jsonObjectPropertyLocations.isNull("PropertySizeUnit4")) {
                                    JSONObject jsonObjecPropertySizeUnit4 = jsonObjectPropertyLocations.getJSONObject("PropertySizeUnit4");
                                    if(jsonObjecPropertySizeUnit4.getString("SizeUnit").equalsIgnoreCase("null"))
                                        propertyArea.setPropertySizeUnit4("N/A");
                                    else
                                        propertyArea.setPropertySizeUnit4(jsonObjecPropertySizeUnit4.getString("SizeUnit"));
                                }
                                propertyAreas.add(propertyArea);

                            }
                            JSONArray jsonArrayPropertyPlcs = propertySize.getJSONArray("PropertyPlcs");
                            ArrayList<InventoryModel_Area.PropertyPlc> propertyPlcs = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyPlcs.length(); k++) {
                                InventoryModel_Area.PropertyPlc propertyPlc = inventoryModels.new PropertyPlc();
                                JSONObject jsonObjectPropertyPlcs = jsonArrayPropertyPlcs.getJSONObject(k);
                                if(jsonObjectPropertyPlcs.getString("Id").equalsIgnoreCase("null"))
                                    propertyPlc.setId("N/A");
                                else
                                    propertyPlc.setId(jsonObjectPropertyPlcs.getString("Id"));
                                if(jsonObjectPropertyPlcs.getString("PropertyId").equalsIgnoreCase("null"))
                                    propertyPlc.setPropertyId("N/A");
                                else
                                    propertyPlc.setPropertyId(jsonObjectPropertyPlcs.getString("PropertyId"));
                                if(jsonObjectPropertyPlcs.getString("Name").equalsIgnoreCase("null"))
                                    propertyPlc.setName("N/A");
                                else
                                    propertyPlc.setName(jsonObjectPropertyPlcs.getString("Name"));
                                propertyPlcs.add(propertyPlc);
                            }
                            JSONArray jsonArrayPropertyImages = propertySize.getJSONArray("PropertyImages");
                            ArrayList<InventoryModel_Area.PropertyImage> propertyImages = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyImages.length(); k++) {
                                InventoryModel_Area.PropertyImage propertyImage = inventoryModels.new PropertyImage();
                                JSONObject jsonObjectPropertyImages = jsonArrayPropertyImages.getJSONObject(k);
                                if(jsonObjectPropertyImages.getString("Id").equalsIgnoreCase("null"))
                                    propertyImage.setId("N/A");
                                else
                                    propertyImage.setId(jsonObjectPropertyImages.getString("Id"));
                                if(jsonObjectPropertyImages.getString("Name").equalsIgnoreCase("null"))
                                    propertyImage.setName("N/A");
                                else
                                    propertyImage.setName(jsonObjectPropertyImages.getString("Name"));
                                if(jsonObjectPropertyImages.getString("Type").equalsIgnoreCase("null"))
                                    propertyImage.setType("N/A");
                                else
                                    propertyImage.setType(jsonObjectPropertyImages.getString("Type"));
                                if(jsonObjectPropertyImages.getString("Url").equalsIgnoreCase("null"))
                                    propertyImage.setUrl("N/A");
                                else
                                    propertyImage.setUrl(jsonObjectPropertyImages.getString("Url"));
                                if(jsonObjectPropertyImages.getString("ImageCode").equalsIgnoreCase("null"))
                                    propertyImage.setImageCode("N/A");
                                else
                                    propertyImage.setImageCode(jsonObjectPropertyImages.getString("ImageCode"));
                                propertyImages.add(propertyImage);
                            }
                            JSONArray jsonArrayPropertySides = propertySize.getJSONArray("PropertySides");
                            ArrayList<InventoryModel_Area.PropertySides> propertySides = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertySides.length(); k++) {
                                InventoryModel_Area.PropertySides propertySides1 = inventoryModels.new PropertySides();
                                JSONObject jsonObjectPropertySides = jsonArrayPropertySides.getJSONObject(k);
                                if(jsonObjectPropertySides.getString("Id").equalsIgnoreCase("null"))
                                    propertySides1.setId("N/A");
                                else
                                    propertySides1.setId(jsonObjectPropertySides.getString("Id"));
                                if(jsonObjectPropertySides.getString("Name").equalsIgnoreCase("null"))
                                    propertySides1.setName("N/A");
                                else
                                    propertySides1.setName(jsonObjectPropertySides.getString("Name"));
                                if(jsonObjectPropertySides.getString("Length").equalsIgnoreCase("null"))
                                    propertySides1.setLength("N/A");
                                else
                                    propertySides1.setLength(jsonObjectPropertySides.getString("Length"));
                                if(jsonObjectPropertySides.getString("LengthUnitId").equalsIgnoreCase("null"))
                                    propertySides1.setLengthUnitId("N/A");
                                else
                                    propertySides1.setLengthUnitId(jsonObjectPropertySides.getString("LengthUnitId"));
                                if(jsonObjectPropertySides.getString("Width").equalsIgnoreCase("null"))
                                    propertySides1.setWidth("N/A");
                                else
                                    propertySides1.setWidth(jsonObjectPropertySides.getString("Width"));
                                if(jsonObjectPropertySides.getString("WidthUnitId").equalsIgnoreCase("null"))
                                    propertySides1.setWidthUnitId("N/A");
                                else
                                    propertySides1.setWidthUnitId(jsonObjectPropertySides.getString("WidthUnitId"));
                                propertySides.add(propertySides1);
                            }
                            JSONArray jsonArrayPropertyFinancials = propertySize.getJSONArray("PropertyFinancials");
                            ArrayList<InventoryModel_Area.PropertyFinancials> propertyFinancialses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyFinancials.length(); k++) {
                                InventoryModel_Area.PropertyFinancials propertyFinancials = inventoryModels.new PropertyFinancials();
                                JSONObject jsonObjectPropertyFinancials = jsonArrayPropertyFinancials.getJSONObject(k);
                                if(jsonObjectPropertyFinancials.getString("Id").equalsIgnoreCase("null"))
                                    propertyFinancials.setId("N/A");
                                else
                                    propertyFinancials.setId(jsonObjectPropertyFinancials.getString("Id"));
                                if(jsonObjectPropertyFinancials.getString("DemandPrice").equalsIgnoreCase("null"))
                                    propertyFinancials.setDemandPrice("N/A");
                                else
                                    propertyFinancials.setDemandPrice(jsonObjectPropertyFinancials.getString("DemandPrice"));
                                if(jsonObjectPropertyFinancials.getString("InclusivePrice").equalsIgnoreCase("null"))
                                    propertyFinancials.setInclusivePrice("N/A");
                                else
                                    propertyFinancials.setInclusivePrice(jsonObjectPropertyFinancials.getString("InclusivePrice"));
                                if(jsonObjectPropertyFinancials.getString("IsNegociable").equalsIgnoreCase("null"))
                                    propertyFinancials.setIsNegociable("N/A");
                                else
                                    propertyFinancials.setIsNegociable(jsonObjectPropertyFinancials.getString("IsNegociable"));
                                if(jsonObjectPropertyFinancials.getString("Price").equalsIgnoreCase("null"))
                                    propertyFinancials.setPrice("N/A");
                                else
                                    propertyFinancials.setPrice(jsonObjectPropertyFinancials.getString("Price"));
                                if(jsonObjectPropertyFinancials.getString("PriceUnitId").equalsIgnoreCase("null"))
                                    propertyFinancials.setPriceUnitId("N/A");
                                else
                                    propertyFinancials.setPriceUnitId(jsonObjectPropertyFinancials.getString("PriceUnitId"));
                                propertyFinancialses.add(propertyFinancials);

                            }
                            JSONArray jsonArrayPropertyLoans = propertySize.getJSONArray("PropertyLoans");
                            ArrayList<InventoryModel_Area.PropertyLoans> propertyLoanses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyLoans.length(); k++) {
                                InventoryModel_Area.PropertyLoans propertyLoans = inventoryModels.new PropertyLoans();
                                JSONObject jsonObjectPropertyLoans = jsonArrayPropertyLoans.getJSONObject(k);
                                if(jsonObjectPropertyLoans.getString("BankName").equalsIgnoreCase("null"))
                                    propertyLoans.setBankName("N/A");
                                else
                                    propertyLoans.setBankName(jsonObjectPropertyLoans.getString("BankName"));
                                if(jsonObjectPropertyLoans.getString("Id").equalsIgnoreCase("null"))
                                    propertyLoans.setId("N/A");
                                else
                                    propertyLoans.setId(jsonObjectPropertyLoans.getString("Id"));
                                if(jsonObjectPropertyLoans.getString("LoanAmount").equalsIgnoreCase("null"))
                                    propertyLoans.setLoanAmount("N/A");
                                else
                                    propertyLoans.setLoanAmount(jsonObjectPropertyLoans.getString("LoanAmount"));
                                if(jsonObjectPropertyLoans.getString("LoanTenure").equalsIgnoreCase("null"))
                                    propertyLoans.setLoanTenure("N/A");
                                else
                                    propertyLoans.setLoanTenure(jsonObjectPropertyLoans.getString("LoanTenure"));
                                if(jsonObjectPropertyLoans.getString("FullDisbursed").equalsIgnoreCase("null"))
                                    propertyLoans.setFullDisbursed("N/A");
                                else
                                    propertyLoans.setFullDisbursed(jsonObjectPropertyLoans.getString("FullDisbursed"));
                                if(jsonObjectPropertyLoans.getString("LastInstallmentPaidOn").equalsIgnoreCase("null"))
                                    propertyLoans.setLastInstallmentPaidOn("N/A");
                                else
                                    propertyLoans.setLastInstallmentPaidOn(jsonObjectPropertyLoans.getString("LastInstallmentPaidOn"));
                                if(jsonObjectPropertyLoans.getString("TotalOutstandingAfterLastInstallment").equalsIgnoreCase("null"))
                                    propertyLoans.setTotalOutstandingAfterLastInstallment("N/A");
                                else
                                    propertyLoans.setTotalOutstandingAfterLastInstallment(jsonObjectPropertyLoans.getString("TotalOutstandingAfterLastInstallment"));
                                propertyLoanses.add(propertyLoans);

                            }
                            JSONArray jsonArrayPropertyOtherExpences = propertySize.getJSONArray("PropertyOtherExpences");
                            ArrayList<InventoryModel_Area.PropertyOtherExpences> propertyOtherExpences = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOtherExpences.length(); k++) {
                                InventoryModel_Area.PropertyOtherExpences propertyOtherExpences1 = inventoryModels.new PropertyOtherExpences();
                                JSONObject jsonObjectPropertyOtherExpences = jsonArrayPropertyOtherExpences.getJSONObject(k);
                                if(jsonObjectPropertyOtherExpences.getString("Id").equalsIgnoreCase("null"))
                                    propertyOtherExpences1.setId("N/A");
                                else
                                    propertyOtherExpences1.setId(jsonObjectPropertyOtherExpences.getString("Id"));
                                if(jsonObjectPropertyOtherExpences.getString("Title").equalsIgnoreCase("null"))
                                    propertyOtherExpences1.setTitle("N/A");
                                else
                                    propertyOtherExpences1.setTitle(jsonObjectPropertyOtherExpences.getString("Title"));
                                if(jsonObjectPropertyOtherExpences.getString("Value").equalsIgnoreCase("null"))
                                    propertyOtherExpences1.setValue("N/A");
                                else
                                    propertyOtherExpences1.setValue(jsonObjectPropertyOtherExpences.getString("Value"));
                                if(jsonObjectPropertyOtherExpences.getString("LastPaidDate").equalsIgnoreCase("null"))
                                    propertyOtherExpences1.setLastPaidDate("N/A");
                                else
                                    propertyOtherExpences1.setLastPaidDate(jsonObjectPropertyOtherExpences.getString("LastPaidDate"));
                                propertyOtherExpences.add(propertyOtherExpences1);

                            }

                            JSONArray jsonArrayPropertyLegalStatus = propertySize.getJSONArray("PropertyLegalStatus");
                            ArrayList<InventoryModel_Area.PropertyLegalStatus> propertyLegalStatuses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyLegalStatus.length(); k++) {
                                InventoryModel_Area.PropertyLegalStatus propertyLegalStatus = inventoryModels.new PropertyLegalStatus();
                                JSONObject jsonObjectPropertyOwners = jsonArrayPropertyLegalStatus.getJSONObject(k);
                                if(jsonObjectPropertyOwners.getString("Id").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setId("N/A");
                                else
                                    propertyLegalStatus.setId(jsonObjectPropertyOwners.getString("Id"));
                                if(jsonObjectPropertyOwners.getString("PropertyId").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setPropertyId("N/A");
                                else
                                    propertyLegalStatus.setPropertyId(jsonObjectPropertyOwners.getString("PropertyId"));
                                if(jsonObjectPropertyOwners.getString("TransferRestriction").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setTransferRestriction("N/A");
                                else
                                    propertyLegalStatus.setTransferRestriction(jsonObjectPropertyOwners.getString("TransferRestriction"));
                                if(jsonObjectPropertyOwners.getString("OwnerShipStatus").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setOwnerShipStatus("N/A");
                                else
                                    propertyLegalStatus.setOwnerShipStatus(jsonObjectPropertyOwners.getString("OwnerShipStatus"));
                                if(jsonObjectPropertyOwners.getString("OwnerShipType").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setOwnerShipType("N/A");
                                else
                                    propertyLegalStatus.setOwnerShipType(jsonObjectPropertyOwners.getString("OwnerShipType"));
                                if(jsonObjectPropertyOwners.getString("TransferByWayOf").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setTransferByWayOf("N/A");
                                else
                                    propertyLegalStatus.setTransferByWayOf(jsonObjectPropertyOwners.getString("TransferByWayOf"));
                                if(jsonObjectPropertyOwners.getString("OwnedBy").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setOwnedBy("N/A");
                                else
                                    propertyLegalStatus.setOwnedBy(jsonObjectPropertyOwners.getString("OwnedBy"));
                                if(jsonObjectPropertyOwners.getString("PossessionDate").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setPossessionDate("N/A");
                                else
                                    propertyLegalStatus.setPossessionDate(jsonObjectPropertyOwners.getString("PossessionDate"));
                                if(jsonObjectPropertyOwners.getString("DateFrom").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setDateFrom("N/A");
                                else
                                    propertyLegalStatus.setDateFrom(jsonObjectPropertyOwners.getString("DateFrom"));
                                if(jsonObjectPropertyOwners.getString("NoOfTennure").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setNoOfTennure("N/A");
                                else
                                    propertyLegalStatus.setNoOfTennure(jsonObjectPropertyOwners.getString("NoOfTennure"));
                                if(jsonObjectPropertyOwners.getString("LeaseAlignmentPaid").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setLeaseAlignmentPaid("N/A");
                                else
                                    propertyLegalStatus.setLeaseAlignmentPaid(jsonObjectPropertyOwners.getString("LeaseAlignmentPaid"));
                                if(jsonObjectPropertyOwners.getString("LastPaidDate").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setLastPaidDate("N/A");
                                else
                                    propertyLegalStatus.setLastPaidDate(jsonObjectPropertyOwners.getString("LastPaidDate"));
                                if(jsonObjectPropertyOwners.getString("LeaseAmountPayable").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setLeaseAmountPayable("N/A");
                                else
                                    propertyLegalStatus.setLeaseAmountPayable(jsonObjectPropertyOwners.getString("LeaseAmountPayable"));
                                if(jsonObjectPropertyOwners.getString("PaymentCircle").equalsIgnoreCase("null"))
                                    propertyLegalStatus.setPaymentCircle("N/A");
                                else
                                    propertyLegalStatus.setPaymentCircle(jsonObjectPropertyOwners.getString("PaymentCircle"));
                                propertyLegalStatuses.add(propertyLegalStatus);

                            }

                            JSONArray jsonArrayPropertyOwners = propertySize.getJSONArray("PropertyOwners");
                            ArrayList<InventoryModel_Area.PropertyOwners> propertyOwnerses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOwners.length(); k++) {
                                InventoryModel_Area.PropertyOwners propertyOwners = inventoryModels.new PropertyOwners();
                                JSONObject jsonObjectPropertyOwners = jsonArrayPropertyOwners.getJSONObject(k);
                                if(jsonObjectPropertyOwners.getString("Id").equalsIgnoreCase("null"))
                                    propertyOwners.setId("N/A");
                                else
                                    propertyOwners.setId(jsonObjectPropertyOwners.getString("Id"));
                                if(jsonObjectPropertyOwners.getString("ContactId").equalsIgnoreCase("null"))
                                    propertyOwners.setCompanyId("N/A");
                                else
                                    propertyOwners.setContactId(jsonObjectPropertyOwners.getString("ContactId"));
                                if(jsonObjectPropertyOwners.getString("PanNo").equalsIgnoreCase("null"))
                                    propertyOwners.setPanNo("N/A");
                                else
                                    propertyOwners.setPanNo(jsonObjectPropertyOwners.getString("PanNo"));
                                if(jsonObjectPropertyOwners.getString("AdhaarCardNo").equalsIgnoreCase("null"))
                                    propertyOwners.setAdhaarCardNo("N/A");
                                else
                                    propertyOwners.setAdhaarCardNo(jsonObjectPropertyOwners.getString("AdhaarCardNo"));
                                if(jsonObjectPropertyOwners.getString("Address").equalsIgnoreCase("null"))
                                    propertyOwners.setAddress("N/A");
                                else
                                    propertyOwners.setAddress(jsonObjectPropertyOwners.getString("Address"));
                                if(jsonObjectPropertyOwners.getString("FirstName").equalsIgnoreCase("null"))
                                    propertyOwners.setFirstName("N/A");
                                else
                                    propertyOwners.setFirstName(jsonObjectPropertyOwners.getString("FirstName"));
                                if(jsonObjectPropertyOwners.getString("LastName").equalsIgnoreCase("null"))
                                    propertyOwners.setLastName("N/A");
                                else
                                    propertyOwners.setLastName(jsonObjectPropertyOwners.getString("LastName"));
                                if(jsonObjectPropertyOwners.getString("Email").equalsIgnoreCase("null"))
                                    propertyOwners.setEmail("N/A");
                                else
                                    propertyOwners.setEmail(jsonObjectPropertyOwners.getString("Email"));
                                if(jsonObjectPropertyOwners.getString("PhoneNumber").equalsIgnoreCase("null"))
                                    propertyOwners.setPhoneNumber("N/A");
                                else
                                    propertyOwners.setPhoneNumber(jsonObjectPropertyOwners.getString("PhoneNumber"));
                                if(jsonObjectPropertyOwners.getString("Occupation").equalsIgnoreCase("null"))
                                    propertyOwners.setOccupation("N/A");
                                else
                                    propertyOwners.setOccupation(jsonObjectPropertyOwners.getString("Occupation"));
                                if(jsonObjectPropertyOwners.getString("CompanyName").equalsIgnoreCase("null"))
                                    propertyOwners.setCompanyName("N/A");
                                else
                                    propertyOwners.setCompanyName(jsonObjectPropertyOwners.getString("CompanyName"));
                                if(jsonObjectPropertyOwners.getString("CompanyType").equalsIgnoreCase("null"))
                                    propertyOwners.setCompanyType("N/A");
                                else
                                    propertyOwners.setCompanyType(jsonObjectPropertyOwners.getString("CompanyType"));
                                propertyOwnerses.add(propertyOwners);

                            }

                            JSONArray jsonArrayPropertyOtherquestions = propertySize.getJSONArray("PropertyOtherQuestions");
                            ArrayList<InventoryModel_Area.PropertyOtherQuestions> propertyOtherQuestionses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOtherquestions.length(); k++) {
                                InventoryModel_Area.PropertyOtherQuestions propertyOtherQuestions = inventoryModels.new PropertyOtherQuestions();
                                JSONObject jsonObjectPropertyOtherQuestions = jsonArrayPropertyOtherquestions.getJSONObject(k);
                                if(jsonObjectPropertyOtherQuestions.getString("Id").equalsIgnoreCase("null"))
                                    propertyOtherQuestions.setId("N/A");
                                else
                                    propertyOtherQuestions.setId(jsonObjectPropertyOtherQuestions.getString("Id"));
                                if(jsonObjectPropertyOtherQuestions.getString("ContactId").equalsIgnoreCase("null"))
                                    propertyOtherQuestions.setContactId("N/A");
                                else
                                    propertyOtherQuestions.setContactId(jsonObjectPropertyOtherQuestions.getString("ContactId"));

                                InventoryModel_Area.PropertyOtherQuestions.Contact contact = inventoryModels.new PropertyOtherQuestions().new Contact();
                                JSONObject jsonContact = jsonObjectPropertyOtherQuestions.getJSONObject("Contact");

                                if(jsonContact.getString("Prefix").equalsIgnoreCase("null"))
                                    contact.setPrefix("N/A");
                                else
                                    contact.setPrefix(jsonContact.getString("Prefix"));
                                if(jsonContact.getString("FirstName").equalsIgnoreCase("null"))
                                    contact.setFirstName("N/A");
                                else
                                    contact.setFirstName(jsonContact.getString("FirstName"));
                                if(jsonContact.getString("LastName").equalsIgnoreCase("null"))
                                    contact.setLastName("N/A");
                                else
                                    contact.setLastName(jsonContact.getString("LastName"));
                                if(jsonContact.getString("MiddleName").equalsIgnoreCase("null"))
                                    contact.setMiddleName("N/A");
                                else
                                    contact.setMiddleName(jsonContact.getString("MiddleName"));
                                if(jsonContact.getString("Designation").equalsIgnoreCase("null"))
                                    contact.setDesignation("N/A");
                                else
                                    contact.setDesignation(jsonContact.getString("Designation"));

                                JSONArray addressArray = jsonContact.getJSONArray("Addresses");
                                ArrayList<InventoryModel_Area.PropertyOtherQuestions.Contact.Addresses> addressesArrayList = new ArrayList<>();
                                addressesArrayList.clear();

                                for(int i = 0; i < addressArray.length(); i++){
                                    InventoryModel_Area.PropertyOtherQuestions.Contact.Addresses addresses = inventoryModels. new PropertyOtherQuestions(). new Contact(). new Addresses();
                                    JSONObject addressObject = addressArray.getJSONObject(i);
                                    if(addressObject.getString("UnitName").equalsIgnoreCase("null"))
                                        addresses.setUnitName("N/A");
                                    else
                                        addresses.setUnitName(addressObject.getString("UnitName"));
                                    if(addressObject.getString("SubAreaName").equalsIgnoreCase("null"))
                                        addresses.setSubAreaName("N/A");
                                    else
                                        addresses.setSubAreaName(addressObject.getString("SubAreaName"));
                                    if(addressObject.getString("LocalityName").equalsIgnoreCase("null"))
                                        addresses.setLocalityName("N/A");
                                    else
                                        addresses.setLocalityName(addressObject.getString("LocalityName"));
                                    if(addressObject.getString("VillageName").equalsIgnoreCase("null"))
                                        addresses.setVillageName("N/A");
                                    else
                                        addresses.setVillageName(addressObject.getString("VillageName"));
                                    if(addressObject.getString("TownName").equalsIgnoreCase("null"))
                                        addresses.setTownName("N/A");
                                    else
                                        addresses.setTownName(addressObject.getString("TownName"));

                                    addressesArrayList.add(addresses);
                                }
                                contact.setAddresses(addressesArrayList);

                                JSONArray phoneArray = jsonContact.getJSONArray("Phones");
                                ArrayList<InventoryModel_Area.PropertyOtherQuestions.Contact.Phones> phonesArrayList = new ArrayList<>();
                                phonesArrayList.clear();

                                for(int i = 0; i < phoneArray.length(); i++){
                                    InventoryModel_Area.PropertyOtherQuestions.Contact.Phones phones = inventoryModels. new PropertyOtherQuestions(). new Contact(). new Phones();
                                    JSONObject phoneObject = phoneArray.getJSONObject(i);

                                    if(phoneObject.getString("PhoneCode").equalsIgnoreCase("null"))
                                        phones.setPhoneCode("N/A");
                                    else
                                        phones.setPhoneCode(phoneObject.getString("PhoneCode"));
                                    if(phoneObject.getString("PhoneNumber").equalsIgnoreCase("null"))
                                        phones.setPhoneNumber("N/A");
                                    else
                                        phones.setPhoneNumber(phoneObject.getString("PhoneNumber"));
                                    phonesArrayList.add(phones);
                                }
                                contact.setPhones(phonesArrayList);

                                JSONArray emailArray = jsonContact.getJSONArray("EmailAddresses");
                                ArrayList<InventoryModel_Area.PropertyOtherQuestions.Contact.EmailAddresses> emailAddressesArrayList = new ArrayList<>();
                                emailAddressesArrayList.clear();

                                for(int i = 0; i < emailArray.length(); i++){
                                    InventoryModel_Area.PropertyOtherQuestions.Contact.EmailAddresses emailAddresses = inventoryModels. new PropertyOtherQuestions(). new Contact(). new EmailAddresses();
                                    JSONObject emailObject = emailArray.getJSONObject(i);

                                    if(emailObject.getString("Address").equalsIgnoreCase("null"))
                                        emailAddresses.setAddress("N/A");
                                    else
                                        emailAddresses.setAddress(emailObject.getString("Address"));
                                    emailAddressesArrayList.add(emailAddresses);
                                }
                                contact.setEmailAddresses(emailAddressesArrayList);
                                propertyOtherQuestions.setContact(contact);
                                if(jsonObjectPropertyOtherQuestions.getString("Relation").equalsIgnoreCase("null"))
                                    propertyOtherQuestions.setRelation("N/A");
                                else
                                    propertyOtherQuestions.setRelation(jsonObjectPropertyOtherQuestions.getString("Relation"));
                                if(jsonObjectPropertyOtherQuestions.getString("SubmitOfBehalfOf").equalsIgnoreCase("null"))
                                    propertyOtherQuestions.setSubmitOfBehalfOf("N/A");
                                else
                                    propertyOtherQuestions.setSubmitOfBehalfOf(jsonObjectPropertyOtherQuestions.getString("SubmitOfBehalfOf"));
                                if(jsonObjectPropertyOtherQuestions.getString("AgentId").equalsIgnoreCase("null"))
                                    propertyOtherQuestions.setAgentId("N/A");
                                propertyOtherQuestions.setAgentId(jsonObjectPropertyOtherQuestions.getString("AgentId"));
                                if(jsonObjectPropertyOtherQuestions.getString("FirstName").equalsIgnoreCase("null"))
                                    propertyOtherQuestions.setFirstName("N/A");
                                else
                                    propertyOtherQuestions.setFirstName(jsonObjectPropertyOtherQuestions.getString("FirstName"));
                                if(jsonObjectPropertyOtherQuestions.getString("LastName").equalsIgnoreCase("null"))
                                    propertyOtherQuestions.setLastName("N/A");
                                else
                                    propertyOtherQuestions.setLastName(jsonObjectPropertyOtherQuestions.getString("LastName"));
                                if(jsonObjectPropertyOtherQuestions.getString("Email").equalsIgnoreCase("null"))
                                    propertyOtherQuestions.setEmail("N/A");
                                else
                                    propertyOtherQuestions.setEmail(jsonObjectPropertyOtherQuestions.getString("Email"));
                                propertyOtherQuestionses.add(propertyOtherQuestions);

                            }

                            JSONArray jsonArrayPropertyBedrooms = propertySize.getJSONArray("PropertyBedrooms");
                            ArrayList<InventoryModel_Area.PropertyBedrooms> propertyBedroomses = new ArrayList<>();
                            propertyBedroomses.clear();
                            for (int k = 0; k < jsonArrayPropertyBedrooms.length(); k++) {
                                InventoryModel_Area.PropertyBedrooms propertyBedrooms = inventoryModels.new PropertyBedrooms();
                                JSONObject jsonObjectPropertyBedrooms = jsonArrayPropertyBedrooms.getJSONObject(k);
                                if(jsonObjectPropertyBedrooms.getString("Id").equalsIgnoreCase("null"))
                                    propertyBedrooms.setId("N/A");
                                else
                                    propertyBedrooms.setId(jsonObjectPropertyBedrooms.getString("Id"));
                                if(jsonObjectPropertyBedrooms.getString("BedRoomName").equalsIgnoreCase("null"))
                                    propertyBedrooms.setBedRoomName("N/A");
                                else
                                    propertyBedrooms.setBedRoomName(jsonObjectPropertyBedrooms.getString("BedRoomName"));
                                if(jsonObjectPropertyBedrooms.getString("GroundType").equalsIgnoreCase("null"))
                                    propertyBedrooms.setGroundType("N/A");
                                else
                                    propertyBedrooms.setGroundType(jsonObjectPropertyBedrooms.getString("GroundType"));
                                if(jsonObjectPropertyBedrooms.getString("FloorNo").equalsIgnoreCase("null"))
                                    propertyBedrooms.setFloorNo("N/A");
                                else
                                    propertyBedrooms.setFloorNo(jsonObjectPropertyBedrooms.getString("FloorNo"));
                                if(jsonObjectPropertyBedrooms.getString("BedRoomType").equalsIgnoreCase("null"))
                                    propertyBedrooms.setBedRoomType("N/A");
                                else
                                    propertyBedrooms.setBedRoomType(jsonObjectPropertyBedrooms.getString("BedRoomType"));
                                if(jsonObjectPropertyBedrooms.getString("Category").equalsIgnoreCase("null"))
                                    propertyBedrooms.setCategory("N/A");
                                else
                                    propertyBedrooms.setCategory(jsonObjectPropertyBedrooms.getString("Category"));

                                propertyBedroomses.add(propertyBedrooms);

                            }

                            JSONArray jsonArrayPropertyAccomodation = propertySize.getJSONArray("PropertyAccommodationDetails");
                            ArrayList<InventoryModel_Area.PropertyAccommodationDetails> accommodationDetails = new ArrayList<>();
                            accommodationDetails.clear();
                            for(int k = 0; k < jsonArrayPropertyAccomodation.length(); k++){
                                InventoryModel_Area.PropertyAccommodationDetails accommodationDetails1 = inventoryModels.new PropertyAccommodationDetails();
                                JSONObject object = jsonArrayPropertyAccomodation.getJSONObject(k);
                                if(object.getString("Id").equalsIgnoreCase("null"))
                                    accommodationDetails1.setId("N/A");
                                else
                                    accommodationDetails1.setId(object.getString("Id"));
                                if(object.getString("PropertyId").equalsIgnoreCase("null"))
                                    accommodationDetails1.setPropertyId("N/A");
                                else
                                    accommodationDetails1.setPropertyId(object.getString("PropertyId"));
                                if(object.getString("Name").equalsIgnoreCase("null"))
                                    accommodationDetails1.setName("N/A");
                                else
                                    accommodationDetails1.setName(object.getString("Name"));
                                if(object.getString("Type").equalsIgnoreCase("null"))
                                    accommodationDetails1.setType("N/A");
                                else
                                    accommodationDetails1.setType(object.getString("Type"));
                                if(object.getString("TotalCount").equalsIgnoreCase("null"))
                                    accommodationDetails1.setTotalCount("N/A");
                                else
                                    accommodationDetails1.setTotalCount(object.getString("TotalCount"));

                                accommodationDetails.add(accommodationDetails1);
                            }

                            JSONArray jsonArrayPropertySpecifications = propertySize.getJSONArray("PropertySpecifications");
                            ArrayList<InventoryModel_Area.PropertySpecifications> propertySpecificationses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertySpecifications.length(); k++) {
                                InventoryModel_Area.PropertySpecifications propertySpecifications = inventoryModels.new PropertySpecifications();
                                JSONObject jsonObjectPropertySpecifications = jsonArrayPropertySpecifications.getJSONObject(k);
                                if(jsonObjectPropertySpecifications.getString("Id").equalsIgnoreCase("null"))
                                    propertySpecifications.setId("N/A");
                                else
                                    propertySpecifications.setId(jsonObjectPropertySpecifications.getString("Id"));
                                if(jsonObjectPropertySpecifications.getString("PropertyId").equalsIgnoreCase("null"))
                                    propertySpecifications.setPropertyId("N/A");
                                else
                                    propertySpecifications.setPropertyId(jsonObjectPropertySpecifications.getString("PropertyId"));
                                if(jsonObjectPropertySpecifications.getString("Name").equalsIgnoreCase("null"))
                                    propertySpecifications.setName("N/A");
                                else
                                    propertySpecifications.setName(jsonObjectPropertySpecifications.getString("Name"));
                                if(jsonObjectPropertySpecifications.getString("Category").equalsIgnoreCase("null"))
                                    propertySpecifications.setCategory("N/A");
                                else
                                    propertySpecifications.setCategory(jsonObjectPropertySpecifications.getString("Category"));
                                if(jsonObjectPropertySpecifications.getString("Description").equalsIgnoreCase("null"))
                                    propertySpecifications.setDescription("N/A");
                                else
                                    propertySpecifications.setDescription(jsonObjectPropertySpecifications.getString("Description"));


                                propertySpecificationses.add(propertySpecifications);

                            }

                            inventoryModels.setPropertyLoanses(propertyLoanses);
                            inventoryModels.setPropertyLocations(propertyLocations);
                            inventoryModels.setPropertyAreas(propertyAreas);
                            inventoryModels.setPropertyFinancialses(propertyFinancialses);
                            inventoryModels.setPropertyImages(propertyImages);
                            inventoryModels.setPropertyOtherExpences(propertyOtherExpences);
                            inventoryModels.setPropertyPlcs(propertyPlcs);
                            inventoryModels.setPropertyOtherQuestionses(propertyOtherQuestionses);
                            inventoryModels.setPropertyOwnerses(propertyOwnerses);
                            inventoryModels.setPropertyLegalStatuses(propertyLegalStatuses);
                            inventoryModels.setPropertyBedroomses(propertyBedroomses);
                            inventoryModels.setPropertyAccommodationDetailses(accommodationDetails);
                            inventoryModels.setPropertySpecificationses(propertySpecificationses);
                            property_data.add(inventoryModels);

                        }

                        BuildView builtViewFragment = new BuildView();
                        Bundle builtViewbundle = new Bundle();


                        builtViewbundle.putSerializable("property_data", property_data);
                        builtViewFragment.setArguments(builtViewbundle);
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, builtViewFragment).commit();

                        property_type_txt.setText(property_data.get(0).getNameobject());

                        property_type.setText(Utils.getEmptyValue(property_data.get(0).getTitle()));
                        if(!property_data.get(0).getPropertyLocations().get(0).getUnitName().equalsIgnoreCase("") || !property_data.get(0).getPropertyLocations().get(0).getUnitName().equalsIgnoreCase("null") )
                            unitName = property_data.get(0).getPropertyLocations().get(0).getUnitName() + " ";
                        else
                            unitName = "";

                        if(!property_data.get(0).getPropertyLocations().get(0).getFloorNo().equalsIgnoreCase("") || !property_data.get(0).getPropertyLocations().get(0).getFloorNo().equalsIgnoreCase("null") )
                            floorName = property_data.get(0).getPropertyLocations().get(0).getFloorNo() + " ";
                        else
                            floorName = "";

                        if(!property_data.get(0).getPropertyLocations().get(0).getTowerName().equalsIgnoreCase("") || !property_data.get(0).getPropertyLocations().get(0).getTowerName().equalsIgnoreCase("null") )
                            towerName = property_data.get(0).getPropertyLocations().get(0).getTowerName() + " ";
                        else
                            towerName = "";

                        if(!property_data.get(0).getPropertyLocations().get(0).getSubArea().equalsIgnoreCase("") || !property_data.get(0).getPropertyLocations().get(0).getSubArea().equalsIgnoreCase("null") )
                            subArea = property_data.get(0).getPropertyLocations().get(0).getSubArea() + " ";
                        else
                            subArea = "";

                        if(!property_data.get(0).getPropertyLocations().get(0).getSubDivision().equalsIgnoreCase("") || !property_data.get(0).getPropertyLocations().get(0).getSubDivision().equalsIgnoreCase("null") )
                            subDivision = property_data.get(0).getPropertyLocations().get(0).getSubDivision() + " ";
                        else
                            subDivision = "";

                        if(!property_data.get(0).getPropertyLocations().get(0).getDistrict().equalsIgnoreCase("") || !property_data.get(0).getPropertyLocations().get(0).getDistrict().equalsIgnoreCase("null") )
                            district = property_data.get(0).getPropertyLocations().get(0).getDistrict() + " ";
                        else
                            district = "";

                        if(!property_data.get(0).getPropertyLocations().get(0).getState().equalsIgnoreCase("") || !property_data.get(0).getPropertyLocations().get(0).getState().equalsIgnoreCase("null") )
                            state = property_data.get(0).getPropertyLocations().get(0).getState();
                        else
                            state = "";

                        locality.setText(unitName + floorName + towerName + subArea + subDivision + district + state);
                        try {
                            parking.setText(property_data.get(0).getPropertyAccommodationDetailses().get(0).getTotalCount() + " Parking");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            double price_txt = Double.parseDouble(String.valueOf(property_data.get(0).getPropertyFinancialses().get(0).getDemandPrice()));
                            price.setText(Utils.getConvertedPrice((long) price_txt, this));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            area_unit.setText(property_data.get(0).getPropertyAreas().get(0).getPlotArea() + " " + property_data.get(0).getPropertyAreas().get(0).getPropertySizeUnit4());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            for(int i=0;i<property_data.get(0).getPropertyBedroomses().size();i++){
                                if(property_data.get(0).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("room")){
                                    room_count=room_count+1;
                                }
                                if(property_data.get(0).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("kitchen")){
                                    kitchen_count=kitchen_count+1;
                                }

                                if(property_data.get(0).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("bathroom")){
                                    bathroom_count=bathroom_count+1;
                                }

                                if(property_data.get(0).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("parking")){
                                    parking_count=parking_count+1;
                                }
                            }


                            bedroom.setText(room_count+" Bedroom" );
                            kitchen.setText(kitchen_count+" Kitchen");
                            bathroom.setText(bathroom_count+" Bathroom");
                            parking.setText(parking_count+" Parking");

                            for(int index=0;index<property_data.get(0).getPropertyAccommodationDetailses().size();index++){
                                if(property_data.get(0).getPropertyAccommodationDetailses().get(index).getType().equalsIgnoreCase("Parking")){
                                    parking.setText(property_data.get(0).getPropertyAccommodationDetailses().get(index).getTotalCount()+" Parking");
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                                .build();

                        ImageLoader imageLoader = ImageLoader.getInstance();
                        if (!imageLoader.isInited()) {
                            imageLoader.init(config);
                        }
                        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                                .cacheOnDisc(true).resetViewBeforeLoading(true)
                                .showImageForEmptyUri(R.drawable.image_logo)
                                .showImageOnFail(R.drawable.image_logo)
                                .showImageOnLoading(R.drawable.image_logo).build();
                        if (!property_data.get(0).getPropertyImages().isEmpty())
                            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + property_data.get(0).getPropertyImages().get(0).getUrl(), project_image, options);


                    }
                    if(property_data.get(0).getPropertyImages().isEmpty()){
                        gallery_list.setVisibility(View.GONE);
                    }else{
                        gallery_list.setAdapter(new GalleryListAdapter(this,property_data.get(0).getPropertyImages()));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void mapPopup() {
        dialog = new Dialog(PropertyDetail.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map_pop_up);
        close = (ImageView) dialog.findViewById(R.id.close);
        tab_layout_map = (TabLayout) dialog.findViewById(R.id.tab_layout_map);
        Tab11 = tab_layout_map.newTab().setText("Project Layout Plan");
        Tab22 = tab_layout_map.newTab().setText("Tower Cluster Plan");
        tab_layout_map.addTab(Tab11);
        tab_layout_map.addTab(Tab22);
        tab_layout_map.setTabGravity(TabLayout.GRAVITY_FILL);
        tab_layout_map.getTabAt(0);
        Tab11.select();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

    }

    private void recommendPropertyPopup(){
        dialog = new Dialog(PropertyDetail.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.recommend_property_popup);
        close = (ImageView)dialog.findViewById(R.id.close);
        recommend_project_image = (ImageView)dialog.findViewById(R.id.recommend_project_image);
        recommend_property_type = (TextView) dialog.findViewById(R.id.recommend_property_type);
        recommend_property_locality = (TextView) dialog.findViewById(R.id.recommend_property_locality);
        Picasso.with(PropertyDetail.this).load(Urls.BASE_CONTACT_IMAGE_URL + property_data.get(0).getPropertyImages().get(0).getUrl()).into(recommend_project_image);
        recommend_property_type.setText(Utils.getEmptyValue(property_data.get(0).getTitle()));
        recommend_property_locality.setText(unitName + floorName + towerName + subArea + subDivision + district + state);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }

}
