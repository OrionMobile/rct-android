package com.bnhabitat.areaModule.areaActivities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.Developer_list;
import com.bnhabitat.areaModule.adapter.Govt_Dept_Adapter;
import com.bnhabitat.areaModule.adapter.Govt_Notification_Adapter;
import com.bnhabitat.areaModule.adapter.ImageAdapter;
import com.bnhabitat.areaModule.adapter.Image_Video_Adapter;
import com.bnhabitat.areaModule.adapter.StateImageAdapter;

import java.util.ArrayList;

public class CityDetail extends AppCompatActivity implements ViewPager.OnPageChangeListener, ViewPagerListener{

    RecyclerView list_images_videos, developer_list, govt_dept_rep, govt_notification_rep, latest_update_rep, gmada_videos, gmada_photos;

    ArrayList<Integer> array_image_videos = new ArrayList<Integer>();
    ArrayList<String> developer_name = new ArrayList<>();
    ArrayList<String> govt_detail_list = new ArrayList<>();
    ArrayList<String> govt_notify_list = new ArrayList<>();
    ArrayList<Integer> mohali_videos = new ArrayList<Integer>();

    ViewPager viewPager;

    ImageAdapter imageAdapter;
    Image_Video_Adapter image_video_adapter;
    Govt_Dept_Adapter govt_dept_adapter;
    Developer_list developer_list_adapter;
    Govt_Notification_Adapter govt_notification_adapter;
    StateImageAdapter stateImageAdapter;

    ImageView imgBack, imgPrevious, imgNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_detail);

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.view_images);
        imgPrevious = (ImageView) findViewById(R.id.imgPrevious);
        imgNext = (ImageView) findViewById(R.id.imgNext);
        list_images_videos = (RecyclerView) findViewById(R.id.list_images_videos);
        developer_list = (RecyclerView) findViewById(R.id.developer_list);
        govt_dept_rep = (RecyclerView) findViewById(R.id.govt_dept_rep);
        govt_notification_rep = (RecyclerView) findViewById(R.id.govt_notification_rep);
        latest_update_rep = (RecyclerView) findViewById(R.id.latest_update_rep);
        gmada_photos = (RecyclerView) findViewById(R.id.gmada_photos);

        array_image_videos.add(R.drawable.mohali);
        array_image_videos.add(R.drawable.fortis);
        array_image_videos.add(R.drawable.gillco_valley);
        array_image_videos.add(R.drawable.connaught_residency);
        array_image_videos.add(R.drawable.iiser);

        mohali_videos.add(R.drawable.mohali);
        mohali_videos.add(R.drawable.fortis);

        govt_detail_list.add("Department of Local Government");
        govt_detail_list.add("PUDA");
        govt_detail_list.add("RERA");
        govt_detail_list.add("Real Estate Regularity Authority");

        developer_name.add("Emaar India");
        developer_name.add("TDi Infra");

        govt_notify_list.add("Notice u/s 59 for appearance in respect of project and not...");
        govt_notify_list.add("Notice u/s 59 for appearance in respect of project and not...");
        govt_notify_list.add("Notice u/s 59 for appearance in respect of project and not...");

        /*imageAdapter = new ImageAdapter(this, array_image_videos);
        viewPager.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();*/

        /*image_video_adapter = new Image_Video_Adapter(this, array_image_videos, this);
        list_images_videos.setAdapter(image_video_adapter);
        list_images_videos.setLayoutManager(new LinearLayoutManager(this,  LinearLayout.HORIZONTAL, false));
        image_video_adapter.notifyDataSetChanged();*/

        /*developer_list_adapter = new Developer_list(this, developer_name);
        developer_list.setAdapter(developer_list_adapter);
        developer_list.setLayoutManager(new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false));
        developer_list_adapter.notifyDataSetChanged();*/

        /*govt_dept_adapter = new Govt_Dept_Adapter(this, govt_detail_list);
        govt_dept_rep.setAdapter(govt_dept_adapter);
        govt_dept_rep.setLayoutManager(new LinearLayoutManager(this));
        govt_dept_adapter.notifyDataSetChanged();*/

        govt_notification_adapter = new Govt_Notification_Adapter(this, govt_notify_list);
        govt_notification_rep.setAdapter(govt_notification_adapter);
        govt_notification_rep.setLayoutManager(new LinearLayoutManager(this));
        govt_notification_adapter.notifyDataSetChanged();

        govt_notification_adapter = new Govt_Notification_Adapter(this, govt_notify_list);
        latest_update_rep.setAdapter(govt_notification_adapter);
        latest_update_rep.setLayoutManager(new LinearLayoutManager(this));
        govt_notification_adapter.notifyDataSetChanged();

        /*stateImageAdapter = new StateImageAdapter(this, mohali_videos);
        gmada_videos.setAdapter(stateImageAdapter);
        gmada_videos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        stateImageAdapter.notifyDataSetChanged();

        stateImageAdapter = new StateImageAdapter(this, mohali_videos);
        gmada_photos.setAdapter(stateImageAdapter);
        gmada_photos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        stateImageAdapter.notifyDataSetChanged();*/

        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(getItem(-1), false);

            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(getItem(+1), false);

            }
        });

    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void setViewPager(int position) {
        viewPager.setCurrentItem(position, false);

    }
}
