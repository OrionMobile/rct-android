package com.bnhabitat.areaModule.areaActivities;

public interface ViewPagerListener {
    void setViewPager(int position);
}
