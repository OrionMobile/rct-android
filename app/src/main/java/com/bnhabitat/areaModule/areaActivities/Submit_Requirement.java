package com.bnhabitat.areaModule.areaActivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;

public class Submit_Requirement extends AppCompatActivity {

    ImageView imgBack;
    TextView done;

    ImageView residential_drop, residential_up, commercial_drop, commercial_up, forSell_residential_drop, forSell_residential_up, forSell_drop, forSell_up;
    LinearLayout lnrOpenNav, residential_lay, forSell_lay_res, commercial_lay, forSell_lay;
    static int flagBedroom_res = 0;
    String strBedroom_res;
    static int flagBath_res = 0;
    String strBath_res;
    static int flagKitchen_res = 0;
    String strKitchen_res;

    TextView txtMinValue_res, txtMaxValue_res, txtMinValueLotSizeSelected_res, txtMaxValueLotSizeSelected_res;
    TextView txtMinValue_com, txtMaxValue_com, txtMinValueLotSizeSelected_com, txtMaxValueLotSizeSelected_com;

    Number minCost=0, maxCost=1000000;
    Number minLot_res=500, maxLot_res=80000;

    static String strMin = "10000";
    static String strMax = "10000000";

    static String strMin_lot_res = "500";
    static String strMax_lot_res = "80000";

    CrystalRangeSeekbar rangeSeekbar_res, LotSize_res;
    CrystalRangeSeekbar rangeSeekbar_com, LotSize_com;

    Number minCost_com=0, maxCost_com=1000000;
    Number minLot_com=500, maxLot_com=80000;

    static String strMin_com = "10000";
    static String strMax_com = "10000000";

    static String strMin_lot_com = "500";
    static String strMax_lot_com = "80000";

    static int flagCabin = 0;
    String strCabin;
    static int flagBathroom = 0;
    String strBathroom;
    static int flagPentry = 0;
    String strPentry;

    Button decBedroom_res, incBedroom_res, decBath_res, incBath_res, decKitchen_res, incKitchen_res, decCabin, incCabin, decBathroom, incBathroom, decPentry, incPentry;
    TextView bedroom_text_res, bath_text_res, kitchen_text_res,cabin_text, bathroom_text, pentry_text;

    SwitchCompat switchButton1, switchButton2, switchButton3, switchButton4, switchButton5, switchforsell_residential, switchforsell;

    Button buy_res, rent_res, pg_res, any_res, freehold_res, lease_res;
    Button buy_com, id_lease_com, lease_com, any_com, freehold_com, lease_owner_com;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit__requirement);

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        residential_lay = (LinearLayout) findViewById(R.id.residential_lay);
        residential_drop = (ImageView) findViewById(R.id.residential_drop);
        residential_up = (ImageView) findViewById(R.id.residential_up);

        switchButton1 = (SwitchCompat) findViewById(R.id.switchButton1);
        switchButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    residential_drop.setVisibility(View.GONE);
                    residential_up.setVisibility(View.VISIBLE);
                    residential_lay.setVisibility(View.VISIBLE);
                }
                else{
                    residential_drop.setVisibility(View.VISIBLE);
                    residential_up.setVisibility(View.GONE);
                    residential_lay.setVisibility(View.GONE);
                }
            }
        });

        forSell_lay_res = (LinearLayout) findViewById(R.id.forSell_lay_residential);
        forSell_residential_drop = (ImageView) findViewById(R.id.forSell_residential_drop);
        forSell_residential_up = (ImageView) findViewById(R.id.forSell_residential_up);

        switchforsell_residential = (SwitchCompat) findViewById(R.id.switchforsell_residential);
        switchforsell_residential.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    forSell_residential_drop.setVisibility(View.GONE);
                    forSell_residential_up.setVisibility(View.VISIBLE);
                    forSell_lay_res.setVisibility(View.VISIBLE);
                }
                else{
                    forSell_residential_drop.setVisibility(View.VISIBLE);
                    forSell_residential_up.setVisibility(View.GONE);
                    forSell_lay_res.setVisibility(View.GONE);
                }
            }
        });

        decBedroom_res = (Button) findViewById(R.id.decBedroom_res);
        incBedroom_res = (Button) findViewById(R.id.incBedroom_res);
        bedroom_text_res = (TextView) findViewById(R.id.bedroom_text_res);

        decBedroom_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBedroom_res > 0)
                    flagBedroom_res--;
                strBedroom_res = "TotalRooms%20eq%20" + flagBedroom_res + "%20and%20";
                bedroom_text_res.setText(String.valueOf(flagBedroom_res));

            }
        });

        incBedroom_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBedroom_res++;
                strBedroom_res = "TotalRooms%20eq%20" + flagBedroom_res + "%20and%20";
                bedroom_text_res.setText(String.valueOf(flagBedroom_res));
            }
        });

        decBath_res = (Button) findViewById(R.id.decBath_res);
        incBath_res = (Button) findViewById(R.id.incBath_res);
        bath_text_res = (TextView) findViewById(R.id.bath_text_res);

        decBath_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBath_res > 0)
                    flagBath_res--;
                strBath_res = "TotalRooms%20eq%20" + flagBath_res + "%20and%20";
                bath_text_res.setText(String.valueOf(flagBath_res));

            }
        });

        incBath_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBath_res++;
                strBath_res = "TotalRooms%20eq%20" + flagBath_res + "%20and%20";
                bath_text_res.setText(String.valueOf(flagBath_res));
            }
        });

        decKitchen_res = (Button) findViewById(R.id.decKitchen_res);
        incKitchen_res = (Button) findViewById(R.id.incKitchen_res);
        kitchen_text_res = (TextView) findViewById(R.id.kitchen_text_res);

        decKitchen_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagKitchen_res > 0)
                    flagKitchen_res--;
                strKitchen_res = "TotalRooms%20eq%20" + flagKitchen_res + "%20and%20";
                kitchen_text_res.setText(String.valueOf(flagKitchen_res));

            }
        });

        incKitchen_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagKitchen_res++;
                strKitchen_res = "TotalRooms%20eq%20" + flagKitchen_res + "%20and%20";
                kitchen_text_res.setText(String.valueOf(flagKitchen_res));
            }
        });

        txtMinValue_res = (TextView) findViewById(R.id.txtMinValue_res);
        txtMaxValue_res = (TextView) findViewById(R.id.txtMaxValue_res);

        rangeSeekbar_res = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar_res);

        rangeSeekbar_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin = String.valueOf(minValue);
                strMax = String.valueOf(maxValue);

                txtMinValue_res.setText("Min $ " + String.valueOf(minValue));
                txtMaxValue_res.setText("Max $ " + String.valueOf(maxValue));

                minCost=minValue;
                maxCost=maxValue;

                Log.e("TAG", "minCost: "+minCost+ " maxCost: "+maxCost );

            }
        });

        txtMinValueLotSizeSelected_res = (TextView) findViewById(R.id.txtMinValueLotSizeSelected_res);
        txtMaxValueLotSizeSelected_res = (TextView) findViewById(R.id.txtMaxValueLotSizeSelected_res);

        LotSize_res = (CrystalRangeSeekbar) findViewById(R.id.LotSize_res);

        LotSize_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_lot_res = String.valueOf(minValue);
                strMax_lot_res = String.valueOf(maxValue);

                txtMinValueLotSizeSelected_res.setText("Min $ " + String.valueOf(minValue));
                txtMaxValueLotSizeSelected_res.setText("Max $ " + String.valueOf(maxValue));

                minLot_res=minValue;
                maxLot_res=maxValue;

                Log.e("TAG", "minCost: "+minLot_res+ " maxCost: "+maxLot_res );

            }
        });

        buy_res = (Button) findViewById(R.id.buy_res);
        rent_res = (Button) findViewById(R.id.rent_res);
        pg_res = (Button) findViewById(R.id.pg_res);

        buy_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buy_res.setBackgroundResource(R.drawable.left_button_selected);
                buy_res.setTextColor(getResources().getColor(R.color.white));
                rent_res.setSelected(false);
                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
                pg_res.setSelected(false);
                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        rent_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rent_res.setBackgroundResource(R.drawable.center_button_selected);
                rent_res.setTextColor(getResources().getColor(R.color.white));
                buy_res.setSelected(false);
                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
                pg_res.setSelected(false);
                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        pg_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pg_res.setBackgroundResource(R.drawable.right_button_selected);
                pg_res.setTextColor(getResources().getColor(R.color.white));
                buy_res.setSelected(false);
                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
                rent_res.setSelected(false);
                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        any_res = (Button) findViewById(R.id.any_res);
        freehold_res = (Button) findViewById(R.id.freehold_res);
        lease_res = (Button) findViewById(R.id.lease_res);

        any_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                any_res.setBackgroundResource(R.drawable.left_button_selected);
                any_res.setTextColor(getResources().getColor(R.color.white));
                freehold_res.setSelected(false);
                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
                lease_res.setSelected(false);
                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        freehold_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freehold_res.setBackgroundResource(R.drawable.center_button_selected);
                freehold_res.setTextColor(getResources().getColor(R.color.white));
                any_res.setSelected(false);
                any_res.setBackgroundResource(R.drawable.left_button_deselected);
                any_res.setTextColor(getResources().getColor(R.color.app_blue));
                lease_res.setSelected(false);
                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_res.setBackgroundResource(R.drawable.right_button_selected);
                lease_res.setTextColor(getResources().getColor(R.color.white));
                any_res.setSelected(false);
                any_res.setBackgroundResource(R.drawable.left_button_deselected);
                any_res.setTextColor(getResources().getColor(R.color.app_blue));
                freehold_res.setSelected(false);
                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        commercial_lay = (LinearLayout) findViewById(R.id.commercial_lay);
        commercial_drop = (ImageView) findViewById(R.id.commercial_drop);
        commercial_up = (ImageView) findViewById(R.id.commercial_up);

        switchButton2 = (SwitchCompat) findViewById(R.id.switchButton2);
        switchButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    commercial_drop.setVisibility(View.GONE);
                    commercial_up.setVisibility(View.VISIBLE);
                    commercial_lay.setVisibility(View.VISIBLE);
                }
                else{
                    commercial_drop.setVisibility(View.VISIBLE);
                    commercial_up.setVisibility(View.GONE);
                    commercial_lay.setVisibility(View.GONE);
                }
            }
        });

        forSell_lay = (LinearLayout) findViewById(R.id.forSell_lay);
        forSell_drop = (ImageView) findViewById(R.id.forSell_drop);
        forSell_up = (ImageView) findViewById(R.id.forSell_up);

        switchforsell = (SwitchCompat) findViewById(R.id.switchforsell);
        switchforsell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    forSell_drop.setVisibility(View.GONE);
                    forSell_up.setVisibility(View.VISIBLE);
                    forSell_lay.setVisibility(View.VISIBLE);
                }
                else{
                    forSell_drop.setVisibility(View.VISIBLE);
                    forSell_up.setVisibility(View.GONE);
                    forSell_lay.setVisibility(View.GONE);
                }
            }
        });

        decCabin = (Button) findViewById(R.id.decCabin);
        incCabin = (Button) findViewById(R.id.incCabin);
        cabin_text = (TextView) findViewById(R.id.cabin_text);

        decCabin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagCabin > 0)
                    flagCabin--;
                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
                cabin_text.setText(String.valueOf(flagCabin));

            }
        });

        incCabin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagCabin++;
                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
                cabin_text.setText(String.valueOf(flagCabin));
            }
        });

        decBathroom = (Button) findViewById(R.id.decBathroom);
        incBathroom = (Button) findViewById(R.id.incBathroom);
        bathroom_text = (TextView) findViewById(R.id.bathroom_text);

        decBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBathroom > 0)
                    flagBathroom--;
                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
                bathroom_text.setText(String.valueOf(flagBathroom));

            }
        });

        incBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBathroom++;
                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
                bathroom_text.setText(String.valueOf(flagBathroom));
            }
        });

        decPentry = (Button) findViewById(R.id.decPentry);
        incPentry = (Button) findViewById(R.id.incPentry);
        pentry_text = (TextView) findViewById(R.id.pentry_text);

        decPentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagPentry > 0)
                    flagPentry--;
                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
                pentry_text.setText(String.valueOf(flagPentry));

            }
        });

        incPentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagPentry++;
                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
                pentry_text.setText(String.valueOf(flagPentry));
            }
        });

        txtMinValue_com = (TextView) findViewById(R.id.txtMinValue_com);
        txtMaxValue_com = (TextView) findViewById(R.id.txtMaxValue_com);

        rangeSeekbar_com = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar_com);

        rangeSeekbar_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_com = String.valueOf(minValue);
                strMax_com = String.valueOf(maxValue);

                txtMinValue_com.setText("Min $ " + String.valueOf(minValue));
                txtMaxValue_com.setText("Max $ " + String.valueOf(maxValue));

                minCost_com=minValue;
                maxCost_com=maxValue;

                Log.e("TAG", "minCost: "+minCost_com+ " maxCost: "+maxCost_com );

            }
        });

        txtMinValueLotSizeSelected_com = (TextView) findViewById(R.id.txtMinValueLotSizeSelected_com);
        txtMaxValueLotSizeSelected_com = (TextView) findViewById(R.id.txtMaxValueLotSizeSelected_com);

        LotSize_com = (CrystalRangeSeekbar) findViewById(R.id.LotSize_com);

        LotSize_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_lot_com = String.valueOf(minValue);
                strMax_lot_com = String.valueOf(maxValue);

                txtMinValueLotSizeSelected_com.setText("Min $ " + String.valueOf(minValue));
                txtMaxValueLotSizeSelected_com.setText("Max $ " + String.valueOf(maxValue));

                minLot_com=minValue;
                maxLot_com=maxValue;

                Log.e("TAG", "minCost: "+minLot_com+ " maxCost: "+maxLot_com );

            }
        });

        buy_com = (Button) findViewById(R.id.buy_com);
        id_lease_com = (Button) findViewById(R.id.id_lease_com);
        lease_com = (Button) findViewById(R.id.lease_com);

        buy_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buy_com.setBackgroundResource(R.drawable.left_button_selected);
                buy_com.setTextColor(getResources().getColor(R.color.white));
                id_lease_com.setSelected(false);
                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_com.setSelected(false);
                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        id_lease_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id_lease_com.setBackgroundResource(R.drawable.center_button_selected);
                id_lease_com.setTextColor(getResources().getColor(R.color.white));
                buy_com.setSelected(false);
                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_com.setSelected(false);
                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_com.setBackgroundResource(R.drawable.right_button_selected);
                lease_com.setTextColor(getResources().getColor(R.color.white));
                buy_com.setSelected(false);
                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
                id_lease_com.setSelected(false);
                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });
        any_com = (Button) findViewById(R.id.any_com);
        freehold_com = (Button) findViewById(R.id.freehold_com);
        lease_owner_com = (Button) findViewById(R.id.lease_owner_com);

        any_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                any_com.setBackgroundResource(R.drawable.left_button_selected);
                any_com.setTextColor(getResources().getColor(R.color.white));
                freehold_com.setSelected(false);
                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_owner_com.setSelected(false);
                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        freehold_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freehold_com.setBackgroundResource(R.drawable.center_button_selected);
                freehold_com.setTextColor(getResources().getColor(R.color.white));
                any_com.setSelected(false);
                any_com.setBackgroundResource(R.drawable.left_button_deselected);
                any_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_owner_com.setSelected(false);
                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_owner_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_owner_com.setBackgroundResource(R.drawable.right_button_selected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.white));
                any_com.setSelected(false);
                any_com.setBackgroundResource(R.drawable.left_button_deselected);
                any_com.setTextColor(getResources().getColor(R.color.app_blue));
                freehold_com.setSelected(false);
                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });
    }
}
