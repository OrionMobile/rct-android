package com.bnhabitat.areaModule.areaActivities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.FeaturesAdapter;
import com.bnhabitat.areaModule.adapter.Govt_Notification_Adapter;
import com.bnhabitat.areaModule.adapter.ImageAdapter;
import com.bnhabitat.areaModule.adapter.Image_Video_Adapter;
import com.bnhabitat.areaModule.adapter.ProjectListingAdapter;
import com.bnhabitat.areaModule.adapter.StateImageAdapter;
import com.bnhabitat.areaModule.model.KeydistanceModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.township.townactivities.AddSectorLocality;
import com.bnhabitat.township.townactivities.Add_Project;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VillageDetail extends AppCompatActivity implements ViewPager.OnPageChangeListener, ViewPagerListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener, View.OnClickListener {

    RecyclerView project_list, list_images_videos, govt_notification_rep, latest_update_rep, mohali_videos, mohali_photos, features;
    private View actionB,action_A, action_c, action_d, action_e, action_f, action_g;
    ArrayList<Integer> array_image_videos = new ArrayList<Integer>();
    ArrayList<String> projectListingArrayList = new ArrayList<>();
    ArrayList<String> govt_notify_list = new ArrayList<>();
    ArrayList<Integer> mohali_videos_list = new ArrayList<Integer>();

    ArrayList<KeydistanceModel> featureData = new ArrayList<>();

    ViewPager viewPager;

    ImageAdapter imageAdapter;
    Image_Video_Adapter image_video_adapter;
    Govt_Notification_Adapter govt_notification_adapter;
    StateImageAdapter stateImageAdapter;
    FeaturesAdapter featuresAdapter;

    ImageView  imgBack, imgFilter, imgShare, iv_location, imgPrevious, imgNext, residential_drop, residential_up, commercial_drop, commercial_up, forSell_residential_drop, forSell_residential_up, forSell_drop, forSell_up;
    LinearLayout residential_lay, forSell_lay_res, commercial_lay, forSell_lay;
    static int flagCabin_res = 0;
    String strCabin_res;
    static int flagBathroom_res = 0;
    String strBathroom_res;
    static int flagPentry_res = 0;
    String strPentry_res;

    TextView txtMinValue_res, txtMaxValue_res, txtMinValueSquareFeetSelected_res, txtMaxValueSquareFeetSelected_res, txtMinValueLotSizeSelected_res, txtMaxValueLotSizeSelected_res;
    TextView txtMinValue_com, txtMaxValue_com, txtMinValueSquareFeetSelected_com, txtMaxValueSquareFeetSelected_com, txtMinValueLotSizeSelected_com, txtMaxValueLotSizeSelected_com;

    Number minCost=0, maxCost=1000000;
    Number minCost_sfs=500, maxCost_sfs=80000;
    Number minLot_res=500, maxLot_res=80000;

    static String strMin = "10000";
    static String strMax = "10000000";

    static String strMin_sfs = "500";
    static String strMax_sfs = "80000";

    static String strMin_lot_res = "500";
    static String strMax_lot_res = "80000";

    CrystalRangeSeekbar rangeSeekbar_res, squareFeetRate_res, LotSize_res;
    CrystalRangeSeekbar rangeSeekbar_com, squareFeetRate_com, LotSize_com;

    Number minCost_com=0, maxCost_com=1000000;
    Number minCost_sfs_com=500, maxCost_sfs_com=80000;
    Number minLot_com=500, maxLot_com=80000;

    static String strMin_com = "10000";
    static String strMax_com = "10000000";

    static String strMin_sfs_com = "500";
    static String strMax_sfs_com = "80000";

    static String strMin_lot_com = "500";
    static String strMax_lot_com = "80000";

    static int flagCabin = 0;
    String strCabin;
    static int flagBathroom = 0;
    String strBathroom;
    static int flagPentry = 0;
    String strPentry;

    Button decCabin_res, incCabin_res, decBathroom_res, incBathroom_res, decPentry_res, incPentry_res, decCabin, incCabin, decBathroom, incBathroom, decPentry, incPentry;
    TextView name_city, cabin_text_res, bathroom_text_res, pentry_text_res,cabin_text, bathroom_text, pentry_text;

    SwitchCompat switchButton1, switchButton2, switchButton3, switchButton4, switchButton5, switchforsell_residential, switchforsell;

    Button buy_res, rent_res, pg_res, any_res, freehold_res, lease_res;
    Button buy_com, id_lease_com, lease_com, any_com, freehold_com, lease_owner_com;

    LinearLayout submit_requirement_lay;
    String Id, name;

    TextView VillageName, stateName, districtName, totalArea, totalAreaUnits, villageName, villageType, policeStation, postOffice, population, aboutVillage, lineCount, abt_village;
    TextView agri_area, agri_area_unit, shamlat_area, shamlat_area_unit, forest_area, forest_area_unit, kandi_area, kandi_area_unit;

    ArrayList<StateListModel.Pictures> pictures1 = new ArrayList<>();
    ArrayList<StateListModel.Pictures> pictures = new ArrayList<>();

    DrawerLayout drawer_layout;
    Dialog dialog, dialog1;
    ImageView userPic, msg, call;
    TextView first_last_name, professional_txt, submitReq, propertSell_Rent;
    String contact = "", email = "";
    private static final int REQUEST_PHONE_CALL = 198;
    ImageView close, img, close1, image;
    TextView project_name, project_name1, desc, description;
    RadioButton yes, no, buy, rent, yes1, no1, sell, rent1;
    LinearLayout property_detailing, property_detailing1;
    Spinner category_spinner, property_spinner, category_spinner1, property_spinner1;
    CheckBox check_studio, check_2bhk, check_3bhk, check_4bhk;
    EditText message, datePicker, media, tower_name_no, unit_no, message1, datePick, media1;
    Button submit, submit1;
    TextView no_data, no_data1;
    RelativeLayout backward1, forward1;
    ImageView left1, right1;
    String name_number;
    FloatingActionsMenu floatingActionsMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_village_detail);

        /*action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(VillageDetail.this, Add_Project.class);
                startActivity(intent);

            }
        });
        action_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(VillageDetail.this, Add_township.class);
                startActivity(intent);

            }
        });
        action_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(VillageDetail.this, AddSectorLocality.class);
                startActivity(intent);

            }
        });
        action_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        action_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        action_g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(VillageDetail.this, Submit_Requirement.class);
                startActivity(intent);

            }
        });



        switchButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    residential_drop.setVisibility(View.GONE);
                    residential_up.setVisibility(View.VISIBLE);
                    residential_lay.setVisibility(View.VISIBLE);
                }
                else{
                    residential_drop.setVisibility(View.VISIBLE);
                    residential_up.setVisibility(View.GONE);
                    residential_lay.setVisibility(View.GONE);
                }
            }
        });


        switchforsell_residential.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    forSell_residential_drop.setVisibility(View.GONE);
                    forSell_residential_up.setVisibility(View.VISIBLE);
                    forSell_lay_res.setVisibility(View.VISIBLE);
                }
                else{
                    forSell_residential_drop.setVisibility(View.VISIBLE);
                    forSell_residential_up.setVisibility(View.GONE);
                    forSell_lay_res.setVisibility(View.GONE);
                }
            }
        });



        decCabin_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagCabin_res > 0)
                    flagCabin_res--;
                strCabin_res = "TotalRooms%20eq%20" + flagCabin_res + "%20and%20";
                cabin_text_res.setText(String.valueOf(flagCabin_res));

            }
        });

        incCabin_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagCabin_res++;
                strCabin_res = "TotalRooms%20eq%20" + flagCabin_res + "%20and%20";
                cabin_text_res.setText(String.valueOf(flagCabin_res));
            }
        });

        decBathroom_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBathroom_res > 0)
                    flagBathroom_res--;
                strBathroom_res = "TotalRooms%20eq%20" + flagBathroom_res + "%20and%20";
                bathroom_text_res.setText(String.valueOf(flagBathroom_res));

            }
        });

        incBathroom_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBathroom_res++;
                strBathroom_res = "TotalRooms%20eq%20" + flagBathroom_res + "%20and%20";
                bathroom_text_res.setText(String.valueOf(flagBathroom_res));
            }
        });



        decPentry_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagPentry_res > 0)
                    flagPentry_res--;
                strPentry_res = "TotalRooms%20eq%20" + flagPentry_res + "%20and%20";
                pentry_text_res.setText(String.valueOf(flagPentry_res));

            }
        });

        incPentry_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagPentry_res++;
                strPentry_res = "TotalRooms%20eq%20" + flagPentry_res + "%20and%20";
                pentry_text_res.setText(String.valueOf(flagPentry_res));
            }
        });

        rangeSeekbar_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin = String.valueOf(minValue);
                strMax = String.valueOf(maxValue);

                txtMinValue_res.setText("Min $ " + String.valueOf(minValue));
                txtMaxValue_res.setText("Max $ " + String.valueOf(maxValue));

                minCost=minValue;
                maxCost=maxValue;

                Log.e("TAG", "minCost: "+minCost+ " maxCost: "+maxCost );

            }
        });

        squareFeetRate_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_sfs = String.valueOf(minValue);
                strMax_sfs = String.valueOf(maxValue);

                txtMinValueSquareFeetSelected_res.setText("Min $ " + String.valueOf(minValue));
                txtMaxValueSquareFeetSelected_res.setText("Max $ " + String.valueOf(maxValue));

                minCost_sfs=minValue;
                maxCost_sfs=maxValue;

                Log.e("TAG", "minCost: "+minCost_sfs+ " maxCost: "+maxCost_sfs );

            }
        });


        LotSize_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_lot_res = String.valueOf(minValue);
                strMax_lot_res = String.valueOf(maxValue);

                txtMinValueLotSizeSelected_res.setText("Min $ " + String.valueOf(minValue));
                txtMaxValueLotSizeSelected_res.setText("Max $ " + String.valueOf(maxValue));

                minLot_res=minValue;
                maxLot_res=maxValue;

                Log.e("TAG", "minCost: "+minLot_res+ " maxCost: "+maxLot_res );

            }
        });

        buy_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buy_res.setBackgroundResource(R.drawable.left_button_selected);
                buy_res.setTextColor(getResources().getColor(R.color.white));
                rent_res.setSelected(false);
                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
                pg_res.setSelected(false);
                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        rent_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rent_res.setBackgroundResource(R.drawable.center_button_selected);
                rent_res.setTextColor(getResources().getColor(R.color.white));
                buy_res.setSelected(false);
                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
                pg_res.setSelected(false);
                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        pg_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pg_res.setBackgroundResource(R.drawable.right_button_selected);
                pg_res.setTextColor(getResources().getColor(R.color.white));
                buy_res.setSelected(false);
                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
                rent_res.setSelected(false);
                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        any_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                any_res.setBackgroundResource(R.drawable.left_button_selected);
                any_res.setTextColor(getResources().getColor(R.color.white));
                freehold_res.setSelected(false);
                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
                lease_res.setSelected(false);
                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        freehold_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freehold_res.setBackgroundResource(R.drawable.center_button_selected);
                freehold_res.setTextColor(getResources().getColor(R.color.white));
                any_res.setSelected(false);
                any_res.setBackgroundResource(R.drawable.left_button_deselected);
                any_res.setTextColor(getResources().getColor(R.color.app_blue));
                lease_res.setSelected(false);
                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_res.setBackgroundResource(R.drawable.right_button_selected);
                lease_res.setTextColor(getResources().getColor(R.color.white));
                any_res.setSelected(false);
                any_res.setBackgroundResource(R.drawable.left_button_deselected);
                any_res.setTextColor(getResources().getColor(R.color.app_blue));
                freehold_res.setSelected(false);
                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        switchButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    commercial_drop.setVisibility(View.GONE);
                    commercial_up.setVisibility(View.VISIBLE);
                    commercial_lay.setVisibility(View.VISIBLE);
                }
                else{
                    commercial_drop.setVisibility(View.VISIBLE);
                    commercial_up.setVisibility(View.GONE);
                    commercial_lay.setVisibility(View.GONE);
                }
            }
        });

        switchforsell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    forSell_drop.setVisibility(View.GONE);
                    forSell_up.setVisibility(View.VISIBLE);
                    forSell_lay.setVisibility(View.VISIBLE);
                }
                else{
                    forSell_drop.setVisibility(View.VISIBLE);
                    forSell_up.setVisibility(View.GONE);
                    forSell_lay.setVisibility(View.GONE);
                }
            }
        });

        decCabin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagCabin > 0)
                    flagCabin--;
                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
                cabin_text.setText(String.valueOf(flagCabin));

            }
        });

        incCabin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagCabin++;
                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
                cabin_text.setText(String.valueOf(flagCabin));
            }
        });

        decBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBathroom > 0)
                    flagBathroom--;
                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
                bathroom_text.setText(String.valueOf(flagBathroom));

            }
        });

        incBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBathroom++;
                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
                bathroom_text.setText(String.valueOf(flagBathroom));
            }
        });

        decPentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagPentry > 0)
                    flagPentry--;
                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
                pentry_text.setText(String.valueOf(flagPentry));

            }
        });

        incPentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagPentry++;
                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
                pentry_text.setText(String.valueOf(flagPentry));
            }
        });

        rangeSeekbar_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_com = String.valueOf(minValue);
                strMax_com = String.valueOf(maxValue);

                txtMinValue_com.setText("Min $ " + String.valueOf(minValue));
                txtMaxValue_com.setText("Max $ " + String.valueOf(maxValue));

                minCost_com=minValue;
                maxCost_com=maxValue;

                Log.e("TAG", "minCost: "+minCost_com+ " maxCost: "+maxCost_com );

            }
        });

        squareFeetRate_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_sfs_com = String.valueOf(minValue);
                strMax_sfs_com = String.valueOf(maxValue);

                txtMinValueSquareFeetSelected_com.setText("Min $ " + String.valueOf(minValue));
                txtMaxValueSquareFeetSelected_com.setText("Max $ " + String.valueOf(maxValue));

                minCost_sfs_com=minValue;
                maxCost_sfs_com=maxValue;

                Log.e("TAG", "minCost: "+minCost_sfs_com+ " maxCost: "+maxCost_sfs_com );

            }
        });

        LotSize_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_lot_com = String.valueOf(minValue);
                strMax_lot_com = String.valueOf(maxValue);

                txtMinValueLotSizeSelected_com.setText("Min $ " + String.valueOf(minValue));
                txtMaxValueLotSizeSelected_com.setText("Max $ " + String.valueOf(maxValue));

                minLot_com=minValue;
                maxLot_com=maxValue;

                Log.e("TAG", "minCost: "+minLot_com+ " maxCost: "+maxLot_com );

            }
        });

        buy_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buy_com.setBackgroundResource(R.drawable.left_button_selected);
                buy_com.setTextColor(getResources().getColor(R.color.white));
                id_lease_com.setSelected(false);
                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_com.setSelected(false);
                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        id_lease_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id_lease_com.setBackgroundResource(R.drawable.center_button_selected);
                id_lease_com.setTextColor(getResources().getColor(R.color.white));
                buy_com.setSelected(false);
                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_com.setSelected(false);
                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_com.setBackgroundResource(R.drawable.right_button_selected);
                lease_com.setTextColor(getResources().getColor(R.color.white));
                buy_com.setSelected(false);
                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
                id_lease_com.setSelected(false);
                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        any_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                any_com.setBackgroundResource(R.drawable.left_button_selected);
                any_com.setTextColor(getResources().getColor(R.color.white));
                freehold_com.setSelected(false);
                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_owner_com.setSelected(false);
                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        freehold_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freehold_com.setBackgroundResource(R.drawable.center_button_selected);
                freehold_com.setTextColor(getResources().getColor(R.color.white));
                any_com.setSelected(false);
                any_com.setBackgroundResource(R.drawable.left_button_deselected);
                any_com.setTextColor(getResources().getColor(R.color.app_blue));
                lease_owner_com.setSelected(false);
                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        lease_owner_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lease_owner_com.setBackgroundResource(R.drawable.right_button_selected);
                lease_owner_com.setTextColor(getResources().getColor(R.color.white));
                any_com.setSelected(false);
                any_com.setBackgroundResource(R.drawable.left_button_deselected);
                any_com.setTextColor(getResources().getColor(R.color.app_blue));
                freehold_com.setSelected(false);
                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
            }
        });

        submit_requirement_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VillageDetail.this, Submit_Requirement.class));
            }
        });*/


        /*imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });*/

        //onVillageDetail();

        /*if(PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("") || PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("null"))
            name_number = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        else
            name_number = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "");
        first_last_name.setText(name_number);
        contact = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        email = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.EMAIL, "");

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (ContextCompat.checkSelfPermission(VillageDetail.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(VillageDetail.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                } else {
                    callIntent.setData(Uri.parse("tel:" + contact));
                    startActivity(callIntent);
                }
            }
        });

        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
                startActivity(Intent.createChooser(intent, "Choose"));
            }
        });

        submitReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitReqPopUp(pictures.get(0).getUrl());
            }
        });

        propertSell_Rent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                propertySell_RentPopUp(pictures.get(0).getUrl());
            }
        });*/

        /*array_image_videos.add(R.drawable.mohali);
        array_image_videos.add(R.drawable.fortis);
        array_image_videos.add(R.drawable.gillco_valley);
        array_image_videos.add(R.drawable.connaught_residency);
        array_image_videos.add(R.drawable.iiser);

        mohali_videos_list.add(R.drawable.mohali);
        mohali_videos_list.add(R.drawable.fortis);

        projectListingArrayList.add("DFL HYDE Park");
        projectListingArrayList.add("Omaxe The Lake Apartment");
        projectListingArrayList.add("GMADA");
        projectListingArrayList.add("TDI City-I Mohali");

        govt_notify_list.add("Notice u/s 59 for appearance in respect of project and not...");
        govt_notify_list.add("Notice u/s 59 for appearance in respect of project and not...");
        govt_notify_list.add("Notice u/s 59 for appearance in respect of project and not...");
*/
        /*projectListingAdapter = new ProjectListingAdapter(this, projectListingArrayList);
        project_list.setLayoutManager(new LinearLayoutManager(this));
        project_list.setAdapter(projectListingAdapter);
        projectListingAdapter.notifyDataSetChanged();*/

        /*govt_notification_adapter = new Govt_Notification_Adapter(this, govt_notify_list);
        govt_notification_rep.setAdapter(govt_notification_adapter);
        govt_notification_rep.setLayoutManager(new LinearLayoutManager(this));
        govt_notification_adapter.notifyDataSetChanged();

        govt_notification_adapter = new Govt_Notification_Adapter(this, govt_notify_list);
        latest_update_rep.setAdapter(govt_notification_adapter);
        latest_update_rep.setLayoutManager(new LinearLayoutManager(this));
        govt_notification_adapter.notifyDataSetChanged();*/

        /*imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItemBack(viewPager.getCurrentItem())==0)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else if(getItemBack(viewPager.getCurrentItem())==pictures.size()-1)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else
                    viewPager.setCurrentItem(getItemBack(-1), false);

            }
        });*/

        /*imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItem(+1) == pictures.size())
                    viewPager.setCurrentItem(getItem(0), true);
                else
                    viewPager.setCurrentItem(getItem(+1), false);

            }
        });*/

        /*if(pictures1.size()>2){
            backward1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mohali_photos.smoothScrollToPosition(getItem() - 1);
                    right1.setColorFilter(ContextCompat.getColor(VillageDetail.this, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                }
            });

            forward1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mohali_photos.smoothScrollToPosition(getId() + 1);
                    left1.setColorFilter(ContextCompat.getColor(VillageDetail.this, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                }
            });
        }else{
            right1.setColorFilter(ContextCompat.getColor(VillageDetail.this, R.color.divider_light_grey), android.graphics.PorterDuff.Mode.MULTIPLY);
            left1.setColorFilter(ContextCompat.getColor(VillageDetail.this, R.color.divider_light_grey), android.graphics.PorterDuff.Mode.MULTIPLY);
        }*/


    }

    @Override
    protected void onStart() {
        initialization();
        Intent intent = getIntent();
        Id = intent.getStringExtra("VillageId");
        name = intent.getStringExtra("VillageName");
        VillageName.setText(name);
        abt_village.setText("About " + name);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.transparent));
            }

            @SuppressLint("ResourceType")
            @Override
            public void onMenuCollapsed() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.tran));

            }
        });
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawer_layout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

                hideSoftKeyboard(VillageDetail.this);

            }
        });
        onClicks();

        //onCheckedChangedListeners();

        //onRangeSeekBars();

        //set();

        super.onStart();
    }

    @Override
    protected void onResume() {
        onVillageDetail();
        super.onResume();
    }

    private void onCheckedChangedListeners() {

        switchButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    residential_drop.setVisibility(View.GONE);
                    residential_up.setVisibility(View.VISIBLE);
                    residential_lay.setVisibility(View.VISIBLE);
                }
                else{
                    residential_drop.setVisibility(View.VISIBLE);
                    residential_up.setVisibility(View.GONE);
                    residential_lay.setVisibility(View.GONE);
                }
            }
        });

        switchforsell_residential.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    forSell_residential_drop.setVisibility(View.GONE);
                    forSell_residential_up.setVisibility(View.VISIBLE);
                    forSell_lay_res.setVisibility(View.VISIBLE);
                }
                else{
                    forSell_residential_drop.setVisibility(View.VISIBLE);
                    forSell_residential_up.setVisibility(View.GONE);
                    forSell_lay_res.setVisibility(View.GONE);
                }
            }
        });


        switchButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    commercial_drop.setVisibility(View.GONE);
                    commercial_up.setVisibility(View.VISIBLE);
                    commercial_lay.setVisibility(View.VISIBLE);
                }
                else{
                    commercial_drop.setVisibility(View.VISIBLE);
                    commercial_up.setVisibility(View.GONE);
                    commercial_lay.setVisibility(View.GONE);
                }
            }
        });


        switchforsell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    forSell_drop.setVisibility(View.GONE);
                    forSell_up.setVisibility(View.VISIBLE);
                    forSell_lay.setVisibility(View.VISIBLE);
                }
                else{
                    forSell_drop.setVisibility(View.VISIBLE);
                    forSell_up.setVisibility(View.GONE);
                    forSell_lay.setVisibility(View.GONE);
                }
            }
        });

    }

    private void onRangeSeekBars() {

        rangeSeekbar_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin = String.valueOf(minValue);
                strMax = String.valueOf(maxValue);

                txtMinValue_res.setText("$" + String.valueOf(minValue));
                txtMaxValue_res.setText("$" + String.valueOf(maxValue));

                minCost=minValue;
                maxCost=maxValue;

                Log.e("TAG", "minCost: "+minCost+ " maxCost: "+maxCost );

            }
        });


        squareFeetRate_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_sfs = String.valueOf(minValue);
                strMax_sfs = String.valueOf(maxValue);

                txtMinValueSquareFeetSelected_res.setText("$" + String.valueOf(minValue));
                txtMaxValueSquareFeetSelected_res.setText("$" + String.valueOf(maxValue));

                minCost_sfs=minValue;
                maxCost_sfs=maxValue;

                Log.e("TAG", "minCost: "+minCost_sfs+ " maxCost: "+maxCost_sfs );

            }
        });

        LotSize_res.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_lot_res = String.valueOf(minValue);
                strMax_lot_res = String.valueOf(maxValue);

                txtMinValueLotSizeSelected_res.setText("$" + String.valueOf(minValue));
                txtMaxValueLotSizeSelected_res.setText("$" + String.valueOf(maxValue));

                minLot_res=minValue;
                maxLot_res=maxValue;

                Log.e("TAG", "minCost: "+minLot_res+ " maxCost: "+maxLot_res );

            }
        });

        rangeSeekbar_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_com = String.valueOf(minValue);
                strMax_com = String.valueOf(maxValue);

                txtMinValue_com.setText("$" + String.valueOf(minValue));
                txtMaxValue_com.setText("$" + String.valueOf(maxValue));

                minCost_com=minValue;
                maxCost_com=maxValue;

                Log.e("TAG", "minCost: "+minCost_com+ " maxCost: "+maxCost_com );

            }
        });


        squareFeetRate_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_sfs_com = String.valueOf(minValue);
                strMax_sfs_com = String.valueOf(maxValue);

                txtMinValueSquareFeetSelected_com.setText("$" + String.valueOf(minValue));
                txtMaxValueSquareFeetSelected_com.setText("$" + String.valueOf(maxValue));

                minCost_sfs_com=minValue;
                maxCost_sfs_com=maxValue;

                Log.e("TAG", "minCost: "+minCost_sfs_com+ " maxCost: "+maxCost_sfs_com );

            }
        });


        LotSize_com.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin_lot_com = String.valueOf(minValue);
                strMax_lot_com = String.valueOf(maxValue);

                txtMinValueLotSizeSelected_com.setText("$" + String.valueOf(minValue));
                txtMaxValueLotSizeSelected_com.setText("$" + String.valueOf(maxValue));

                minLot_com=minValue;
                maxLot_com=maxValue;

                Log.e("TAG", "minCost: "+minLot_com+ " maxCost: "+maxLot_com );

            }
        });
    }

    private void initialization() {

        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        VillageName = (TextView) findViewById(R.id.VillageName);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgFilter = (ImageView) findViewById(R.id.imgFilter);
        imgShare = (ImageView) findViewById(R.id.imgShare);
        iv_location = (ImageView) findViewById(R.id.iv_location);
        viewPager = (ViewPager) findViewById(R.id.view_images);
        imgPrevious = (ImageView) findViewById(R.id.imgPrevious);
        imgNext = (ImageView) findViewById(R.id.imgNext);
        list_images_videos = (RecyclerView) findViewById(R.id.list_images_videos);
        project_list = (RecyclerView) findViewById(R.id.project_list);
        govt_notification_rep = (RecyclerView) findViewById(R.id.govt_notification_rep);
        latest_update_rep = (RecyclerView) findViewById(R.id.latest_update_rep);
        mohali_videos = (RecyclerView) findViewById(R.id.mohali_videos);
        mohali_photos = (RecyclerView) findViewById(R.id.mohali_photos);
        features = (RecyclerView) findViewById(R.id.features);

        no_data = (TextView) findViewById(R.id.no_data);
        no_data1 = (TextView) findViewById(R.id.no_data1);
        stateName = (TextView) findViewById(R.id.stateName);
        districtName = (TextView) findViewById(R.id.districtName);
        totalArea = (TextView) findViewById(R.id.totalArea);
        totalAreaUnits = (TextView) findViewById(R.id.totalAreaUnits);
        totalAreaUnits.setVisibility(View.GONE);
        villageName = (TextView) findViewById(R.id.villageName);
        villageType = (TextView) findViewById(R.id.villageType);
        policeStation = (TextView) findViewById(R.id.policeStation);
        postOffice = (TextView) findViewById(R.id.postOffice);
        population = (TextView) findViewById(R.id.population);
        aboutVillage = (TextView) findViewById(R.id.aboutVillage);
        abt_village = (TextView) findViewById(R.id.abt_village);
        lineCount = (TextView) findViewById(R.id.lineCount);
        agri_area = (TextView) findViewById(R.id.agri_area);
        agri_area_unit = (TextView) findViewById(R.id.agri_area_unit);
        shamlat_area = (TextView) findViewById(R.id.shamlat_area);
        shamlat_area_unit = (TextView) findViewById(R.id.shamlat_area_unit);
        forest_area = (TextView) findViewById(R.id.forest_area);
        forest_area_unit = (TextView) findViewById(R.id.forest_area_unit);
        kandi_area = (TextView) findViewById(R.id.kandi_area);
        kandi_area_unit = (TextView) findViewById(R.id.kandi_area_unit);

        userPic = (ImageView) findViewById(R.id.userPic);
        msg = (ImageView) findViewById(R.id.msg);
        call = (ImageView) findViewById(R.id.call);

        first_last_name = (TextView) findViewById(R.id.first_last_name);
        professional_txt = (TextView) findViewById(R.id.professional_txt);
        submitReq = (TextView) findViewById(R.id.submitReq);
        propertSell_Rent = (TextView) findViewById(R.id.propertSell_Rent);

        floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        action_c = findViewById(R.id.action_c);
        action_d = findViewById(R.id.action_d);
        action_e= findViewById(R.id.action_e);
        action_f = findViewById(R.id.action_f);
        action_g = findViewById(R.id.action_g);

//        residential_lay = (LinearLayout) findViewById(R.id.residential_lay);
//        residential_drop = (ImageView) findViewById(R.id.residential_drop);
//        residential_up = (ImageView) findViewById(R.id.residential_up);
//        switchButton1 = (SwitchCompat) findViewById(R.id.switchButton1);
//        forSell_lay_res = (LinearLayout) findViewById(R.id.forSell_lay_residential);
//        forSell_residential_drop = (ImageView) findViewById(R.id.forSell_residential_drop);
//        forSell_residential_up = (ImageView) findViewById(R.id.forSell_residential_up);
//        switchforsell_residential = (SwitchCompat) findViewById(R.id.switchforsell_residential);
//        decCabin_res = (Button) findViewById(R.id.decCabin_res);
//        incCabin_res = (Button) findViewById(R.id.incCabin_res);
//        cabin_text_res = (TextView) findViewById(R.id.cabin_text_res);
//        decBathroom_res = (Button) findViewById(R.id.decBathroom_res);
//        incBathroom_res = (Button) findViewById(R.id.incBathroom_res);
//        bathroom_text_res = (TextView) findViewById(R.id.bathroom_text_res);
//        decPentry_res = (Button) findViewById(R.id.decPentry_res);
//        incPentry_res = (Button) findViewById(R.id.incPentry_res);
//        pentry_text_res = (TextView) findViewById(R.id.pentry_text_res);
//        txtMinValue_res = (TextView) findViewById(R.id.txtMinValue_res);
//        txtMaxValue_res = (TextView) findViewById(R.id.txtMaxValue_res);
//        rangeSeekbar_res = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar_res);
//        txtMinValueSquareFeetSelected_res = (TextView) findViewById(R.id.txtMinValueSquareFeetSelected_res);
//        txtMaxValueSquareFeetSelected_res = (TextView) findViewById(R.id.txtMaxValueSquareFeetSelected_res);
//        squareFeetRate_res = (CrystalRangeSeekbar) findViewById(R.id.squareFeetRate_res);
//        txtMinValueLotSizeSelected_res = (TextView) findViewById(R.id.txtMinValueLotSizeSelected_res);
//        txtMaxValueLotSizeSelected_res = (TextView) findViewById(R.id.txtMaxValueLotSizeSelected_res);
//        LotSize_res = (CrystalRangeSeekbar) findViewById(R.id.LotSize_res);
//        buy_res = (Button) findViewById(R.id.buy_res);
//        rent_res = (Button) findViewById(R.id.rent_res);
//        pg_res = (Button) findViewById(R.id.pg_res);
//        any_res = (Button) findViewById(R.id.any_res);
//        freehold_res = (Button) findViewById(R.id.freehold_res);
//        lease_res = (Button) findViewById(R.id.lease_res);
//        commercial_lay = (LinearLayout) findViewById(R.id.commercial_lay);
//        commercial_drop = (ImageView) findViewById(R.id.commercial_drop);
//        commercial_up = (ImageView) findViewById(R.id.commercial_up);
//        switchButton2 = (SwitchCompat) findViewById(R.id.switchButton2);
//        forSell_lay = (LinearLayout) findViewById(R.id.forSell_lay);
//        forSell_drop = (ImageView) findViewById(R.id.forSell_drop);
//        forSell_up = (ImageView) findViewById(R.id.forSell_up);
//        switchforsell = (SwitchCompat) findViewById(R.id.switchforsell);
//        decCabin = (Button) findViewById(R.id.decCabin);
//        incCabin = (Button) findViewById(R.id.incCabin);
//        cabin_text = (TextView) findViewById(R.id.cabin_text);
//        decBathroom = (Button) findViewById(R.id.decBathroom);
//        incBathroom = (Button) findViewById(R.id.incBathroom);
//        bathroom_text = (TextView) findViewById(R.id.bathroom_text);
//        decPentry = (Button) findViewById(R.id.decPentry);
//        incPentry = (Button) findViewById(R.id.incPentry);
//        pentry_text = (TextView) findViewById(R.id.pentry_text);
//        txtMinValue_com = (TextView) findViewById(R.id.txtMinValue_com);
//        txtMaxValue_com = (TextView) findViewById(R.id.txtMaxValue_com);
//        rangeSeekbar_com = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar_com);
//        txtMinValueSquareFeetSelected_com = (TextView) findViewById(R.id.txtMinValueSquareFeetSelected_com);
//        txtMaxValueSquareFeetSelected_com = (TextView) findViewById(R.id.txtMaxValueSquareFeetSelected_com);
//        squareFeetRate_com = (CrystalRangeSeekbar) findViewById(R.id.squareFeetRate_com);
//        txtMinValueLotSizeSelected_com = (TextView) findViewById(R.id.txtMinValueLotSizeSelected_com);
//        txtMaxValueLotSizeSelected_com = (TextView) findViewById(R.id.txtMaxValueLotSizeSelected_com);
//        LotSize_com = (CrystalRangeSeekbar) findViewById(R.id.LotSize_com);
//        buy_com = (Button) findViewById(R.id.buy_com);
//        id_lease_com = (Button) findViewById(R.id.id_lease_com);
//        lease_com = (Button) findViewById(R.id.lease_com);
//        any_com = (Button) findViewById(R.id.any_com);
//        freehold_com = (Button) findViewById(R.id.freehold_com);
//        lease_owner_com = (Button) findViewById(R.id.lease_owner_com);
//        submit_requirement_lay = (LinearLayout) findViewById(R.id.submit_requirement_lay);

//        backward1 = (RelativeLayout) findViewById(R.id.backward1);
//        forward1 = (RelativeLayout) findViewById(R.id.forward1);

        /*left1 = (ImageView) findViewById(R.id.left1);
        right1 = (ImageView) findViewById(R.id.right1);*/

    }

    private void onClicks() {

        imgBack.setOnClickListener(this);

        //imgFilter.setOnClickListener(this);

        imgShare.setOnClickListener(this);

        imgPrevious.setOnClickListener(this);

        imgNext.setOnClickListener(this);

        iv_location.setOnClickListener(this);

        action_A.setOnClickListener(this);

        actionB.setOnClickListener(this);

        action_c.setOnClickListener(this);

        action_d.setOnClickListener(this);

        action_e.setOnClickListener(this);

        action_f.setOnClickListener(this);

        action_g.setOnClickListener(this);

        //more_states.setOnClickListener(this);

//        decCabin.setOnClickListener(this);
//
//        incCabin.setOnClickListener(this);
//
//        decBathroom.setOnClickListener(this);
//
//        incBathroom.setOnClickListener(this);
//
//        decPentry.setOnClickListener(this);
//
//        incPentry.setOnClickListener(this);
//
//        //scroll_filter.setOnClickListener(this);
//
//        buy_com.setOnClickListener(this);
//
//        id_lease_com.setOnClickListener(this);
//
//        lease_com.setOnClickListener(this);
//
//        any_com.setOnClickListener(this);
//
//        freehold_com.setOnClickListener(this);
//
//        lease_owner_com.setOnClickListener(this);
//
//        submit_requirement_lay.setOnClickListener(this);
//
//        buy_res.setOnClickListener(this);
//
//        rent_res.setOnClickListener(this);
//
//        pg_res.setOnClickListener(this);
//
//        any_res.setOnClickListener(this);
//
//        freehold_res.setOnClickListener(this);
//
//        lease_res.setOnClickListener(this);
//
//        decCabin_res.setOnClickListener(this);
//
//        incCabin_res.setOnClickListener(this);
//
//        decBathroom_res.setOnClickListener(this);
//
//        incBathroom_res.setOnClickListener(this);
//
//        decPentry_res.setOnClickListener(this);
//
//        incPentry_res.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.imgBack:
                finish();
                break;
//            case R.id.imgFilter:
//                openDrawer();
//                break;
            case R.id.imgShare:
                intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text));

                startActivity(Intent.createChooser(intent, "Choose"));
                break;
            case R.id.iv_location:
                mapPopup(pictures1);
                break;
            case R.id.imgPrevious:
                if(getItemBack(viewPager.getCurrentItem())==0)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else if(getItemBack(viewPager.getCurrentItem())==pictures.size()-1)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else
                    viewPager.setCurrentItem(getItemBack(-1), false);
                break;
            case R.id.imgNext:
                if(getItem(+1) == pictures.size())
                    viewPager.setCurrentItem(getItem(0), true);
                else
                    viewPager.setCurrentItem(getItem(+1), false);
                break;
            case R.id.action_a:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_b:
                floatingActionsMenu.collapse();
                intent = new Intent(VillageDetail.this, Add_Project.class);
                startActivity(intent);
                break;
            case R.id.action_c:
                floatingActionsMenu.collapse();
                intent = new Intent(VillageDetail.this, Add_township.class);
                startActivity(intent);
                break;
            case R.id.action_d:
                floatingActionsMenu.collapse();
                intent = new Intent(VillageDetail.this, AddSectorLocality.class);
                startActivity(intent);
                break;
            case R.id.action_e:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_f:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_g:
                floatingActionsMenu.collapse();
                intent = new Intent(VillageDetail.this, Submit_Requirement.class);
                startActivity(intent);
                break;
//            case R.id.decCabin:
//                if (flagCabin > 0)
//                    flagCabin--;
//                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
//                cabin_text.setText(String.valueOf(flagCabin));
//                break;
//            case R.id.incCabin:
//                flagCabin++;
//                strCabin = "TotalRooms%20eq%20" + flagCabin + "%20and%20";
//                cabin_text.setText(String.valueOf(flagCabin));
//                break;
//            case R.id.decBathroom:
//                if (flagBathroom > 0)
//                    flagBathroom--;
//                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
//                bathroom_text.setText(String.valueOf(flagBathroom));
//                break;
//            case R.id.incBathroom:
//                flagBathroom++;
//                strBathroom = "TotalRooms%20eq%20" + flagBathroom + "%20and%20";
//                bathroom_text.setText(String.valueOf(flagBathroom));
//                break;
//            case R.id.decPentry:
//                if (flagPentry > 0)
//                    flagPentry--;
//                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
//                pentry_text.setText(String.valueOf(flagPentry));
//                break;
//            case R.id.incPentry:
//                flagPentry++;
//                strPentry = "TotalRooms%20eq%20" + flagPentry + "%20and%20";
//                pentry_text.setText(String.valueOf(flagPentry));
//                break;
//            case R.id.buy_com:
//                buy_com.setBackgroundResource(R.drawable.left_button_selected);
//                buy_com.setTextColor(getResources().getColor(R.color.white));
//                id_lease_com.setSelected(false);
//                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
//                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_com.setSelected(false);
//                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.id_lease_com:
//                id_lease_com.setBackgroundResource(R.drawable.center_button_selected);
//                id_lease_com.setTextColor(getResources().getColor(R.color.white));
//                buy_com.setSelected(false);
//                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
//                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_com.setSelected(false);
//                lease_com.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.lease_com:
//                lease_com.setBackgroundResource(R.drawable.right_button_selected);
//                lease_com.setTextColor(getResources().getColor(R.color.white));
//                buy_com.setSelected(false);
//                buy_com.setBackgroundResource(R.drawable.left_button_deselected);
//                buy_com.setTextColor(getResources().getColor(R.color.app_blue));
//                id_lease_com.setSelected(false);
//                id_lease_com.setBackgroundResource(R.drawable.center_button_deselected);
//                id_lease_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.any_com:
//                any_com.setBackgroundResource(R.drawable.left_button_selected);
//                any_com.setTextColor(getResources().getColor(R.color.white));
//                freehold_com.setSelected(false);
//                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
//                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_owner_com.setSelected(false);
//                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.freehold_com:
//                freehold_com.setBackgroundResource(R.drawable.center_button_selected);
//                freehold_com.setTextColor(getResources().getColor(R.color.white));
//                any_com.setSelected(false);
//                any_com.setBackgroundResource(R.drawable.left_button_deselected);
//                any_com.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_owner_com.setSelected(false);
//                lease_owner_com.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_owner_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.lease_owner_com:
//                lease_owner_com.setBackgroundResource(R.drawable.right_button_selected);
//                lease_owner_com.setTextColor(getResources().getColor(R.color.white));
//                any_com.setSelected(false);
//                any_com.setBackgroundResource(R.drawable.left_button_deselected);
//                any_com.setTextColor(getResources().getColor(R.color.app_blue));
//                freehold_com.setSelected(false);
//                freehold_com.setBackgroundResource(R.drawable.center_button_deselected);
//                freehold_com.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.submit_requirement_lay:
//                startActivity(new Intent(VillageDetail.this, Submit_Requirement.class));
//                break;
//            case R.id.buy_res:
//                buy_res.setBackgroundResource(R.drawable.left_button_selected);
//                buy_res.setTextColor(getResources().getColor(R.color.white));
//                rent_res.setSelected(false);
//                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
//                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
//                pg_res.setSelected(false);
//                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
//                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.rent_res:
//                rent_res.setBackgroundResource(R.drawable.center_button_selected);
//                rent_res.setTextColor(getResources().getColor(R.color.white));
//                buy_res.setSelected(false);
//                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
//                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
//                pg_res.setSelected(false);
//                pg_res.setBackgroundResource(R.drawable.right_button_deselected);
//                pg_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.pg_res:
//                pg_res.setBackgroundResource(R.drawable.right_button_selected);
//                pg_res.setTextColor(getResources().getColor(R.color.white));
//                buy_res.setSelected(false);
//                buy_res.setBackgroundResource(R.drawable.left_button_deselected);
//                buy_res.setTextColor(getResources().getColor(R.color.app_blue));
//                rent_res.setSelected(false);
//                rent_res.setBackgroundResource(R.drawable.center_button_deselected);
//                rent_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.any_res:
//                any_res.setBackgroundResource(R.drawable.left_button_selected);
//                any_res.setTextColor(getResources().getColor(R.color.white));
//                freehold_res.setSelected(false);
//                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
//                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_res.setSelected(false);
//                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.freehold_res:
//                freehold_res.setBackgroundResource(R.drawable.center_button_selected);
//                freehold_res.setTextColor(getResources().getColor(R.color.white));
//                any_res.setSelected(false);
//                any_res.setBackgroundResource(R.drawable.left_button_deselected);
//                any_res.setTextColor(getResources().getColor(R.color.app_blue));
//                lease_res.setSelected(false);
//                lease_res.setBackgroundResource(R.drawable.right_button_deselected);
//                lease_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.lease_res:
//                lease_res.setBackgroundResource(R.drawable.right_button_selected);
//                lease_res.setTextColor(getResources().getColor(R.color.white));
//                any_res.setSelected(false);
//                any_res.setBackgroundResource(R.drawable.left_button_deselected);
//                any_res.setTextColor(getResources().getColor(R.color.app_blue));
//                freehold_res.setSelected(false);
//                freehold_res.setBackgroundResource(R.drawable.center_button_deselected);
//                freehold_res.setTextColor(getResources().getColor(R.color.app_blue));
//                break;
//            case R.id.decCabin_res:
//                if (flagCabin_res > 0)
//                    flagCabin_res--;
//                strCabin_res = "TotalRooms%20eq%20" + flagCabin_res + "%20and%20";
//                cabin_text_res.setText(String.valueOf(flagCabin_res));
//                break;
//            case R.id.incCabin_res:
//                flagCabin_res++;
//                strCabin_res = "TotalRooms%20eq%20" + flagCabin_res + "%20and%20";
//                cabin_text_res.setText(String.valueOf(flagCabin_res));
//                break;
//            case R.id.decBathroom_res:
//                if (flagBathroom_res > 0)
//                    flagBathroom_res--;
//                strBathroom_res = "TotalRooms%20eq%20" + flagBathroom_res + "%20and%20";
//                bathroom_text_res.setText(String.valueOf(flagBathroom_res));
//                break;
//            case R.id.incBathroom_res:
//                flagBathroom_res++;
//                strBathroom_res = "TotalRooms%20eq%20" + flagBathroom_res + "%20and%20";
//                bathroom_text_res.setText(String.valueOf(flagBathroom_res));
//                break;
//            case R.id.decPentry_res:
//                if (flagPentry_res > 0)
//                    flagPentry_res--;
//                strPentry_res = "TotalRooms%20eq%20" + flagPentry_res + "%20and%20";
//                pentry_text_res.setText(String.valueOf(flagPentry_res));
//                break;
//            case R.id.incPentry_res:
//                flagPentry_res++;
//                strPentry_res = "TotalRooms%20eq%20" + flagPentry_res + "%20and%20";
//                pentry_text_res.setText(String.valueOf(flagPentry_res));
//                break;

        }

    }

    private int getItem(int i) {
        if(viewPager.getCurrentItem() == pictures.size()-1)
            return 0;
        else
            return viewPager.getCurrentItem() + i;
    }

    private int getItemBack(int i) {

        if(viewPager.getCurrentItem() == pictures.size()-1)
            return pictures.size()-2;
        else if(viewPager.getCurrentItem() == 0)
            return pictures.size()-1;
        else
            return viewPager.getCurrentItem() + i;

    }

    public void onVillageDetail(){
        new CommonSyncwithoutstatus(this,
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_VILLAGE_DETAIL +
                        Id ,
                "",
                "Loading...",this,
                Urls.URL_VILLAGE_DETAIL,
                Constants.GET)
                .execute();
    }

    @Override
    public void onResultListener(String result, String which) {

       if(null != result && !result.equalsIgnoreCase("")){
           if(which.equalsIgnoreCase(Urls.URL_VILLAGE_DETAIL)){
               try{
                   JSONObject jsonObject = new JSONObject(result);
                   int statusCode = jsonObject.getInt("StatusCode");
                   if(statusCode == 200){
                       final JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                       if(jsonObject1.getString("Tehsil").equalsIgnoreCase("null"))
                           villageName.setText("N/A");
                       else
                           villageName.setText(jsonObject1.getString("Tehsil"));
                       String vill = jsonObject1.getString("Name");
                       PreferenceConnector.getInstance(this).savePreferences("Village", vill);
                       if(jsonObject1.getString("State").equalsIgnoreCase("null"))
                           stateName.setText("N/A");
                       else
                           stateName.setText(jsonObject1.getString("State"));
                       String st = jsonObject1.getString("State");
                       PreferenceConnector.getInstance(this).savePreferences("State", st);
                       if(jsonObject1.getString("District").equalsIgnoreCase("null"))
                           districtName.setText("N/A");
                       else
                           districtName.setText(jsonObject1.getString("District"));
                       String dist = jsonObject1.getString("District");
                       PreferenceConnector.getInstance(this).savePreferences("Dist", dist);
                       if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                           totalArea.setText("N/A");
                       else if(!jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                           totalArea.setText(jsonObject1.getString("TotalArea") + " N/A" );
                       else if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && !jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                           totalArea.setText("N/A " + jsonObject1.getString("TotalAreaSizeUnit"));
                       else
                           totalArea.setText(jsonObject1.getString("TotalArea") + " " + jsonObject1.getString("TotalAreaSizeUnit"));
                       /*if(jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                           totalAreaUnits.setText("N/A");
                       else
                           totalAreaUnits.setText(jsonObject1.getString("TotalAreaSizeUnit"));*/
                       if(jsonObject1.getString("VillageType").equalsIgnoreCase("null"))
                           villageType.setText("N/A");
                       else
                           villageType.setText(jsonObject1.getString("VillageType"));
                       if(jsonObject1.getString("PoliceStation").equalsIgnoreCase("null"))
                           policeStation.setText("N/A");
                       else
                           policeStation.setText(jsonObject1.getString("PoliceStation"));
                       if(jsonObject1.getString("NameOfPostOffice").equalsIgnoreCase("null"))
                           postOffice.setText("N/A");
                       else
                           postOffice.setText(jsonObject1.getString("NameOfPostOffice"));
                       if(jsonObject1.getString("PopulatedArea").equalsIgnoreCase("null"))
                           population.setText("N/A");
                       else
                           population.setText(jsonObject1.getString("PopulatedArea"));
                       if(jsonObject1.getString("AboutVillage").equalsIgnoreCase("null"))
                           aboutVillage.setText("N/A");
                       else
                           aboutVillage.setText(jsonObject1.getString("AboutVillage"));

                       aboutVillage.post(new Runnable() {
                           @Override
                           public void run() {
                               final int lineCount1 = aboutVillage.getLineCount();
                               if(lineCount1 <= 5) {
                                   aboutVillage.setEllipsize(null);
                                   lineCount.setVisibility(View.GONE);
                               }
                               else{
                                   lineCount.setVisibility(View.VISIBLE);
                                   lineCount.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {
                                           lineCount.setVisibility(View.GONE);
                                           aboutVillage.setEllipsize(null);
                                           aboutVillage.setMaxLines(Integer.MAX_VALUE);
                                           try {
                                               aboutVillage.setText(jsonObject1.getString("AboutVillage"));
                                           } catch (JSONException e) {
                                               e.printStackTrace();
                                           }
                                       }
                                   });
                               }
                               // Use lineCount here
                           }
                       });

                       if(jsonObject1.getString("AreaUnderAgriculture").equalsIgnoreCase("null") && (jsonObject1.getString("AreaUnderAgricultureSizeUnit").equalsIgnoreCase("null") || jsonObject1.getString("AreaUnderAgricultureSizeUnit").equalsIgnoreCase("none")))
                           agri_area_unit.setText("N/A");
                       else if(jsonObject1.getString("AreaUnderAgriculture").equalsIgnoreCase("null") && (!jsonObject1.getString("AreaUnderAgricultureSizeUnit").equalsIgnoreCase("null") || !jsonObject1.getString("AreaUnderAgricultureSizeUnit").equalsIgnoreCase("none")))
                           agri_area_unit.setText("N/A " + jsonObject1.getString("AreaUnderAgricultureSizeUnit"));
                       else if(!jsonObject1.getString("AreaUnderAgriculture").equalsIgnoreCase("null") && (jsonObject1.getString("AreaUnderAgricultureSizeUnit").equalsIgnoreCase("null") || jsonObject1.getString("AreaUnderAgricultureSizeUnit").equalsIgnoreCase("none")))
                           agri_area_unit.setText(jsonObject1.getString("AreaUnderAgriculture") + " N/A");
                       else
                           agri_area_unit.setText(jsonObject1.getString("AreaUnderAgriculture") + " " + jsonObject1.getString("AreaUnderAgricultureSizeUnit"));

                       if(jsonObject1.getString("ShamlatLandArea").equalsIgnoreCase("null") && (jsonObject1.getString("ShamlatLandAreaSizeUnit").equalsIgnoreCase("null") || jsonObject1.getString("ShamlatLandAreaSizeUnit").equalsIgnoreCase("none")))
                           shamlat_area_unit.setText("N/A");
                       else if(jsonObject1.getString("ShamlatLandArea").equalsIgnoreCase("null") && (!jsonObject1.getString("ShamlatLandAreaSizeUnit").equalsIgnoreCase("null") || !jsonObject1.getString("ShamlatLandAreaSizeUnit").equalsIgnoreCase("none")))
                           shamlat_area_unit.setText("N/A" + " " + jsonObject1.getString("ShamlatLandAreaSizeUnit"));
                       else if(!jsonObject1.getString("ShamlatLandArea").equalsIgnoreCase("null") && (jsonObject1.getString("ShamlatLandAreaSizeUnit").equalsIgnoreCase("null") || jsonObject1.getString("ShamlatLandAreaSizeUnit").equalsIgnoreCase("none")))
                           shamlat_area_unit.setText(jsonObject1.getString("ShamlatLandArea") + " N/A");
                       else
                           shamlat_area_unit.setText(jsonObject1.getString("ShamlatLandArea") + " " + jsonObject1.getString("ShamlatLandAreaSizeUnit"));

                       if(jsonObject1.getString("AreaUnderSecton4Forest").equalsIgnoreCase("null") && (jsonObject1.getString("AreaUnderSecton4ForestSizeUnit").equalsIgnoreCase("null") || jsonObject1.getString("AreaUnderSecton4ForestSizeUnit").equalsIgnoreCase("none")))
                           forest_area_unit.setText("N/A");
                       else if(jsonObject1.getString("AreaUnderSecton4Forest").equalsIgnoreCase("null") && (!jsonObject1.getString("AreaUnderSecton4ForestSizeUnit").equalsIgnoreCase("null") || !jsonObject1.getString("AreaUnderSecton4ForestSizeUnit").equalsIgnoreCase("none")))
                           forest_area_unit.setText("N/A " + jsonObject1.getString("AreaUnderSecton4ForestSizeUnit"));
                       else if(!jsonObject1.getString("AreaUnderSecton4Forest").equalsIgnoreCase("null") && (jsonObject1.getString("AreaUnderSecton4ForestSizeUnit").equalsIgnoreCase("null") || jsonObject1.getString("AreaUnderSecton4ForestSizeUnit").equalsIgnoreCase("none")))
                           forest_area_unit.setText(jsonObject1.getString("AreaUnderSecton4Forest") + " N/A");
                       else
                           forest_area_unit.setText(jsonObject1.getString("AreaUnderSecton4Forest") + " " + jsonObject1.getString("AreaUnderSecton4ForestSizeUnit"));

                       if(jsonObject1.getString("AreaUnderSection5KandiArea").equalsIgnoreCase("null") && (jsonObject1.getString("AreaUnderSection5KandiAreaSizeUnit").equalsIgnoreCase("null") || jsonObject1.getString("AreaUnderSection5KandiAreaSizeUnit").equalsIgnoreCase("none")))
                           kandi_area_unit.setText("N/A");
                       else if(!jsonObject1.getString("AreaUnderSection5KandiArea").equalsIgnoreCase("null") && (jsonObject1.getString("AreaUnderSection5KandiAreaSizeUnit").equalsIgnoreCase("null") || jsonObject1.getString("AreaUnderSection5KandiAreaSizeUnit").equalsIgnoreCase("none")))
                           kandi_area_unit.setText(jsonObject1.getString("AreaUnderSection5KandiArea") + " N/A");
                       else if(jsonObject1.getString("AreaUnderSection5KandiArea").equalsIgnoreCase("null") && (!jsonObject1.getString("AreaUnderSection5KandiAreaSizeUnit").equalsIgnoreCase("null") || !jsonObject1.getString("AreaUnderSection5KandiAreaSizeUnit").equalsIgnoreCase("none")))
                           kandi_area_unit.setText("N/A " + jsonObject1.getString("AreaUnderSection5KandiAreaSizeUnit"));
                       else
                           kandi_area_unit.setText(jsonObject1.getString("AreaUnderSection5KandiArea") + " " + jsonObject1.getString("AreaUnderSection5KandiAreaSizeUnit"));

                       JSONArray jsonArray = jsonObject1.getJSONArray("Pictures");
                       pictures.clear();
                       if(jsonArray.length()!=0){
                           for(int i = 0; i<jsonArray.length(); i++){
                               JSONObject object = jsonArray.getJSONObject(i);
                               StateListModel.Pictures stateListModel = new StateListModel.Pictures();
                               stateListModel.setId(object.getString("Id"));
                               stateListModel.setUrl(Urls.BASE_CONTACT_IMAGE_URL +object.getString("Url"));
                               pictures.add(stateListModel);
                           }
                       }

                       imageAdapter = new ImageAdapter(this, pictures);
                       viewPager.setAdapter(imageAdapter);
                       imageAdapter.notifyDataSetChanged();

                       image_video_adapter = new Image_Video_Adapter(this, pictures, this);
                       list_images_videos.setAdapter(image_video_adapter);
                       list_images_videos.setLayoutManager(new LinearLayoutManager(this,  LinearLayout.HORIZONTAL, false));
                       image_video_adapter.notifyDataSetChanged();

                       JSONArray jsonArray1 = jsonObject1.getJSONArray("UploadRevenueMapFile");
                       pictures1.clear();
                       if(jsonArray1.length()!=0){
                           for(int i = 0; i<jsonArray1.length(); i++){
                               JSONObject object = jsonArray1.getJSONObject(i);
                               StateListModel.Pictures stateListModel = new StateListModel.Pictures();
                               stateListModel.setId(object.getString("Id"));
                               stateListModel.setUrl(Urls.BASE_CONTACT_IMAGE_URL +object.getString("Url"));
                               pictures1.add(stateListModel);
                           }
                       }

                       stateImageAdapter = new StateImageAdapter(this, pictures1);
                       mohali_photos.setAdapter(stateImageAdapter);
                       mohali_photos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                       stateImageAdapter.notifyDataSetChanged();

                       if(pictures1.size() == 0){
                           mohali_photos.setVisibility(View.GONE);
                           no_data1.setVisibility(View.VISIBLE);
                       }else{
                           mohali_photos.setVisibility(View.VISIBLE);
                           no_data1.setVisibility(View.GONE);
                       }

                       JSONArray jsonArray2 = jsonObject1.getJSONArray("KeyDistances");
                       featureData.clear();
                       if(jsonArray2.length()!=0){
                           for(int i = 0; i<jsonArray2.length(); i++){
                               JSONObject object = jsonArray2.getJSONObject(i);
                               KeydistanceModel keydistanceModel = new KeydistanceModel();
                               keydistanceModel.setKey(object.getString("Key"));
                               keydistanceModel.setValue(object.getString("Value"));
                               keydistanceModel.setValue1(object.getString("Value1"));
                               keydistanceModel.setValue2(object.getString("Value2"));
                               featureData.add(keydistanceModel);
                           }
                       }

                       featuresAdapter = new FeaturesAdapter(this, featureData);
                       features.setAdapter(featuresAdapter);
                       features.setLayoutManager(new LinearLayoutManager(this));
                       featuresAdapter.notifyDataSetChanged();

                       if(featureData.size() == 0){
                           features.setVisibility(View.GONE);
                           no_data.setVisibility(View.VISIBLE);
                       }else{
                           features.setVisibility(View.VISIBLE);
                           no_data.setVisibility(View.GONE);
                       }
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void setViewPager(int position) {
        viewPager.setCurrentItem(position, false);

    }

    public void submitReqPopUp(String url){
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.submit_requirement_popup);
        close = (ImageView) dialog.findViewById(R.id.close);
        img = (ImageView) dialog.findViewById(R.id.img);
        Picasso.with(this).load(url).into(img);
        project_name = (TextView) dialog.findViewById(R.id.name);
        project_name.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Village", ""));
        description = (TextView) dialog.findViewById(R.id.description);
        description.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Dist", "") +
                PreferenceConnector.getInstance(this).loadSavedPreferences("State", ""));
        yes = (RadioButton) dialog.findViewById(R.id.yes);
        no = (RadioButton) dialog.findViewById(R.id.no);
        buy = (RadioButton) dialog.findViewById(R.id.buy);
        rent = (RadioButton) dialog.findViewById(R.id.rent);
        property_detailing = (LinearLayout) dialog.findViewById(R.id.property_detailing);
        category_spinner = (Spinner) dialog.findViewById(R.id.category_spinner);
        property_spinner = (Spinner) dialog.findViewById(R.id.property_spinner);
        check_studio = (CheckBox) dialog.findViewById(R.id.check_studio);
        check_2bhk = (CheckBox) dialog.findViewById(R.id.check_2bhk);
        check_3bhk = (CheckBox) dialog.findViewById(R.id.check_3bhk);
        check_4bhk = (CheckBox) dialog.findViewById(R.id.check_4bhk);
        message = (EditText) dialog.findViewById(R.id.message);
        datePicker = (EditText) dialog.findViewById(R.id.datePicker);
        media = (EditText) dialog.findViewById(R.id.media);
        submit = (Button) dialog.findViewById(R.id.submit);

        yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    property_detailing.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    startActivity(new Intent(VillageDetail.this, Submit_Requirement.class));
                }
            }
        });

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }

    private void propertySell_RentPopUp(String url) {
        dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.property_sell_rent);
        close1 = (ImageView) dialog1.findViewById(R.id.close);
        image = (ImageView) dialog1.findViewById(R.id.image);
        Picasso.with(this).load(url).into(image);
        project_name = (TextView) dialog1.findViewById(R.id.project_name);
        project_name.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Village", ""));
        desc = (TextView) dialog1.findViewById(R.id.desc);
        desc.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Dist", "") +
                PreferenceConnector.getInstance(this).loadSavedPreferences("State", ""));
        yes1 = (RadioButton) dialog1.findViewById(R.id.yes);
        no1 = (RadioButton) dialog1.findViewById(R.id.no);
        sell = (RadioButton) dialog1.findViewById(R.id.sell);
        rent1 = (RadioButton) dialog1.findViewById(R.id.rent);
        property_detailing1 = (LinearLayout) dialog1.findViewById(R.id.property_detailing);
        category_spinner1 = (Spinner) dialog1.findViewById(R.id.category_spinner);
        property_spinner1 = (Spinner) dialog1.findViewById(R.id.property_spinner);
        tower_name_no = (EditText) dialog1.findViewById(R.id.tower_name_no);
        unit_no = (EditText) dialog1.findViewById(R.id.unit_no);
        message1 = (EditText) dialog1.findViewById(R.id.message1);
        datePick = (EditText) dialog1.findViewById(R.id.datePick);
        media1 = (EditText) dialog1.findViewById(R.id.media);
        submit1 = (Button) dialog1.findViewById(R.id.submit);

        yes1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    property_detailing1.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    //startActivity(new Intent(getContext(), Submit_Requirement.class));

                }
            }
        });

        dialog1.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog1.show();

        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.cancel();
            }
        });
    }

    private void mapPopup(ArrayList<StateListModel.Pictures> pictures) {
        dialog = new Dialog(VillageDetail.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map_pop_up);
        close = (ImageView) dialog.findViewById(R.id.close);
        viewPager = (ViewPager) dialog.findViewById(R.id.map_images);
        imageAdapter = new ImageAdapter(this, pictures);
        viewPager.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

}
