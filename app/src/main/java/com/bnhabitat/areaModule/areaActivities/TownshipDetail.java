package com.bnhabitat.areaModule.areaActivities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.Govt_Notification_Adapter;
import com.bnhabitat.areaModule.adapter.ImageAdapter;
import com.bnhabitat.areaModule.adapter.Image_Video_Adapter;
import com.bnhabitat.areaModule.adapter.ProjectListingAdapter;
import com.bnhabitat.areaModule.adapter.RecreationalAdapter;
import com.bnhabitat.areaModule.adapter.StateImageAdapter;
import com.bnhabitat.areaModule.adapter.TownshipFileAdapter;
import com.bnhabitat.areaModule.adapter.TownshipImageAdapter;
import com.bnhabitat.areaModule.model.ClubFacilities;
import com.bnhabitat.areaModule.model.ProjectListModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.areaModule.model.TownshipFilesModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.township.townactivities.AddSectorLocality;
import com.bnhabitat.township.townactivities.Add_Project;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TownshipDetail extends AppCompatActivity implements ViewPager.OnPageChangeListener, ViewPagerListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener, View.OnClickListener {

    RecyclerView list_images_videos, project_list, gmada_photos;

    public ViewPager viewPager;

    TownshipImageAdapter imageAdapter;
    TownshipFileAdapter image_video_adapter;
    ProjectListingAdapter projectListingAdapter;
    StateImageAdapter stateImageAdapter;
    RecreationalAdapter recreationalAdapter;
    ArrayList<ProjectListModel> projectListingArrayList = new ArrayList<>();
    ArrayList<ProjectListModel.ProjectCategoryTypes> resTypes  = new ArrayList<>();
    ArrayList<ProjectListModel.ProjectCategoryTypes> comTypes  = new ArrayList<>();
    ArrayList<TownshipFilesModel> townshipFilesModels = new ArrayList<>();
    ArrayList<TownshipFilesModel> zoningFilesModels = new ArrayList<>();

    ImageView imgBack, imgLocation, imgFilter, imgPrevious, imgNext;
    TextView townshipName, township_name, district, state, totalArea, totalAreaUnits;

    //INCLUDED LAYOUT
    TextView project_count, residential_count, commercial_count;

    //QUICK FACTS TEXTVIEW
    TextView tehsilName, localbodytype, localBodyName, developerComapnyName, negativeMark, zipcode;

    //PROJECTS COUNT
    TextView total_projects;

    String Id, Name;

    //CLUB
    LinearLayout clubLayout, clubing, description, areas, area, coverArea, superArea, buildUpArea, carpetArea, clubfacilities, clubImages;
    TextView clubName, clubDescription, totalarea, totalareaunits, coverarea, coverareaunits, superarea, superareaunits, builduparea, buildupareaunits, carpetarea, carpetareaunits;
    RecyclerView clubFac, clubImg;

    Dialog dialog, dialog1;
    ImageView userPic, msg, call;
    TextView first_last_name, professional_txt, submitReq, propertSell_Rent;
    String contact = "", email = "";
    private static final int REQUEST_PHONE_CALL = 198;
    ImageView close, img, close1, image;
    TextView project_name, project_name1, desc, description1;
    RadioButton yes, no, buy, rent, yes1, no1, sell, rent1;
    LinearLayout property_detailing, property_detailing1;
    Spinner category_spinner, property_spinner, category_spinner1, property_spinner1;
    CheckBox check_studio, check_2bhk, check_3bhk, check_4bhk;
    EditText message, datePicker, media, tower_name_no, unit_no, message1, datePick, media1;
    Button submit, submit1;
    RelativeLayout backward1, forward1;
    ImageView left1, right1;
    String name_number;
    FloatingActionsMenu floatingActionsMenu;
    private View actionB,action_A, action_c, action_d, action_e, action_f, action_g;
    TextView no_data, no_data1, no_data2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_township_detail);

        /*action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(TownshipDetail.this, Add_Project.class);
                startActivity(intent);

            }
        });
        action_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(TownshipDetail.this, Add_township.class);
                startActivity(intent);

            }
        });
        action_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(TownshipDetail.this, AddSectorLocality.class);
                startActivity(intent);

            }
        });
        action_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        action_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        action_g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(TownshipDetail.this, Submit_Requirement.class);
                startActivity(intent);

            }
        });*/




        /*imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapPopup(zoningFilesModels);
            }
        });*/

        //onTownshipDetail();
        /*if(PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("") || PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("null"))
            name_number = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        else
            name_number = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "");
        first_last_name.setText(name_number);
        contact = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        email = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.EMAIL, "");

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (ContextCompat.checkSelfPermission(TownshipDetail.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(TownshipDetail.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                } else {
                    callIntent.setData(Uri.parse("tel:" + contact));
                    startActivity(callIntent);
                }
            }
        });

        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
                startActivity(Intent.createChooser(intent, "Choose"));
            }
        });

        submitReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitReqPopUp(townshipFilesModels.get(0).getFile().getUrl());
            }
        });

        propertSell_Rent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                propertySell_RentPopUp(townshipFilesModels.get(0).getFile().getUrl());
            }
        });*/


        /*imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItemBack(viewPager.getCurrentItem())==0)
                    viewPager.setCurrentItem(getItemBack(townshipFilesModels.size()-1), true);
                else if(getItemBack(viewPager.getCurrentItem())==townshipFilesModels.size()-1)
                    viewPager.setCurrentItem(getItemBack(townshipFilesModels.size()-1), true);
                else
                    viewPager.setCurrentItem(getItemBack(-1), false);

            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItem(+1) == townshipFilesModels.size())
                    viewPager.setCurrentItem(getItem(0), true);
                else
                viewPager.setCurrentItem(getItem(+1), false);

            }
        });*/

    }

    @Override
    protected void onStart() {
        Intent intent = getIntent();
        Id = intent.getStringExtra("TownshipId");
        Name = intent.getStringExtra("TownshipName");
        initialize();
        onClicks();
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.transparent));
            }

            @SuppressLint("ResourceType")
            @Override
            public void onMenuCollapsed() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.tran));

            }
        });
        townshipName.setText(Name);
        clubLayout.setVisibility(View.GONE);
        super.onStart();
    }

    private void onClicks() {
        imgBack.setOnClickListener(this);

        //imgFilter.setOnClickListener(this);

        imgLocation.setOnClickListener(this);

        action_A.setOnClickListener(this);

        actionB.setOnClickListener(this);

        action_c.setOnClickListener(this);

        action_d.setOnClickListener(this);

        action_e.setOnClickListener(this);

        action_f.setOnClickListener(this);

        action_g.setOnClickListener(this);

        imgPrevious.setOnClickListener(this);

        imgNext.setOnClickListener(this);
    }

    private void initialize() {

        townshipName = (TextView) findViewById(R.id.townshipName);

        clubFac = (RecyclerView) findViewById(R.id.clubFac);
        clubImg = (RecyclerView) findViewById(R.id.clubImg);

        no_data = (TextView) findViewById(R.id.no_data);
        no_data1 = (TextView) findViewById(R.id.no_data1);
        no_data2 = (TextView) findViewById(R.id.no_data2);
        clubName = (TextView) findViewById(R.id.clubName);
        clubDescription = (TextView) findViewById(R.id.clubDescription);
        totalarea = (TextView) findViewById(R.id.totalarea);
        totalareaunits = (TextView) findViewById(R.id.totalareaunits);
        coverarea = (TextView) findViewById(R.id.coverarea);
        coverareaunits = (TextView) findViewById(R.id.coverareaunits);
        superarea = (TextView) findViewById(R.id.superarea);
        superareaunits = (TextView) findViewById(R.id.superareaunits);
        builduparea = (TextView) findViewById(R.id.builduparea);
        buildupareaunits = (TextView) findViewById(R.id.buildupareaunits);
        carpetarea = (TextView) findViewById(R.id.carpetarea);
        carpetareaunits = (TextView) findViewById(R.id.carpetareaunits);

        clubLayout = (LinearLayout) findViewById(R.id.clubLayout);
        clubing = (LinearLayout) findViewById(R.id.clubing);
        description = (LinearLayout) findViewById(R.id.description);
        areas = (LinearLayout) findViewById(R.id.areas);
        area = (LinearLayout) findViewById(R.id.area);
        coverArea = (LinearLayout) findViewById(R.id.coverArea);
        superArea = (LinearLayout) findViewById(R.id.superArea);
        buildUpArea = (LinearLayout) findViewById(R.id.buildUpArea);
        carpetArea = (LinearLayout) findViewById(R.id.carpetArea);
        clubfacilities = (LinearLayout) findViewById(R.id.clubfacilities);
        clubImages = (LinearLayout) findViewById(R.id.clubImages);

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgLocation = (ImageView) findViewById(R.id.imgLocation);
        imgFilter = (ImageView) findViewById(R.id.imgFilter);

        project_count = (TextView) findViewById(R.id.project_count);
        residential_count = (TextView) findViewById(R.id.residential_count);
        commercial_count = (TextView) findViewById(R.id.commercial_count);

        township_name = (TextView) findViewById(R.id.township_name);
        district = (TextView) findViewById(R.id.district);
        state = (TextView) findViewById(R.id.state);
        totalArea = (TextView) findViewById(R.id.totalArea);
        totalAreaUnits = (TextView) findViewById(R.id.totalAreaUnits);
        totalAreaUnits.setVisibility(View.GONE);
        tehsilName = (TextView) findViewById(R.id.tehsilName);
        localbodytype = (TextView) findViewById(R.id.localbodytype);
        localBodyName = (TextView) findViewById(R.id.localBodyName);
        developerComapnyName = (TextView) findViewById(R.id.developerComapnyName);
        negativeMark = (TextView) findViewById(R.id.negativeMark);
        zipcode = (TextView) findViewById(R.id.zipcode);

        total_projects = (TextView) findViewById(R.id.total_projects);
        viewPager = (ViewPager) findViewById(R.id.view_images);
        imgPrevious = (ImageView) findViewById(R.id.imgPrevious);
        imgNext = (ImageView) findViewById(R.id.imgNext);
        list_images_videos = (RecyclerView) findViewById(R.id.list_images_videos);
        project_list = (RecyclerView) findViewById(R.id.project_list);
        gmada_photos = (RecyclerView) findViewById(R.id.gmada_photos);

        userPic = (ImageView) findViewById(R.id.userPic);
        msg = (ImageView) findViewById(R.id.msg);
        call = (ImageView) findViewById(R.id.call);

        first_last_name = (TextView) findViewById(R.id.first_last_name);
        professional_txt = (TextView) findViewById(R.id.professional_txt);
        submitReq = (TextView) findViewById(R.id.submitReq);
        propertSell_Rent = (TextView) findViewById(R.id.propertSell_Rent);

        floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        action_c = findViewById(R.id.action_c);
        action_d = findViewById(R.id.action_d);
        action_e= findViewById(R.id.action_e);
        action_f = findViewById(R.id.action_f);
        action_g = findViewById(R.id.action_g);

        /*backward1 = (RelativeLayout) findViewById(R.id.backward1);
        forward1 = (RelativeLayout) findViewById(R.id.forward1);

        left1 = (ImageView) findViewById(R.id.left1);
        right1 = (ImageView) findViewById(R.id.right1);*/
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.imgBack:
                finish();
                break;
            case R.id.imgLocation:
                mapPopup(zoningFilesModels);
                break;
            case R.id.action_a:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_b:
                floatingActionsMenu.collapse();
                intent = new Intent(TownshipDetail.this, Add_Project.class);
                startActivity(intent);
                break;
            case R.id.action_c:
                floatingActionsMenu.collapse();
                intent = new Intent(TownshipDetail.this, Add_township.class);
                startActivity(intent);
                break;
            case R.id.action_d:
                floatingActionsMenu.collapse();
                intent = new Intent(TownshipDetail.this, AddSectorLocality.class);
                startActivity(intent);
                break;
            case R.id.action_e:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_f:
                floatingActionsMenu.collapse();
                break;
            case R.id.action_g:
                floatingActionsMenu.collapse();
                intent = new Intent(TownshipDetail.this, Submit_Requirement.class);
                startActivity(intent);
                break;
            case R.id.imgPrevious:
                if(getItemBack(viewPager.getCurrentItem())==0)
                    viewPager.setCurrentItem(getItemBack(townshipFilesModels.size()-1), true);
                else if(getItemBack(viewPager.getCurrentItem())==townshipFilesModels.size()-1)
                    viewPager.setCurrentItem(getItemBack(townshipFilesModels.size()-1), true);
                else
                    viewPager.setCurrentItem(getItemBack(-1), false);
                break;
            case R.id.imgNext:
                if(getItem(+1) == townshipFilesModels.size())
                    viewPager.setCurrentItem(getItem(0), true);
                else
                    viewPager.setCurrentItem(getItem(+1), false);
                break;

        }

    }

    public int getItem(int i) {
        if(viewPager.getCurrentItem() == townshipFilesModels.size()-1)
            return 0;
        else
            return viewPager.getCurrentItem() + i;
    }

    private int getItemBack(int i) {

        if(viewPager.getCurrentItem() == townshipFilesModels.size()-1)
            return townshipFilesModels.size()-2;
        else if(viewPager.getCurrentItem() == 0)
            return townshipFilesModels.size()-1;
        else
            return viewPager.getCurrentItem() + i;

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onResume() {
        onTownshipDetail();
        super.onResume();
    }

    @Override
    public void setViewPager(int position) {
        viewPager.setCurrentItem(position, false);
    }

    public void onTownshipDetail(){
        new CommonSyncwithoutstatus(this,
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_TOWNSHIP_DETAIL +
                        Id ,
                "",
                "Loading...",this,
                Urls.URL_TOWNSHIP_DETAIL,
                Constants.GET)
                .execute();
    }

    @Override
    public void onResultListener(String result, String which) {

        if(null != result && !result.equalsIgnoreCase("")){
            if(which.equalsIgnoreCase(Urls.URL_TOWNSHIP_DETAIL)){
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    int statusCode = jsonObject.getInt("StatusCode");
                    if(statusCode == 200){
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");

                        JSONArray jsonArray = jsonObject1.getJSONArray("Projects");
                        project_count.setText(String.valueOf(jsonArray.length()));
                        total_projects.setText(String.valueOf(jsonArray.length()));

                        projectListingArrayList.clear();
                        for(int i = 0 ; i<jsonArray.length(); i++){
                            JSONObject jsonObjectnew = jsonArray.getJSONObject(i);
                            ProjectListModel projectListModel = new ProjectListModel();
                            projectListModel.setName(jsonObjectnew.getString("Name"));
                            projectListModel.setLocalityName(jsonObjectnew.getString("LocalityName"));
                            projectListModel.setZipCode(jsonObjectnew.getString("ZipCode"));
                            projectListModel.setStateName(jsonObjectnew.getString("StateName"));
                            projectListModel.setDistrictName(jsonObjectnew.getString("DistrictName"));
                            projectListModel.setTehsilName(jsonObjectnew.getString("TehsilName"));
                            projectListModel.setTownName(jsonObjectnew.getString("TownName"));

                            JSONArray jsonArray1 = jsonObjectnew.getJSONArray("ProjectCategoryTypes");
                            ArrayList<ProjectListModel.ProjectCategoryTypes> projectCategoryTypes  = new ArrayList<>();
                            projectCategoryTypes.clear();
                            for(int index = 0; index < jsonArray1.length(); index++){
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(index);
                                ProjectListModel.ProjectCategoryTypes projectCategoryTypes1 = new ProjectListModel.ProjectCategoryTypes();
                                projectCategoryTypes1.setType(jsonObject2.getString("Type"));
                                if(projectCategoryTypes1.getType().equalsIgnoreCase("Residential"))
                                    resTypes.add(projectCategoryTypes1);

                                if(projectCategoryTypes1.getType().equalsIgnoreCase("Commercial"))
                                    comTypes.add(projectCategoryTypes1);

                                projectCategoryTypes.add(projectCategoryTypes1);
                            }

                            residential_count.setText(String.valueOf(resTypes.size()));
                            commercial_count.setText(String.valueOf(comTypes.size()));

                            projectListModel.setProjectCategoryTypes(projectCategoryTypes);

                            JSONArray jsonArray2 = jsonObjectnew.getJSONArray("ProjectFiles");
                            ArrayList<ProjectListModel.ProjectFiles> projectFiles = new ArrayList<>();
                            projectFiles.clear();
                            for(int i1 = 0; i1<jsonArray2.length(); i1++){
                                JSONObject jsonObject2 = jsonArray2.getJSONObject(i1);
                                ProjectListModel.ProjectFiles projectFiles1 = new ProjectListModel.ProjectFiles();
                                projectFiles1.setCategory(jsonObject2.getString("Category"));
                                projectFiles1.setFileUrl(Urls.BASE_CONTACT_IMAGE_URL + jsonObject2.getString("FileUrl"));
                                projectFiles.add(projectFiles1);
                            }

                            projectListModel.setProjectFiles(projectFiles);
                            projectListingArrayList.add(projectListModel);
                        }

                        projectListingAdapter = new ProjectListingAdapter(this, projectListingArrayList);
                        project_list.setLayoutManager(new LinearLayoutManager(this));
                        project_list.setAdapter(projectListingAdapter);
                        projectListingAdapter.notifyDataSetChanged();

                        if(jsonObject1.getString("Name").equalsIgnoreCase("null"))
                            township_name.setText("N/A");
                        else
                            township_name.setText(jsonObject1.getString("Name"));
                        String townshipname = jsonObject1.getString("Name");
                        PreferenceConnector.getInstance(this).savePreferences("Township", townshipname);
                        if(jsonObject1.getString("DistrictName").equalsIgnoreCase("null"))
                            district.setText("N/A");
                        else
                            district.setText(jsonObject1.getString("DistrictName"));
                        String dis = jsonObject1.getString("DistrictName");
                        PreferenceConnector.getInstance(this).savePreferences("Dist", dis);
                        if(jsonObject1.getString("StateName").equalsIgnoreCase("null"))
                            state.setText("N/A");
                        else
                            state.setText(jsonObject1.getString("StateName"));
                        String st = jsonObject1.getString("StateName");
                        PreferenceConnector.getInstance(this).savePreferences("Stating", st);
                        if(!jsonObject1.getString("PropertySizeUnit").equalsIgnoreCase("null")) {
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("PropertySizeUnit");
                            if (jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject2.getString("SizeUnit").equalsIgnoreCase("null"))
                                totalArea.setText("N/A");
                            else if (!jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject2.getString("SizeUnit").equalsIgnoreCase("null"))
                                totalArea.setText(jsonObject1.getString("TotalArea") + " N/A");
                            else if (jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && !jsonObject2.getString("SizeUnit").equalsIgnoreCase("null"))
                                totalArea.setText("N/A " + jsonObject2.getString("SizeUnit"));
                            else
                                totalArea.setText(jsonObject1.getString("TotalArea") + " " + jsonObject2.getString("SizeUnit"));
                        }else{
                            if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null"))
                                totalArea.setText("N/A");
                            else if(!jsonObject1.getString("TotalArea").equalsIgnoreCase("null"))
                                totalArea.setText(jsonObject1.getString("TotalArea") + " N/A");
                        }
                        /*JSONObject jsonObject2 = jsonObject1.getJSONObject("PropertySizeUnit");
                        if(jsonObject2.getString("SizeUnit").equalsIgnoreCase("null"))
                            totalAreaUnits.setText("N/A");
                        else
                            totalAreaUnits.setText(jsonObject2.getString("SizeUnit"));*/
                        if(jsonObject1.getString("TehsilName").equalsIgnoreCase("null"))
                            tehsilName.setText("N/A");
                        else
                            tehsilName.setText(jsonObject1.getString("TehsilName"));
                        if(jsonObject1.getString("LocalBodyType").equalsIgnoreCase("null"))
                            localbodytype.setText("N/A");
                        else
                            localbodytype.setText(jsonObject1.getString("LocalBodyType"));
                        if(jsonObject1.getString("LocalBodyName").equalsIgnoreCase("null"))
                            localBodyName.setText("N/A");
                        else
                            localBodyName.setText(jsonObject1.getString("LocalBodyName"));
                        if(jsonObject1.getString("DeveloperCompanyName").equalsIgnoreCase("null"))
                            developerComapnyName.setText("N/A");
                        else
                            developerComapnyName.setText(jsonObject1.getString("DeveloperCompanyName"));
                        if(jsonObject1.getString("AreaNegativeMark").equalsIgnoreCase("null"))
                            negativeMark.setText("N/A");
                        else
                            negativeMark.setText(jsonObject1.getString("AreaNegativeMark"));
                        if(jsonObject1.getString("ZipCode").equalsIgnoreCase("null"))
                            zipcode.setText("N/A");
                        else
                            zipcode.setText(jsonObject1.getString("ZipCode"));

                        JSONArray jsonArray2 = jsonObject1.getJSONArray("TownshipFiles");
                        /*ArrayList<TownshipFilesModel> townshipFilesModels = new ArrayList<>();
                        ArrayList<TownshipFilesModel> zoningFilesModels = new ArrayList<>();*/
                        townshipFilesModels.clear();
                        zoningFilesModels.clear();
                        if(jsonArray2.length()!=0){
                            for(int i = 0; i < jsonArray2.length(); i++){
                                JSONObject object = jsonArray2.getJSONObject(i);
                                TownshipFilesModel townshipFilesModel = new TownshipFilesModel();
                                townshipFilesModel.setCategory(object.getString("Category"));
                                JSONObject object1 = object.getJSONObject("File");
                                TownshipFilesModel.File file = new TownshipFilesModel.File();
                                file.setUrl(Urls.BASE_CONTACT_IMAGE_URL + object1.getString("Url"));
                                townshipFilesModel.setFile(file);
                                if(townshipFilesModel.getCategory().equalsIgnoreCase("Photos"))
                                    townshipFilesModels.add(townshipFilesModel);
                                if(townshipFilesModel.getCategory().equalsIgnoreCase("zoningPlan"))
                                    zoningFilesModels.add(townshipFilesModel);
                            }
                        }

                        imageAdapter = new TownshipImageAdapter(this, townshipFilesModels);
                        viewPager.setAdapter(imageAdapter);
                        imageAdapter.notifyDataSetChanged();

                        image_video_adapter = new TownshipFileAdapter(this, townshipFilesModels, this);
                        list_images_videos.setAdapter(image_video_adapter);
                        list_images_videos.setLayoutManager(new LinearLayoutManager(this,  LinearLayout.HORIZONTAL, false));
                        image_video_adapter.notifyDataSetChanged();

                        stateImageAdapter = new StateImageAdapter(this, zoningFilesModels, "");
                        gmada_photos.setAdapter(stateImageAdapter);
                        gmada_photos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                        stateImageAdapter.notifyDataSetChanged();

                        if(zoningFilesModels.size() == 0){
                            gmada_photos.setVisibility(View.GONE);
                            no_data2.setVisibility(View.VISIBLE);
                        }else{
                            gmada_photos.setVisibility(View.VISIBLE);
                            no_data2.setVisibility(View.GONE);
                        }

                        JSONObject jsonObject3 = jsonObject1.getJSONObject("Club");
                        if(jsonObject3.length() != 0){
                            clubLayout.setVisibility(View.VISIBLE);
                        }

                        if(jsonObject3.has("Name")) {
                            clubName.setVisibility(View.VISIBLE);
                            if(jsonObject3.getString("Name").equalsIgnoreCase("null"))
                                clubName.setText("N/A");
                            else
                                clubName.setText(jsonObject3.getString("Name"));
                        }
                        else
                            clubName.setVisibility(View.GONE);

                        if(jsonObject3.has("Description")){
                            clubDescription.setVisibility(View.VISIBLE);
                            if(jsonObject3.getString("Description").equalsIgnoreCase("null"))
                                clubDescription.setText("N/A");
                            else
                                clubDescription.setText(jsonObject3.getString("Description"));
                        }
                        else
                            clubDescription.setVisibility(View.GONE);

                        if((jsonObject3.has("Area") && jsonObject3.has("AreaSizeUnit")) ||
                                (jsonObject3.has("CoverAreaSize") && jsonObject3.has("CoverAreaSizeUnit")) ||
                                (jsonObject3.has("SuperAreaSize") && jsonObject3.has("SuperAreaSizeUnit")) ||
                                (jsonObject3.has("BuiltUpAreaSize") && jsonObject3.has("BuiltUpAreaSizeUnit")) ||
                                (jsonObject3.has("CarpetAreaSize") && jsonObject3.has("CarpetAreaSizeUnit"))){
                            areas.setVisibility(View.VISIBLE);
                        }
                        else
                            areas.setVisibility(View.GONE);

                        if((jsonObject3.has("Area") && jsonObject3.has("AreaSizeUnit"))) {
                            area.setVisibility(View.VISIBLE);
                            if(jsonObject3.getString("Area").equalsIgnoreCase("null") && jsonObject3.getString("AreaSizeUnit").equalsIgnoreCase("null"))
                                totalareaunits.setText("N/A");
                            else if(!jsonObject3.getString("Area").equalsIgnoreCase("null") && jsonObject3.getString("AreaSizeUnit").equalsIgnoreCase("null"))
                                totalareaunits.setText(jsonObject3.getString("Area") + " N/A");
                            else if(jsonObject3.getString("Area").equalsIgnoreCase("null") && !jsonObject3.getString("AreaSizeUnit").equalsIgnoreCase("null"))
                                totalareaunits.setText("N/A " + jsonObject3.getString("AreaSizeUnit"));
                            else
                                totalareaunits.setText(jsonObject3.getString("Area")+ " " + jsonObject3.getString("AreaSizeUnit"));
                        }
                        else
                            area.setVisibility(View.GONE);

                        if((jsonObject3.has("CoverAreaSize") && jsonObject3.has("CoverAreaSizeUnit"))) {
                            coverArea.setVisibility(View.VISIBLE);
                            if(jsonObject3.getString("CoverAreaSize").equalsIgnoreCase("null") && jsonObject3.getString("CoverAreaSizeUnit").equalsIgnoreCase("null"))
                                coverareaunits.setText("N/A");
                            else if(!jsonObject3.getString("CoverAreaSize").equalsIgnoreCase("null") && jsonObject3.getString("CoverAreaSizeUnit").equalsIgnoreCase("null"))
                                coverareaunits.setText(jsonObject3.getString("CoverAreaSize") + " N/A");
                            else if(jsonObject3.getString("CoverAreaSize").equalsIgnoreCase("null") && !jsonObject3.getString("CoverAreaSizeUnit").equalsIgnoreCase("null"))
                                coverareaunits.setText("N/A " + jsonObject3.getString("CoverAreaSizeUnit"));
                            else
                                coverareaunits.setText(jsonObject3.getString("CoverAreaSize") + " " + jsonObject3.getString("CoverAreaSizeUnit"));
                        }
                        else
                            coverArea.setVisibility(View.GONE);

                        if((jsonObject3.has("SuperAreaSize") && jsonObject3.has("SuperAreaSizeUnit"))) {
                            superArea.setVisibility(View.VISIBLE);
                            if(jsonObject3.getString("SuperAreaSize").equalsIgnoreCase("null") && jsonObject3.getString("SuperAreaSizeUnit").equalsIgnoreCase("null"))
                                superareaunits.setText("N/A");
                            else if(!jsonObject3.getString("SuperAreaSize").equalsIgnoreCase("null") && jsonObject3.getString("SuperAreaSizeUnit").equalsIgnoreCase("null"))
                                superareaunits.setText(jsonObject3.getString("SuperAreaSize") + " N/A");
                            else if(jsonObject3.getString("SuperAreaSize").equalsIgnoreCase("null") && !jsonObject3.getString("SuperAreaSizeUnit").equalsIgnoreCase("null"))
                                superareaunits.setText("N/A " + jsonObject3.getString("SuperAreaSizeUnit"));
                            else
                                superareaunits.setText(jsonObject3.getString("SuperAreaSize") + " " + jsonObject3.getString("SuperAreaSizeUnit"));
                        }
                        else
                            superArea.setVisibility(View.GONE);

                        if((jsonObject3.has("BuiltUpAreaSize") && jsonObject3.has("BuiltUpAreaSizeUnit"))) {
                            buildUpArea.setVisibility(View.VISIBLE);
                            if(jsonObject3.getString("BuiltUpAreaSize").equalsIgnoreCase("null") && jsonObject3.getString("BuiltUpAreaSizeUnit").equalsIgnoreCase("null"))
                                buildupareaunits.setText("N/A" + " ");
                            else if(!jsonObject3.getString("BuiltUpAreaSize").equalsIgnoreCase("null") && jsonObject3.getString("BuiltUpAreaSizeUnit").equalsIgnoreCase("null"))
                                buildupareaunits.setText(jsonObject3.getString("BuiltUpAreaSize") + " N/A");
                            else if(jsonObject3.getString("BuiltUpAreaSize").equalsIgnoreCase("null") && !jsonObject3.getString("BuiltUpAreaSizeUnit").equalsIgnoreCase("null"))
                                buildupareaunits.setText("N/A " + jsonObject3.getString("BuildUpAreaSizeUnit"));
                            else
                                buildupareaunits.setText(jsonObject3.getString("BuiltUpAreaSize") + " " + jsonObject3.getString("BuiltUpAreaSizeUnit"));
                        }
                        else
                            buildUpArea.setVisibility(View.GONE);

                        if((jsonObject3.has("CarpetAreaSize") && jsonObject3.has("CarpetAreaSizeUnit"))) {
                            carpetArea.setVisibility(View.VISIBLE);
                            if(jsonObject3.getString("CarpetAreaSize").equalsIgnoreCase("null") && jsonObject3.getString("CarpetAreaSizeUnit").equalsIgnoreCase("null"))
                                carpetareaunits.setText("N/A");
                            else if(!jsonObject3.getString("CarpetAreaSize").equalsIgnoreCase("null") && jsonObject3.getString("CarpetAreaSizeUnit").equalsIgnoreCase("null"))
                                carpetareaunits.setText(jsonObject3.getString("CarpetAreaSize") + " N/A");
                            else if(jsonObject3.getString("CarpetAreaSize").equalsIgnoreCase("null") && !jsonObject3.getString("CarpetAreaSizeUnit").equalsIgnoreCase("null"))
                                carpetareaunits.setText("N/A " + jsonObject3.getString("CarpetAreaSizeUnit"));
                            else
                                carpetareaunits.setText(jsonObject3.getString("CarpetAreaSize") + " " + jsonObject3.getString("CarpetAreaSizeUnit"));
                        }
                        else
                            carpetArea.setVisibility(View.GONE);

                        JSONArray jsonArray1 = jsonObject3.optJSONArray("ClubAmenities");
                        if(jsonArray1.length()!=0){
                            clubfacilities.setVisibility(View.VISIBLE);
                        }
                        else
                            clubfacilities.setVisibility(View.GONE);

                        ArrayList<ClubFacilities> clubFacilitiesArrayList = new ArrayList<>();
                        clubFacilitiesArrayList.clear();

                        for(int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject4 = jsonArray1.getJSONObject(i);
                            ClubFacilities clubFacilities = new ClubFacilities();

                            clubFacilities.setAmenityId(jsonObject4.getString("AmenityId"));
                            JSONObject jsonObject5 = jsonObject4.getJSONObject("Amenity");
                            ClubFacilities.Amenity amenity = new ClubFacilities.Amenity();
                            amenity.setValue(jsonObject5.getString("Value"));

                            clubFacilities.setAmenity(amenity);
                            clubFacilitiesArrayList.add(clubFacilities);
                        }

                        recreationalAdapter = new RecreationalAdapter(this, clubFacilitiesArrayList, "");
                        clubFac.setAdapter(recreationalAdapter);
                        clubFac.setLayoutManager(new GridLayoutManager(this, 2));
                        recreationalAdapter.notifyDataSetChanged();

                        if(clubFacilitiesArrayList.size() == 0){
                            clubFac.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                        }else{
                            clubFac.setVisibility(View.VISIBLE);
                            no_data.setVisibility(View.GONE);
                        }

                        JSONArray jsonArray3 = jsonObject3.optJSONArray("ClubFiles");
                        if(jsonArray3.length() != 0)
                            clubImages.setVisibility(View.VISIBLE);
                        else
                            clubImages.setVisibility(View.GONE);

                        ArrayList<TownshipFilesModel> clubImages = new ArrayList<>();
                        clubImages.clear();
                        if(jsonArray3.length()!=0){
                            for(int i = 0; i < jsonArray3.length(); i++){
                                JSONObject object = jsonArray3.getJSONObject(i);
                                TownshipFilesModel townshipFilesModel = new TownshipFilesModel();
                                townshipFilesModel.setCategory(object.getString("Category"));
                                JSONObject object1 = object.getJSONObject("File");
                                TownshipFilesModel.File file = new TownshipFilesModel.File();
                                file.setUrl(Urls.BASE_CONTACT_IMAGE_URL + object1.getString("Url"));
                                townshipFilesModel.setFile(file);
                                clubImages.add(townshipFilesModel);
                            }
                        }

                        stateImageAdapter = new StateImageAdapter(this, clubImages, "");
                        clubImg.setAdapter(stateImageAdapter);
                        clubImg.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                        stateImageAdapter.notifyDataSetChanged();

                        if(clubImages.size() == 0){
                            clubImg.setVisibility(View.GONE);
                            no_data1.setVisibility(View.VISIBLE);
                        }else{
                            clubImg.setVisibility(View.VISIBLE);
                            no_data1.setVisibility(View.GONE);
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void submitReqPopUp(String url){
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.submit_requirement_popup);
        close = (ImageView) dialog.findViewById(R.id.close);
        img = (ImageView) dialog.findViewById(R.id.img);
        Picasso.with(this).load(url).into(img);
        project_name = (TextView) dialog.findViewById(R.id.name);
        project_name.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Township", ""));
        description1 = (TextView) dialog.findViewById(R.id.description);
        description1.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Dist", "") + PreferenceConnector.getInstance(this).loadSavedPreferences("Stating", ""));
        yes = (RadioButton) dialog.findViewById(R.id.yes);
        no = (RadioButton) dialog.findViewById(R.id.no);
        buy = (RadioButton) dialog.findViewById(R.id.buy);
        rent = (RadioButton) dialog.findViewById(R.id.rent);
        property_detailing = (LinearLayout) dialog.findViewById(R.id.property_detailing);
        category_spinner = (Spinner) dialog.findViewById(R.id.category_spinner);
        property_spinner = (Spinner) dialog.findViewById(R.id.property_spinner);
        check_studio = (CheckBox) dialog.findViewById(R.id.check_studio);
        check_2bhk = (CheckBox) dialog.findViewById(R.id.check_2bhk);
        check_3bhk = (CheckBox) dialog.findViewById(R.id.check_3bhk);
        check_4bhk = (CheckBox) dialog.findViewById(R.id.check_4bhk);
        message = (EditText) dialog.findViewById(R.id.message);
        datePicker = (EditText) dialog.findViewById(R.id.datePicker);
        media = (EditText) dialog.findViewById(R.id.media);
        submit = (Button) dialog.findViewById(R.id.submit);

        yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    property_detailing.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    startActivity(new Intent(TownshipDetail.this, Submit_Requirement.class));
                }
            }
        });

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }

    private void propertySell_RentPopUp(String url) {
        dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.property_sell_rent);
        close1 = (ImageView) dialog1.findViewById(R.id.close);
        image = (ImageView) dialog1.findViewById(R.id.image);
        Picasso.with(this).load(url).into(image);
        project_name1 = (TextView) dialog1.findViewById(R.id.project_name);
        project_name1.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Township", ""));
        desc = (TextView) dialog1.findViewById(R.id.desc);
        desc.setText(PreferenceConnector.getInstance(this).loadSavedPreferences("Dist", "") + PreferenceConnector.getInstance(this).loadSavedPreferences("Stating", ""));
        yes1 = (RadioButton) dialog1.findViewById(R.id.yes);
        no1 = (RadioButton) dialog1.findViewById(R.id.no);
        sell = (RadioButton) dialog1.findViewById(R.id.sell);
        rent1 = (RadioButton) dialog1.findViewById(R.id.rent);
        property_detailing1 = (LinearLayout) dialog1.findViewById(R.id.property_detailing);
        category_spinner1 = (Spinner) dialog1.findViewById(R.id.category_spinner);
        property_spinner1 = (Spinner) dialog1.findViewById(R.id.property_spinner);
        tower_name_no = (EditText) dialog1.findViewById(R.id.tower_name_no);
        unit_no = (EditText) dialog1.findViewById(R.id.unit_no);
        message1 = (EditText) dialog1.findViewById(R.id.message1);
        datePick = (EditText) dialog1.findViewById(R.id.datePick);
        media1 = (EditText) dialog1.findViewById(R.id.media);
        submit1 = (Button) dialog1.findViewById(R.id.submit);

        yes1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    property_detailing1.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    //startActivity(new Intent(getContext(), Submit_Requirement.class));

                }
            }
        });

        dialog1.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog1.show();

        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.cancel();
            }
        });
    }

    private void mapPopup(ArrayList<TownshipFilesModel> pictures) {
        dialog = new Dialog(TownshipDetail.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.map_pop_up);
        close = (ImageView) dialog.findViewById(R.id.close);
        viewPager = (ViewPager) dialog.findViewById(R.id.map_images);
        imageAdapter = new TownshipImageAdapter(this, pictures);
        viewPager.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

}
