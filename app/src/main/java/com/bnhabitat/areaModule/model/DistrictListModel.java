package com.bnhabitat.areaModule.model;

import java.io.Serializable;
import java.util.ArrayList;

public class DistrictListModel implements Serializable {

    String Id;
    String District;
    String StateId;
    String TotalRecords;
    String Projects;
    String Townships;
    String Localities;
    String Properties;
    public ArrayList<Pictures> Pictures = new ArrayList<>();

    public DistrictListModel() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStateId() {
        return StateId;
    }

    public void setStateId(String stateId) {
        StateId = stateId;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        TotalRecords = totalRecords;
    }

    public String getProjects() {
        return Projects;
    }

    public void setProjects(String projects) {
        Projects = projects;
    }

    public String getTownships() {
        return Townships;
    }

    public void setTownships(String townships) {
        Townships = townships;
    }

    public String getLocalities() {
        return Localities;
    }

    public void setLocalities(String localities) {
        Localities = localities;
    }

    public String getProperties() {
        return Properties;
    }

    public void setProperties(String properties) {
        Properties = properties;
    }

    public ArrayList<Pictures> getPictures() {
        return Pictures;
    }

    public void setPictures(ArrayList<Pictures> pictures) {
        Pictures = pictures;
    }

    public static class Pictures{
        String Id;
        String Name;
        String OriginalFileName;
        String Url;
        String Type;

        public Pictures() {
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getOriginalFileName() {
            return OriginalFileName;
        }

        public void setOriginalFileName(String originalFileName) {
            OriginalFileName = originalFileName;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }
    }
}
