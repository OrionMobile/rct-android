package com.bnhabitat.areaModule.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ProjectListModel implements Serializable {

    String Id;
    String TownshipName;
    String Name;
    String LocalityName;
    String TownName;
    String ZipCode;
    String StateName;
    String TehsilName;
    String DistrictName;
    ArrayList<ProjectCategoryTypes> ProjectCategoryTypes = new ArrayList<>();
    ArrayList<ProjectFiles> ProjectFiles = new ArrayList<>();

    public ProjectListModel() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTownshipName() {
        return TownshipName;
    }

    public void setTownshipName(String townshipName) {
        TownshipName = townshipName;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLocalityName() {
        return LocalityName;
    }

    public void setLocalityName(String localityName) {
        LocalityName = localityName;
    }

    public String getTownName() {
        return TownName;
    }

    public void setTownName(String townName) {
        TownName = townName;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getTehsilName() {
        return TehsilName;
    }

    public void setTehsilName(String tehsilName) {
        TehsilName = tehsilName;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public ArrayList<ProjectCategoryTypes> getProjectCategoryTypes() {
        return ProjectCategoryTypes;
    }

    public void setProjectCategoryTypes(ArrayList<ProjectCategoryTypes> projectCategoryTypes) {
        ProjectCategoryTypes = projectCategoryTypes;
    }

    public ArrayList<ProjectFiles> getProjectFiles() {
        return ProjectFiles;
    }

    public void setProjectFiles(ArrayList<ProjectFiles> projectFiles) {
        ProjectFiles = projectFiles;
    }

    public static class ProjectCategoryTypes{
        String Type;

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }
    }

    public static class ProjectFiles{
        String Category;
        String FileUrl;

        public String getCategory() {
            return Category;
        }

        public void setCategory(String category) {
            Category = category;
        }

        public String getFileUrl() {
            return FileUrl;
        }

        public void setFileUrl(String fileUrl) {
            FileUrl = fileUrl;
        }
    }
}
