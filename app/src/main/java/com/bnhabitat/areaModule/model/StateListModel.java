package com.bnhabitat.areaModule.model;

import java.io.Serializable;
import java.util.ArrayList;

public class StateListModel implements Serializable {


    public StateListModel() {
    }

    String Id;
    String Name;
    String TotalRecords;
    String TotalCities;
    String DistrictCount;

    ArrayList<Pictures> Pictures = new ArrayList<>();

    public ArrayList<Pictures> getPictures() {
        return Pictures;
    }

    public void setPictures(ArrayList<Pictures> pictures) {
        Pictures = pictures;
    }

    String TotalProperties;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTotalCities() {
        return TotalCities;
    }

    public void setTotalCities(String totalCities) {
        TotalCities = totalCities;
    }

    public String getDistrictCount() {
        return DistrictCount;
    }

    public void setDistrictCount(String districtCount) {
        DistrictCount = districtCount;
    }

    public String getTotalProperties() {
        return TotalProperties;
    }

    public void setTotalProperties(String totalProperties) {
        TotalProperties = totalProperties;
    }

    public String getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        TotalRecords = totalRecords;
    }

    public static class Pictures{

        String Id;
        String Name;
        String OriginalFileName;
        String Url;
        String Type;

        public Pictures() {
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getOriginalFileName() {
            return OriginalFileName;
        }

        public void setOriginalFileName(String originalFileName) {
            OriginalFileName = originalFileName;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }
    }

}
