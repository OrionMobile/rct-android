package com.bnhabitat.areaModule.model;

import java.io.Serializable;

public class RecreationalModel implements Serializable {

    String Id, Key, Value;

    public RecreationalModel() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
