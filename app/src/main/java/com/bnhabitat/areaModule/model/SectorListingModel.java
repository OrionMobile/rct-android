package com.bnhabitat.areaModule.model;

import java.io.Serializable;

public class SectorListingModel implements Serializable {

    String Id, name, TotalRecords;

    public SectorListingModel() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        TotalRecords = totalRecords;
    }
}
