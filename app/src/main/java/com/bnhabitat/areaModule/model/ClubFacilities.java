package com.bnhabitat.areaModule.model;

import java.io.Serializable;

public class ClubFacilities implements Serializable {
    String AmenityId;
    public Amenity Amenity;

    public ClubFacilities() {
    }

    public String getAmenityId() {
        return AmenityId;
    }

    public void setAmenityId(String amenityId) {
        AmenityId = amenityId;
    }

    public Amenity getAmenity() {
        return Amenity;
    }

    public void setAmenity(Amenity amenity) {
        Amenity = amenity;
    }

    public static class Amenity{
        String Type;
        String Title;
        String Value;

        public Amenity() {
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getValue() {
            return Value;
        }

        public void setValue(String value) {
            Value = value;
        }
    }
}
