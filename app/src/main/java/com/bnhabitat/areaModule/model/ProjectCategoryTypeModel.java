package com.bnhabitat.areaModule.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ProjectCategoryTypeModel implements Serializable {

    String Id, CategoryType, CategoryTypeId, Type;
    ArrayList<Accomodations> Accomodations = new ArrayList<>();
    ArrayList<Towers> Towers = new ArrayList<>();

    public ProjectCategoryTypeModel() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCategoryType() {
        return CategoryType;
    }

    public void setCategoryType(String categoryType) {
        CategoryType = categoryType;
    }

    public String getCategoryTypeId() {
        return CategoryTypeId;
    }

    public void setCategoryTypeId(String categoryTypeId) {
        CategoryTypeId = categoryTypeId;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public ArrayList<Accomodations> getAccomodations() {
        return Accomodations;
    }

    public void setAccomodations(ArrayList<Accomodations> accomodations) {
        Accomodations = accomodations;
    }

    public ArrayList<Towers> getTowers() {
        return Towers;
    }

    public void setTowers(ArrayList<Towers> towers) {
        Towers = towers;
    }

    public static class Accomodations implements Serializable{
        String Title, Bedrooms, Floors;
        ArrayList<AccommodationFloors> AccommodationFloors = new ArrayList<>();
        ArrayList<AccommodationRooms> AccommodationRooms = new ArrayList<>();

        public Accomodations() {
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getBedrooms() {
            return Bedrooms;
        }

        public void setBedrooms(String bedrooms) {
            Bedrooms = bedrooms;
        }

        public String getFloors() {
            return Floors;
        }

        public void setFloors(String floors) {
            Floors = floors;
        }

        public ArrayList<AccommodationFloors> getAccommodationFloors() {
            return AccommodationFloors;
        }

        public void setAccommodationFloors(ArrayList<AccommodationFloors> accommodationFloors) {
            AccommodationFloors = accommodationFloors;
        }

        public ArrayList<AccommodationRooms> getAccommodationRooms() {
            return AccommodationRooms;
        }

        public void setAccommodationRooms(ArrayList<AccommodationRooms> accommodationRooms) {
            AccommodationRooms = accommodationRooms;
        }

        public static class AccommodationFloors implements Serializable{
            String FloorNumber;

            public AccommodationFloors() {
            }

            public String getFloorNumber() {
                return FloorNumber;
            }

            public void setFloorNumber(String floorNumber) {
                FloorNumber = floorNumber;
            }
        }

        public static class AccommodationRooms implements Serializable{
            String BedRoomName, Category;

            public AccommodationRooms() {
            }

            public String getBedRoomName() {
                return BedRoomName;
            }

            public void setBedRoomName(String bedRoomName) {
                BedRoomName = bedRoomName;
            }

            public String getCategory() {
                return Category;
            }

            public void setCategory(String category) {
                Category = category;
            }
        }
    }

    public static class Towers implements Serializable{
        String Name, TotalFloor, TotalPassengerLift, TotalCargoLift, TotalStaircase, TotalBasements;
        ProjectCategoryType ProjectCategoryType;
        ArrayList<ProjectFloors> ProjectFloors = new ArrayList<>();
        ArrayList<TowerAmenities> TowerAmenities = new ArrayList<>();
        ArrayList<TowerBasementDetails> TowerBasementDetails = new ArrayList<>();

        public Towers() {
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getTotalFloor() {
            return TotalFloor;
        }

        public void setTotalFloor(String totalFloor) {
            TotalFloor = totalFloor;
        }

        public String getTotalPassengerLift() {
            return TotalPassengerLift;
        }

        public void setTotalPassengerLift(String totalPassengerLift) {
            TotalPassengerLift = totalPassengerLift;
        }

        public String getTotalCargoLift() {
            return TotalCargoLift;
        }

        public void setTotalCargoLift(String totalCargoLift) {
            TotalCargoLift = totalCargoLift;
        }

        public String getTotalStaircase() {
            return TotalStaircase;
        }

        public void setTotalStaircase(String totalStaircase) {
            TotalStaircase = totalStaircase;
        }

        public String getTotalBasements() {
            return TotalBasements;
        }

        public void setTotalBasements(String totalBasements) {
            TotalBasements = totalBasements;
        }

        public ProjectCategoryType getProjectCategoryType() {
            return ProjectCategoryType;
        }

        public void setProjectCategoryType(ProjectCategoryType projectCategoryType) {
            ProjectCategoryType = projectCategoryType;
        }

        public ArrayList<ProjectFloors> getProjectFloors() {
            return ProjectFloors;
        }

        public void setProjectFloors(ArrayList<ProjectFloors> projectFloors) {
            ProjectFloors = projectFloors;
        }

        public ArrayList<TowerAmenities> getTowerAmenities() {
            return TowerAmenities;
        }

        public void setTowerAmenities(ArrayList<TowerAmenities> towerAmenities) {
            TowerAmenities = towerAmenities;
        }

        public ArrayList<TowerBasementDetails> getTowerBasementDetails() {
            return TowerBasementDetails;
        }

        public void setTowerBasementDetails(ArrayList<TowerBasementDetails> towerBasementDetails) {
            TowerBasementDetails = towerBasementDetails;
        }

        public static class ProjectCategoryType implements Serializable{
            String BasementType;

            public ProjectCategoryType() {
            }

            public String getBasementType() {
                return BasementType;
            }

            public void setBasementType(String basementType) {
                BasementType = basementType;
            }
        }

        public static class ProjectFloors implements Serializable{
            String Name;

            public ProjectFloors() {
            }

            public String getName() {
                return Name;
            }

            public void setName(String name) {
                Name = name;
            }
        }

        public static class TowerAmenities implements Serializable{
            String Id, AmenityId;
            Amenity Amenity;

            public TowerAmenities() {
            }

            public String getId() {
                return Id;
            }

            public void setId(String id) {
                Id = id;
            }

            public String getAmenityId() {
                return AmenityId;
            }

            public void setAmenityId(String amenityId) {
                AmenityId = amenityId;
            }

            public Amenity getAmenity() {
                return Amenity;
            }

            public void setAmenity(Amenity amenity) {
                Amenity = amenity;
            }

            public static class Amenity implements Serializable{
                String Id, Type, Title, Value;

                public Amenity() {
                }

                public String getId() {
                    return Id;
                }

                public void setId(String id) {
                    Id = id;
                }

                public String getType() {
                    return Type;
                }

                public void setType(String type) {
                    Type = type;
                }

                public String getTitle() {
                    return Title;
                }

                public void setTitle(String title) {
                    Title = title;
                }

                public String getValue() {
                    return Value;
                }

                public void setValue(String value) {
                    Value = value;
                }
            }
        }

        public static class TowerBasementDetails implements Serializable{
            String Id, Title, Value;

            public TowerBasementDetails() {
            }

            public String getId() {
                return Id;
            }

            public void setId(String id) {
                Id = id;
            }

            public String getTitle() {
                return Title;
            }

            public void setTitle(String title) {
                Title = title;
            }

            public String getValue() {
                return Value;
            }

            public void setValue(String value) {
                Value = value;
            }
        }
    }
}
