package com.bnhabitat.areaModule.model;

import java.io.Serializable;

public class ProjectAmenityModel implements Serializable {

    String Id, ProjectId, AmenityId;
    Amenity Amenity;

    public ProjectAmenityModel() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getProjectId() {
        return ProjectId;
    }

    public void setProjectId(String projectId) {
        ProjectId = projectId;
    }

    public String getAmenityId() {
        return AmenityId;
    }

    public void setAmenityId(String amenityId) {
        AmenityId = amenityId;
    }

    public Amenity getAmenity() {
        return Amenity;
    }

    public void setAmenity(Amenity amenity) {
        Amenity = amenity;
    }

    public static class Amenity{

        String Id, Type, Title, Value, Size, SizeUnitId, SizeUnit, PropertySizeUnit;

        public Amenity() {
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getValue() {
            return Value;
        }

        public void setValue(String value) {
            Value = value;
        }

        public String getSize() {
            return Size;
        }

        public void setSize(String size) {
            Size = size;
        }

        public String getSizeUnitId() {
            return SizeUnitId;
        }

        public void setSizeUnitId(String sizeUnitId) {
            SizeUnitId = sizeUnitId;
        }

        public String getSizeUnit() {
            return SizeUnit;
        }

        public void setSizeUnit(String sizeUnit) {
            SizeUnit = sizeUnit;
        }

        public String getPropertySizeUnit() {
            return PropertySizeUnit;
        }

        public void setPropertySizeUnit(String propertySizeUnit) {
            PropertySizeUnit = propertySizeUnit;
        }
    }
}
