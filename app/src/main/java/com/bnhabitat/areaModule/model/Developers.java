package com.bnhabitat.areaModule.model;

import java.io.Serializable;

public class Developers implements Serializable {
    String Id;
    String Name;
    String LogoUrl;

    public Developers() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLogoUrl() {
        return LogoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        LogoUrl = logoUrl;
    }
}
