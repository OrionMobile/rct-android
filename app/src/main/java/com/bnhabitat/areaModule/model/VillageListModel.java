package com.bnhabitat.areaModule.model;

import java.io.Serializable;
import java.util.ArrayList;

public class VillageListModel implements Serializable {

    String Id;
    String StateId;
    String DistrictId;
    String Name;
    String VillageType;
    public ArrayList<Pictures> Pictures = new ArrayList<>();
    String TotalRecords;
    String Projects;
    String Townships;
    String Localities;
    String Properties;

    public VillageListModel() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStateId() {
        return StateId;
    }

    public void setStateId(String stateId) {
        StateId = stateId;
    }

    public String getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(String districtId) {
        DistrictId = districtId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getVillageType() {
        return VillageType;
    }

    public void setVillageType(String villageType) {
        VillageType = villageType;
    }

    public ArrayList<Pictures> getPictures() {
        return Pictures;
    }

    public void setPictures(ArrayList<Pictures> pictures) {
        Pictures = pictures;
    }

    public String getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        TotalRecords = totalRecords;
    }

    public String getProjects() {
        return Projects;
    }

    public void setProjects(String projects) {
        Projects = projects;
    }

    public String getTownships() {
        return Townships;
    }

    public void setTownships(String townships) {
        Townships = townships;
    }

    public String getLocalities() {
        return Localities;
    }

    public void setLocalities(String localities) {
        Localities = localities;
    }

    public String getProperties() {
        return Properties;
    }

    public void setProperties(String properties) {
        Properties = properties;
    }

    public static class Pictures{
        String Id;
        String Name;
        String OriginalFileName;
        String Url;
        String Type;

        public Pictures() {
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getOriginalFileName() {
            return OriginalFileName;
        }

        public void setOriginalFileName(String originalFileName) {
            OriginalFileName = originalFileName;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }
    }

}

