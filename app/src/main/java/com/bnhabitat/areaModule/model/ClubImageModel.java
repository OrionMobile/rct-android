package com.bnhabitat.areaModule.model;

import java.io.Serializable;

public class ClubImageModel implements Serializable {

    String Id, Category, FileUrl;

    public ClubImageModel() {

    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getFileUrl() {
        return FileUrl;
    }

    public void setFileUrl(String fileUrl) {
        FileUrl = fileUrl;
    }
}
