package com.bnhabitat.areaModule.model;

import java.io.Serializable;
import java.util.ArrayList;

public class CityListModel implements Serializable {

    String Id;
    String City;
    String StateId;
    String TotalRecords;
    ArrayList<Projects> Projects = new ArrayList<>();
    String length;
    String Townships;
    String Localities;
    String Properties;
    String DistrictId;
    public ArrayList<Pictures> Pictures = new ArrayList<>();

    public CityListModel() {
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getStateId() {
        return StateId;
    }

    public void setStateId(String stateId) {
        StateId = stateId;
    }

    public String getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        TotalRecords = totalRecords;
    }

    public ArrayList<Projects> getProjects() {
        return Projects;
    }

    public void setProjects(ArrayList<Projects> projects) {
        Projects = projects;
    }

    public String getTownships() {
        return Townships;
    }

    public void setTownships(String townships) {
        Townships = townships;
    }

    public String getLocalities() {
        return Localities;
    }

    public void setLocalities(String localities) {
        Localities = localities;
    }

    public String getProperties() {
        return Properties;
    }

    public void setProperties(String properties) {
        Properties = properties;
    }

    public String getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(String districtId) {
        DistrictId = districtId;
    }

    public ArrayList<Pictures> getPictures() {
        return Pictures;
    }

    public void setPictures(ArrayList<Pictures> pictures) {
        Pictures = pictures;
    }

    public static class Projects{

    }

    public static class Pictures{
        String Id;
        String Name;
        String OriginalFileName;
        String Url;
        String Type;

        public Pictures() {
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getOriginalFileName() {
            return OriginalFileName;
        }

        public void setOriginalFileName(String originalFileName) {
            OriginalFileName = originalFileName;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }
    }


}
