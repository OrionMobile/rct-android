package com.bnhabitat.areaModule.model;

import java.io.Serializable;

public class TownshipFilesModel implements Serializable {

    String Category;
    public File File;

    public TownshipFilesModel() {
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public File getFile() {
        return File;
    }

    public void setFile(File file) {
        File = file;
    }

    public static class File{
        String Url;

        public File() {
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }
    }
}
