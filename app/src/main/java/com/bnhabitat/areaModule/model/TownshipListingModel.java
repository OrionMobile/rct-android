package com.bnhabitat.areaModule.model;

import java.io.Serializable;
import java.util.ArrayList;

public class TownshipListingModel implements Serializable {

    String Id;
    String Name;
    ArrayList<Projects> Projects = new ArrayList<>();
    ArrayList<TownshipFiles> TownshipFiles = new ArrayList<>();


    public TownshipListingModel() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public ArrayList<Projects> getProjects() {
        return Projects;
    }

    public void setProjects(ArrayList<Projects> projects) {
        Projects = projects;
    }

    public ArrayList<TownshipFiles> getTownshipFiles() {
        return TownshipFiles;
    }

    public void setTownshipFiles(ArrayList<TownshipFiles> townshipFiles) {
        TownshipFiles = townshipFiles;
    }

    public static class Projects{
        String Name;
        ArrayList<ProjectCategoryTypes> ProjectCategoryTypes = new ArrayList<>();
        ArrayList<ProjectFiles> ProjectFiles = new ArrayList<>();

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public ArrayList<ProjectCategoryTypes> getProjectCategoryTypes() {
            return ProjectCategoryTypes;
        }

        public void setProjectCategoryTypes(ArrayList<ProjectCategoryTypes> projectCategoryTypes) {
            ProjectCategoryTypes = projectCategoryTypes;
        }

        public ArrayList<TownshipListingModel.Projects.ProjectFiles> getProjectFiles() {
            return ProjectFiles;
        }

        public void setProjectFiles(ArrayList<TownshipListingModel.Projects.ProjectFiles> projectFiles) {
            ProjectFiles = projectFiles;
        }

        public static class ProjectCategoryTypes{
            String Type;

            public String getType() {
                return Type;
            }

            public void setType(String type) {
                Type = type;
            }
        }

        public static class ProjectFiles{
            String Category;
            String FileUrl;

            public String getCategory() {
                return Category;
            }

            public void setCategory(String category) {
                Category = category;
            }

            public String getFileUrl() {
                return FileUrl;
            }

            public void setFileUrl(String fileUrl) {
                FileUrl = fileUrl;
            }
        }
    }

    public static class TownshipFiles{

        String Category;
        public Files Files;

        public String getCategory() {
            return Category;
        }

        public void setCategory(String category) {
            Category = category;
        }

        public Files getFiles() {
            return Files;
        }

        public void setFiles(Files files) {
            Files = files;
        }

        public static class Files{
            String Id;
            String Url;

            public String getId() {
                return Id;
            }

            public void setId(String id) {
                Id = id;
            }

            public String getUrl() {
                return Url;
            }

            public void setUrl(String url) {
                Url = url;
            }
        }
    }
}
