package com.bnhabitat.areaModule.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Departments implements Serializable {

    String Id, Name, LogoUrl, Website, Industry;
    ArrayList<Addresses> Addresses = new ArrayList<>();

    public Departments() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLogoUrl() {
        return LogoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        LogoUrl = logoUrl;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public String getIndustry() {
        return Industry;
    }

    public void setIndustry(String industry) {
        Industry = industry;
    }

    public ArrayList<Addresses> getAddresses() {
        return Addresses;
    }

    public void setAddresses(ArrayList<Addresses> addresses) {
        Addresses = addresses;
    }

    public static class Addresses{

        String ProjectName, UnitName, TowerName, SubAreaName, TownName, LocalityName, TownshipName, VillageName, TehsilName, ZipCode, GooglePlusCode, DistrictName, StateName, CountryName;

        public Addresses() {
        }

        public String getProjectName() {
            return ProjectName;
        }

        public void setProjectName(String projectName) {
            ProjectName = projectName;
        }

        public String getUnitName() {
            return UnitName;
        }

        public void setUnitName(String unitName) {
            UnitName = unitName;
        }

        public String getTowerName() {
            return TowerName;
        }

        public void setTowerName(String towerName) {
            TowerName = towerName;
        }

        public String getSubAreaName() {
            return SubAreaName;
        }

        public void setSubAreaName(String subAreaName) {
            SubAreaName = subAreaName;
        }

        public String getTownName() {
            return TownName;
        }

        public void setTownName(String townName) {
            TownName = townName;
        }

        public String getLocalityName() {
            return LocalityName;
        }

        public void setLocalityName(String localityName) {
            LocalityName = localityName;
        }

        public String getTownshipName() {
            return TownshipName;
        }

        public void setTownshipName(String townshipName) {
            TownshipName = townshipName;
        }

        public String getVillageName() {
            return VillageName;
        }

        public void setVillageName(String villageName) {
            VillageName = villageName;
        }

        public String getTehsilName() {
            return TehsilName;
        }

        public void setTehsilName(String tehsilName) {
            TehsilName = tehsilName;
        }

        public String getZipCode() {
            return ZipCode;
        }

        public void setZipCode(String zipCode) {
            ZipCode = zipCode;
        }

        public String getGooglePlusCode() {
            return GooglePlusCode;
        }

        public void setGooglePlusCode(String googlePlusCode) {
            GooglePlusCode = googlePlusCode;
        }

        public String getDistrictName() {
            return DistrictName;
        }

        public void setDistrictName(String districtName) {
            DistrictName = districtName;
        }

        public String getStateName() {
            return StateName;
        }

        public void setStateName(String stateName) {
            StateName = stateName;
        }

        public String getCountryName() {
            return CountryName;
        }

        public void setCountryName(String countryName) {
            CountryName = countryName;
        }
    }
}
