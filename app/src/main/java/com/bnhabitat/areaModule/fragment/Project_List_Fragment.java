package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;

import java.util.ArrayList;

public class Project_List_Fragment extends Fragment implements TabChangeListener {

    View view;
    TabLayout.Tab Tab1, Tab2;
    TabLayout tabLayout;

    String id, stateId, districtId, projectCount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.project_list_fragment, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);

        id=getArguments().getString("CityId");
        districtId=getArguments().getString("DistrictId");
        stateId=getArguments().getString("StateId");
        projectCount=getArguments().getString("projectCount");

        Tab1 = tabLayout.newTab().setText(projectCount + " Projects");
        Tab2 = tabLayout.newTab().setText("About City");
        Tab1.select();
        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Bundle bundle = new Bundle();
        bundle.putString("CityId", id);
        bundle.putString("DistrictId", districtId);
        bundle.putString("StateId", stateId);

        Project_Listing_Fragment project_listing_fragment = new Project_Listing_Fragment();
        project_listing_fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_project_listing, project_listing_fragment).commit();

        tabLayout.getTabAt(0);
        Tab1.select();
        bindWidgetsWithAnEvent();

        return view;
    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        Bundle bundle = new Bundle();
        bundle.putString("CityId", id);
        bundle.putString("DistrictId", districtId);
        bundle.putString("StateId", stateId);
        switch (tabPosition) {
            case 0:
                Project_Listing_Fragment project_listing_fragment = new Project_Listing_Fragment();
                project_listing_fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_project_listing, project_listing_fragment).commit();

                break;
            case 1:

                break;
        }
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
