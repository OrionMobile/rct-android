package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.BedroomAdapter;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.InventoryModel_Area;

import java.util.ArrayList;

public class AccomodationView extends Fragment implements TabChangeListener {

    View view;
    ArrayList<InventoryModel_Area> property_data;
    ArrayList<InventoryModel_Area.PropertyBedrooms> propertyBedroomsList=new ArrayList<>();
    ArrayList<InventoryModel_Area.PropertyBedrooms> propertyBedroomsList1=new ArrayList<>();
    ArrayList<InventoryModel_Area.PropertyBedrooms> propertyBedroomsList2=new ArrayList<>();
    ArrayList<InventoryModel_Area.PropertyAccommodationDetails> accommodationDetails = new ArrayList<>();
    ArrayList<InventoryModel_Area.PropertyAccommodationDetails> accommodationDetails1 = new ArrayList<>();
    ArrayList<InventoryModel_Area.PropertyAccommodationDetails> accommodationDetails2= new ArrayList<>();
    RelativeLayout rltv_hdr;

    LinearLayout bedrooms, accomodation;
    RecyclerView room_list, balcony_list, parking_list, features_list;
    TextView room, balcony, parking, featues;

    BedroomAdapter bedroomAdapter;

    public AccomodationView() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view=inflater.inflate(R.layout.accomodation_view_fragment, container, false);

        bedrooms = (LinearLayout) view.findViewById(R.id.bedrooms);
        accomodation = (LinearLayout) view.findViewById(R.id.accomodation);

        room_list=(RecyclerView)view.findViewById(R.id.room_list);
        balcony_list=(RecyclerView)view.findViewById(R.id.balcony_list);
        parking_list=(RecyclerView)view.findViewById(R.id.parking_list);
        features_list=(RecyclerView)view.findViewById(R.id.features_list);

        room = (TextView) view.findViewById(R.id.room);
        balcony = (TextView) view.findViewById(R.id.balcony);
        parking = (TextView) view.findViewById(R.id.parking);
        featues = (TextView) view.findViewById(R.id.featues);
        rltv_hdr = (RelativeLayout) view.findViewById(R.id.rltv_hdr);

        try {
            Bundle bundle = this.getArguments();
            if (bundle != null)
                property_data = (ArrayList<InventoryModel_Area>) bundle.getSerializable("property_data");

            if(property_data.size() == 0){
                rltv_hdr.setVisibility(View.VISIBLE);
            }

            if(property_data.size() != 0){
                propertyBedroomsList = property_data.get(0).getPropertyBedroomses();
                accommodationDetails = property_data.get(0).getPropertyAccommodationDetailses();
            }

            if(propertyBedroomsList.size() == 0)
                bedrooms.setVisibility(View.GONE);
            else
                bedrooms.setVisibility(View.VISIBLE);

            if(accommodationDetails.size() == 0)
                accomodation.setVisibility(View.GONE);
            else
                accomodation.setVisibility(View.VISIBLE);

            propertyBedroomsList1.clear();
            propertyBedroomsList2.clear();
            accommodationDetails1.clear();
            accommodationDetails2.clear();

            for(int i=0;i<propertyBedroomsList.size();i++) {
                try {

                    if(propertyBedroomsList.get(i).getCategory().equalsIgnoreCase("room")){
                        InventoryModel_Area.PropertyBedrooms propertyBedrooms = new InventoryModel_Area().new PropertyBedrooms();
                        propertyBedrooms.setBedRoomName(propertyBedroomsList.get(i).getBedRoomName());
                        propertyBedrooms.setBedRoomType(propertyBedroomsList.get(i).getBedRoomType());
                        propertyBedrooms.setFloorNo(propertyBedroomsList.get(i).getFloorNo());
                        propertyBedroomsList1.add(propertyBedrooms);
                    }

                    if(propertyBedroomsList.get(i).getCategory().equalsIgnoreCase("balcony")){
                        InventoryModel_Area.PropertyBedrooms propertyBedrooms = new InventoryModel_Area().new PropertyBedrooms();
                        propertyBedrooms.setBedRoomName(propertyBedroomsList.get(i).getBedRoomName());
                        propertyBedrooms.setBedRoomType(propertyBedroomsList.get(i).getBedRoomType());
                        propertyBedrooms.setFloorNo(propertyBedroomsList.get(i).getFloorNo());
                        propertyBedroomsList2.add(propertyBedrooms);
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            for(int i = 0; i < accommodationDetails.size(); i++){
                try{
                    if(accommodationDetails.get(i).getType().equalsIgnoreCase("parking")){
                        InventoryModel_Area.PropertyAccommodationDetails accommodationDetail = new InventoryModel_Area().new PropertyAccommodationDetails();
                        accommodationDetail.setName(accommodationDetails.get(i).getName());
                        accommodationDetail.setType(accommodationDetails.get(i).getType());
                        accommodationDetails1.add(accommodationDetail);
                    }

                    if(accommodationDetails.get(i).getType().equalsIgnoreCase("feature")){
                        InventoryModel_Area.PropertyAccommodationDetails accommodationDetail = new InventoryModel_Area().new PropertyAccommodationDetails();
                        accommodationDetail.setName(accommodationDetails.get(i).getName());
                        accommodationDetail.setType(accommodationDetails.get(i).getType());
                        accommodationDetails2.add(accommodationDetail);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(propertyBedroomsList1.size() == 0){
            room.setVisibility(View.GONE);
            room_list.setVisibility(View.GONE);
        }
        else{
            bedroomAdapter = new BedroomAdapter(getActivity(), propertyBedroomsList1);
            room_list.setLayoutManager(new LinearLayoutManager(getActivity()));
            room_list.setAdapter(bedroomAdapter);
            bedroomAdapter.notifyDataSetChanged();
        }

        if(propertyBedroomsList1.size() == 0){
            balcony.setVisibility(View.GONE);
            balcony_list.setVisibility(View.GONE);
        }

        else{
            bedroomAdapter = new BedroomAdapter(getActivity(), propertyBedroomsList2);
            balcony_list.setLayoutManager(new LinearLayoutManager(getActivity()));
            balcony_list.setAdapter(bedroomAdapter);
            bedroomAdapter.notifyDataSetChanged();
        }

        if(accommodationDetails1.size() == 0){
            parking.setVisibility(View.GONE);
            parking_list.setVisibility(View.GONE);
        }

        else{
            bedroomAdapter = new BedroomAdapter(getActivity(), accommodationDetails1, "");
            parking_list.setLayoutManager(new LinearLayoutManager(getActivity()));
            parking_list.setAdapter(bedroomAdapter);
            bedroomAdapter.notifyDataSetChanged();
        }

        if(accommodationDetails2.size() == 0){
            featues.setVisibility(View.GONE);
            features_list.setVisibility(View.GONE);
        }

        else{
            bedroomAdapter = new BedroomAdapter(getActivity(), accommodationDetails2, "");
            features_list.setLayoutManager(new LinearLayoutManager(getActivity()));
            features_list.setAdapter(bedroomAdapter);
            bedroomAdapter.notifyDataSetChanged();
        }

        return view;
    }


    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
