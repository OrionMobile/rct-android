package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.SubareaListingAdapter;
import com.bnhabitat.areaModule.adapter.TownshipListingAdapter;
import com.bnhabitat.areaModule.model.SectorListingModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Subarea_Listing_Fragment extends Fragment implements TabChangeListener,  CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener  {

    View view;
    RecyclerView rvSubareaList;
    ArrayList<SectorListingModel> subareaListingArrayList = new ArrayList<>();
    SubareaListingAdapter subareaListingAdapter;
    LinearLayoutManager linearLayoutManager;

    String stateId, districtId, cityId;
    TextView more_states, no_data;

    int totalRecords;
    private int pageSize = 10;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.subarea_listing_fragment, container, false);
        rvSubareaList = (RecyclerView) view.findViewById(R.id.rvSubareaList);
        more_states = (TextView) view.findViewById(R.id.more_states);
        no_data = (TextView) view.findViewById(R.id.no_data);
        linearLayoutManager = new LinearLayoutManager(getActivity());

        stateId = getArguments().getString("StateId");
        districtId = getArguments().getString("DistrictId");
        cityId = getArguments().getString("CityId");

        //sectorListingApi();

        more_states.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pageSize = pageSize + 10;

                sectorListingApi();
            }
        });

        subareaListingAdapter = new SubareaListingAdapter(getActivity(), subareaListingArrayList);
        rvSubareaList.setLayoutManager(linearLayoutManager);
        rvSubareaList.setAdapter(subareaListingAdapter);
        return view;
    }

    @Override
    public void onResume() {
        sectorListingApi();
        super.onResume();
    }

    public void sectorListingApi(){
        new CommonSyncwithoutstatus(getActivity(),
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_SECTOR_LIST +
                        pageSize + "/" + stateId + "/" + districtId + "/0/0/" + cityId + "/0/0",
                "",
                "Loading...",this,
                Urls.URL_SECTOR_LIST,
                Constants.GET)
                .execute();

    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    @Override
    public void onResultListener(String result, String which) {

        if(null !=result && !result.equalsIgnoreCase("")){
            if(which.equalsIgnoreCase(Urls.URL_SECTOR_LIST)){
                try{
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.optJSONArray("Result");
                    subareaListingArrayList.clear();

                    if(jsonArray.length()!=0) {
                        for (int index = 0; index < jsonArray.length(); index++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(index);

                            SectorListingModel sectorListingModel = new SectorListingModel();
                            sectorListingModel.setId(jsonObject1.getString("Id"));
                            sectorListingModel.setName(jsonObject1.getString("Name"));
                            sectorListingModel.setTotalRecords(jsonObject1.getString("TotalRecords"));
                            totalRecords = jsonObject1.getInt("TotalRecords");
                            subareaListingArrayList.add(sectorListingModel);

                        }

                        subareaListingAdapter = new SubareaListingAdapter(getActivity(), subareaListingArrayList);
                        rvSubareaList.setLayoutManager(linearLayoutManager);
                        rvSubareaList.setAdapter(subareaListingAdapter);
                        subareaListingAdapter.notifyDataSetChanged();
                        try {
                            if (totalRecords <= subareaListingArrayList.size() + 1)
                                more_states.setVisibility(View.GONE);
                            else
                                more_states.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        rvSubareaList.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public  int getSubarea()
    {
        int my_list = 0;
        if(subareaListingArrayList!=null)
        {

            my_list = subareaListingArrayList.size();
        }
        else
        {
            Toast.makeText(getContext(), "No data Found", Toast.LENGTH_SHORT);
        }
        return my_list;
    }
}
