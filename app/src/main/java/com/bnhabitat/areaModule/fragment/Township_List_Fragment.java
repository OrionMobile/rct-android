package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.interfaces.TabChangeListener;

import java.util.ArrayList;

public class Township_List_Fragment extends Fragment implements TabChangeListener {

    View view;
    static TabLayout.Tab Tab1, Tab2;
    TabLayout tabLayout;
    AboutCityFragment aboutCityFragment;
    String id, name, townshipCount;
    int size;
    Township_Listing_Fragment township_listing_fragment = new Township_Listing_Fragment();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.township_list_fragent, container, false);

        id = getArguments().getString("CityId");
        name = getArguments().getString("CityName");
        townshipCount = getArguments().getString("townshipCount");

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        Tab1 = tabLayout.newTab().setText(townshipCount + " Township");
        Tab2 = tabLayout.newTab().setText("About " + name);
        Tab1.select();
        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Bundle bundle = new Bundle();
        bundle.putString("CityId", id);
        Township_Listing_Fragment township_listing_fragment = new Township_Listing_Fragment();
        township_listing_fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_township_listing, township_listing_fragment).commit();
        tabLayout.getTabAt(0);
        Tab1.select();
        bindWidgetsWithAnEvent();

        return view;
    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        final ImageView iv_location = (ImageView) getActivity().getWindow().findViewById(R.id.iv_location);
        Bundle bundle = new Bundle();
        bundle.putString("CityId", id);
        switch (tabPosition) {
            case 0:
                Township_Listing_Fragment township_listing_fragment = new Township_Listing_Fragment();
                iv_location.setVisibility(View.GONE);
                township_listing_fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_township_listing, township_listing_fragment).commit();

                break;
            case 1:
                aboutCityFragment = new AboutCityFragment();
                iv_location.setVisibility(View.VISIBLE);
                aboutCityFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_township_listing, aboutCityFragment).commit();

                break;
        }
    }

    public  ArrayList<StateListModel.Pictures> getCityArrayList()
    {
        ArrayList<StateListModel.Pictures> my_list = new ArrayList<>();
        if(aboutCityFragment!=null)
        {

            my_list = aboutCityFragment.getCityArrayList();
        }
        else
        {
            Toast.makeText(getContext(), "No data Found", Toast.LENGTH_SHORT);
        }
        return my_list;
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
