package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.DistrictListingAdapter;
import com.bnhabitat.areaModule.model.DistrictListModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DistrictList extends Fragment implements TabChangeListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener {

    RecyclerView rvlist;
    TextView more_states, no_data;

    ArrayList<DistrictListModel> districtListModelArrayList = new ArrayList<>();

    int totalRecords;
    private int pageSize = 10;

    DistrictListingAdapter districtListingAdapter;
    View view;
    String Id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.district_list, container, false);

        initialization(view);

        Id = getArguments().getString("Id");

        //onAreaDistrictList();

        more_states.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pageSize = pageSize + 10;

                onAreaDistrictList();
            }
        });

        /*districtListingAdapter = new DistrictListingAdapter(getContext(), districtListModelArrayList);
        rvlist.setAdapter(districtListingAdapter);
        rvlist.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        districtListingAdapter.notifyDataSetChanged();*/

        return view;
    }

    @Override
    public void onResume() {
        onAreaDistrictList();
        super.onResume();
    }

    private void initialization(View view) {
        rvlist= (RecyclerView) view.findViewById(R.id.rvlist);
        more_states = (TextView) view.findViewById(R.id.more_states);
        no_data = (TextView) view.findViewById(R.id.no_data);
    }

    private void onAreaDistrictList() {

        new CommonSyncwithoutstatus(getActivity(),
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_DISTRICT_LIST +
                        pageSize + "/" + Id + "/0",
                "",
                "Loading...",this,
                Urls.URL_DISTRICT_LIST,
                Constants.GET)
                .execute();

    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_DISTRICT_LIST)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    JSONArray stateArray = jsonObject.getJSONArray("Result");
                    districtListModelArrayList.clear();

                    if(stateArray.length()!=0) {

                        for (int index = 0; index < stateArray.length(); index++) {
                            JSONObject jsonObject1 = stateArray.getJSONObject(index);

                            DistrictListModel districtListModel = new DistrictListModel();
                            districtListModel.setId(jsonObject1.getString("Id"));
                            districtListModel.setStateId(jsonObject1.getString("StateId"));
                            districtListModel.setDistrict(jsonObject1.getString("District"));
                            districtListModel.setProjects(jsonObject1.getString("Projects"));
                            districtListModel.setTownships(jsonObject1.getString("Townships"));
                            districtListModel.setLocalities(jsonObject1.getString("Localities"));
                            districtListModel.setProperties(jsonObject1.getString("Properties"));
                            JSONArray jsonArray = jsonObject1.optJSONArray("Pictures");
                            ArrayList<DistrictListModel.Pictures> picturesArrayList = new ArrayList<>();

                            picturesArrayList.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                DistrictListModel.Pictures pictures = new DistrictListModel.Pictures();
                                if (jsonObject2.length() > 0) {
                                    pictures.setId(jsonObject2.getString("Id"));
                                    pictures.setUrl(Urls.BASE_CONTACT_IMAGE_URL + jsonObject2.getString("Url"));
                                    picturesArrayList.add(pictures);
                                } else {
                                    pictures.setId("");
                                    pictures.setUrl("");
                                    picturesArrayList.add(pictures);
                                }
                            }
                            districtListModel.setPictures(picturesArrayList);
                            districtListModel.setTotalRecords(jsonObject1.getString("TotalRecords"));

                            totalRecords = jsonObject1.getInt("TotalRecords");

                            districtListModelArrayList.add(districtListModel);

                        }
                        districtListingAdapter = new DistrictListingAdapter(getContext(), districtListModelArrayList);
                        rvlist.setAdapter(districtListingAdapter);
                        rvlist.setLayoutManager(new GridLayoutManager(getContext(), 2));
                        districtListingAdapter.notifyDataSetChanged();

                        try {
                            if (totalRecords <= districtListModelArrayList.size() + 1)
                                more_states.setVisibility(View.GONE);
                            else
                                more_states.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        rvlist.setVisibility(View.GONE);
                        more_states.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
