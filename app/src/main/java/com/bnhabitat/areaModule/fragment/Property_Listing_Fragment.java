package com.bnhabitat.areaModule.fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.PropertyListingAdapter;
import com.bnhabitat.areaModule.areaActivities.PropertyDetail;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel_Area;
import com.bnhabitat.ui.adapters.PropertyType_adapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Property_Listing_Fragment extends Fragment implements TabChangeListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener    {
    View view;
    LinearLayout search_lay, search_filter_lay;
    TextView searchbar, title_name, title_name2, view_all, view_all2, txtNoResults, count1, count2;
    RelativeLayout view_lay, view_lay2;
    RecyclerView for_list, for_list_1;
    String id, stateId, districtId;
    ArrayList<InventoryModel_Area> foryouArrayList = new ArrayList<>();
    ArrayList<InventoryModel_Area> foryouArrayList2 = new ArrayList<>();
    String view_data = "";
    String view_data1 = "";
    LinearLayoutManager layoutManager;
    LinearLayoutManager layoutManager1;
    Dialog dialog;
    Button search_btn;
    Button residential, commercial, house, plot, apartment, decBedroom, decBathroom, incBedroom, incBathroom;
    TextView bed_text, bath_text;
    EditText edtTextSearch;
    CrystalRangeSeekbar rangeSeekbar;
    TextView txtMinValue, txtMaxValue;
    RecyclerView recyclerView;
    static int flagBedroom = 0;
    static int flagBathroom = 0;
    String from = "";
    static String strMin = "10000";
    static String strMax = "10000000";

    static String strText = "";
    static String strBath = "";
    static String strBed = "";
    static  String propertyType = "Residential";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.property_listing_fragment, container, false);

        id = getArguments().getString("CityId");
        districtId=getArguments().getString("DistrictId");
        stateId=getArguments().getString("StateId");
        initialize(view);
        //onPropertyListing();

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        for_list.setLayoutManager(layoutManager);
        for_list_1.setLayoutManager(layoutManager1);

        view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_data = "visible";
                view_lay.setVisibility(View.GONE);
                layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                for_list.setLayoutManager(layoutManager);
                for_list.setAdapter(new PropertyListingAdapter(getActivity(), foryouArrayList, view_data));
            }
        });

        view_all2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_data1 = "visible";
                view_lay2.setVisibility(View.GONE);
                layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                for_list_1.setLayoutManager(layoutManager1);
                for_list_1.setAdapter(new PropertyListingAdapter(getActivity(), foryouArrayList2, view_data1));
            }
        });

        search_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                residential.setBackground(getResources().getDrawable(R.drawable.one_sided_round_btn));
                commercial.setBackground(getResources().getDrawable(R.drawable.right_side_round_btn));

                recyclerView.setVisibility(View.GONE);
                flagBedroom=0;
                flagBathroom=0;
                bed_text.setText("0");
                bath_text.setText("0");


                rangeSeekbar.setMinStartValue(10000).setMaxStartValue(10000000).apply();


                txtMinValue.setText("Min \u20B9 " + "10000");
                txtMaxValue.setText("Max \u20B9 " + "10000000");

                search_filter_lay.setVisibility(View.VISIBLE);

            }
        });

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strText = "PropertyLocations/any(data:data/ZipCode%20eq%20'" + edtTextSearch.getText().toString() +
                        "')%20or%20PropertyLocations/any(data:data/Developer%20eq%20'" + edtTextSearch.getText().toString() +
                        "')%20or%20PropertyLocations/any(data:data/Project%20eq%20'" + edtTextSearch.getText().toString() +
                        "')%20or%20PropertyLocations/any(data:data/State%20eq%20'" + edtTextSearch.getText().toString() +
                        "')%20or%20PropertyLocations/any(data:data/District%20eq%20'" + edtTextSearch.getText().toString() +
                        "')%20or%20PropertyLocations/any(data:data/SubDivision%20eq%20'" + edtTextSearch.getText().toString() +
                        "')%20or%20PropertyLocations/any(data:data/SubArea%20eq%20'" + edtTextSearch.getText().toString() +
                        "')%20or%20PropertyLocations/any(data:data/TowerName%20eq%20'" + edtTextSearch.getText().toString() + "')%20and%20";

                if (edtTextSearch.getText().toString().equalsIgnoreCase("")) {
                    getSearchList("");
                } else {
                    getSearchList(strText);
                }


                edtTextSearch.setText("");

            }
        });
        residential.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                residential.setBackground(getResources().getDrawable(R.drawable.left_side_orange));
                commercial.setBackground(getResources().getDrawable(R.drawable.right_side_round_btn));

                propertyType ="Residential";
                from = "PropertyType/Type%20eq%20'Residential'%20or%20";
                onGetPropertyType("Residential");
            }
        });

        commercial.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                commercial.setBackground(getResources().getDrawable(R.drawable.right_side_orange));
                residential.setBackground(getResources().getDrawable(R.drawable.one_sided_round_btn));

                propertyType ="Commercial";
                from = "PropertyType/Type%20eq%20'Commercial'%20or%20";
                onGetPropertyType("Commercial");

            }
        });

        apartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apartment.setBackground(getResources().getDrawable(R.drawable.left_side_orange));
                house.setBackground(getResources().getDrawable(R.drawable.btn_with_border));
                plot.setBackground(getResources().getDrawable(R.drawable.right_side_round_btn));
            }
        });

        house.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                house.setBackgroundColor(getResources().getColor(R.color.orange));
                apartment.setBackground(getResources().getDrawable(R.drawable.one_sided_round_btn));
                plot.setBackground(getResources().getDrawable(R.drawable.right_side_round_btn));
            }
        });

        plot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plot.setBackground(getResources().getDrawable(R.drawable.right_side_orange));
                apartment.setBackground(getResources().getDrawable(R.drawable.one_sided_round_btn));
                house.setBackground(getResources().getDrawable(R.drawable.btn_with_border));

            }
        });

        decBedroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBedroom > 0)
                    flagBedroom--;
                strBed = "TotalRooms%20eq%20" + flagBedroom + "%20and%20";
                bed_text.setText(String.valueOf(flagBedroom));

            }
        });

        incBedroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBedroom++;
                strBed = "TotalRooms%20eq%20" + flagBedroom + "%20and%20";
                bed_text.setText(String.valueOf(flagBedroom));
            }
        });


        decBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagBathroom > 0)
                    flagBathroom--;
                strBath = "TotalBathrooms%20eq%20" + flagBathroom + "%20and%20";
                bath_text.setText(String.valueOf(flagBathroom));

            }
        });

        incBathroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flagBathroom++;
                strBath = "TotalBathrooms%20eq%20" + flagBathroom + "%20and%20";
                bath_text.setText(String.valueOf(flagBathroom));
            }
        });

        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {

                strMin = String.valueOf(minValue);
                strMax = String.valueOf(maxValue);

                txtMinValue.setText("Min \u20B9 " + String.valueOf(minValue));
                txtMaxValue.setText("Max \u20B9 " + String.valueOf(maxValue));

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        onPropertyListing();
        super.onResume();
    }

    private void initialize(View view) {
        search_lay = (LinearLayout) view.findViewById(R.id.search_lay);
        view_lay = (RelativeLayout) view.findViewById(R.id.view_lay);
        view_lay2 = (RelativeLayout) view.findViewById(R.id.view_lay2);
        for_list = (RecyclerView) view.findViewById(R.id.for_list);
        for_list_1 = (RecyclerView) view.findViewById(R.id.for_list_1);
        searchbar = (TextView) view.findViewById(R.id.searchbar);
        title_name = (TextView) view.findViewById(R.id.title_name);
        title_name2 = (TextView) view.findViewById(R.id.title_name2);
        view_all = (TextView) view.findViewById(R.id.view_all);
        view_all2 = (TextView) view.findViewById(R.id.view_all2);
        count1 = (TextView) view.findViewById(R.id.count1);
        count2 = (TextView) view.findViewById(R.id.count2);
        txtNoResults = (TextView) view.findViewById(R.id.txtNoResults);
        search_filter_lay = (LinearLayout) view.findViewById(R.id.search_filter_lay);
        search_btn = (Button) view.findViewById(R.id.search_btn);
        residential = (Button) view.findViewById(R.id.residential_btn);
        commercial = (Button) view.findViewById(R.id.commercial_btn);
        house = (Button) view.findViewById(R.id.house_btn);
        plot = (Button) view.findViewById(R.id.plot_btn);
        apartment = (Button) view.findViewById(R.id.apartment_btn);
        incBedroom = (Button) view.findViewById(R.id.incBedroom);
        decBedroom = (Button) view.findViewById(R.id.decBedroom);
        bed_text = (TextView) view.findViewById(R.id.bed_text);
        bath_text = (TextView) view.findViewById(R.id.bath_text);
        incBathroom = (Button) view.findViewById(R.id.incBathroom);
        decBathroom = (Button) view.findViewById(R.id.decBathroom);
        edtTextSearch = (EditText) view.findViewById(R.id.edtTextSearch);
        rangeSeekbar = (CrystalRangeSeekbar) view.findViewById(R.id.rangeSeekbar);
        txtMinValue = (TextView) view.findViewById(R.id.txtMinValue);
        txtMaxValue = (TextView) view.findViewById(R.id.txtMaxValue);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
    }

    private void getSearchList(String text) {

        Log.e("type_id", "getSearchList: "+PropertyType_adapter.type_id );
        new CommonSyncwithoutstatus(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_LISTING + id + "?$filter=" + text + strBath + strBed + from + PropertyType_adapter.type_id + "PropertyFinancials/any(data:data/DemandPrice%20ge%20" + strMin + ")%20and%20PropertyFinancials/any(data:data/DemandPrice%20le%20" + strMax + ")",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_LISTING,
                Constants.GET).execute();

    }

    private void onGetPropertyType(String text) {

//      String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonSyncwithoutstatus(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_TYPE + text,
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_TYPE,
                Constants.GET).execute();


    }

    private void onPropertyListing() {

        new CommonSyncwithoutstatus(getActivity(),
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_PROPERTY_LISTING +
                        id,
                "",
                "Loading...",this,
                Urls.URL_PROPERTY_LISTING,
                Constants.GET)
                .execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if(null != result && !result.equalsIgnoreCase("")){
            if(which.equalsIgnoreCase(Urls.URL_PROPERTY_LISTING)){
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    if(jsonArray.length() == 0){
                        Utils.showErrorMessage("No results found", getActivity());
                        foryouArrayList.clear();
                        txtNoResults.setVisibility(View.VISIBLE);
                        for_list.setVisibility(View.GONE);
                        for_list_1.setVisibility(View.GONE);
                        view_all.setVisibility(View.GONE);
                        view_all2.setVisibility(View.GONE);
                        title_name.setText("Residential");
                        title_name2.setText("Commercial");
                        count1.setText("0");
                        count2.setText("0");

                        for_list.setAdapter(new PropertyListingAdapter(getActivity(), foryouArrayList, view_data));
                        for_list_1.setAdapter(new PropertyListingAdapter(getActivity(), foryouArrayList2, view_data1));
                        try {
                            search_filter_lay.setVisibility(View.GONE);
                        } catch (Exception e) {

                        }
                    } else{

                        foryouArrayList.clear();
                        foryouArrayList2.clear();

                        txtNoResults.setVisibility(View.GONE);
                        for_list.setVisibility(View.VISIBLE);
                        for_list_1.setVisibility(View.VISIBLE);
                        view_all.setVisibility(View.VISIBLE);
                        view_all2.setVisibility(View.VISIBLE);

                        for (int index = 0; index < jsonArray.length(); index++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(index);

                            InventoryModel_Area inventoryModels = new InventoryModel_Area();

                            inventoryModels.setId(jsonObject1.getString("Id"));
                            inventoryModels.setPropertyTypeId(jsonObject1.getString("PropertyTypeId"));
                            inventoryModels.setCompanyId(jsonObject1.getString("CompanyId"));
                            inventoryModels.setCam(jsonObject1.getString("Cam"));
                            inventoryModels.setCreatedById(jsonObject1.getString("CreatedById"));

                            JSONArray jsonArray1 = jsonObject1.getJSONArray("PropertyLocations");
                            ArrayList<InventoryModel_Area.PropertyLocation> propertyLocations = new ArrayList<>();
                            for (int i = 0; i < jsonArray1.length(); i++) {
                                InventoryModel_Area.PropertyLocation propertyLocation = inventoryModels.new PropertyLocation();
                                JSONObject jsonObjectPropertyLocations = jsonArray1.getJSONObject(i);
                                propertyLocation.setId(jsonObjectPropertyLocations.getString("Id"));
                                propertyLocation.setPropertyId(jsonObjectPropertyLocations.getString("PropertyId"));
                                propertyLocation.setDeveloper(jsonObjectPropertyLocations.getString("Developer"));
                                propertyLocation.setDeveloperLogo(Urls.BASE_CONTACT_IMAGE_URL + jsonObjectPropertyLocations.getString("DeveloperLogo"));
                                propertyLocation.setProject(jsonObjectPropertyLocations.getString("Project"));
                                propertyLocation.setZipCode(jsonObjectPropertyLocations.getString("ZipCode"));
                                propertyLocation.setState(jsonObjectPropertyLocations.getString("State"));
                                propertyLocation.setDistrict(jsonObjectPropertyLocations.getString("District"));
                                propertyLocation.setTownName(jsonObjectPropertyLocations.getString("TownName"));
                                propertyLocation.setSector(jsonObjectPropertyLocations.getString("Sector"));
                                propertyLocation.setSubArea(jsonObjectPropertyLocations.getString("SubArea"));
                                propertyLocation.setUnitName(jsonObjectPropertyLocations.getString("UnitNo"));
                                propertyLocation.setTowerName(jsonObjectPropertyLocations.getString("TowerName"));
                                propertyLocation.setFloorNo(jsonObjectPropertyLocations.getString("FloorNo"));
                                propertyLocation.setGooglePlusCode(jsonObjectPropertyLocations.getString("GooglePlusCode"));
                                propertyLocations.add(propertyLocation);


                            }
                            JSONArray jsonArrayPropertyAreas = jsonObject1.getJSONArray("PropertyAreas");
                            ArrayList<InventoryModel_Area.PropertyArea> propertyAreas = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyAreas.length(); k++) {
                                JSONObject jsonObjectPropertyLocations = jsonArrayPropertyAreas.getJSONObject(k);
                                InventoryModel_Area.PropertyArea propertyArea = inventoryModels.new PropertyArea();
                                propertyArea.setId(jsonObjectPropertyLocations.getString("Id"));
                                propertyArea.setPropertyId(jsonObjectPropertyLocations.getString("PropertyId"));
                                propertyArea.setPlotShape(jsonObjectPropertyLocations.getString("PlotShape"));
                                propertyArea.setPlotArea(jsonObjectPropertyLocations.getString("PlotArea"));
                                propertyArea.setPlotAreaUnitId(jsonObjectPropertyLocations.getString("PlotAreaUnitId"));
                                propertyArea.setFrontSize(jsonObjectPropertyLocations.getString("FrontSize"));
                                propertyArea.setFrontSizeUnitId(jsonObjectPropertyLocations.getString("FrontSizeUnitId"));
                                propertyArea.setDepthSize(jsonObjectPropertyLocations.getString("DepthSize"));
                                propertyArea.setDepthSizeUnitId(jsonObjectPropertyLocations.getString("DepthSizeUnitId"));
                                propertyArea.setStreetOrRoadType(jsonObjectPropertyLocations.getString("StreetOrRoadType"));
                                propertyArea.setRoadWidth(jsonObjectPropertyLocations.getString("RoadWidth"));
                                propertyArea.setRoadWidthUnitId(jsonObjectPropertyLocations.getString("RoadWidthUnitId"));
                                propertyArea.setEnteranceDoorFacing(jsonObjectPropertyLocations.getString("EnteranceDoorFacing"));
                                propertyArea.setParkingInFront(jsonObjectPropertyLocations.getString("ParkingInFront"));
                                propertyArea.setHaveWalkingPath(jsonObjectPropertyLocations.getString("HaveWalkingPath"));
                                propertyArea.setPlotOrCoverArea(jsonObjectPropertyLocations.getString("PlotOrCoverArea"));
                                propertyArea.setPlotOrCoverAreaUnitId(jsonObjectPropertyLocations.getString("PlotOrCoverAreaUnitId"));
                                propertyArea.setSuperArea(jsonObjectPropertyLocations.getString("SuperArea"));
                                propertyArea.setSuperAreaUnitId(jsonObjectPropertyLocations.getString("SuperAreaUnitId"));
                                propertyArea.setBuiltUpArea(jsonObjectPropertyLocations.getString("BuiltUpArea"));
                                propertyArea.setBuiltUpAreaUnitId(jsonObjectPropertyLocations.getString("BuiltUpAreaUnitId"));
                                propertyArea.setCarpetArea(jsonObjectPropertyLocations.getString("CarpetArea"));
                                propertyArea.setCarpetAreaUnitId(jsonObjectPropertyLocations.getString("CarpetAreaUnitId"));
                                if (!jsonObjectPropertyLocations.isNull("PropertySizeUnit4")) {
                                    JSONObject jsonObjecPropertySizeUnit4 = jsonObjectPropertyLocations.getJSONObject("PropertySizeUnit4");
                                    propertyArea.setPropertySizeUnit4(jsonObjecPropertySizeUnit4.getString("SizeUnit"));
                                }
                                propertyAreas.add(propertyArea);

                            }
                            JSONArray jsonArrayPropertyPlcs = jsonObject1.getJSONArray("PropertyPlcs");
                            ArrayList<InventoryModel_Area.PropertyPlc> propertyPlcs = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyPlcs.length(); k++) {
                                InventoryModel_Area.PropertyPlc propertyPlc = inventoryModels.new PropertyPlc();
                                JSONObject jsonObjectPropertyPlcs = jsonArrayPropertyPlcs.getJSONObject(k);
                                propertyPlc.setId(jsonObjectPropertyPlcs.getString("Id"));
                                propertyPlc.setPropertyId(jsonObjectPropertyPlcs.getString("PropertyId"));
                                propertyPlc.setName(jsonObjectPropertyPlcs.getString("Name"));
                                propertyPlcs.add(propertyPlc);
                            }

                            JSONArray jsonArrayPropertyImages = jsonObject1.getJSONArray("PropertyImages");
                            ArrayList<InventoryModel_Area.PropertyImage> propertyImages = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyImages.length(); k++) {
                                InventoryModel_Area.PropertyImage propertyImage = inventoryModels.new PropertyImage();
                                JSONObject jsonObjectPropertyImages = jsonArrayPropertyImages.getJSONObject(k);
                                propertyImage.setId(jsonObjectPropertyImages.getString("Id"));
                                propertyImage.setName(jsonObjectPropertyImages.getString("Name"));
                                propertyImage.setType(jsonObjectPropertyImages.getString("Type"));
                                propertyImage.setUrl(Urls.BASE_CONTACT_IMAGE_URL + jsonObjectPropertyImages.getString("Url"));
                                propertyImage.setImageCode(jsonObjectPropertyImages.getString("ImageCode"));
                                propertyImages.add(propertyImage);
                            }

                            JSONArray jsonArrayPropertySides = jsonObject1.getJSONArray("PropertySides");
                            ArrayList<InventoryModel_Area.PropertySides> propertySides = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertySides.length(); k++) {
                                InventoryModel_Area.PropertySides propertySides1 = inventoryModels.new PropertySides();
                                JSONObject jsonObjectPropertySides = jsonArrayPropertySides.getJSONObject(k);
                                propertySides1.setId(jsonObjectPropertySides.getString("Id"));
                                propertySides1.setName(jsonObjectPropertySides.getString("Name"));
                                propertySides1.setLength(jsonObjectPropertySides.getString("Length"));
                                propertySides1.setLengthUnitId(jsonObjectPropertySides.getString("LengthUnitId"));
                                propertySides1.setWidth(jsonObjectPropertySides.getString("Width"));
                                propertySides1.setWidthUnitId(jsonObjectPropertySides.getString("WidthUnitId"));
                                propertySides.add(propertySides1);
                            }

                            JSONArray jsonArrayPropertyFinancials = jsonObject1.getJSONArray("PropertyFinancials");
                            ArrayList<InventoryModel_Area.PropertyFinancials> propertyFinancialses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyFinancials.length(); k++) {
                                InventoryModel_Area.PropertyFinancials propertyFinancials = inventoryModels.new PropertyFinancials();
                                JSONObject jsonObjectPropertyFinancials = jsonArrayPropertyFinancials.getJSONObject(k);
                                propertyFinancials.setId(jsonObjectPropertyFinancials.getString("Id"));
                                propertyFinancials.setDemandPrice(jsonObjectPropertyFinancials.getString("DemandPrice"));
                                propertyFinancials.setInclusivePrice(jsonObjectPropertyFinancials.getString("InclusivePrice"));
                                propertyFinancials.setIsNegociable(jsonObjectPropertyFinancials.getString("IsNegociable"));
                                propertyFinancials.setPrice(jsonObjectPropertyFinancials.getString("Price"));
                                propertyFinancials.setPriceUnitId(jsonObjectPropertyFinancials.getString("PriceUnitId"));
                                propertyFinancialses.add(propertyFinancials);

                            }

                            JSONArray jsonArrayPropertyLoans = jsonObject1.getJSONArray("PropertyLoans");
                            ArrayList<InventoryModel_Area.PropertyLoans> propertyLoanses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyLoans.length(); k++) {
                                InventoryModel_Area.PropertyLoans propertyLoans = inventoryModels.new PropertyLoans();
                                JSONObject jsonObjectPropertyLoans = jsonArrayPropertyLoans.getJSONObject(k);
                                propertyLoans.setId(jsonObjectPropertyLoans.getString("Id"));
                                propertyLoans.setLoanAmount(jsonObjectPropertyLoans.getString("LoanAmount"));
                                propertyLoans.setLoanTenure(jsonObjectPropertyLoans.getString("LoanTenure"));
                                propertyLoans.setFullDisbursed(jsonObjectPropertyLoans.getString("FullDisbursed"));
                                propertyLoans.setLastInstallmentPaidOn(jsonObjectPropertyLoans.getString("LastInstallmentPaidOn"));
                                propertyLoans.setTotalOutstandingAfterLastInstallment(jsonObjectPropertyLoans.getString("TotalOutstandingAfterLastInstallment"));
                                propertyLoanses.add(propertyLoans);

                            }

                            JSONArray jsonArrayPropertyOtherExpences = jsonObject1.getJSONArray("PropertyOtherExpences");
                            ArrayList<InventoryModel_Area.PropertyOtherExpences> propertyOtherExpences = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOtherExpences.length(); k++) {
                                InventoryModel_Area.PropertyOtherExpences propertyOtherExpences1 = inventoryModels.new PropertyOtherExpences();
                                JSONObject jsonObjectPropertyOtherExpences = jsonArrayPropertyOtherExpences.getJSONObject(k);
                                propertyOtherExpences1.setId(jsonObjectPropertyOtherExpences.getString("Id"));
                                propertyOtherExpences1.setTitle(jsonObjectPropertyOtherExpences.getString("Title"));
                                propertyOtherExpences1.setValue(jsonObjectPropertyOtherExpences.getString("Value"));
                                propertyOtherExpences1.setLastPaidDate(jsonObjectPropertyOtherExpences.getString("LastPaidDate"));
                                propertyOtherExpences.add(propertyOtherExpences1);

                            }
                            JSONArray jsonArrayPropertyLegalStatus = jsonObject1.getJSONArray("PropertyLegalStatus");
                            ArrayList<InventoryModel_Area.PropertyLegalStatus> propertyLegalStatuses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyLegalStatus.length(); k++) {
                                InventoryModel_Area.PropertyLegalStatus propertyLegalStatus = inventoryModels.new PropertyLegalStatus();
                                JSONObject jsonObjectPropertyOwners = jsonArrayPropertyLegalStatus.getJSONObject(k);
                                propertyLegalStatus.setId(jsonObjectPropertyOwners.getString("Id"));
                                propertyLegalStatus.setPropertyId(jsonObjectPropertyOwners.getString("PropertyId"));
                                propertyLegalStatus.setTransferRestriction(jsonObjectPropertyOwners.getString("TransferRestriction"));
                                propertyLegalStatus.setOwnerShipStatus(jsonObjectPropertyOwners.getString("OwnerShipStatus"));
                                propertyLegalStatus.setOwnerShipType(jsonObjectPropertyOwners.getString("OwnerShipType"));
                                propertyLegalStatus.setTransferByWayOf(jsonObjectPropertyOwners.getString("TransferByWayOf"));
                                propertyLegalStatus.setOwnedBy(jsonObjectPropertyOwners.getString("OwnedBy"));
                                propertyLegalStatus.setPossessionDate(jsonObjectPropertyOwners.getString("PossessionDate"));
                                propertyLegalStatus.setDateFrom(jsonObjectPropertyOwners.getString("DateFrom"));
                                propertyLegalStatus.setNoOfTennure(jsonObjectPropertyOwners.getString("NoOfTennure"));
                                propertyLegalStatus.setLeaseAlignmentPaid(jsonObjectPropertyOwners.getString("LeaseAlignmentPaid"));
                                propertyLegalStatus.setLastPaidDate(jsonObjectPropertyOwners.getString("LastPaidDate"));
                                propertyLegalStatus.setLeaseAmountPayable(jsonObjectPropertyOwners.getString("LeaseAmountPayable"));
                                propertyLegalStatus.setPaymentCircle(jsonObjectPropertyOwners.getString("PaymentCircle"));
                                propertyLegalStatuses.add(propertyLegalStatus);

                            }
                            JSONArray jsonArrayPropertyOwners = jsonObject1.getJSONArray("PropertyOwners");
                            ArrayList<InventoryModel_Area.PropertyOwners> propertyOwnerses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOwners.length(); k++) {
                                InventoryModel_Area.PropertyOwners propertyOwners = inventoryModels.new PropertyOwners();
                                JSONObject jsonObjectPropertyOwners = jsonArrayPropertyOwners.getJSONObject(k);
                                propertyOwners.setId(jsonObjectPropertyOwners.getString("Id"));
                                propertyOwners.setContactId(jsonObjectPropertyOwners.getString("ContactId"));
                                propertyOwners.setPanNo(jsonObjectPropertyOwners.getString("PanNo"));
                                propertyOwners.setAdhaarCardNo(jsonObjectPropertyOwners.getString("AdhaarCardNo"));
                                propertyOwners.setAddress(jsonObjectPropertyOwners.getString("Address"));
                                propertyOwners.setFirstName(jsonObjectPropertyOwners.getString("FirstName"));
                                propertyOwners.setLastName(jsonObjectPropertyOwners.getString("LastName"));
                                propertyOwners.setEmail(jsonObjectPropertyOwners.getString("Email"));
                                propertyOwners.setPhoneNumber(jsonObjectPropertyOwners.getString("PhoneNumber"));
                                propertyOwners.setOccupation(jsonObjectPropertyOwners.getString("Occupation"));
                                propertyOwners.setCompanyName(jsonObjectPropertyOwners.getString("CompanyName"));
                                propertyOwners.setCompanyType(jsonObjectPropertyOwners.getString("CompanyType"));
                                propertyOwnerses.add(propertyOwners);

                            }

                            JSONArray jsonArrayPropertyOtherquestions = jsonObject1.getJSONArray("PropertyOtherQuestions");
                            ArrayList<InventoryModel_Area.PropertyOtherQuestions> propertyOtherQuestionses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOtherquestions.length(); k++) {
                                InventoryModel_Area.PropertyOtherQuestions propertyOtherQuestions = inventoryModels.new PropertyOtherQuestions();
                                JSONObject jsonObjectPropertyOtherQuestions = jsonArrayPropertyOtherquestions.getJSONObject(k);
                                propertyOtherQuestions.setId(jsonObjectPropertyOtherQuestions.getString("Id"));
                                propertyOtherQuestions.setContactId(jsonObjectPropertyOtherQuestions.getString("ContactId"));
                                propertyOtherQuestions.setRelation(jsonObjectPropertyOtherQuestions.getString("Relation"));
                                propertyOtherQuestions.setSubmitOfBehalfOf(jsonObjectPropertyOtherQuestions.getString("SubmitOfBehalfOf"));
                                propertyOtherQuestions.setAgentId(jsonObjectPropertyOtherQuestions.getString("AgentId"));
                                propertyOtherQuestions.setFirstName(jsonObjectPropertyOtherQuestions.getString("FirstName"));
                                propertyOtherQuestions.setLastName(jsonObjectPropertyOtherQuestions.getString("LastName"));
                                propertyOtherQuestions.setEmail(jsonObjectPropertyOtherQuestions.getString("Email"));
                                propertyOtherQuestionses.add(propertyOtherQuestions);

                            }
                            ArrayList<InventoryModel_Area.PropertyAccommodationDetails> propertyAccommodationDetails = new ArrayList<>();
                            if (!jsonObject1.isNull("PropertyAccommodationDetails")) {
                                JSONArray jsonArrayPropertyAccommodationDetails = jsonObject1.getJSONArray("PropertyAccommodationDetails");

                                for (int k = 0; k < jsonArrayPropertyAccommodationDetails.length(); k++) {
                                    InventoryModel_Area.PropertyAccommodationDetails propertyAccommodationDetails1 = inventoryModels.new PropertyAccommodationDetails();
                                    JSONObject jsonObjectPropertyAccommodationDetails = jsonArrayPropertyAccommodationDetails.getJSONObject(k);
                                    propertyAccommodationDetails1.setId(jsonObjectPropertyAccommodationDetails.getString("Id"));
                                    propertyAccommodationDetails1.setName(jsonObjectPropertyAccommodationDetails.getString("Name"));
                                    propertyAccommodationDetails1.setType(jsonObjectPropertyAccommodationDetails.getString("Type"));
                                    propertyAccommodationDetails1.setTotalCount(jsonObjectPropertyAccommodationDetails.getString("TotalCount"));

                                    propertyAccommodationDetails.add(propertyAccommodationDetails1);

                                }
                            }
                            JSONArray jsonArrayPropertyBedrooms = jsonObject1.getJSONArray("PropertyBedrooms");
                            ArrayList<InventoryModel_Area.PropertyBedrooms> propertyBedroomses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyBedrooms.length(); k++) {
                                InventoryModel_Area.PropertyBedrooms propertyBedrooms = inventoryModels.new PropertyBedrooms();
                                JSONObject jsonObjectPropertyBedrooms = jsonArrayPropertyBedrooms.getJSONObject(k);
                                propertyBedrooms.setId(jsonObjectPropertyBedrooms.getString("Id"));
                                propertyBedrooms.setBedRoomName(jsonObjectPropertyBedrooms.getString("BedRoomName"));
                                propertyBedrooms.setGroundType(jsonObjectPropertyBedrooms.getString("GroundType"));
                                propertyBedrooms.setFloorNo(jsonObjectPropertyBedrooms.getString("FloorNo"));
                                propertyBedrooms.setBedRoomType(jsonObjectPropertyBedrooms.getString("BedRoomType"));
                                propertyBedrooms.setCategory(jsonObjectPropertyBedrooms.getString("Category"));
                                propertyBedroomses.add(propertyBedrooms);

                            }
                            JSONArray jsonArrayPropertySpecifications = jsonObject1.getJSONArray("PropertySpecifications");
                            ArrayList<InventoryModel_Area.PropertySpecifications> propertySpecificationses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertySpecifications.length(); k++) {
                                InventoryModel_Area.PropertySpecifications propertySpecifications = inventoryModels.new PropertySpecifications();
                                JSONObject jsonObjectPropertySpecifications = jsonArrayPropertySpecifications.getJSONObject(k);
                                propertySpecifications.setId(jsonObjectPropertySpecifications.getString("Id"));
                                propertySpecifications.setPropertyId(jsonObjectPropertySpecifications.getString("PropertyId"));
                                propertySpecifications.setName(jsonObjectPropertySpecifications.getString("Name"));
                                propertySpecifications.setCategory(jsonObjectPropertySpecifications.getString("Category"));
                                propertySpecifications.setDescription(jsonObjectPropertySpecifications.getString("Description"));


                                propertySpecificationses.add(propertySpecifications);

                            }
                            inventoryModels.setPropertyLoanses(propertyLoanses);
                            inventoryModels.setPropertyLocations(propertyLocations);
                            inventoryModels.setPropertyAreas(propertyAreas);
                            inventoryModels.setPropertyFinancialses(propertyFinancialses);
                            inventoryModels.setPropertyImages(propertyImages);
                            inventoryModels.setPropertyOtherExpences(propertyOtherExpences);
                            inventoryModels.setPropertyPlcs(propertyPlcs);
                            inventoryModels.setPropertyOtherQuestionses(propertyOtherQuestionses);
                            inventoryModels.setPropertyOwnerses(propertyOwnerses);
                            inventoryModels.setPropertyLegalStatuses(propertyLegalStatuses);
                            inventoryModels.setPropertyAccommodationDetailses(propertyAccommodationDetails);
                            inventoryModels.setPropertyBedroomses(propertyBedroomses);
                            inventoryModels.setPropertySpecificationses(propertySpecificationses);

                            if(!jsonObject1.getString("PropertyType").equalsIgnoreCase("null")) {
                                JSONObject objectPropertyType = jsonObject1.getJSONObject("PropertyType");
                                inventoryModels.setPropertyobjectId(objectPropertyType.getString("Id"));
                                inventoryModels.setNameobject(objectPropertyType.getString("Name"));
                                inventoryModels.setTypeobject(objectPropertyType.getString("Type"));
                            }

                            if(inventoryModels.getTypeobject().equalsIgnoreCase("Residential"))
                                foryouArrayList.add(inventoryModels);
                            if(inventoryModels.getTypeobject().equalsIgnoreCase("Commercial"))
                                foryouArrayList2.add(inventoryModels);
                        }

                        if(foryouArrayList.size()!=0) {
                            count1.setText(String.valueOf(foryouArrayList.size()));
                            title_name.setText(foryouArrayList.get(0).getTypeobject());
                        }
                        if(foryouArrayList2.size()!=0) {
                            title_name2.setText(foryouArrayList2.get(0).getTypeobject());
                            count2.setText(String.valueOf(foryouArrayList2.size()));
                        }

                        if(foryouArrayList.size() == 0 && foryouArrayList2.size() != 0) {
                            view_lay.setVisibility(View.GONE);
                            for_list.setVisibility(View.GONE);
                            view_all.setVisibility(View.GONE);
                            for_list_1.setAdapter(new PropertyListingAdapter(getActivity(), foryouArrayList2, view_data1));
                        } else if(foryouArrayList.size() != 0 && foryouArrayList2.size() == 0) {
                            for_list.setAdapter(new PropertyListingAdapter(getActivity(), foryouArrayList, view_data));
                            view_lay2.setVisibility(View.GONE);
                            for_list_1.setVisibility(View.GONE);
                            view_all2.setVisibility(View.GONE);
                        } else {
                            for_list.setAdapter(new PropertyListingAdapter(getActivity(), foryouArrayList, view_data));
                            for_list_1.setAdapter(new PropertyListingAdapter(getActivity(), foryouArrayList2, view_data1));
                        }

                        try {
                            search_filter_lay.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if(which.equalsIgnoreCase(Urls.URL_PROPERTY_TYPE)){

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray arrayTypes = jsonObject.getJSONArray("Result");

                    if (arrayTypes.length() != 0) {

                        recyclerView.setVisibility(View.VISIBLE);

                        ArrayList<String> listNames = new ArrayList<>();

                        for (int i = 0; i < arrayTypes.length(); i++) {
                            String Name = arrayTypes.getJSONObject(i).getString("Name");
                            listNames.add(Name);
                        }

                        PropertyType_adapter adapter = new PropertyType_adapter(getActivity(), listNames);
                        adapter.notifyDataSetChanged();

                        recyclerView.setHasFixedSize(true);

                        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
                        recyclerView.setLayoutManager(gridLayoutManager);

                        recyclerView.setAdapter(adapter);


                    } else {
                        recyclerView.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
