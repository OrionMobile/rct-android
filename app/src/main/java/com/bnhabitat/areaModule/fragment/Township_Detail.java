package com.bnhabitat.areaModule.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.Gallery_Image_Adapter;
import com.bnhabitat.areaModule.adapter.Project_Image_Adapter;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.interfaces.TabChangeListener;

import java.util.ArrayList;

public class Township_Detail extends Fragment implements TabChangeListener {

    LinearLayout project_images_details, gallery_images_details;
    RecyclerView project_images_layout, gallery_images_layout;

    ArrayList<String> project_naming = new ArrayList<>();
    ArrayList<String> gallery_naming = new ArrayList<>();

    Project_Image_Adapter project_image_adapter;
    Gallery_Image_Adapter gallery_image_adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.township_detail_fragment, container, false);
        project_images_details = (LinearLayout) view.findViewById(R.id.project_images_details);
        gallery_images_details = (LinearLayout) view.findViewById(R.id.gallery_images_details);

        project_images_layout = (RecyclerView) view.findViewById(R.id.project_images_layout);
        gallery_images_layout = (RecyclerView) view.findViewById(R.id.gallery_images_layout);

        project_naming.add("Wellington Heights Extension");
        project_naming.add("Connaught Plaza");
        project_naming.add("Wellington Heights Extension");
        project_naming.add("Connaught Plaza");

        gallery_naming.add("Plan - Ground Floor");
        gallery_naming.add("Plan - First and Second Floor");

        project_image_adapter = new Project_Image_Adapter(getActivity(), project_naming);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        project_images_layout.setLayoutManager(linearLayoutManager);
        project_images_layout.setAdapter(project_image_adapter);

        gallery_image_adapter = new Gallery_Image_Adapter(getActivity(), gallery_naming);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        gallery_images_layout.setLayoutManager(linearLayoutManager1);
        gallery_images_layout.setAdapter(gallery_image_adapter);

        return view;
    }

    /*public  ArrayList<StateListModel.Pictures> getStateArrayList()
    {
        return pictures1;
    }*/

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
