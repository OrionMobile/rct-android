package com.bnhabitat.areaModule.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.Developer_list;
import com.bnhabitat.areaModule.adapter.DistrictListingAdapter;
import com.bnhabitat.areaModule.adapter.Govt_Dept_Adapter;
import com.bnhabitat.areaModule.adapter.Govt_Notification_Adapter;
import com.bnhabitat.areaModule.adapter.ImageAdapter;
import com.bnhabitat.areaModule.adapter.Image_Video_Adapter;
import com.bnhabitat.areaModule.adapter.StateImageAdapter;
import com.bnhabitat.areaModule.areaActivities.StateListing;
import com.bnhabitat.areaModule.areaActivities.Submit_Requirement;
import com.bnhabitat.areaModule.areaActivities.ViewPagerListener;
import com.bnhabitat.areaModule.model.Departments;
import com.bnhabitat.areaModule.model.Developers;
import com.bnhabitat.areaModule.model.DistrictListModel;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.nostra13.universalimageloader.utils.L;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class StateDetail extends Fragment implements TabChangeListener, ViewPagerListener, ViewPager.OnPageChangeListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener {

    RecyclerView list_images_videos, developer_list, govt_dept_rep, govt_notification_rep, latest_update_rep, gmada_videos, gmada_photos;

    ArrayList<String> officialLanguage = new ArrayList<>();
    ArrayList<String> borderStateList = new ArrayList<>();
    ArrayList<StateListModel.Pictures> pictures = new ArrayList<>();
    ArrayList<StateListModel.Pictures> pictures1 = new ArrayList<>();
    ArrayList<Developers> developersArrayList = new ArrayList<>();
    ArrayList<Departments> departmentsArrayList = new ArrayList<>();
    String view_data = "";

    ViewPager viewPager;

    ImageAdapter imageAdapter;
    Image_Video_Adapter image_video_adapter;
    Govt_Dept_Adapter govt_dept_adapter;
    Developer_list developer_list_adapter;
    StateImageAdapter stateImageAdapter;

    ImageView imgPrevious, imgNext;

    String Id;

    Dialog dialog, dialog1;

    TextView villageReg, about_state, lineCount, project_count, residential_count, commercial_count;

    TextView no_data, no_data1, no_data2, state, stateing, statename, country, capital, totalArea, totalAreaUnits, region, officialLanguages, borderStates, urbanVillageNumber, ruralVillageNumber, iso, totalDistricts, totalTehsils, totalCities;

    ImageView userPic, msg, call;
    TextView first_last_name, professional_txt, submitReq, propertSell_Rent;
    String contact = "", email = "";
    private static final int REQUEST_PHONE_CALL = 198;
    ImageView close, img, close1, image;
    TextView name, project_name, desc, description, total_developers;
    RadioButton yes, no, buy, rent, yes1, no1, sell, rent1;
    LinearLayout property_detailing, property_detailing1;
    Spinner category_spinner, property_spinner, category_spinner1, property_spinner1;
    CheckBox check_studio, check_2bhk, check_3bhk, check_4bhk;
    EditText message, datePicker, media, tower_name_no, unit_no, message1, datePick, media1;
    Button submit, submit1;
    LinearLayout viewall, dev_count_layout;
    String name_number;
    RelativeLayout backward, forward, backward1, forward1;
    static int pos = 0;
    ImageView left, right, left1, right1;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_state_detail, container, false);
        initial(view);

        Id = getArguments().getString("Id");

        //onAreaStateDetail();

        if(PreferenceConnector.getInstance(getContext()).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("") || PreferenceConnector.getInstance(getContext()).loadSavedPreferences(Constants.USERNAME, "").equalsIgnoreCase("null"))
            name_number = PreferenceConnector.getInstance(getContext()).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        else
            name_number = PreferenceConnector.getInstance(getContext()).loadSavedPreferences(Constants.USERNAME, "");
        first_last_name.setText(name_number);
        contact = PreferenceConnector.getInstance(getContext()).loadSavedPreferences(Constants.PHONE_NUMBER, "");
        email = PreferenceConnector.getInstance(getContext()).loadSavedPreferences(Constants.EMAIL, "");

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                } else {
                    callIntent.setData(Uri.parse("tel:" + contact));
                    startActivity(callIntent);
                }
            }
        });

        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_text));
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
                startActivity(Intent.createChooser(intent, "Choose"));
            }
        });

        submitReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitReqPopUp(pictures.get(0).getUrl());
            }
        });

        propertSell_Rent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                propertySell_RentPopUp(pictures.get(0).getUrl());
            }
        });
        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItemBack(viewPager.getCurrentItem())==0)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else if(getItemBack(viewPager.getCurrentItem())==pictures.size()-1)
                    viewPager.setCurrentItem(getItemBack(pictures.size()-1), true);
                else
                    viewPager.setCurrentItem(getItemBack(-1), true);

            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItem(+1) == pictures.size())
                    viewPager.setCurrentItem(getItem(0), true);
                else
                    viewPager.setCurrentItem(getItem(+1), true);

            }
        });

        total_developers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                view_data = "visible";
                viewall.setVisibility(View.GONE);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                developer_list.setLayoutManager(layoutManager);
                developer_list.setAdapter(new Developer_list(getActivity(), developersArrayList, view_data));
            }
        });

/*        if(developersArrayList.size()>2){

            backward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    developer_list.smoothScrollToPosition(getId() - 1);
                    right.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                }
            });

            forward.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    developer_list.smoothScrollToPosition(getId() + 1);
                    //left.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                }
            });

        }else{
            right.setColorFilter(ContextCompat.getColor(getContext(), R.color.divider_light_grey), android.graphics.PorterDuff.Mode.MULTIPLY);
            left.setColorFilter(ContextCompat.getColor(getContext(), R.color.divider_light_grey), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        if(pictures1.size()>2){
            backward1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gmada_photos.smoothScrollToPosition(getId() - 1);
                    right1.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                }
            });

            forward1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    gmada_photos.smoothScrollToPosition(getId() + 1);
                    left1.setColorFilter(ContextCompat.getColor(getActivity(), R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                }
            });
        }else{
            right1.setColorFilter(ContextCompat.getColor(getContext(), R.color.divider_light_grey), android.graphics.PorterDuff.Mode.MULTIPLY);
            left1.setColorFilter(ContextCompat.getColor(getContext(), R.color.divider_light_grey), android.graphics.PorterDuff.Mode.MULTIPLY);
        }*/
        return view;
    }

    @Override
    public void onResume() {
        onAreaStateDetail();
        super.onResume();
    }

    private void initial(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.view_images);
        imgPrevious = (ImageView) view.findViewById(R.id.imgPrevious);
        imgNext = (ImageView) view.findViewById(R.id.imgNext);
        project_count = (TextView) view.findViewById(R.id.project_count);
        residential_count = (TextView) view.findViewById(R.id.residential_count);
        commercial_count = (TextView) view.findViewById(R.id.commercial_count);
        country = (TextView) view.findViewById(R.id.country);
        capital = (TextView) view.findViewById(R.id.capital);
        region = (TextView) view.findViewById(R.id.region);
        totalArea = (TextView) view.findViewById(R.id.totalArea);
        totalAreaUnits = (TextView) view.findViewById(R.id.totalAreaUnits);
        totalAreaUnits.setVisibility(View.GONE);
        totalDistricts = (TextView) view.findViewById(R.id.totalDistricts);
        totalTehsils = (TextView) view.findViewById(R.id.totalTehsils);
        totalCities = (TextView) view.findViewById(R.id.totalCities);
        officialLanguages = (TextView) view.findViewById(R.id.officialLanguages);
        urbanVillageNumber = (TextView) view.findViewById(R.id.urbanVillageNumber);
        ruralVillageNumber = (TextView) view.findViewById(R.id.ruralVillageNumber);
        iso = (TextView) view.findViewById(R.id.iso);
        borderStates = (TextView) view.findViewById(R.id.borderStates);
        //villageReg = (TextView) view.findViewById(R.id.villageReg);
        about_state = (TextView) view.findViewById(R.id.about_state);
        lineCount = (TextView) view.findViewById(R.id.lineCount);
        statename = (TextView) view.findViewById(R.id.statename);
        stateing = (TextView) view.findViewById(R.id.stateing);
        state = (TextView) view.findViewById(R.id.state);
        total_developers = (TextView) view.findViewById(R.id.total_developers);
        no_data = (TextView) view.findViewById(R.id.no_data);
        no_data1 = (TextView) view.findViewById(R.id.no_data1);
        no_data2 = (TextView) view.findViewById(R.id.no_data2);

        list_images_videos = (RecyclerView) view.findViewById(R.id.list_images_videos);
        developer_list = (RecyclerView) view.findViewById(R.id.developer_list);
        govt_dept_rep = (RecyclerView) view.findViewById(R.id.govt_dept_rep);
        govt_notification_rep = (RecyclerView) view.findViewById(R.id.govt_notification_rep);
        latest_update_rep = (RecyclerView) view.findViewById(R.id.latest_update_rep);
        gmada_videos = (RecyclerView) view.findViewById(R.id.state_videos);
        gmada_photos = (RecyclerView) view.findViewById(R.id.state_photos);

        userPic = (ImageView) view.findViewById(R.id.userPic);
        msg = (ImageView) view.findViewById(R.id.msg);
        call = (ImageView) view.findViewById(R.id.call);

        left = (ImageView) view.findViewById(R.id.left);
        right = (ImageView) view.findViewById(R.id.right);
        /*left1 = (ImageView) view.findViewById(R.id.left1);
        right1 = (ImageView) view.findViewById(R.id.right1);*/

        first_last_name = (TextView) view.findViewById(R.id.first_last_name);
        professional_txt = (TextView) view.findViewById(R.id.professional_txt);
        submitReq = (TextView) view.findViewById(R.id.submitReq);
        propertSell_Rent = (TextView) view.findViewById(R.id.propertSell_Rent);

        viewall = (LinearLayout) view.findViewById(R.id.viewall);
        dev_count_layout = (LinearLayout) view.findViewById(R.id.dev_count_layout);

//        backward = (RelativeLayout) view.findViewById(R.id.backward);
//        forward = (RelativeLayout) view.findViewById(R.id.forward);
//        backward1 = (RelativeLayout) view.findViewById(R.id.backward1);
//        forward1 = (RelativeLayout) view.findViewById(R.id.forward1);

    }

    private int getItem(int i) {

        if(viewPager.getCurrentItem() == pictures.size()-1)
            return 0;
        else
            return viewPager.getCurrentItem() + i;

    }

    private int getItemBack(int i) {

        if(viewPager.getCurrentItem() == pictures.size()-1)
            return pictures.size()-2;
        else if(viewPager.getCurrentItem() == 0)
            return pictures.size()-1;
        else
            return viewPager.getCurrentItem() + i;

    }


    public void mapPopUp(){
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setContentView(R.layout.layout_add_accomo);
    }

    private void onAreaStateDetail() {

        new CommonSyncwithoutstatus(getActivity(),
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_STATE_DETAIL +
                        Id ,
                "",
                "Loading...",this,
                Urls.URL_STATE_DETAIL,
                Constants.GET)
                .execute();

    }

    @Override
    public void onResultListener(String result, String which) {

        if(null != result && !result.equalsIgnoreCase("")){
            if(which.equalsIgnoreCase(Urls.URL_STATE_DETAIL)){
                try{
                    JSONObject jsonObject = new JSONObject(result);
                    int statusCode = jsonObject.getInt("StatusCode");
                    if (statusCode == 200) {
                        final JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        if(jsonObject1.getString("Country").equalsIgnoreCase("null"))
                            country.setText("N/A");
                        else
                            country.setText(jsonObject1.getString("Country"));
                        String country =jsonObject1.getString("Country");
                        PreferenceConnector.getInstance(getContext()).savePreferences("Country", country);
                        if(jsonObject1.getString("Capital").equalsIgnoreCase("null"))
                            capital.setText("N/A");
                        else
                            capital.setText(jsonObject1.getString("Capital"));
                        if(jsonObject1.getString("Region").equalsIgnoreCase("null"))
                            region.setText("N/A");
                        else
                            region.setText(jsonObject1.getString("Region"));
                        if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            totalArea.setText("N/A");
                        else if(!jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            totalArea.setText(jsonObject1.getString("TotalArea") + " N/A" );
                        else if(jsonObject1.getString("TotalArea").equalsIgnoreCase("null") && !jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            totalArea.setText("N/A " + jsonObject1.getString("TotalAreaSizeUnit"));
                        else
                            totalArea.setText(jsonObject1.getString("TotalArea") + " " + jsonObject1.getString("TotalAreaSizeUnit"));
                        /*if(jsonObject1.getString("TotalAreaSizeUnit").equalsIgnoreCase("null"))
                            totalAreaUnits.setText("N/A");
                        else
                            totalAreaUnits.setText(jsonObject1.getString("TotalAreaSizeUnit"));*/

                        JSONArray districtNumber = jsonObject1.getJSONArray("Districts");
                        totalDistricts.setText(Integer.toString(districtNumber.length()));
                        if(jsonObject1.getString("Name").equalsIgnoreCase("null"))
                            statename.setText("N/A");
                        else
                            statename.setText(jsonObject1.getString("Name"));
                        String name = jsonObject1.getString("Name");
                        PreferenceConnector.getInstance(getContext()).savePreferences("StateName", name);
                        stateing.setText(jsonObject1.getString("Name"));
                        state.setText(getResources().getString(R.string.govt_dept)+ " " + jsonObject1.getString("Name"));
                        totalTehsils.setText(jsonObject1.getString("TotalTehsils"));
                        totalCities.setText(jsonObject1.getString("TotalCities"));
                        project_count.setText(jsonObject1.getString("TotalProperties"));
                        residential_count.setText(jsonObject1.getString("TotalResidentialProperties"));
                        commercial_count.setText(jsonObject1.getString("TotalCommercialProperties"));
                        urbanVillageNumber.setText(jsonObject1.getString("TotalUrbanVillages"));
                        ruralVillageNumber.setText(jsonObject1.getString("TotalRuralVillages"));
                        if(jsonObject1.getString("Iso3166Code").equalsIgnoreCase("null"))
                            iso.setText("N/A");
                        else
                            iso.setText(jsonObject1.getString("Iso3166Code"));

                        borderStateList.clear();
                        JSONArray borderingStates = jsonObject1.getJSONArray("BorderingState");
                        if(borderingStates.length()!=0){
                            for(int i = 0; i <borderingStates.length(); i++){
                                JSONObject language = borderingStates.getJSONObject(i);
                                String lang = (String) language.get("Value");
                                borderStateList.add(lang);
                            }
                            borderStates.setText(TextUtils.join(", ",borderStateList));
                        }else{
                            borderStates.setText("N/A");
                        }

                        officialLanguage.clear();
                        JSONArray officialLanguageArray = jsonObject1.getJSONArray("OfficialLanguages");
                        if(officialLanguageArray.length()!=0){
                            for(int i = 0; i <officialLanguageArray.length(); i++){
                                JSONObject language = officialLanguageArray.getJSONObject(i);
                                String lang = (String) language.get("Value");
                                officialLanguage.add(lang);
                            }
                            officialLanguages.setText(TextUtils.join(", ", officialLanguage));
                        }else{
                            officialLanguages.setText("N/A");
                        }


                        if(jsonObject1.getString("AboutState").equalsIgnoreCase("null"))
                            about_state.setText("N/A");
                        else
                            about_state.setText(jsonObject1.getString("AboutState"));

                        about_state.post(new Runnable() {
                            @Override
                            public void run() {
                                final int lineCount1 = about_state.getLineCount();
                                if(lineCount1 <= 5) {
                                    about_state.setEllipsize(null);
                                    lineCount.setVisibility(View.GONE);
                                }
                                else{
                                    lineCount.setVisibility(View.VISIBLE);
                                    lineCount.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            lineCount.setVisibility(View.GONE);
                                            about_state.setEllipsize(null);
                                            about_state.setMaxLines(Integer.MAX_VALUE);
                                            try {
                                                about_state.setText(jsonObject1.getString("Aboutstate"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                                // Use lineCount here
                            }
                        });

                        pictures.clear();
                        JSONArray jsonArray = jsonObject1.getJSONArray("Pictures");
                        if(jsonArray.length()!=0){
                            for(int i = 0; i<jsonArray.length(); i++){
                                JSONObject object = jsonArray.getJSONObject(i);
                                StateListModel.Pictures stateListModel = new StateListModel.Pictures();
                                stateListModel.setId(object.getString("Id"));
                                stateListModel.setUrl(Urls.BASE_CONTACT_IMAGE_URL +object.getString("Url"));
                                pictures.add(stateListModel);
                            }
                        }

                        imageAdapter = new ImageAdapter(getContext(), pictures);
                        viewPager.setAdapter(imageAdapter);
                        imageAdapter.notifyDataSetChanged();

                        image_video_adapter = new Image_Video_Adapter(getContext(), pictures, this);
                        list_images_videos.setAdapter(image_video_adapter);
                        list_images_videos.setLayoutManager(new LinearLayoutManager(getContext(),  LinearLayout.HORIZONTAL, false));
                        image_video_adapter.notifyDataSetChanged();

                        developersArrayList.clear();
                        JSONArray developers = jsonObject1.getJSONArray("Developers");
                        if(developers.length()!=0){
                            for(int i = 0; i< developers.length(); i++){
                                JSONObject dev = developers.getJSONObject(i);
                                Developers developers1 = new Developers();
                                developers1.setId(dev.getString("Id"));
                                developers1.setName(dev.getString("Name"));
                                developers1.setLogoUrl(Urls.BASE_CONTACT_IMAGE_URL + dev.getString("LogoUrl"));
                                developersArrayList.add(developers1);
                            }
                        }

                        developer_list_adapter = new Developer_list(getContext(), developersArrayList, view_data);
                        developer_list.setAdapter(developer_list_adapter);
                        developer_list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayout.HORIZONTAL, false));
                        developer_list_adapter.notifyDataSetChanged();

                        if(developersArrayList.size() == 0) {
                            viewall.setVisibility(View.GONE);
                            developer_list.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                        }
                        else {
                            viewall.setVisibility(View.VISIBLE);
                            developer_list.setVisibility(View.VISIBLE);
                            no_data.setVisibility(View.GONE);
                        }

                        pictures1.clear();
                        JSONArray jsonArray1 = jsonObject1.getJSONArray("PoliticalMapImageUrl");
                        if(jsonArray1.length()!=0){
                            for(int i = 0; i<jsonArray1.length(); i++){
                                JSONObject object = jsonArray1.getJSONObject(i);
                                StateListModel.Pictures stateListModel = new StateListModel.Pictures();
                                stateListModel.setId(object.getString("Id"));
                                stateListModel.setUrl(Urls.BASE_CONTACT_IMAGE_URL +object.getString("Url"));
                                pictures1.add(stateListModel);
                            }
                        }

                        stateImageAdapter = new StateImageAdapter(getContext(), pictures1);
                        gmada_videos.setAdapter(stateImageAdapter);
                        gmada_videos.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                        stateImageAdapter.notifyDataSetChanged();

                        stateImageAdapter = new StateImageAdapter(getContext(), pictures1);
                        gmada_photos.setAdapter(stateImageAdapter);
                        gmada_photos.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                        stateImageAdapter.notifyDataSetChanged();

                        if(pictures1.size() == 0){
                            gmada_photos.setVisibility(View.GONE);
                            no_data2.setVisibility(View.VISIBLE);
                        }else{
                            gmada_photos.setVisibility(View.VISIBLE);
                            no_data2.setVisibility(View.GONE);
                        }

                        JSONArray department = jsonObject1.getJSONArray("Departments");
                        departmentsArrayList.clear();
                        if(department.length()>0){
                            for(int i = 0; i < department.length(); i++){
                                JSONObject dept = department.getJSONObject(i);
                                Departments departments = new Departments();

                                departments.setId(dept.getString("Id"));
                                departments.setName(dept.getString("Name"));
                                departments.setLogoUrl(Urls.BASE_CONTACT_IMAGE_URL + dept.getString("LogoUrl"));
                                departments.setIndustry(dept.getString("Industry"));
                                departments.setWebsite(dept.getString("Website"));
                                JSONArray address = dept.getJSONArray("Addresses");
                                ArrayList<Departments.Addresses> addresses = new ArrayList<>();
                                addresses.clear();

                                for(int index = 0; index < address.length(); index++){
                                    JSONObject addressObject = address.getJSONObject(index);
                                    Departments.Addresses addresses1 = new Departments.Addresses();

                                    if(addressObject.length() > 0){
                                        addresses1.setProjectName(addressObject.getString("ProjectName"));
                                        addresses1.setUnitName(addressObject.getString("UnitName"));
                                        addresses1.setTowerName(addressObject.getString("TowerName"));
                                        addresses1.setSubAreaName(addressObject.getString("SubAreaName"));
                                        addresses1.setTownName(addressObject.getString("TownName"));
                                        addresses1.setLocalityName(addressObject.getString("LocalityName"));
                                        addresses1.setTownshipName(addressObject.getString("TownshipName"));
                                        addresses1.setVillageName(addressObject.getString("VillageName"));
                                        addresses1.setTehsilName(addressObject.getString("TehsilName"));
                                        addresses1.setZipCode(addressObject.getString("ZipCode"));
                                        addresses1.setGooglePlusCode(addressObject.getString("GooglePlusCode"));
                                        addresses1.setDistrictName(addressObject.getString("DistrictName"));
                                        addresses1.setStateName(addressObject.getString("StateName"));
                                        addresses1.setCountryName(addressObject.getString("CountryName"));

                                        addresses.add(addresses1);
                                    }
                                    else{
                                        addresses1.setProjectName("");
                                        addresses1.setUnitName("");
                                        addresses1.setTowerName("");
                                        addresses1.setSubAreaName("");
                                        addresses1.setTownName("");
                                        addresses1.setLocalityName("");
                                        addresses1.setTownshipName("");
                                        addresses1.setVillageName("");
                                        addresses1.setTehsilName("");
                                        addresses1.setZipCode("");
                                        addresses1.setGooglePlusCode("");
                                        addresses1.setDistrictName("");
                                        addresses1.setStateName("");
                                        addresses1.setCountryName("");

                                        addresses.add(addresses1);
                                    }
                                }

                                departments.setAddresses(addresses);
                                departmentsArrayList.add(departments);
                            }
                        }

                        govt_dept_adapter = new Govt_Dept_Adapter(getContext(), departmentsArrayList);
                        govt_dept_rep.setAdapter(govt_dept_adapter);
                        govt_dept_rep.setLayoutManager(new LinearLayoutManager(getContext()));
                        govt_dept_adapter.notifyDataSetChanged();

                        if(departmentsArrayList.size() == 0){
                            govt_dept_rep.setVisibility(View.GONE);
                            no_data1.setVisibility(View.VISIBLE);
                        }
                        else{
                            govt_dept_rep.setVisibility(View.VISIBLE);
                            no_data1.setVisibility(View.GONE);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void setViewPager(int position) {
        viewPager.setCurrentItem(position, true);

    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    public void submitReqPopUp(String url){
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.submit_requirement_popup);
        close = (ImageView) dialog.findViewById(R.id.close);
        img = (ImageView) dialog.findViewById(R.id.img);
        Picasso.with(getContext()).load(url).into(img);
        name = (TextView) dialog.findViewById(R.id.name);
        name.setText(PreferenceConnector.getInstance(getContext()).loadSavedPreferences("StateName", ""));
        description = (TextView) dialog.findViewById(R.id.description);
        description.setText(PreferenceConnector.getInstance(getContext()).loadSavedPreferences("Country", ""));
        yes = (RadioButton) dialog.findViewById(R.id.yes);
        no = (RadioButton) dialog.findViewById(R.id.no);
        buy = (RadioButton) dialog.findViewById(R.id.buy);
        rent = (RadioButton) dialog.findViewById(R.id.rent);
        property_detailing = (LinearLayout) dialog.findViewById(R.id.property_detailing);
        category_spinner = (Spinner) dialog.findViewById(R.id.category_spinner);
        property_spinner = (Spinner) dialog.findViewById(R.id.property_spinner);
        check_studio = (CheckBox) dialog.findViewById(R.id.check_studio);
        check_2bhk = (CheckBox) dialog.findViewById(R.id.check_2bhk);
        check_3bhk = (CheckBox) dialog.findViewById(R.id.check_3bhk);
        check_4bhk = (CheckBox) dialog.findViewById(R.id.check_4bhk);
        message = (EditText) dialog.findViewById(R.id.message);
        datePicker = (EditText) dialog.findViewById(R.id.datePicker);
        media = (EditText) dialog.findViewById(R.id.media);
        submit = (Button) dialog.findViewById(R.id.submit);

        yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    property_detailing.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    startActivity(new Intent(getContext(), Submit_Requirement.class));
                }
            }
        });

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }

    private void propertySell_RentPopUp(String url) {
        dialog1 = new Dialog(getContext());
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.property_sell_rent);
        close1 = (ImageView) dialog1.findViewById(R.id.close);
        image = (ImageView) dialog1.findViewById(R.id.image);
        Picasso.with(getContext()).load(url).into(image);
        project_name = (TextView) dialog1.findViewById(R.id.project_name);
        project_name.setText(PreferenceConnector.getInstance(getContext()).loadSavedPreferences("StateName", ""));
        desc = (TextView) dialog1.findViewById(R.id.desc);
        desc.setText(PreferenceConnector.getInstance(getContext()).loadSavedPreferences("Country", ""));
        yes1 = (RadioButton) dialog1.findViewById(R.id.yes);
        no1 = (RadioButton) dialog1.findViewById(R.id.no);
        sell = (RadioButton) dialog1.findViewById(R.id.sell);
        rent1 = (RadioButton) dialog1.findViewById(R.id.rent);
        property_detailing1 = (LinearLayout) dialog1.findViewById(R.id.property_detailing);
        category_spinner1 = (Spinner) dialog1.findViewById(R.id.category_spinner);
        property_spinner1 = (Spinner) dialog1.findViewById(R.id.property_spinner);
        tower_name_no = (EditText) dialog1.findViewById(R.id.tower_name_no);
        unit_no = (EditText) dialog1.findViewById(R.id.unit_no);
        message1 = (EditText) dialog1.findViewById(R.id.message1);
        datePick = (EditText) dialog1.findViewById(R.id.datePick);
        media1 = (EditText) dialog1.findViewById(R.id.media);
        submit1 = (Button) dialog1.findViewById(R.id.submit);

        yes1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    property_detailing1.setVisibility(View.VISIBLE);
                    no.setChecked(false);
                }
            }
        });

        no1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    //startActivity(new Intent(getContext(), Submit_Requirement.class));

                }
            }
        });

        dialog1.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog1.show();

        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.cancel();
            }
        });
    }

    public  ArrayList<StateListModel.Pictures> getStateArrayList()
    {
        return pictures1;
    }

}
