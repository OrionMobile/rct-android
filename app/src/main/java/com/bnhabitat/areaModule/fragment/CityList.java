package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.CityListingAdapter;
import com.bnhabitat.areaModule.adapter.DistrictListingAdapter;
import com.bnhabitat.areaModule.model.CityListModel;
import com.bnhabitat.areaModule.model.DistrictListModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CityList extends Fragment implements TabChangeListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener {

    RecyclerView rvlist;
    TextView more_states, no_data;
    View view;
    String stateId, districtId;

    int totalRecords;
    private int pageSize = 10;
    CityListingAdapter cityListingAdapter;
    ArrayList<CityListModel> cityListModelArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.district_list, container, false);

        initialization(view);

        stateId = getArguments().getString("stateId");
        districtId = getArguments().getString("districtId");

        //onAreaDistrictCityList();

        more_states.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pageSize = pageSize + 10;

                onAreaDistrictCityList();
            }
        });

        cityListingAdapter = new CityListingAdapter(getContext(), cityListModelArrayList);
        rvlist.setAdapter(cityListingAdapter);
        rvlist.setLayoutManager(new GridLayoutManager(getContext(), 2));
        cityListingAdapter.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onResume() {
        onAreaDistrictCityList();
        super.onResume();
    }

    private void initialization(View view) {
        rvlist= (RecyclerView) view.findViewById(R.id.rvlist);
        more_states = (TextView) view.findViewById(R.id.more_states);
        no_data = (TextView) view.findViewById(R.id.no_data);
    }

    private void onAreaDistrictCityList() {

        new CommonSyncwithoutstatus(getActivity(),
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_CITY_LIST +
                        pageSize + "/" + stateId + "/" + districtId + "/0/0/0/0/0",
                "",
                "Loading...",this,
                Urls.URL_CITY_LIST,
                Constants.GET)
                .execute();

    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_CITY_LIST)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    JSONArray stateArray = jsonObject.getJSONArray("Result");
                    cityListModelArrayList.clear();

                    if(stateArray.length()!=0) {
                        for (int index = 0; index < stateArray.length(); index++) {
                            JSONObject jsonObject1 = stateArray.getJSONObject(index);

                            CityListModel cityListModel = new CityListModel();
                            cityListModel.setId(jsonObject1.getString("Id"));
                            cityListModel.setCity(jsonObject1.getString("Name"));
                            JSONArray jsonArray = jsonObject1.optJSONArray("Projects");
                            cityListModel.setLength(String.valueOf(jsonArray.length()));
                            cityListModel.setTownships(jsonObject1.getString("Townships"));
                            cityListModel.setLocalities(jsonObject1.getString("Localities"));
                            cityListModel.setProperties(jsonObject1.getString("Properties"));
                            cityListModel.setDistrictId(jsonObject1.getString("DistrictId"));
                            cityListModel.setStateId(jsonObject1.getString("StateId"));
                            JSONArray jsonArray1 = jsonObject1.optJSONArray("Pictures");
                            ArrayList<CityListModel.Pictures> picturesArrayList = new ArrayList<>();
                            picturesArrayList.clear();
                            for (int i = 0; i < jsonArray1.length(); i++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(i);
                                CityListModel.Pictures pictures = new CityListModel.Pictures();

                                if (jsonObject2.length() > 0) {
                                    pictures.setId(jsonObject2.getString("Id"));
                                    pictures.setUrl(Urls.BASE_CONTACT_IMAGE_URL + jsonObject2.getString("Url"));
                                    picturesArrayList.add(pictures);
                                } else {
                                    pictures.setId("");
                                    pictures.setUrl("");
                                    picturesArrayList.add(pictures);
                                }
                            }
                            cityListModel.setPictures(picturesArrayList);

                            cityListModel.setTotalRecords(jsonObject1.getString("TotalRecords"));

                            totalRecords = jsonObject1.getInt("TotalRecords");

                            cityListModelArrayList.add(cityListModel);

                        }
                        cityListingAdapter = new CityListingAdapter(getContext(), cityListModelArrayList);
                        rvlist.setAdapter(cityListingAdapter);
                        rvlist.setLayoutManager(new GridLayoutManager(getContext(), 2));
                        cityListingAdapter.notifyDataSetChanged();

                        try {
                            if (totalRecords <= cityListModelArrayList.size() + 1)
                                more_states.setVisibility(View.GONE);
                            else
                                more_states.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        rvlist.setVisibility(View.GONE);
                        more_states.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
