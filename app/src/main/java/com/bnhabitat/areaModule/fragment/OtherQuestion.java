package com.bnhabitat.areaModule.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel_Area;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;

public class OtherQuestion extends Fragment implements TabChangeListener {

    View view;
    ImageView call, msg;
    LinearLayout id_proof, pan;
    TextView submited_behalf, first_last_name, professional_txt, address;
    ArrayList<InventoryModel_Area> property_data;
    private static final int REQUEST_PHONE_CALL = 198;
    static String contactNumber = "";
    static String email = "";
    String prefix = "", firstname = "", lastname = "", middlename= "";
    String unitname = "", subarea = "", locality = "", village = "", town = "";

    public OtherQuestion() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.other_questions_fragment, container, false);
        onIntializeView(view);
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null)
                property_data = (ArrayList<InventoryModel_Area>) bundle.getSerializable("property_data");

            submited_behalf.setText(Utils.getEmptyValue(property_data.get(0).getPropertyOtherQuestionses().get(0).getSubmitOfBehalfOf()));
            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getPrefix().equalsIgnoreCase("null"))
                prefix = property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getPrefix() + " ";
            else
                prefix = "";

            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getFirstName().equalsIgnoreCase("null"))
                firstname = property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getFirstName() + " ";
            else
                firstname = "";

            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getMiddleName().equalsIgnoreCase("null"))
                middlename = property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getMiddleName() + " ";
            else
                middlename = "";

            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getLastName().equalsIgnoreCase("null"))
                lastname = property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getLastName() + " ";
            else
                lastname = "";

            first_last_name.setText(Utils.getEmptyValue(prefix + firstname + middlename + lastname));
            professional_txt.setText(Utils.getEmptyValue(property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getDesignation()));

            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getUnitName().equalsIgnoreCase("null"))
                unitname= property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getUnitName() + " ";
            else
                unitname = "";

            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getSubAreaName().equalsIgnoreCase("null"))
                subarea= property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getSubAreaName() + " ";
            else
                subarea = "";

            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getLocalityName().equalsIgnoreCase("null"))
                locality= property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getLocalityName() + " ";
            else
                locality = "";

            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getVillageName().equalsIgnoreCase("null"))
                village= property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getVillageName() + " ";
            else
                village = "";

            if(!property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getTownName().equalsIgnoreCase("null"))
                town= property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getAddresses().get(0).getTownName() + " ";
            else
                town = "";

            address.setText(Utils.getEmptyValue(unitname + subarea + locality + village + town));

            contactNumber = Utils.getEmptyValue(/*property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getPhones().get(0).getPhoneCode() +*/
                    property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getPhones().get(0).getPhoneNumber());
            email =Utils.getEmptyValue(property_data.get(0).getPropertyOtherQuestionses().get(0).getContact().getEmailAddresses().get(0).getAddress());


        } catch (Exception e) {

        }


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                } else {
                    callIntent.setData(Uri.parse("tel:" + contactNumber));
                    startActivity(callIntent);
                }
            }
        });

        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);

                intent.setType("text/html");
                // intent.putExtra(Intent.EXTRA_SUBJECT, "Download app");
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
                startActivity(Intent.createChooser(intent, "Choose"));
            }
        });

        return view;
    }

    public void onIntializeView(View view) {

        pan = (LinearLayout) view.findViewById(R.id.pan);
        pan.setVisibility(View.GONE);
        id_proof = (LinearLayout) view.findViewById(R.id.id_proof);
        id_proof.setVisibility(View.GONE);
        first_last_name = (TextView) view.findViewById(R.id.first_last_name);
        professional_txt = (TextView) view.findViewById(R.id.professional_txt);
        address = (TextView) view.findViewById(R.id.address);
        submited_behalf = (TextView) view.findViewById(R.id.submited_behalf);
        call = (ImageView) view.findViewById(R.id.call);
        msg = (ImageView) view.findViewById(R.id.msg);

    }


    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
