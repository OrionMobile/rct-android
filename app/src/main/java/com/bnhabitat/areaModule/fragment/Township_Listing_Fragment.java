package com.bnhabitat.areaModule.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.TownshipListingAdapter;
import com.bnhabitat.areaModule.model.StateListModel;
import com.bnhabitat.areaModule.model.TownshipListingModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.township.model.TownshipFileModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Township_Listing_Fragment extends Fragment implements TabChangeListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener  {

    View view;
    RecyclerView rvTownshipList;
    ArrayList<TownshipListingModel> townshipListingArrayList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    TownshipListingAdapter townshipListingAdapter;
    TextView no_data;
    String id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.township_listing_fragment, container, false);

        //PreferenceConnector.getInstance(getActivity()).savePreferences("FragmentClicked", "false");

        id = getArguments().getString("CityId");
        rvTownshipList = (RecyclerView) view.findViewById(R.id.rvTownshipList);
        no_data = (TextView) view.findViewById(R.id.no_data);
        linearLayoutManager = new LinearLayoutManager(getActivity());

        //onTownshipListing();
        return view;
    }

    @Override
    public void onResume() {
        onTownshipListing();
        super.onResume();
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    private void onTownshipListing() {

        new CommonSyncwithoutstatus(getActivity(),
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_TOWNSHIP_LIST +
                        id,
                "",
                "Loading...",this,
                Urls.URL_TOWNSHIP_LIST,
                Constants.GET)
                .execute();

    }

    @Override
    public void onResultListener(String result, String which) {

        if(null !=result && !result.equalsIgnoreCase("")){
            if(which.equalsIgnoreCase(Urls.URL_TOWNSHIP_LIST)){
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.optJSONArray("Result");
                    townshipListingArrayList.clear();

                    if(jsonArray.length()!=0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            TownshipListingModel townshipListingModel = new TownshipListingModel();
                            townshipListingModel.setId(jsonObject1.getString("Id"));
                            townshipListingModel.setName(jsonObject1.getString("Name"));

                            JSONArray jsonArray1 = jsonObject1.getJSONArray("Projects");
                            ArrayList<TownshipListingModel.Projects> projectsArrayList = new ArrayList<>();
                            projectsArrayList.clear();

                            for (int index = 0; index < jsonArray1.length(); index++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(index);
                                TownshipListingModel.Projects projects = new TownshipListingModel.Projects();

                                JSONArray jsonArray2 = jsonObject2.getJSONArray("ProjectCategoryTypes");
                                ArrayList<TownshipListingModel.Projects.ProjectCategoryTypes> projectCategoryTypes = new ArrayList<>();
                                projectCategoryTypes.clear();
                                for (int index1 = 0; index1 < jsonArray2.length(); index1++) {
                                    JSONObject jsonObject3 = jsonArray2.getJSONObject(index1);
                                    TownshipListingModel.Projects.ProjectCategoryTypes projectCategoryTypes1 = new TownshipListingModel.Projects.ProjectCategoryTypes();
                                    projectCategoryTypes1.setType(jsonObject3.getString("Type"));
                                    projectCategoryTypes.add(projectCategoryTypes1);
                                }
                                projects.setProjectCategoryTypes(projectCategoryTypes);
                                projectsArrayList.add(projects);
                            }
                            townshipListingModel.setProjects(projectsArrayList);

                            JSONArray jsonArray3 = jsonObject1.getJSONArray("TownshipFiles");
                            ArrayList<TownshipListingModel.TownshipFiles> townshipFiles = new ArrayList<>();
                            townshipFiles.clear();

                            for (int index2 = 0; index2 < jsonArray3.length(); index2++) {
                                JSONObject jsonObject2 = jsonArray3.getJSONObject(index2);

                                TownshipListingModel.TownshipFiles townshipFiles1 = new TownshipListingModel.TownshipFiles();
                                townshipFiles1.setCategory(jsonObject2.getString("Category"));
                                JSONObject jsonObject3 = jsonObject2.getJSONObject("File");
                                TownshipListingModel.TownshipFiles.Files files = new TownshipListingModel.TownshipFiles.Files();
                                files.setUrl(Urls.BASE_CONTACT_IMAGE_URL + jsonObject3.getString("Url"));
                                townshipFiles1.setFiles(files);
                                townshipFiles.add(townshipFiles1);
                            }
                            townshipListingModel.setTownshipFiles(townshipFiles);

                            townshipListingArrayList.add(townshipListingModel);
                        }

                        townshipListingAdapter = new TownshipListingAdapter(getActivity(), townshipListingArrayList);
                        rvTownshipList.setLayoutManager(linearLayoutManager);
                        rvTownshipList.setAdapter(townshipListingAdapter);
                        townshipListingAdapter.notifyDataSetChanged();
                    }else{
                        rvTownshipList.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public int getTownships() {
        int my_list = 0;
        if(townshipListingArrayList!=null)
            my_list = townshipListingArrayList.size();

        return my_list;
    }
}
