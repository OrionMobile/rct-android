package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.VillageListingAdapter;
import com.bnhabitat.areaModule.model.VillageListModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VillageList extends Fragment implements TabChangeListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener  {

    RecyclerView rvlist;
    TextView more_states, no_data;
    View view;
    String stateId, districtId;

    int totalRecords;
    private int pageSize = 10;

    VillageListingAdapter villageListingAdapter;
    ArrayList<VillageListModel> villageListModelArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.district_list, container, false);

        initialization(view);

        stateId = getArguments().getString("stateId");
        districtId = getArguments().getString("districtId");

        //onAreaDistrictVillageList();

        more_states.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pageSize = pageSize + 10;

                onAreaDistrictVillageList();
            }
        });

        villageListingAdapter = new VillageListingAdapter(getContext(), villageListModelArrayList);
        rvlist.setAdapter(villageListingAdapter);
        rvlist.setLayoutManager(new GridLayoutManager(getContext(), 2));
        villageListingAdapter.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onResume() {
        onAreaDistrictVillageList();
        super.onResume();
    }

    private void initialization(View view) {
        rvlist= (RecyclerView) view.findViewById(R.id.rvlist);
        more_states = (TextView) view.findViewById(R.id.more_states);
        no_data = (TextView) view.findViewById(R.id.no_data);
    }

    private void onAreaDistrictVillageList() {

        new CommonSyncwithoutstatus(getActivity(),
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_VILLAGE_LIST +
                        pageSize + "/" + stateId + "/" + districtId + "/0/0/0",
                "",
                "Loading...",this,
                Urls.URL_VILLAGE_LIST,
                Constants.GET)
                .execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_VILLAGE_LIST)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    JSONArray stateArray = jsonObject.getJSONArray("Result");
                    villageListModelArrayList.clear();

                    if(stateArray.length()!=0) {
                        for (int index = 0; index < stateArray.length(); index++) {
                            JSONObject jsonObject1 = stateArray.getJSONObject(index);

                            VillageListModel villageListModel = new VillageListModel();
                            villageListModel.setId(jsonObject1.getString("Id"));
                            villageListModel.setName(jsonObject1.getString("Name"));
                            JSONArray jsonArray = jsonObject1.optJSONArray("Pictures");
                            ArrayList<VillageListModel.Pictures> picturesArrayList = new ArrayList<>();
                            picturesArrayList.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                VillageListModel.Pictures pictures = new VillageListModel.Pictures();

                                if (jsonObject2.length() > 0) {
                                    pictures.setId(jsonObject2.getString("Id"));
                                    pictures.setUrl(Urls.BASE_CONTACT_IMAGE_URL + jsonObject2.getString("Url"));
                                    picturesArrayList.add(pictures);
                                } else {
                                    pictures.setId("");
                                    pictures.setUrl("");
                                    picturesArrayList.add(pictures);
                                }
                            }
                            villageListModel.setPictures(picturesArrayList);

                            villageListModel.setTotalRecords(jsonObject1.getString("TotalRecords"));

                            totalRecords = jsonObject1.getInt("TotalRecords");

                            villageListModelArrayList.add(villageListModel);

                        }
                        villageListingAdapter = new VillageListingAdapter(getContext(), villageListModelArrayList);
                        rvlist.setAdapter(villageListingAdapter);
                        rvlist.setLayoutManager(new GridLayoutManager(getContext(), 2));
                        villageListingAdapter.notifyDataSetChanged();

                        try {
                            if (totalRecords <= villageListModelArrayList.size() + 1)
                                more_states.setVisibility(View.GONE);
                            else
                                more_states.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        rvlist.setVisibility(View.GONE);
                        more_states.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
