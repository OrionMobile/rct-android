package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.ProjectListingAdapter;
import com.bnhabitat.areaModule.adapter.TownshipListingAdapter;
import com.bnhabitat.areaModule.model.ProjectListModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.ProjectListingModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Project_Listing_Fragment extends Fragment implements TabChangeListener, CommonSyncwithoutstatus.OnAsyncResultListener,CommonAsync.OnAsyncResultListener   {

    View view;
    RecyclerView rvProjectList;
    TextView no_data;
    ArrayList<ProjectListModel> projectListingArrayList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    ProjectListingAdapter projectListingAdapter;
    String id, stateId, districtId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.project_listing_fragment, container, false);

        id = getArguments().getString("CityId");
        districtId=getArguments().getString("DistrictId");
        stateId=getArguments().getString("StateId");

        rvProjectList = (RecyclerView) view.findViewById(R.id.rvProjectList);
        no_data = (TextView) view.findViewById(R.id.no_data);
        linearLayoutManager = new LinearLayoutManager(getActivity());

        //onProjectListing();
        return view;
    }

    @Override
    public void onResume() {
        onProjectListing();
        super.onResume();
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    private void onProjectListing() {

        new CommonSyncwithoutstatus(getActivity(),
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_PROJECT_LISTS +
                        id,
                "",
                "Loading...",this,
                Urls.URL_PROJECT_LISTS,
                Constants.GET)
                .execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if(null!=result && !result.equalsIgnoreCase("")){
            if(which.equalsIgnoreCase(Urls.URL_PROJECT_LISTS)){
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.optJSONArray("Result");
                    projectListingArrayList.clear();
                    if(jsonArray.length()!=0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            ProjectListModel projectListModel = new ProjectListModel();
                            projectListModel.setId(jsonObject1.getString("Id"));
                            projectListModel.setName(jsonObject1.getString("Name"));
                            projectListModel.setLocalityName(jsonObject1.getString("LocalityName"));
                            projectListModel.setZipCode(jsonObject1.getString("ZipCode"));
                            projectListModel.setStateName(jsonObject1.getString("StateName"));
                            projectListModel.setDistrictName(jsonObject1.getString("DistrictName"));
                            projectListModel.setTehsilName(jsonObject1.getString("TehsilName"));
                            projectListModel.setTownName(jsonObject1.getString("TownName"));

                            JSONArray jsonArray1 = jsonObject1.getJSONArray("ProjectCategoryTypes");
                            ArrayList<ProjectListModel.ProjectCategoryTypes> projectCategoryTypes = new ArrayList<>();
                            projectCategoryTypes.clear();
                            for (int index = 0; index < jsonArray1.length(); index++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(index);
                                ProjectListModel.ProjectCategoryTypes projectCategoryTypes1 = new ProjectListModel.ProjectCategoryTypes();
                                projectCategoryTypes1.setType(jsonObject2.getString("Type"));
                                projectCategoryTypes.add(projectCategoryTypes1);
                            }

                            projectListModel.setProjectCategoryTypes(projectCategoryTypes);

                            JSONArray jsonArray2 = jsonObject1.getJSONArray("ProjectFiles");
                            ArrayList<ProjectListModel.ProjectFiles> projectFiles = new ArrayList<>();
                            projectFiles.clear();
                            for (int i1 = 0; i1 < jsonArray2.length(); i1++) {
                                JSONObject jsonObject2 = jsonArray2.getJSONObject(i1);
                                ProjectListModel.ProjectFiles projectFiles1 = new ProjectListModel.ProjectFiles();
                                projectFiles1.setCategory(jsonObject2.getString("Category"));
                                projectFiles1.setFileUrl(Urls.BASE_CONTACT_IMAGE_URL + jsonObject2.getString("FileUrl"));
                                projectFiles.add(projectFiles1);
                            }

                            projectListModel.setProjectFiles(projectFiles);
                            projectListingArrayList.add(projectListModel);
                        }
                        projectListingAdapter = new ProjectListingAdapter(getActivity(), projectListingArrayList);
                        rvProjectList.setLayoutManager(linearLayoutManager);
                        rvProjectList.setAdapter(projectListingAdapter);
                        projectListingAdapter.notifyDataSetChanged();
                    }
                    else{
                        rvProjectList.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
