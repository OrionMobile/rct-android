package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;

import java.util.ArrayList;

public class Subarea_List_Fragment extends Fragment implements TabChangeListener {

    View view;
    TabLayout.Tab Tab1, Tab2;
    TabLayout tabLayout;
    int size;
    AboutCityFragment aboutCityFragment;
    String id, stateId, districtId, sectorCount, name;
    Subarea_Listing_Fragment subarea_listing_fragment = new Subarea_Listing_Fragment();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.subarea_list_fragment, container, false);

        id=getArguments().getString("CityId");
        name = getArguments().getString("CityName");
        districtId=getArguments().getString("DistrictId");
        stateId=getArguments().getString("StateId");
        sectorCount=getArguments().getString("sectorCount");

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        Tab1 = tabLayout.newTab().setText(sectorCount + " Subarea");
        Tab2 = tabLayout.newTab().setText("About " + name);
        Tab1.select();
        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Bundle bundle = new Bundle();
        bundle.putString("CityId", id);
        bundle.putString("DistrictId", districtId);
        bundle.putString("StateId", stateId);


        Subarea_Listing_Fragment subarea_listing_fragment = new Subarea_Listing_Fragment();
        subarea_listing_fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_subarea_listing, subarea_listing_fragment).commit();

        tabLayout.getTabAt(0);
        Tab1.select();
        bindWidgetsWithAnEvent();

        return view;
    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        final ImageView iv_location = (ImageView) getActivity().getWindow().findViewById(R.id.iv_location);
        Bundle bundle = new Bundle();
        bundle.putString("CityId", id);
        bundle.putString("DistrictId", districtId);
        bundle.putString("StateId", stateId);
        switch (tabPosition) {
            case 0:
                Subarea_Listing_Fragment subarea_listing_fragment = new Subarea_Listing_Fragment();
                iv_location.setVisibility(View.GONE);
                subarea_listing_fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_subarea_listing, subarea_listing_fragment).commit();

                break;
            case 1:
                aboutCityFragment = new AboutCityFragment();
                iv_location.setVisibility(View.VISIBLE);
                aboutCityFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_subarea_listing, aboutCityFragment).commit();

                break;
        }
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
