package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.InventoryModel_Area;

import java.util.ArrayList;

public class BuildView extends Fragment implements TabChangeListener {

    View view;
    LinearLayout plcs, faceroad, tehsils;
    TextView tower_num, floor_num, cover_area, super_area, builtup_area, carpet_area, plc, enterence_door_facing, facing_road, tehsil, about_prop, about_property, address;
    ArrayList<InventoryModel_Area> property_data;

    public BuildView() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.build_view_fragment, container, false);
        onIntializeView();
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null)
                property_data = (ArrayList<InventoryModel_Area>) bundle.getSerializable("property_data");

            tower_num.setText(property_data.get(0).getPropertyLocations().get(0).getTowerName());
            floor_num.setText(property_data.get(0).getPropertyLocations().get(0).getFloorNo());
            cover_area.setText(property_data.get(0).getPropertyAreas().get(0).getPlotOrCoverArea());
            super_area.setText(property_data.get(0).getPropertyAreas().get(0).getSuperArea());
            builtup_area.setText(property_data.get(0).getPropertyAreas().get(0).getBuiltUpArea());
            carpet_area.setText(property_data.get(0).getPropertyAreas().get(0).getCarpetArea());
            enterence_door_facing.setText(property_data.get(0).getPropertyAreas().get(0).getEnteranceDoorFacing());
            tehsil.setText(property_data.get(0).getPropertyAreas().get(0).getEnteranceDoorFacing());
            plcs.setVisibility(View.GONE);
            faceroad.setVisibility(View.GONE);
            tehsils.setVisibility(View.GONE);
            about_prop.setVisibility(View.GONE);
            about_property.setVisibility(View.GONE);
            facing_road.setText(property_data.get(0).getPropertyAreas().get(0).getStreetOrRoadType() + " " + property_data.get(0).getPropertyAreas().get(0).getRoadWidth());
            address.setText(property_data.get(0).getPropertyLocations().get(0).getSubArea() + " " + property_data.get(0).getPropertyLocations().get(0).getDistrict() + " " + property_data.get(0).getPropertyLocations().get(0).getState());
        } catch (Exception e) {

        }

        return view;
    }

    public void onIntializeView() {
        plcs = (LinearLayout) view.findViewById(R.id.plcs);
        faceroad = (LinearLayout) view.findViewById(R.id.faceroad);
        tehsils = (LinearLayout) view.findViewById(R.id.tehsils);
        tower_num = (TextView) view.findViewById(R.id.tower_num);
        floor_num = (TextView) view.findViewById(R.id.floor_num);
        cover_area = (TextView) view.findViewById(R.id.cover_area);
        super_area = (TextView) view.findViewById(R.id.super_area);
        builtup_area = (TextView) view.findViewById(R.id.builtup_area);
        carpet_area = (TextView) view.findViewById(R.id.carpet_area);
        plc = (TextView) view.findViewById(R.id.plc);
        enterence_door_facing = (TextView) view.findViewById(R.id.enterence_door_facing);
        facing_road = (TextView) view.findViewById(R.id.facing_road);
        tehsil = (TextView) view.findViewById(R.id.tehsil);
        about_prop = (TextView) view.findViewById(R.id.about_prop);
        about_property = (TextView) view.findViewById(R.id.about_property);
        address = (TextView) view.findViewById(R.id.address);
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
