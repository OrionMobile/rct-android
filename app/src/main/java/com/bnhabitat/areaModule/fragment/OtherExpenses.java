/*
package com.bnhabitat.areaModule.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.InventoryModel_Area;
import com.bnhabitat.ui.adapters.OtherExpensesViewAdapter;

import java.util.ArrayList;

public class OtherExpenses extends Fragment implements TabChangeListener {

    RecyclerView other_expense_list;
    View view;
    ArrayList<InventoryModel_Area> property_data;
    ArrayList<InventoryModel_Area.PropertyOtherExpences> propertyOtherExpences=new ArrayList<>();

    public OtherExpenses() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view=inflater.inflate(R.layout.other_expenses_view_fragment, container, false);
        other_expense_list=(RecyclerView)view.findViewById(R.id.other_expense_list);
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null)
                property_data = (ArrayList<InventoryModel_Area>) bundle.getSerializable("property_data");
            other_expense_list.setLayoutManager(new LinearLayoutManager(getActivity()));
            propertyOtherExpences = property_data.get(0).getPropertyOtherExpences();

            otherExpensesViewAdapter = new OtherExpensesAdapter(getActivity(),propertyOtherExpences);
            other_expense_list.setAdapter(otherExpensesViewAdapter);

        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }


    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
*/
