package com.bnhabitat.data.sizeunit;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;

import java.util.ArrayList;
import java.util.Date;


public class SizeUnitTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {
        }

        public static final String ID = "id";
        public static final String SERVER_ID = "server_id";
        public static final String SIZE_UNIT_ID = "SizeUnitId";
        public static final String STANDARD_UNIT_ID = "StandardUnitId";
        public static final String STATE_ID = "StateId";
        public static final String SIZE_UNIT_VALUE = "SizeUnitValue";
        public static final String UNIT_NAME = "unitName";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";

    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "size_unit";

    private final static SizeUnitTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new SizeUnitTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static SizeUnitTable getInstance() {

        return instance;
    }

    private SizeUnitTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.SERVER_ID,
            Fields.SIZE_UNIT_ID,
            Fields.STANDARD_UNIT_ID,
            Fields.STATE_ID,
            Fields.SIZE_UNIT_VALUE,
            Fields.UNIT_NAME,
            Fields.CREATED,
            Fields.MODIFIED,
    };


    public Long write(
            int serverId,
            int sizeUnitId,
            int standardUnitId,
            int stateId,
            String sizeUnitValue,
            String unitName,
            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.SIZE_UNIT_ID, sizeUnitId);
        mContentValues.put(Fields.STANDARD_UNIT_ID, standardUnitId);
        mContentValues.put(Fields.STATE_ID, stateId);
        mContentValues.put(Fields.SIZE_UNIT_VALUE, sizeUnitValue);
        mContentValues.put(Fields.UNIT_NAME, unitName);
        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateUnits(int serverId,
                            int sizeUnitId,
                            int standardUnitId,
                            int stateId,
                            String sizeUnitValue,
                            String unitName,
                            Date modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.SIZE_UNIT_ID, sizeUnitId);
        mContentValues.put(Fields.STANDARD_UNIT_ID, standardUnitId);
        mContentValues.put(Fields.STATE_ID, stateId);
        mContentValues.put(Fields.SIZE_UNIT_VALUE, sizeUnitValue);
        mContentValues.put(Fields.UNIT_NAME, unitName);
        mContentValues.put(Fields.MODIFIED, modified.getTime());
        openDatabase();
        db.update(NAME, mContentValues, Fields.SERVER_ID + " = ?", new String[]{String.valueOf(serverId)});
    }


//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}


    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE if not exists  " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.SIZE_UNIT_ID + " INTEGER,"
                + Fields.SERVER_ID + " TEXT,"
                + Fields.STANDARD_UNIT_ID + " TEXT,"
                + Fields.STATE_ID + " INTEGER,"
                + Fields.SIZE_UNIT_VALUE + " INTEGER,"
                + Fields.UNIT_NAME + " TEXT,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);

    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public boolean getSizeUnit(int serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;


        return false;
    }


    public String readCategories(int sizeUnitId, int stateId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SIZE_UNIT_ID + "=" + sizeUnitId + " and " + Fields.STATE_ID + "=" + stateId,
                null, null, null, null);

        String size = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
                //messageModeList.add(generateObjectFromCursor(cursor));
                size = generateObjectFromCursor(cursor).getSizeUnitValue();
                cursor.moveToNext();
            }
            cursor.close();
        }
        return size;
    }

    public ArrayList<SizeUnitModel> getSizes() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, Fields.UNIT_NAME, null, null);

        ArrayList<SizeUnitModel> sizeUnitModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
                //messageModeList.add(generateObjectFromCursor(cursor));
                sizeUnitModels.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return sizeUnitModels;
    }

    public SizeUnitModel getSizeUnitModel(int sizeUnitId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "=" + sizeUnitId,
                null, null, null, null);

        SizeUnitModel size = new SizeUnitModel();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
                //messageModeList.add(generateObjectFromCursor(cursor));
                size = generateObjectFromCursor(cursor);

                cursor.moveToNext();
            }
            cursor.close();
        }

        return size;

    }

    public String checkState(int stateId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.STATE_ID + "=" + stateId,
                null, null, null, null);

        String size = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
                //messageModeList.add(generateObjectFromCursor(cursor));
                size = generateObjectFromCursor(cursor).getSizeUnitValue();
                cursor.moveToNext();
            }
            cursor.close();
        }
        return size;
    }


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.SERVER_ID,
                Fields.SIZE_UNIT_ID,
                Fields.STANDARD_UNIT_ID,
                Fields.STATE_ID,
                Fields.SIZE_UNIT_VALUE,
                Fields.UNIT_NAME,
                Fields.CREATED,
                Fields.MODIFIED
        };
    }


    public SizeUnitModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        SizeUnitModel categoryModel = new SizeUnitModel();

        categoryModel.setSizeUnitId(cursor.getInt(cursor.getColumnIndex(Fields.SIZE_UNIT_ID)));
        categoryModel.setSizeUnitValue(cursor.getString(cursor.getColumnIndex(Fields.SIZE_UNIT_VALUE)));
        categoryModel.setStandardUnitId(cursor.getInt(cursor.getColumnIndex(Fields.STANDARD_UNIT_ID)));
        categoryModel.setStateId(cursor.getInt(cursor.getColumnIndex(Fields.STATE_ID)));
        categoryModel.setSizeName(cursor.getString(cursor.getColumnIndex(Fields.UNIT_NAME)));

        return categoryModel;
    }

}
