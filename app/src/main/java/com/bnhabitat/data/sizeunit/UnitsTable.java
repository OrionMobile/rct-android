package com.bnhabitat.data.sizeunit;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by developer15 on 3/28/2016.
 */
public class UnitsTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {
        }

        public static final String ID = "id";
        public static final String UNIT_ID = "UnitId";
        public static final String UNIT_TYPE_ID = "UnitTypeId";
        public static final String VALUE_PER_PIVOT_UNIT = "valuePerPivotUnit";
        public static final String UNIT_NAME = "UnitName";
        public static final String DESCRIPTION = "Description";
        public static final String IS_PIVOT_UNIT = "isPivotUnit";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";

    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "units";

    private final static UnitsTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new UnitsTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static UnitsTable getInstance() {

        return instance;
    }

    private UnitsTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }



    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.UNIT_ID,
            Fields.UNIT_TYPE_ID,
            Fields.VALUE_PER_PIVOT_UNIT,
            Fields.DESCRIPTION,
            Fields.IS_PIVOT_UNIT,
            Fields. UNIT_NAME,
            Fields.CREATED,
            Fields.MODIFIED,
    };




    public Long write(
            int unitId,
            int unitTypeId,
            int valuePerPivotUnit,
            String unitName,
            String description,
            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.UNIT_ID, unitId);
        mContentValues.put(Fields.UNIT_TYPE_ID, unitTypeId);
        mContentValues.put(Fields.VALUE_PER_PIVOT_UNIT, valuePerPivotUnit);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.UNIT_NAME, unitName);
        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateUnits(int unitId,
                             int unitTypeId,
                             int valuePerPivotUnit,
                             String unitName,
                             String description,
                            Date modified){

        ContentValues mContentValues  = new ContentValues();


        mContentValues.put(Fields.UNIT_TYPE_ID, unitTypeId);
        mContentValues.put(Fields.VALUE_PER_PIVOT_UNIT, valuePerPivotUnit);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.UNIT_NAME, unitName);
        mContentValues.put(Fields.UNIT_ID, unitId);
        mContentValues.put(Fields.MODIFIED, modified.getTime());
        openDatabase();
        db.update(NAME, mContentValues, Fields.UNIT_ID + " = ?", new String[] { String.valueOf(unitId) });
    }



    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.UNIT_ID + " INTEGER,"
                + Fields.UNIT_TYPE_ID + " TEXT,"
                + Fields.VALUE_PER_PIVOT_UNIT + " TEXT,"
                + Fields.DESCRIPTION + " INTEGER,"
                + Fields.IS_PIVOT_UNIT + " INTEGER,"
                + Fields.UNIT_NAME + " TEXT,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);

    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public boolean isUnitAvailable(int serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.UNIT_ID + "= " + serverId ,
                null, null, null, null);

        if (cursor.getCount()>0)
            return true;


        return false;
    }


//    public String readCategories(int sizeUnitId, int stateId) {
//
//        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
//                Fields.SIZE_UNIT_ID + "=" + sizeUnitId + " and " + Fields.STATE_ID + "=" + stateId,
//                null, null, null, null);
//
//        String size = "";
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
//                //messageModeList.add(generateObjectFromCursor(cursor));
//                size = generateObjectFromCursor(cursor).getSizeUnitValue();
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//        return size;
//    }

//    public SizeUnitModel getSizeUnitModel(int sizeUnitId){
//
//        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
//                Fields.SERVER_ID + "=" + sizeUnitId,
//                null, null, null, null);
//
//        SizeUnitModel size = new SizeUnitModel();
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
//                //messageModeList.add(generateObjectFromCursor(cursor));
//                size = generateObjectFromCursor(cursor);
//
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//
//        return size;
//
//    }

    public String getUnitName(int stateId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.UNIT_ID + "=" + stateId,
                null, null, null, null);

        String size = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {



                size = generateObjectFromCursor(cursor).getUnitName();
                cursor.moveToNext();
            }
            cursor.close();
        }
        return size;
    }


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.UNIT_ID,
                Fields.UNIT_TYPE_ID,
                Fields.VALUE_PER_PIVOT_UNIT,
                Fields.DESCRIPTION,
                Fields.IS_PIVOT_UNIT,
                Fields.UNIT_NAME,
                Fields.CREATED,
                Fields.MODIFIED
        };
    }

    public ArrayList<UnitsModel> getUnits() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null);

        ArrayList<UnitsModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;
    }

    public UnitsModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        UnitsModel unitsModel = new UnitsModel();

        unitsModel.setUnitName(cursor.getString(cursor.getColumnIndex(Fields.UNIT_NAME)));
        unitsModel.setUnitTypeId(cursor.getInt(cursor.getColumnIndex(Fields.UNIT_TYPE_ID)));
        unitsModel.setUnitId(cursor.getInt(cursor.getColumnIndex(Fields.UNIT_ID)));

        return unitsModel;
    }

}
