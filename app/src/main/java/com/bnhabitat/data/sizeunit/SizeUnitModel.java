package com.bnhabitat.data.sizeunit;


public class SizeUnitModel {
	
	int standardUnitId;
    int sizeUnitId;
    String sizeUnitValue;
    int stateId;
	String sizeName;

	public String getSizeName() {
		return sizeName;
	}

	public void setSizeName(String sizeName) {
		this.sizeName = sizeName;
	}

	public int getStandardUnitId() {
		return standardUnitId;
	}
	public void setStandardUnitId(int standardUnitId) {
		this.standardUnitId = standardUnitId;
	}
	public int getSizeUnitId() {
		return sizeUnitId;
	}
	public void setSizeUnitId(int sizeUnitId) {
		this.sizeUnitId = sizeUnitId;
	}
	public String getSizeUnitValue() {
		return sizeUnitValue;
	}
	public void setSizeUnitValue(String sizeUnitValue) {
		this.sizeUnitValue = sizeUnitValue;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
    
    

}
