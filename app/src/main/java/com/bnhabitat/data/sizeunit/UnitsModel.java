package com.bnhabitat.data.sizeunit;

/**
 * Created by developer15 on 3/28/2016.
 */
public class UnitsModel {


    int unitId;
    int unitTypeId;
    int valuePerPivotUnit;
    String unitName;
    String description;
    int isPivotUnit;


    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getUnitTypeId() {
        return unitTypeId;
    }

    public void setUnitTypeId(int unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    public int getValuePerPivotUnit() {
        return valuePerPivotUnit;
    }

    public void setValuePerPivotUnit(int valuePerPivotUnit) {
        this.valuePerPivotUnit = valuePerPivotUnit;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsPivotUnit() {
        return isPivotUnit;
    }

    public void setIsPivotUnit(int isPivotUnit) {
        this.isPivotUnit = isPivotUnit;
    }
}
