package com.bnhabitat.data.projects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.utils.Constants;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by developer15 on 3/21/2016.
 */
public class FavoriteProjectsTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {
        }

        public static final String ID = "id";
        public static final String PROJECT_ID = "project_id";
        public static final String DEVELOPER_ID = "developer_id";
        public static final String CITY_ID = "city_id";
        public static final String TITLE = "title";
        public static final String STATUS = "status";
        public static final String ADDRESS = "address";
        public static final String LOCALITY = "locality";
        public static final String MIN_PRICE = "min_pirce";
        public static final String MAX_PRICE = "max_pirce";
        public static final String SIZE = "size";
        public static final String PROPERTY_IMG = "property_img";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";
        public static final String IS_FAVOURITE = "is_favorite";
        public static final String IS_PREFERED = "is_preferred";
        public static final String FAV_MODIFIED = "fav_modified";
        public static final String BUILDER_COMPANY_ID = "builder_comp_id";
    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "favorite_projects";

    private final static FavoriteProjectsTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new FavoriteProjectsTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static FavoriteProjectsTable getInstance() {

        return instance;
    }

    private FavoriteProjectsTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.PROJECT_ID,
            Fields.DEVELOPER_ID,
            Fields.TITLE,
            Fields.STATUS,
            Fields.ADDRESS,
            Fields.LOCALITY,
            Fields.CITY_ID,
            Fields.MIN_PRICE,
            Fields.MAX_PRICE,
            Fields.SIZE,
            Fields.PROPERTY_IMG,
            Fields.CREATED,
            Fields.MODIFIED,
            Fields.IS_FAVOURITE,
            Fields.IS_PREFERED,
            Fields.FAV_MODIFIED,
            Fields.BUILDER_COMPANY_ID,

    };


    public Long write(
            String projectId,
            String cityId,
            String developerId,
            String title,
            String address,
            String locality,
            String minPrice,
            String maxPrice,
            String size,
            String propertyImg,
            String status,
            int isPreferred,
            int isFavorite,
            String builderCompId,
            Date favModified,
            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.PROJECT_ID, projectId);
        mContentValues.put(Fields.CITY_ID, cityId);
        mContentValues.put(Fields.DEVELOPER_ID, developerId);
        mContentValues.put(Fields.DEVELOPER_ID, developerId);
        mContentValues.put(Fields.TITLE, title);
        mContentValues.put(Fields.ADDRESS, address);
        mContentValues.put(Fields.LOCALITY, locality);
        mContentValues.put(Fields.MIN_PRICE, minPrice);
        mContentValues.put(Fields.MAX_PRICE, maxPrice);
        mContentValues.put(Fields.SIZE, size);
        mContentValues.put(Fields.PROPERTY_IMG, propertyImg);
        mContentValues.put(Fields.STATUS, status);
        mContentValues.put(Fields.IS_FAVOURITE, isFavorite);
        mContentValues.put(Fields.IS_PREFERED, isPreferred);
        mContentValues.put(Fields.FAV_MODIFIED, favModified.getTime());
        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());
        mContentValues.put(Fields.BUILDER_COMPANY_ID, builderCompId);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateFavoriteProjects(String projectId,
                                       String title,
                                       String address,
                                       String locality,

                                       String propertyImg,
                                       String status,
                                       String builderCompId,
                                       Date modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.PROJECT_ID, projectId);
        mContentValues.put(Fields.TITLE, title);
        mContentValues.put(Fields.ADDRESS, address);
        mContentValues.put(Fields.LOCALITY, locality);

        mContentValues.put(Fields.PROPERTY_IMG, propertyImg);
        mContentValues.put(Fields.STATUS, status);
        mContentValues.put(Fields.MODIFIED, modified.getTime());
        mContentValues.put(Fields.BUILDER_COMPANY_ID, builderCompId);

        openDatabase();
        db.update(NAME, mContentValues, Fields.PROJECT_ID + " = ?", new String[]{String.valueOf(projectId)});
    }

    public void updateAllFavoriteProject() {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.IS_PREFERED, 0);


        openDatabase();
        db.update(NAME, mContentValues, null, null);
    }

    public void updateFavoriteProject(String projectId,
                                      String cityId,
                                      String developerId,
                                      String title,
                                      String address,
                                      String locality,
                                      String propertyImg,
                                      String status,
                                      String builderCompId,
                                      Date modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.PROJECT_ID, projectId);
        mContentValues.put(Fields.TITLE, title);
        mContentValues.put(Fields.CITY_ID, cityId);
        mContentValues.put(Fields.DEVELOPER_ID, developerId);
        mContentValues.put(Fields.ADDRESS, address);
        mContentValues.put(Fields.LOCALITY, locality);
        mContentValues.put(Fields.IS_PREFERED, 1);
        mContentValues.put(Fields.PROPERTY_IMG, propertyImg);
        mContentValues.put(Fields.STATUS, status);
        mContentValues.put(Fields.MODIFIED, modified.getTime());
        mContentValues.put(Fields.BUILDER_COMPANY_ID, builderCompId);

        openDatabase();
        db.update(NAME, mContentValues, Fields.PROJECT_ID + " = ?", new String[]{String.valueOf(projectId)});
    }

    public void updateFavoriteProject(String projectId,
                                      int isFavorite,
                                      Date modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.PROJECT_ID, projectId);
        mContentValues.put(Fields.IS_FAVOURITE, isFavorite);
        mContentValues.put(Fields.FAV_MODIFIED, modified.getTime());
        // mContentValues.put(Fields.MODIFIED, modified.getTime());

        openDatabase();
        db.update(NAME, mContentValues, Fields.PROJECT_ID + " = ?", new String[]{String.valueOf(projectId)});
    }


//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}


    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.PROJECT_ID + " TEXT,"
                + Fields.CITY_ID + " TEXT,"
                + Fields.DEVELOPER_ID + " TEXT,"
                + Fields.TITLE + " TEXT,"
                + Fields.STATUS + " TEXT,"
                + Fields.ADDRESS + " TEXT,"
                + Fields.LOCALITY + " TEXT,"
                + Fields.MIN_PRICE + " TEXT,"
                + Fields.MAX_PRICE + " TEXT,"
                + Fields.SIZE + " TEXT,"
                + Fields.PROPERTY_IMG + " TEXT,"
                + Fields.IS_FAVOURITE + " TEXT,"
                + Fields.IS_PREFERED + " TEXT,"
                + Fields.BUILDER_COMPANY_ID + " TEXT,"
                + Fields.FAV_MODIFIED + " DATETIME,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);

    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        super.migrate(db, toVersion);

        switch (toVersion) {
            case Constants.DATABASE_VERSION_06:
                try{
                    db.execSQL("ALTER table " + NAME + " ADD COLUMN " + Fields.CITY_ID + " TEXT");
                    db.execSQL("ALTER table " + NAME + " ADD COLUMN " + Fields.DEVELOPER_ID + " TEXT");
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public void deleteRecord(String id) {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME + " where " + Fields.PROJECT_ID + " == " + id;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public boolean isCategoryAvailable(int serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.PROJECT_ID + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;


        return false;
    }


    public boolean isProjectAvailable(String projectId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.PROJECT_ID + "=" + projectId,
                null, null, null, null);


        if (cursor.getCount() > 0)
            return true;

        return false;
    }

    public void updatePreferredProject(String projectId,
                                      int isFavorite) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.PROJECT_ID, projectId);
        mContentValues.put(Fields.IS_PREFERED, isFavorite);

        openDatabase();
        db.update(NAME, mContentValues, Fields.PROJECT_ID + " = ?", new String[]{String.valueOf(projectId)});
    }

    public int isProjectFavorite(String projectId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.PROJECT_ID + "=" + projectId,
                null, null, null, null);

        int isFav = 0;

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                isFav = cursor.getInt(cursor.getColumnIndex(Fields.IS_FAVOURITE));

                cursor.moveToNext();
            }
            cursor.close();
        }

        return isFav;
    }

    public ArrayList<ProjectsModel> getFavouriteProjects() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.IS_FAVOURITE + " == 1",
                null, null, null, Fields.MODIFIED + " DESC");

        ArrayList<ProjectsModel> projectsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                projectsModels.add(generateObjectFromCursor(cursor));

                cursor.moveToNext();
            }
            cursor.close();
        }

        return projectsModels;
    }

    public ArrayList<ProjectsModel> getPreferredProjects() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.IS_PREFERED + " == 1",
                null, null, null, Fields.MODIFIED + " DESC");

        ArrayList<ProjectsModel> projectsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                projectsModels.add(generateObjectFromCursor(cursor));

                cursor.moveToNext();
            }
            cursor.close();
        }

        return projectsModels;
    }

    public ArrayList<ProjectsModel> getPreferredProjects(String cityId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CITY_ID + " == " + cityId+" and "+ Fields.IS_PREFERED+" == 1",
                null, null, null, Fields.MODIFIED + " DESC");

        ArrayList<ProjectsModel> projectsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                projectsModels.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return projectsModels;
    }

    public ArrayList<String> getPreferredProjectsImages() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.IS_PREFERED + " == 1",
                null, null, null, Fields.MODIFIED + " DESC");

        ArrayList<String> projectsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                projectsModels.add(generateObjectFromCursor(cursor).getTopImage1());

                cursor.moveToNext();
            }
            cursor.close();
        }

        return projectsModels;
    }

    public ArrayList<String> getPreferredProjectsImages(String cityId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CITY_ID + " == "+cityId+" and "+ Fields.IS_PREFERED + " == 1",
                null, null, null, Fields.MODIFIED + " DESC");

        ArrayList<String> projectsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                projectsModels.add(generateObjectFromCursor(cursor).getTopImage1());

                cursor.moveToNext();
            }
            cursor.close();
        }

        return projectsModels;
    }


    public String getProject(String id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.PROJECT_ID + "==" + id,
                null, null, null, null);

        String categoryName = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

//                categoryName = generateCommonObjectFromCursor(cursor).getTitle();

                cursor.moveToNext();
            }
            cursor.close();
        }

        return categoryName;
    }


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.PROJECT_ID,
                Fields.TITLE,
                Fields.STATUS,
                Fields.ADDRESS,
                Fields.LOCALITY,
                Fields.MIN_PRICE,
                Fields.MAX_PRICE,
                Fields.SIZE,
                Fields.PROPERTY_IMG,
                Fields.CREATED,
                Fields.MODIFIED,
                Fields.IS_FAVOURITE,
                Fields.IS_PREFERED,
                Fields.FAV_MODIFIED,
                Fields.BUILDER_COMPANY_ID,

        };
    }


    public ProjectsModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        ProjectsModel projectsModel = new ProjectsModel();

        projectsModel.setId(cursor.getString(cursor.getColumnIndex(Fields.PROJECT_ID)));
        projectsModel.setTitle(cursor.getString(cursor.getColumnIndex(Fields.TITLE)));
        projectsModel.setProjectStatus(cursor.getString(cursor.getColumnIndex(Fields.STATUS)));
        projectsModel.setRegion(cursor.getString(cursor.getColumnIndex(Fields.ADDRESS)));
        projectsModel.setLocality(cursor.getString(cursor.getColumnIndex(Fields.LOCALITY)));
        projectsModel.setMinPrice(cursor.getString(cursor.getColumnIndex(Fields.MIN_PRICE)));
        projectsModel.setMaxPrice(cursor.getString(cursor.getColumnIndex(Fields.MAX_PRICE)));
        projectsModel.setPropertyTitles(cursor.getString(cursor.getColumnIndex(Fields.SIZE)));
        projectsModel.setTopImage1(cursor.getString(cursor.getColumnIndex(Fields.PROPERTY_IMG)));
//        projectsModel.setCompId(cursor.getString(cursor.getColumnIndex(Fields.BUILDER_COMPANY_ID)));


        return projectsModel;
    }

//    public CommonDialogModel generateCommonObjectFromCursor(Cursor cursor) {
//        if (cursor == null) {
//            return null;
//        }
//        CommonDialogModel stateModel = new CommonDialogModel();
//
//        stateModel.setId(cursor.getString(cursor.getColumnIndex(Fields.PROJECT_ID)));
//        //   stateModel.setTitle(cursor.getString(cursor.getColumnIndex(Fields.PROJECT_JSON)));
//
//        return stateModel;
//    }

}
