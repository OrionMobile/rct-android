package com.bnhabitat.data.projects;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ProjectsModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by gourav on 11/20/2017.
 */

public class AllProjectsTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {
        }


        public static final String ID = "id";
        public static final String PROJECT_ID = "project_id";
        public static final String ALL_PROJECTS_JSON = "all_projects";
        public static final String PROJECT_JSON = "project_json";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";

    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "all_projects";

    private final static AllProjectsTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new AllProjectsTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static AllProjectsTable getInstance() {

        return instance;
    }

    private AllProjectsTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.PROJECT_ID,
            Fields.ALL_PROJECTS_JSON,
            Fields.PROJECT_JSON,
            Fields.CREATED,
            Fields.MODIFIED,
    };


    public Long write(

            String projectJson,

            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();



//        mContentValues.put(Fields.PROJECT_ID, projectId);
        mContentValues.put(Fields.ALL_PROJECTS_JSON, projectJson);


        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


//    public void updateProject(
//
//
//                             ){
//
//        ContentValues mContentValues  = new ContentValues();
//        mContentValues.put(Fields.PROJECT_ID, projectId);
//        mContentValues.put(Fields.ALL_PROJECTS_JSON, projectJson);
//
//        mContentValues.put(Fields.MODIFIED, modified.getTime());
//
//        openDatabase();
//        db.update(NAME, mContentValues, Fields.PROJECT_ID + " = ?", new String[] { String.valueOf(projectId) });
//    }


//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}


    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE if not exists  " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.PROJECT_ID + " TEXT,"

                + Fields.ALL_PROJECTS_JSON + " TEXT,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);

    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        super.migrate(db, toVersion);

        switch (toVersion){


        }
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public boolean isCategoryAvailable(int serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.PROJECT_ID + "= " + serverId ,
                null, null, null, null);

        if (cursor.getCount()>0)
            return true;


        return false;
    }


//    public String readCategories(int sizeUnitId, int stateId) {
//
//        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
//                Fields.SIZE_UNIT_ID + "=" + sizeUnitId + " and " + Fields.STATE_ID + "=" + stateId,
//                null, null, null, null);
//
//        String size = "";
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
//                //messageModeList.add(generateObjectFromCursor(cursor));
//                size = generateObjectFromCursor(cursor).getSizeUnitValue();
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//        return size;
//    }

    public String checkCategory(int categoryId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.PROJECT_ID + "=" + categoryId,
                null, null, null, null);

        String size = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

//
            }
            cursor.close();
        }
        return size;
    }

    public boolean isProjectAvailable(String projectId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.PROJECT_ID + "=" + projectId,
                null, null, null, null);


        if (cursor.getCount()>0)
            return true;

        return false;
    }

    public ArrayList<CommonDialogModel> getRecentProjects() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, Fields.MODIFIED+" DESC");

        ArrayList<CommonDialogModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateCommonObjectFromCursor(cursor));

                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;
    }

    public ArrayList<ProjectsModel> getRecentProjectsModel() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, Fields.MODIFIED+" DESC");

        ArrayList<ProjectsModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateProjectObjectFromCursor(cursor));

                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;
    }

    public String getProjects() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
               null,
                null, null, null, null);

        String categoryName = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                categoryName =  generateCommonObjectFromCursor(cursor).getTitle();

                cursor.moveToNext();
            }
            cursor.close();
        }

        return categoryName;
    }





    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.PROJECT_ID,
                Fields.ALL_PROJECTS_JSON,

                Fields.CREATED,
                Fields.MODIFIED,
        };
    }


//    public SizeUnitModel generateObjectFromCursor(Cursor cursor) {
//        if (cursor == null) {
//            return null;
//        }
//        SizeUnitModel categoryModel = new SizeUnitModel();
////
////        categoryModel.setSizeUnitId(cursor.getInt(cursor.getColumnIndex(Fields.SIZE_UNIT_ID)));
////        categoryModel.setSizeUnitValue(cursor.getString(cursor.getColumnIndex(Fields.SIZE_UNIT_VALUE)));
////        categoryModel.setStandardUnitId(cursor.getInt(cursor.getColumnIndex(Fields.STANDARD_UNIT_ID)));
////        categoryModel.setStateId(cursor.getInt(cursor.getColumnIndex(Fields.STATE_ID)));
//
//
//        return categoryModel;
//    }

    public CommonDialogModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        CommonDialogModel stateModel = new CommonDialogModel();

//        stateModel.setId(cursor.getString(cursor.getColumnIndex(Fields.PROJECT_ID)));
        stateModel.setTitle(cursor.getString(cursor.getColumnIndex(Fields.ALL_PROJECTS_JSON)));

        return stateModel;
    }


    public ProjectsModel generateProjectObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        ProjectsModel projectsModel = new ProjectsModel();

//        projectsModel.setId(cursor.getString(cursor.getColumnIndex(Fields.PROJECT_ID)));

        try {

            JSONObject jsonObject = new JSONObject(cursor.getString(cursor.getColumnIndex(Fields.ALL_PROJECTS_JSON)));
            JSONObject resultObject = jsonObject.getJSONObject("Result");

            projectsModel.setTitle(resultObject.toString());
//            projectsModel.setProjectStatus(resultObject.getString("ProjectStatus"));
//            projectsModel.setRegion(resultObject.getString("Region"));
//            projectsModel.setLocality(resultObject.getString("Locality"));
//            projectsModel.setMinPrice("0.0");
//            projectsModel.setMaxPrice("0.0");
//            projectsModel.setPropertyTitles("");
//            projectsModel.setTopImage1(resultObject.getString("topImage1"));
//            projectsModel.setCompId(resultObject.getString("builderCompanyId"));


        }catch (Exception e){
            e.printStackTrace();
        }

        return projectsModel;
    }


}

