package com.bnhabitat.data.location;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.CommonDialogModel;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by developer15 on 4/12/2016.
 */
public class RegionTable  extends AbstractTable {

    private static final class Fields implements BaseColumns {


        private Fields() {
        }

        public static final String ID = "id";
        public static final String CITY_ID = "city_id";
        public static final String REGION_NAME = "region_name";
        public static final String IS_ACTIVE = "isActive";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";

    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "regions";

    private final static RegionTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new RegionTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static RegionTable getInstance() {

        return instance;
    }

    private RegionTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }

    public static final String COUNTRY_ID = "country_id";
    public static final String STATE_ID = "stateId";
    public static final String DESCRIPTION = "citydescription";
    public static final String IS_ACTIVE = "isActive";
    public static final String DISTRICT = "District";


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.CITY_ID,
            Fields.REGION_NAME,
            Fields.IS_ACTIVE,
            Fields.CREATED,
            Fields.MODIFIED,
    };


    public Long write(
            int cityId,
            String cityName,
            int isActive,
            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.CITY_ID, cityId);
        mContentValues.put(Fields.REGION_NAME, cityName);
        mContentValues.put(Fields.IS_ACTIVE, isActive);
        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateCity(int cityId,
                           String cityName,
                           String isActive,
                           Date modified) {

        openDatabase();
        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.CITY_ID, cityId);
        mContentValues.put(Fields.REGION_NAME, cityName);
        mContentValues.put(Fields.IS_ACTIVE, isActive);
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        db.update(NAME, mContentValues, Fields.CITY_ID + " = ?", new String[]{String.valueOf(cityId)});
    }

//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}


    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.CITY_ID + " INTEGER ,"
                + Fields.REGION_NAME + " TEXT,"
                + Fields.IS_ACTIVE + " TEXT,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);

    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public boolean isRegionAvailable(String name, int cityId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CITY_ID + "== '" + cityId +"' and "+ Fields.REGION_NAME + "== '" + name+"'",
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;


        return false;
    }

    public ArrayList<CommonDialogModel> getRegions(String id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CITY_ID+" == '"+id+"'",
                null, null, null, null);

        ArrayList<CommonDialogModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;

    }

    public ArrayList<CommonDialogModel> getCities() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null);

        ArrayList<CommonDialogModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;

    }




//    public String  getCityId(String cityName) {
//
//        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
//                Fields.CITY_NAME+"=='"+cityName +"'",
//                null, null, null, null);
//
//        String size="";
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                //Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
//                //messageModeList.add(generateObjectFromCursor(cursor));
//                size=generateObjectFromCursor(cursor).getServerId();
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//        return size;
//    }


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.CITY_ID,
                Fields.REGION_NAME,

                Fields.IS_ACTIVE,

                Fields.CREATED,
                Fields.MODIFIED
        };
    }


    public StateModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        StateModel stateModel = new StateModel();

        stateModel.setStateId(cursor.getInt(cursor.getColumnIndex(Fields.CITY_ID)));
        stateModel.setStateName(cursor.getString(cursor.getColumnIndex(Fields.REGION_NAME)));
        stateModel.setServerId(cursor.getString(cursor.getColumnIndex(Fields.CITY_ID)));
        return stateModel;
    }


    public CommonDialogModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        CommonDialogModel stateModel = new CommonDialogModel();

        stateModel.setId(cursor.getString(cursor.getColumnIndex(Fields.CITY_ID)));
        stateModel.setTitle(cursor.getString(cursor.getColumnIndex(Fields.REGION_NAME)));

        return stateModel;
    }
}

