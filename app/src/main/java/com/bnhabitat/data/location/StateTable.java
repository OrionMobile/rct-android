package com.bnhabitat.data.location;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.CommonDialogModel;

import java.util.ArrayList;
import java.util.Date;


public class StateTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {
        }


        public static final String ID = "id";
        public static final String STATE_ID = "state_id";
        public static final String COUNTRY_ID = "countryId";
        public static final String STATE_NAME = "state_name";
        public static final String DESCRIPTION = "description";
        public static final String IS_ACTIVE = "isActive";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";

    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "states";

    private final static StateTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new StateTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static StateTable getInstance() {

        return instance;
    }

    private StateTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.STATE_ID,
            Fields.COUNTRY_ID,
            Fields.STATE_NAME,
            Fields.DESCRIPTION,
            Fields.IS_ACTIVE,
            Fields.CREATED,
            Fields.MODIFIED,
    };


    public Long write(
            int stateId,
            int countryId,
            String description,
            String isActive,
            String stateName,
            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.STATE_ID, stateId);
        mContentValues.put(Fields.STATE_NAME, stateName);
        mContentValues.put(Fields.IS_ACTIVE, isActive);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.COUNTRY_ID, countryId);
        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateState(int stateId,
                            int countryId,
                            String description,
                            String isActive,
                            String stateName,
                            Date modified) {
        openDatabase();
        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.STATE_ID, stateId);
        mContentValues.put(Fields.STATE_NAME, stateName);
        mContentValues.put(Fields.IS_ACTIVE, isActive);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.COUNTRY_ID, countryId);
        mContentValues.put(Fields.MODIFIED, modified.getTime());

        db.update(NAME, mContentValues, Fields.STATE_ID + " = ?", new String[]{String.valueOf(stateId)});
    }


    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.STATE_ID + " INTEGER,"
                + Fields.STATE_NAME + " TEXT,"
                + Fields.COUNTRY_ID + " TEXT,"
                + Fields.DESCRIPTION + " TEXT,"
                + Fields.IS_ACTIVE + " TEXT,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";
        DatabaseManager.execSQL(db, sql);
    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {

        openDatabase();
        String mSelectQuery = "Delete from " + NAME;
        db.execSQL(mSelectQuery);
        closeDatabase();

    }

    public boolean isStateAvailable(int serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.STATE_ID + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;
        return false;
    }

//    public ArrayList<StateModel> getStates() {
//
//        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
//                null,
//                null, null, null, null);
//
//        ArrayList<StateModel> stateModels = new ArrayList<>();
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                stateModels.add(generateObjectFromCursor(cursor));
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//
//        return stateModels;
//
//    }


    public ArrayList<CommonDialogModel> getStates() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null);

        ArrayList<CommonDialogModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;

    }


//	public String  getStates(int sizeUnitId,int stateId) {
//
//		Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
//				Fields.SIZE_UNIT_ID+"="+sizeUnitId +" and "+Fields.STATE_ID+"="+stateId,
//				null, null, null, null);
//
//		String size="";
//
//		if (cursor != null && cursor.moveToFirst()) {
//			while (!cursor.isAfterLast()) {
//
//				Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
//				//messageModeList.add(generateObjectFromCursor(cursor));
//				size=generateObjectFromCursor(cursor).getSizeUnitValue();
//				cursor.moveToNext();
//			}
//			cursor.close();
//		}
//		return size;
//	}


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.STATE_ID,
                Fields.COUNTRY_ID,
                Fields.STATE_NAME,
                Fields.DESCRIPTION,
                Fields.IS_ACTIVE,
                Fields.CREATED,
                Fields.MODIFIED,
        };
    }


    public StateModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        StateModel stateModel = new StateModel();

        stateModel.setStateId(cursor.getInt(cursor.getColumnIndex(Fields.STATE_ID)));
        stateModel.setStateName(cursor.getString(cursor.getColumnIndex(Fields.STATE_NAME)));

        return stateModel;
    }


    public CommonDialogModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        CommonDialogModel stateModel = new CommonDialogModel();

        stateModel.setId(cursor.getString(cursor.getColumnIndex(Fields.STATE_ID)));
        stateModel.setTitle(cursor.getString(cursor.getColumnIndex(Fields.STATE_NAME)));

        return stateModel;
    }

}

