package com.bnhabitat.data.location;

/**
 * Created by developer15 on 3/15/2016.
 */
public class CompaniesModel {
    String serverId;
    int id;
    String name;
    String aboutCompany;
    int CompanyOwner;
    int NumberOfProjects;
    String logo;

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAboutCompany() {
        return aboutCompany;
    }

    public void setAboutCompany(String aboutCompany) {
        this.aboutCompany = aboutCompany;
    }

    public int getCompanyOwner() {
        return CompanyOwner;
    }

    public void setCompanyOwner(int companyOwner) {
        CompanyOwner = companyOwner;
    }

    public int getNumberOfProjects() {
        return NumberOfProjects;
    }

    public void setNumberOfProjects(int numberOfProjects) {
        NumberOfProjects = numberOfProjects;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
