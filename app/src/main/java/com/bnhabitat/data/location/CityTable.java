package com.bnhabitat.data.location;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.CommonDialogModel;

import java.util.ArrayList;
import java.util.Date;


public class CityTable extends AbstractTable {

    private static final class Fields implements BaseColumns {


        private Fields() {
        }

        public static final String ID = "id";

        public static final String CITY_ID = "city_id";
        public static final String CITY_NAME = "city_name";
        public static final String COUNTRY_ID = "country_id";
        public static final String STATE_ID = "stateId";
        public static final String DESCRIPTION = "citydescription";
        public static final String IS_ACTIVE = "isActive";
        public static final String DISTRICT = "District";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";

    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "city";

    private final static CityTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new CityTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static CityTable getInstance() {

        return instance;
    }

    private CityTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }

    public static final String COUNTRY_ID = "country_id";
    public static final String STATE_ID = "stateId";
    public static final String DESCRIPTION = "citydescription";
    public static final String IS_ACTIVE = "isActive";
    public static final String DISTRICT = "District";


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.CITY_ID,
            Fields.CITY_NAME,

            Fields.COUNTRY_ID,
            Fields.STATE_ID,
            Fields.DESCRIPTION,
            Fields.IS_ACTIVE,
            Fields.DISTRICT,


            Fields.CREATED,
            Fields.MODIFIED,
    };


    public Long write(
            int cityId,
            String cityName,
            int countryId,
            int stateId,
            String description,
            String isActive,
            int district,
            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.CITY_ID, cityId);
        mContentValues.put(Fields.CITY_NAME, cityName);
        mContentValues.put(Fields.COUNTRY_ID, countryId);
        mContentValues.put(Fields.STATE_ID, stateId);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.IS_ACTIVE, isActive);
        mContentValues.put(Fields.DISTRICT, district);
        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateCity(int cityId,
                           String cityName,
                           int countryId,
                           int stateId,
                           String description,
                           String isActive,
                           int district,
                           Date modified) {

        openDatabase();
        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.CITY_ID, cityId);
        mContentValues.put(Fields.CITY_NAME, cityName);
        mContentValues.put(Fields.COUNTRY_ID, countryId);
        mContentValues.put(Fields.STATE_ID, stateId);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.IS_ACTIVE, isActive);
        mContentValues.put(Fields.DISTRICT, district);
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        db.update(NAME, mContentValues, Fields.CITY_ID + " = ?", new String[]{String.valueOf(cityId)});
    }

//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}


    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE if not exists  " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.CITY_ID + " INTEGER ,"
                + Fields.CITY_NAME + " TEXT,"
                + Fields.COUNTRY_ID + " TEXT,"
                + Fields.STATE_ID + " TEXT,"
                + Fields.DESCRIPTION + " TEXT,"
                + Fields.IS_ACTIVE + " TEXT,"
                + Fields.DISTRICT + " TEXT,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);

    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public boolean isCityAvailable(int cityId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CITY_ID + "= " + cityId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;


        return false;
    }

    public ArrayList<CommonDialogModel> getCities(String id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.STATE_ID+" == '"+id+"'",
                null, null, null, null);

        ArrayList<CommonDialogModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;

    }

    public ArrayList<CommonDialogModel> getCities() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null);

        ArrayList<CommonDialogModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;

    }


    public String getCityId(String cityName) {

		Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
				Fields.CITY_NAME+"=='"+cityName +"'",
				null, null, null, null);

		String size="";

		if (cursor != null && cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {

				//Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
				//messageModeList.add(generateObjectFromCursor(cursor));
				size=generateObjectFromCursor(cursor).getServerId();
				cursor.moveToNext();
			}
			cursor.close();
		}
		return size;
	}


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.CITY_ID,
                Fields.CITY_NAME,
                Fields.COUNTRY_ID,
                Fields.STATE_ID,
                Fields.DESCRIPTION,
                Fields.IS_ACTIVE,
                Fields.DISTRICT,
                Fields.CREATED,
                Fields.MODIFIED
        };
    }


    public StateModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        StateModel stateModel = new StateModel();

        stateModel.setStateId(cursor.getInt(cursor.getColumnIndex(Fields.CITY_ID)));
        stateModel.setStateName(cursor.getString(cursor.getColumnIndex(Fields.CITY_NAME)));
        stateModel.setServerId(cursor.getString(cursor.getColumnIndex(Fields.CITY_ID)));
        return stateModel;
    }


    public CommonDialogModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        CommonDialogModel stateModel = new CommonDialogModel();

        stateModel.setId(cursor.getString(cursor.getColumnIndex(Fields.CITY_ID)));
        stateModel.setTitle(cursor.getString(cursor.getColumnIndex(Fields.CITY_NAME)));

        return stateModel;
    }
}

