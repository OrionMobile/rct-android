package com.bnhabitat.data.location;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;

import java.util.Date;


public class TownshipTable  extends AbstractTable {

	private static final class Fields implements BaseColumns {

		private Fields() {
		}
		public static final String ID = "id";
		public static final String TOWNSHIP_ID = "township_id";
		public static final String TOWNSHIP_NAME = "township_name";
		public static final String CREATED = "created";
		public static final String MODIFIED = "modified";
	
	}
	
 
	private final DatabaseManager databaseManager;

	private static final String NAME = "township";

	private final static TownshipTable instance;
	SQLiteDatabase db;

	private void closeDatabase(){		
		//db.close();		
	}

	private void openDatabase(){

		db = databaseManager.getWritableDatabase();

	}

	static {
		instance = new TownshipTable(DatabaseManager.getInstance());
		DatabaseManager.getInstance().addTable(instance);

	}

	public static TownshipTable getInstance() {

		return instance;
	}

	private TownshipTable(DatabaseManager databaseManager) {

		this.databaseManager = databaseManager;
	}
	
	private static final String[] PROJECTION = new String[] {
		Fields.ID,
		Fields.TOWNSHIP_ID,
		Fields.TOWNSHIP_NAME,
		Fields.CREATED,
		Fields.MODIFIED,
	};

	

	public Long write(
			int cityId,
			String cityName,
			Date created,
			Date modified){

		ContentValues mContentValues  = new ContentValues();
		
		mContentValues.put(Fields.TOWNSHIP_ID, cityId);
		mContentValues.put(Fields.TOWNSHIP_NAME, cityName);
		mContentValues.put(Fields.CREATED, created.getTime());
		mContentValues.put(Fields.MODIFIED, modified.getTime());
		
		
		openDatabase();
		return db.insert(NAME,null, mContentValues);
	}


//	public void updateMessage(Long messageId, 
//			int status,
//			String messageServerId,
//			Date modified){
//	
//		ContentValues mContentValues  = new ContentValues();
//		mContentValues.put(Fields.MESSAGE_SERVER_ID, messageServerId);
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//		mContentValues.put(Fields.MODIFIED, modified.getTime());
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//	}

//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}



	@Override
	public void create(SQLiteDatabase db) {

		String sql = "CREATE TABLE if not exists " + NAME + " ("
				+ Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT," 
				+ Fields.TOWNSHIP_ID + " INTEGER UNIQUE,"
				+ Fields.TOWNSHIP_NAME + " TEXT,"
				+ Fields.CREATED + " DATETIME, "
				+ Fields.MODIFIED + " DATETIME" +
				");";

		
		DatabaseManager.execSQL(db, sql);

	}


	
	
	@Override
	protected String getTableName() {
		return NAME;
	}

	@Override
	protected String[] getProjection() {
		return PROJECTION;
	}

	public void deleteAllRecords(){
		

		openDatabase();

		String mSelectQuery = "Delete from "+ NAME ;

		db.execSQL(mSelectQuery);

		closeDatabase();
		
	}

	
//	public String  readCategories(int sizeUnitId,int stateId) {
//
//		Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(), 
//				Fields.SIZE_UNIT_ID+"="+sizeUnitId +" and "+Fields.STATE_ID+"="+stateId,
//				null, null, null, null);
//		
//		String size="";
//		
//		if (cursor != null && cursor.moveToFirst()) {
//			while (!cursor.isAfterLast()) {
//
//				Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
//				//messageModeList.add(generateObjectFromCursor(cursor));
//				size=generateObjectFromCursor(cursor).getSizeUnitValue();
//				cursor.moveToNext();
//			}
//			cursor.close();
//		}
//		return size;
//	}

	public String[] getAllColumns() {
		return new String[] {
				Fields.ID, 
				Fields.TOWNSHIP_ID, 
				Fields.TOWNSHIP_NAME,
				Fields.CREATED, 
				Fields.MODIFIED
		};
	}


   


	public StateModel generateObjectFromCursor(Cursor cursor) {
		if (cursor == null) {
			return null;
		}
		StateModel stateModel = new StateModel();

		stateModel.setStateId(cursor.getInt(cursor.getColumnIndex(Fields.TOWNSHIP_ID)));
		stateModel.setStateName(cursor.getString(cursor.getColumnIndex(Fields.TOWNSHIP_NAME)));

		return stateModel;
	}

}


