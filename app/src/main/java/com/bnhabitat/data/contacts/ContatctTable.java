package com.bnhabitat.data.contacts;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.GroupModel;

import java.util.ArrayList;

/**
 * Created by gourav on 8/8/2017.
 */

public class ContatctTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {

        }

        public static final String ID = "id";
        public static final String SERVER_ID = "server_id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String COMPANY_NAME = "company_name";
        public static final String PHONE_NUMBER = "phone_number";
        public static final String EMAIL_ADDRESS = "email_address";
        public static final String IS_SYNC = "is_sync";
        public static final String GROUP_ID = "group_Id";
        public static final String GROUP_NAME = "group_Name";
        public static final String DOB = "dob";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";
        public static final String IMAGE_URL = "image_url";
    }

    private final DatabaseManager databaseManager;

    private static final String NAME = "contacts";

    private final static ContatctTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {

        db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new ContatctTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static ContatctTable getInstance() {

        return instance;
    }

    private ContatctTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.SERVER_ID + " TEXT,"
                + Fields.FIRST_NAME + " TEXT,"
                + Fields.LAST_NAME + " TEXT,"
                + Fields.COMPANY_NAME + " TEXT,"
                + Fields.PHONE_NUMBER + " TEXT,"
                + Fields.EMAIL_ADDRESS + " TEXT,"
                + Fields.IS_SYNC + " NUMERIC,"
                + Fields.GROUP_ID + " TEXT,"
                + Fields.GROUP_NAME + " TEXT,"
                + Fields.DOB + " TEXT,"
                + Fields.IMAGE_URL + " TEXT,"
                + Fields.CREATED + " TEXT, "
                + Fields.MODIFIED + " TEXT" +
                ");";


        DatabaseManager.execSQL(db, sql);
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.SERVER_ID,

            Fields.FIRST_NAME,
            Fields.LAST_NAME,
            Fields.COMPANY_NAME,
            Fields.PHONE_NUMBER,
            Fields.EMAIL_ADDRESS,
            Fields.IS_SYNC,
            Fields.GROUP_ID,
            Fields.GROUP_NAME,
            Fields.DOB,
            Fields.IMAGE_URL,

            Fields.CREATED,
            Fields.MODIFIED,
    };

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public Long write(
            int serverId,
            String firstname,
            String lastname,
            String companyname,
            String phoneNumber,
            String email_address,
            int issync,
            String group_id,
            String group_name,
            String dob,
            String image_url,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.FIRST_NAME, firstname);
        mContentValues.put(Fields.LAST_NAME, lastname);
        mContentValues.put(Fields.COMPANY_NAME, companyname);
        mContentValues.put(Fields.PHONE_NUMBER, phoneNumber);
        mContentValues.put(Fields.EMAIL_ADDRESS, email_address);
        mContentValues.put(Fields.IS_SYNC, issync);
        mContentValues.put(Fields.GROUP_ID, group_id);
        mContentValues.put(Fields.GROUP_NAME, group_name);
        mContentValues.put(Fields.DOB, dob);
        mContentValues.put(Fields.IMAGE_URL, image_url);
        mContentValues.put(Fields.CREATED, created);
        mContentValues.put(Fields.MODIFIED, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }

    public void updateCategory(int serverId,
                               String firstname,
                               String lastname,
                               String companyname,
                               String phoneNumber,
                               String email_address,

                               int issync,
                               String group_id,
                               String group_name,
                               String dob,
                               String imageUrl,
                               String modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.FIRST_NAME, firstname);
        mContentValues.put(Fields.LAST_NAME, lastname);
        mContentValues.put(Fields.COMPANY_NAME, companyname);
        mContentValues.put(Fields.PHONE_NUMBER, phoneNumber);
        mContentValues.put(Fields.EMAIL_ADDRESS, email_address);
        mContentValues.put(Fields.IS_SYNC, issync);
        mContentValues.put(Fields.GROUP_ID, group_id);
        mContentValues.put(Fields.GROUP_NAME, group_name);
        mContentValues.put(Fields.DOB, dob);
        mContentValues.put(Fields.IMAGE_URL, imageUrl);
        mContentValues.put(Fields.MODIFIED, modified);

        openDatabase();
        db.update(NAME, mContentValues, Fields.SERVER_ID + " = ?", new String[]{String.valueOf(serverId)});
    }

    public ArrayList<ContactsModel> getContacts() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null, null);

        ArrayList<ContactsModel> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }
  public ArrayList<ContactsModel> getGroupContacts(String groupId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.GROUP_ID + "=="+groupId ,
                null, null, null, null, null);

        ArrayList<ContactsModel> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }

    public ArrayList<ContactsModel> getNoSynced() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.IS_SYNC + "==0",
                null, null, null, null, null);

        ArrayList<ContactsModel> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }

    public ArrayList<GroupModel> getUniquesGroups() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, Fields.GROUP_NAME, null, null, null);

        ArrayList<GroupModel> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                GroupModel contactsModel = generateGroupCommonObjectFromCursor(cursor);
                if (null != contactsModel.getId())
                    contactsModels.add(contactsModel);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }

    public boolean isContactAvailable(String serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;

        cursor.close();
        return false;
    }

    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.SERVER_ID,
                Fields.FIRST_NAME,
                Fields.LAST_NAME,
                Fields.COMPANY_NAME,
                Fields.PHONE_NUMBER,
                Fields.EMAIL_ADDRESS,
                Fields.IS_SYNC,
                Fields.GROUP_ID,
                Fields.GROUP_NAME,
                Fields.DOB,
                Fields.IMAGE_URL,
                Fields.CREATED,
                Fields.MODIFIED,
        };
    }

    public ContactsModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        ContactsModel contactsModel = new ContactsModel();

        contactsModel.setId(cursor.getString(cursor.getColumnIndex(Fields.SERVER_ID)));

        contactsModel.setFirstName(cursor.getString(cursor.getColumnIndex(Fields.FIRST_NAME)));
        contactsModel.setLastName(cursor.getString(cursor.getColumnIndex(Fields.LAST_NAME)));
        contactsModel.setCompanyName(cursor.getString(cursor.getColumnIndex(Fields.COMPANY_NAME)));
        contactsModel.setPhonenumber(cursor.getString(cursor.getColumnIndex(Fields.PHONE_NUMBER)));
        contactsModel.setEmail(cursor.getString(cursor.getColumnIndex(Fields.EMAIL_ADDRESS)));
        contactsModel.setGroup_id(cursor.getString(cursor.getColumnIndex(Fields.GROUP_ID)));
        contactsModel.setGroup_name(cursor.getString(cursor.getColumnIndex(Fields.GROUP_NAME)));
        contactsModel.setDob(cursor.getString(cursor.getColumnIndex(Fields.DOB)));
        contactsModel.setImageUrl(cursor.getString(cursor.getColumnIndex(Fields.IMAGE_URL)));


        return contactsModel;
    }


    public GroupModel generateGroupCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        GroupModel groupModel = new GroupModel();
        groupModel.setId(cursor.getString(cursor.getColumnIndex(Fields.GROUP_ID)));
        groupModel.setName(cursor.getString(cursor.getColumnIndex(Fields.GROUP_NAME)));
        return groupModel;
    }

    public void updateSync(
            String orderId
    ) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.IS_SYNC, 1);
        openDatabase();
        db.update(NAME, mContentValues, Fields.PHONE_NUMBER + " = ?", new String[]{orderId});
    }

    public void updateAllSync(

    ) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.IS_SYNC, 1);
        openDatabase();
        db.update(NAME, mContentValues, null, null);
    }
}
