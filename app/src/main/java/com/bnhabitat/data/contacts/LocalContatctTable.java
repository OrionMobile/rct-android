package com.bnhabitat.data.contacts;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.ContactsModel;

import java.util.ArrayList;

/**
 * Created by gourav on 8/8/2017.
 */

public class LocalContatctTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {

        }

        public static final String ID = "id";
        public static final String SERVER_ID = "server_id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String COMPANY_NAME = "company_name";
        public static final String PHONE_NUMBER = "phone_number";
        public static final String IS_SYNC = "is_sync";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";
    }

    private final DatabaseManager databaseManager;

    private static final String NAME = "localcontacts";

    private final static LocalContatctTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {

        db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new LocalContatctTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static LocalContatctTable getInstance() {

        return instance;
    }

    private LocalContatctTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.SERVER_ID + " TEXT,"
                + Fields.FIRST_NAME + " TEXT,"
                + Fields.LAST_NAME + " TEXT,"
                + Fields.COMPANY_NAME + " TEXT,"
                + Fields.PHONE_NUMBER + " TEXT,"
                + Fields.IS_SYNC + " NUMERIC,"
                + Fields.CREATED + " TEXT, "
                + Fields.MODIFIED + " TEXT" +
                ");";


        DatabaseManager.execSQL(db, sql);
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.SERVER_ID,

            Fields.FIRST_NAME,
            Fields.LAST_NAME,
            Fields.COMPANY_NAME,
            Fields.PHONE_NUMBER,
            Fields.IS_SYNC,
            Fields.CREATED,
            Fields.MODIFIED,
    };

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public Long write(
            int serverId,
            String firstname,
            String lastname,
            String companyname,
            String phoneNumber,
            int issync,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.FIRST_NAME, firstname);
        mContentValues.put(Fields.LAST_NAME, lastname);
        mContentValues.put(Fields.COMPANY_NAME, companyname);
        mContentValues.put(Fields.PHONE_NUMBER, phoneNumber);
        mContentValues.put(Fields.IS_SYNC, 0);
        mContentValues.put(Fields.CREATED, created);
        mContentValues.put(Fields.MODIFIED, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }
 public Long write(
            long serverId,
            String firstname,
            String lastname,
            String companyname,
            String phoneNumber,
            int issync,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.FIRST_NAME, firstname);
        mContentValues.put(Fields.LAST_NAME, lastname);
        mContentValues.put(Fields.COMPANY_NAME, companyname);
        mContentValues.put(Fields.PHONE_NUMBER, phoneNumber);
        mContentValues.put(Fields.IS_SYNC, 0);
        mContentValues.put(Fields.CREATED, created);
        mContentValues.put(Fields.MODIFIED, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }

    public void updateCategory(int serverId,
                               String firstname,
                               String lastname,
                               String companyname,
                               String phoneNumber,
                               int issync,

                               String modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.FIRST_NAME, firstname);
        mContentValues.put(Fields.LAST_NAME, lastname);
        mContentValues.put(Fields.COMPANY_NAME, companyname);
        mContentValues.put(Fields.PHONE_NUMBER, phoneNumber);
        mContentValues.put(Fields.IS_SYNC, issync);
        mContentValues.put(Fields.MODIFIED, modified);

        openDatabase();
        db.update(NAME, mContentValues, Fields.SERVER_ID + " = ?", new String[]{String.valueOf(serverId)});
    }

    public ArrayList<ContactsModel> getContacts() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.IS_SYNC + "==0",
                null, null, null, null, null);

        ArrayList<ContactsModel> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }

    public ArrayList<ContactsModel> getNoSynced() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.IS_SYNC + "==0",
                null, null, null, null, null);

        ArrayList<ContactsModel> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }

    public boolean isContactAvailable(String serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;

        cursor.close();
        return false;
    }

    public boolean isPhoneAvailable(String phoneNumber) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.PHONE_NUMBER + "= '" + phoneNumber+"'",
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;

        cursor.close();
        return false;
    }

    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.SERVER_ID,
                Fields.FIRST_NAME,
                Fields.LAST_NAME,
                Fields.COMPANY_NAME,
                Fields.PHONE_NUMBER,
                Fields.IS_SYNC,
                Fields.CREATED,
                Fields.MODIFIED,
        };
    }

    public ContactsModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        ContactsModel contactsModel = new ContactsModel();

        contactsModel.setId(cursor.getString(cursor.getColumnIndex(Fields.SERVER_ID)));

        contactsModel.setFirstName(cursor.getString(cursor.getColumnIndex(Fields.FIRST_NAME)));
        contactsModel.setLastName(cursor.getString(cursor.getColumnIndex(Fields.LAST_NAME)));
        contactsModel.setCompanyName(cursor.getString(cursor.getColumnIndex(Fields.COMPANY_NAME)));
        contactsModel.setPhonenumber(cursor.getString(cursor.getColumnIndex(Fields.PHONE_NUMBER)));
//        contactsModel.setSync(cursor.getString(cursor.getColumnIndex(Fields.PHONE_NUMBER)));


        return contactsModel;
    }

    public void updateSync(
            String phonenumber
    ) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.IS_SYNC, 1);
        openDatabase();
        db.update(NAME, mContentValues, Fields.PHONE_NUMBER + " = ?", new String[]{phonenumber});
    }

    public void updateAllSync(

    ) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.IS_SYNC, 1);
        openDatabase();
        db.update(NAME, mContentValues, null, null);
    }
}
