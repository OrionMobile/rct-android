package com.bnhabitat.data.contacts;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.ContactsModel;

import java.util.ArrayList;

/**
 * Created by gourav on 8/8/2017.
 */

public class LocalEmailTable extends AbstractTable {



    private static final class Fields implements BaseColumns {

        private Fields() {

        }
        public static final String ID = "id";
        public static final String CONTACT_ID = "contact_id";
        public static final String EMAIL_ID = "email_id";
        public static final String EMAIL_ADDRESS = "email_address";
        public static final String IS_SYNCED = "is_synced";
        public static final String CREATED_DATE = "created_date";
        public static final String UPDATED_DATE = "updated_date";
    }
    private final DatabaseManager databaseManager;

    private static final String NAME = "localemails";

    private final static LocalEmailTable instance;
    SQLiteDatabase db;

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }
    static {
        instance = new LocalEmailTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static LocalEmailTable getInstance() {

        return instance;
    }

    private LocalEmailTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.CONTACT_ID + " TEXT,"
                + Fields.EMAIL_ID + " TEXT,"
                + Fields.EMAIL_ADDRESS + " TEXT,"
                + Fields.IS_SYNCED + " TEXT,"

                + Fields.CREATED_DATE + " DATETIME, "
                + Fields.UPDATED_DATE + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);
    }
    public Long write(
            int contact_id,
            int email_id,
            String emailaddress,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.CONTACT_ID, contact_id);
        mContentValues.put(Fields.EMAIL_ID, email_id);
//
        mContentValues.put(Fields.EMAIL_ADDRESS, emailaddress);
        mContentValues.put(Fields.CREATED_DATE, created);
        mContentValues.put(Fields.UPDATED_DATE, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }
    public Long write(
            long contact_id,
            int email_id,
            String emailaddress,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.CONTACT_ID, contact_id);
        mContentValues.put(Fields.EMAIL_ID, email_id);
//
        mContentValues.put(Fields.EMAIL_ADDRESS, emailaddress);
        mContentValues.put(Fields.CREATED_DATE, created);
        mContentValues.put(Fields.UPDATED_DATE, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }
    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.CONTACT_ID,

            Fields.EMAIL_ID,
            Fields.EMAIL_ADDRESS,
            Fields.IS_SYNCED,
            Fields.CREATED_DATE,
            Fields.UPDATED_DATE,

    };
    public void updateEmail(
            int email_id,
            String emailaddress,

            String modified){

        ContentValues mContentValues  = new ContentValues();


//
        mContentValues.put(Fields.EMAIL_ADDRESS, emailaddress);

        mContentValues.put(Fields.UPDATED_DATE, modified);

        openDatabase();
        db.update(NAME, mContentValues, Fields.EMAIL_ID + " = ?", new String[] { String.valueOf(email_id) });
    }
    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }
    public ArrayList<ContactsModel.Email> getEmails(String contact_id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CONTACT_ID + "=" + contact_id,
                null, null, null, null, null);

        ArrayList<ContactsModel.Email> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }
    public ArrayList<ContactsModel.Email> getEmails() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null, null);

        ArrayList<ContactsModel.Email> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.CONTACT_ID,
                Fields.EMAIL_ID,
                Fields.EMAIL_ADDRESS,
                Fields.CREATED_DATE,
                Fields.UPDATED_DATE,
        };
    }

    public ContactsModel.Email generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        ContactsModel.Email contactsModel = new ContactsModel().new Email();

        contactsModel.setId(cursor.getString(cursor.getColumnIndex(Fields.EMAIL_ID)));

        contactsModel.setContactId(cursor.getString(cursor.getColumnIndex(Fields.CONTACT_ID)));
        contactsModel.setAddress(cursor.getString(cursor.getColumnIndex(Fields.EMAIL_ADDRESS)));
//        contactsModel.setC(cursor.getString(cursor.getColumnIndex(Fields.COMPANY_NAME)));
//        contactsModel.setPhonenumber(cursor.getString(cursor.getColumnIndex(Fields.PHONE_NUMBER)));


        return contactsModel;
    }
}
