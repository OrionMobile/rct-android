package com.bnhabitat.data.contacts;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.ContactsModel;

import java.util.ArrayList;

/**
 * Created by gourav on 8/17/2017.
 */

public class LocalPhoneTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {

        }

        public static final String ID = "id";
        public static final String CONTACT_ID = "contact_id";
        public static final String NUMBER_ID = "number_id";
        public static final String PHONE_NUMBER = "phone_number";
        public static final String IS_SYNCED = "is_synced";
        public static final String CREATED_DATE = "created_date";
        public static final String UPDATED_DATE = "updated_date";
    }

    private final DatabaseManager databaseManager;

    private static final String NAME = "localphones";

    private final static LocalPhoneTable instance;
    SQLiteDatabase db;

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new LocalPhoneTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static LocalPhoneTable getInstance() {

        return instance;
    }

    private LocalPhoneTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }

    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.CONTACT_ID,

            Fields.NUMBER_ID,
            Fields.PHONE_NUMBER,
            Fields.IS_SYNCED,
            Fields.CREATED_DATE,
            Fields.UPDATED_DATE,

    };

    @Override
    public void create(SQLiteDatabase db) {
        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + LocalPhoneTable.Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + LocalPhoneTable.Fields.CONTACT_ID + " TEXT,"
                + LocalPhoneTable.Fields.NUMBER_ID + " TEXT,"
                + LocalPhoneTable.Fields.PHONE_NUMBER + " TEXT,"
                + LocalPhoneTable.Fields.IS_SYNCED + " TEXT,"

                + LocalPhoneTable.Fields.CREATED_DATE + " DATETIME, "
                + LocalPhoneTable.Fields.UPDATED_DATE + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);
    }

    public Long write(
            int contact_id,
            int number_id,
            String phoneNumber,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(LocalPhoneTable.Fields.CONTACT_ID, contact_id);
        mContentValues.put(LocalPhoneTable.Fields.NUMBER_ID, number_id);
//
        mContentValues.put(LocalPhoneTable.Fields.PHONE_NUMBER, phoneNumber);
        mContentValues.put(LocalPhoneTable.Fields.CREATED_DATE, created);
        mContentValues.put(LocalPhoneTable.Fields.UPDATED_DATE, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }

    public Long write(
            long contact_id,
            int number_id,
            String phoneNumber,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(LocalPhoneTable.Fields.CONTACT_ID, contact_id);
        mContentValues.put(LocalPhoneTable.Fields.NUMBER_ID, number_id);
//
        mContentValues.put(LocalPhoneTable.Fields.PHONE_NUMBER, phoneNumber);
        mContentValues.put(LocalPhoneTable.Fields.CREATED_DATE, created);
        mContentValues.put(LocalPhoneTable.Fields.UPDATED_DATE, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }
//    public ArrayList<ContactsModel.Phones> getPhones(String contact_id) {
//
//        Cursor cursor = databaseManager.getReadableDatabase().rawQuery("SELECT * FROM " + NAME + " WHERE " + Fields.CONTACT_ID + " = ?", new String[]{contact_id});
//        ArrayList<ContactsModel.Phones> contactsModels = new ArrayList<>();
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                contactsModels.add(generateCommonObjectFromCursor(cursor));
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//
//        return contactsModels;
//    }

    public ArrayList<ContactsModel.Phones> getPhones(String contact_id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                LocalPhoneTable.Fields.CONTACT_ID + "=" + contact_id,
                null, null, null, null, null);

        ArrayList<ContactsModel.Phones> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }

    public ArrayList<ContactsModel.Phones> getPhones() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null, null);

        ArrayList<ContactsModel.Phones> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }


    public String[] getAllColumns() {
        return new String[]{
                LocalPhoneTable.Fields.ID,
                LocalPhoneTable.Fields.CONTACT_ID,
                LocalPhoneTable.Fields.NUMBER_ID,
                LocalPhoneTable.Fields.PHONE_NUMBER,
                LocalPhoneTable.Fields.CREATED_DATE,
                LocalPhoneTable.Fields.UPDATED_DATE,
        };
    }

    public ContactsModel.Phones generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        ContactsModel.Phones contactsModel = new ContactsModel().new Phones();

        contactsModel.setId(cursor.getString(cursor.getColumnIndex(LocalPhoneTable.Fields.NUMBER_ID)));

        contactsModel.setContactId(cursor.getString(cursor.getColumnIndex(LocalPhoneTable.Fields.CONTACT_ID)));
        contactsModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(LocalPhoneTable.Fields.PHONE_NUMBER)));
//        contactsModel.setC(cursor.getString(cursor.getColumnIndex(Fields.COMPANY_NAME)));
//        contactsModel.setPhonenumber(cursor.getString(cursor.getColumnIndex(Fields.PHONE_NUMBER)));


        return contactsModel;
    }
}
