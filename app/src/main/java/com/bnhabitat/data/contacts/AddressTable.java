package com.bnhabitat.data.contacts;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.ui.adapters.AddressAdapter;

import java.util.ArrayList;

/**
 * Created by gourav on 8/8/2017.
 */

public class AddressTable extends AbstractTable {



    private static final class Fields implements BaseColumns {

        private Fields() {

        }
        public static final String ID = "id";
        public static final String CONTACT_ID = "contact_id";
        public static final String ADDRESS_ID = "address_id";
        public static final String ADDRESS_Line1 = "address_line1";
        public static final String ADDRESS_Line2 = "address_line2";
        public static final String ADDRESS_Line3 = "address_line3";
        public static final String DEVELOPER = "developer";
        public static final String TOWER = "tower";
        public static final String FLOOR = "floor";
        public static final String UNIT_NO = "unit_no";
        public static final String CITY = "city";
        public static final String ZIPCODE = "zipcode";
        public static final String COUNTRY = "country";
        public static final String STATE = "state";
        public static final String IS_SYNCED = "is_synced";
        public static final String CREATED_DATE = "created_date";
        public static final String UPDATED_DATE = "updated_date";
    }

    private final DatabaseManager databaseManager;

    private static final String NAME = "address";

    private final static AddressTable instance;
    SQLiteDatabase db;

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }
    static {
        instance = new AddressTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static AddressTable getInstance() {

        return instance;
    }


    private AddressTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }
    @Override
    public void create(SQLiteDatabase db) {
        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.CONTACT_ID + " TEXT,"
                + Fields.ADDRESS_ID + " TEXT,"
                + Fields.ADDRESS_Line1 + " TEXT,"
                + Fields.ADDRESS_Line2 + " TEXT,"
                + Fields.ADDRESS_Line3 + " TEXT,"
                + Fields.DEVELOPER + " TEXT,"
                + Fields.TOWER + " TEXT,"
                + Fields.FLOOR + " TEXT,"
                + Fields.UNIT_NO + " TEXT,"
                + Fields.CITY + " TEXT,"
                + Fields.ZIPCODE + " TEXT,"
                + Fields.COUNTRY + " TEXT,"
                + Fields.STATE + " TEXT,"
                + Fields.IS_SYNCED + " TEXT,"

                + Fields.CREATED_DATE + " DATETIME, "
                + Fields.UPDATED_DATE + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);
    }
    public Long write(
            int contact_id,
            int address_id,
            String address1,
            String address2,
            String address3,
            String developername,
            String tower,
            String floor,
            String unit_no,
            String city,
            String zip,
            String country,
            String state,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.CONTACT_ID, contact_id);
        mContentValues.put(Fields.ADDRESS_ID, address_id);
//
        mContentValues.put(Fields.ADDRESS_Line1, address1);
        mContentValues.put(Fields.ADDRESS_Line2, address2);
        mContentValues.put(Fields.ADDRESS_Line3, address3);
        mContentValues.put(Fields.DEVELOPER, developername);
        mContentValues.put(Fields.TOWER, tower);
        mContentValues.put(Fields.FLOOR, floor);
        mContentValues.put(Fields.UNIT_NO, unit_no);
        mContentValues.put(Fields.CITY, city);
        mContentValues.put(Fields.ZIPCODE, zip);
        mContentValues.put(Fields.COUNTRY, country);
        mContentValues.put(Fields.STATE, state);
        mContentValues.put(Fields.CREATED_DATE, created);
        mContentValues.put(Fields.UPDATED_DATE, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }

    public void updateAddress(

            int address_id,
            String address1,
            String address2,
            String address3,
            String developername,
            String tower,
            String floor,
            String unit_no,
            String city,
            String zip,
            String country,
            String state,

            String modified){

        ContentValues mContentValues  = new ContentValues();


//


        mContentValues.put(Fields.ADDRESS_ID, address_id);
//
        mContentValues.put(Fields.ADDRESS_Line1, address1);
        mContentValues.put(Fields.ADDRESS_Line2, address2);
        mContentValues.put(Fields.ADDRESS_Line2, address2);
        mContentValues.put(Fields.ADDRESS_Line3, address3);
        mContentValues.put(Fields.DEVELOPER, developername);
        mContentValues.put(Fields.TOWER, tower);
        mContentValues.put(Fields.FLOOR, floor);
        mContentValues.put(Fields.UNIT_NO, unit_no);
        mContentValues.put(Fields.CITY, city);
        mContentValues.put(Fields.ZIPCODE, zip);
        mContentValues.put(Fields.COUNTRY, country);
        mContentValues.put(Fields.STATE, state);

        mContentValues.put(Fields.UPDATED_DATE, modified);

        openDatabase();
        db.update(NAME, mContentValues, Fields.ADDRESS_ID + " = ?", new String[] { String.valueOf(address_id) });
    }
    public boolean isAddressAvailable(String serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.ADDRESS_ID + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;

        cursor.close();
        return false;
    }
    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.CONTACT_ID,

            Fields.ADDRESS_ID,
            Fields.ADDRESS_Line1,
            Fields.ADDRESS_Line2,
            Fields.ADDRESS_Line3,
            Fields.DEVELOPER,
            Fields.TOWER,
            Fields.FLOOR,
            Fields.UNIT_NO,
            Fields.CITY,
            Fields.ZIPCODE,
            Fields.COUNTRY,
            Fields.STATE,
            Fields.IS_SYNCED,
            Fields.CREATED_DATE,
            Fields.UPDATED_DATE,

    };
    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }
    public ArrayList<AddressModel> addressesArrayList(String contact_id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CONTACT_ID + "=" + contact_id,
                null, null, null, null, null);

        ArrayList<AddressModel> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }
    public ArrayList<AddressModel> getEmails() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null, null);

        ArrayList<AddressModel> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }

    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.CONTACT_ID,
                Fields.ADDRESS_ID,
                Fields.ADDRESS_Line1,
                Fields.ADDRESS_Line2,
                Fields.ADDRESS_Line3,
                Fields.DEVELOPER,
                Fields.TOWER,
                Fields.FLOOR,
                Fields.UNIT_NO,
                Fields.CITY,
                Fields.ZIPCODE,
                Fields.COUNTRY,
                Fields.STATE,
                Fields.CREATED_DATE,
                Fields.UPDATED_DATE,
        };
    }
    public AddressModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        AddressModel addresses = new  AddressModel();

        addresses.setId(cursor.getString(cursor.getColumnIndex(Fields.ADDRESS_ID)));

        addresses.setContactId(cursor.getString(cursor.getColumnIndex(Fields.CONTACT_ID)));
        addresses.setAddress1(cursor.getString(cursor.getColumnIndex(Fields.ADDRESS_Line1)));
        addresses.setAddress2(cursor.getString(cursor.getColumnIndex(Fields.ADDRESS_Line2)));
        addresses.setAddress3(cursor.getString(cursor.getColumnIndex(Fields.ADDRESS_Line3)));
        addresses.setDeveloper_name(cursor.getString(cursor.getColumnIndex(Fields.DEVELOPER)));
        addresses.setTower(cursor.getString(cursor.getColumnIndex(Fields.TOWER)));
        addresses.setFloor(cursor.getString(cursor.getColumnIndex(Fields.FLOOR)));
        addresses.setUnitName(cursor.getString(cursor.getColumnIndex(Fields.UNIT_NO)));
        addresses.setCity_name(cursor.getString(cursor.getColumnIndex(Fields.CITY)));
        addresses.setZipcode(cursor.getString(cursor.getColumnIndex(Fields.ZIPCODE)));
       // addresses.setCountryName(cursor.getString(cursor.getColumnIndex(Fields.COUNTRY)));
        addresses.setStateName(cursor.getString(cursor.getColumnIndex(Fields.STATE)));
//        contactsModel.setC(cursor.getString(cursor.getColumnIndex(Fields.COMPANY_NAME)));
//        contactsModel.setPhonenumber(cursor.getString(cursor.getColumnIndex(Fields.PHONE_NUMBER)));


        return addresses;
    }
}
