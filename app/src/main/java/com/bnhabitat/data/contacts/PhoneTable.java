package com.bnhabitat.data.contacts;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.models.ContactsModel;

import java.util.ArrayList;

/**
 * Created by gourav on 8/8/2017.
 */

public class PhoneTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {

        }

        public static final String ID = "id";
        public static final String CONTACT_ID = "contact_id";
        public static final String NUMBER_ID = "number_id";
        public static final String PHONE_NUMBER = "phone_number";
        public static final String IS_SYNCED = "is_synced";
        public static final String CREATED_DATE = "created_date";
        public static final String UPDATED_DATE = "updated_date";
    }

    private final DatabaseManager databaseManager;

    private static final String NAME = "phones";

    private final static PhoneTable instance;
    SQLiteDatabase db;

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new PhoneTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static PhoneTable getInstance() {

        return instance;
    }

    private PhoneTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }

    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.CONTACT_ID,

            Fields.NUMBER_ID,
            Fields.PHONE_NUMBER,
            Fields.IS_SYNCED,
            Fields.CREATED_DATE,
            Fields.UPDATED_DATE,

    };

    @Override
    public void create(SQLiteDatabase db) {
        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.CONTACT_ID + " TEXT,"
                + Fields.NUMBER_ID + " TEXT,"
                + Fields.PHONE_NUMBER + " TEXT,"
                + Fields.IS_SYNCED + " TEXT,"

                + Fields.CREATED_DATE + " DATETIME, "
                + Fields.UPDATED_DATE + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);
    }

    public Long write(
            int contact_id,
            int number_id,
            String phoneNumber,
            String created,
            String modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.CONTACT_ID, contact_id);
        mContentValues.put(Fields.NUMBER_ID, number_id);
//
        mContentValues.put(Fields.PHONE_NUMBER, phoneNumber);
        mContentValues.put(Fields.CREATED_DATE, created);
        mContentValues.put(Fields.UPDATED_DATE, modified);


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }
 public void updateCategory(
                               int number_id,
                               String phoneNumber,

                               String modified){

        ContentValues mContentValues  = new ContentValues();


//
     mContentValues.put(Fields.PHONE_NUMBER, phoneNumber);

     mContentValues.put(Fields.UPDATED_DATE, modified);

        openDatabase();
        db.update(NAME, mContentValues, Fields.NUMBER_ID + " = ?", new String[] { String.valueOf(number_id) });
    }

    public boolean isPhoneAvailable(String serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.NUMBER_ID + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;

        cursor.close();
        return false;
    }
    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }
//    public ArrayList<ContactsModel.Phones> getPhones(String contact_id) {
//
//        Cursor cursor = databaseManager.getReadableDatabase().rawQuery("SELECT * FROM " + NAME + " WHERE " + Fields.CONTACT_ID + " = ?", new String[]{contact_id});
//        ArrayList<ContactsModel.Phones> contactsModels = new ArrayList<>();
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                contactsModels.add(generateCommonObjectFromCursor(cursor));
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//
//        return contactsModels;
//    }

    public ArrayList<ContactsModel.Phones> getPhones(String contact_id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CONTACT_ID + "=" + contact_id,
                null, null, null, null, null);

        ArrayList<ContactsModel.Phones> contactsModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                contactsModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return contactsModels;
    }

    public ArrayList<ContactsModel.Phones> getPhones() {

            Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                    null,
                    null, null, null, null, null);

            ArrayList<ContactsModel.Phones> contactsModels = new ArrayList<>();

            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {

                    contactsModels.add(generateCommonObjectFromCursor(cursor));
                    cursor.moveToNext();
                }
                cursor.close();
            }

        return contactsModels;
    }


    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.CONTACT_ID,
                Fields.NUMBER_ID,
                Fields.PHONE_NUMBER,
                Fields.CREATED_DATE,
                Fields.UPDATED_DATE,
        };
    }

    public void updateAllSync(

    ) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.IS_SYNCED, 1);
        openDatabase();
        db.update(NAME, mContentValues, null, null);
    }

    public ContactsModel.Phones generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        ContactsModel.Phones contactsModel = new ContactsModel().new Phones();

        contactsModel.setId(cursor.getString(cursor.getColumnIndex(Fields.NUMBER_ID)));

        contactsModel.setContactId(cursor.getString(cursor.getColumnIndex(Fields.CONTACT_ID)));
        contactsModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(Fields.PHONE_NUMBER)));
//        contactsModel.setC(cursor.getString(cursor.getColumnIndex(Fields.COMPANY_NAME)));
//        contactsModel.setPhonenumber(cursor.getString(cursor.getColumnIndex(Fields.PHONE_NUMBER)));


        return contactsModel;
    }
}
