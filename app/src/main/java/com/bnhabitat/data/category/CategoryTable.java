package com.bnhabitat.data.category;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.data.sizeunit.SizeUnitModel;
import com.bnhabitat.models.CommonDialogModel;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by developer15 on 3/3/2016.
 */
public class CategoryTable extends AbstractTable {

    private static final class Fields implements BaseColumns {

        private Fields() {
        }


        public static final String ID = "id";
        public static final String SERVER_ID = "server_id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";

    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "categories";

    private final static CategoryTable instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new CategoryTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static CategoryTable getInstance() {

        return instance;
    }

    private CategoryTable(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.SERVER_ID,
            Fields.NAME,
            Fields.DESCRIPTION,
            Fields.CREATED,
            Fields.MODIFIED,
    };


    public Long write(
            int serverId,
            String name,
            String description,
            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.NAME, name);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateCategory(  int serverId,
                              String name,
                              String description,
                            Date modified){

        ContentValues mContentValues  = new ContentValues();
        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.NAME, name);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.MODIFIED, modified.getTime());

        openDatabase();
        db.update(NAME, mContentValues, Fields.SERVER_ID + " = ?", new String[] { String.valueOf(serverId) });
    }


//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}

    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE if not exists " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.SERVER_ID + " TEXT,"
                + Fields.NAME + " TEXT,"
                + Fields.DESCRIPTION + " TEXT,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);

    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public boolean isCategoryAvailable(int serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "= " + serverId ,
                null, null, null, null);

        if (cursor.getCount()>0)
            return true;


        return false;
    }


//    public String readCategories(int sizeUnitId, int stateId) {
//
//        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
//                Fields.SIZE_UNIT_ID + "=" + sizeUnitId + " and " + Fields.STATE_ID + "=" + stateId,
//                null, null, null, null);
//
//        String size = "";
//
//        if (cursor != null && cursor.moveToFirst()) {
//            while (!cursor.isAfterLast()) {
//
//                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
//                //messageModeList.add(generateObjectFromCursor(cursor));
//                size = generateObjectFromCursor(cursor).getSizeUnitValue();
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//        return size;
//    }

    public String checkCategory(int categoryId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "=" + categoryId,
                null, null, null, null);

        String size = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
                //messageModeList.add(generateObjectFromCursor(cursor));
                size = generateObjectFromCursor(cursor).getSizeUnitValue();
                cursor.moveToNext();
            }
            cursor.close();
        }
        return size;
    }

    public ArrayList<CommonDialogModel> getCategory() {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                null,
                null, null, null, null);

        ArrayList<CommonDialogModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;
    }

    public String getCategoryName(int id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID+"=="+id,
                null, null, null, null);

        String categoryName = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                categoryName =  generateCommonObjectFromCursor(cursor).getTitle();

                cursor.moveToNext();
            }
            cursor.close();
        }

        return categoryName;
    }





    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.SERVER_ID,
                Fields.NAME,
                Fields.DESCRIPTION,
                Fields.CREATED,
                Fields.MODIFIED,
        };
    }


    public SizeUnitModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
           SizeUnitModel categoryModel = new SizeUnitModel();
//
//        categoryModel.setSizeUnitId(cursor.getInt(cursor.getColumnIndex(Fields.SIZE_UNIT_ID)));
//        categoryModel.setSizeUnitValue(cursor.getString(cursor.getColumnIndex(Fields.SIZE_UNIT_VALUE)));
//        categoryModel.setStandardUnitId(cursor.getInt(cursor.getColumnIndex(Fields.STANDARD_UNIT_ID)));
//        categoryModel.setStateId(cursor.getInt(cursor.getColumnIndex(Fields.STATE_ID)));


        return categoryModel;
    }

    public CommonDialogModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        CommonDialogModel stateModel = new CommonDialogModel();

        stateModel.setId(cursor.getString(cursor.getColumnIndex(Fields.SERVER_ID)));
        stateModel.setTitle(cursor.getString(cursor.getColumnIndex(Fields.NAME)));

        return stateModel;
    }

}
