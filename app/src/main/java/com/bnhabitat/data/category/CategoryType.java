package com.bnhabitat.data.category;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.bnhabitat.data.AbstractTable;
import com.bnhabitat.data.DatabaseManager;
import com.bnhabitat.data.sizeunit.SizeUnitModel;
import com.bnhabitat.models.CommonDialogModel;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by developer15 on 3/3/2016.
 */
public class CategoryType extends AbstractTable {


    private static final class Fields implements BaseColumns {

        private Fields() {
        }


        public static final String ID = "id";
        public static final String SERVER_ID = "server_id";
        public static final String CATEGORY_ID = "category_id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String AREA1 = "area1";
        public static final String AREA2 = "area2";
        public static final String AREA3 = "area3";
        public static final String AREA4 = "area4";
        public static final String DEFAULT_UNIT_ID = "default_unit_id";
        public static final String MINUNITRANGE = "min_unit_range";
        public static final String MAXUNITRANGE = "max_unit_range";
        public static final String CREATED = "created";
        public static final String MODIFIED = "modified";

    }


    private final DatabaseManager databaseManager;

    private static final String NAME = "category_type";

    private final static CategoryType instance;
    SQLiteDatabase db;

    private void closeDatabase() {
        //db.close();
    }

    private void openDatabase() {

        db = databaseManager.getWritableDatabase();

    }

    static {
        instance = new CategoryType(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);

    }

    public static CategoryType getInstance() {

        return instance;
    }

    private CategoryType(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
    }


    private static final String[] PROJECTION = new String[]{
            Fields.ID,
            Fields.SERVER_ID,
            Fields.NAME,
            Fields.CATEGORY_ID,
            Fields.DESCRIPTION,

            Fields.AREA1,
            Fields.AREA2,
            Fields.AREA3,
            Fields.AREA4,
            Fields.DEFAULT_UNIT_ID,
            Fields.MINUNITRANGE,
            Fields.MAXUNITRANGE,
            Fields.CREATED,
            Fields.MODIFIED,
    };


    public Long write(
            int serverId,
            int categoryId,
            String name,
            String description,
            String area1,
            String area2,
            String area3,
            String area4,
            String DefaultUnitID,
            String MinUnitRange,
            String MaxUnitRange,
            Date created,
            Date modified) {

        ContentValues mContentValues = new ContentValues();

        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.NAME, name);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.CATEGORY_ID, categoryId);

        mContentValues.put(Fields.AREA1, area1);
        mContentValues.put(Fields.AREA2, area2);
        mContentValues.put(Fields.AREA3, area3);
        mContentValues.put(Fields.AREA4, area4);
        mContentValues.put(Fields.DEFAULT_UNIT_ID, DefaultUnitID);
        mContentValues.put(Fields.MINUNITRANGE, MinUnitRange);
        mContentValues.put(Fields.MAXUNITRANGE, MaxUnitRange);
        mContentValues.put(Fields.CREATED, created.getTime());
        mContentValues.put(Fields.MODIFIED, modified.getTime());


        openDatabase();
        return db.insert(NAME, null, mContentValues);
    }


    public void updateCategoryType(int serverId,
                                   int categoryId,
                                   String name,
                                   String description,
                                   String area1,
                                   String area2,
                                   String area3,
                                   String area4,
                                   String DefaultUnitID,
                                   String MinUnitRange,
                                   String MaxUnitRange,
                                   Date modified) {

        ContentValues mContentValues = new ContentValues();
        mContentValues.put(Fields.SERVER_ID, serverId);
        mContentValues.put(Fields.NAME, name);
        mContentValues.put(Fields.CATEGORY_ID, categoryId);
        mContentValues.put(Fields.DESCRIPTION, description);
        mContentValues.put(Fields.MODIFIED, modified.getTime());
        mContentValues.put(Fields.AREA1, area1);
        mContentValues.put(Fields.AREA2, area2);
        mContentValues.put(Fields.AREA3, area3);
        mContentValues.put(Fields.AREA4, area4);
        mContentValues.put(Fields.DEFAULT_UNIT_ID, DefaultUnitID);
        mContentValues.put(Fields.MINUNITRANGE, MinUnitRange);
        mContentValues.put(Fields.MAXUNITRANGE, MaxUnitRange);

        openDatabase();
        db.update(NAME, mContentValues, Fields.SERVER_ID + " = ?", new String[]{String.valueOf(serverId)});
    }


//	public void updateMessageImageStatus(Long messageId,int status){
//
//
//		ContentValues mContentValues  = new ContentValues();
//
//		mContentValues.put(Fields.MESSAGE_STATUS, status);
//
//		db.update(NAME, mContentValues, Fields.MESSAGE_ID + " = ?", new String[] { String.valueOf(messageId) });
//
//	}


    @Override
    public void create(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.SERVER_ID + " TEXT,"
                + Fields.NAME + " TEXT,"
                + Fields.DESCRIPTION + " TEXT,"
                + Fields.CATEGORY_ID + " TEXT,"
                + Fields.AREA1 + " TEXT,"
                + Fields.AREA2 + " TEXT,"
                + Fields.AREA3 + " TEXT,"
                + Fields.AREA4 + " TEXT,"
                + Fields.DEFAULT_UNIT_ID + " TEXT,"
                + Fields.MINUNITRANGE + " TEXT,"
                + Fields.MAXUNITRANGE + " TEXT,"
                + Fields.CREATED + " DATETIME, "
                + Fields.MODIFIED + " DATETIME" +
                ");";


        DatabaseManager.execSQL(db, sql);

    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    public void deleteAllRecords() {


        openDatabase();

        String mSelectQuery = "Delete from " + NAME;

        db.execSQL(mSelectQuery);

        closeDatabase();

    }

    public boolean isCategoryAvailable(int serverId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "= " + serverId,
                null, null, null, null);

        if (cursor.getCount() > 0)
            return true;


        return false;
    }


    public String checkCategory(int categoryId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "=" + categoryId,
                null, null, null, null);

        String size = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                Log.e("Size unit value", generateObjectFromCursor(cursor).getSizeUnitValue());
                //messageModeList.add(generateObjectFromCursor(cursor));
                size = generateObjectFromCursor(cursor).getSizeUnitValue();
                cursor.moveToNext();
            }
            cursor.close();
        }
        return size;
    }


    public CategoryTypeModel getAreaCategory(int categoryId) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "=" + categoryId,
                null, null, null, null);

        CategoryTypeModel categoryTypeModel = new CategoryTypeModel();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                categoryTypeModel = generateCategoryObjectFromCursor(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return categoryTypeModel;
    }

    public String[] getAllColumns() {
        return new String[]{
                Fields.ID,
                Fields.SERVER_ID,
                Fields.NAME,
                Fields.DESCRIPTION,
                Fields.AREA1,
                Fields.AREA2,
                Fields.AREA3,
                Fields.AREA4,
                Fields.DEFAULT_UNIT_ID,
                Fields.MINUNITRANGE,
                Fields.MAXUNITRANGE,
                Fields.CREATED,
                Fields.MODIFIED,
        };
    }


    public ArrayList<CommonDialogModel> getPropertyType(String id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.CATEGORY_ID + " == '" + id + "'",
                null, null, null, null);

        ArrayList<CommonDialogModel> stateModels = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                stateModels.add(generateCommonObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }

        return stateModels;

    }

    public String getCategoryName(int id) {

        Cursor cursor = databaseManager.getReadableDatabase().query(NAME, getAllColumns(),
                Fields.SERVER_ID + "==" + id,
                null, null, null, null);

        String categoryName = "";

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                categoryName = generateCommonObjectFromCursor(cursor).getTitle();

                cursor.moveToNext();
            }
            cursor.close();
        }

        return categoryName;
    }


    public SizeUnitModel generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        SizeUnitModel categoryModel = new SizeUnitModel();

//        categoryModel.setSizeUnitId(cursor.getInt(cursor.getColumnIndex(Fields.SIZE_UNIT_ID)));
//        categoryModel.setSizeUnitValue(cursor.getString(cursor.getColumnIndex(Fields.SIZE_UNIT_VALUE)));
//        categoryModel.setStandardUnitId(cursor.getInt(cursor.getColumnIndex(Fields.STANDARD_UNIT_ID)));
//        categoryModel.setStateId(cursor.getInt(cursor.getColumnIndex(Fields.STATE_ID)));


        return categoryModel;
    }


    public CommonDialogModel generateCommonObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        CommonDialogModel stateModel = new CommonDialogModel();

        stateModel.setId(cursor.getString(cursor.getColumnIndex(Fields.SERVER_ID)));
        stateModel.setTitle(cursor.getString(cursor.getColumnIndex(Fields.NAME)));
        stateModel.setDefaultUnitID(cursor.getString(cursor.getColumnIndex(Fields.DEFAULT_UNIT_ID)));
        stateModel.setMinUnitRange(cursor.getString(cursor.getColumnIndex(Fields.MINUNITRANGE)));
        stateModel.setMaxUnitRange(cursor.getString(cursor.getColumnIndex(Fields.MAXUNITRANGE)));
        return stateModel;
    }

    public CategoryTypeModel generateCategoryObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        CategoryTypeModel categoryTypeModel = new CategoryTypeModel();

        categoryTypeModel.setArea1(cursor.getString(cursor.getColumnIndex(Fields.AREA1)));
        categoryTypeModel.setArea2(cursor.getString(cursor.getColumnIndex(Fields.AREA2)));
        categoryTypeModel.setArea3(cursor.getString(cursor.getColumnIndex(Fields.AREA3)));
        categoryTypeModel.setArea4(cursor.getString(cursor.getColumnIndex(Fields.AREA4)));
        categoryTypeModel.setDefaultUnitID(cursor.getString(cursor.getColumnIndex(Fields.DEFAULT_UNIT_ID)));
        categoryTypeModel.setMinUnitRange(cursor.getString(cursor.getColumnIndex(Fields.MINUNITRANGE)));
        categoryTypeModel.setMaxUnitRange(cursor.getString(cursor.getColumnIndex(Fields.MAXUNITRANGE)));
        return categoryTypeModel;
    }


}
