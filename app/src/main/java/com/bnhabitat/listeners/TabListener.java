package com.bnhabitat.listeners;

import android.app.ActionBar;

import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;


import com.bnhabitat.R;

/**
 * Created by gourav on 2/1/2018.
 */

public class TabListener implements android.support.v7.app.ActionBar.TabListener {
  Fragment fragment;
    public TabListener(Fragment fragment) {
        // TODO Auto-generated constructor stub
        this.fragment = fragment;
    }





    @Override
    public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        ft.replace(R.id.fragment_container, fragment);
    }

    @Override
    public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {
        ft.remove(fragment);
    }

    @Override
    public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft) {

    }
}
