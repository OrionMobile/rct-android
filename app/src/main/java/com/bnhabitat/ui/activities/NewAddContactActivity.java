package com.bnhabitat.ui.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.contacts.AddressTable;
import com.bnhabitat.data.contacts.ContatctTable;
import com.bnhabitat.data.contacts.EmailTable;
import com.bnhabitat.data.contacts.LocalContatctTable;
import com.bnhabitat.data.contacts.LocalPhoneTable;
import com.bnhabitat.data.contacts.PhoneTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.ContactSocialDetails;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class NewAddContactActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener {

    ArrayList<ContactsModel> contactArraylist = new ArrayList<>();
    private EditText first_name, last_name, mobile_no, email, prefix, middle_name;
    private TextView terms_and_conditions, no_thanks, quick_add_manage;
    private ImageView add_more, save_more, back_btn;
    ArrayList<String> getPhones = new ArrayList<>();
    ArrayList<String> getEmails = new ArrayList<>();
    private Button allow_me;
    ArrayList<String> contactList;
    Cursor cursor;
    int counter;
    String userCountryCode = "91";
    String inputJson = "";
    String input = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_add_contact);
        init();
        onClicks();
    }


    public void init() {

        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        prefix = (EditText) findViewById(R.id.prefix);
        middle_name = (EditText) findViewById(R.id.middle_name);
        mobile_no = (EditText) findViewById(R.id.mobile_no);
        email = (EditText) findViewById(R.id.email);
        terms_and_conditions = (TextView) findViewById(R.id.terms_and_conditions);
        quick_add_manage = (TextView) findViewById(R.id.quick_add_manage);
        no_thanks = (TextView) findViewById(R.id.no_thanks);
        add_more = (ImageView) findViewById(R.id.add_more);
        save_more = (ImageView) findViewById(R.id.save_more);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        allow_me = (Button) findViewById(R.id.allow_me);


        SpannableString ss = new SpannableString("You can quick add/manage contacts without any  hustle free Terms and condtions");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                final Dialog dialog = new Dialog(NewAddContactActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog);

                TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
                text.setText("Terms and Conditions are a set of rules and guidelines that a user must agree to in order to use your website or mobile app. It acts as a legal contract between you (the company) who has the website or mobile app and the user who access your website and mobile app.");

                Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                int linkColor = ContextCompat.getColor(NewAddContactActivity.this, R.color.blue);
                ds.setColor(linkColor);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 59, 78, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        quick_add_manage.setText(ss);
        quick_add_manage.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void onClicks() {

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });


        add_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                input = "addMore";
                if (first_name.getText().toString().length() > 0) {
                    if (last_name.getText().toString().length() > 0) {
                        if (!mobile_no.getText().toString().equalsIgnoreCase("")) {
                            if (mobile_no.getText().toString().length() == 10) {
                                onPostContacts();

                            } else {

                                Toast.makeText(NewAddContactActivity.this, "Please enter valid number", Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            Toast.makeText(NewAddContactActivity.this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        Toast.makeText(NewAddContactActivity.this, "Please enter last name", Toast.LENGTH_SHORT).show();
                    }
                } else {

                        Toast.makeText(NewAddContactActivity.this, "Please enter first name", Toast.LENGTH_SHORT).show();
                    }


            }
        });
        save_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (first_name.getText().toString().length() > 0) {
                    if (last_name.getText().toString().length() > 0) {
                    if (mobile_no.getText().toString().length() > 0) {
                        if (mobile_no.getText().toString().length() == 10) {

                            if (email.getText().toString().length() > 0) {

                                if (!Utils.isEmailAddressValid(email.getText().toString())) {
                                    Toast.makeText(NewAddContactActivity.this, "Please enter valid email", Toast.LENGTH_SHORT).show();

                                }
                            }

                            onPostContacts();

                        } else {

                            Toast.makeText(NewAddContactActivity.this, "Please enter valid number", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        Toast.makeText(NewAddContactActivity.this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(NewAddContactActivity.this, "Please enter last name", Toast.LENGTH_SHORT).show();
                }
                } else {

                    Toast.makeText(NewAddContactActivity.this, "Please enter first name", Toast.LENGTH_SHORT).show();
                }

            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        allow_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);
                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
                } else {
                    getAllContacts();
                    // Android version is lesser than 6.0 or the permission is already granted.

                }
            }
        });

        terms_and_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                getAllContacts();

            }
        } else {
            Toast.makeText(this, "Until you grant the permission, we cannot display the names", Toast.LENGTH_SHORT).show();
        }

    }

    public void getAllContacts() {
        contactList = new ArrayList<String>();
        String phoneNumber = null;
        String email = null;
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;
        String LAST_UPDATED_TIME = ContactsContract.CommonDataKinds.Email.CONTACT_LAST_UPDATED_TIMESTAMP;
        StringBuffer output;
        ContentResolver contentResolver = this.getContentResolver();
        String savedDateTime = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.CONTACT_MODIFIED, "15028907728");
        cursor = contentResolver.query(CONTENT_URI, null, LAST_UPDATED_TIME + ">='" + savedDateTime + "'", null, null);
        ArrayList<ContactsModel> contactsModels = new ArrayList<>();
        ArrayList<ContactsModel> contactsModels1 = new ArrayList<>();
        // Iterate every contact in the phone
        if (cursor.getCount() > 0) {
            counter = 0;
            while (cursor.moveToNext()) {
                output = new StringBuffer();

//                Utils.showProgressDialog(NewAddContactActivity.this);
                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                String lastUpdated = cursor.getString(cursor.getColumnIndex(LAST_UPDATED_TIME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                Log.e("last updated", lastUpdated + "--" + name);
                if (hasPhoneNumber > 0) {
//
                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);


                    // Read every email id associated with the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);
                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        output.append("\n Email:" + email);
                    }


                    // Add the contact to the ArrayList
                    Log.e("output", "output" + output.toString());


                    if (!LocalContatctTable.getInstance().isContactAvailable(contact_id)) {

                        ContactsModel contactsModel = new ContactsModel();
                        contactsModel.setFirstName(name);

                        ArrayList<ContactsModel.Phones> phones = new ArrayList<>();
                        while (phoneCursor.moveToNext()) {

                            ContactsModel.Phones phone = contactsModel.new Phones();
                            phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));


                            String str = phoneNumber.replaceAll("\\D+", "");


                            if (phoneNumber.startsWith("+")) {
                                str = phoneNumber.replaceAll("\\D+", "");
                            } else if (str.startsWith("00")) {
                                str = str.replaceFirst("^0+(?!$)", "");
                            } else if (str.startsWith("0")) {
                                str = str.replaceFirst("^0+(?!$)", userCountryCode);
                            }

                            else if (str.startsWith("18")) {
                                str =  str ;
                            }else {
                                str = userCountryCode + str;
                            }

                            phone.setPhoneNumber(str);
                            phones.add(phone);

                            Log.e("name", name + " " + str);
                            // phones.add(phone);
                            if (!LocalContatctTable.getInstance().isContactAvailable(contact_id))
                                LocalContatctTable.getInstance().write(Integer.parseInt(contact_id), name, "", "Bn-habitat", str, 0, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, ""));

                            LocalPhoneTable.getInstance().write(Integer.parseInt(contact_id), 0, str, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, ""));
                        }
                        contactsModel.setPhones(phones);
                        contactsModels1.add(contactsModel);
                    }


                    emailCursor.close();
                    phoneCursor.close();
                    Log.e("output", "output" + output.toString());

                    PreferenceConnector.getInstance(this).savePreferences(Constants.CONTACT_MODIFIED, String.valueOf(System.currentTimeMillis()));


                }

            }
            pushContacts(contactsModels1);
            new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_POST_CONTACTS,
                    inputJson,
                    "Loading...",
                    this,
                    Urls.URL_POST_CONTACTS,
                    Constants.POST).execute();

        } else {
            Utils.showErrorMessage("You already sync all contacts", this);
        }

        //   if (contactsModels.size() > 0)
//        onSyncRequest();

    }

    private void pushContacts(ArrayList<ContactsModel> contactsModels) {
        try {



            JSONArray jsonArrayTop = new JSONArray();

            for (int i = 0; i < contactsModels.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("FirstName", contactsModels.get(i).getFirstName());
                jsonObject.put("LastName", contactsModels.get(i).getLastName());
                jsonObject.put("CompanyId", "1");
                jsonObject.put("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));

                JSONArray jsonArray = new JSONArray();
                ArrayList<ContactsModel.Phones> phones = contactsModels.get(i).getPhones();

                for (int j = 0; j < phones.size(); j++) {

                    JSONObject jsonObject1 = new JSONObject();

                    jsonObject1.put("PhoneNumber", phones.get(j).getPhoneNumber());

                    jsonObject1.put("Mode", "");

                    jsonArray.put(jsonObject1);

                }
                JSONArray grouparray = new JSONArray();
                JSONObject jsonObject4 = new JSONObject();
                jsonObject4.put("Id", "");
                grouparray.put(jsonObject4);
                jsonObject.put("Phones", jsonArray);
//                jsonObject.put("Groups", grouparray);
                jsonArrayTop.put(jsonObject);



                    ContatctTable.getInstance().write(0, contactsModels.get(i).getFirstName(),
                            contactsModels.get(i).getLastName(), "",phones.get(0).getPhoneNumber() , "", 1,
                            "", "", "",
                            "", "01-01-2017", "01-01-2017");
                    for (int indexJ = 0; indexJ < contactsModels.get(i).getPhones().size(); indexJ++) {
                        PhoneTable.getInstance().write(0, 0,
                                contactsModels.get(i).getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");

                    }
//                    for (int indexJ1 = 0; indexJ1 < contactsModels.get(i).getEmails().size(); indexJ1++) {
//                        EmailTable.getInstance().write(Integer.parseInt(contactsModels.get(i).getEmails().get(indexJ1).getContactId()),
//                                Integer.parseInt(contactsModels.get(i).getEmails().get(indexJ1).getId()), contactsModels.get(i).getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");
//
//                    }
//                    for (int indexJ2 = 0; indexJ2 < contactsModel.getAddresses().size(); indexJ2++) {
//                        AddressTable.getInstance().write(Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getContactId()), Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(), contactsModel.getAddresses().get(indexJ2).getAddress2(), contactsModel.getAddresses().get(indexJ2).getAddress3(), contactsModel.getAddresses().get(indexJ2).getDeveloper(), contactsModel.getAddresses().get(indexJ2).getTower(), contactsModel.getAddresses().get(indexJ2).getFloor(), contactsModel.getAddresses().get(indexJ2).getUnitName(), contactsModel.getAddresses().get(indexJ2).getCity_name(), contactsModel.getAddresses().get(indexJ2).getZipcode(), contactsModel.getAddresses().get(indexJ2).getCountryId(), contactsModel.getAddresses().get(indexJ2).getStateName(), "01-01-2017", "01-01-2017");
//
//                    }


            }


            inputJson = jsonArrayTop.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onPostContacts() {

        new CommonAsync(NewAddContactActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
                getLoginInputJson(),
                "Loading...",
                this,
                Urls.URL_CREATE_CONTACTS,
                Constants.POST).execute();
    }

    private String getLoginInputJson() {

        String inputJson = "";
//


//
        try {


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("FirstName", first_name.getText().toString());
            jsonObject.put("LastName", last_name.getText().toString());

            jsonObject.put("Prefix", prefix.getText().toString());
            jsonObject.put("MiddleName", middle_name.getText().toString());


            jsonObject.put("CompanyId", "1");


            jsonObject.put("Gender", "male");
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            if (!mobile_no.getText().toString().equalsIgnoreCase("")) {

                getPhones.add(mobile_no.getText().toString());
            }

            if (!email.getText().toString().equalsIgnoreCase("")) {

                getEmails.add(email.getText().toString());
            }

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < getPhones.size(); i++) {

                JSONObject jsonObject2 = new JSONObject();

                jsonObject2.put("PhoneNumber", getPhones.get(i));

//                    jsonObject1.put("PhoneType", 1);
                jsonObject2.put("Mode", "");

                jsonArray.put(jsonObject2);

            }
            JSONArray emailArray = new JSONArray();
            for (int i = 0; i < getEmails.size(); i++) {

                JSONObject jsonObject3 = new JSONObject();

                jsonObject3.put("Address", getEmails.get(i));

//                    jsonObject1.put("PhoneType", 1);
                jsonObject3.put("Label", "Email Label");

                emailArray.put(jsonObject3);

            }
            jsonObject.put("Phones", jsonArray);
            jsonObject.put("EmailAddresses", emailArray);
//            jsonObject.put("Groups", grouparray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_CREATE_CONTACTS)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {
                        ContactsModel contactModel = new ContactsModel();
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String msg = jsonObject1.getString("Message");
                        if (jsonObject1.getBoolean("Success")) {

                            msg = jsonObject1.getString("Message");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String id = jsonObject2.getString("Id");
                            String FirstName = jsonObject2.getString("FirstName");
                            String LastName = jsonObject2.getString("LastName");
                            String CompanyId = jsonObject2.getString("CompanyId");
                            String CompanyName = jsonObject2.getString("CompanyName");
                            String DateOfBirthStr = jsonObject2.getString("DateOfBirthStr");
                            String Prefix = jsonObject2.getString("Prefix");
                            String Sufix = jsonObject2.getString("Sufix");
                            String MaritalStatus = jsonObject2.getString("MaritalStatus");
                            String ExistingImageUrl = jsonObject2.getString("ExistingImageUrl");
                            String Religion = jsonObject2.getString("Religion");
                            String Gender = jsonObject2.getString("Gender");

                            String MiddleName = jsonObject2.getString("MiddleName");
                            contactModel.setId(id);
                            contactModel.setFirstName(FirstName);
                            contactModel.setLastName(LastName);
                            contactModel.setCompanyName(CompanyName);
                            contactModel.setDateOfBirthStr(DateOfBirthStr);
                            contactModel.setPrefix(Prefix);
                            contactModel.setMiddleName(MiddleName);
                            contactModel.setSufix(MiddleName);

                            if(Sufix.equalsIgnoreCase("null"))
                                contactModel.setSufix("");
                            else
                                contactModel.setSufix(Sufix);
                            contactModel.setCompanyId(CompanyId);
                            contactModel.setMaritalStatus(MaritalStatus);
                            contactModel.setExistingImageUrl(ExistingImageUrl);
                          //  contactModel.setReligion(Religion);
                            if(Religion.equalsIgnoreCase("null"))
                                contactModel.setReligion("");
                            else
                                contactModel.setReligion(Religion);
                            contactModel.setGender(Gender);
                            JSONArray sizeJsonArray = jsonObject2.getJSONArray("Addresses");
                            ArrayList<AddressModel> addresses = new ArrayList();
                            ContactsModel contactsModel = new ContactsModel();
                            for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                                AddressModel addresses1 = new AddressModel();
                                JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                                addresses1.setId(dataObj.getString("Id"));

                                addresses1.setCountryName(dataObj.getString("CountryName"));
                                addresses1.setStateName(dataObj.getString("StateName"));
//                                addresses1.setFirstName(dataObj.getString("FirstName"));
//                                addresses1.setLastName(dataObj.getString("LastName"));
//                                addresses1.setEmail(dataObj.getString("Email"));
//                                addresses1.setCompany(dataObj.getString("Company"));
                                addresses1.setCity_name(dataObj.getString("CityName"));
                                addresses1.setAddress1(dataObj.getString("Address1"));
                                addresses1.setAddress2(dataObj.getString("Address2"));
//                                addresses1.setPhoneNumber(dataObj.getString("PhoneNumber"));
//                                addresses1.setFaxNumber(dataObj.getString("FaxNumber"));
                                addresses.add(addresses1);
                            }
                            JSONArray phonesJsonArray = jsonObject2.getJSONArray("Phones");
                            ArrayList<ContactsModel.Phones> phones = new ArrayList();
                            for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                                ContactsModel.Phones phones1 = contactsModel.new Phones();
                                JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                                phones1.setId(dataObj.getString("Id"));
                                phones1.setContactId(dataObj.getString("ContactId"));
                                phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                phones1.setPhoneCode(dataObj.getString("PhoneCode"));
                                phones1.setPhoneType(dataObj.getString("Mode"));
//
                                phones.add(phones1);

                            }
                            JSONArray emailJsonArray = jsonObject2.getJSONArray("EmailAddresses");
                            ArrayList<ContactsModel.Email> emails = new ArrayList();
                            for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                                ContactsModel.Email email = contactsModel.new Email();
                                JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                                email.setId(dataObj.getString("Id"));
                                email.setContactId(dataObj.getString("ContactId"));
                                email.setAddress(dataObj.getString("Address"));
                                email.setLabel(dataObj.getString("Label"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                emails.add(email);
                            }

                           /* JSONArray socialJsonArray = jsonObject2.getJSONArray("ContactAttributes");
                            ArrayList<ContactsModel.ContactAttributes> socialArray = new ArrayList();
                            for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                                ContactsModel.ContactAttributes contactAttributes = contactsModel.new ContactAttributes();
                                JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                                contactAttributes.setKey(dataObj.getString("Key"));
                                contactAttributes.setValue(dataObj.getString("Value"));

//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                socialArray.add(contactAttributes);
                            }*/

                            JSONArray socialJsonArray = jsonObject2.getJSONArray("ContactSocialDetails");
                            ArrayList<ContactSocialDetails> socialArray = new ArrayList();
                            for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                                ContactSocialDetails contactAttributes = new ContactSocialDetails();
                                JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                                contactAttributes.setType(dataObj.getString("Type"));
                                contactAttributes.setDetail(dataObj.getString("Detail"));

                                socialArray.add(contactAttributes);
                            }

                            JSONArray contactRelation = jsonObject2.getJSONArray("ContactRelations");
                            ArrayList<ContactsModel.ContactRelations> contactRelationses = new ArrayList();
                            for (int indexJ = 0; indexJ < contactRelation.length(); indexJ++) {

                                ContactsModel.ContactRelations contactRelations = contactsModel.new ContactRelations();
                                JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                                contactRelations.setRelation(dataObj.getString("Relation"));
                                contactRelations.setRelationFirstName(dataObj.getString("RelationFirstName"));
                                contactRelations.setRelationLastName(dataObj.getString("RelationLastName"));
                                contactRelations.setId(Utils.getFilteredValueInt(dataObj.getInt("ContactId")));

//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                contactRelationses.add(contactRelations);
//                                PhoneTable.getInstance().write(Integer.parseInt(contactsModel.getPhones().get(indexJ).getContactId()), Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()), contactsModel.getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");

                            }


                            contactModel.setContactRelationses(contactRelationses);
                            contactModel.setPhones(phones);
                            contactModel.setAddresses(addresses);
                            contactModel.setEmails(emails);
                            contactModel.setContactSocialDetails(socialArray);
                            contactArraylist.add(contactModel);
                            String phone = "";
                            String email_address = "";
                            ContatctTable.getInstance().write(Integer.parseInt(contactArraylist.get(0).getId()), contactArraylist.get(0).getFirstName(), contactArraylist.get(0).getLastName(), "", phone, email_address, 1, "","", "", "", "01-01-2017", "01-01-2017");
                            for (int indexJ = 0; indexJ < contactArraylist.get(0).getPhones().size(); indexJ++) {
                                PhoneTable.getInstance().write(Integer.parseInt(contactArraylist.get(0).getPhones().get(indexJ).getContactId()), Integer.parseInt(contactArraylist.get(0).getPhones().get(indexJ).getId()), contactArraylist.get(0).getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");

                            }
                            for (int indexJ1 = 0; indexJ1 < contactArraylist.get(0).getEmails().size(); indexJ1++) {
                                EmailTable.getInstance().write(Integer.parseInt(contactArraylist.get(0).getEmails().get(indexJ1).getContactId()), Integer.parseInt(contactArraylist.get(0).getEmails().get(indexJ1).getId()), contactArraylist.get(0).getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");

                            }





                            if (input.equalsIgnoreCase("addMore")) {
                                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                                first_name.setText("");
                                last_name.setText("");
                                email.setText("");
                                mobile_no.setText("");
                                prefix.setText("");
                                middle_name.setText("");
                                input = "";
                                first_name.requestFocus();
                            } else {
                                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(NewAddContactActivity.this, CompleteContactInfoActivity.class);
                                intent.putExtra("ModelName", contactArraylist);
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            if (msg.contains("Contact already exist.")) {
                                Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.contact_name_numb_exist), getString(R.string.ok), this);
                            } else {
                                Utils.showWarningErrorMessage(getString(R.string.warning), msg, getString(R.string.ok), this);

                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_POST_CONTACTS)) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);
                    JSONObject resultInsert = jsonObject.getJSONObject("Result");
                    JSONArray projectsArray = resultInsert.getJSONArray("Result");

                    if (projectsArray.length() == 0) {
//                        Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                    }
                    Intent intent = new Intent(this, MainDashboardActivity.class);
                    intent.putExtra(Constants.FROM, "Sync_finish");
                    startActivity(intent);


                    LocalContatctTable.getInstance().updateAllSync();
                    Toast.makeText(this, "Syncing finished ", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {

                    e.printStackTrace();

                }


            }

        }

    }
}
