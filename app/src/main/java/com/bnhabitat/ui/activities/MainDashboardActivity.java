package com.bnhabitat.ui.activities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bnhabitat.R;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.ui.fragments.ContactFragment;
import com.bnhabitat.ui.fragments.DashboardFragment;
import com.bnhabitat.ui.fragments.TeamFragment;
import com.bnhabitat.utils.Constants;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;

public class MainDashboardActivity extends AppCompatActivity implements View.OnClickListener{
    LinearLayout team_lay,contact_lay,dash_lay;
    String from="";
    ArrayList<ContactsModel>searchProjectsModels=new ArrayList<>();
    private IntentFilter mIntentFilter;
    public static final String mBroadcastStringAction = "com.truiton.broadcast.string";
    ImageView dashboard_text,contact_text,team_text,create_contact,group,back;
    private View actionB,action_A, action_E;
    public static ArrayList<ContactsModel> contactsModelsGlobal = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_dashboard);
        final FloatingActionsMenu menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        action_E = findViewById(R.id.action_e);
        menuMultipleActions.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

                menuMultipleActions.setBackgroundColor(getResources().getColor(R.color.white));
//                transparentLayout.setVisibility(View.VISIBLE);
            }

            @SuppressLint("ResourceType")
            @Override
            public void onMenuCollapsed() {

                menuMultipleActions.setBackgroundColor(getResources().getColor(R.color.tran));

            }
        });
        /*actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
                Intent intent = new Intent(MainDashboardActivity.this, AddContatctActivity.class);
                startActivity(intent);

            }
        });*/
        action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
                Intent intent = new Intent(MainDashboardActivity.this, SelectionPropertyTypeActivity.class);
                startActivity(intent);
//                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                String shareBody = "https://bnhabitat.com/app/"+normalTextEnc;
//                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "");
//                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
//                startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
        action_E.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
                Intent intent = new Intent(MainDashboardActivity.this, NewAddContactActivity.class);
                startActivity(intent);
            }
        });

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastStringAction);
        team_lay=(LinearLayout)findViewById(R.id.team_lay);
        contact_lay=(LinearLayout)findViewById(R.id.contact_lay);
        dash_lay=(LinearLayout)findViewById(R.id.dash_lay);
        dashboard_text=(ImageView) findViewById(R.id.dashboard_text);
        contact_text=(ImageView) findViewById(R.id.contact_text);
        create_contact=(ImageView) findViewById(R.id.create_contact);
        group=(ImageView) findViewById(R.id.group);
        back=(ImageView) findViewById(R.id.back);
        team_text=(ImageView) findViewById(R.id.team_text);
        dash_lay.setOnClickListener(this);
        team_lay.setOnClickListener(this);
        contact_lay.setOnClickListener(this);
        create_contact.setOnClickListener(this);
        group.setOnClickListener(this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        loadFragment(new DashboardFragment());
        try {
            from = getIntent().getStringExtra(Constants.FROM);
            searchProjectsModels = (ArrayList<ContactsModel>)getIntent().getSerializableExtra("Array_contactlist");

            if (from != null ){

                if (from.equalsIgnoreCase("advance_search")) {

                    team_text.setImageResource(R.drawable.team_inactive);
                    contact_text.setImageResource(R.drawable.contact_active);
                    dashboard_text.setImageResource(R.drawable.dashboard_inactive);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Array_contactlist", searchProjectsModels);
                    bundle.putString("AssignContact", "");
                    ContactFragment myFragment = new ContactFragment();
                    myFragment.setArguments(bundle);

                    loadFragment(myFragment);

//                    FragmentManager fm = getFragmentManager();
//// create a FragmentTransaction to begin the transaction and replace the Fragment
//                    FragmentTransaction fragmentTransaction = fm.beginTransaction();
//// replace the FrameLayout with new Fragment
//                    fragmentTransaction.replace(R.id.fragment_view, myFragment);
//                    fragmentTransaction.commit();
                }
            else if ( from.equalsIgnoreCase("AssignContactActivity")) {
                    team_text.setImageResource(R.drawable.team_inactive);
                    contact_text.setImageResource(R.drawable.contact_active);
                    dashboard_text.setImageResource(R.drawable.dashboard_inactive);
                    Bundle bundle = new Bundle();
                    bundle.putString("AssignContact", "Assign");
                    ContactFragment myFragment = new ContactFragment();
                    myFragment.setArguments(bundle);
                    FragmentManager fm = getFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
                    FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
                    fragmentTransaction.replace(R.id.fragment_view, myFragment);
                    fragmentTransaction.commit();
                }

                else if( from.equalsIgnoreCase("Sync_finish")){

                    create_contact.setVisibility(View.VISIBLE);
                    group.setVisibility(View.GONE);

                    team_text.setImageResource(R.drawable.team_inactive);
                    contact_text.setImageResource(R.drawable.contact_active);
                    dashboard_text.setImageResource(R.drawable.dashboard_inactive);
                    loadFragment(new ContactFragment());
                    finish();
                }
                else if( from.equalsIgnoreCase("EditQuery")){

                    create_contact.setVisibility(View.VISIBLE);
                    group.setVisibility(View.GONE);

                    team_text.setImageResource(R.drawable.team_inactive);
                    contact_text.setImageResource(R.drawable.contact_active);
                    dashboard_text.setImageResource(R.drawable.dashboard_inactive);
                    loadFragment(new ContactFragment());
                    finish();

                }
            }




        }catch (Exception e){
            e.printStackTrace();
        }




//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.team_lay:
                create_contact.setVisibility(View.GONE);
                group.setVisibility(View.VISIBLE);
                team_text.setImageResource(R.drawable.team_active);
                contact_text.setImageResource(R.drawable.contacts_inactive);
                dashboard_text.setImageResource(R.drawable.dashboard_inactive);

                loadFragment(new TeamFragment());
                break;
            case R.id.contact_lay:
                create_contact.setVisibility(View.VISIBLE);
                group.setVisibility(View.GONE);

                team_text.setImageResource(R.drawable.team_inactive);
                contact_text.setImageResource(R.drawable.contact_active);
                dashboard_text.setImageResource(R.drawable.dashboard_inactive);
                loadFragment(new ContactFragment());
                break;
            case R.id.dash_lay:
                group.setVisibility(View.GONE);

                create_contact.setVisibility(View.VISIBLE);
                team_text.setImageResource(R.drawable.team_inactive);
                contact_text.setImageResource(R.drawable.contacts_inactive);
                dashboard_text.setImageResource(R.drawable.dashboard_active);
                loadFragment(new DashboardFragment());
                break;
            case R.id.create_contact:
                Intent intent=new Intent(MainDashboardActivity.this,NewAddContactActivity.class);
                startActivity(intent);

                break;
            case R.id.group:
                Intent intent1=new Intent(MainDashboardActivity.this,GroupActivity.class);
                startActivity(intent1);

                break;
        }
    }

    private void loadFragment(Fragment fragment) {

        FragmentManager fm = getFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_view, fragment);
     //   fragmentTransaction.detach(fragment);
    //    fragmentTransaction.attach(fragment);
        fragmentTransaction.commit();

    }



}
