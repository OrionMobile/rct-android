package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.PropertyOwnerShipProofs;
import com.bnhabitat.models.SpecificationListModel;
import com.bnhabitat.ui.adapters.InventoryAdapter;
import com.bnhabitat.ui.fragments.DetailFragment;
import com.bnhabitat.ui.fragments.DraftFragment;
import com.bnhabitat.ui.fragments.FinancialandBankLoanFragment;
import com.bnhabitat.ui.fragments.InventoryFragment;
import com.bnhabitat.ui.fragments.LocationFragment;
import com.bnhabitat.ui.fragments.SharedFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyPropertyActivity extends AppCompatActivity implements CommonSyncwithoutstatus.OnAsyncResultListener {
    TabLayout tabLayout;
    TextView property_select_name, text_count;
    TabLayout.Tab Tab1, Tab2, Tab3, Tab4, Tab5, Tab6, Tab7, Tab8;
    JSONObject json;
    String PropertyId, PropertyName;
    int number_count_data = 3;
    ImageView back_btn;
    ArrayList<InventoryModel> inventoryModelArrayList = new ArrayList<>();

    String from_savedrafts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_property);

        from_savedrafts = getIntent().getStringExtra("from_savedrafts");


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        Tab1 = tabLayout.newTab().setText("Inventory");
        Tab2 = tabLayout.newTab().setText("Draft");
        Tab3 = tabLayout.newTab().setText("Shared");

        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
        tabLayout.addTab(Tab3);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getInventoryList();


    }

    @Override
    protected void onResume() {
        super.onResume();
        getInventoryList();
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_myproperty, fragment);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                InventoryFragment inventoryFragment = new InventoryFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("property_data", inventoryModelArrayList);
                bundle.putSerializable("from_savedrafts", from_savedrafts);
                inventoryFragment.setArguments(bundle);
//              Bundle inventorybundle = new Bundle();
//              inventoryFragment.setArguments(inventorybundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myproperty, inventoryFragment).commit();

                break;
            case 1:
                DraftFragment draftFragment = new DraftFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("property_data", inventoryModelArrayList);
                bundle1.putSerializable("from_savedrafts", from_savedrafts);

                draftFragment.setArguments(bundle1);
                //Inflate the fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myproperty, draftFragment).commit();

                break;
            case 2:
                try {
                    SharedFragment sharedFragment = new SharedFragment();
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myproperty, sharedFragment).commit();


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public void onBackPressed() {
        //Intent intent = new Intent(this, InventoryDashboardActivity.class);
        //startActivity(intent);
        finish();
    }

    private void getInventoryList() {

        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_INVENTORY_LIST,
                "",
                "Loading..",
                this,
                Urls.URL_INVENTORY_LIST,
                Constants.GET).execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_INVENTORY_LIST)) {

                try {
                    //   JSONObject jsonObject = new JSONObject(result);

                    //    if (jsonObject.getInt("StatusCode") == 200) {

                    JSONArray propertySize = new JSONArray(result);

                    if (propertySize.length() == 0) {
//                  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {

                        inventoryModelArrayList.clear();
                        for (int index = 0; index < propertySize.length(); index++) {
                            JSONObject jsonObject1 = propertySize.getJSONObject(index);

                            InventoryModel inventoryModels = new InventoryModel();

                            inventoryModels.setId(jsonObject1.optString("Id"));
                            inventoryModels.setPropertyTypeId(jsonObject1.optString("PropertyTypeId"));
                            inventoryModels.setCompanyId(jsonObject1.optString("CompanyId"));
                            inventoryModels.setCam(jsonObject1.optString("Cam"));
                            inventoryModels.setGroundType(jsonObject1.optString("GroundType"));

                            try {

                                inventoryModels.setDraft(jsonObject1.getBoolean("IsDraft"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            inventoryModels.setCreatedById(jsonObject1.optString("CreatedById"));

                            inventoryModels.setTotalRooms(jsonObject1.getInt("TotalRooms"));
                            inventoryModels.setTotalBathrooms(jsonObject1.getInt("TotalBathrooms"));
                            inventoryModels.setTotalParkings(jsonObject1.getInt("TotalParkings"));
                            inventoryModels.setTotalKitchen(jsonObject1.getInt("TotalKitchen"));

                            ArrayList<InventoryModel.PropertyLocation> propertyLocations = new ArrayList<>();
                            if (jsonObject1.has("PropertyType")) {
                                JSONObject objectPropertyType = jsonObject1.optJSONObject("PropertyType");

                                if (objectPropertyType != null) {
                                    inventoryModels.setPropertyobjectId(objectPropertyType.optString("Id"));
                                    inventoryModels.setNameobject(objectPropertyType.optString("Name"));
                                    inventoryModels.setTypeobject(objectPropertyType.optString("Type"));
                                }


                                JSONArray jsonArray = jsonObject1.getJSONArray("PropertyLocations");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    InventoryModel.PropertyLocation propertyLocation = inventoryModels.new PropertyLocation();
                                    JSONObject jsonObjectPropertyLocations = jsonArray.getJSONObject(i);
                                    propertyLocation.setId(jsonObjectPropertyLocations.optString("Id"));
                                    propertyLocation.setPropertyId(jsonObjectPropertyLocations.optString("PropertyId"));
                                    propertyLocation.setDeveloper(jsonObjectPropertyLocations.optString("Developer"));
                                    propertyLocation.setProject(jsonObjectPropertyLocations.optString("Project"));
                                    propertyLocation.setZipCode(jsonObjectPropertyLocations.optString("ZipCode"));
                                    propertyLocation.setState(jsonObjectPropertyLocations.optString("State"));
                                    propertyLocation.setTownName(jsonObjectPropertyLocations.optString("TownName"));
                                    propertyLocation.setSector(jsonObjectPropertyLocations.optString("Sector"));
                                    propertyLocation.setDistrict(jsonObjectPropertyLocations.optString("District"));
                                    propertyLocation.setSubArea(jsonObjectPropertyLocations.optString("SubArea"));
                                    propertyLocation.setSubDivision(jsonObjectPropertyLocations.optString("SubDivision"));
                                    propertyLocation.setUnitName(jsonObjectPropertyLocations.optString("UnitNo"));
                                    propertyLocation.setTowerName(jsonObjectPropertyLocations.optString("TowerName"));
                                    propertyLocation.setFloorNo(jsonObjectPropertyLocations.optString("FloorNo"));
                                    propertyLocation.setGooglePlusCode(jsonObjectPropertyLocations.optString("GooglePlusCode"));
                                    if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue")) {
                                        JSONObject areaatribute = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue");
                                        propertyLocation.setAreaAttributeValue(areaatribute.optString("Value"));
                                    }
                                    if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue1")) {
                                        JSONObject areaatribute1 = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue1");
                                        propertyLocation.setAreaAttributeValue1(areaatribute1.optString("Value"));
                                    }
                                    if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue2")) {
                                        JSONObject areaatribute2 = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue2");
                                        propertyLocation.setAreaAttributeValue2(areaatribute2.optString("Value"));
                                    }
                                    if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue3")) {
                                        JSONObject areaatribute3 = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue3");
                                        propertyLocation.setAreaAttributeValue3(areaatribute3.optString("Value"));
                                    }
                                    if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue4")) {
                                        JSONObject areaatribute4 = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue4");
                                        propertyLocation.setAreaAttributeValue4(areaatribute4.optString("Value"));
                                    }
                                    propertyLocations.add(propertyLocation);


                                }


                            }


                            JSONArray jsonArrayPropertyAreas = jsonObject1.getJSONArray("PropertyAreas");
                            ArrayList<InventoryModel.PropertyArea> propertyAreas = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyAreas.length(); k++) {
                                JSONObject jsonObjectPropertyLocations = jsonArrayPropertyAreas.getJSONObject(k);
                                InventoryModel.PropertyArea propertyArea = inventoryModels.new PropertyArea();
                                propertyArea.setId(jsonObjectPropertyLocations.optString("Id"));
                                propertyArea.setPropertyId(jsonObjectPropertyLocations.optString("PropertyId"));
                                propertyArea.setPlotShape(jsonObjectPropertyLocations.optString("PlotShape"));
                                propertyArea.setPlotArea(jsonObjectPropertyLocations.optString("PlotArea"));
                                propertyArea.setPlotAreaUnitId(jsonObjectPropertyLocations.optInt("PlotAreaUnitId"));
                                propertyArea.setFrontSize(jsonObjectPropertyLocations.optString("FrontSize"));
                                propertyArea.setFrontSizeUnitId(jsonObjectPropertyLocations.optInt("FrontSizeUnitId"));
                                propertyArea.setDepthSize(jsonObjectPropertyLocations.optString("DepthSize"));
                                propertyArea.setDepthSizeUnitId(jsonObjectPropertyLocations.optString("DepthSizeUnitId"));
                                propertyArea.setStreetOrRoadType(jsonObjectPropertyLocations.optString("StreetOrRoadType"));
                                propertyArea.setRoadWidth(jsonObjectPropertyLocations.optString("RoadWidth"));
                                propertyArea.setRoadWidthUnitId(jsonObjectPropertyLocations.optInt("RoadWidthUnitId"));
                                propertyArea.setEnteranceDoorFacing(jsonObjectPropertyLocations.optString("EnteranceDoorFacing"));
                                propertyArea.setParkingInFront(jsonObjectPropertyLocations.optString("ParkingInFront"));
                                propertyArea.setHaveWalkingPath(jsonObjectPropertyLocations.optString("HaveWalkingPath"));
                                propertyArea.setPlotOrCoverArea(jsonObjectPropertyLocations.optString("PlotOrCoverArea"));
                                propertyArea.setPlotOrCoverAreaUnitId(jsonObjectPropertyLocations.optString("PlotOrCoverAreaUnitId"));
                                propertyArea.setSuperArea(jsonObjectPropertyLocations.optString("SuperArea"));
                                propertyArea.setSuperAreaUnitId(jsonObjectPropertyLocations.optString("SuperAreaUnitId"));
                                propertyArea.setBuiltUpArea(jsonObjectPropertyLocations.optString("BuiltUpArea"));
                                propertyArea.setBuiltUpAreaUnitId(jsonObjectPropertyLocations.optString("BuiltUpAreaUnitId"));
                                propertyArea.setCarpetArea(jsonObjectPropertyLocations.optString("CarpetArea"));
                                propertyArea.setCarpetAreaUnitId(jsonObjectPropertyLocations.optString("CarpetAreaUnitId"));
                                if (!jsonObjectPropertyLocations.isNull("PropertySizeUnit4")) {
                                    JSONObject jsonObjecPropertySizeUnit4 = jsonObjectPropertyLocations.getJSONObject("PropertySizeUnit4");
                                    propertyArea.setPropertySizeUnit4(jsonObjecPropertySizeUnit4.optString("SizeUnit"));
                                }
                                propertyAreas.add(propertyArea);

                            }
                            JSONArray jsonArrayPropertyPlcs = jsonObject1.getJSONArray("PropertyPlcs");
                            ArrayList<InventoryModel.PropertyPlc> propertyPlcs = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyPlcs.length(); k++) {
                                InventoryModel.PropertyPlc propertyPlc = inventoryModels.new PropertyPlc();
                                JSONObject jsonObjectPropertyPlcs = jsonArrayPropertyPlcs.getJSONObject(k);
                                propertyPlc.setId(jsonObjectPropertyPlcs.optString("Id"));
                              propertyPlc.setPropertyId(jsonObjectPropertyPlcs.optString("PropertyId"));
                                propertyPlc.setName(jsonObjectPropertyPlcs.optString("Name"));
                                propertyPlc.setChecked(jsonObjectPropertyPlcs.getBoolean("IsChecked"));

                                propertyPlcs.add(propertyPlc);
                            }
                            JSONArray jsonArrayPropertyImages = jsonObject1.getJSONArray("PropertyImages");
                            ArrayList<InventoryModel.PropertyImage> propertyImages = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyImages.length(); k++) {
                                InventoryModel.PropertyImage propertyImage = new InventoryModel.PropertyImage();
                                JSONObject jsonObjectPropertyImages = jsonArrayPropertyImages.getJSONObject(k);
                                propertyImage.setId(jsonObjectPropertyImages.optString("Id"));
                                propertyImage.setName(jsonObjectPropertyImages.optString("Name"));
                                propertyImage.setType(jsonObjectPropertyImages.optString("Type"));
                                propertyImage.setUrl(jsonObjectPropertyImages.optString("Url"));
                                propertyImage.setImageCode(jsonObjectPropertyImages.optString("ImageCode"));
                                propertyImages.add(propertyImage);
                            }
                            JSONArray jsonArrayPropertySides = jsonObject1.getJSONArray("PropertySides");
                            ArrayList<InventoryModel.PropertySides> propertySides = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertySides.length(); k++) {
                                InventoryModel.PropertySides propertySides1 = inventoryModels.new PropertySides();
                                JSONObject jsonObjectPropertySides = jsonArrayPropertySides.getJSONObject(k);
                                propertySides1.setId(jsonObjectPropertySides.optString("Id"));
                                propertySides1.setName(jsonObjectPropertySides.optString("Name"));
                                propertySides1.setLength(jsonObjectPropertySides.optString("Length"));
                                propertySides1.setLengthUnitId(jsonObjectPropertySides.optString("LengthUnitId"));
                                propertySides1.setWidth(jsonObjectPropertySides.optString("Width"));
                                propertySides1.setWidthUnitId(jsonObjectPropertySides.optString("WidthUnitId"));
                                propertySides.add(propertySides1);
                            }
                            JSONArray jsonArrayPropertyFinancials = jsonObject1.getJSONArray("PropertyFinancials");
                            ArrayList<InventoryModel.PropertyFinancials> propertyFinancialses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyFinancials.length(); k++) {
                                InventoryModel.PropertyFinancials propertyFinancials = inventoryModels.new PropertyFinancials();
                                JSONObject jsonObjectPropertyFinancials = jsonArrayPropertyFinancials.getJSONObject(k);
                                propertyFinancials.setId(jsonObjectPropertyFinancials.optString("Id"));
                                propertyFinancials.setDemandPrice(jsonObjectPropertyFinancials.optString("DemandPrice"));
                                propertyFinancials.setInclusivePrice(jsonObjectPropertyFinancials.optBoolean("InclusivePrice"));
                                propertyFinancials.setNegociable(jsonObjectPropertyFinancials.optBoolean("IsNegociable"));
                                propertyFinancials.setPrice(jsonObjectPropertyFinancials.optString("Price"));
                                propertyFinancials.setPriceUnitId(jsonObjectPropertyFinancials.optInt("PriceUnitId"));
                                propertyFinancialses.add(propertyFinancials);

                            }
                            JSONArray jsonArrayPropertyLoans = jsonObject1.getJSONArray("PropertyLoans");
                            ArrayList<InventoryModel.PropertyLoans> propertyLoanses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyLoans.length(); k++) {
                                InventoryModel.PropertyLoans propertyLoans = inventoryModels.new PropertyLoans();
                                JSONObject jsonObjectPropertyLoans = jsonArrayPropertyLoans.getJSONObject(k);
                                propertyLoans.setId(jsonObjectPropertyLoans.optString("Id"));
                                propertyLoans.setLoanAmount(jsonObjectPropertyLoans.optString("LoanAmount"));
                                propertyLoans.setLoanTenure(jsonObjectPropertyLoans.optString("LoanTenure"));
                                propertyLoans.setFullDisbursed(jsonObjectPropertyLoans.optString("FullDisbursed"));
                                propertyLoans.setLastInstallmentPaidOn(jsonObjectPropertyLoans.optString("LastInstallmentPaidOn"));
                                propertyLoans.setTotalOutstandingAfterLastInstallment(jsonObjectPropertyLoans.optString("TotalOutstandingAfterLastInstallment"));
                                propertyLoanses.add(propertyLoans);

                            }
                            JSONArray jsonArrayPropertyOtherExpences = jsonObject1.getJSONArray("PropertyOtherExpences");
                            ArrayList<InventoryModel.PropertyOtherExpences> propertyOtherExpences = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOtherExpences.length(); k++) {
                                InventoryModel.PropertyOtherExpences propertyOtherExpences1 = inventoryModels.new PropertyOtherExpences();
                                JSONObject jsonObjectPropertyOtherExpences = jsonArrayPropertyOtherExpences.getJSONObject(k);
                                propertyOtherExpences1.setId(jsonObjectPropertyOtherExpences.optString("Id"));
                                propertyOtherExpences1.setTitle(jsonObjectPropertyOtherExpences.optString("Title"));
                                propertyOtherExpences1.setValue(jsonObjectPropertyOtherExpences.optString("Value"));
                                propertyOtherExpences1.setLastPaidDate(jsonObjectPropertyOtherExpences.optString("LastPaidDate"));
                                propertyOtherExpences.add(propertyOtherExpences1);

                            }
                            JSONArray jsonArrayPropertyLegalStatus = jsonObject1.getJSONArray("PropertyLegalStatus");
                            ArrayList<InventoryModel.PropertyLegalStatus> propertyLegalStatuses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyLegalStatus.length(); k++) {
                                InventoryModel.PropertyLegalStatus propertyLegalStatus = inventoryModels.new PropertyLegalStatus();
                                JSONObject jsonObjectPropertyOwners = jsonArrayPropertyLegalStatus.getJSONObject(k);
                                propertyLegalStatus.setId(jsonObjectPropertyOwners.optString("Id"));
                                propertyLegalStatus.setPropertyId(jsonObjectPropertyOwners.optString("PropertyId"));
                                propertyLegalStatus.setTransferRestriction(jsonObjectPropertyOwners.optString("TransferRestriction"));
                                propertyLegalStatus.setOwnerShipStatus(jsonObjectPropertyOwners.optString("OwnerShipStatus"));
                                propertyLegalStatus.setOwnerShipType(jsonObjectPropertyOwners.optString("OwnerShipType"));
                                propertyLegalStatus.setTransferByWayOf(jsonObjectPropertyOwners.optString("TransferByWayOf"));
                                propertyLegalStatus.setOwnedBy(jsonObjectPropertyOwners.optString("OwnedBy"));
                                propertyLegalStatus.setPossessionDate(jsonObjectPropertyOwners.optString("PossessionDate"));
                                propertyLegalStatus.setDateFrom(jsonObjectPropertyOwners.optString("DateFrom"));
                                propertyLegalStatus.setNoOfTennure(jsonObjectPropertyOwners.optString("NoOfTennure"));
                                propertyLegalStatus.setLeaseAlignmentPaid(jsonObjectPropertyOwners.optString("LeaseAlignmentPaid"));
                                propertyLegalStatus.setLastPaidDate(jsonObjectPropertyOwners.optString("LastPaidDate"));
                                propertyLegalStatus.setLeaseAmountPayable(jsonObjectPropertyOwners.optString("LeaseAmountPayable"));
                                propertyLegalStatus.setPaymentCircle(jsonObjectPropertyOwners.optString("PaymentCircle"));
                                propertyLegalStatuses.add(propertyLegalStatus);

                            }
                            JSONArray jsonArrayPropertyOwners = jsonObject1.getJSONArray("PropertyOwners");
                            ArrayList<InventoryModel.PropertyOwners> propertyOwnerses = new ArrayList<>();


                            for (int k = 0; k < jsonArrayPropertyOwners.length(); k++) {
                                InventoryModel.PropertyOwners propertyOwners = inventoryModels.new PropertyOwners();
                                JSONObject jsonObjectPropertyOwners = jsonArrayPropertyOwners.getJSONObject(k);
                                propertyOwners.setId(jsonObjectPropertyOwners.optString("Id"));
                                propertyOwners.setContactId(jsonObjectPropertyOwners.optString("ContactId"));
                                propertyOwners.setPanNo(jsonObjectPropertyOwners.optString("PanNo"));
                                propertyOwners.setAdhaarCardNo(jsonObjectPropertyOwners.optString("AdhaarCardNo"));
                                propertyOwners.setAddress(jsonObjectPropertyOwners.optString("Address"));
                                propertyOwners.setFirstName(jsonObjectPropertyOwners.optString("FirstName"));
                                propertyOwners.setLastName(jsonObjectPropertyOwners.optString("LastName"));
                                propertyOwners.setEmail(jsonObjectPropertyOwners.optString("Email"));
                                propertyOwners.setPhoneNumber(jsonObjectPropertyOwners.optString("PhoneNumber"));
                                propertyOwners.setOccupation(jsonObjectPropertyOwners.optString("Occupation"));
                                propertyOwners.setCompanyName(jsonObjectPropertyOwners.optString("CompanyName"));
                                propertyOwners.setCompanyType(jsonObjectPropertyOwners.optString("CompanyType"));

                                if (jsonArrayPropertyOwners.length()>0){

                                    JSONArray jsonArrayPropertyProof = jsonObjectPropertyOwners.getJSONArray("PropertyOwnerShipProofs");
                                    ArrayList<PropertyOwnerShipProofs> propertyOwnersesProof = new ArrayList<>();

                                    for (int jj=0 ; jj<jsonArrayPropertyProof.length(); jj++){

                                        PropertyOwnerShipProofs propertyOwnerShipProofs = new PropertyOwnerShipProofs();
                                        JSONObject jsonObjectPropertyOwnersProof = jsonArrayPropertyProof.getJSONObject(jj);
                                        propertyOwnerShipProofs.setId(jsonObjectPropertyOwnersProof.optString("Id"));
                                        propertyOwnerShipProofs.setUrl(jsonObjectPropertyOwnersProof.optString("Url"));
                                      /*  propertyOwnerShipProofs.setId(jsonObjectPropertyOwnersProof.optString("Id"));
                                    propertyOwnerShipProofs.setId(jsonObjectPropertyOwnersProof.optString("Id"));
*/                                 propertyOwnersesProof.add(propertyOwnerShipProofs);

                                    }

                                    propertyOwners.setPropertyOwnerShipProofs(propertyOwnersesProof);

                                }



                                propertyOwnerses.add(propertyOwners);

                            }

                            JSONArray jsonArrayPropertyOtherquestions = jsonObject1.getJSONArray("PropertyOtherQuestions");
                            ArrayList<InventoryModel.PropertyOtherQuestions> propertyOtherQuestionses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOtherquestions.length(); k++) {
                                InventoryModel.PropertyOtherQuestions propertyOtherQuestions = inventoryModels.new PropertyOtherQuestions();
                                JSONObject jsonObjectPropertyOtherQuestions = jsonArrayPropertyOtherquestions.getJSONObject(k);
                                propertyOtherQuestions.setId(jsonObjectPropertyOtherQuestions.optString("Id"));
                                propertyOtherQuestions.setContactId(jsonObjectPropertyOtherQuestions.optString("ContactId"));
                                propertyOtherQuestions.setRelation(jsonObjectPropertyOtherQuestions.optString("Relation"));
                                propertyOtherQuestions.setSubmitOfBehalfOf(jsonObjectPropertyOtherQuestions.optString("SubmitOfBehalfOf"));
                                propertyOtherQuestions.setAgentId(jsonObjectPropertyOtherQuestions.optString("AgentId"));
                                propertyOtherQuestions.setFirstName(jsonObjectPropertyOtherQuestions.optString("FirstName"));
                                propertyOtherQuestions.setLastName(jsonObjectPropertyOtherQuestions.optString("LastName"));
                                propertyOtherQuestions.setEmail(jsonObjectPropertyOtherQuestions.optString("Email"));
                                propertyOtherQuestions.setContactNo(jsonObjectPropertyOtherQuestions.optString("ContactNo"));

                                propertyOtherQuestionses.add(propertyOtherQuestions);

                            }
                            JSONArray jsonArrayPropertyAccommodationDetails = jsonObject1.getJSONArray("PropertyAccommodationDetails");
                            ArrayList<InventoryModel.PropertyAccommodationDetails> propertyAccommodationDetails = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyAccommodationDetails.length(); k++) {
                                InventoryModel.PropertyAccommodationDetails propertyAccommodationDetails1 = inventoryModels.new PropertyAccommodationDetails();
                                JSONObject jsonObjectPropertyAccommodationDetails = jsonArrayPropertyAccommodationDetails.getJSONObject(k);
                                propertyAccommodationDetails1.setId(jsonObjectPropertyAccommodationDetails.optString("Id"));
                                propertyAccommodationDetails1.setPropertyId(jsonObjectPropertyAccommodationDetails.optString("PropertyId"));
                                propertyAccommodationDetails1.setName(jsonObjectPropertyAccommodationDetails.optString("Name"));
                                propertyAccommodationDetails1.setType(jsonObjectPropertyAccommodationDetails.optString("Type"));
                                propertyAccommodationDetails1.setTotalCount(jsonObjectPropertyAccommodationDetails.optString("TotalCount"));

                                propertyAccommodationDetails.add(propertyAccommodationDetails1);

                            }

                            JSONArray jsonArraySpecifictions = jsonObject1.getJSONArray("PropertySpecifications");
                            ArrayList<InventoryModel.PropertySpecifications> propertySpecifications = new ArrayList<>();
                            for (int k = 0; k < jsonArraySpecifictions.length(); k++) {
                                InventoryModel.PropertySpecifications propertySpecifications1 = inventoryModels.new PropertySpecifications();
                                JSONObject jsonObjectPropertyAccommodationDetails = jsonArraySpecifictions.getJSONObject(k);
                                propertySpecifications1.setId(jsonObjectPropertyAccommodationDetails.optString("Id"));
                                propertySpecifications1.setName(jsonObjectPropertyAccommodationDetails.optString("Name"));
                                propertySpecifications1.setDescription(jsonObjectPropertyAccommodationDetails.optString("Description"));
                                propertySpecifications1.setCategory(jsonObjectPropertyAccommodationDetails.optString("Category"));
                                propertySpecifications1.setCapacity(jsonObjectPropertyAccommodationDetails.optString("Capacity"));

                                ArrayList<SpecificationListModel.PropertyBedroom> propertySpecificationsBedrooms = new ArrayList<>();

                                propertySpecificationsBedrooms.clear();

/*
                                for (int h=0; h<jsonObjectPropertyAccommodationDetails.getJSONArray("PropertyBedrooms").length();h++){

                                    SpecificationListModel.PropertyBedroom  propertyBedroom= new  SpecificationListModel.PropertyBedroom();
                                    JSONObject jsonObjectPropertyAccommodationDetailsRoom = jsonArraySpecifictions.getJSONObject(h);

                                    propertyBedroom.setId(jsonObjectPropertyAccommodationDetailsRoom.optString("Id"));

                                }
*/

                           //     propertySpecifications1.setPropertyBedrooms(propertySpecificationsBedrooms);

                                propertySpecifications.add(propertySpecifications1);

                            }

                            inventoryModels.setPropertyLoanses(propertyLoanses);
                            inventoryModels.setPropertyLocations(propertyLocations);
                            inventoryModels.setPropertyAreas(propertyAreas);
                            inventoryModels.setPropertyFinancialses(propertyFinancialses);
                            inventoryModels.setPropertyImages(propertyImages);
                            inventoryModels.setPropertyOtherExpences(propertyOtherExpences);
                            inventoryModels.setPropertyPlcs(propertyPlcs);
                            inventoryModels.setPropertyOtherQuestionses(propertyOtherQuestionses);
                            inventoryModels.setPropertyOwnerses(propertyOwnerses);
                            inventoryModels.setPropertyLegalStatuses(propertyLegalStatuses);
                            inventoryModels.setPropertyAccommodationDetailses(propertyAccommodationDetails);
                            inventoryModels.setPropertySpecificationses(propertySpecifications);

                            inventoryModelArrayList.add(inventoryModels);


                        }


                        InventoryFragment inventoryFragment = new InventoryFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("property_data", inventoryModelArrayList);


                        inventoryFragment.setArguments(bundle);
//                      Bundle inventorybundle = new Bundle();

//                      inventoryFragment.setArguments(inventorybundle);
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myproperty, inventoryFragment).commit();
                        bindWidgetsWithAnEvent();


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }

}

