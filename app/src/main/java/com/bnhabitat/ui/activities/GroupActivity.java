package com.bnhabitat.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.NewGroupItemsModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class GroupActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener {

    ArrayList<NewGroupItemsModel> arrayListgroupnames = new ArrayList<>();
    ArrayList<NewGroupItemsModel> arrayListgroupnamesTemp = new ArrayList<>();
    private RecyclerView groups;
    LinearLayout no_contact_found;
    LinearLayoutManager linearLayoutManager;
    EditText search_contacts;
    ImageView create_group;
    ImageView back_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        groups = (RecyclerView) findViewById(R.id.groups);
        no_contact_found = (LinearLayout) findViewById(R.id.no_contact_found);
        search_contacts = (EditText) findViewById(R.id.search_contacts);
        create_group=(ImageView) findViewById(R.id.create_group);
        back_button=(ImageView) findViewById(R.id.back);


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });
        linearLayoutManager = new LinearLayoutManager(this);
        onGetRequest();
        create_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GroupActivity.this,CreateNewGroupActivity.class));
            }
        });
        search_contacts.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                arrayListgroupnamesTemp.clear();
                List<NewGroupItemsModel> list = filter(s.toString(), arrayListgroupnames, true);


                arrayListgroupnamesTemp.addAll(list);
                if (arrayListgroupnamesTemp.isEmpty()) {
                    groups.setVisibility(View.GONE);
                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
                    groups.setVisibility(View.VISIBLE);
                    no_contact_found.setVisibility(View.GONE);
                    groups.setAdapter(new NewGroupAdapter(arrayListgroupnamesTemp,GroupActivity.this));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }
    public static List<NewGroupItemsModel> filter(String string,
                                                  List<NewGroupItemsModel> iterable, boolean byName) {
        if (iterable == null)
            return new LinkedList<>();
        else {
            List<NewGroupItemsModel> collected = new LinkedList<>();
            Iterator<NewGroupItemsModel> iterator = iterable.iterator();
            if (iterator == null)
                return collected;
            while (iterator.hasNext()) {
                NewGroupItemsModel item = iterator.next();

                if (item.getName().toLowerCase().contains(string.toLowerCase()))
                    collected.add(item);

            }
            return collected;
        }
    }
    private void onGetRequest() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonAsync(GroupActivity.this,  Urls.CONTACT_MANAGEMENT_URL+ Urls.URL_GROUP_LISTING+ PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""),
                "",
                "Loading..",
                GroupActivity.this,
                Urls.URL_GROUP_LISTING,
                Constants.GET).execute();

//
    }
    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_GROUP_LISTING)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        if (projectsArray.length() == 0) {
                            Toast.makeText(GroupActivity.this, "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                        }
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            NewGroupItemsModel newGroupItemsModel = new NewGroupItemsModel();

                            newGroupItemsModel.setId(jsonObject1.getString("Id"));
                            newGroupItemsModel.setName(jsonObject1.getString("GroupName"));
                            newGroupItemsModel.setDescription(jsonObject1.getString("Description"));
//                            newGroupItemsModel.setSlug(jsonObject1.getString("Slug"));
//                            newGroupItemsModel.setTaxonomyId(jsonObject1.getString("TaxonomyId"));

                            arrayListgroupnames.add(newGroupItemsModel);

                        }
                        NewGroupAdapter createNewGroupAdapter = new NewGroupAdapter(arrayListgroupnames, GroupActivity.this);

                        groups.setLayoutManager(linearLayoutManager);
                        groups.setAdapter(createNewGroupAdapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }
        public class NewGroupAdapter extends RecyclerView.Adapter<NewGroupAdapter.ViewHolder> {
            ArrayList<NewGroupItemsModel> arrayList = new ArrayList<>();
            Context context;

            public NewGroupAdapter(ArrayList<NewGroupItemsModel> arrayList, Context context) {
                this.arrayList = arrayList;
                this.context = context;

//                contactListsClicked = new ArrayList<>();
//
//                for (NewGroupItemsModel contact : arrayList) {
//                    contactListsClicked.add(false);
//                }
            }

            @Override
            public NewGroupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_group_items, parent, false);
                NewGroupAdapter.ViewHolder viewHolder = new NewGroupAdapter.ViewHolder(itemView);


                return viewHolder;
            }

            @Override
            public void onBindViewHolder(NewGroupAdapter.ViewHolder holder, final int position) {

                int i = position;

                holder.contact_name.setText(arrayList.get(i).getName());
                holder.desc.setText(arrayList.get(i).getDescription());

                holder.contact_select_image.setBackground(getResources().getDrawable(R.drawable.grey_tick));

//                if (selectedPosition == position) {
//
//                    holder.contact_select_image.setBackground(getResources().getDrawable(R.drawable.blue_tick));
//
//                }

//                holder.contact_select_image.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        selecteditem=arrayList.get(position).getId();
//                        if (selectedPosition == position) {
//
//
//                            selectedPosition = -1;
//                        } else
//                            selectedPosition = position;
//                      NewGroupAdapter.this.notifyDataSetChanged();
//
//                    }
//                });

            }

            @Override
            public int getItemCount() {
                return arrayList.size();
            }

            public class ViewHolder extends RecyclerView.ViewHolder {
                TextView contact_name,desc;
                ImageView contact_select_image;

                public ViewHolder(View itemView) {
                    super(itemView);
                    contact_name = (TextView) itemView.findViewById(R.id.contact_name);
                    desc = (TextView) itemView.findViewById(R.id.desc);

                    contact_select_image = (ImageView) itemView.findViewById(R.id.contact_select_image);
                }
            }
        }
}
