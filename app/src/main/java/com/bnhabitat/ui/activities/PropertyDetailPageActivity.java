package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.ui.adapters.ForyouAdapter;
import com.bnhabitat.ui.adapters.GalleryAdapter;
import com.bnhabitat.ui.adapters.GalleryListAdapter;
import com.bnhabitat.ui.adapters.GalleryListAdapterInventory;
import com.bnhabitat.ui.fragments.AccomodationViewFragment;
import com.bnhabitat.ui.fragments.BuiltViewFragment;
import com.bnhabitat.ui.fragments.DetailFragment;
import com.bnhabitat.ui.fragments.Financial_Bank_viewFragment;
import com.bnhabitat.ui.fragments.FinancialandBankLoanFragment;
import com.bnhabitat.ui.fragments.LegalStatusFragment;
import com.bnhabitat.ui.fragments.LegalStatusViewFragment;
import com.bnhabitat.ui.fragments.LocationFragment;
import com.bnhabitat.ui.fragments.OtherExpensesFragment;
import com.bnhabitat.ui.fragments.OtherQuestionViewFragment;
import com.bnhabitat.ui.fragments.OtherQuestionsFragment;
import com.bnhabitat.ui.fragments.OtherexpensesViewFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PropertyDetailPageActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener {
    RecyclerView gallery_list;
    TabLayout tab_layout_detail;
    TabLayout.Tab Tab1, Tab2, Tab3, Tab4, Tab5, Tab6, Tab7, Tab8;
    ImageView project_image, edit_property, imgShare;
    LinearLayoutManager layoutManager;
    ImageView back_btn;
    String property_id;
    TextView property_type, locality, area_unit, price, bedroom, kitchen, parking, bathroom, property_type_txt;
    int room_count = 0, kitchen_count = 0, bathroom_count = 0, parking_count = 0;
    ArrayList<InventoryModel> property_data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail_page);
        gallery_list = (RecyclerView) findViewById(R.id.gallery_list);
        property_type = (TextView) findViewById(R.id.property_type);
        area_unit = (TextView) findViewById(R.id.area_unit);
        price = (TextView) findViewById(R.id.price);
        property_type_txt = (TextView) findViewById(R.id.property_type_txt);
        locality = (TextView) findViewById(R.id.locality);
        parking = (TextView) findViewById(R.id.parking);
        kitchen = (TextView) findViewById(R.id.kitchen);
        bedroom = (TextView) findViewById(R.id.bedroom);
        bathroom = (TextView) findViewById(R.id.bathroom);
        project_image = (ImageView) findViewById(R.id.project_image);
        edit_property = (ImageView) findViewById(R.id.edit_property);
        tab_layout_detail = (TabLayout) findViewById(R.id.tab_layout_detail);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        imgShare = (ImageView) findViewById(R.id.imgShare);

        property_id = getIntent().getStringExtra(Constants.PROPERTY_ID) == null ? "" : getIntent().getStringExtra(Constants.PROPERTY_ID);
        Tab1 = tab_layout_detail.newTab().setText("Built Up");
        Tab2 = tab_layout_detail.newTab().setText("Accomodation");
        Tab3 = tab_layout_detail.newTab().setText("Financial & Bank Loan's");
        Tab4 = tab_layout_detail.newTab().setText("Other Expenses");
        Tab5 = tab_layout_detail.newTab().setText("Legal Status");
        Tab6 = tab_layout_detail.newTab().setText("Other Questions");
        tab_layout_detail.addTab(Tab1);
        tab_layout_detail.addTab(Tab2);
        tab_layout_detail.addTab(Tab3);
        tab_layout_detail.addTab(Tab4);
        tab_layout_detail.addTab(Tab5);
        tab_layout_detail.addTab(Tab6);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        linearLayoutManager = new GridLayoutManager(getActivity(),2);
        gallery_list.setLayoutManager(layoutManager);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);

                intent.setType("text/html");
                // intent.putExtra(Intent.EXTRA_SUBJECT, "Download app");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intent, "Choose"));

            }
        });


        getDetailData();

        BuiltViewFragment builtViewFragment = new BuiltViewFragment();
        Bundle builtViewbundle = new Bundle();
        builtViewbundle.putSerializable("property_data", property_data);
        builtViewFragment.setArguments(builtViewbundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, builtViewFragment).commit();
        tab_layout_detail.getTabAt(0);
        Tab1.select();

        bindWidgetsWithAnEvent();

    }

    public void bindWidgetsWithAnEvent() {
        tab_layout_detail.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                BuiltViewFragment builtViewFragment = new BuiltViewFragment();
                Bundle builtViewbundle = new Bundle();

//                locationbundle.putString("PropertyId", PropertyId);


                builtViewbundle.putSerializable("property_data", property_data);
                builtViewFragment.setArguments(builtViewbundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, builtViewFragment).commit();

//
                break;
            case 1:
                AccomodationViewFragment accomodationViewFragment = new AccomodationViewFragment();
                Bundle bundle = new Bundle();
//                bundle.putString("PropertyId", PropertyId);


                bundle.putSerializable("property_data", property_data);
                accomodationViewFragment.setArguments(bundle);
                //Inflate the fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, accomodationViewFragment).commit();

                break;
            case 2:
                try {
                    Financial_Bank_viewFragment financial_bank_viewFragment = new Financial_Bank_viewFragment();
                    Bundle bundle1 = new Bundle();

//                    bundle1.putString("PropertyId", PropertyId);
                    bundle1.putSerializable("property_data", property_data);

                    financial_bank_viewFragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, financial_bank_viewFragment).commit();


                } catch (Exception e) {
                    e.printStackTrace();
                }
//
                break;
            case 3:
                try {
                    OtherexpensesViewFragment otherexpensesViewFragment = new OtherexpensesViewFragment();
                    Bundle bundle1 = new Bundle();

//                    bundle1.putString("PropertyId", PropertyId);

                    bundle1.putSerializable("property_data", property_data);
                    otherexpensesViewFragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, otherexpensesViewFragment).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
//
//
                break;
            case 4:
                try {
                    LegalStatusViewFragment legalStatusViewFragment = new LegalStatusViewFragment();
                    Bundle bundle1 = new Bundle();

//                    bundle1.putString("PropertyId", PropertyId);

                    bundle1.putSerializable("property_data", property_data);
                    legalStatusViewFragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, legalStatusViewFragment).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
//
                break;
            case 5:
                try {
                    OtherQuestionViewFragment otherQuestionViewFragment = new OtherQuestionViewFragment();
                    Bundle bundle1 = new Bundle();


//                    bundle1.putString("PropertyId", PropertyId);

                    bundle1.putSerializable("property_data", property_data);
                    otherQuestionViewFragment.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.fragment_container_detail, otherQuestionViewFragment).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
//
                break;
        }
    }

    private void getDetailData() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_DETAIL + property_id,
                "",
                "Loading...",
                this,
                Urls.URL_PROPERTY_DETAIL,
                Constants.GET).execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_DETAIL)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject propertySize = jsonObject.getJSONObject("Result");

                        if (propertySize.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                            InventoryModel inventoryModels = new InventoryModel();

                            inventoryModels.setId(propertySize.getString("Id"));
                            inventoryModels.setPropertyTypeId(propertySize.getString("PropertyTypeId"));
                            inventoryModels.setCompanyId(propertySize.getString("CompanyId"));
                            inventoryModels.setCam(propertySize.getString("Cam"));
                            inventoryModels.setTitle(propertySize.getString("Title"));
                            inventoryModels.setCreatedById(propertySize.getString("CreatedById"));

                            JSONObject objectPropertyType = propertySize.getJSONObject("PropertyType");
                            inventoryModels.setPropertyobjectId(objectPropertyType.getString("Id"));
                            inventoryModels.setNameobject(objectPropertyType.getString("Name"));
                            inventoryModels.setTypeobject(objectPropertyType.getString("Type"));
                            JSONArray jsonArray = propertySize.getJSONArray("PropertyLocations");
                            ArrayList<InventoryModel.PropertyLocation> propertyLocations = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                InventoryModel.PropertyLocation propertyLocation = inventoryModels.new PropertyLocation();
                                JSONObject jsonObjectPropertyLocations = jsonArray.getJSONObject(i);
                                propertyLocation.setId(jsonObjectPropertyLocations.getString("Id"));
                                propertyLocation.setPropertyId(jsonObjectPropertyLocations.getString("PropertyId"));
                                propertyLocation.setDeveloper(jsonObjectPropertyLocations.getString("Developer"));
                                propertyLocation.setProject(jsonObjectPropertyLocations.getString("Project"));
                                propertyLocation.setZipCode(jsonObjectPropertyLocations.getString("ZipCode"));
                                propertyLocation.setState(jsonObjectPropertyLocations.getString("State"));
                                propertyLocation.setDistrict(jsonObjectPropertyLocations.getString("District"));
                                propertyLocation.setSubArea(jsonObjectPropertyLocations.getString("SubArea"));
                                propertyLocation.setUnitName(jsonObjectPropertyLocations.getString("UnitNo"));
                                propertyLocation.setTowerName(jsonObjectPropertyLocations.getString("TowerName"));
                                propertyLocation.setFloorNo(jsonObjectPropertyLocations.getString("FloorNo"));
                                propertyLocation.setGooglePlusCode(jsonObjectPropertyLocations.getString("GooglePlusCode"));
                                if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue")) {
                                    JSONObject areaatribute = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue");
                                    propertyLocation.setAreaAttributeValue(areaatribute.getString("Value"));
                                }
                                if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue1")) {
                                    JSONObject areaatribute1 = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue1");
                                    propertyLocation.setAreaAttributeValue1(areaatribute1.getString("Value"));
                                }
                                if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue2")) {
                                    JSONObject areaatribute2 = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue2");
                                    propertyLocation.setAreaAttributeValue2(areaatribute2.getString("Value"));
                                }
                                if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue3")) {
                                    JSONObject areaatribute3 = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue3");
                                    propertyLocation.setAreaAttributeValue3(areaatribute3.getString("Value"));
                                }
                                if (!jsonObjectPropertyLocations.isNull("AreaAttributeValue4")) {
                                    JSONObject areaatribute4 = jsonObjectPropertyLocations.getJSONObject("AreaAttributeValue4");
                                    propertyLocation.setAreaAttributeValue4(areaatribute4.getString("Value"));
                                }
                                propertyLocations.add(propertyLocation);


                            }
                            JSONArray jsonArrayPropertyAreas = propertySize.getJSONArray("PropertyAreas");
                            ArrayList<InventoryModel.PropertyArea> propertyAreas = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyAreas.length(); k++) {
                                JSONObject jsonObjectPropertyLocations = jsonArrayPropertyAreas.getJSONObject(k);
                                InventoryModel.PropertyArea propertyArea = inventoryModels.new PropertyArea();
                                propertyArea.setId(jsonObjectPropertyLocations.getString("Id"));
                                propertyArea.setPropertyId(jsonObjectPropertyLocations.getString("PropertyId"));
                                propertyArea.setPlotShape(jsonObjectPropertyLocations.getString("PlotShape"));
                                propertyArea.setPlotArea(jsonObjectPropertyLocations.getString("PlotArea"));
                                propertyArea.setPlotAreaUnitId(jsonObjectPropertyLocations.getInt("PlotAreaUnitId"));
                                propertyArea.setFrontSize(jsonObjectPropertyLocations.getString("FrontSize"));
                                propertyArea.setFrontSizeUnitId(jsonObjectPropertyLocations.getInt("FrontSizeUnitId"));
                                propertyArea.setDepthSize(jsonObjectPropertyLocations.getString("DepthSize"));
                                propertyArea.setDepthSizeUnitId(jsonObjectPropertyLocations.getString("DepthSizeUnitId"));
                                propertyArea.setStreetOrRoadType(jsonObjectPropertyLocations.getString("StreetOrRoadType"));
                                propertyArea.setRoadWidth(jsonObjectPropertyLocations.getString("RoadWidth"));
                                propertyArea.setRoadWidthUnitId(jsonObjectPropertyLocations.getInt("RoadWidthUnitId"));
                                propertyArea.setEnteranceDoorFacing(jsonObjectPropertyLocations.getString("EnteranceDoorFacing"));
                                propertyArea.setParkingInFront(jsonObjectPropertyLocations.getString("ParkingInFront"));
                                propertyArea.setHaveWalkingPath(jsonObjectPropertyLocations.getString("HaveWalkingPath"));
                                propertyArea.setPlotOrCoverArea(jsonObjectPropertyLocations.getString("PlotOrCoverArea"));
                                propertyArea.setPlotOrCoverAreaUnitId(jsonObjectPropertyLocations.getString("PlotOrCoverAreaUnitId"));
                                propertyArea.setSuperArea(jsonObjectPropertyLocations.getString("SuperArea"));
                                propertyArea.setSuperAreaUnitId(jsonObjectPropertyLocations.getString("SuperAreaUnitId"));
                                propertyArea.setBuiltUpArea(jsonObjectPropertyLocations.getString("BuiltUpArea"));
                                propertyArea.setBuiltUpAreaUnitId(jsonObjectPropertyLocations.getString("BuiltUpAreaUnitId"));
                                propertyArea.setCarpetArea(jsonObjectPropertyLocations.getString("CarpetArea"));
                                propertyArea.setCarpetAreaUnitId(jsonObjectPropertyLocations.getString("CarpetAreaUnitId"));
                                if (!jsonObjectPropertyLocations.isNull("PropertySizeUnit4")) {
                                    JSONObject jsonObjecPropertySizeUnit4 = jsonObjectPropertyLocations.getJSONObject("PropertySizeUnit4");
                                    propertyArea.setPropertySizeUnit4(jsonObjecPropertySizeUnit4.getString("SizeUnit"));
                                }
                                propertyAreas.add(propertyArea);

                            }
                            JSONArray jsonArrayPropertyPlcs = propertySize.getJSONArray("PropertyPlcs");
                            ArrayList<InventoryModel.PropertyPlc> propertyPlcs = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyPlcs.length(); k++) {
                                InventoryModel.PropertyPlc propertyPlc = inventoryModels.new PropertyPlc();
                                JSONObject jsonObjectPropertyPlcs = jsonArrayPropertyPlcs.getJSONObject(k);
                                propertyPlc.setId(jsonObjectPropertyPlcs.getString("Id"));
                                propertyPlc.setPropertyId(jsonObjectPropertyPlcs.getString("PropertyId"));
                                propertyPlc.setName(jsonObjectPropertyPlcs.getString("Name"));
                                propertyPlcs.add(propertyPlc);
                            }
                            JSONArray jsonArrayPropertyImages = propertySize.getJSONArray("PropertyImages");
                            ArrayList<InventoryModel.PropertyImage> propertyImages = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyImages.length(); k++) {
                                InventoryModel.PropertyImage propertyImage = new InventoryModel.PropertyImage();
                                JSONObject jsonObjectPropertyImages = jsonArrayPropertyImages.getJSONObject(k);
                                propertyImage.setId(jsonObjectPropertyImages.getString("Id"));
                                propertyImage.setName(jsonObjectPropertyImages.getString("Name"));
                                propertyImage.setType(jsonObjectPropertyImages.getString("Type"));
                                propertyImage.setUrl(jsonObjectPropertyImages.getString("Url"));
                                propertyImage.setImageCode(jsonObjectPropertyImages.getString("ImageCode"));
                                propertyImages.add(propertyImage);
                            }
                            JSONArray jsonArrayPropertySides = propertySize.getJSONArray("PropertySides");
                            ArrayList<InventoryModel.PropertySides> propertySides = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertySides.length(); k++) {
                                InventoryModel.PropertySides propertySides1 = inventoryModels.new PropertySides();
                                JSONObject jsonObjectPropertySides = jsonArrayPropertySides.getJSONObject(k);
                                propertySides1.setId(jsonObjectPropertySides.getString("Id"));
                                propertySides1.setName(jsonObjectPropertySides.getString("Name"));
                                propertySides1.setLength(jsonObjectPropertySides.getString("Length"));
                                propertySides1.setLengthUnitId(jsonObjectPropertySides.getString("LengthUnitId"));
                                propertySides1.setWidth(jsonObjectPropertySides.getString("Width"));
                                propertySides1.setWidthUnitId(jsonObjectPropertySides.getString("WidthUnitId"));
                                propertySides.add(propertySides1);
                            }
                            JSONArray jsonArrayPropertyFinancials = propertySize.getJSONArray("PropertyFinancials");
                            ArrayList<InventoryModel.PropertyFinancials> propertyFinancialses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyFinancials.length(); k++) {
                                InventoryModel.PropertyFinancials propertyFinancials = inventoryModels.new PropertyFinancials();
                                JSONObject jsonObjectPropertyFinancials = jsonArrayPropertyFinancials.getJSONObject(k);
                                propertyFinancials.setId(jsonObjectPropertyFinancials.getString("Id"));
                                propertyFinancials.setDemandPrice(jsonObjectPropertyFinancials.getString("DemandPrice"));
                                propertyFinancials.setInclusivePrice(jsonObjectPropertyFinancials.optBoolean("InclusivePrice"));
                                propertyFinancials.setNegociable(jsonObjectPropertyFinancials.optBoolean("IsNegociable"));
                                propertyFinancials.setPrice(jsonObjectPropertyFinancials.getString("Price"));
                                propertyFinancials.setPriceUnitId(jsonObjectPropertyFinancials.getInt("PriceUnitId"));
                                propertyFinancialses.add(propertyFinancials);

                            }
                            JSONArray jsonArrayPropertyLoans = propertySize.getJSONArray("PropertyLoansModel");
                            ArrayList<InventoryModel.PropertyLoans> propertyLoanses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyLoans.length(); k++) {
                                InventoryModel.PropertyLoans propertyLoans = inventoryModels.new PropertyLoans();
                                JSONObject jsonObjectPropertyLoans = jsonArrayPropertyLoans.getJSONObject(k);
                                propertyLoans.setId(jsonObjectPropertyLoans.getString("Id"));
                                propertyLoans.setLoanAmount(jsonObjectPropertyLoans.getString("LoanAmount"));
                                propertyLoans.setLoanTenure(jsonObjectPropertyLoans.getString("LoanTenure"));
                                propertyLoans.setFullDisbursed(jsonObjectPropertyLoans.getString("FullDisbursed"));
                                propertyLoans.setLastInstallmentPaidOn(jsonObjectPropertyLoans.getString("LastInstallmentPaidOn"));
                                propertyLoans.setTotalOutstandingAfterLastInstallment(jsonObjectPropertyLoans.getString("TotalOutstandingAfterLastInstallment"));
                                propertyLoanses.add(propertyLoans);

                            }
                            JSONArray jsonArrayPropertyOtherExpences = propertySize.getJSONArray("PropertyOtherExpences");
                            ArrayList<InventoryModel.PropertyOtherExpences> propertyOtherExpences = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOtherExpences.length(); k++) {
                                InventoryModel.PropertyOtherExpences propertyOtherExpences1 = inventoryModels.new PropertyOtherExpences();
                                JSONObject jsonObjectPropertyOtherExpences = jsonArrayPropertyOtherExpences.getJSONObject(k);
                                propertyOtherExpences1.setId(jsonObjectPropertyOtherExpences.getString("Id"));
                                propertyOtherExpences1.setTitle(jsonObjectPropertyOtherExpences.getString("Title"));
                                propertyOtherExpences1.setValue(jsonObjectPropertyOtherExpences.getString("Value"));
                                propertyOtherExpences1.setLastPaidDate(jsonObjectPropertyOtherExpences.getString("LastPaidDate"));
                                propertyOtherExpences.add(propertyOtherExpences1);

                            }
                            JSONArray jsonArrayPropertyLegalStatus = propertySize.getJSONArray("PropertyLegalStatus");
                            ArrayList<InventoryModel.PropertyLegalStatus> propertyLegalStatuses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyLegalStatus.length(); k++) {
                                InventoryModel.PropertyLegalStatus propertyLegalStatus = inventoryModels.new PropertyLegalStatus();
                                JSONObject jsonObjectPropertyOwners = jsonArrayPropertyLegalStatus.getJSONObject(k);
                                propertyLegalStatus.setId(jsonObjectPropertyOwners.getString("Id"));
                                propertyLegalStatus.setPropertyId(jsonObjectPropertyOwners.getString("PropertyId"));
                                propertyLegalStatus.setTransferRestriction(jsonObjectPropertyOwners.getString("TransferRestriction"));
                                propertyLegalStatus.setOwnerShipStatus(jsonObjectPropertyOwners.getString("OwnerShipStatus"));
                                propertyLegalStatus.setOwnerShipType(jsonObjectPropertyOwners.getString("OwnerShipType"));
                                propertyLegalStatus.setTransferByWayOf(jsonObjectPropertyOwners.getString("TransferByWayOf"));
                                propertyLegalStatus.setOwnedBy(jsonObjectPropertyOwners.getString("OwnedBy"));
                                propertyLegalStatus.setPossessionDate(jsonObjectPropertyOwners.getString("PossessionDate"));
                                propertyLegalStatus.setDateFrom(jsonObjectPropertyOwners.getString("DateFrom"));
                                propertyLegalStatus.setNoOfTennure(jsonObjectPropertyOwners.getString("NoOfTennure"));
                                propertyLegalStatus.setLeaseAlignmentPaid(jsonObjectPropertyOwners.getString("LeaseAlignmentPaid"));
                                propertyLegalStatus.setLastPaidDate(jsonObjectPropertyOwners.getString("LastPaidDate"));
                                propertyLegalStatus.setLeaseAmountPayable(jsonObjectPropertyOwners.getString("LeaseAmountPayable"));
                                propertyLegalStatus.setPaymentCircle(jsonObjectPropertyOwners.getString("PaymentCircle"));
                                propertyLegalStatuses.add(propertyLegalStatus);

                            }
                            JSONArray jsonArrayPropertyOwners = propertySize.getJSONArray("PropertyOwners");
                            ArrayList<InventoryModel.PropertyOwners> propertyOwnerses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOwners.length(); k++) {
                                InventoryModel.PropertyOwners propertyOwners = inventoryModels.new PropertyOwners();
                                JSONObject jsonObjectPropertyOwners = jsonArrayPropertyOwners.getJSONObject(k);
                                propertyOwners.setId(jsonObjectPropertyOwners.getString("Id"));
                                propertyOwners.setContactId(jsonObjectPropertyOwners.getString("ContactId"));
                                propertyOwners.setPanNo(jsonObjectPropertyOwners.getString("PanNo"));
                                propertyOwners.setAdhaarCardNo(jsonObjectPropertyOwners.getString("AdhaarCardNo"));
                                propertyOwners.setAddress(jsonObjectPropertyOwners.getString("Address"));
                                propertyOwners.setFirstName(jsonObjectPropertyOwners.getString("FirstName"));
                                propertyOwners.setLastName(jsonObjectPropertyOwners.getString("LastName"));
                                propertyOwners.setEmail(jsonObjectPropertyOwners.getString("Email"));
                                propertyOwners.setPhoneNumber(jsonObjectPropertyOwners.getString("PhoneNumber"));
                                propertyOwners.setOccupation(jsonObjectPropertyOwners.getString("Occupation"));
                                propertyOwners.setCompanyName(jsonObjectPropertyOwners.getString("CompanyName"));
                                propertyOwners.setCompanyType(jsonObjectPropertyOwners.getString("CompanyType"));
                                propertyOwnerses.add(propertyOwners);

                            }

                            JSONArray jsonArrayPropertyOtherquestions = propertySize.getJSONArray("PropertyOtherQuestions");
                            ArrayList<InventoryModel.PropertyOtherQuestions> propertyOtherQuestionses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyOtherquestions.length(); k++) {
                                InventoryModel.PropertyOtherQuestions propertyOtherQuestions = inventoryModels.new PropertyOtherQuestions();
                                JSONObject jsonObjectPropertyOtherQuestions = jsonArrayPropertyOtherquestions.getJSONObject(k);
                                propertyOtherQuestions.setId(jsonObjectPropertyOtherQuestions.getString("Id"));
                                propertyOtherQuestions.setContactId(jsonObjectPropertyOtherQuestions.getString("ContactId"));
                                propertyOtherQuestions.setRelation(jsonObjectPropertyOtherQuestions.getString("Relation"));
                                propertyOtherQuestions.setSubmitOfBehalfOf(jsonObjectPropertyOtherQuestions.getString("SubmitOfBehalfOf"));
                                propertyOtherQuestions.setAgentId(jsonObjectPropertyOtherQuestions.getString("AgentId"));
                                propertyOtherQuestions.setFirstName(jsonObjectPropertyOtherQuestions.getString("FirstName"));
                                propertyOtherQuestions.setLastName(jsonObjectPropertyOtherQuestions.getString("LastName"));
                                propertyOtherQuestions.setEmail(jsonObjectPropertyOtherQuestions.getString("Email"));
                                propertyOtherQuestionses.add(propertyOtherQuestions);

                            }
//                                JSONArray jsonArrayPropertyAccommodationDetails = propertySize.getJSONArray("PropertyAccommodationDetails");
//                                ArrayList<InventoryModel.PropertyAccommodationDetails> propertyAccommodationDetails = new ArrayList<>();
//                                for (int k = 0; k < jsonArrayPropertyAccommodationDetails.length(); k++) {
//                                    InventoryModel.PropertyAccommodationDetails propertyAccommodationDetails1 = inventoryModels.new PropertyAccommodationDetails();
//                                    JSONObject jsonObjectPropertyAccommodationDetails = jsonArrayPropertyAccommodationDetails.getJSONObject(k);
//                                    propertyAccommodationDetails1.setId(jsonObjectPropertyAccommodationDetails.getString("Id"));
//                                    propertyAccommodationDetails1.setName(jsonObjectPropertyAccommodationDetails.getString("Name"));
//                                    propertyAccommodationDetails1.setType(jsonObjectPropertyAccommodationDetails.getString("Type"));
//                                    propertyAccommodationDetails1.setTotalCount(jsonObjectPropertyAccommodationDetails.getString("TotalCount"));
//
//                                    propertyAccommodationDetails.add(propertyAccommodationDetails1);
//
//                                }
                            JSONArray jsonArrayPropertyBedrooms = propertySize.getJSONArray("PropertyBedrooms");
                            ArrayList<InventoryModel.PropertyBedrooms> propertyBedroomses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertyBedrooms.length(); k++) {
                                InventoryModel.PropertyBedrooms propertyBedrooms = inventoryModels.new PropertyBedrooms();
                                JSONObject jsonObjectPropertyBedrooms = jsonArrayPropertyBedrooms.getJSONObject(k);
                                propertyBedrooms.setId(jsonObjectPropertyBedrooms.getString("Id"));
                                propertyBedrooms.setBedRoomName(jsonObjectPropertyBedrooms.getString("BedRoomName"));
                                propertyBedrooms.setGroundType(jsonObjectPropertyBedrooms.getString("GroundType"));
                                propertyBedrooms.setFloorNo(jsonObjectPropertyBedrooms.getString("FloorNo"));
                                propertyBedrooms.setBedRoomType(jsonObjectPropertyBedrooms.getString("BedRoomType"));
                                propertyBedrooms.setCategory(jsonObjectPropertyBedrooms.getString("Category"));

                                propertyBedroomses.add(propertyBedrooms);

                            }

                            JSONArray jsonArrayPropertySpecifications = propertySize.getJSONArray("PropertySpecifications");
                            ArrayList<InventoryModel.PropertySpecifications> propertySpecificationses = new ArrayList<>();
                            for (int k = 0; k < jsonArrayPropertySpecifications.length(); k++) {
                                InventoryModel.PropertySpecifications propertySpecifications = inventoryModels.new PropertySpecifications();
                                JSONObject jsonObjectPropertySpecifications = jsonArrayPropertySpecifications.getJSONObject(k);
                                propertySpecifications.setId(jsonObjectPropertySpecifications.getString("Id"));
                                propertySpecifications.setPropertyId(jsonObjectPropertySpecifications.getString("PropertyId"));
                                propertySpecifications.setName(jsonObjectPropertySpecifications.getString("Name"));
                                propertySpecifications.setCategory(jsonObjectPropertySpecifications.getString("Category"));
                                propertySpecifications.setDescription(jsonObjectPropertySpecifications.getString("Description"));


                                propertySpecificationses.add(propertySpecifications);

                            }

                            inventoryModels.setPropertyLoanses(propertyLoanses);
                            inventoryModels.setPropertyLocations(propertyLocations);
                            inventoryModels.setPropertyAreas(propertyAreas);
                            inventoryModels.setPropertyFinancialses(propertyFinancialses);
                            inventoryModels.setPropertyImages(propertyImages);
                            inventoryModels.setPropertyOtherExpences(propertyOtherExpences);
                            inventoryModels.setPropertyPlcs(propertyPlcs);
                            inventoryModels.setPropertyOtherQuestionses(propertyOtherQuestionses);
                            inventoryModels.setPropertyOwnerses(propertyOwnerses);
                            inventoryModels.setPropertyLegalStatuses(propertyLegalStatuses);
                            //   inventoryModels.setPropertyAccommodationDetailses(propertyAccommodationDetails);
                            inventoryModels.setPropertyBedroomses(propertyBedroomses);
                            inventoryModels.setPropertySpecificationses(propertySpecificationses);
                            property_data.add(inventoryModels);


                        }

                        BuiltViewFragment builtViewFragment = new BuiltViewFragment();
                        Bundle builtViewbundle = new Bundle();

//                locationbundle.putString("PropertyId", PropertyId);


                        builtViewbundle.putSerializable("property_data", property_data);
                        builtViewFragment.setArguments(builtViewbundle);
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_detail, builtViewFragment).commit();

                        property_type_txt.setText(property_data.get(0).getNameobject());

                        property_type.setText(Utils.getEmptyValue(property_data.get(0).getTitle()));
                        locality.setText(property_data.get(0).getPropertyLocations().get(0).getUnitName() + " " + Utils.getEmptyValue(property_data.get(0).getPropertyLocations().get(0).getAreaAttributeValue()));
                        try {
                            parking.setText(property_data.get(0).getPropertyAccommodationDetailses().get(0).getTotalCount() + " Parking");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            double price_txt = Double.parseDouble(String.valueOf(property_data.get(0).getPropertyFinancialses().get(0).getDemandPrice()));
                            price.setText(Utils.getConvertedPrice((long) price_txt, this));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            area_unit.setText(property_data.get(0).getPropertyAreas().get(0).getPlotArea() + " " + property_data.get(0).getPropertyAreas().get(0).getPropertySizeUnit4());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            for (int i = 0; i < property_data.get(0).getPropertyBedroomses().size(); i++) {
                                if (property_data.get(0).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("room")) {
                                    room_count = room_count + 1;
                                }
                                if (property_data.get(0).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("kitchen")) {
                                    kitchen_count = kitchen_count + 1;
                                }

                                if (property_data.get(0).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("bathroom")) {
                                    bathroom_count = bathroom_count + 1;
                                }

                                if (property_data.get(0).getPropertyBedroomses().get(i).getCategory().equalsIgnoreCase("parking")) {
                                    parking_count = parking_count + 1;
                                }


                            }


                            bedroom.setText(room_count + " Bedroom");
                            kitchen.setText(kitchen_count + " Kitchen");
                            bathroom.setText(bathroom_count + " Bathroom");
                            parking.setText(parking_count + " Parking");

                            for (int index = 0; index < property_data.get(0).getPropertyAccommodationDetailses().size(); index++) {
                                if (property_data.get(0).getPropertyAccommodationDetailses().get(index).getType().equalsIgnoreCase("Parking")) {
                                    parking.setText(property_data.get(0).getPropertyAccommodationDetailses().get(index).getTotalCount() + " Parking");
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                                .build();

                        ImageLoader imageLoader = ImageLoader.getInstance();
                        if (!imageLoader.isInited()) {
                            imageLoader.init(config);
                        }
                        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                                .cacheOnDisc(true).resetViewBeforeLoading(true)
                                .showImageForEmptyUri(R.drawable.image_logo)
                                .showImageOnFail(R.drawable.image_logo)
                                .showImageOnLoading(R.drawable.image_logo).build();
                        if (!property_data.get(0).getPropertyImages().isEmpty())
                            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + property_data.get(0).getPropertyImages().get(0).getUrl(), project_image, options);


                    }
                    if (property_data.get(0).getPropertyImages().isEmpty()) {
                        gallery_list.setVisibility(View.GONE);
                    } else {
                        gallery_list.setAdapter(new GalleryListAdapterInventory(this, property_data.get(0).getPropertyImages()));
                    }

//

//
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
