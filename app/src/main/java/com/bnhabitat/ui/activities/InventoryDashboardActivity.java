package com.bnhabitat.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.ui.fragments.ForyouFragment;
import com.bnhabitat.ui.fragments.InventoryDashboardFragment;
import com.bnhabitat.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

public class InventoryDashboardActivity extends BaseActivity implements SendMessage {
    LinearLayout lnrOpenNav;
    TabLayout.Tab Tab1, Tab2, Tab3, Tab4;
    ImageView back_btn;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_dashboard);

        set();

        //   String format=   Utils.format(1000000);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        lnrOpenNav = (LinearLayout) findViewById(R.id.lnrOpenNav);
        SpannableString ss = new SpannableString(getResources().getString(R.string.if_you_are));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mobIntent = new Intent(Intent.ACTION_DIAL);

                mobIntent.setData(Uri.parse("tel:" + "18000001111"));
                if (ActivityCompat.checkSelfPermission(InventoryDashboardActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(mobIntent);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 118, 132, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = (TextView) findViewById(R.id.phone_number_dashboard);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
        Tab1 = tabLayout.newTab().setText("For You");
        Tab2 = tabLayout.newTab().setText("Dashboard");
        Tab3 = tabLayout.newTab().setText("Message");
        Tab4 = tabLayout.newTab().setText("Settings");
        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
        tabLayout.addTab(Tab3);
        tabLayout.addTab(Tab4);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        lnrOpenNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer();
                }
        });

        InventoryDashboardFragment inventoryDashboardFragment = new InventoryDashboardFragment();

        //Inflate the fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_inventory_container, inventoryDashboardFragment).commit();

        tabLayout.getTabAt(1);
        Tab2.select();

        bindWidgetsWithAnEvent();

//        sell_lay=(LinearLayout)findViewById(R.id.sell_lay);
//        sell_lay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent=new Intent(InventoryDashboardActivity.this,SelectionPropertyTypeActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                ForyouFragment foryouFragment = new ForyouFragment();

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_inventory_container, foryouFragment).commit();

                lnrOpenNav.setVisibility(View.VISIBLE);

                break;
            case 1:
                InventoryDashboardFragment inventoryDashboardFragment = new InventoryDashboardFragment();

                //Inflate the fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_inventory_container, inventoryDashboardFragment).commit();
                lnrOpenNav.setVisibility(View.GONE);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){

                }


                break;
            case 2:
//                try {
//                    FinancialandBankLoanFragment financialandBankLoanFragment = new FinancialandBankLoanFragment();
//
//                    //Inflate the fragment
//                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, financialandBankLoanFragment).commit();
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
                lnrOpenNav.setVisibility(View.GONE);
                break;
            case 3:
//                try {
//                    OtherExpensesFragment otherExpensesFragment = new OtherExpensesFragment();
//
//                    //Inflate the fragment
//                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, otherExpensesFragment).commit();
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
                lnrOpenNav.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void sendData(JSONObject message, int position) {

    }

    @Override
    public void sendData(ArrayList<ContactsModel> contactsModels, int position) {

    }

    @Override
    public void sendData(int position) {
        if (position == 1) {
            tabLayout.getTabAt(position);
            Tab1.select();
        } else if (position == 2) {
            tabLayout.getTabAt(position);
            Tab2.select();
        }
    }

    @Override
    public void sendData(String id, int position) {

    }

    @Override
    public void EditId(ArrayList<InventoryModel> inventoryModels, String EditId) {

    }



    @Override
    public void onBackPressed() {

        if(ForyouFragment.isVisible(getApplicationContext())){
            ForyouFragment. hideView(getApplicationContext());
        }
        else {
            super.onBackPressed();
        }

    }

}
