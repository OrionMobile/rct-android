package com.bnhabitat.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.bnhabitat.R;
import com.bnhabitat.township.townactivities.ProjectNameSepecification;
import com.bnhabitat.township.townactivities.Project_name;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class SplashActivity extends AppCompatActivity {

    private static final int REQUEST_PHONE_CALL = 198;
    private static final int REQUEST_PHONE_CALL_test = 198;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

/*
        try{


        PackageInfo info = getPackageManager().getPackageInfo(
                "com.facebook.samples.loginhowto",
                PackageManager.GET_SIGNATURES);
        for (Signature signature : info.signatures) {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());
            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
        }
    } catch (
    PackageManager.NameNotFoundException e) {

    } catch (
    NoSuchAlgorithmException e) {

    }
*/

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
        } else {
            Thread th = new Thread(r);
            th.start();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        Thread th = new Thread(r);
                        th.start();
                    }

                }
        }
    }

    Runnable r = new Runnable() {
        @Override
        public void run() {


            try {
                Thread.sleep(2000);
                if (!PreferenceConnector.getInstance(SplashActivity.this).loadSavedPreferences(Constants.USER_ID, "").equalsIgnoreCase("")) {

                    if (!PreferenceConnector.getInstance(SplashActivity.this).loadBooleanPreferences(Constants.IS_INTIALIZED)) {
                        Intent i = new Intent(SplashActivity.this, IntializeActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else {

                       Intent i = new Intent(SplashActivity.this, NewDashboardActivity.class);
                     //  Intent i = new Intent(SplashActivity.this, ProjectNameSepecification.class);

                        startActivity(i);
                        finish();
                    }


                }

                else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    };

}
