package com.bnhabitat.ui.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.contacts.AddressTable;
import com.bnhabitat.data.contacts.ContatctTable;
import com.bnhabitat.data.contacts.EmailTable;
import com.bnhabitat.data.contacts.LocalAddressTable;
import com.bnhabitat.data.contacts.LocalContatctTable;
import com.bnhabitat.data.contacts.LocalEmailTable;
import com.bnhabitat.data.contacts.LocalPhoneTable;
import com.bnhabitat.data.contacts.PhoneTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.EmailModel;
import com.bnhabitat.models.PhoneModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.bnhabitat.ui.adapters.ContactsAdapter.contactdetail;

public class AddContatctActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener {
    int mYear, mMonth, mDay, count_address = 0, count_email = 0, count_phone = 0;
    LinearLayout layout_email2, phone_lay2, email_view, view_phone, lay_address_view, address_view;
    ImageView back, email_add, phone_add2, phone_add, email_add2, address_add;
    EditText name, last_name, company_name, address_1, address_2, address_3, pan, adhar, website, profession, location, phone_1, phone_2,
            email_1, email_2, address_line2, address_line3, address_line1, zip_1, zip_2, country_1, country_2, state_1, state_2, developer, developer1, tower, tower1, floor, floor1, unitno_txt, unitno_txt1;
    TextView dob, tag, phone_code, city_1, city_2, title;
    ArrayList<PhoneModel> updatePhones = new ArrayList<>();
    ArrayList<EmailModel> emailModels = new ArrayList<>();
    ArrayList<String> getPhones = new ArrayList<>();
    ArrayList<String> getEmails = new ArrayList<>();
    ArrayList<AddressModel> addresses = new ArrayList<>();
    Button save;

    private String country_full = "";
    private String country = "", update = "";
    private double longitude;
    private double latitude;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private static final int REQUEST_CODE_AUTOCOMPLETE2 = 2;
    String contact_detail = "";
    ArrayList<ContactsModel.Phones> phones;
    private String phone_id = "", phone_id1 = "";
    private String email_id = "", email_id1 = "";
    int position;
    ContactsModel contactsFirebaseModels;
    //    ArrayList<ContactsModel> contactsFirebaseModels=new ArrayList<>();
    ArrayList<ContactsModel.Phones> getPhone_number = new ArrayList<>();
    ArrayList<ContactsModel.Email> emails = new ArrayList<>();
    ArrayList<AddressModel> addressesArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contatct);
        save = (Button) findViewById(R.id.save);
        name = (EditText) findViewById(R.id.name);
        last_name = (EditText) findViewById(R.id.last_name);
        company_name = (EditText) findViewById(R.id.company_name);
        address_1 = (EditText) findViewById(R.id.address_1);
        address_2 = (EditText) findViewById(R.id.address_2);
        address_3 = (EditText) findViewById(R.id.address_3);
        pan = (EditText) findViewById(R.id.pan);
        adhar = (EditText) findViewById(R.id.adhar);
        website = (EditText) findViewById(R.id.website);
        profession = (EditText) findViewById(R.id.profession);
        location = (EditText) findViewById(R.id.location_1);
        phone_1 = (EditText) findViewById(R.id.phone_1);
        phone_2 = (EditText) findViewById(R.id.phone_2);
        email_1 = (EditText) findViewById(R.id.email_1);
        email_2 = (EditText) findViewById(R.id.email_2);
        address_line2 = (EditText) findViewById(R.id.address_line2);
        address_line1 = (EditText) findViewById(R.id.address_line1);
        address_line3 = (EditText) findViewById(R.id.addressline_3);
        developer = (EditText) findViewById(R.id.dev_name);
        tower = (EditText) findViewById(R.id.tower_txt);
        floor = (EditText) findViewById(R.id.floor);
        unitno_txt = (EditText) findViewById(R.id.unit_no_txt);
        developer1 = (EditText) findViewById(R.id.dev_name1);
        tower1 = (EditText) findViewById(R.id.tower_txt1);
        floor1 = (EditText) findViewById(R.id.floor1);
        unitno_txt1 = (EditText) findViewById(R.id.unit_no_txt1);
        city_1 = (TextView) findViewById(R.id.city_1);
        city_2 = (TextView) findViewById(R.id.city_2);
        title = (TextView) findViewById(R.id.title);
        zip_1 = (EditText) findViewById(R.id.zip_1);
        zip_2 = (EditText) findViewById(R.id.zip_2);
        country_1 = (EditText) findViewById(R.id.country_1);
        country_2 = (EditText) findViewById(R.id.country_2);
        state_1 = (EditText) findViewById(R.id.state_1);
        state_2 = (EditText) findViewById(R.id.state_2);
        dob = (TextView) findViewById(R.id.dob);
        back = (ImageView) findViewById(R.id.back);
        address_add = (ImageView) findViewById(R.id.address_add);
        email_add = (ImageView) findViewById(R.id.email_add);
        phone_add2 = (ImageView) findViewById(R.id.phone_add2);
        phone_add = (ImageView) findViewById(R.id.phone_add);
        email_add2 = (ImageView) findViewById(R.id.email_add2);
        phone_lay2 = (LinearLayout) findViewById(R.id.phone_lay2);
        layout_email2 = (LinearLayout) findViewById(R.id.layout_email2);
        email_view = (LinearLayout) findViewById(R.id.email_view);
        view_phone = (LinearLayout) findViewById(R.id.view_phone);
        address_view = (LinearLayout) findViewById(R.id.address_view);
        lay_address_view = (LinearLayout) findViewById(R.id.lay_address_view);
        try {

            update = getIntent().getStringExtra("update") == null ? "" : getIntent().getStringExtra("update");
            position = getIntent().getIntExtra("position", 0);
            contactsFirebaseModels = (ContactsModel) getIntent().getSerializableExtra(contactdetail);
            getPhone_number = PhoneTable.getInstance().getPhones(contactsFirebaseModels.getId());
//            getPhone_number = PhoneTable.getInstance().getPhones(contactsFirebaseModels.getId());
            emails = EmailTable.getInstance().getEmails(contactsFirebaseModels.getId());
            addressesArrayList = AddressTable.getInstance().addressesArrayList(contactsFirebaseModels.getId());
//            Log.e("",""+getPhone_number.get(position))
            if (update.equalsIgnoreCase("")) {

            } else {
                title.setText("Update Contact");
                name.setText(contactsFirebaseModels.getFirstName());
                last_name.setText(contactsFirebaseModels.getLastName());
                dob.setText(contactsFirebaseModels.getDob());
                company_name.setText(contactsFirebaseModels.getCompanyName());

                if (getPhone_number.size() != 0) {
                    if (getPhone_number.size() == 1) {

                        phone_1.setText(getPhone_number.get(0).getPhoneNumber());
                    } else {
                        phone_1.setText(getPhone_number.get(0).getPhoneNumber());
                        phone_2.setText(getPhone_number.get(1).getPhoneNumber());
                    }
//                        if (phones.get(0).getPhoneNumber() != null)
//                            phone_1.setText(phones.get(0).getPhoneNumber());
//                        if (phones.get(1).getPhoneNumber() != null)
//                            phone_2.setText(phones.get(1).getPhoneNumber());
                }
                if (emails.size() != 0) {
                    if (emails.size() == 1) {

                        email_1.setText(emails.get(0).getAddress());
                    } else {
                        email_1.setText(emails.get(0).getAddress());
                        email_2.setText(emails.get(1).getAddress());
                    }
//
                }
                if (addressesArrayList.size() != 0) {
                    if (addressesArrayList.size() == 1) {

                        address_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getAddress1()));
                        address_2.setText(Utils.getFilteredValue(addressesArrayList.get(0).getAddress2()));
                        address_3.setText(Utils.getFilteredValue(addressesArrayList.get(0).getAddress3()));
                        developer.setText(Utils.getFilteredValue(addressesArrayList.get(0).getDeveloper_name()));
                        tower.setText(Utils.getFilteredValue(addressesArrayList.get(0).getTower()));
                        floor.setText(Utils.getFilteredValue(addressesArrayList.get(0).getFloor()));
                        unitno_txt.setText(Utils.getFilteredValue(addressesArrayList.get(0).getUnitName()));
                        city_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getCity_name()));
                        zip_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getZipcode()));
                        state_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getStateName()));
                        country_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getCountryName()));
                    } else {
                        address_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getAddress1()));
                        address_2.setText(Utils.getFilteredValue(addressesArrayList.get(0).getAddress2()));
                        address_3.setText(Utils.getFilteredValue(addressesArrayList.get(0).getAddress3()));
                        developer.setText(Utils.getFilteredValue(addressesArrayList.get(0).getDeveloper_name()));
                        tower.setText(Utils.getFilteredValue(addressesArrayList.get(0).getTower()));
                        floor.setText(Utils.getFilteredValue(addressesArrayList.get(0).getFloor()));
                        unitno_txt.setText(Utils.getFilteredValue(addressesArrayList.get(0).getUnitName()));
                        city_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getCity_name()));
                        zip_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getZipcode()));
                        state_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getStateName()));
                        country_1.setText(Utils.getFilteredValue(addressesArrayList.get(0).getCountryName()));
                        address_line1.setText(Utils.getFilteredValue(addressesArrayList.get(1).getAddress1()));
                        address_line2.setText(Utils.getFilteredValue(addressesArrayList.get(1).getAddress2()));
                        address_line3.setText(Utils.getFilteredValue(addressesArrayList.get(1).getAddress3()));
                        developer1.setText(Utils.getFilteredValue(addressesArrayList.get(1).getDeveloper_name()));
                        tower1.setText(Utils.getFilteredValue(addressesArrayList.get(1).getTower()));
                        floor1.setText(Utils.getFilteredValue(addressesArrayList.get(1).getFloor()));
                        unitno_txt1.setText(Utils.getFilteredValue(addressesArrayList.get(1).getUnitName()));
                        city_2.setText(Utils.getFilteredValue(addressesArrayList.get(1).getCity_name()));
                        zip_2.setText(Utils.getFilteredValue(addressesArrayList.get(1).getZipcode()));
                        state_2.setText(Utils.getFilteredValue(addressesArrayList.get(1).getStateName()));
                        country_2.setText(Utils.getFilteredValue(addressesArrayList.get(1).getCountryName()));
                    }
//                        if (phones.get(0).getPhoneNumber() != null)
//                            phone_1.setText(phones.get(0).getPhoneNumber());
//                        if (phones.get(1).getPhoneNumber() != null)
//                            phone_2.setText(phones.get(1).getPhoneNumber());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (update.equalsIgnoreCase("")) {
            save.setText("Add Contact");
        } else {
            save.setText("Update Contact");
//            onViewContacts();
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        phone_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count_phone == 0) {
                    phone_add.setImageDrawable(getResources().getDrawable(R.drawable.minus_contatct));
                    phone_lay2.setVisibility(View.VISIBLE);
                    view_phone.setVisibility(View.VISIBLE);
                    count_phone++;
                } else {
                    phone_add.setImageDrawable(getResources().getDrawable(R.drawable.plus_cont));
                    phone_lay2.setVisibility(View.GONE);
                    view_phone.setVisibility(View.GONE);
                    count_phone = 0;
                }
            }
        });
        email_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count_email == 0) {
                    email_add.setImageDrawable(getResources().getDrawable(R.drawable.minus_contatct));
                    layout_email2.setVisibility(View.VISIBLE);
                    email_view.setVisibility(View.VISIBLE);
                    count_email++;
                } else {
                    email_add.setImageDrawable(getResources().getDrawable(R.drawable.plus_cont));
                    layout_email2.setVisibility(View.GONE);
                    email_view.setVisibility(View.GONE);
                    count_email = 0;
                }
            }
        });
        address_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count_address == 0) {
                    address_add.setImageDrawable(getResources().getDrawable(R.drawable.minus_contatct));
                    lay_address_view.setVisibility(View.VISIBLE);
                    address_view.setVisibility(View.VISIBLE);
                    count_address++;
                } else {
                    address_add.setImageDrawable(getResources().getDrawable(R.drawable.plus_cont));
                    lay_address_view.setVisibility(View.GONE);
                    address_view.setVisibility(View.GONE);
                    count_address = 0;
                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (update.equalsIgnoreCase("")) {
                    if (!name.getText().toString().equalsIgnoreCase("") && (!phone_1.getText().toString().equalsIgnoreCase(""))) {
                        if (Utils.getInstance().isOnline(AddContatctActivity.this))
                            onPostContacts();
                        else {

                            long time = System.currentTimeMillis();
                            LocalContatctTable.getInstance().write(time, name.getText().toString(), last_name.getText().toString(), company_name.getText().toString(), phone_1.getText().toString(), 0, "", "");

                            if (!phone_1.getText().toString().equalsIgnoreCase(""))
                                LocalPhoneTable.getInstance().write(time, 0, phone_1.getText().toString(), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""));

                            if (!phone_2.getText().toString().equalsIgnoreCase(""))
                                LocalPhoneTable.getInstance().write(time, 0, phone_2.getText().toString(), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""));
                            if (!email_1.getText().toString().equalsIgnoreCase(""))
                                LocalEmailTable.getInstance().write(time, 0, email_1.getText().toString(), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""));

                            if (!email_2.getText().toString().equalsIgnoreCase(""))
                                LocalEmailTable.getInstance().write(time, 0, email_2.getText().toString(), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""));
                            if (!address_1.getText().toString().equalsIgnoreCase(""))
                                LocalAddressTable.getInstance().write(time, 0, address_1.getText().toString(), address_2.getText().toString(), address_3.getText().toString(), developer.getText().toString(), tower.getText().toString(), floor.getText().toString(), unitno_txt.getText().toString(), city_1.getText().toString(), zip_1.getText().toString(), country_1.getText().toString(), state_1.getText().toString(), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""));

                            if (!address_line1.getText().toString().equalsIgnoreCase(""))
                                LocalAddressTable.getInstance().write(time, 0, address_line1.getText().toString(), address_line2.getText().toString(), address_line3.getText().toString(), developer1.getText().toString(), tower1.getText().toString(), floor1.getText().toString(), unitno_txt1.getText().toString(), city_2.getText().toString(), zip_2.getText().toString(), country_2.getText().toString(), state_2.getText().toString(), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(AddContatctActivity.this).loadSavedPreferences(Constants.DATE_TIME, ""));
                            finish();
                        }
                    } else {
                        Toast.makeText(AddContatctActivity.this, "Please add First name and Phone number", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (!name.getText().toString().equalsIgnoreCase("") && (!phone_1.getText().toString().equalsIgnoreCase(""))) {
                        if (Utils.getInstance().isOnline(AddContatctActivity.this))
                            onUpdateContacts();
                        else {
                            ContatctTable.getInstance().updateCategory(Integer.parseInt(contactsFirebaseModels.getId()), name.getText().toString(), last_name.getText().toString(), company_name.getText().toString(), phone_1.getText().toString(), email_1.getText().toString(), 0, contactsFirebaseModels.getGroup_id(), contactsFirebaseModels.getGroup_name(), contactsFirebaseModels.getDob(),contactsFirebaseModels.getImageUrl(), "");
                            ArrayList<ContactsModel.Phones> phones = PhoneTable.getInstance().getPhones(contactsFirebaseModels.getId());


                            if (phones.size() > 0)
                                PhoneTable.getInstance().updateCategory(
                                        Integer.parseInt(phones.get(0).getId()),
                                        phone_1.getText().toString(), "");
                            else
                                PhoneTable.getInstance().write(Integer.parseInt(contactsFirebaseModels.getId()),
                                        0,
                                        phone_1.getText().toString(), "", "");

                            if (phones.size() > 1)
                                PhoneTable.getInstance().updateCategory(
                                        Integer.parseInt(phones.get(1).getId()),
                                        phone_2.getText().toString(), "");
                            else {
                                if (!phone_2.getText().toString().equalsIgnoreCase(""))
                                    PhoneTable.getInstance().write(Integer.parseInt(contactsFirebaseModels.getId()),
                                            0,
                                            phone_2.getText().toString(), "", "");
                            }


                            ArrayList<ContactsModel.Email> emails = EmailTable.getInstance().getEmails(contactsFirebaseModels.getId());

                            if (emails.size() > 0)
                                EmailTable.getInstance().updateEmail(Integer.parseInt(emails.get(0).getId()), email_1.getText().toString(), "");

                            else
                                EmailTable.getInstance().write(Integer.parseInt(contactsFirebaseModels.getId()), 0, email_1.getText().toString(), "", "");


                            if (emails.size() > 1)
                                EmailTable.getInstance().updateEmail(Integer.parseInt(emails.get(1).getId()), email_2.getText().toString(), "");

                            else {
                                if (!email_2.getText().toString().equalsIgnoreCase(""))
                                    EmailTable.getInstance().write(Integer.parseInt(contactsFirebaseModels.getId()), 0, email_2.getText().toString(), "", "");

                            }
                            ArrayList<AddressModel> addresses = AddressTable.getInstance().addressesArrayList(contactsFirebaseModels.getId());

                            if (addresses.size() > 0)
                                AddressTable.getInstance().updateAddress(Integer.parseInt(addresses.get(0).getId()), address_1.getText().toString(), address_2.getText().toString(), address_3.getText().toString(), developer.getText().toString(), tower.getText().toString(), floor.getText().toString(), unitno_txt.getText().toString(), city_1.getText().toString(), zip_1.getText().toString(), country_1.getText().toString(), state_1.getText().toString(), "");

                            else
                                AddressTable.getInstance().write(Integer.parseInt(contactsFirebaseModels.getId()), 0, address_1.getText().toString(), address_2.getText().toString(), address_3.getText().toString(), developer.getText().toString(), tower.getText().toString(), floor.getText().toString(), unitno_txt.getText().toString(), city_1.getText().toString(), zip_1.getText().toString(), country_1.getText().toString(), state_1.getText().toString(), "", "");


                            if (addresses.size() > 1)
                                AddressTable.getInstance().updateAddress(Integer.parseInt(addresses.get(1).getId()), address_line1.getText().toString(), address_line2.getText().toString(), address_line3.getText().toString(), developer1.getText().toString(), tower1.getText().toString(), floor1.getText().toString(), unitno_txt1.getText().toString(), city_2.getText().toString(), zip_2.getText().toString(), country_2.getText().toString(), state_2.getText().toString(), "");

                            else {
                                if (!address_line1.getText().toString().equalsIgnoreCase(""))
                                    AddressTable.getInstance().write(Integer.parseInt(contactsFirebaseModels.getId()), 0, address_line1.getText().toString(), address_line2.getText().toString(), address_line3.getText().toString(), developer1.getText().toString(), tower1.getText().toString(), floor1.getText().toString(), unitno_txt1.getText().toString(), city_2.getText().toString(), zip_2.getText().toString(), country_2.getText().toString(), state_2.getText().toString(), "", "");

                            }


                            finish();

////                            if (phones.size()==0){
////                                PhoneTable.getInstance().write(Integer.parseInt(contactsFirebaseModels.getId()),
////                                        0,
////                                        phone_1.getText().toString(),"","");
////                            }
////                           if (phones.size()==1){
////                               PhoneTable.getInstance().write(Integer.parseInt(contactsFirebaseModels.getId()),
////                                       0,
////                                       phone_2.getText().toString(),"","");
////                           }
//
//
                        }
//
                    } else {
                        Toast.makeText(AddContatctActivity.this, "Please add First name and Phone number", Toast.LENGTH_SHORT).show();
                    }
//
                }

            }
        });
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(AddContatctActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        /*      Your code   to get date and time    */
                        dob.setText(selectedyear + "-" + (selectedmonth + 1) + "-" + selectedday);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }

        });
        city_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    // The autocomplete activity requires Google Play Services to be available. The intent
                    // builder checks this and throws an exception if it is not the case.
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(typeFilter)
                            .build(AddContatctActivity.this);
                    startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
                } catch (GooglePlayServicesRepairableException e) {
                    // Indicates that Google Play Services is either not installed or not up to date. Prompt
                    // the user to correct the issue.
                    GoogleApiAvailability.getInstance().getErrorDialog(AddContatctActivity.this, e.getConnectionStatusCode(),
                            0 /* requestCode */).show();
                } catch (GooglePlayServicesNotAvailableException e) {
                    // Indicates that Google Play Services is not available and the problem is not easily
                    // resolvable.
                    String message = "Google Play Services is not available: " +
                            GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

                    Log.e("", message);
                    Toast.makeText(AddContatctActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
        city_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    // The autocomplete activity requires Google Play Services to be available. The intent
                    // builder checks this and throws an exception if it is not the case.
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(typeFilter)
                            .build(AddContatctActivity.this);
                    startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE2);
                } catch (GooglePlayServicesRepairableException e) {
                    // Indicates that Google Play Services is either not installed or not up to date. Prompt
                    // the user to correct the issue.
                    GoogleApiAvailability.getInstance().getErrorDialog(AddContatctActivity.this, e.getConnectionStatusCode(),
                            0 /* requestCode */).show();
                } catch (GooglePlayServicesNotAvailableException e) {
                    // Indicates that Google Play Services is not available and the problem is not easily
                    // resolvable.
                    String message = "Google Play Services is not available: " +
                            GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

                    Log.e("", message);
                    Toast.makeText(AddContatctActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });

        email_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    email_id = emails.get(0).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        email_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    email_id1 = emails.get(1).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        phone_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    phone_id = getPhone_number.get(0).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        phone_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    phone_id1 = getPhone_number.get(1).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                LatLng myloc = place.getLatLng();

                latitude = myloc.latitude;
                longitude = myloc.longitude;
//
//                mMap.animateCamera( CameraUpdateFactory.newLatLngZoom(
//                        new LatLng(latitude, longitude ), 14));
//                current_loc.setVisibility(View.VISIBLE);

                // Format the place's details and display them in the TextView.
//                address_1.setText(formatPlaceDetails(getResources(),
//                        place.getAddress()
//
//                ));
                getCompleteAddressString(latitude, longitude, this);
            }
        } else if (requestCode == REQUEST_CODE_AUTOCOMPLETE2) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                LatLng myloc = place.getLatLng();

                latitude = myloc.latitude;
                longitude = myloc.longitude;
//
//                mMap.animateCamera( CameraUpdateFactory.newLatLngZoom(
//                        new LatLng(latitude, longitude ), 14));
//                current_loc.setVisibility(View.VISIBLE);

                // Format the place's details and display them in the TextView.
//                address_1.setText(formatPlaceDetails(getResources(),
//                        place.getAddress()
//
//                ));
                getCompleteAddressString1(latitude, longitude, this);
            }
        }
    }

    private static Spanned formatPlaceDetails(Resources res,
                                              CharSequence address) {
        Log.e("addresssssss", res.getString(R.string.place_details, address));
        return Html.fromHtml(res.getString(R.string.place_details, address
        ));

    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE, Context context) {
        String string = "";


        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {

            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            String adressName = addresses.get(0).getAddressLine(0);
            String stateName = addresses.get(0).getAddressLine(1);
            String cityName = addresses.get(0).getAddressLine(2);
            String countryName = addresses.get(0).getAddressLine(3);
            String cityname1[] = cityName.split(",");


            city_1.setText(cityname1[0]);
            String zipname[] = cityname1[1].split(" ");

            zip_1.setText(zipname[2]);
            state_1.setText(zipname[1]);
            country_1.setText(countryName);
//
        } catch (Exception e) {
            e.printStackTrace();

            Log.w("My s", "Canont get Address!");
        }
        return string;
    }

    private String getCompleteAddressString1(double LATITUDE, double LONGITUDE, Context context) {
        String string = "";


        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {

            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            String adressName = addresses.get(0).getAddressLine(0);
            String stateName = addresses.get(0).getAddressLine(1);
            String cityName = addresses.get(0).getAddressLine(2);
            String countryName = addresses.get(0).getAddressLine(3);
            String cityname1[] = cityName.split(",");


            city_2.setText(cityname1[0]);
            String zipname[] = cityname1[1].split(" ");

            zip_2.setText(zipname[2]);
            state_2.setText(zipname[1]);
            country_2.setText(countryName);
//
//
//
//
//
//
        } catch (Exception e) {
            e.printStackTrace();

            Log.w("My s", "Canont get Address!");
        }
        return string;
    }

    private void onPostContacts() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
                getLoginInputJson(),
                "Loading...",
                this,
                Urls.URL_CREATE_CONTACTS,
                Constants.POST).execute();
    }

    private void onUpdateContacts() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
                getUpdateInputJson(),
                "Loading...",
                this,
                Urls.URL_CREATE_CONTACTS,
                Constants.POST).execute();
    }

//    private void onViewContacts() {
//
//        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_VIEW_CONTACTS + contactsFirebaseModels.get(position).getId(),
//                "",
//                "Loading...",
//                this,
//                Urls.URL_VIEW_CONTACTS,
//                Constants.GET).execute();
//    }

    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().
            setTypeFilter(Place.TYPE_COUNTRY).build();

    private String getLoginInputJson() {

        String inputJson = "";
//


//
        try {


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("FirstName", name.getText().toString());
            jsonObject.put("LastName", last_name.getText().toString());
            jsonObject.put("CompanyId", "1");

            jsonObject.put("DateOfBirth", dob.getText().toString());
            jsonObject.put("Gender", "male");
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            if (!phone_1.getText().toString().equalsIgnoreCase("")) {

                getPhones.add(phone_1.getText().toString());
            }
            if (!phone_2.getText().toString().equalsIgnoreCase("")) {
                getPhones.add(phone_2.getText().toString());
            }
            if (!email_1.getText().toString().equalsIgnoreCase("")) {
                getEmails.add(email_1.getText().toString());
            }
            if (!email_2.getText().toString().equalsIgnoreCase("")) {
                getEmails.add(email_2.getText().toString());
            }
            if (!address_1.getText().toString().equalsIgnoreCase("") || !city_1.getText().toString().equalsIgnoreCase("")) {
                AddressModel addressModel = new AddressModel();
                addressModel.setAddress1(address_1.getText().toString());
                addressModel.setAddress2(address_2.getText().toString());
                addressModel.setAddress3(address_3.getText().toString());
                addressModel.setZipcode(zip_1.getText().toString());
                addressModel.setDeveloper(developer.getText().toString());
                addressModel.setTower(tower.getText().toString());
                addressModel.setFloor(floor.getText().toString());
                addressModel.setUnit_no(unitno_txt.getText().toString());
                addressModel.setCity_name(city_1.getText().toString());
                addressModel.setCountryName(country_1.getText().toString());
                addressModel.setStateName(state_1.getText().toString());
                addresses.add(addressModel);
            }


            if (!address_line1.getText().toString().equalsIgnoreCase("") || !city_2.getText().toString().equalsIgnoreCase("")) {
                AddressModel addressModel1 = new AddressModel();
                addressModel1.setAddress1(address_line1.getText().toString());
                addressModel1.setAddress2(address_line2.getText().toString());
                addressModel1.setZipcode(zip_2.getText().toString());
                addressModel1.setCity_name(city_2.getText().toString());
                addressModel1.setCountryName(country_2.getText().toString());
                addressModel1.setStateName(state_2.getText().toString());
                addressModel1.setAddress3(address_line3.getText().toString());

                addressModel1.setDeveloper(developer1.getText().toString());
                addressModel1.setTower(tower1.getText().toString());
                addressModel1.setFloor(floor1.getText().toString());
                addressModel1.setUnit_no(unitno_txt1.getText().toString());

                addresses.add(addressModel1);
            }

            JSONArray addressArray = new JSONArray();
//                 getPhones=contactsFirebaseModels.get(index).getPhones();
//
            for (int i = 0; i < addresses.size(); i++) {

                JSONObject jsonObject1 = new JSONObject();

                jsonObject1.put("StateName", addresses.get(i).getStateName());

//                    jsonObject1.put("PhoneType", 1);
                jsonObject1.put("CountryName", addresses.get(i).getCountryName());
                jsonObject1.put("CityName", addresses.get(i).getCity_name());
                jsonObject1.put("Address1", addresses.get(i).getAddress1());
                jsonObject1.put("Address2", addresses.get(i).getAddress2());
                jsonObject1.put("Address3", addresses.get(i).getAddress3());

                jsonObject1.put("DeveloperName", addresses.get(i).getDeveloper());
                jsonObject1.put("Tower", addresses.get(i).getTower());
                jsonObject1.put("Floor", addresses.get(i).getFloor());
                jsonObject1.put("UnitName", addresses.get(i).getUnit_no());
                jsonObject1.put("ZipCode", addresses.get(i).getZipcode());

                addressArray.put(jsonObject1);

            }

//                 getPhones=contactsFirebaseModels.get(index).getPhones();
//
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < getPhones.size(); i++) {

                JSONObject jsonObject2 = new JSONObject();

                jsonObject2.put("PhoneNumber", getPhones.get(i));

//                    jsonObject1.put("PhoneType", 1);
                jsonObject2.put("Mode", "");

                jsonArray.put(jsonObject2);

            }
            JSONArray emailArray = new JSONArray();
            for (int i = 0; i < getEmails.size(); i++) {

                JSONObject jsonObject3 = new JSONObject();

                jsonObject3.put("Address", getEmails.get(i));

//                    jsonObject1.put("PhoneType", 1);
                jsonObject3.put("Label", "Email Label");

                emailArray.put(jsonObject3);

            }
            JSONArray grouparray = new JSONArray();
            JSONObject jsonObject4 = new JSONObject();
            jsonObject4.put("Id", "");
            grouparray.put(jsonObject4);
            jsonObject.put("Phones", jsonArray);
            jsonObject.put("Addresses", addressArray);
            jsonObject.put("EmailAddresses", emailArray);
//            jsonObject.put("Groups", grouparray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

    private String getUpdateInputJson() {

        String inputJson = "";
//


//
        try {


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Id", contactsFirebaseModels.getId());
            jsonObject.put("FirstName", name.getText().toString());
            jsonObject.put("LastName", last_name.getText().toString());
            jsonObject.put("CompanyId", "1");

            jsonObject.put("DateOfBirth", dob.getText().toString());
            jsonObject.put("Gender", "male");
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            ArrayList<ContactsModel.Phones> phones = PhoneTable.getInstance().getPhones(contactsFirebaseModels.getId());

            if (phones.size() > 0) {
                PhoneModel phoneModel = new PhoneModel();
                phoneModel.setPhone(phone_1.getText().toString());
                phoneModel.setId(phones.get(0).getId());
                updatePhones.add(phoneModel);
            } else {
                if (!phone_1.getText().toString().equalsIgnoreCase("")) {
                    PhoneModel phoneModel = new PhoneModel();
                    phoneModel.setPhone(phone_1.getText().toString());
                    phoneModel.setId("0");
                    updatePhones.add(phoneModel);
                }
            }

            if (phones.size() > 1) {
                PhoneModel phoneModel1 = new PhoneModel();
                phoneModel1.setPhone(phone_2.getText().toString());
                phoneModel1.setId(phones.get(1).getId());
                updatePhones.add(phoneModel1);
            } else {
                if (!phone_2.getText().toString().equalsIgnoreCase("")) {
                    PhoneModel phoneModel1 = new PhoneModel();
                    phoneModel1.setPhone(phone_2.getText().toString());
                    phoneModel1.setId("0");
                    updatePhones.add(phoneModel1);
                }
            }

            ArrayList<ContactsModel.Email> emails = EmailTable.getInstance().getEmails(contactsFirebaseModels.getId());

            if (emails.size() > 0) {
                EmailModel emailModel = new EmailModel();
                emailModel.setAddress(email_1.getText().toString());
                emailModel.setId(emails.get(0).getId());
                emailModels.add(emailModel);
            } else {
                if (!email_1.getText().toString().equalsIgnoreCase("")) {
                    EmailModel emailModel = new EmailModel();
                    emailModel.setAddress(email_1.getText().toString());
                    emailModel.setId("0");
                    emailModels.add(emailModel);
                }
            }

            if (emails.size() > 1) {
                EmailModel emailModel1 = new EmailModel();
                emailModel1.setAddress(email_2.getText().toString());
                emailModel1.setId(emails.get(1).getId());
                emailModels.add(emailModel1);
            } else {
                if (!email_2.getText().toString().equalsIgnoreCase("")) {
                    EmailModel emailModel1 = new EmailModel();
                    emailModel1.setAddress(email_2.getText().toString());
                    emailModel1.setId("0");
                    emailModels.add(emailModel1);
                }
            }
            ArrayList<AddressModel> addresses1 = AddressTable.getInstance().addressesArrayList(contactsFirebaseModels.getId());

            if (addresses1.size() > 0) {
                AddressModel addressModel = new AddressModel();
                addressModel.setId(addresses1.get(0).getId());
                addressModel.setAddress1(address_1.getText().toString());
                addressModel.setAddress2(address_2.getText().toString());
                addressModel.setAddress3(address_3.getText().toString());
                addressModel.setDeveloper(developer.getText().toString());
                addressModel.setTower(tower.getText().toString());
                addressModel.setFloor(floor.getText().toString());
                addressModel.setUnit_no(unitno_txt.getText().toString());

                addressModel.setCity_name(city_1.getText().toString());
                addressModel.setZipcode(zip_1.getText().toString());
                addressModel.setCountryName(country_1.getText().toString());
                addressModel.setStateName(state_1.getText().toString());
                addresses.add(addressModel);
            } else {
                if (!address_1.getText().toString().equalsIgnoreCase("") || !city_1.getText().toString().equalsIgnoreCase("")) {
                    AddressModel addressModel = new AddressModel();
                    addressModel.setId("0");
                    addressModel.setAddress1(address_1.getText().toString());
                    addressModel.setAddress2(address_2.getText().toString());
                    addressModel.setAddress3(address_3.getText().toString());

                    addressModel.setDeveloper(developer.getText().toString());
                    addressModel.setTower(tower.getText().toString());
                    addressModel.setFloor(floor.getText().toString());
                    addressModel.setUnit_no(unitno_txt.getText().toString());
                    addressModel.setCity_name(city_1.getText().toString());
                    addressModel.setZipcode(zip_1.getText().toString());
                    addressModel.setCountryName(country_1.getText().toString());
                    addressModel.setStateName(state_1.getText().toString());
                    addresses.add(addressModel);
                }
            }

            if (addresses1.size() > 1) {
                AddressModel addressModel1 = new AddressModel();
                addressModel1.setId(addresses1.get(1).getId());
                addressModel1.setAddress1(address_line1.getText().toString());
                addressModel1.setAddress2(address_line2.getText().toString());
                addressModel1.setAddress3(address_line3.getText().toString());

                addressModel1.setDeveloper(developer1.getText().toString());
                addressModel1.setTower(tower1.getText().toString());
                addressModel1.setFloor(floor1.getText().toString());
                addressModel1.setUnit_no(unitno_txt1.getText().toString());
                addressModel1.setCity_name(city_2.getText().toString());
                addressModel1.setZipcode(zip_2.getText().toString());
                addressModel1.setCountryName(country_2.getText().toString());
                addressModel1.setStateName(state_2.getText().toString());
                addresses.add(addressModel1);
            } else {
                if (!address_line1.getText().toString().equalsIgnoreCase("") || !city_2.getText().toString().equalsIgnoreCase("")) {

                    AddressModel addressModel1 = new AddressModel();
                    addressModel1.setId("0");
                    addressModel1.setAddress1(address_line1.getText().toString());
                    addressModel1.setAddress2(address_line2.getText().toString());
                    addressModel1.setAddress3(address_line3.getText().toString());
                    addressModel1.setDeveloper(developer1.getText().toString());
                    addressModel1.setTower(tower1.getText().toString());
                    addressModel1.setFloor(floor1.getText().toString());
                    addressModel1.setUnit_no(unitno_txt1.getText().toString());
                    addressModel1.setCity_name(city_2.getText().toString());
                    addressModel1.setZipcode(zip_2.getText().toString());
                    addressModel1.setCountryName(country_2.getText().toString());
                    addressModel1.setStateName(state_2.getText().toString());
                    addresses.add(addressModel1);
                }
            }

            JSONArray emailArray = new JSONArray();
            for (int i = 0; i < emailModels.size(); i++) {

                JSONObject jsonObject3 = new JSONObject();
                if (emailModels.get(i).getId().equalsIgnoreCase("0")) {
                    jsonObject3.put("Id", "0");
                    jsonObject3.put("IsModify", "false");
                } else {
                    jsonObject3.put("Id", emailModels.get(i).getId());
                    jsonObject3.put("IsModify", "true");
                }
                jsonObject3.put("Address", emailModels.get(i).getAddress());

//                    jsonObject1.put("PhoneType", 1);
                jsonObject3.put("Label", "Email Label");

                emailArray.put(jsonObject3);

            }
//            if (!phone_2.getText().toString().equalsIgnoreCase("")) {
//                PhoneModel phoneModel1 = new PhoneModel();
//                phoneModel1.setPhone(phone_2.getText().toString());
//                phoneModel1.setId(phone_id1);
//                updatePhones.add(phoneModel1);
//
//            }else{
//                PhoneModel phoneModel1 = new PhoneModel();
//                phoneModel1.setPhone(phone_2.getText().toString());
//                phoneModel1.setId("0");
//                updatePhones.add(phoneModel1);
//            }

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < updatePhones.size(); i++) {

                JSONObject jsonObject2 = new JSONObject();
                if (updatePhones.get(i).getId().equalsIgnoreCase("0")) {
                    jsonObject2.put("Id", "0");
                    jsonObject2.put("IsModify", "false");
                } else {
                    jsonObject2.put("Id", updatePhones.get(i).getId());
                    jsonObject2.put("IsModify", "true");
                }


                jsonObject2.put("PhoneNumber", updatePhones.get(i).getPhone());

//                    jsonObject1.put("PhoneType", 1);
                jsonObject2.put("Mode", "");

                jsonArray.put(jsonObject2);

            }


            JSONArray addressArray = new JSONArray();
//                 getPhones=contactsFirebaseModels.get(index).getPhones();
//
            for (int i = 0; i < addresses.size(); i++) {

                JSONObject jsonObject1 = new JSONObject();
                if (addresses.get(i).getId().equalsIgnoreCase("0")) {
                    jsonObject1.put("Id", "0");
                    jsonObject1.put("IsModify", "false");
                } else {
                    jsonObject1.put("Id", addresses.get(i).getId());
                    jsonObject1.put("IsModify", "true");
                }

                jsonObject1.put("StateName", addresses.get(i).getStateName());

//                    jsonObject1.put("PhoneType", 1);
                jsonObject1.put("CountryName", addresses.get(i).getCountryName());
                jsonObject1.put("CityName", addresses.get(i).getCity_name());
                jsonObject1.put("Address1", addresses.get(i).getAddress1());
                jsonObject1.put("Address2", addresses.get(i).getAddress2());
                jsonObject1.put("Address3", addresses.get(i).getAddress3());

                jsonObject1.put("DeveloperName", addresses.get(i).getDeveloper());
                jsonObject1.put("Tower", addresses.get(i).getTower());
                jsonObject1.put("Floor", addresses.get(i).getFloor());
                jsonObject1.put("UnitName", addresses.get(i).getUnit_no());
                jsonObject1.put("ZipCode", addresses.get(i).getZipcode());
//                jsonObject1.put("Address2", addresses.get(i).getAddress2());

                addressArray.put(jsonObject1);

            }
            JSONArray grouparray = new JSONArray();
            JSONObject jsonObject4 = new JSONObject();
            jsonObject4.put("Id", "");
            grouparray.put(jsonObject4);

//                 getPhones=contactsFirebaseModels.get(index).getPhones();
//


            jsonObject.put("Phones", jsonArray);
            jsonObject.put("Addresses", addressArray);
            jsonObject.put("EmailAddresses", emailArray);
//            jsonObject.put("Groups", grouparray);


            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_CREATE_CONTACTS)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String msg = "";
                        if (jsonObject1.getBoolean("Success")) {

                            msg = jsonObject1.getString("Message");
                            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
//                            Intent intent=new Intent(AddContatctActivity.this,AllContactActivity.class);
//                            startActivity(intent);
//                            finish();
                        } else {
                            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_VIEW_CONTACTS)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                    String id = jsonObject1.getString("Id");
                    String FirstName = jsonObject1.getString("FirstName");
                    String LastName = jsonObject1.getString("LastName");
                    String CompanyId = jsonObject1.getString("CompanyId");
                    String CompanyName = jsonObject1.getString("CompanyName");
                    String DateOfBirthStr = jsonObject1.getString("DateOfBirthStr");
                    name.setText(FirstName);
                    last_name.setText(LastName);
                    dob.setText(DateOfBirthStr);
                    company_name.setText(CompanyName);

                    JSONArray sizeJsonArray = jsonObject1.getJSONArray("Addresses");
                    ArrayList<AddressModel> addresses = new ArrayList();
                    ContactsModel contactsModel = new ContactsModel();
                    for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                        AddressModel addresses1 = new AddressModel();
                        JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                        addresses1.setId(dataObj.getString("Id"));

                        addresses1.setCountryName(dataObj.getString("CountryName"));
                        addresses1.setStateName(dataObj.getString("StateName"));
//                                addresses1.setFirstName(dataObj.getString("FirstName"));
//                                addresses1.setLastName(dataObj.getString("LastName"));
//                                addresses1.setEmail(dataObj.getString("Email"));
//                                addresses1.setCompany(dataObj.getString("Company"));
                        addresses1.setCity_name(dataObj.getString("CityName"));
                        addresses1.setAddress1(dataObj.getString("Address1"));
                        addresses1.setAddress2(dataObj.getString("Address2"));
//                                addresses1.setPhoneNumber(dataObj.getString("PhoneNumber"));
//                                addresses1.setFaxNumber(dataObj.getString("FaxNumber"));
                        addresses.add(addresses1);
                    }
                    JSONArray phonesJsonArray = jsonObject1.getJSONArray("Phones");
                    phones = new ArrayList();
                    for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                        ContactsModel.Phones phones1 = contactsModel.new Phones();
                        JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                        phones1.setId(dataObj.getString("Id"));
                        phones1.setContactId(dataObj.getString("ContactId"));
                        phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
                        phones1.setPhoneType(dataObj.getString("Mode"));
//
                        phones.add(phones1);
                    }
                    JSONArray emailJsonArray = jsonObject1.getJSONArray("EmailAddresses");
                    ArrayList<ContactsModel.Email> emails = new ArrayList();
                    for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                        ContactsModel.Email email = contactsModel.new Email();
                        JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                        email.setId(dataObj.getString("Id"));
                        email.setContactId(dataObj.getString("ContactId"));
                        email.setAddress(dataObj.getString("Address"));
                        email.setLabel(dataObj.getString("Label"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                        emails.add(email);
                    }

                    if (phones.size() != 0) {
                        if (phones.size() == 1) {

                            phone_1.setText(phones.get(0).getPhoneNumber());
                        } else {
                            phone_1.setText(phones.get(0).getPhoneNumber());
                            phone_2.setText(phones.get(1).getPhoneNumber());
                        }
//
                    }
                    if (!addresses.isEmpty()) {
                        if (addresses.size() == 1) {
                            address_1.setText(addresses.get(0).getAddress1());
                            address_2.setText(addresses.get(0).getAddress2());
                            address_3.setText(addresses.get(0).getAddress3());
                            developer.setText(addresses.get(0).getDeveloper_name());
                            tower.setText(addresses.get(0).getTower());
                            floor.setText(addresses.get(0).getFloor());
                            unitno_txt.setText(addresses.get(0).getUnitName());
                            city_1.setText(addresses.get(0).getCity_name());
                            country_1.setText(addresses.get(0).getCountryName());
                            state_1.setText(addresses.get(0).getStateName());

                        } else {
                            address_1.setText(addresses.get(0).getAddress1());
                            address_2.setText(addresses.get(0).getAddress2());
                            address_3.setText(addresses.get(0).getAddress3());
                            developer.setText(addresses.get(0).getDeveloper_name());
                            tower.setText(addresses.get(0).getTower());
                            floor.setText(addresses.get(0).getFloor());
                            unitno_txt.setText(addresses.get(0).getUnitName());
                            city_1.setText(addresses.get(0).getCity_name());
                            country_1.setText(addresses.get(0).getCountryName());
                            state_1.setText(addresses.get(0).getStateName());
                            address_line1.setText(addresses.get(1).getAddress1());
                            address_line2.setText(addresses.get(1).getAddress2());
                            address_line3.setText(addresses.get(1).getAddress3());
                            developer1.setText(addresses.get(1).getDeveloper_name());
                            tower1.setText(addresses.get(1).getTower());
                            floor1.setText(addresses.get(1).getFloor());
                            unitno_txt1.setText(addresses.get(1).getUnitName());
                            country_2.setText(addresses.get(1).getCountryName());
                            state_2.setText(addresses.get(1).getStateName());
                            city_2.setText(addresses.get(1).getCity_name());
                        }


                    }
                    if (!emails.isEmpty()) {
                        if (emails.size() == 1) {
                            email_1.setText(emails.get(0).getAddress());
                        } else {
                            email_1.setText(emails.get(0).getAddress());
                            email_2.setText(emails.get(1).getAddress());
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
