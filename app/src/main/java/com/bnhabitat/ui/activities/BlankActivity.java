package com.bnhabitat.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.bnhabitat.R;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;

/**
 * Created by gourav on 7/6/2017.
 */

public class BlankActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blank_activity);
        String id = getIntent().getData().getQueryParameter("p") == null ? "" : getIntent().getData().getQueryParameter("p");
        Log.d("host", "host" + id);
        if(PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,"").equalsIgnoreCase("")){
            Intent intent=new Intent(BlankActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }else{
            Intent intent=new Intent(BlankActivity.this,ProjectDetailActivity.class);
            intent.putExtra("id",id);
            startActivity(intent);
            finish();
        }


    }
}
