package com.bnhabitat.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.MyinvitesModel;
import com.bnhabitat.ui.adapters.MyinvitesAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class MyInvitesActivity extends AppCompatActivity implements CommonSyncwithoutstatus.OnAsyncResultListener{
    RecyclerView invite_list;
    LinearLayoutManager linearLayoutManager;
    MyinvitesAdapter myinvitesAdapter;
    TextView no_customer;
    ArrayList<MyinvitesModel>myinvitesModels=new ArrayList<>();
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_invites);
        invite_list=(RecyclerView)findViewById(R.id.invite_list);
        back=(ImageView)findViewById(R.id.back);
        no_customer=(TextView)findViewById(R.id.no_customer);
        linearLayoutManager=new LinearLayoutManager(this);
        invite_list.setLayoutManager(linearLayoutManager);
        onSyncRequest();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_ALL_USERS)) {
//                contactsFirebaseModels.clear();
                try {

                    JSONArray projectsArray = new JSONArray(result);

                    if (projectsArray.length() == 0) {
                        no_customer.setVisibility(View.VISIBLE);
                        no_customer.setVisibility(View.VISIBLE);
                        invite_list.setVisibility(View.GONE);
//                        Toast.makeText(this, "No result found!!!", Toast.LENGTH_LONG).show();
                    }
                    else {


                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            MyinvitesModel myinvitesModel = new MyinvitesModel();
                            myinvitesModel.setId(jsonObject1.getString("Id"));
                            myinvitesModel.setUserName(jsonObject1.getString("UserName"));
                            myinvitesModel.setFirstName(jsonObject1.getString("FirstName"));
                            myinvitesModel.setLastName(jsonObject1.getString("LastName"));
                            myinvitesModel.setEmail(jsonObject1.getString("Email"));
                            myinvitesModel.setGroupName(jsonObject1.getString("GroupName"));
                            myinvitesModel.setPhone(jsonObject1.getString("Phone"));
                            myinvitesModels.add(myinvitesModel);



                        }
                        myinvitesAdapter=new MyinvitesAdapter(this,myinvitesModels);
                        invite_list.setAdapter(myinvitesAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void onSyncRequest() {
        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_USERS+ PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""), "", "Loading...", this, Urls.URL_ALL_USERS, Constants.GET).execute();

//
    }
    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }
    @Subscribe
    public void onEvent(String s) {
        if(isPermissionGranted()) {

            Log.e("call=", String.valueOf(s));
            Intent callIntent = new Intent(Intent.ACTION_CALL);

            callIntent.setData(Uri.parse("tel:" + s));
            startActivity(callIntent);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 2);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

}
