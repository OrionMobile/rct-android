package com.bnhabitat.ui.activities;


import android.app.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.ActivityCommunicator;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.listeners.TabListener;
import com.bnhabitat.models.AreaEntitiyModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.models.StateModel;
import com.bnhabitat.ui.adapters.InventoryAdapter;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.ui.fragments.AccomodationResidentialPropertyFragment;
import com.bnhabitat.ui.fragments.CommercialSpecificationFragment;
import com.bnhabitat.ui.fragments.DetailFragment;
import com.bnhabitat.ui.fragments.FinancialandBankLoanFragment;
import com.bnhabitat.ui.fragments.LegalStatusFragment;
import com.bnhabitat.ui.fragments.LocationFragment;
import com.bnhabitat.ui.fragments.OtherExpensesFragment;
import com.bnhabitat.ui.fragments.OtherQuestionsFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

import static android.support.test.InstrumentationRegistry.getArguments;

public class PropertyItemActivity extends AppCompatActivity implements SendMessage, CommonAsync.OnAsyncResultListener, ActivityCommunicator {
    ArrayList<InventoryModel> inventoryModelArrayList = new ArrayList<>();
    TabLayout tabLayout;
    String property_pid;
    ActivityCommunicator activityCommunicator;
    TextView property_select_name, text_count;
    public TextView save_draft;
    TabLayout.Tab Tab1, Tab2, Tab3, Tab4, Tab5, Tab6, Tab7, Tab8;
    JSONObject json;
    String PropertyId, PropertyName, edit_property, PropertyTypeId;
    String EditId;
    ArrayList<InventoryModel> json_Data;
    int number_count_data = 3;
    ImageView back_btn;
    public static String from;
    SendMessage SM=PropertyItemActivity.this;
    private UserInteractionListener userInteractionListener;
    ArrayList<StateModel> stateModels =new ArrayList<>();
    ArrayList<String> area_list = new ArrayList<>();
    ArrayList<PropertyUnitsModel> propertyUnitsModels = new ArrayList<>();
    ArrayList<PropertyUnitsModel> propertyUnitsModels_length = new ArrayList<>();
    String from_savedrafts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_item);

        from_savedrafts = getIntent().getStringExtra("from_savedrafts");

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        property_select_name = (TextView) findViewById(R.id.property_select_name);
        text_count = (TextView) findViewById(R.id.text_count);
        save_draft = (TextView) findViewById(R.id.save_draft);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        try {

            edit_property = getIntent().getStringExtra("edit") == null ? "" : getIntent().getStringExtra("edit");

            if (edit_property.equalsIgnoreCase("edit_property")) {

                EditId = getIntent().getStringExtra("Id");
                json_Data = (ArrayList<InventoryModel>) getIntent().getSerializableExtra("json_Data");
                PropertyId = getIntent().getStringExtra("PropertyId") == null ? "" : getIntent().getStringExtra("PropertyId");
                PropertyName = getIntent().getStringExtra("PropertyName") == null ? "" : getIntent().getStringExtra("PropertyName");
                PropertyTypeId = getIntent().getStringExtra("PropertyTypeId") == null ? "" : getIntent().getStringExtra("PropertyTypeId");
                property_select_name.setText(PropertyName);
            } else {
                PropertyId = getIntent().getStringExtra("PropertyId") == null ? "" : getIntent().getStringExtra("PropertyId");
                PropertyName = getIntent().getStringExtra("PropertyName") == null ? "" : getIntent().getStringExtra("PropertyName");
                PropertyTypeId = getIntent().getStringExtra("PropertyTypeId") == null ? "" : getIntent().getStringExtra("PropertyTypeId");
                property_select_name.setText(PropertyName);
            }


            from = getIntent().getStringExtra(Constants.FROM);
        } catch (Exception e) {

        }
        // Set Tab Icon and Titles
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        if (PropertyTypeId.equalsIgnoreCase("1")|| PropertyTypeId.equalsIgnoreCase("7")
                || PropertyTypeId.equalsIgnoreCase("8") || PropertyTypeId.equalsIgnoreCase("9") ||
                PropertyTypeId.equalsIgnoreCase("10") ||
                 PropertyTypeId.equalsIgnoreCase("11") ||PropertyTypeId.equalsIgnoreCase("12") ||
                PropertyTypeId.equalsIgnoreCase("13") ||  PropertyTypeId.equalsIgnoreCase("14") ||
        PropertyTypeId.equalsIgnoreCase("15")) {
            Tab1 = tabLayout.newTab().setText("Location");
            Tab2 = tabLayout.newTab().setText("Details");
            Tab3 = tabLayout.newTab().setText("Financial & Bank Loan's");
            Tab4 = tabLayout.newTab().setText("Other Expenses");
            Tab5 = tabLayout.newTab().setText("Legal Status");
            Tab6 = tabLayout.newTab().setText("Other Questions");
            tabLayout.addTab(Tab1);
            tabLayout.addTab(Tab2);
            tabLayout.addTab(Tab3);
            tabLayout.addTab(Tab4);
            tabLayout.addTab(Tab5);
            tabLayout.addTab(Tab6);
            bindWidgetsWithAnEvent();
            text_count.setText("" + number_count_data + "/8");
        } else {

            Tab1 = tabLayout.newTab().setText("Location");
            Tab2 = tabLayout.newTab().setText("Built Up");
            Tab3 = tabLayout.newTab().setText("Accomodation");
            Tab4 = tabLayout.newTab().setText("Specifications");
            Tab5 = tabLayout.newTab().setText("Financial & Bank Loan's");
            Tab6 = tabLayout.newTab().setText("Other Expenses");
            Tab7 = tabLayout.newTab().setText("Legal Status");
            Tab8 = tabLayout.newTab().setText("Other Questions");
            tabLayout.addTab(Tab1);
            tabLayout.addTab(Tab2);
            tabLayout.addTab(Tab3);
            tabLayout.addTab(Tab4);
            tabLayout.addTab(Tab5);
            tabLayout.addTab(Tab6);
            tabLayout.addTab(Tab7);
            tabLayout.addTab(Tab8);
            bindWidgetsWithAnEvent1();
            text_count.setText("" + number_count_data + "/10");
        }

        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    return true;
                }
            });
        }

        replaceFragment(new DetailFragment());
        setCurrentTabFragment(0);

        LocationFragment locationFragment = new LocationFragment();
        Bundle bundle = new Bundle();

        bundle.putString("PropertyId", PropertyId);
        bundle.putString("PropertyTypeId", PropertyTypeId);
        bundle.putString("edit", edit_property);
        if (EditId != null && !EditId.equalsIgnoreCase("")) {

            bundle.putString("editId", EditId);
            bundle.putString("PropertyTypeId", PropertyTypeId);

        }

        bundle.putSerializable("json_Data", json_Data);
        bundle.putSerializable("arealist", area_list);

        locationFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, locationFragment).commit();
        tabLayout.getTabAt(0);
        Tab1.select();

        getPropertySizes();
        getPropertylengthSizes();

    }


    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    public void bindWidgetsWithAnEvent1() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment2(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                LocationFragment locationFragment = new LocationFragment();
                Bundle locationbundle = new Bundle();

                locationbundle.putString("PropertyId", PropertyId);
                locationbundle.putString("PropertyTypeId", PropertyTypeId);
                locationbundle.putString("pid", property_pid);
                if (EditId != null && !EditId.equalsIgnoreCase(""))
                    locationbundle.putString("editId", EditId);
                locationbundle.putString("edit", edit_property);
                locationbundle.putSerializable("json_Data", json_Data);
                locationbundle.putSerializable("arealist", area_list);

                locationFragment.setArguments(locationbundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, locationFragment).commit();
                save_draft.setVisibility(View.GONE);


                break;
            case 1:
                DetailFragment detailFragment1 = new DetailFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("propertyunits", propertyUnitsModels);
                bundle.putSerializable("propertyunits_length", propertyUnitsModels_length);
                if (EditId != null && !EditId.equalsIgnoreCase(""))
                    bundle.putString("editId", EditId);
                bundle.putString("pid", property_pid);
                bundle.putString("edit", edit_property);
                bundle.putString("PropertyTypeId", PropertyTypeId);

                bundle.putSerializable("json_Data", json_Data);
                detailFragment1.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, detailFragment1).commit();
                number_count_data = number_count_data + 1;
                text_count.setText("" + number_count_data + "/8");
                save_draft.setVisibility(View.VISIBLE);

                break;
            case 2:
                try {
                    FinancialandBankLoanFragment financialandBankLoanFragment = new FinancialandBankLoanFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("propertyunits", propertyUnitsModels);
                    bundle1.putString("PropertyId", PropertyId);
                    bundle1.putString("PropertyTypeId", PropertyTypeId);
                    bundle1.putString("edit", edit_property);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        bundle1.putString("editId", EditId);
                    bundle1.putString("pid", property_pid);
                    bundle1.putSerializable("json_Data", json_Data);
                    financialandBankLoanFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, financialandBankLoanFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/8");
                    save_draft.setVisibility(View.VISIBLE);


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 3:
                try {
                    OtherExpensesFragment otherExpensesFragment = new OtherExpensesFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("propertyunits", propertyUnitsModels);
                    bundle1.putString("PropertyId", PropertyId);
                    bundle1.putString("pid", property_pid);
                    bundle1.putString("PropertyTypeId", PropertyTypeId);
                    bundle1.putString("edit", edit_property);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        bundle1.putString("editId", EditId);
                    bundle1.putSerializable("json_Data", json_Data);
                    otherExpensesFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, otherExpensesFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/8");
                    save_draft.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 4:
                try {
                    LegalStatusFragment legalStatusFragment = new LegalStatusFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("propertyunits", propertyUnitsModels);
                    bundle1.putString("PropertyId", PropertyId);
                    bundle1.putString("pid", property_pid);
                    bundle1.putString("edit", edit_property);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        bundle1.putString("editId", EditId);
                    bundle1.putString("PropertyTypeId", PropertyTypeId);
                    bundle1.putSerializable("json_Data", json_Data);

                    legalStatusFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, legalStatusFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/8");
                    save_draft.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 5:

                try {

                    OtherQuestionsFragment otherQuestionsFragment = new OtherQuestionsFragment();
                    Bundle locationbundle1 = new Bundle();

                    locationbundle1.putString("PropertyId", PropertyId);
                    locationbundle1.putString("pid", property_pid);
                    locationbundle1.putString("edit", edit_property);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        locationbundle1.putString("editId", EditId);
                    locationbundle1.putString("PropertyTypeId", PropertyTypeId);

                    locationbundle1.putSerializable("json_Data", json_Data);
                    otherQuestionsFragment.setArguments(locationbundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, otherQuestionsFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/10");
                    save_draft.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    private void setCurrentTabFragment2(int tabPosition) {
        switch (tabPosition) {
            case 0:
                LocationFragment locationFragment = new LocationFragment();
                Bundle locationbundle = new Bundle();

                locationbundle.putString("PropertyId", PropertyId);
                locationbundle.putString("edit", edit_property);
                locationbundle.putString("PropertyTypeId", PropertyTypeId);
                if (EditId != null && !EditId.equalsIgnoreCase(""))
                    locationbundle.putString("editId", EditId);
                locationbundle.putSerializable("json_Data", json_Data);
                locationbundle.putSerializable("arealist", area_list);

                locationFragment.setArguments(locationbundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, locationFragment).commit();
                number_count_data = number_count_data + 1;
                text_count.setText("" + number_count_data + "/10");
                save_draft.setVisibility(View.GONE);

                break;
            case 1:
                DetailFragment detailFragment1 = new DetailFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("propertyunits", propertyUnitsModels);
                bundle.putSerializable("propertyunits_length", propertyUnitsModels_length);
                bundle.putString("pid", property_pid);
                bundle.putString("PropertyId", PropertyId);
                bundle.putString("PropertyTypeId", PropertyTypeId);
                if (EditId != null && !EditId.equalsIgnoreCase(""))
                    bundle.putString("editId", EditId);
                bundle.putString("edit", edit_property);
                bundle.putSerializable("json_Data", json_Data);
                bundle.putString("built", "built_area");

                detailFragment1.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, detailFragment1).commit();
                number_count_data = number_count_data + 1;
                text_count.setText("" + number_count_data + "/10");
                save_draft.setVisibility(View.VISIBLE);

                break;
            case 2:
                try {
                    AccomodationResidentialPropertyFragment accomodationResidentialPropertyFragment = new AccomodationResidentialPropertyFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("propertyunits", propertyUnitsModels);
                    bundle1.putString("PropertyId", PropertyId);
                    bundle1.putString("PropertyTypeId", PropertyTypeId);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        bundle1.putString("editId", EditId);
                    bundle1.putString("edit", edit_property);
                    bundle1.putString("pid", property_pid);
                    bundle1.putSerializable("json_Data", json_Data);
                    Log.e("PropertyId", "PropertyId" + PropertyId);
                    accomodationResidentialPropertyFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, accomodationResidentialPropertyFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/10");
                    save_draft.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 3:
                try {
                    CommercialSpecificationFragment residentialspecification = new CommercialSpecificationFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("propertyunits", propertyUnitsModels);
                    bundle1.putString("PropertyId", PropertyId);
                    bundle1.putString("pid", property_pid);
                    bundle1.putString("edit", edit_property);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        bundle1.putString("editId", EditId);
                    bundle1.putString("PropertyTypeId", PropertyTypeId);
                    bundle1.putSerializable("json_Data", json_Data);
                    residentialspecification.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, residentialspecification).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/10");
                    save_draft.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 4:
                try {
                    FinancialandBankLoanFragment financialandBankLoanFragment = new FinancialandBankLoanFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("propertyunits", propertyUnitsModels);
                    bundle1.putString("PropertyId", PropertyId);
                    bundle1.putString("pid", property_pid);
                    bundle1.putString("PropertyTypeId", PropertyTypeId);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        bundle1.putString("editId", EditId);
                    bundle1.putString("edit", edit_property);
                    bundle1.putSerializable("json_Data", json_Data);
                    financialandBankLoanFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, financialandBankLoanFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/10");
                    save_draft.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 5:
                try {
                    OtherExpensesFragment otherExpensesFragment = new OtherExpensesFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("propertyunits", propertyUnitsModels);
                    bundle1.putString("PropertyId", PropertyId);
                    bundle1.putString("pid", property_pid);
                    bundle1.putString("edit", edit_property);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        bundle1.putString("editId", EditId);
                    bundle1.putSerializable("json_Data", json_Data);
                    bundle1.putString("PropertyTypeId", PropertyTypeId);

                    otherExpensesFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, otherExpensesFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/10");
                    save_draft.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 6:
                try {
                    LegalStatusFragment legalStatusFragment = new LegalStatusFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("propertyunits", propertyUnitsModels);
                    bundle1.putString("PropertyId", PropertyId);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        bundle1.putString("editId", EditId);
                    bundle1.putString("edit", edit_property);
                    bundle1.putString("pid", property_pid);
                    bundle1.putString("PropertyTypeId", PropertyTypeId);

                    bundle1.putSerializable("json_Data", json_Data);
                    legalStatusFragment.setArguments(bundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, legalStatusFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/10");
                    save_draft.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 7:
                try {

                    OtherQuestionsFragment otherQuestionsFragment = new OtherQuestionsFragment();
                    Bundle locationbundle1 = new Bundle();

                    locationbundle1.putString("PropertyId", PropertyId);
                    if (EditId != null && !EditId.equalsIgnoreCase(""))
                        locationbundle1.putString("editId", EditId);
                    locationbundle1.putString("edit", edit_property);
                    locationbundle1.putString("pid", property_pid);
                    locationbundle1.putString("PropertyTypeId", PropertyTypeId);

                    locationbundle1.putSerializable("json_Data", json_Data);
                    otherQuestionsFragment.setArguments(locationbundle1);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, otherQuestionsFragment).commit();
                    number_count_data = number_count_data + 1;
                    text_count.setText("" + number_count_data + "/10");
                    save_draft.setVisibility(View.VISIBLE);
                    save_draft.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public void sendData(int position) {

    }


    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_container, fragment);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    @Override
    public void sendData(JSONObject message, final int pos) {


    }

    @Override
    public void sendData(ArrayList<ContactsModel> contactsModels, int position) {

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_UNITS + "/area")) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                PropertyUnitsModel propertyUnitsModel = new PropertyUnitsModel();

                                propertyUnitsModel.setId(jsonObject1.getString("Id"));
                                propertyUnitsModel.setSizeUnitId(jsonObject1.getString("SizeUnit"));
                                propertyUnitsModels.add(propertyUnitsModel);


                            }


                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            else  if (which.equalsIgnoreCase(Urls.URL_PROPERTY_STATE_LISTING + "/1")) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {
//                  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                StateModel stateModel = new StateModel();

                                stateModel.setId(jsonObject1.getInt("Id"));
                                stateModel.setCountryId(jsonObject1.getInt("CountryId"));
                                stateModel.setName(jsonObject1.getString("Name"));

                                stateModels.add(stateModel);

                            }
                            area_list.clear();

                            for (int i = 0; i < stateModels.size(); i++) {
                                area_list.add(stateModels.get(i).getName());
                            }

                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_UNITS + "/length")) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                PropertyUnitsModel propertyUnitsModel = new PropertyUnitsModel();

                                propertyUnitsModel.setId(jsonObject1.getString("Id"));
                                propertyUnitsModel.setSizeUnitId(jsonObject1.getString("SizeUnit"));
                                propertyUnitsModels_length.add(propertyUnitsModel);


                            }


                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }
    }


    private void getPropertySizes() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/area",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_UNITS + "/area",
                Constants.GET).execute();

    }

    private void getPropertylengthSizes() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_UNITS + "/length",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_UNITS + "/length",
                Constants.GET).execute();

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
            finish();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            finish();
            super.onBackPressed();
             Intent intent=new Intent(PropertyItemActivity.this,SelectPropertyActivity.class);
            intent.putExtra(Constants.FROM,from);
            startActivity(intent);
            finish();

        }
    }

    @Override
    public JSONObject passDataToActivity() {

        return json;
    }

    @Override
    public void sendData(String pid, int pos) {
        property_pid = pid;
        if (pos == 0) {
            tabLayout.getTabAt(pos);
            Tab1.select();

        } else if (pos == 1) {
            tabLayout.getTabAt(pos);
            Tab2.select();

        }
        else if (pos == 2) {
            tabLayout.getTabAt(pos);
            Tab3.select();

        } else if (pos == 3) {
            tabLayout.getTabAt(pos);
            Tab4.select();

        } else if (pos == 4) {
            tabLayout.getTabAt(pos);
            Tab5.select();

        } else if (pos == 5) {
            tabLayout.getTabAt(pos);
            Tab6.select();

        } else if (pos == 6) {
            tabLayout.getTabAt(pos);
            Tab7.select();

        } else if (pos == 7) {
            tabLayout.getTabAt(pos);
            Tab8.select();

        }

    }
    @Override
    public void EditId(ArrayList<InventoryModel> inventoryModels, String EditId) {

        this.EditId = EditId;
        json_Data = inventoryModels;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (userInteractionListener != null)
            userInteractionListener.onUserInteraction();
    }


}
