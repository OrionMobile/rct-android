package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;


public class ResetPasswordActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener{
    @BindView(R.id.new_pass)
    EditText new_pass;
    @BindView(R.id.confirm_pass)
    EditText confirm_pass;
    private String new_pass_txt,confirm_password_txt;

    @OnClick(R.id.submit)
    public void onSubmit(){
        if(isValid())
        callForgot();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
    }

    private void callForgot() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CHANGE_PASSWORD,
                getChangeInputJson(),
                "Loading...",
                this,
                Urls.URL_CHANGE_PASSWORD,
                Constants.POST).execute();
    }
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_CHANGE_PASSWORD)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);
                    String Message="";
                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        Message=resultObject.getString("Message");
                        if(resultObject.getBoolean("Success")){

                            Utils.showSuccessErrorMessage(getString(R.string.success), Message, getString(R.string.ok), this);

                        }else{
                            Utils.showWarningErrorMessage(getString(R.string.warning), Message, getString(R.string.ok), this);


                        }




//                            } else {
//                                Utils.showWarningErrorMessage(getString(R.string.warning), "Otp doesn't match", getString(R.string.ok), this);
////
//                            }

//                        if (!resultObject.getBoolean("EmailSent")) {
//
//
//                            Utils.showSuccessErrorMessage(getString(R.string.success), msg, getString(R.string.ok), this);
//
//                        }else{
//                            Utils.showWarningErrorMessage(getString(R.string.warning), msg, getString(R.string.ok), this);
//                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ResetPasswordActivity.this,"Not able to register. Server error", Toast.LENGTH_LONG).show();

                }

            }
        }

    }



    private String getChangeInputJson() {

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("UserId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));
            jsonObject.put("Password", new_pass.getText().toString().trim());

            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


    }
    private boolean isValid() {

        new_pass_txt = new_pass.getText().toString().trim();
        confirm_password_txt = confirm_pass.getText().toString().trim();

        if(new_pass_txt.equalsIgnoreCase("")){
            Utils.showWarningErrorMessage(getString(R.string.warning),getString(R.string.enter_password),getString(R.string.ok),this);
            return false;
        }else if(confirm_password_txt.equalsIgnoreCase("")){
            Utils.showWarningErrorMessage(getString(R.string.warning),getString(R.string.enter_confirm_password),getString(R.string.ok),this);
            return false;
        }else if(new_pass_txt.length()< Constants.PASSWORD_MIN_LENGTH){
            Utils.showWarningErrorMessage(getString(R.string.warning),getString(R.string.enter_six_digit),getString(R.string.ok),this);
            return false;
        }else if(!new_pass_txt.equals(confirm_password_txt)){
            Utils.showWarningErrorMessage(getString(R.string.warning),getString(R.string.password_doesnt_match),getString(R.string.ok),this);
            return false;
        }
        return true;
    }
    public void onEvent(DialogModel dialogModel){
        if(dialogModel.getClickedOn().equalsIgnoreCase(getString(R.string.ok))) {
            Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        }
    }
}
