package com.bnhabitat.ui.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.township.townactivities.AddSectorLocality;
import com.bnhabitat.township.townactivities.Add_Project;
import com.bnhabitat.township.townactivities.Add_township;
import com.bnhabitat.ui.fragments.ForyouFragment;
import com.bnhabitat.ui.fragments.InventoryDashboardFragment;
import com.bnhabitat.ui.fragments.NewDashboardFragment;
import com.bnhabitat.ui.fragments.SettingsFragment;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.json.JSONObject;

import java.util.ArrayList;

public class NewDashboardActivity extends BaseActivity implements SendMessage {

    TabLayout.Tab Tab1, Tab2, Tab3, Tab4;
    TabLayout tabLayout;

    private View actionB,action_A, action_c, action_d, action_e, action_f, action_g;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_dashboard);

        final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        action_c = findViewById(R.id.action_c);
        action_d = findViewById(R.id.action_d);
        action_e= findViewById(R.id.action_e);
        action_f = findViewById(R.id.action_f);
        action_g = findViewById(R.id.action_g);


        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.transparent));
            }

            @SuppressLint("ResourceType")
            @Override
            public void onMenuCollapsed() {

                floatingActionsMenu.setBackgroundColor(getResources().getColor(R.color.tran));

            }
        });
        action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(NewDashboardActivity.this, Add_Project.class);
                startActivity(intent);

            }
        });
        action_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(NewDashboardActivity.this, Add_township.class);
                startActivity(intent);

            }
        });
        action_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(NewDashboardActivity.this, AddSectorLocality.class);
                startActivity(intent);

            }
        });
        action_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                Intent intent = new Intent(NewDashboardActivity.this, NewAddContactActivity.class);
                startActivity(intent);

            }
        });
        action_f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
                //Intent intent = new Intent(Township.this, Township.class);
                //startActivity(intent);

            }
        });
        action_g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*floatingActionsMenu.collapse();
                Intent intent = new Intent(NewDashboardActivity.this, Submit_Requirement.class);
                startActivity(intent);
*/
            }
        });
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        Tab1 = tabLayout.newTab().setText("Properties");
        Tab2 = tabLayout.newTab().setText("Dashboard");
        Tab3 = tabLayout.newTab().setText("Alerts");
        Tab4 = tabLayout.newTab().setText("Settings");
        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
        tabLayout.addTab(Tab3);
        tabLayout.addTab(Tab4);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        NewDashboardFragment newDashboardFragment = new NewDashboardFragment();

        //Inflate the fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_inventory_container, newDashboardFragment).commit();

        tabLayout.getTabAt(1);
        Tab2.select();

        bindWidgetsWithAnEvent();
    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {

            case 0:
                ForyouFragment foryouFragment = new ForyouFragment();

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_inventory_container, foryouFragment).commit();

                //lnrOpenNav.setVisibility(View.VISIBLE);

                break;
            case 1:
                NewDashboardFragment newDashboardFragment = new NewDashboardFragment();

                //Inflate the fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_inventory_container, newDashboardFragment).commit();
                //lnrOpenNav.setVisibility(View.GONE);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){

                }


                break;
            case 2:
//
                break;
            case 3:

                SettingsFragment settingsFragment = new SettingsFragment();

                //Inflate fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_inventory_container, settingsFragment).commit();
                //lnrOpenNav.setVisibility(View.GONE);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){

                }


                break;
//
        }
    }

    @Override
    public void sendData(JSONObject message, int position) {

    }

    @Override
    public void sendData(ArrayList<ContactsModel> contactsModels, int position) {

    }

    @Override
    public void sendData(int position) {
        if (position == 1) {
            tabLayout.getTabAt(position);
            Tab1.select();
        } else if (position == 2) {
            tabLayout.getTabAt(position);
            Tab2.select();
        }
    }

    @Override
    public void sendData(String id, int position) {

    }

    @Override
    public void EditId(ArrayList<InventoryModel> inventoryModels, String EditId) {

    }

}
