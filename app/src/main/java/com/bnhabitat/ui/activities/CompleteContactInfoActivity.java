package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.ui.adapters.SimpleFragmentPagerAdapter;
import com.bnhabitat.ui.fragments.AddressContactInfo;
import com.bnhabitat.ui.fragments.ContactSourceInfo;
import com.bnhabitat.ui.fragments.DetailFragment;
import com.bnhabitat.ui.fragments.FinancialandBankLoanFragment;
import com.bnhabitat.ui.fragments.LocationFragment;
import com.bnhabitat.ui.fragments.OtherExpensesFragment;
import com.bnhabitat.ui.fragments.PersonalContactInfo;
import com.bnhabitat.ui.fragments.ProfessionalContactInfo;

import org.json.JSONObject;

import java.util.ArrayList;

public class CompleteContactInfoActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener, SendMessage {
    public static ViewPager viewPager;
    SimpleFragmentPagerAdapter adapter;
    ArrayList<ContactsModel> contactsModels = new ArrayList<>();
    ArrayList<ContactsModel> contactsArraylist = new ArrayList<>();
    ImageView back_btn;
    String edit_data;
    TextView title;
    TabLayout tabLayout;
    TabLayout.Tab Tab1, Tab2, Tab3, Tab4;
    String contact_id;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_contact_info);


        // Find the view pager that will allow the user to swipe between fragments
//         viewPager = (ViewPager) findViewById(R.id.viewpager);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        title = (TextView) findViewById(R.id.title);
        Tab1 = tabLayout.newTab().setText("Personal");
        Tab2 = tabLayout.newTab().setText("Professional");
        Tab3 = tabLayout.newTab().setText("Addresses");
        Tab4 = tabLayout.newTab().setText("Contact Source");
        tabLayout.addTab(Tab1);
        tabLayout.addTab(Tab2);
        tabLayout.addTab(Tab3);
        tabLayout.addTab(Tab4);
        contactsModels = (ArrayList<ContactsModel>) getIntent().getSerializableExtra("ModelName");
        try {
            edit_data = getIntent().getStringExtra("edit") == null ? "" : getIntent().getStringExtra("edit");
            contactsModels = (ArrayList<ContactsModel>) getIntent().getSerializableExtra("ModelName");
            contact_id = getIntent().getStringExtra("contact_id");
            if (edit_data.equalsIgnoreCase("")) {
                title.setText("Add Contact");
            } else {
                title.setText("Edit Contact");
            }
        } catch (Exception e) {

        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


//        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
//        for (int i = 0; i < tabStrip.getChildCount(); i++) {
//            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    return true;
//                }
//            });
//        }


        PersonalContactInfo personalContactInfo = new PersonalContactInfo();
        try {
            if (!contactsModels.isEmpty()) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("ModelName", contactsModels);
                bundle.putString("edit", edit_data);
                personalContactInfo.setArguments(bundle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_add_contact, personalContactInfo).commit();
        tabLayout.getTabAt(0);
        Tab1.select();

        bindWidgetsWithAnEvent();

        // Create an adapter that knows which fragment should be shown on each page
//       adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager(),contactsModels);
//
//
//        // Set the adapter onto the view pager
//        viewPager.setAdapter(adapter);
//        if(getIntent().getExtras()!=null){
//
//
//


    }

    public void bindWidgetsWithAnEvent() {

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        try {
            if (getIntent().getStringExtra("position").equalsIgnoreCase("1")) {

                Tab2.select();
            }

            if (getIntent().getStringExtra("position").equalsIgnoreCase("0")) {
                Tab1.select();
            }
            if (getIntent().getStringExtra("position").equalsIgnoreCase("2")) {
                Tab3.select();

            }
            if (getIntent().getStringExtra("position").equalsIgnoreCase("3")) {
                Tab4.select();
            }
        } catch (Exception e) {

        }

    }


    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                PersonalContactInfo personalContactInfo = new PersonalContactInfo();
                try {
                    if (!contactsModels.isEmpty()) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("ModelName", contactsModels);
                        bundle.putString("edit", edit_data);
                        personalContactInfo.setArguments(bundle);
                    }
                } catch (Exception e) {

                }

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_add_contact, personalContactInfo).commit();

//
                break;
            case 1:
                ProfessionalContactInfo professionalContactInfo = new ProfessionalContactInfo();
                Bundle bundle = new Bundle();
                ;

//                if(contactsModels==null||contactsModels.isEmpty()){
//                    bundle.putSerializable("newContactAdd",contactsArraylist);
//                    bundle.putSerializable("ModelName",contactsModels);
//
//
//                }else{
                bundle.putSerializable("ModelName", contactsModels);
                bundle.putString("edit", edit_data);
//                }
                professionalContactInfo.setArguments(bundle);
                //Inflate the fragment
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_add_contact, professionalContactInfo).commit();

                break;
            case 2:
                try {
                    AddressContactInfo addressContactInfo = new AddressContactInfo();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("ModelName", contactsModels);
                    bundle1.putString("edit", edit_data);
                    addressContactInfo.setArguments(bundle1);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_add_contact, addressContactInfo).commit();


                } catch (Exception e) {
                    e.printStackTrace();
                }
//
                break;
            case 3:
                try {
                    ContactSourceInfo contactSourceInfo = new ContactSourceInfo();
                    Bundle bundle2 = new Bundle();
                    bundle2.putSerializable("ModelName", contactsModels);
                    bundle2.putString("edit", edit_data);
                    contactSourceInfo.setArguments(bundle2);
                    //Inflate the fragment
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_add_contact, contactSourceInfo).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
//
//
                break;
        }
    }
//    private void onPostContacts() {
//
//        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
//                getLoginInputJson(),
//                "Loading...",
//                this,
//                Urls.URL_CREATE_CONTACTS,
//                Constants.POST).execute();
//    }

//    private String getLoginInputJson() {
//
//        String inputJson = "";
////
//
//
////
//        try {
//
//
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("FirstName", name.getText().toString());
//            jsonObject.put("LastName", last_name.getText().toString());
//            jsonObject.put("CompanyId", "1");
//
//            jsonObject.put("DateOfBirth", dob.getText().toString());
//            jsonObject.put("Gender", "male");
//            jsonObject.put("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));
//            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));
//            if (!phone_1.getText().toString().equalsIgnoreCase("")) {
//
//                getPhones.add(phone_1.getText().toString());
//            }
//            if (!phone_2.getText().toString().equalsIgnoreCase("")) {
//                getPhones.add(phone_2.getText().toString());
//            }
//            if (!email_1.getText().toString().equalsIgnoreCase("")) {
//                getEmails.add(email_1.getText().toString());
//            }
//            if (!email_2.getText().toString().equalsIgnoreCase("")) {
//                getEmails.add(email_2.getText().toString());
//            }
//            if (!address_1.getText().toString().equalsIgnoreCase("")||!city_1.getText().toString().equalsIgnoreCase("")) {
//                AddressModel addressModel = new AddressModel();
//                addressModel.setAddress1(address_1.getText().toString());
//                addressModel.setAddress2(address_2.getText().toString());
//                addressModel.setAddress3(address_3.getText().toString());
//                addressModel.setZipcode(zip_1.getText().toString());
//                addressModel.setDeveloper(developer.getText().toString());
//                addressModel.setTower(tower.getText().toString());
//                addressModel.setFloor(floor.getText().toString());
//                addressModel.setUnit_no(unitno_txt.getText().toString());
//                addressModel.setCity_name(city_1.getText().toString());
//                addressModel.setCountryName(country_1.getText().toString());
//                addressModel.setStateName(state_1.getText().toString());
//                addresses.add(addressModel);
//            }
//
//
//            if (!address_line1.getText().toString().equalsIgnoreCase("")||!city_2.getText().toString().equalsIgnoreCase("")) {
//                AddressModel addressModel1 = new AddressModel();
//                addressModel1.setAddress1(address_line1.getText().toString());
//                addressModel1.setAddress2(address_line2.getText().toString());
//                addressModel1.setZipcode(zip_2.getText().toString());
//                addressModel1.setCity_name(city_2.getText().toString());
//                addressModel1.setCountryName(country_2.getText().toString());
//                addressModel1.setStateName(state_2.getText().toString());
//                addressModel1.setAddress3(address_line3.getText().toString());
//
//                addressModel1.setDeveloper(developer1.getText().toString());
//                addressModel1.setTower(tower1.getText().toString());
//                addressModel1.setFloor(floor1.getText().toString());
//                addressModel1.setUnit_no(unitno_txt1.getText().toString());
//
//                addresses.add(addressModel1);
//            }
//
//            JSONArray addressArray = new JSONArray();
////                 getPhones=contactsFirebaseModels.get(index).getPhones();
////
//            for (int i = 0; i < addresses.size(); i++) {
//
//                JSONObject jsonObject1 = new JSONObject();
//
//                jsonObject1.put("StateName", addresses.get(i).getStateName());
//
////                    jsonObject1.put("PhoneType", 1);
//                jsonObject1.put("CountryName", addresses.get(i).getCountryName());
//                jsonObject1.put("CityName", addresses.get(i).getCity_name());
//                jsonObject1.put("Address1", addresses.get(i).getAddress1());
//                jsonObject1.put("Address2", addresses.get(i).getAddress2());
//                jsonObject1.put("Address3", addresses.get(i).getAddress3());
//
//                jsonObject1.put("DeveloperName", addresses.get(i).getDeveloper());
//                jsonObject1.put("Tower", addresses.get(i).getTower());
//                jsonObject1.put("Floor", addresses.get(i).getFloor());
//                jsonObject1.put("UnitNo", addresses.get(i).getUnit_no());
//                jsonObject1.put("ZipCode", addresses.get(i).getZipcode());
//
//                addressArray.put(jsonObject1);
//
//            }
//
////                 getPhones=contactsFirebaseModels.get(index).getPhones();
////
//            JSONArray jsonArray = new JSONArray();
//            for (int i = 0; i < getPhones.size(); i++) {
//
//                JSONObject jsonObject2 = new JSONObject();
//
//                jsonObject2.put("PhoneNumber", getPhones.get(i));
//
////                    jsonObject1.put("PhoneType", 1);
//                jsonObject2.put("Mode", "");
//
//                jsonArray.put(jsonObject2);
//
//            }
//            JSONArray emailArray = new JSONArray();
//            for (int i = 0; i < getEmails.size(); i++) {
//
//                JSONObject jsonObject3 = new JSONObject();
//
//                jsonObject3.put("Address", getEmails.get(i));
//
////                    jsonObject1.put("PhoneType", 1);
//                jsonObject3.put("Label", "Email Label");
//
//                emailArray.put(jsonObject3);
//
//            }
//            JSONArray grouparray = new JSONArray();
//            JSONObject jsonObject4 = new JSONObject();
//            jsonObject4.put("Id", "");
//            grouparray.put(jsonObject4);
//            jsonObject.put("Phones", jsonArray);
//            jsonObject.put("Addresses", addressArray);
//            jsonObject.put("EmailAddresses", emailArray);
////            jsonObject.put("Groups", grouparray);
//
//            inputJson = jsonObject.toString();
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return inputJson;
//    }

    @Override
    public void onResultListener(String result, String which) {

    }

    @Override
    public void sendData(JSONObject message, final int pos) {


    }

    @Override
    public void sendData(int pos) {
//        if (pos == 1) {
//            tabLayout.getTabAt(pos);
//            Tab2.select();
//        } else if (pos == 2) {
//            tabLayout.getTabAt(pos);
//            Tab3.select();
//        } else if (pos == 3) {
//            tabLayout.getTabAt(pos);
//            Tab4.select();
//        }
    }

    @Override
    public void sendData(String id, int position) {

    }

    @Override
    public void EditId(ArrayList<InventoryModel> inventoryModels, String EditId) {

    }



    @Override
    public void sendData(ArrayList<ContactsModel> contactsModels, int pos) {
        contactsArraylist = contactsModels;
        if (pos == 1) {
            tabLayout.getTabAt(pos);
            Tab2.select();
        } else if (pos == 2) {
            tabLayout.getTabAt(pos);
            Tab3.select();
        } else if (pos == 3) {
            tabLayout.getTabAt(pos);
            Tab4.select();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  if (edit_data.equalsIgnoreCase("")) {
            super.onBackPressed();
        } else {
            Intent intent = new Intent(CompleteContactInfoActivity.this, ProfileActivity.class);
            intent.putExtra("contact_id", contact_id);
            startActivity(intent);
        }*/
    }
}
