package com.bnhabitat.ui.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Utils;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import static java.lang.Integer.parseInt;

public class CalculateEmiActivity extends AppCompatActivity {
    private int minPriceIntent = 1;
    private int maxPriceIntent = 50;
    private double minBValue = 0.0;
    private double maxBValue = 100.0;
    private String minPrice = "1";
    private String maxPrice = "50";
    Spinner bank_spinner;
    private TextView minSlideTxt;
    private TextView maxSlideTxt;
    private TextView total_price,interst_payable,total_amount_with_interest,principal_amount;
    private TextView property_value,loan_amount;
    private TextView loan_amount_percentage;
    EditText funds_percentage;
    private TextView monthly_emi;
    TextView funds_available;
    private PieChart mPieChart;
    PieModel payablePieModel,interestPieModel;
    double propertyValue;
     double loanAmount;
    ImageView back;
    int months=1;
     EditText rate_of_interest;
    String[] country = {"Axis", "HDFC", "ICICI", "Dena Bank", "SBI"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_emi);



        minSlideTxt = (TextView) findViewById(R.id.minSlideTxt);
        maxSlideTxt = (TextView) findViewById(R.id.maxSlideTxt);
        rate_of_interest = (EditText) findViewById(R.id.rate_of_interest);
        funds_percentage = (EditText) findViewById(R.id.funds_percentage);
        loan_amount_percentage = (TextView) findViewById(R.id.loan_amount_percentage);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//         ProperertyValue=PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.EFFECTIVE_PRICE,"")
        monthly_emi = (TextView) findViewById(R.id.monthly_emi);
        interst_payable = (TextView) findViewById(R.id.interst_payable);
        property_value = (TextView) findViewById(R.id.property_value);
        loan_amount = (TextView) findViewById(R.id.loan_amount);
        funds_available = (TextView) findViewById(R.id.funds_available);
        total_amount_with_interest = (TextView) findViewById(R.id.total_amount_with_interest);
        principal_amount = (TextView) findViewById(R.id.principal_amount);
        bank_spinner=(Spinner)findViewById(R.id.bank_spinner);
        mPieChart = (PieChart) findViewById(R.id.piechart);

        payablePieModel=new PieModel("", 75, Color.parseColor("#FE6DA8"));
        mPieChart.addPieSlice(payablePieModel);
        interestPieModel=new PieModel("", 25, Color.parseColor("#56B7F1"));
        mPieChart.addPieSlice(interestPieModel);
         final String PropertyValue= PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.EFFECTIVE_PRICE,"");
        final double propertyLongValue= Double.parseDouble(PropertyValue);
        property_value.setText( Utils.getConvertedPrice(Long.parseLong(String.format("%.0f", propertyLongValue)),CalculateEmiActivity.this));
         propertyValue= propertyLongValue;

        double fundsAvailable= propertyValue*30.0f/100.0f;

         loanAmount=propertyValue-fundsAvailable;
        loan_amount.setText(Utils.getConvertedPrice((long) loanAmount,CalculateEmiActivity.this));
        funds_available.setText(Utils.getConvertedPrice((long) fundsAvailable,CalculateEmiActivity.this));


        rate_of_interest.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                if(!rate_of_interest.getText().toString().equalsIgnoreCase("")){
                    Float value= Float.parseFloat(rate_of_interest.getText().toString());
                    if(value==0){
                        showLoanPayments("1",String.valueOf(months),String.valueOf(loanAmount));

                    }
                    else{

                        showLoanPayments(String.valueOf(value),String.valueOf(months),String.valueOf(loanAmount));
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        funds_percentage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int value=0;
                if(!funds_percentage.getText().toString().equalsIgnoreCase("")){
                     value= Integer.parseInt(funds_percentage.getText().toString());
                }
                else{
                    loan_amount_percentage.setText(String.valueOf(100-value)+"%");
                  value=30;
                }



                if(value<30){
                    loan_amount_percentage.setText(String.valueOf(100-value)+"%");
                    value=30;
                    Toast.makeText(CalculateEmiActivity.this, "funds must be greater than 30%", Toast.LENGTH_SHORT).show();
                }
                else if(value>=99){

                    loan_amount_percentage.setText(String.valueOf(100-value)+"%");
                    value=99;

                    Toast.makeText(CalculateEmiActivity.this, "funds must be less than 100%", Toast.LENGTH_SHORT).show();
                }
                else{



                    double fundsAvailable= propertyValue*value/100.0f;

                     loanAmount=propertyValue-fundsAvailable;
                    loan_amount_percentage.setText(String.valueOf(100-value)+"%");
                    loan_amount.setText(Utils.getConvertedPrice((long) loanAmount,CalculateEmiActivity.this));
                    funds_available.setText(Utils.getConvertedPrice((long) fundsAvailable,CalculateEmiActivity.this));
//                        funds_percentage.setText(String.valueOf(fundsPercentage)+"%");
//                        loan_amount_percentage.setText(String.valueOf(100-fundsPercentage)+"%");
                    showLoanPayments(rate_of_interest.getText().toString(),String.valueOf(months),String.valueOf(loanAmount));
                }









            }

            @Override
            public void afterTextChanged(Editable editable) {



            }
        });

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        bank_spinner.setAdapter(aa);
         try{

         }catch (Exception e){

             e.printStackTrace();
         }



        SeekBar budgetRangeSeekBar = (SeekBar) findViewById(R.id.budgetRangeSeekBar);

        budgetRangeSeekBar.setMax(20);
        budgetRangeSeekBar.setProgress(20);
        budgetRangeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                int Progress=i;

                 if(Progress==0){
                     Progress=1;
                     seekBar.setProgress(1);
                 }
                minSlideTxt.setText(String.valueOf(Progress)+" yr");

                 months=Progress;

                if(loan_amount.getText().toString().equalsIgnoreCase("-")){
                    showLoanPayments(rate_of_interest.getText().toString(),String.valueOf(months), String.valueOf(propertyValue));
                }
                else{
                    showLoanPayments(rate_of_interest.getText().toString(),String.valueOf(months), String.valueOf(loanAmount));
                }



            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        showLoanPayments(rate_of_interest.getText().toString(),"20", String.valueOf(loanAmount));
    }


    public void showLoanPayments(String rateOfInterest,String months,String totalAmount) {


        int yearToMonths=parseInt(months)*12;
        double loanAmount = Double.parseDouble(totalAmount);
        double interestRate = Double.parseDouble(rateOfInterest);
        double loanPeriod = yearToMonths;
        double r = interestRate/1200;
        double r1 = Math.pow(r+1,loanPeriod);

        double monthlyPayment = (double) ((r+(r/(r1-1))) * loanAmount);
        double totalPayment = monthlyPayment * loanPeriod;

        double interestPayable=totalPayment-loanAmount;

        mPieChart.setInnerPadding(0);
        mPieChart.setLegendHeight(0);

        int pricipalPieModelValue= (int) (loanAmount/totalPayment*100);
        int InterestPieModelValue= (int) (interestPayable/totalPayment*100);

         payablePieModel.setValue(pricipalPieModelValue);
         interestPieModel.setValue(InterestPieModelValue);
        mPieChart.startAnimation();
        principal_amount.setText(Utils.getConvertedPrice((long) loanAmount,CalculateEmiActivity.this)+"  ("+pricipalPieModelValue+"%)");
        interst_payable.setText(Utils.getConvertedPrice((long) interestPayable,CalculateEmiActivity.this)+"  ("+(InterestPieModelValue+1)+"%)");
        monthly_emi.setText("Monthly Emi "+ Utils.getConvertedPrice((long) monthlyPayment,CalculateEmiActivity.this));
        total_amount_with_interest.setText(Utils.getConvertedPrice((long) totalPayment,CalculateEmiActivity.this));
    }


}
