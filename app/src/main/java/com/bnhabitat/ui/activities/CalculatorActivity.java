package com.bnhabitat.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.utils.Utils;

public class CalculatorActivity extends AppCompatActivity {
    private EditText rent_sqft;
    private EditText ror_txt;
    private EditText price_per_sq;
    private ImageView calculated;
    TextView symbol,symbol1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        rent_sqft = (EditText)findViewById(R.id.rent_sqft);
        price_per_sq = (EditText) findViewById(R.id.price_per_sq);
        ror_txt = (EditText) findViewById(R.id.ror_txt);
        symbol = (TextView) findViewById(R.id.symbol);
        symbol1 = (TextView) findViewById(R.id.symbol1);
        calculated = (ImageView) findViewById(R.id.calculated);
        symbol.setText(Utils.getSymbol(CalculatorActivity.this));
        symbol1.setText(Utils.getSymbol(CalculatorActivity.this));
        calculated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rent_sqft.getText().toString().equals("") && ror_txt.getText().toString().equals("") && price_per_sq.getText().toString().equals("")) {
                    Toast.makeText(CalculatorActivity.this, "Please fill atleast two fields", Toast.LENGTH_SHORT).show();
                } else {

                    if (rent_sqft.getText().toString().length() > 0 && ror_txt.getText().toString().length() > 0) {
                        double PricePerSQft = (Double.parseDouble(rent_sqft.getText().toString()) * 12 * 100) / Double.parseDouble(ror_txt.getText().toString());
                        price_per_sq.setText("" + PricePerSQft);
                    } else if (rent_sqft.getText().toString().length() > 0 && price_per_sq.getText().toString().length() > 0) {
                        double ror = (Double.parseDouble(rent_sqft.getText().toString()) * 12 * 100) / Double.parseDouble(price_per_sq.getText().toString());
                        ror_txt.setText("" + ror);
                    } else if (price_per_sq.getText().toString().length() > 0 && ror_txt.getText().toString().length() > 0) {
                        double rent = ((Double.parseDouble(price_per_sq.getText().toString()) * Double.parseDouble(ror_txt.getText().toString())) / (12 * 100));
                        Log.e("rent","rent"+rent);

                        rent_sqft.setText("" + rent);
                    } else {
                        Toast.makeText(CalculatorActivity.this, "Please fill atleast two fields", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
