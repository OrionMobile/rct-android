package com.bnhabitat.ui.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.connection.ConnectionManager;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.helper.InsecureSSLSocketFactory;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import at.grabner.circleprogress.CircleProgressView;

public class OtpVerificationActivitybkp extends AppCompatActivity implements CommonAsync.OnAsyncResultListener,  CommonSyncwithoutstatus.OnAsyncResultListener{
    SmsVerifyCatcher smsVerifyCatcher;
    private String verifyurl;
    ImageView moving_image;
    EditText otp_edittxt;
    String output;
    private HttpClient client;
    Animation animSlide;
    private InputStream inputStream;
    TextView submit, change;
    CircleProgressView mCircleView;
    private String accessToken = Constants.UTILITY_TOKEN;
    private static String otp = "", Otpcode="";
    private TextView resend_otp, sit_back_text, timer;
    //   boolean isResendBtnActivated;
    private final int smsWaitTime = 60000;//(int) TimeUnit.MINUTES.toMillis(smsWaitTimeInMinutes); //milliseconds  (2 minutes)
    private final int oneSecond = 1000;
    //  private ResendButtonTimer resendButtonTimer;
    private MyCountDownTimer myCountDownTimer;
    String phoneNumber;
    TextView txtLastAttempt, timer1;
    private  String UserId = "";
    private static int resendAttemt = 1;
    String forgot;
    StringBuffer response;
    LinearLayout resendOtp;
    RelativeLayout circularView, change_email_password;
    static boolean isOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        phoneNumber = getIntent().getStringExtra(Constants.PHONE_NUMBER);
        txtLastAttempt = (TextView) findViewById(R.id.txtLastAttempt);
        moving_image = (ImageView) findViewById(R.id.moving_image);
        otp_edittxt = (EditText) findViewById(R.id.otp_edittxt);
        circularView = (RelativeLayout) findViewById(R.id.circularView);
        change_email_password = (RelativeLayout) findViewById(R.id.change_email_password);
        change = (TextView) findViewById(R.id.change);
        resendOtp = (LinearLayout) findViewById(R.id.ResendOtp);
        //resend_otp = (TextView) findViewById(R.id.resend_otp);
        resend_otp = (TextView) findViewById(R.id.resendOtp);
        sit_back_text = (TextView) findViewById(R.id.sit_back_text);
        timer = (TextView) findViewById(R.id.timer);
        timer1 = (TextView) findViewById(R.id.txtCount);
        mCircleView = (CircleProgressView) findViewById(R.id.circleView);
        //   resendButtonTimer = new ResendButtonTimer(smsWaitTime, oneSecond);
        myCountDownTimer = new MyCountDownTimer(smsWaitTime, oneSecond);
        myCountDownTimer.start();
        submit = (TextView) findViewById(R.id.submit);
        try {
            forgot = getIntent().getStringExtra("forgot") == null ? "" : getIntent().getStringExtra("forgot");
        } catch (Exception e) {

        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!otp_edittxt.getText().toString().equalsIgnoreCase("")) {



                    if (null != myCountDownTimer)
                        myCountDownTimer.cancel();

//                  PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.PIN_SENT, "");

//                  PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.USER_ID, UserId);

//                  PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.PIN_SENT, Otpcode);
                    verifyurl=Urls.VERIFY_USER;
//                  verifyUser();
                    new AsyncCaller().execute();


                } else {
                    Utils.showWarningErrorMessage(getString(R.string.warning), "Please enter OTP received on registered Mobile Number", getString(R.string.ok), OtpVerificationActivitybkp.this);

                }




            }
        });

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).loadSavedPreferences(Constants.USERNAME, "");
                PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.USERNAME, name);
                //startActivity(new Intent(OtpVerificationActivitybkp.this, NewRegistrationActivity.class));
                Intent intent = new Intent(OtpVerificationActivitybkp.this, NewRegistrationActivity.class);
                intent.putExtra("otpvarification", isOtp);
                startActivity(intent);
                finish();
            }
        });

        resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // if (isResendBtnActivated) {


                //view gone
                resendOtp.setVisibility(View.GONE);
                circularView.setVisibility(View.VISIBLE);
                change_email_password.setVisibility(View.GONE);
                resendAttemt = resendAttemt + 1;


                new com.bnhabitat.helper.CommonAsync(OtpVerificationActivitybkp.this,
                        Urls.CONTACT_MANAGEMENT_URL + Urls.URL_SEND_OTP,
                        getInputs(),
                        "Resending...",
                        OtpVerificationActivitybkp.this,
                        Urls.URL_SEND_OTP,
                        Constants.POST).execute();



            }
        });

        /*final Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                animSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide);

// Start the animation like this

                moving_image.startAnimation(animSlide);
                handler.postDelayed(this, 1000);
            }
        };

        handler.postDelayed(r, 1000);*/
//
        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                otp_edittxt.setText(code);//set code in edit text
                //then you can send verification code to server
            }
        });

        if (forgot.equalsIgnoreCase("")) {
            sendOtp();
        }


    }


    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }


        @Override
        public void onTick(long millisUntilFinished) {


            long millis = millisUntilFinished;
            String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

            mCircleView.setMaxValue(60000);
            mCircleView.setValueAnimated(60000 - millis + 1);

            timer1.setText(hms);

        }

        @Override
        public void onFinish() {

            // onTimerFinish();

            Utils.showWarningErrorMessageOTP(getString(R.string.time_out), getString(R.string.otp_time_out), getString(R.string.ok), OtpVerificationActivitybkp.this);

            //visibility settings
            circularView.setVisibility(View.GONE);
            resendOtp.setVisibility(View.VISIBLE);
            change_email_password.setVisibility(View.VISIBLE);

        }
    }


    private void onTimerFinish() {
        timer.setText("00:00");

        //     isResendBtnActivated = true;
        sit_back_text.setText("Automatic verification failed.\n Please enter the code sent via SMS");
        sit_back_text.setTextColor(getResources().getColor(R.color.red));

//

    }

    /*private class ResendButtonTimer extends CountDownTimer {

        public ResendButtonTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));


            timer.setText(hms);
        }

        @Override
        public void onFinish() {

            sit_back_text.setText("Automatic verification failed.\n Please enter the code sent via SMS");
            sit_back_text.setTextColor(getResources().getColor(R.color.red));

            resendButtonTimer.cancel();
        }
    }*/


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {

            if (null != myCountDownTimer)
                myCountDownTimer.cancel();

            smsVerifyCatcher.onStop();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private String getInputs() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.CountryCode, "91");

            //    jsonObject.put(Constants.MobileNumber, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.COUNTRY_PHONE_CODE, ""));

            if (phoneNumber != null) {

                jsonObject.put(Constants.MobileNumber, phoneNumber);
            } else {
                jsonObject.put(Constants.MobileNumber, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, ""));
            }


            inputJson = jsonObject.toString();
        } catch (Exception e) {

        }
        return inputJson;
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            smsVerifyCatcher.onStop();
        }catch (Exception e){
            e.printStackTrace();
        }
//

    }


    /**
     * need for Android 6 real time permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void sendOtp() {
        try {

            new com.bnhabitat.helper.CommonAsync(OtpVerificationActivitybkp.this,
                    Urls.CONTACT_MANAGEMENT_URL + Urls.URL_SEND_OTP,
                    getInputs(),
                    "Resending...",
                    OtpVerificationActivitybkp.this,
                    Urls.URL_SEND_OTP,
                    Constants.POST).execute();


        } catch (Exception e) {

        }
    }

    private String getInput() {
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("CountryCode", "91");

            if (phoneNumber != null) {

                jsonObject.put(Constants.MobileNumber, phoneNumber);
            } else {
                jsonObject.put(Constants.MobileNumber, PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, ""));
            }


            return jsonObject.toString();
        }

        catch (Exception e) {
            e.printStackTrace();
            return jsonObject.toString();
        }


    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_SEND_OTP)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);

                    String msg = "";
                    if (jsonObject.getInt("StatusCode") == 200) {


                        if (resendAttemt >= 3) {
                            txtLastAttempt.setVisibility(View.GONE);
                            resend_otp.setVisibility(View.GONE);
                            myCountDownTimer.start();

                        } else {
                            txtLastAttempt.setVisibility(View.GONE);


                            //  resendButtonTimer.start();
                            //.setText("Sit Back and Relax! while we verify your mobile number");
                            //sit_back_text.setTextColor(getResources().getColor(R.color.backgroud));
                            JSONObject resultObject = jsonObject.getJSONObject("Result");
//                            msg = resultObject.getString("OneTimePassword");
//                            Otpcode = msg;

//                            callPhoneNo(Otpcode);
                            myCountDownTimer.start();
                        }


                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(OtpVerificationActivitybkp.this, "Not able to register. Server error", Toast.LENGTH_LONG).show();
                }


            }

            else if (which.equalsIgnoreCase(Constants.VERIFY_USER_API)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String msg = "";
                    if (jsonObject.getInt("StatusCode") == 200) {


                        PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.USER_ID, UserId);

                        PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.PIN_SENT, Otpcode);


                        Intent i = new Intent(OtpVerificationActivitybkp.this, IntializeActivity.class);
                        startActivity(i);
                        finishAffinity();

//
                    }

                    else {
                        Utils.showWarningErrorMessage(getString(R.string.warning), "Please enter Valid OTP sent on your Registered Mobile Number.", getString(R.string.ok), OtpVerificationActivitybkp.this);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }

    }

    public class CommonAsync extends AsyncTask<Void, Void, Void> {


        private ConnectionManager connectionManager;
        private Context context;
        private String url;

        private String input = "";
        private InputStream inputStream;
        private ProgressDialog authDialog;
        private String message;
        private String output = "";
        private String which = "";
        private int requestType;

        public CommonAsync(Context context,
                           String url,
                           String input,
                           String message,
                           String which,
                           int requestType) {
            // TODO Auto-generated constructor stub
            this.context = context;
            this.url = url;
            this.input = input;
            this.message = message;

            this.which = which;
            connectionManager = ConnectionManager.getInstance(context);
            this.requestType = requestType;

            Log.e("Data", "url: " + url + "  input: " + input);
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //viewProgressVisible(message);


        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            try {


                if (requestType == Constants.GET) {

                    inputStream = connectionManager.connectionEstablishedGet(url, accessToken, PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.EMAIL, ""));

                } else {

                    if (input.trim().equalsIgnoreCase(""))
                        inputStream = connectionManager.connectionEstablished(url, accessToken, PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.EMAIL, ""));
                    else
                        inputStream = connectionManager.connectionEstablished(url, input, accessToken, PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.EMAIL, ""));

                }

                output = connectionManager.converResponseToString(inputStream);
                Log.e("JSON", output);

            } catch (Exception e) {

                output = "";
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);


            if (null != output && !output.equalsIgnoreCase("")) {

                try {

                    JSONObject jsonObject = new JSONObject(output);
                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        UserId = resultObject.getString("UserId");
                        if (UserId.equalsIgnoreCase("0")) {
                            Utils.showWarningErrorMessage(getString(R.string.warning), "User doesn't exist", getString(R.string.ok), OtpVerificationActivitybkp.this);
//
                        } else {
//                            PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.USER_ID, UserId);
                            Otpcode = resultObject.getString("Otp");
//                            PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.PIN_SENT, Otpcode);
                        }

                    } else {

                        Toast.makeText(OtpVerificationActivitybkp.this, "Server not responding", Toast.LENGTH_LONG).show();
                        finish();
                    }

                } catch (Exception e) {

                    e.printStackTrace();

                    Toast.makeText(OtpVerificationActivitybkp.this, "Server  not responding", Toast.LENGTH_LONG).show();
                    finish();

                }
            } else {

                Toast.makeText(OtpVerificationActivitybkp.this, "Unable to connect. Please check that you are connected to the Internet and try again.", Toast.LENGTH_LONG).show();
                finish();

            }

        }


    }


    public void callPhoneNo(String Otpcode) {
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, "android.permission.READ_PHONE_NUMBERS") != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String sim1 = telephonyManager.getLine1Number();

//        if(sim1!=null){
//            if(sim1.equalsIgnoreCase())
//        }

        PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.PIN_SENT, Otpcode);
    }


    private void verifyUserTrue() {
        try {

            new com.bnhabitat.helper.CommonAsync(OtpVerificationActivitybkp.this, Urls.VERIFY_USER_TRUE,
                    getInput(true),
                    "",
                    OtpVerificationActivitybkp.this,
                    Constants.VERIFY_USER_API,
                    Constants.POST).execute();


/*
            new CommonAsync(OtpVerificationActivitybkp.this,
                    Urls.CONTACT_MANAGEMENT_URL + Urls.VERIFY_USER_TRUE,
                    getInput(true),
                    "",
                    Urls.VERIFY_USER_TRUE,
                    Constants.POST).execute();
*/

        } catch (Exception e) {

        }
    }

    private String getInput(boolean isVerified) {

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.PhoneVerify, isVerified);
            jsonObject.put(Constants.USER_ID, UserId);


            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


    }
    private class AsyncCaller extends AsyncTask<Void, Void, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(OtpVerificationActivitybkp.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            try {


                URL obj = new URL(verifyurl);
                HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
//

                String urlParameters = "authkey=110400ARgbrZJBe571341d6"+"&mobile="+PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).loadSavedPreferences(Constants.PHONE_NUMBER,"")+"&otp="+otp_edittxt.getText().toString();

                // Send post request
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + verifyurl);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result
                System.out.println(response.toString());

            } catch (IOException  e) {
                output = e.toString();
                e.printStackTrace();
            }
            return response.toString();


        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                String message=jsonObject.getString("message");
                String type=jsonObject.getString("type");
                if(type.equalsIgnoreCase("success")){
                    if (forgot.equalsIgnoreCase("forgot_screen")) {
                        Intent intent = new Intent(OtpVerificationActivitybkp.this, ResetPasswordActivity.class);

                        startActivity(intent);
                        finish();

                    } else {
                        PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.USER_ID, UserId);

                        PreferenceConnector.getInstance(OtpVerificationActivitybkp.this).savePreferences(Constants.PIN_SENT, Otpcode);
                        verifyUserTrue();

                    }

                }else{
                    Utils.showWarningErrorMessage(getString(R.string.warning), "Please enter Valid OTP sent on your Registered Mobile Number.", getString(R.string.ok), OtpVerificationActivitybkp.this);

                }
            }catch (Exception e){

            }


            //this method will be running on UI thread

            pdLoading.dismiss();
        }

    }


}

