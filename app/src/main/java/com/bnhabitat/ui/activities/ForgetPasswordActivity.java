package com.bnhabitat.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPasswordActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener {
    String Otp;
    @BindView(R.id.submit)
    TextView sumbit;
    @BindView(R.id.email_view)
    ImageView email_view;
    @BindView(R.id.email_phn_adress)
    EditText email_phn_adress;
    @BindView(R.id.signIn)
    TextView signIn;

    @OnClick(R.id.submit)
    public void onSubmit() {
        if (email_phn_adress.getText().toString().length() > 0) {

            callForgot();


        } else {
            Utils.showWarningErrorMessage(getString(R.string.warning), "Please enter your Email or Mobile Number", getString(R.string.ok), this);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);

        email_phn_adress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (email_phn_adress.getText().toString().length() > 0) {
                    email_view.setImageResource(R.drawable.email_slctd);
                } else {
                    email_view.setImageResource(R.drawable.email);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ForgetPasswordActivity.this, LoginActivity.class));
            }
        });


    }

    private void callForgot() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_FORGOT,
                getForgotInputJson(),
                "Loading...",
                this,
                Urls.URL_FORGOT,
                Constants.POST).execute();
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_FORGOT)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);
                    String msg = "";
                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        String UserId = resultObject.getString("UserId");
                        if (UserId.equalsIgnoreCase("0")) {
                            Utils.showWarningErrorMessage(getString(R.string.warning), "User doesn't exist", getString(R.string.ok), this);
//
                        } else {
                            PreferenceConnector.getInstance(this).savePreferences(Constants.USER_ID, UserId);
//                            Otp = resultObject.getString("Otp");

                            Intent intent = new Intent(ForgetPasswordActivity.this, OtpVerificationActivity.class);
                            intent.putExtra("forgot", "forgot_screen");
                            intent.putExtra(Constants.PHONE_NUMBER,email_phn_adress.getText().toString().trim());
                            startActivity(intent);
                            finishAffinity();

//                            } else {
//                                Utils.showWarningErrorMessage(getString(R.string.warning), "Otp doesn't match", getString(R.string.ok), this);
////
//                            }
                        }
//                        if (!resultObject.getBoolean("EmailSent")) {
//
//
//                            Utils.showSuccessErrorMessage(getString(R.string.success), msg, getString(R.string.ok), this);
//
//                        }else{
//                            Utils.showWarningErrorMessage(getString(R.string.warning), msg, getString(R.string.ok), this);
//                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ForgetPasswordActivity.this, "Not able to register. Server error", Toast.LENGTH_LONG).show();

                }

            }
        }

    }


    private String getForgotInputJson() {

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("PhoneCode", "91");
            jsonObject.put("EmailOrPhone", email_phn_adress.getText().toString().trim());

            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


    }


}
