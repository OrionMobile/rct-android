package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.ContactSocialDetails;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.RelationModel;
import com.bnhabitat.ui.adapters.PagerAdapter;
import com.bnhabitat.ui.adapters.RelationAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener {
  /*  TextView name, age, domain, email, phn, verified, profession, industry, designation,
            annual_income, tower, unit_no, city, pincode, state, country, building_name, tehsil, developer_name,
            contact_source, date, campaign, tag, owner, notes, relation_name, relation;*/
    public  static String contact_id;
    ImageView edit_contact_source, edit_profession, edit_address, add_contact, back, photo_edit, profile_img;
    ArrayList<ContactsModel> contactsModels = new ArrayList<>();
    ImageLoader imageLoader;
    DisplayImageOptions options;
    RecyclerView rv_relation;
    ArrayList<RelationModel> relationArrayList = new ArrayList<>();
    RelationAdapter relationAdapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        add_contact = (ImageView) findViewById(R.id.add_contact);
        back = (ImageView) findViewById(R.id.back);

        Toolbar toolbar = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
        }
        setSupportActionBar(toolbar);
        contact_id = getIntent().getStringExtra("contact_id");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Lead Contact"));
        tabLayout.addTab(tabLayout.newTab().setText("Register Contact"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });

     /*   intializeView();
        contact_id = getIntent().getStringExtra("contact_id");
        // onGetDetail();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(ProfileActivity.this)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();




        edit_contact_source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, CompleteContactInfoActivity.class);
                intent.putExtra("position", "3");
                intent.putExtra("ModelName", contactsModels);
                intent.putExtra("contact_id", contact_id);
                intent.putExtra("edit", "edit_data");
                startActivity(intent);
                finish();
            }
        });
        edit_profession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, CompleteContactInfoActivity.class);
                intent.putExtra("position", "1");
                intent.putExtra("ModelName", contactsModels);
                intent.putExtra("contact_id", contact_id);
                intent.putExtra("edit", "edit_data");
                startActivity(intent);
                finish();
            }
        });
        edit_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, CompleteContactInfoActivity.class);
                intent.putExtra("position", "2");
                intent.putExtra("ModelName", contactsModels);
                intent.putExtra("contact_id", contact_id);
                intent.putExtra("edit", "edit_data");
                startActivity(intent);
                finish();
            }
        });
        photo_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(ProfileActivity.this, CompleteContactInfoActivity.class);
                    intent.putExtra("position", "0");
                    intent.putExtra("ModelName", contactsModels);
                    intent.putExtra("edit", "edit_data");
                    intent.putExtra("contact_id", contact_id);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        */

        add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Intent intent = new Intent(ProfileActivity.this, CompleteContactInfoActivity.class);
                Intent intent = new Intent(ProfileActivity.this, NewAddContactActivity.class);

                startActivity(intent);
            }
        });
    }


    public void setSupportActionBar(Toolbar toolbar) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    public void intializeView() {
        rv_relation = (RecyclerView) findViewById(R.id.rv_relation);
        linearLayoutManager = new LinearLayoutManager(ProfileActivity.this);
        relationAdapter = new RelationAdapter(this, relationArrayList);
        rv_relation.setLayoutManager(linearLayoutManager);
        rv_relation.setAdapter(relationAdapter);
        edit_contact_source = (ImageView) findViewById(R.id.edit_contact_source);
        edit_profession = (ImageView) findViewById(R.id.edit_profession);
        edit_address = (ImageView) findViewById(R.id.edit_address);
        photo_edit = (ImageView) findViewById(R.id.photo_edit);
        profile_img = (ImageView) findViewById(R.id.profile_img);
     /*   name = (TextView) findViewById(R.id.name);
        age = (TextView) findViewById(R.id.age);
        domain = (TextView) findViewById(R.id.domain);
        email = (TextView) findViewById(R.id.email);
        phn = (TextView) findViewById(R.id.phn);
        verified = (TextView) findViewById(R.id.verified);
      *//*  relation = (TextView) findViewById(R.id.relation);
        relation_name = (TextView) findViewById(R.id.relation_name);*//*
        profession = (TextView) findViewById(R.id.profession);
        industry = (TextView) findViewById(R.id.industry);
        designation = (TextView) findViewById(R.id.designation);
        tower = (TextView) findViewById(R.id.tower);
        unit_no = (TextView) findViewById(R.id.unit_no);
        city = (TextView) findViewById(R.id.city);
        pincode = (TextView) findViewById(R.id.pincode);
        state = (TextView) findViewById(R.id.state);
        country = (TextView) findViewById(R.id.country);
        building_name = (TextView) findViewById(R.id.building_name);
        tehsil = (TextView) findViewById(R.id.tehsil);
        developer_name = (TextView) findViewById(R.id.developer_name);
        annual_income = (TextView) findViewById(R.id.annual_income);
        contact_source = (TextView) findViewById(R.id.contact_source);
        date = (TextView) findViewById(R.id.date);
        campaign = (TextView) findViewById(R.id.campaign);
        tag = (TextView) findViewById(R.id.tag);
        owner = (TextView) findViewById(R.id.owner);
       // group = (TextView) findViewById(R.id.group);
        notes = (TextView) findViewById(R.id.notes);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
    //    onGetDetail();
    }

    private void onGetDetail() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CONTACT_DETAIL + contact_id + "/" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""), "", "Loading...", this, Urls.URL_CONTACT_DETAIL, Constants.GET).execute();


    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_CONTACT_DETAIL)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                    ContactsModel contactDetailModel = new ContactsModel();
                    contactDetailModel.setId(Utils.getFilteredValue(jsonObject1.getString("Id")));
                    contactDetailModel.setFirstName(Utils.getFilteredValue(jsonObject1.getString("FirstName")));
                    contactDetailModel.setLastName(Utils.getFilteredValue(jsonObject1.getString("LastName")));
                    contactDetailModel.setPrefix(Utils.getFilteredValue(jsonObject1.getString("Prefix")));
                    contactDetailModel.setMiddleName(Utils.getFilteredValue(jsonObject1.getString("MiddleName")));
                    contactDetailModel.setSufix(Utils.getFilteredValue(jsonObject1.getString("Sufix")));
                    contactDetailModel.setMaritalStatus(Utils.getFilteredValue(jsonObject1.getString("MaritalStatus")));
                    contactDetailModel.setMarriageAnniversaryDate(Utils.getFilteredValue(jsonObject1.getString("MarriageAnniversaryDate")));
                    contactDetailModel.setMarriageAnniversaryDateString(Utils.getFilteredValue(jsonObject1.getString("MarriageAnniversaryDateString")));
                    contactDetailModel.setSourceName(Utils.getFilteredValue(jsonObject1.getString("SourceName")));
                    contactDetailModel.setCampaign(Utils.getFilteredValue(jsonObject1.getString("Campaign")));
                    contactDetailModel.setSourceDate(Utils.getFilteredValue(jsonObject1.getString("SourceDateString")));
                    contactDetailModel.setOwnerShip(Utils.getFilteredValue(jsonObject1.getString("OwnerShip")));
                    contactDetailModel.setOwnerUserId(Utils.getFilteredValue(jsonObject1.getString("OwnerUserId")));
                    contactDetailModel.setIndustryName(Utils.getFilteredValue(jsonObject1.getString("IndustryName")));
                    contactDetailModel.setDesignation(Utils.getFilteredValue(jsonObject1.getString("Designation")));
                    contactDetailModel.setAnnualIncome(Utils.getFilteredValue(jsonObject1.getString("IndustryName")));
                    contactDetailModel.setAnnualIncome(Utils.getFilteredValue(jsonObject1.getString("AnnualIncome")));
                    contactDetailModel.setCompanyId(Utils.getFilteredValue(jsonObject1.getString("CompanyId")));
                    contactDetailModel.setCompanyName(Utils.getFilteredValue(jsonObject1.getString("CompanyName")));
                    contactDetailModel.setProfession(Utils.getFilteredValue(jsonObject1.getString("Profession")));
                    contactDetailModel.setReligion(Utils.getFilteredValue(jsonObject1.getString("Religion")));
                    contactDetailModel.setDateOfBirthStr(Utils.getFilteredValue(jsonObject1.getString("DateOfBirthStr")));
                    contactDetailModel.setGender(Utils.getFilteredValue(jsonObject1.getString("Gender")));
                    contactDetailModel.setImageUrl(Utils.getFilteredValue(jsonObject1.getString("ImageUrl")));
                  /*  if (!contactDetailModel.getLastName().equalsIgnoreCase("None"))
                        name.setText(contactDetailModel.getFirstName() + " " + contactDetailModel.getLastName());
                    else
                        name.setText(contactDetailModel.getFirstName());*/
                    if (!contactDetailModel.getImageUrl().equalsIgnoreCase("None"))

                        imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + contactDetailModel.getImageUrl(), profile_img, options);


                    else
                        profile_img.setImageResource(R.drawable.user_profile_img);

                  //  age.setText(contactDetailModel.getGender());
                    JSONArray sizeJsonArray = jsonObject1.getJSONArray("Addresses");
                    ArrayList<AddressModel> addresses = new ArrayList();
                    ContactsModel contactsModel = new ContactsModel();
                    if (sizeJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                            AddressModel addresses1 = new AddressModel();
                            JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                            addresses1.setId(Utils.getFilteredValue(dataObj.getString("Id")));
                            addresses1.setAddressType(Utils.getFilteredValue(dataObj.getString("AddressType")));
                            addresses1.setCountryName(Utils.getFilteredValue(dataObj.getString("CountryName")));

                            addresses1.setCountryId(Utils.getFilteredValue(dataObj.getString("CountryId")));
                            addresses1.setStateName(Utils.getFilteredValue(dataObj.getString("StateName")));
                            addresses1.setTower(Utils.getFilteredValue(dataObj.getString("Tower")));
                            addresses1.setFloor(Utils.getFilteredValue(dataObj.getString("Floor")));
                            addresses1.setZipcode(Utils.getFilteredValue(dataObj.getString("ZipCode")));
                            addresses1.setUnitName(Utils.getFilteredValue(dataObj.getString("UnitNo")));
                            addresses1.setCity_name(Utils.getFilteredValue(dataObj.getString("CityName")));
                            addresses1.setDeveloper_name(Utils.getFilteredValue(dataObj.getString("DeveloperName")));
                            addresses1.setLocality(Utils.getFilteredValue(dataObj.getString("LocalityName")));
                            addresses1.setProjectName(Utils.getFilteredValue(dataObj.getString("ProjectName")));


                            addresses.add(addresses1);
                        }
                    JSONArray phonesJsonArray = jsonObject1.getJSONArray("Phones");
                    ArrayList<ContactsModel.Phones> phones = new ArrayList();
                    if (phonesJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                            ContactsModel.Phones phones1 = contactsModel.new Phones();
                            JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                            phones1.setId(Utils.getFilteredValue(dataObj.getString("Id")));
                            phones1.setContactId(Utils.getFilteredValue(dataObj.getString("ContactId")));
                            phones1.setPhoneNumber(Utils.getFilteredValue(dataObj.getString("PhoneNumber")));
                            phones1.setPhoneCode(Utils.getFilteredValue(dataObj.getString("PhoneCode")));
                            phones1.setPhoneType(Utils.getFilteredValue(dataObj.getString("Mode")));
//
                            phones.add(phones1);

                        }
                    JSONArray contactRelationsArray = jsonObject1.getJSONArray("ContactRelations");
                    ArrayList<ContactsModel.ContactRelations> contactRelations = new ArrayList();
                    if (contactRelationsArray.length() != 0)
                        for (int indexJ = 0; indexJ < contactRelationsArray.length(); indexJ++) {

                            ContactsModel.ContactRelations contactRelations1 = contactsModel.new ContactRelations();
                            JSONObject dataObj = contactRelationsArray.getJSONObject(indexJ);

                            contactRelations1.setRelation(Utils.getFilteredValue(dataObj.getString("Relation")));
                            contactRelations1.setRelationFirstName(Utils.getFilteredValue(dataObj.getString("RelationFirstName")));
                            contactRelations1.setRelationLastName(Utils.getFilteredValue(dataObj.getString("RelationLastName")));
                            contactRelations1.setId(Utils.getFilteredValueInt(dataObj.getInt("ContactId")));
                            contactRelations.add(contactRelations1);

                        }
                    JSONArray emailJsonArray = jsonObject1.getJSONArray("EmailAddresses");
                    ArrayList<ContactsModel.Email> emails = new ArrayList();
                    if (emailJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                            ContactsModel.Email email = contactsModel.new Email();
                            JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                            email.setId(Utils.getFilteredValue(dataObj.getString("Id")));
                            email.setContactId(Utils.getFilteredValue(dataObj.getString("ContactId")));
                            email.setAddress(Utils.getFilteredValue(dataObj.getString("Address")));
                            email.setLabel(Utils.getFilteredValue(dataObj.getString("Label")));
                            emails.add(email);
                        }

                 /*   JSONArray socialJsonArray = jsonObject1.getJSONArray("ContactAttributes");
                    ArrayList<ContactsModel.ContactAttributes> socialArray = new ArrayList();
                    if (socialJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                            ContactsModel.ContactAttributes contactAttributes = contactsModel.new ContactAttributes();
                            JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);
                            contactAttributes.setKey(Utils.getFilteredValue(dataObj.getString("Key")));
                            contactAttributes.setValue(Utils.getFilteredValue(dataObj.getString("Value")));

//                          phones1.setIsPrimary(dataObj.getString("IsPrimary"));

                            socialArray.add(contactAttributes);
                        }*/

                    JSONArray socialJsonArray = jsonObject1.getJSONArray("ContactSocialDetails");
                    ArrayList<ContactSocialDetails> socialArray = new ArrayList();
                    for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                        ContactSocialDetails contactAttributes = new ContactSocialDetails();
                        JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                        contactAttributes.setType(dataObj.getString("Type"));
                        contactAttributes.setDetail(dataObj.getString("Detail"));

                        socialArray.add(contactAttributes);
                    }
                    contactDetailModel.setPhones(phones);
                    contactDetailModel.setAddresses(addresses);
                    contactDetailModel.setEmails(emails);
                    contactDetailModel.setContactRelationses(contactRelations);
                    contactDetailModel.setContactSocialDetails(socialArray);
                    contactsModels.add(contactDetailModel);


                 /*   if (!contactsModels.get(0).getAddresses().isEmpty()) {
                        tower.setText(contactsModels.get(0).getAddresses().get(0).getTower());
                        unit_no.setText(contactsModels.get(0).getAddresses().get(0).getUnitName());
                        city.setText(contactsModels.get(0).getAddresses().get(0).getTownName());
                        pincode.setText(contactsModels.get(0).getAddresses().get(0).getZipPostalCode());
                        state.setText(contactsModels.get(0).getAddresses().get(0).getStateName());
                        country.setText(contactsModels.get(0).getAddresses().get(0).getCountryName());
                        developer_name.setText(contactsModels.get(0).getAddresses().get(0).getDeveloperName());
                        tehsil.setText(contactsModels.get(0).getAddresses().get(0).getTehsilName());
                        building_name.setText(contactsModels.get(0).getAddresses().get(0).getProjectName());

                    } else {
                        tower.setText("None");
                        unit_no.setText("None");
                        // city.setText("None");
                        pincode.setText("None");
                        state.setText("None");
                        country.setText("None");
                        developer_name.setText("None");
                        tehsil.setText("None");
                        building_name.setText("None");


                    }
*/
                    JSONArray relationArray = jsonObject1.getJSONArray("ContactRelations");

                    relationArrayList.clear();

                    if (!(relationArray.length() == 0)) {

                        for (int i = 0; i < relationArray.length(); i++) {

                            JSONObject r1 = relationArray.getJSONObject(i);

                            RelationModel relationModel = new RelationModel();

                            relationModel.setRelationFirstName(r1.getString("RelationFirstName"));
                            relationModel.setRelationLastName(r1.getString("RelationLastName"));
                            relationModel.setRelation(r1.getString("Relation"));


                            relationArrayList.add(relationModel);


                        }


                    } else {
                       /* relation.setText("None");
                        relation_name.setText("None");*/
                    }
                    relationAdapter = new RelationAdapter(this, relationArrayList);
                    rv_relation.setLayoutManager(linearLayoutManager);
                    rv_relation.setAdapter(relationAdapter);
                   /* if (!contactsModels.get(0).getEmails().isEmpty()) {
                        email.setText(contactsModels.get(0).getEmails().get(0).getAddress());
                    } else {
                        email.setText("None");
                    }

*/
                  /*  if (!contactsModels.get(0).getContactAttributes().isEmpty()) {
                        domain.setText(contactsModels.get(0).getContactAttributes().get(0).getValue());
                    } else {
                        domain.setText("None");
                    }

                    if (!contactsModels.get(0).getPhones().isEmpty()) {
                        for (int i = 0; i < contactsModels.get(0).getPhones().size(); i++) {

                            phn.setText(contactDetailModel.getPhones().get(0).getPhoneNumber());
                        }
                    } else {
                        phn.setText("None");
                    }

                    if (!contactsModels.get(0).getEmails().isEmpty()) {
                        for (int i = 0; i < contactsModels.get(0).getEmails().size(); i++) {

                            email.setText(contactDetailModel.getEmails().get(0).getAddress());
                        }


                    } else {
                        email.setText("None");
                    }


                    profession.setText(contactDetailModel.getProfession());
                    designation.setText(contactDetailModel.getDesignation());
                    industry.setText(contactDetailModel.getIndustryName());
                    annual_income.setText(contactDetailModel.getAnnualIncome());
                    contact_source.setText(contactDetailModel.getSourceName());

                    date.setText(contactDetailModel.getSourceDate());
*/

                 /*   campaign.setText(contactDetailModel.getCampaign());
                    if (!contactDetailModel.getContactNotes().isEmpty())
                        notes.setText(contactDetailModel.getContactNotes().get(0).getNote());
*/


/*                    if (!contactDetailModel.getContactRelationses().isEmpty()) {

                        if (!contactDetailModel.getContactRelationses().get(0).getRelationFirstName().equalsIgnoreCase("")) {
                            relation.setText(contactDetailModel.getContactRelationses().get(0).getRelation());
                            relation_name.setText(contactDetailModel.getContactRelationses().get(0).getRelationFirstName() + " "
                                    + contactDetailModel.getContactRelationses().get(0).getRelationLastName());

                        } else {

                            relation.setText(contactDetailModel.getContactRelationses().get(1).getRelation());
                            relation_name.setText(contactDetailModel.getContactRelationses().get(1).getRelationFirstName() + " "
                                    + contactDetailModel.getContactRelationses().get(1).getRelationLastName());
                        }

                    } else {
                        relation.setText("None");
                        relation_name.setText("None");
                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }


}
