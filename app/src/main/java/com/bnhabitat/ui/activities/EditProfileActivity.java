package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.bnhabitat.R;

public class EditProfileActivity extends AppCompatActivity {

    boolean nameisOpened = true, organisationIsOpend = true, phoneIsOpened = true, emailIsOpened = true, addressIsOpened = true, web_urlIsOpened = true, eventIsOpened = true, customerIsOpened = true, customerIsOpened1 = true, connection_details_fieldIsOpened = true, specializtionIsOpened = true;
    LinearLayout name_fields_lay, organisation_feilds, phone_fields, email_fields, address_fields, web_url_fields, event_fields, customer_fields, customer_fields1, connection_details_fields, specialization_fields;
    RelativeLayout name_lay;
    ImageView drop_down_image, back;
    Button save;
    ImageView organisation_add, phone_add, email_add, address_add, web_url_add, event_add, customer_add, customer_add1, connection_details_add, specialization_add;
    Spinner phone_spinner, email_spinner, address_spinner, event_spinner, customer_spinner, connection_details_spinner1, connection_details_spinner, specialization_spinner1, specialization_spinner2, specialization_spinner3;
    String[] country = {"Mobile", "Work mobile", "Work mobile", "Work mobile", "Work mobile"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        name_lay = (RelativeLayout) findViewById(R.id.name_lay);
        name_fields_lay = (LinearLayout) findViewById(R.id.name_fields_lay);
        organisation_feilds = (LinearLayout) findViewById(R.id.organisation_feilds);
        phone_fields = (LinearLayout) findViewById(R.id.phone_fields);
        email_fields = (LinearLayout) findViewById(R.id.email_fields);
        address_fields = (LinearLayout) findViewById(R.id.address_fields);
        event_fields = (LinearLayout) findViewById(R.id.event_fields);
        web_url_fields = (LinearLayout) findViewById(R.id.web_url_fields);
        customer_fields = (LinearLayout) findViewById(R.id.customer_fields);
        customer_fields1 = (LinearLayout) findViewById(R.id.customer_fields1);
        specialization_fields = (LinearLayout) findViewById(R.id.specialization_fields);
        connection_details_fields = (LinearLayout) findViewById(R.id.connection_details_fields);
        save = (Button) findViewById(R.id.save);
        drop_down_image = (ImageView) findViewById(R.id.drop_down_image);
        back = (ImageView) findViewById(R.id.back);
        organisation_add = (ImageView) findViewById(R.id.organisation_add);
        email_add = (ImageView) findViewById(R.id.email_add);
        phone_add = (ImageView) findViewById(R.id.phone_add);
        event_add = (ImageView) findViewById(R.id.event_add);
        address_add = (ImageView) findViewById(R.id.address_add);
        web_url_add = (ImageView) findViewById(R.id.web_url_add);
        customer_add = (ImageView) findViewById(R.id.customer_add);
        customer_add1 = (ImageView) findViewById(R.id.customer_add1);
        specialization_add = (ImageView) findViewById(R.id.specialization_add);
        connection_details_add = (ImageView) findViewById(R.id.connection_details_add);
        phone_spinner = (Spinner) findViewById(R.id.phone_spinner);
        email_spinner = (Spinner) findViewById(R.id.email_spinner);
        address_spinner = (Spinner) findViewById(R.id.address_spinner);
        customer_spinner = (Spinner) findViewById(R.id.customer_spinner);
        connection_details_spinner = (Spinner) findViewById(R.id.connection_details_spinner);
        connection_details_spinner1 = (Spinner) findViewById(R.id.connection_details_spinner1);
        specialization_spinner1 = (Spinner) findViewById(R.id.specialization_spinner1);
        specialization_spinner2 = (Spinner) findViewById(R.id.specialization_spinner2);
        specialization_spinner3 = (Spinner) findViewById(R.id.specialization_spinner3);

        ArrayAdapter aa = new ArrayAdapter(EditProfileActivity.this, android.R.layout.simple_spinner_item, country);
        aa.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        //Setting the ArrayAdapter data on the Spinner
        phone_spinner.setAdapter(aa);
        email_spinner.setAdapter(aa);
        address_spinner.setAdapter(aa);
        customer_spinner.setAdapter(aa);
        connection_details_spinner.setAdapter(aa);
        connection_details_spinner1.setAdapter(aa);
        specialization_spinner1.setAdapter(aa);
        specialization_spinner2.setAdapter(aa);
        specialization_spinner3.setAdapter(aa);
        name_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (nameisOpened) {

                    invisibleAllViews();
                    drop_down_image.setRotation(180);
                    name_fields_lay.setVisibility(View.VISIBLE);
                    nameisOpened = false;
                } else {
                    drop_down_image.setRotation(0);
                    name_fields_lay.setVisibility(View.GONE);
                    nameisOpened = true;

                }

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditProfileActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditProfileActivity.this, DashboardActivity.class);
                startActivity(intent);

            }
        });
        organisation_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (organisationIsOpend) {
                    invisibleAllViews();
                    organisation_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    organisation_feilds.setVisibility(View.VISIBLE);
                    organisationIsOpend = false;
                } else {
                    organisation_feilds.setVisibility(View.GONE);
                    organisation_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    organisationIsOpend = true;

                }
            }
        });
        phone_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (phoneIsOpened) {
                    invisibleAllViews();
                    phone_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    phone_fields.setVisibility(View.VISIBLE);
                    phoneIsOpened = false;
                } else {
                    phone_fields.setVisibility(View.GONE);
                    phone_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    phoneIsOpened = true;

                }
            }
        });
        email_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emailIsOpened) {
                    invisibleAllViews();
                    email_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    email_fields.setVisibility(View.VISIBLE);
                    emailIsOpened = false;
                } else {
                    email_fields.setVisibility(View.GONE);
                    email_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    emailIsOpened = true;

                }
            }
        });

        address_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addressIsOpened) {
                    invisibleAllViews();
                    address_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    address_fields.setVisibility(View.VISIBLE);
                    addressIsOpened = false;
                } else {
                    address_fields.setVisibility(View.GONE);
                    address_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    addressIsOpened = true;

                }
            }
        });
        web_url_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (web_urlIsOpened) {
                    invisibleAllViews();
                    web_url_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    web_url_fields.setVisibility(View.VISIBLE);
                    web_urlIsOpened = false;
                } else {
                    web_url_fields.setVisibility(View.GONE);
                    web_url_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    web_urlIsOpened = true;

                }
            }
        });
        event_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (eventIsOpened) {
                    invisibleAllViews();
                    event_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    event_fields.setVisibility(View.VISIBLE);
                    eventIsOpened = false;
                } else {
                    event_fields.setVisibility(View.GONE);
                    event_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    eventIsOpened = true;

                }
            }
        });

        customer_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (customerIsOpened) {
                    invisibleAllViews();
                    customer_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    customer_fields.setVisibility(View.VISIBLE);
                    customerIsOpened = false;
                } else {
                    customer_fields.setVisibility(View.GONE);
                    customer_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    customerIsOpened = true;

                }
            }
        });
        customer_add1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (customerIsOpened1) {
                    invisibleAllViews();
                    customer_add1.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    customer_fields1.setVisibility(View.VISIBLE);
                    customerIsOpened1 = false;
                } else {
                    customer_fields1.setVisibility(View.GONE);
                    customer_add1.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    customerIsOpened1 = true;

                }
            }
        });
        connection_details_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connection_details_fieldIsOpened) {
                    invisibleAllViews();
                    connection_details_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    connection_details_fields.setVisibility(View.VISIBLE);
                    connection_details_fieldIsOpened = false;
                } else {
                    connection_details_fields.setVisibility(View.GONE);
                    connection_details_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    connection_details_fieldIsOpened = true;

                }
            }
        });
        specialization_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (specializtionIsOpened) {
                    invisibleAllViews();
                    specialization_add.setImageDrawable(getResources().getDrawable(R.drawable.minus));
                    specialization_fields.setVisibility(View.VISIBLE);
                    specializtionIsOpened = false;
                } else {
                    specialization_fields.setVisibility(View.GONE);
                    specialization_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
                    specializtionIsOpened = true;

                }
            }
        });

    }

    private void invisibleAllViews() {

        drop_down_image.setRotation(0);
        name_fields_lay.setVisibility(View.GONE);
        organisation_feilds.setVisibility(View.GONE);
        phone_fields.setVisibility(View.GONE);
        email_fields.setVisibility(View.GONE);
        address_fields.setVisibility(View.GONE);
        web_url_fields.setVisibility(View.GONE);
        event_fields.setVisibility(View.GONE);
        customer_fields.setVisibility(View.GONE);
        customer_fields1.setVisibility(View.GONE);
        connection_details_fields.setVisibility(View.GONE);
        specialization_fields.setVisibility(View.GONE);

        specialization_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
        connection_details_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
        customer_add1.setImageDrawable(getResources().getDrawable(R.drawable.add));
        customer_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
        event_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
        web_url_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
        email_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
        organisation_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
        phone_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
        address_add.setImageDrawable(getResources().getDrawable(R.drawable.add));
    }
}
