package com.bnhabitat.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.squareup.picasso.Picasso;

import junit.framework.Test;

import de.hdodenhof.circleimageview.CircleImageView;

import static io.fabric.sdk.android.services.concurrency.AsyncTask.init;

public class MyProfile extends AppCompatActivity {

    CircleImageView profileImage;
    ImageView imgBack;
    TextView name, designation, email;
    String userId, u_name, u_email, phone;
    TextView tv_firstname,tv_lastname,tv_email,tv_username,tv_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        initializtion();

        userId = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "");

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        u_name = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "");

        u_email = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.EMAIL, "");
        phone = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, "");


        name.setText(u_name.toString());

        designation.setText("Designation - NA");

        //email.setText(u_email.toString());
        email.setText("Company email - NA");

        setData();


    }

    private void setData() {

        Picasso.with(this).load(R.drawable.bnhabitat_logo).placeholder(R.drawable.symbol_check).error(R.drawable.bnhabitat_logo).into(profileImage);
        tv_firstname.setText(u_name.toString());
        tv_lastname.setText(u_name.toString());
        tv_email.setText(u_email);
        tv_username.setText(u_email.toString());
        tv_phone.setText(phone.toString());

    }

    private void initializtion() {

        profileImage = (CircleImageView) findViewById(R.id.profileImg);
        imgBack = (ImageView) findViewById(R.id.back);
        name = (TextView) findViewById(R.id.userName);
        designation = (TextView) findViewById(R.id.designation);
        email = (TextView) findViewById(R.id.userEmail);
        tv_firstname = (TextView) findViewById(R.id.tv_firstname);
        tv_lastname = (TextView) findViewById(R.id.tv_lastname);
        tv_email = (TextView) findViewById(R.id.tv_email);
        tv_username = (TextView) findViewById(R.id.tv_username);
        tv_phone = (TextView) findViewById(R.id.tv_phone);

    }
}
