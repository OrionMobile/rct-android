package com.bnhabitat.ui.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.hbb20.CountryCodePicker;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import okhttp3.internal.Util;

public class NewRegistrationActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener,CommonSyncwithoutstatus.OnAsyncResultListener  {
    int count = 0;
    boolean checked=false;
    int start,end;
    @BindView(R.id.register)
    TextView register;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confPassword)
    EditText confPassword;
    @BindView(R.id.licenseAgreement)
    TextView licenseAgreement;
    @BindView(R.id.privacyPolicy)
    TextView privacyPolicy;

    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.eye_pass)
    ImageView eye_pass;
    @BindView(R.id.eye_pass1)
    ImageView eye_pass1;
    @BindView(R.id.pass_lay)
    LinearLayout pass_lay;
    @BindView(R.id.conf_pass_lay)
    LinearLayout conf_pass_lay;

    Dialog dialog;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    String code_str ;
    String name_txt, email_txt, phone_txt, password_txt, re_password_txt, provider = "", providerId = "";

    @OnClick(R.id.register)
    public void onRegister() {
        if (!provider.equalsIgnoreCase("")) {
            if (isValidWithoutPassword())

                callNextStep();
        } else {
            if (isValid())

                callNextStep();
        }
    }

    @OnClick(R.id.licenseAgreement)
    public void onLicenseAgreement(){
        showCustomDialog(NewRegistrationActivity.this, "License Agreement");

    }

    @OnClick(R.id.privacyPolicy)
    public void onPrivacyPolicy(){
        showCustomDialog1(NewRegistrationActivity.this, "Privacy Policy");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_register_screen);
        ButterKnife.bind(this);

        String u_name = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "");
        if(!u_name.equalsIgnoreCase(""))
            name.setText(u_name);
        else
            name.setText("");

        try {
            provider = getIntent().getStringExtra("provider") == null ? "" : getIntent().getStringExtra("provider");
            providerId = getIntent().getStringExtra("providerId") == null ? "" : getIntent().getStringExtra("providerId");
            name_txt = getIntent().getStringExtra("name");
            email_txt = getIntent().getStringExtra("email");
            email.setText(email_txt);
            name.setText(name_txt);

            if (!provider.equalsIgnoreCase("")) {
                pass_lay.setVisibility(View.GONE);
                pass_lay.setVisibility(View.GONE);
            }else{
                pass_lay.setVisibility(View.VISIBLE);
                pass_lay.setVisibility(View.VISIBLE);
            }
            if (!checked) {

//              eye_pass.setBackgroundResource(getResources().getDrawable(R.drawable.ic_visibility_black_24dp));
                Drawable image = (Drawable) getResources().getDrawable(R.drawable.ic_visibility_off_black_24dp);
                eye_pass.setBackground(image);
                eye_pass1.setBackground(image);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                code_str=ccp.getSelectedCountryCode();

                Log.e("code_str",code_str);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NewRegistrationActivity.this, LoginActivity.class));
                finish();
            }
        });
        eye_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!password.getText().toString().trim().equalsIgnoreCase("")) {
                    if (!checked) {
                        start = password.getSelectionStart();
                        end = password.getSelectionEnd();
                        password.setTransformationMethod(null);
                        password.setSelection(start, end);
                        Drawable image = (Drawable) getResources().getDrawable(R.drawable.ic_visibility_black_24dp);
                        eye_pass.setBackground(image);
                        checked = true;
                    } else {
                        start = password.getSelectionStart();
                        end = password.getSelectionEnd();
                        password.setTransformationMethod(new PasswordTransformationMethod());
                        password.setSelection(start, end);
                        Drawable image = (Drawable) getResources().getDrawable(R.drawable.ic_visibility_off_black_24dp);
                        eye_pass.setBackground(image);
                        checked = false;
                    }
                }
            }
        });

        eye_pass1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!confPassword.getText().toString().trim().equalsIgnoreCase("")) {
                    if (!checked) {
                        start = confPassword.getSelectionStart();
                        end = confPassword.getSelectionEnd();
                        confPassword.setTransformationMethod(null);
                        confPassword.setSelection(start, end);
                        Drawable image = (Drawable) getResources().getDrawable(R.drawable.ic_visibility_black_24dp);
                        eye_pass1.setBackground(image);
                        checked = true;
                    } else {
                        start = confPassword.getSelectionStart();
                        end = confPassword.getSelectionEnd();
                        confPassword.setTransformationMethod(new PasswordTransformationMethod());
                        confPassword.setSelection(start, end);
                        Drawable image = (Drawable) getResources().getDrawable(R.drawable.ic_visibility_off_black_24dp);
                        eye_pass1.setBackground(image);
                        checked = false;
                    }
                }
            }
        });


        /*licenseAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });*/

    }

    @Override
    protected void onResume() {
        super.onResume();

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceConnector.getInstance(this).savePreferences(Constants.USERNAME, "");
    }

    private void callNextStep() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REGISTER,
                getRegisterInputJson(),
                "Register...",
                this,
                Urls.URL_REGISTER,
                Constants.POST).execute();
    }

    private void getToken() {

        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_TOKEN,
                getTokenInputJson(),
                "Loading...",
                this,
                Urls.URL_GET_TOKEN,
                Constants.POST).execute();
    }

    private String getTokenInputJson() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));
            jsonObject.put("AuthKey", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_AUTH_KEY,""));

            inputJson = jsonObject.toString();
        } catch (Exception e) {

        }
        return inputJson;
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_REGISTER)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);
                    String msg = "";
                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        msg = resultObject.getString("Message");
                        if (!resultObject.getBoolean("IsError")) {

                            PreferenceConnector.getInstance(NewRegistrationActivity.this).savePreferences(Constants.USER_ID_REGISTER,
                                    resultObject.getString("UserId"));
                            PreferenceConnector.getInstance(this).savePreferences(Constants.USER_AUTH_KEY, resultObject.getString("AuthKey"));

                            PreferenceConnector.getInstance(this).savePreferences(Constants.USERNAME, resultObject.getString("Name"));
                            String phone=resultObject.getString("PhoneNumber");
                            PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_NUMBER, phone);
                            PreferenceConnector.getInstance(this).savePreferences(Constants.COMPANY_ID,resultObject.getString("CompanyId"));
                            getToken();

                        } else {
                            Utils.showWarningErrorMessage(getString(R.string.warning), msg, getString(R.string.ok), this);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(NewRegistrationActivity.this, "Not able to register. Server error", Toast.LENGTH_LONG).show();

                }

            }
            if(which.equalsIgnoreCase(Urls.URL_GET_TOKEN)){
                try {

                    JSONObject jsonObject = new JSONObject(result);

                    PreferenceConnector.getInstance(this).savePreferences(Constants.APP_AUTH_KEY,jsonObject.getString("AuthToken"));
                    PreferenceConnector.getInstance(this).savePreferences(Constants.EMAIL,jsonObject.getString("Email"));

                    Utils.showSuccessErrorMessage(getString(R.string.success), "User has been registered successfully", getString(R.string.ok), this);


                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }

    }

    private String getRegisterInputJson() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", name_txt);
            jsonObject.put("Email", email_txt);
            jsonObject.put("PhoneNumber", phone_txt);
            jsonObject.put("PhoneCode", "+"+"91");
            jsonObject.put("Password", password_txt);
            jsonObject.put("GroupId", Integer.parseInt("5"));
            jsonObject.put("Provider", provider);
            jsonObject.put("ProviderId", providerId);
            jsonObject.put("DeviceId", Utils.getDeviceId(this));
            jsonObject.put("RegistrationId", "abbcccc");
            jsonObject.put("DeviceName", "Android");
            jsonObject.put("Platform", "Android");
            jsonObject.put("AppId", Integer.parseInt("1"));
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.CREATED_ID,""));
            jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);
            inputJson = jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

    private boolean isValid() {

        name_txt = name.getText().toString();
        email_txt = email.getText().toString();
        phone_txt = phone.getText().toString();
        name_txt = name.getText().toString();
        password_txt = password.getText().toString();
        re_password_txt = confPassword.getText().toString();

//        if (name_txt.equalsIgnoreCase("")) {
//            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_name), getString(R.string.ok), this);
//            return false;
//        } else
//            if (name.length() <= 2) {
//            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.username_should_be_3_characters), getString(R.string.ok), this);
//            return false;
//        }
//        else
        if (email_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_email), getString(R.string.ok), this);
            return false;
        } else if (!Utils.isEmailValid(email_txt)) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_email), getString(R.string.ok), this);
            return false;
        }else if (password_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_password), getString(R.string.ok), this);
            return false;
        } else if (password_txt.length() < Constants.PASSWORD_MIN_LENGTH) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_six_digit), getString(R.string.ok), this);
            return false;
        }else if (re_password_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_confirm_password), getString(R.string.ok), this);
            return false;
        } else if (re_password_txt.length() < Constants.PASSWORD_MIN_LENGTH) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_six_digit), getString(R.string.ok), this);
            return false;
        } else if(!password_txt.equalsIgnoreCase(re_password_txt)){
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.password_doesnt_match), getString(R.string.ok),this);
            return false;
        }else if (phone_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_phone), getString(R.string.ok), this);
            return false;
        }else if (phone_txt.length()!=10) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_phone), getString(R.string.ok), this);
            return false;
        } else if (phone_txt.equalsIgnoreCase("0000000000")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_phone), getString(R.string.ok), this);
            return false;
        } /*else if (count == 0) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.agree_terms_cond), getString(R.string.ok), this);
            return false;
        }*/
        return true;
    }

    private boolean isValidWithoutPassword() {

        name_txt = name.getText().toString();
        email_txt = email.getText().toString();
        phone_txt = phone.getText().toString();

        if (name_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), "Enter your name", getString(R.string.ok), this);
            return false;
        }else if (!Utils.isEmailValid(email_txt)) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_email), getString(R.string.ok), this);
            return false;
        }
        else if (name.length() <= 2) {
            Utils.showWarningErrorMessage(getString(R.string.warning), "Username should be atleast 3 characters long.", getString(R.string.ok), this);
            return false;
        } else if (email_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_email), getString(R.string.ok), this);
            return false;
        }  else if (phone_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_phone), getString(R.string.ok), this);
            return false;
        }
        return true;
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase(getString(R.string.ok))) {
            Intent intent = new Intent(NewRegistrationActivity.this, OtpVerificationActivity.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finishAffinity();
        }
    }

    public void showCustomDialog(Context context, String title) {
        //if (dialog == null) {
        dialog = new Dialog(context, android.R.style.Theme_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle(title);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.license_agreement);
        dialog.setCancelable(false);
        //TextView loadertext = (TextView) dialog.findViewById(R.id.message);
        ///loadertext.setText(title);
        ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        // }
    }

    public void showCustomDialog1(Context context, String title) {
        //if (dialog == null) {
        dialog = new Dialog(context, android.R.style.Theme_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle(title);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.privacy_policy);
        dialog.setCancelable(false);
        //TextView loadertext = (TextView) dialog.findViewById(R.id.message);
        ///loadertext.setText(title);
        ImageView cancel = (ImageView) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        //}
    }

}
