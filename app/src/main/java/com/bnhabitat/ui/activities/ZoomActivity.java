package com.bnhabitat.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.SitePlanModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.TouchImageView;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

public class ZoomActivity extends Activity {

    private TouchImageView zoomImageView;
    private ViewPager imagesPager;
    private ArrayList<SitePlanModel> imageUrls = new ArrayList<>();
    private ArrayList<InventoryModel.PropertyImage> propertyImages = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.LayoutPlans> layout_images = new ArrayList<>();
    private TextView typeTxtVw;
    private TextView titleTxtVw;
    String layout_plan_Images="";
    String  position1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);
//        Utils.getInstance().setCommonTitle(this,"GALLERY",0,0,0,R.drawable.back_ico,true);

        imagesPager = (ViewPager) findViewById(R.id.imagesPager);
        typeTxtVw = (TextView) findViewById(R.id.typeTxtVw);
        titleTxtVw = (TextView) findViewById(R.id.titleTxtVw);
        try{
            layout_plan_Images=getIntent().getStringExtra("layout_plan")==null?"":getIntent().getStringExtra("layout_plan");
            position1=getIntent().getStringExtra("position")==null?"":getIntent().getStringExtra("position");

        if(layout_plan_Images.equalsIgnoreCase("")){
            imageUrls = (ArrayList<SitePlanModel>) getIntent().getSerializableExtra(Constants.ZOOM_LIST);
            imagesPager.setAdapter(new ImagePagerAdapter(this, imageUrls));
        }else if(layout_plan_Images.equalsIgnoreCase("image_gallery")){
            propertyImages = (ArrayList<InventoryModel.PropertyImage>) getIntent().getSerializableExtra(Constants.ZOOM_LIST);
            imagesPager.setAdapter(new ImageGalleryAdapter(this, propertyImages));
        }

        else{
            layout_images = (ArrayList<ProjectsDetailModel.LayoutPlans>) getIntent().getSerializableExtra(Constants.ZOOM_LIST);
            imagesPager.setAdapter(new ImagePagerAdapter1(this, layout_images));
            if(position1!=null)

            titleTxtVw.setVisibility(View.GONE);

        }

        imagesPager.setCurrentItem(Integer.parseInt(position1));
        }catch (Exception e){
            e.printStackTrace();

        }
        try {
            imagesPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                   try {
                       if (position == 0) {
                           titleTxtVw.setText("SitePlan");
                       } else {
                           titleTxtVw.setText("MasterPlan");
                       }
                   }catch (Exception  e){
                       e.printStackTrace();
                   }
//                typeTxtVw.setText(imageUrls.get(position).getName());
//                titleTxtVw.setText(imageUrls.get(position).getTitle());
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

//        int position = getIntent().getIntExtra(Constants.POSITION,0);
//        imagesPager.setCurrentItem(position);

    }

    public class ImagePagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        private ArrayList<SitePlanModel> imageList = new ArrayList<>();


        public ImagePagerAdapter(Context context, ArrayList<SitePlanModel> imageUrls) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.imageList = imageUrls;

        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.zoom_pager_item, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.zoomImageView);

//            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
//                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
//                    .build();
//
//            ImageLoader imageLoader = ImageLoader.getInstance();
//            if (!imageLoader.isInited()) {
//                imageLoader.init(config);
//            }
//            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                    .cacheOnDisc(true).resetViewBeforeLoading(true)
//                    .showImageForEmptyUri(R.drawable.image_logo)
//                    .showImageOnFail(R.drawable.image_logo)
//                    .showImageOnLoading(R.drawable.image_logo).build();
//            imageLoader.displayImage(Urls.BASE_IMAGE_URL + imageList.get(position).getImage(), imageView, options);
            String url = Utils.getGeneratedImageUrl(imageList.get(position).getImage());
            Utils.setImageView(url, imageView, mContext);
            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
    public class ImageGalleryAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        private ArrayList<InventoryModel.PropertyImage> imageList = new ArrayList<>();


        public ImageGalleryAdapter(Context context, ArrayList<InventoryModel.PropertyImage> imageUrls) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.imageList = imageUrls;

        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.zoom_pager_item, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.zoomImageView);

//            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
//                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
//                    .build();
//
//            ImageLoader imageLoader = ImageLoader.getInstance();
//            if (!imageLoader.isInited()) {
//                imageLoader.init(config);
//            }
//            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                    .cacheOnDisc(true).resetViewBeforeLoading(true)
//                    .showImageForEmptyUri(R.drawable.image_logo)
//                    .showImageOnFail(R.drawable.image_logo)
//                    .showImageOnLoading(R.drawable.image_logo).build();
//            imageLoader.displayImage(Urls.BASE_IMAGE_URL + imageList.get(position).getImage(), imageView, options);
            String url = Utils.getGeneratedImageUrl(imageList.get(position).getUrl());
            Utils.setImageView(url, imageView, mContext);
            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
    public class ImagePagerAdapter1 extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        private ArrayList<ProjectsDetailModel.LayoutPlans> imageList = new ArrayList<>();


        public ImagePagerAdapter1(Context context, ArrayList<ProjectsDetailModel.LayoutPlans> imageUrls) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.imageList = imageUrls;

        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.zoom_pager_item, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.zoomImageView);

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }
            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();
            imageLoader.displayImage(Urls.BASE_IMAGE_URL + imageList.get(position).getBigImage(), imageView, options);

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
