package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.township.townactivities.LegalActivity;
import com.bnhabitat.township.townactivities.Township;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.SecurityEncryption;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "33I7K3eOvvAK5d8pQ7QgBBhXI";
    private static final String TWITTER_SECRET = "c5zQve3YIcxoytMR41tzZpUIYKaR9roKLBiZA9mpx1F0udGiSD";

    private CallbackManager callbackManager;
    private String name, username, email, id, email_phone_txt, password_txt, provider = "", providerId = "";
    private GoogleApiClient mGoogleApiClient;
    private String TAG = "Google+";
    private LinearLayout loginButton,fb;
    //ImageView register, login, show_password_image;

    //login and register
    TextView login, register;

    //forgot password text
    TextView forgot;

    //edit text and password
    EditText email_phone, password;

    boolean isShown = true;
    int start, end;
    TwitterAuthClient mTwitterAuthClient;

    private static final int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        try {
            Fabric.with(this, new Twitter(authConfig));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //setContentView(R.layout.activity_login);
        setContentView(R.layout.new_login_screen);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        findViewById(R.id.sign_in_button).setOnClickListener(this);

        try {
            String refer_code_google = PreferenceConnector.getInstance(this).loadSavedPreferences("refer", "");
//            normalTextEnc_refer = SecurityEncryption.encrypt("", refer_code_google);
            String normalTextDec = SecurityEncryption.decrypt("", refer_code_google);
            PreferenceConnector.getInstance(this).savePreferences(Constants.CREATED_ID, normalTextDec);


            Log.e("normalTextDec", "normalTextDec" + normalTextDec);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        PreferenceConnector.getInstance(this).savePreferences(Constants.USER_ID,"1230");
//        PreferenceConnector.getInstance(this).savePreferences(Constants.ACCESS_TOKEN,"ef5d0ab3-8baf-4b7d-84b6-8430c1e2c41b");
//        PreferenceConnector.getInstance(this).savePreferences(Constants.USER_AUTH_KEY,
//               "1ae9ffcb-4758-4afa-8f21-0b9d330c55f5");
/*
        try {
            String packageName = getApplicationContext().getPackageName();
            PackageInfo info = getPackageManager().getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {

        }
*/
        loginButton = (LinearLayout) findViewById(R.id.twitter_login_button);
        register = (TextView) findViewById(R.id.register);
        login = (TextView) findViewById(R.id.login);
        fb = (LinearLayout) findViewById(R.id.fb);
        //show_password_image = (ImageView) findViewById(R.id.show_password_image);
        forgot = (TextView) findViewById(R.id.forgot_password);
        email_phone = (EditText) findViewById(R.id.email_phone);
        password = (EditText) findViewById(R.id.password);

        String text = "<u><font color=\"#03a9f4\">Forgot Password?</font></u>";
        forgot.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

/*
        show_password_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isShown) {
                    start = password.getSelectionStart();
                    end = password.getSelectionEnd();
                    password.setTransformationMethod(null);
                    password.setSelection(start, end);
                    isShown = false;
                    show_password_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_black_24dp));
                } else {
                    start = password.getSelectionStart();
                    end = password.getSelectionEnd();
                    password.setTransformationMethod(new PasswordTransformationMethod());
                    ;

                    password.setSelection(start, end);

                    isShown = true;
                    show_password_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_visibility_off_black_24dp));

                }
            }
        });
*/

        mTwitterAuthClient = new TwitterAuthClient();
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Utils.isOnline(LoginActivity.this)) {
                    Utils.showErrorMessage("Internet connection not available, please try after some time",LoginActivity.this);
                }else {
                provider = "twitter";
                mTwitterAuthClient.authorize(LoginActivity.this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> twitterSessionResult) {
                        // Success

                        TwitterSession session = twitterSessionResult.data;

                        name = session.getUserName();
                        id = "" + session.getUserId();
                        onFBLogin();



//                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(TwitterException e) {
                        Log.d("TwitterKit", "Login with Twitter failure", e);
                        e.printStackTrace();
                    }
                });
            }
            }
        });
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Utils.isOnline(LoginActivity.this)) {
                     Utils.showErrorMessage("Internet connection not available, please try after some time",LoginActivity.this);
                }else{
                    provider = "facebook";
                    AccessToken accessToken = isLoggedIn();
                    if (null == accessToken)
                        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
                    else
                        getuserdata();
                }
            }
        });
//        loginButton.setCallback(new Callback<TwitterSession>() {
//
//
//

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");


                        System.out.println("onSuccess");

                        String accessToken = loginResult.getAccessToken()
                                .getToken();
                        Log.i("accessToken", accessToken);

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object,
                                                            GraphResponse response) {

                                        Log.i("LoginActivity",
                                                response.toString());
                                        try {

                                            id = object.getString("id");
//                                            String url = "http://graph.facebook.com/" + facebookId;
                                            name = object.getString("name");
                                            try {
                                                email = object.getString("email");
                                            } catch (Exception e) {

                                            }
                                            onFBLogin();
//                                            setCheckUsernameRequest();
                                            //  facebookId = id;
                                            // firstName = object.getString("name");
                                            // setFbLoginRequest(id);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields",
                                "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                        //   count=0;

                    }

                    @Override
                    public void onCancel() {
                     //   Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        //Google Plus Sign In-------------------------------------
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, NewRegistrationActivity.class);
                startActivity(intent);


            }
        });
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);

            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Utils.isOnline(LoginActivity.this)) {
                    Utils.showErrorMessage("Internet connection not available, please try after some time",LoginActivity.this);
                }else {
                    if (isValid())
                    PreferenceConnector.getInstance(LoginActivity.this).savePreferences(Constants.PASSWORD,password_txt);
                        onLogin();
                }
//                Intent intent=new Intent(LoginActivity.this,DashboardActivity.class);
//                startActivity(intent);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else if (requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE) {
            //  twitter related handling
            mTwitterAuthClient.onActivityResult(requestCode, responseCode, data);
        } else {
            callbackManager.onActivityResult(requestCode, responseCode, data);
        }

    }

    public void getuserdata() {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        // Application code
                        Log.v("LoginActivity", response.toString());

                        try {
                            //and fill them here like so.
                            id = object.getString("id");
//                            String url = "http://graph.facebook.com/" + facebookId;
                            name = object.getString("name");
                            try {
                                email = object.getString("email");
                            } catch (Exception e) {

                            }
                            onFBLogin();
//                            setCheckUsernameRequest();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public AccessToken isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken;
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            name = acct.getDisplayName();
            email = acct.getEmail();
            id = acct.getId();
//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
//            updateUI(true);
            onFBLogin();
        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }
    // [END handleSignInResult]

    // [START signIn]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

    //    @Override
//    public void onStart() {
//        super.onStart();
//
//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
//        if (opr.isDone()) {
//            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
//            // and the GoogleSignInResult will be available instantly.
//            Log.d(TAG, "Got cached sign-in");
//            GoogleSignInResult result = opr.get();
//            handleSignInResult(result);
//        } else {
//            // If the user has not previously signed in on this device or the sign-in has expired,
//            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
//            // single sign-on will occur in this branch.
////            showProgressDialog();
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(GoogleSignInResult googleSignInResult) {
////                    hideProgressDialog();
//                    handleSignInResult(googleSignInResult);
//                }
//            });
//        }
//    }
    private void updateUI(boolean signedIn) {
//        if (signedIn) {
//            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
//            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
//        } else {
//            mStatusTextView.setText(R.string.signed_out);
//
//            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
//        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                if(!Utils.isOnline(LoginActivity.this)) {
                    Utils.showErrorMessage("Internet connection not available, please try after some time",LoginActivity.this);
                }else {
                    provider = "google_plus";
                    signIn();
                }
                break;
//            case R.id.sign_out_button:
//                signOut();
//                break;
//            case R.id.disconnect_button:
//                revokeAccess();
//                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void onFBLogin() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_LOGIN_FB,
                getFBLoginInputJson(),
                "Login...",
                this,
                Urls.URL_LOGIN_FB,
                Constants.POST).execute();
    }

    private void onLogin() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_LOGIN,
                getLoginInputJson(),
                "Login...",
                this,
                Urls.URL_LOGIN,
                Constants.POST).execute();
    }

    private void getToken() {

        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_TOKEN,
                getTokenInputJson(),
                "Loading...",
                this,
                Urls.URL_GET_TOKEN,
                Constants.POST).execute();
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_LOGIN_FB)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        String msg = resultObject.getString("Message");
                        if (resultObject.getBoolean("ValidateUser")) {
//                            PreferenceConnector.getInstance(this).savePreferences(Constants.USER_AUTH_KEY, resultObject.getString("AuthKey"));
                            JSONObject jsonObject1 = resultObject.getJSONObject("UserDetail");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("UserDetail");

                            boolean isPhoneVerified =  jsonObject2.getBoolean("PhoneVerified");
                            if(isPhoneVerified){
                                PreferenceConnector.getInstance(this).savePreferences(Constants.USERNAME, jsonObject2.getString("FirstName"));
                                PreferenceConnector.getInstance(this).savePreferences(Constants.USER_ID, jsonObject2.getString("Id"));
                                PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_NUMBER, jsonObject2.getString("Phone"));
                                PreferenceConnector.getInstance(this).savePreferences(Constants.COMPANY_ID, jsonObject2.getString("CompanyId"));
//                          PreferenceConnector.getInstance(this).savePreferences(Constants.BROKERUSER_TYPE,resultObject.getString("BrokerUserType"));
                                getToken();
                            }
                            else {
                                Intent it = new Intent(this, OtpVerificationActivity.class);
                                it.putExtra(Constants.PHONE_NUMBER,jsonObject2.getString("Phone"));
                                startActivity(it);
                                finishAffinity();
                            }

//                            PreferenceConnector.getInstance(this).savePreferences(Constants.USER_AUTH_KEY, jsonObject2.getString("AuthKey"));
//                            PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_CODE,resultObject.getString("PhoneCode"));


//                            Utils.showSuccessErrorMessage(getString(R.string.success), msg, getString(R.string.ok), this);
                        } else {
                            Intent intent = new Intent(LoginActivity.this, NewRegistrationActivity.class);
                            intent.putExtra("provider", provider);
                            intent.putExtra("providerId", id);
                            intent.putExtra("name", name);
                            intent.putExtra("email", email);
                            startActivity(intent);

//                            Utils.showWarningErrorMessage(getString(R.string.warning), msg, getString(R.string.ok), this);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (which.equalsIgnoreCase(Urls.URL_LOGIN)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        String msg = resultObject.getString("Message");
                        if (resultObject.getBoolean("ValidateUser")) {
                            JSONObject jsonObject1 = resultObject.getJSONObject("UserDetail");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("UserDetail");


                            boolean isPhoneVerified =  jsonObject2.getBoolean("PhoneVerified");
                            if(isPhoneVerified){
                                PreferenceConnector.getInstance(this).savePreferences(Constants.USER_AUTH_KEY, jsonObject2.getString("AuthToken"));
//                              PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_CODE,resultObject.getString("PhoneCode"));
                                PreferenceConnector.getInstance(this).savePreferences(Constants.USERNAME, jsonObject2.getString("FirstName"));
                                PreferenceConnector.getInstance(this).savePreferences(Constants.USER_ID, jsonObject2.getString("Id"));
                                PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_NUMBER, jsonObject2.getString("Phone"));
                                PreferenceConnector.getInstance(this).savePreferences(Constants.COMPANY_ID, jsonObject2.getString("CompanyId"));

                                getToken();

                            }
                            else {
                                Intent it = new Intent(this, OtpVerificationActivity.class);
                                it.putExtra(Constants.PHONE_NUMBER,jsonObject2.getString("Phone"));
                                startActivity(it);
                                finishAffinity();
                            }



//                            Utils.showSuccessErrorMessage(getString(R.string.success), msg, getString(R.string.ok), this);
                        } else {

                            Utils.showWarningErrorMessage(getString(R.string.warning), msg, getString(R.string.ok), this);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (which.equalsIgnoreCase(Urls.URL_GET_TOKEN)) {
                try {

                    JSONObject jsonObject = new JSONObject(result);

                    PreferenceConnector.getInstance(this).savePreferences(Constants.APP_AUTH_KEY, jsonObject.getString("AuthToken"));
                    PreferenceConnector.getInstance(this).savePreferences(Constants.EMAIL, jsonObject.getString("Email"));


                    if (!PreferenceConnector.getInstance(LoginActivity.this).loadBooleanPreferences(Constants.IS_INTIALIZED)) {
                        PreferenceConnector.getInstance(LoginActivity.this).savePreferences("first_time_login", false);
                        Intent i = new Intent(LoginActivity.this, IntializeActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Intent intent = new Intent(LoginActivity.this, NewDashboardActivity.class);
                        startActivity(intent);
                        finish();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String getLoginInputJson() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("EmailOrPhone", email_phone_txt);
            jsonObject.put("Password", password_txt);
            jsonObject.put("DeviceId", Utils.getDeviceId(this));
            jsonObject.put("RegistrationId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.REGISTRATION_ID, ""));
            jsonObject.put("DeviceName", "Android");
//
            jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonObject.put("Platform", "Android");
            jsonObject.put("AppId", Integer.parseInt("1"));
            inputJson = jsonObject.toString();
        } catch (Exception e) {

        }
        return inputJson;
    }

    private String getTokenInputJson() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("AuthKey", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_AUTH_KEY, ""));

            inputJson = jsonObject.toString();
        } catch (Exception e) {

        }
        return inputJson;
    }

    private String getFBLoginInputJson() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ProviderId", id);
            jsonObject.put("Provider", provider);
            jsonObject.put("DeviceId", Utils.getDeviceId(this));
            jsonObject.put("RegistrationId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.REGISTRATION_ID, ""));
            jsonObject.put("DeviceName", "Android");
//            jsonObject.put("CompanyId", 214);
            jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);


            jsonObject.put("Platform", "Android");
            jsonObject.put("AppId", Integer.parseInt("1"));

//            jsonObject.put("PhoneCode", "91");
//            jsonObject.put("Password", password_txt);
//            jsonObject.put("Role", "customer");
//
//            jsonObject.put("Provider", "general");
//            jsonObject.put("ProviderId", "1");
//
//            jsonObject.put("DeviceId", "adcvddd");
//            jsonObject.put("RegistrationId", "123");
//            jsonObject.put("DeviceName", "abc");
//            jsonObject.put("Platform", "Android");
//            jsonObject.put("AppId", Integer.parseInt("1"));
            inputJson = jsonObject.toString();
        } catch (Exception e) {

        }
        return inputJson;
    }

    private boolean isValid() {

        email_phone_txt = email_phone.getText().toString().trim();
        password_txt = password.getText().toString().trim();

        if (email_phone_txt.equalsIgnoreCase("")) {
            Utils.showErrorMessagestatus(getString(R.string.enter_username), getString(R.string.ok), this);
            return false;
        }

//        else  if (email_phone_txt.matches("[0-9]+") && email_phone_txt.length() < 10){
//            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_phone), getString(R.string.ok), this);
//            return false;
//        }
//
//        else if (!Utils.isEmailAddressValid(email_phone_txt)) {
//            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_email), getString(R.string.ok), this);
//            return false;
//        }
        else if (password_txt.equalsIgnoreCase("")) {
            Utils.showErrorMessagestatus(getString(R.string.enter_password), getString(R.string.ok), this);
            return false;
        }
        else if (password.length() < Constants.PASSWORD_MIN_LENGTH) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_six_digit), getString(R.string.ok), this);
            return false;
        }



        return true;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
        finish();
    }


    public static boolean onlyLetterSpaceQuotes(String str){
        for(int x=0; x<str.length(); x++){
            char ch = str.charAt(x);
            if (!((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == ' '
                    || ch == '\'' || ch == '\"'))
                return false;
        }
        return true;
    }
}
