package com.bnhabitat.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.category.CategoryTable;
import com.bnhabitat.data.category.CategoryType;
import com.bnhabitat.data.location.CityTable;
import com.bnhabitat.data.location.RegionTable;
import com.bnhabitat.data.sizeunit.UnitsModel;
import com.bnhabitat.data.sizeunit.UnitsTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.ui.adapters.AdapterCategoryType;
import com.bnhabitat.ui.adapters.AdapterPropertyType;
import com.bnhabitat.ui.adapters.DemoRangeAdapter;
import com.bnhabitat.ui.views.CustomRangeSeekBar;
import com.bnhabitat.ui.views.ExpandableHeightGridView;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchProjectsActivity extends AppCompatActivity  implements AdapterCategoryType.OnPropertySizesSelected,AdapterPropertyType.OnPropertySizesSelected, CommonAsync.OnAsyncResultListener {

    private ArrayList<TextView> categoriesTextVwList = new ArrayList<>();
    private ArrayList<LinearLayout> lineLayoutList = new ArrayList<>();
    private ExpandableHeightGridView propertyTypeGridVw,categoriesTypeGridVw;
    private LinearLayout propertyTypeslayout;
    private LinearLayout categoriesLayout;
    private int minPriceIntent = 500000;
    private int maxPriceIntent = 50000000;
    private String localityNameIntent = "";
    private String minPrice = "500000";
    private String maxPrice = "50000000";
    private TextView minSlideTxt;
    private TextView maxSlideTxt;
    private TextView searchBtn;
    private TextView minSlideAreaTxt;
    private TextView maxSlideAreaTxt;
    private String minAreaPrice = "10";
    private String maxAreaPrice = "1000";
    private double minBValue = 0.0;
    private double maxBValue = 100.0;
    private double minABValue = 0.0;
    private double maxABValue = 100.0;
    private Button residential, commercial, industrial, institutional, agricultural;
    private boolean residentialIsOpened ;
    private boolean commercialIsOpened ;
    private boolean industrialIsOpened;
    private boolean institutionalIsOpened;
    private boolean agriculturalIsOpened;
    private CardView localityView;
    protected ArrayList<CharSequence> selectedRegions = new ArrayList<>();
    private String selectedRegionsName = "";
    private ArrayList<CommonDialogModel> cityModel = new ArrayList<CommonDialogModel>();
    private ArrayList<CommonDialogModel> stateModel = new ArrayList<CommonDialogModel>();
    private ArrayList<CommonDialogModel> categoryModel = new ArrayList<CommonDialogModel>();
    private ArrayList<CommonDialogModel> categoryTypeModel = new ArrayList<CommonDialogModel>();
    private ArrayList<ProjectsModel> projectsModels = new ArrayList<>();
    private ArrayList<CommonDialogModel> developersModel = new ArrayList<CommonDialogModel>();
    private ArrayList<CommonDialogModel> regionsModel = new ArrayList<CommonDialogModel>();
    ArrayList<UnitsModel> setDefaultSizes=new ArrayList<>();
    private Spinner search_edit_text;
    private ArrayList<String> regionName = new ArrayList<>();
    private ArrayList<String> cityName = new ArrayList<>();
    protected CharSequence[] receivers = {
            "Items1", "Items2", "Items3"};

    protected CharSequence[] receiversId = {
            "Items1", "Items2", "Items3"};
    ImageView back;
    private String city_id;
    private TextView localityTxt;
    String defaultId="";
    CheckBox onebhk, twobhk,threebhk,fourbhk,fivebhk;
    private LinearLayout bedrooms_lay;
    private String numberOfBedrooms="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_projects);
        back=(ImageView)findViewById(R.id.back);
        search_edit_text = (Spinner) findViewById(R.id.search_edit_text);
        localityTxt = (TextView) findViewById(R.id.localityTxt);
        bedrooms_lay=(LinearLayout)findViewById(R.id.bedrooms_lay);
        onebhk=(CheckBox)findViewById(R.id.onebhk) ;
        twobhk=(CheckBox)findViewById(R.id.twobhk) ;
        threebhk=(CheckBox)findViewById(R.id.threebhk) ;
        fourbhk=(CheckBox)findViewById(R.id.fourbhk) ;
        fivebhk=(CheckBox)findViewById(R.id.fivebhk) ;

        cityModel= CityTable.getInstance().getCities();
        for(CommonDialogModel commonDialogModel:cityModel){
            cityName.add(commonDialogModel.getTitle());
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this, R.layout.view_spinner_item, cityName);
        search_edit_text.setAdapter(arrayAdapter);
        search_edit_text.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) adapterView.getSelectedView();
                textView.setTextColor(getResources().getColor(R.color.black));
                textView.setTextColor(getResources().getColor(R.color.white));
               city_id= cityModel.get(i).getId();
                selectedRegions.clear();
                localityTxt.setText("Select Locality");

                regionName.clear();


                regionsModel = RegionTable.getInstance().getRegions(city_id);

                for (CommonDialogModel commonDialogModel : regionsModel) {
                    regionName.add(commonDialogModel.getTitle());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        propertyTypeslayout = (LinearLayout) findViewById(R.id.propertyTypeslayout);
        localityView = (CardView) findViewById(R.id.localityView);
        propertyTypeGridVw = (ExpandableHeightGridView) findViewById(R.id.propertyTypeGridVw);
        categoriesTypeGridVw = (ExpandableHeightGridView) findViewById(R.id.categoriesTypeGridVw);
        minSlideTxt = (TextView) findViewById(R.id.minSlideTxt);
        maxSlideTxt = (TextView) findViewById(R.id.maxSlideTxt);
        minSlideAreaTxt = (TextView) findViewById(R.id.minSlideAreaTxt);
        maxSlideAreaTxt = (TextView) findViewById(R.id.maxSlideAreaTxt);
        searchBtn = (TextView) findViewById(R.id.searchBtn);
        categoriesLayout = (LinearLayout) findViewById(R.id.categoriesLayout);

        bedrooms();

        databaseIntilize();

//        cityTxt.setText(PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.CITY_NAME, Constants.CITY_NAME_CONST));
        city_id = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.CITY_ID, Constants.CITY_ID_CONST);
        regionsModel = RegionTable.getInstance().getRegions(city_id);
        for (CommonDialogModel commonDialogModel : regionsModel) {
            regionName.add(commonDialogModel.getTitle());
        }
        CustomRangeSeekBar budgetRangeSeekBar = (CustomRangeSeekBar) findViewById(R.id.budgetRangeSeekBar);
        budgetRangeSeekBar.setNotifyWhileDragging(true);
        budgetRangeSeekBar.setAdapter(new DemoRangeAdapter());
        budgetRangeSeekBar.setSelectedMinValue(minPriceIntent / 500000);
        budgetRangeSeekBar.setSelectedMaxValue(maxPriceIntent / 500000);
        budgetRangeSeekBar.setOnRangeSeekBarChangeListener(new CustomRangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(CustomRangeSeekBar bar, int minPosition, int maxPosition) {
                minBValue = minPosition;
                maxBValue = maxPosition;

                Log.e("Values", "minBValue" + minBValue + " maxBValue" + maxBValue);
                int maxValueMultiply = 500000;

                if (minPosition == maxPosition)
                    maxValueMultiply = 600000;


                long maxValue = (long) maxBValue * maxValueMultiply;
                long minValue = (long) minBValue * 500000;

                minPrice = String.valueOf(minValue);
                maxPrice = String.valueOf(maxValue);
                minSlideTxt.setText(Utils.getUnit(minPrice,SearchProjectsActivity.this));
                maxSlideTxt.setText(Utils.getUnit(maxPrice,SearchProjectsActivity.this));
            }
        });
        CustomRangeSeekBar areaRangeSeekBar = (CustomRangeSeekBar) findViewById(R.id.areaRangeSeekBar);
        areaRangeSeekBar.setNotifyWhileDragging(true);
        areaRangeSeekBar.setAdapter(new DemoRangeAdapter());
        areaRangeSeekBar.setOnRangeSeekBarChangeListener(new CustomRangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(CustomRangeSeekBar bar, int minPosition, int maxPosition) {
                minABValue = minPosition;
                maxABValue = maxPosition;

                Log.e("Values", "minBValue" + minBValue + " maxBValue" + maxBValue);
                int maxValueMultiply = 10;

                if (minPosition == maxPosition)
                    maxValueMultiply = 11;


                long maxValue = (long) maxABValue * maxValueMultiply;
                long minValue = (long) minABValue * 10;

                minAreaPrice = String.valueOf(minValue);
                maxAreaPrice = String.valueOf(maxValue);
                minSlideAreaTxt.setText(minAreaPrice + " "+defaultId);
                maxSlideAreaTxt.setText(maxAreaPrice + " "+defaultId);
            }
        });
        setcategoryTypeLayout(0);
        setUpperCatergory();
        if (!localityNameIntent.equalsIgnoreCase("")) {
            localityTxt.setText("Selected : " + getWordCounts("Region", localityNameIntent));
            selectedRegionsName = localityNameIntent;
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
        localityView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (regionName.size() == 0) {
                    Toast.makeText(SearchProjectsActivity.this, "No locality is available for this city", Toast.LENGTH_LONG).show();
                } else {
                    showLocalityReceiversDialog();
                }
            }
        });
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSearchResults();
            }
        });
    }


     public void bedrooms(){

         onebhk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                 if(onebhk.isChecked()){


//                     if (!numberOfBedrooms.equalsIgnoreCase(""))
//                         numberOfBedrooms=numberOfBedrooms+"1";
//                     else
                         numberOfBedrooms="1";
                 }
             }
         });
         twobhk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                 if(twobhk.isChecked()){
//                     if (!numberOfBedrooms.equalsIgnoreCase(""))
//                         numberOfBedrooms=numberOfBedrooms+"2";
//                     else
                         numberOfBedrooms="2";
                 }
             }
         });

         threebhk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                 if(threebhk.isChecked()){
//                     if (!numberOfBedrooms.equalsIgnoreCase(""))
//                         numberOfBedrooms=numberOfBedrooms+"3";
//                     else
                         numberOfBedrooms="3";
                 }
             }
         });
         fourbhk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                 if(fourbhk.isChecked()){
//                     if (!numberOfBedrooms.equalsIgnoreCase(""))
//                         numberOfBedrooms=numberOfBedrooms+"4";
//                     else
                         numberOfBedrooms="4";
                 }
             }
         });
         fivebhk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                 if(fivebhk.isChecked()){

//                     if (!numberOfBedrooms.equalsIgnoreCase(""))
//                         numberOfBedrooms=numberOfBedrooms+"5";
//                     else
                         numberOfBedrooms="5";
                 }
             }
         });




    }
   public void databaseIntilize(){


       categoryModel= CategoryTable.getInstance().getCategory();

       regionsModel = RegionTable.getInstance().getRegions("2");
       for (CommonDialogModel commonDialogModel : regionsModel) {
           regionName.add(commonDialogModel.getTitle());
       }
   }

    private void setcategoryTypeLayout(int Position) {


        categoryTypeModel = CategoryType.getInstance().getPropertyType(categoryModel.get(Position).getId());
        propertyTypeslayout.setVisibility(View.VISIBLE);
        propertyTypeGridVw.setExpanded(true);
        AdapterPropertyType adapter = new AdapterPropertyType(SearchProjectsActivity.this, categoryTypeModel, this);
        propertyTypeGridVw.setAdapter(adapter);


        adapter.notifyDataSetChanged();

    }

    private void setUpperCatergory() {

        categoriesLayout.setVisibility(View.VISIBLE);
        categoriesTypeGridVw.setExpanded(true);
        final AdapterCategoryType adapter = new AdapterCategoryType(SearchProjectsActivity.this, categoryModel, this);
        categoriesTypeGridVw.setAdapter(adapter);


        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        categoryModel=null;

    }

    @Override
    public void onPositionSelected(int position,String propertyName) {
        setcategoryTypeLayout(position);
        if(!propertyName.equalsIgnoreCase("Residential")){
            bedrooms_lay.setVisibility(View.GONE);
        }
        else{
            bedrooms_lay.setVisibility(View.VISIBLE);
        }




    }

    @Override
    public void onPropertySizeSelected(ArrayList<String> ids) {



    }

    @Override
    public void onPropertyTextSelected(String name) {
        for (CommonDialogModel categoryTypeModel1 : categoryTypeModel) {
            if (categoryTypeModel1.getTitle().equalsIgnoreCase(name)) {
                setDefaultSizes = UnitsTable.getInstance().getUnits();
                for (UnitsModel unitsModel : setDefaultSizes) {

                    if (String.valueOf(unitsModel.getUnitId()).equalsIgnoreCase(categoryTypeModel1.getDefaultUnitID())) {
                        defaultId = unitsModel.getUnitName();
                    }

                }

                minAreaPrice = categoryTypeModel1.getMinUnitRange();
                maxAreaPrice = categoryTypeModel1.getMaxUnitRange();
                minSlideAreaTxt.setText(minAreaPrice + " " + defaultId);
                maxSlideAreaTxt.setText(maxAreaPrice + " " + defaultId);

            }
        }
    }

    private void getSearchResults() {

        String urlCondition = "";


            urlCondition = "CityId eq " + city_id;

        if (!localityTxt.getText().toString().equalsIgnoreCase("Select Locality")) {

            if (urlCondition.equalsIgnoreCase(""))
                urlCondition = selectedRegionsName;
            else
                urlCondition = urlCondition + " and " + selectedRegionsName;

        }

//        if (!developerTxt.getText().toString().equalsIgnoreCase("Select Developer")) {
//
//            if (urlCondition.equalsIgnoreCase(""))
//                urlCondition = selectedDeveloperId;
//            else
//                urlCondition = urlCondition + " and " + selectedDeveloperId;
//
//        }


//        if (isProjectStatusCheck) {
//
//
//            urlCondition = urlCondition + " and ProjectStatus eq '" + SELECTED_STATUS+"'";
//
//        }

//        if (!propertySizes.equalsIgnoreCase(""))
//            if (urlCondition.equalsIgnoreCase(""))
//                urlCondition = propertySizes;
//            else
//                urlCondition = urlCondition + " and (" + propertySizes + ")";


        if (!minAreaPrice.equalsIgnoreCase("0") && !maxAreaPrice.equalsIgnoreCase("1000"))
            if (urlCondition.equalsIgnoreCase(""))
                urlCondition = " and PropertySize/any( size:  size/sizeValue ge " + minAreaPrice + " and size/sizeValue le " + maxAreaPrice;
            else
                urlCondition = urlCondition + " and PropertySize/any( size:  size/sizeValue ge " + minAreaPrice + " and size/sizeValue le " + maxAreaPrice;

        if (!minPrice.equalsIgnoreCase("500000") && !maxPrice.equalsIgnoreCase("50000000"))
            if (urlCondition.equalsIgnoreCase(""))
                urlCondition = " and PropertySize/any( size:  size/MinimumPrice ge " + minPrice + " and size/MaximumPrice le " + maxPrice;
            else
                urlCondition = urlCondition + " and PropertySize/any( size:  size/MinimumPrice ge " + minPrice + " and size/MaximumPrice le " + maxPrice;


        if (!numberOfBedrooms.equalsIgnoreCase(""))
            if (urlCondition.equalsIgnoreCase(""))
                urlCondition = "PropertySize/any( size:  size/BedRooms eq " + numberOfBedrooms + ")";
            else
                urlCondition = urlCondition + " and size/BedRooms eq " + numberOfBedrooms + ")";

        else if (urlCondition.contains("BedRooms") || urlCondition.contains("MinimumPrice"))
            urlCondition = urlCondition + " )";

        urlCondition = urlCondition.trim();


        new CommonAsync(SearchProjectsActivity.this,
                Urls.URL + (Urls.SEARCH_PROJECT) +   Constants.COMPAN_ID+"/"+ Constants.APP_ID  + "?$filter=" + urlCondition.replace(" ", "%20"),
                "",
                "Intializing..",
                this,
                Urls.SEARCH_PROJECT,
                Constants.GET).execute();
    }
    private int getWordCounts(String word, String sentence) {
        int i = 0;
        Pattern p = Pattern.compile(word);
        Matcher m = p.matcher(sentence);
        while (m.find()) {
            i++;
        }

        return i;
    }
    protected void showLocalityReceiversDialog() {
        boolean[] checkedReceivers = new boolean[regionName.size()];

        receivers = regionName.toArray(new String[regionName.size()]);
        int count = receivers.length;

        for (int i = 0; i < count; i++)
            checkedReceivers[i] = selectedRegions.contains(receivers[i]);

        DialogInterface.OnMultiChoiceClickListener receiversDialogListener = new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked)
                    selectedRegions.add(receivers[which]);
                else
                    selectedRegions.remove(receivers[which]);
            }
        };

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyleN);
        builder
                .setTitle("Select Items")
                .setMultiChoiceItems(receivers, checkedReceivers, receiversDialogListener)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                        StringBuilder stringBuilder = new StringBuilder();
                        StringBuilder regionsBuilder = new StringBuilder();
                        for (CharSequence receivers : selectedRegions) {
                            stringBuilder.append(receivers + ", ");
                            regionsBuilder.append("Region eq '" + receivers + "' or ");
                        }

                        if (selectedRegions.size() > 0)
                            localityTxt.setText("Selected : " + selectedRegions.size());
                        else
                            localityTxt.setText("Select Locality");

                        Log.e("Selected Regions", regionsBuilder.toString());


                        if (regionsBuilder.toString().length() > 0) {
                            selectedRegionsName = "(" + regionsBuilder.toString().substring(0, regionsBuilder.toString().length() - 4) + ")";
                        } else
                            selectedRegionsName = "";
                    }
                });
        android.support.v7.app.AlertDialog dialog = builder.create();

        dialog.show();
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.SEARCH_PROJECT)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        if (projectsArray.length() == 0) {

                            Toast.makeText(this, "No result found!!!", Toast.LENGTH_LONG).show();

                        }

                        projectsModels.clear();
                        for (int index = 0; index < projectsArray.length(); index++) {
                            ProjectsModel projectsModel = new ProjectsModel();

                            projectsModel.setId(projectsArray.getJSONObject(index).getString("Id"));
                            projectsModel.setTitle(projectsArray.getJSONObject(index).getString("title"));
                            projectsModel.setLocality(projectsArray.getJSONObject(index).getString("Locality"));
                            projectsModel.setUpdatedOn(projectsArray.getJSONObject(index).getString("UpdatedOn"));
                            projectsModel.setArea(projectsArray.getJSONObject(index).getString("Area"));
                            projectsModel.setNumberOfTowers(projectsArray.getJSONObject(index).getString("NumberOfTowers"));
                            projectsModel.setAboutSubProject(projectsArray.getJSONObject(index).getString("aboutSubProject"));
                            projectsModel.setPossessionMonth(projectsArray.getJSONObject(index).getString("PossessionMonth"));
                            projectsModel.setPossessionYear(projectsArray.getJSONObject(index).getString("PossessionYear"));

                            projectsModel.setProjectStatus(projectsArray.getJSONObject(index).getString("ProjectStatus"));
                            projectsModel.setRegion(projectsArray.getJSONObject(index).getString("Region"));
                            projectsModel.setImagePath(projectsArray.getJSONObject(index).getString("logoimage"));
                            projectsModel.setTopImage1(projectsArray.getJSONObject(index).getString("topImage1"));
                            projectsModel.setTypeOfProject(projectsArray.getJSONObject(index).getString("TypeOfProject"));
                            projectsModel.setCompanyLogo(projectsArray.getJSONObject(index).getString("CompanyLogo"));
                            projectsModel.setCityName(projectsArray.getJSONObject(index).getString("CityName"));
                            projectsModel.setBuilderCompanyName(projectsArray.getJSONObject(index).getString("builderCompanyName"));


                            JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("PropertySize");
                            ArrayList<ProjectsModel.PropertySize> propertySizes = new ArrayList();


                            for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                                ProjectsModel.PropertySize propertySize = projectsModel.new PropertySize();
                                JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                                propertySize.setId(dataObj.getString("Id"));
                                propertySize.setTitle(dataObj.getString("title"));
                                propertySize.setApplicationForm(dataObj.getString("applicationForm"));
                                propertySize.setProperty_typeIdlevel1(dataObj.getString("property_typeIdlevel1"));
                                propertySize.setProperty_typeIdlevel2(dataObj.getString("property_typeIdlevel2"));
                                propertySize.setProperty_typeIdlevel3(dataObj.getString("property_typeIdlevel3"));
                                propertySize.setFloorPlan(dataObj.getString("floorPlan"));
                                propertySize.setSize(dataObj.getString("size"));
                                propertySize.setSizeUnit(dataObj.getString("sizeUnit"));
                                propertySize.setBuiltarea(dataObj.getString("builtarea"));
                                propertySize.setCarpetarea(dataObj.getString("carpetarea"));
                                propertySize.setSizeUnit2(dataObj.getString("sizeUnit2"));
                                propertySize.setAboutProperty(dataObj.getString("AboutProperty"));
                                propertySize.setLength(dataObj.getString("Length"));
                                propertySize.setBreadth(dataObj.getString("Breadth"));
                                propertySize.setSizeUnit3(dataObj.getString("SizeUnit3"));
                                propertySize.setExtraArea(dataObj.getString("ExtraArea"));
                                propertySize.setMinimumPrice(dataObj.getString("MinimumPrice"));
                                propertySize.setMaximumPrice(dataObj.getString("MaximumPrice"));
                                propertySize.setBathRooms(dataObj.getString("BathRooms"));
                                propertySize.setBedRooms(dataObj.getString("BedRooms"));
                                propertySize.setTowerId(dataObj.getString("TowerId"));
                                propertySize.setIsCostAppliedCarpetArea(dataObj.getString("isCostAppliedCarpetArea"));
                                propertySize.setExtraAreaLabel(dataObj.getString("ExtraAreaLabel"));
                                propertySize.setDiscounts(dataObj.getString("Discounts"));
                                propertySize.setLayoutPlans(dataObj.getString("LayoutPlans"));
                                propertySize.setDiscountGroups(dataObj.getString("DiscountGroups"));
                                propertySize.setTowers(dataObj.getString("Towers"));

                                propertySizes.add(propertySize);


                            }


                            JSONArray projectRelationshipManagerses = projectsArray.getJSONObject(index).getJSONArray("BrokerStaff");
                            ArrayList<ProjectsModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList();
                            if (projectRelationshipManagerses.length() != 0) {
                                for (int indexJ = 0; indexJ < projectRelationshipManagerses.length(); indexJ++) {

                                    ProjectsModel.ProjectRelationshipManagers projectRelationshipManagers = projectsModel.new ProjectRelationshipManagers();
                                    JSONObject dataObj = projectRelationshipManagerses.getJSONObject(indexJ);
                                    projectRelationshipManagers.setId(dataObj.getString("Id"));
                                    projectRelationshipManagers.setName(dataObj.getString("Name"));
                                    projectRelationshipManagers.setEmail(dataObj.getString("Email"));
                                    projectRelationshipManagers.setPhoto(dataObj.getString("Photo"));
                                    projectRelationshipManagers.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                    projectRelationshipManagers.setDesignation(dataObj.getString("Designation"));

                                    projectRelationshipManagersArrayList.add(projectRelationshipManagers);
                                }
                            }

                            JSONArray towersJson = projectsArray.getJSONObject(index).getJSONArray("Towers");
                            ArrayList<ProjectsModel.Towers> towerses = new ArrayList();
                            if (towersJson.length() != 0) {
                                for (int indexJ = 0; indexJ < towersJson.length(); indexJ++) {

                                    ProjectsModel.Towers towersProjectModel = projectsModel.new Towers();
                                    JSONObject dataObj = towersJson.getJSONObject(indexJ);
                                    towersProjectModel.setTowerId(dataObj.getString("TowerId"));
                                    towersProjectModel.setBasement(dataObj.getString("Basement"));
                                    towersProjectModel.setFloorType(dataObj.getString("floorType"));
                                    towersProjectModel.setProjectCategoryId(dataObj.getString("ProjectCategoryId"));
                                    towersProjectModel.setPropertyTypeId(dataObj.getString("PropertyTypeId"));
                                    towersProjectModel.setStories(dataObj.getString("Stories"));
                                    towersProjectModel.setTowerName(dataObj.getString("TowerName"));


                                    towerses.add(towersProjectModel);
                                }
                            }

                            projectsModel.setPropertySizes(propertySizes);
                            projectsModel.setProjectRelationshipManagerses(projectRelationshipManagersArrayList);
                            projectsModel.setTowerses(towerses);
                            projectsModels.add(projectsModel);


                        }


                        Intent intent=new Intent();
                        intent.putExtra("Array_list",projectsModels);
                        setResult(RESULT_OK,intent);
                        finish();

//                        if (projectsModels.size() > 0) {
//                            Intent intent = new Intent(SearchActivity.this, ProjectListingActivity.class);
//                            intent.putExtra(Constants.PROJECTS_LIST, projectsModels);
//
//
//                            intent.putExtra(Constants.CITY_NAME, cityTxt.getText().toString());
//                            intent.putExtra(Constants.CITY_ID, cityId);
//                            intent.putExtra(Constants.LOCALITY_NAME, selectedRegionsName);
//                            intent.putExtra(Constants.MIN_BUDGET, Integer.parseInt(minPrice));
//                            intent.putExtra(Constants.MAX_BUDGET, Integer.parseInt(maxPrice));
//                            intent.putExtra(Constants.SELECTED_PROPERTY, "");
//                            intent.putExtra(Constants.PROPERTY_SIZES, propertySizes);
//                            intent.putExtra(Constants.PROJECT_STATUS, "");
//                            intent.putExtra(Constants.DEVELOPER_ID, "");
//                            intent.putExtra(Constants.MIN_AREA, Integer.parseInt(minAreaPrice));
//                            intent.putExtra(Constants.MAX_AREA, Integer.parseInt(maxAreaPrice));
//
//
//                            startActivityForResult(intent, 4125);
//
//                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Error : No result found!!!", Toast.LENGTH_LONG).show();


                }

            }
        } else {

            Toast.makeText(this, "Error : No result found!!!", Toast.LENGTH_LONG).show();

        }
    }
}
