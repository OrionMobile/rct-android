package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.utils.Constants;

public class SelectionPropertyTypeActivity extends AppCompatActivity {
    LinearLayout residential_lay, commercial_lay, industrial_lay, agriculture_lay, institutional_lay;
    ImageView back_btn;
//    TextView residential,commercial;
    Button btnmyprop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_property_type);
        residential_lay=(LinearLayout)findViewById(R.id.residential_lay);
        commercial_lay=(LinearLayout)findViewById(R.id.commercial_lay);
        industrial_lay=(LinearLayout)findViewById(R.id.industrial_lay);
        agriculture_lay=(LinearLayout)findViewById(R.id.agriculture_lay);
        institutional_lay=(LinearLayout)findViewById(R.id.institutional_lay);
        back_btn=(ImageView) findViewById(R.id.back_btn);
        btnmyprop = (Button) findViewById(R.id.btnmyprop);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        residential_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SelectionPropertyTypeActivity.this,SelectPropertyActivity.class);
                intent.putExtra(Constants.FROM,"Residential");

                startActivity(intent);
               // finish();
            }
        });
        commercial_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SelectionPropertyTypeActivity.this,SelectPropertyActivity.class);
                intent.putExtra(Constants.FROM,"Commercial");

                startActivity(intent);
               // finish();
            }
        });
        industrial_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SelectionPropertyTypeActivity.this, "Industrial Property Type Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });
        agriculture_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SelectionPropertyTypeActivity.this, "Agricultural Property Type Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });
        institutional_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SelectionPropertyTypeActivity.this, "Institutional Property Type Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });


        btnmyprop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SelectionPropertyTypeActivity.this, MyPropertyActivity.class);
                intent.putExtra("from_savedrafts","from_savedrafts");
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
