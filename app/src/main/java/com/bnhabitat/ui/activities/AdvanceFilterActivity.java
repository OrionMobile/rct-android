package com.bnhabitat.ui.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.models.TaxonomyTermModel;
import com.bnhabitat.ui.adapters.AdvanceCommonAdapter;
import com.bnhabitat.ui.adapters.CommonApdater;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class AdvanceFilterActivity extends AppCompatActivity implements View.OnClickListener, CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener {
    LinearLayout industry_lay, designation_lay, annual_income_lay, source_lay, company_lay, tag_lay, profession_lay, religion_lay;
    ArrayList<CommonDialogModel> religion = new ArrayList<>();
    ArrayList<CommonDialogModel> profession = new ArrayList<>();
    ArrayList<CommonDialogModel> tags = new ArrayList<>();
    ArrayList<CommonDialogModel> company = new ArrayList<>();
    ArrayList<CommonDialogModel> source = new ArrayList<>();
    ArrayList<CommonDialogModel> annual_income = new ArrayList<>();
    ArrayList<CommonDialogModel> designation_list = new ArrayList<>();
    ListView listView;
    ArrayList<CommonDialogModel> industry = new ArrayList<>();
    AdvanceCommonAdapter adapterCommonDialog;
    int from;
    String id_string;
    ArrayList<ContactsModel> contactsFirebaseModels = new ArrayList<>();
    public static ArrayList<ContactsModel> contactsFirebaseModelsNew = new ArrayList<>();

    StringBuilder stringBuilder;
    ArrayList<String> id = new ArrayList<>();
    TextView religion_text, profession_txt, source_txt, annual_income_txt, company_txt, industry_txt, tag_txt, designation_txt;
    private ArrayList<TaxonomyTermModel> taxonomyTermModels = new ArrayList();
    public ArrayList<Boolean> checkedItemsListed = new ArrayList<>();
    Button show_result;
    ImageView add_contact, back;
    int top = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advance_filter);
        intializeView();
    }

    public void intializeView() {

        industry_lay = (LinearLayout) findViewById(R.id.industry_lay);
        designation_lay = (LinearLayout) findViewById(R.id.designation_lay);
        annual_income_lay = (LinearLayout) findViewById(R.id.annual_income_lay);
        source_lay = (LinearLayout) findViewById(R.id.source_lay);
        company_lay = (LinearLayout) findViewById(R.id.company_lay);
        tag_lay = (LinearLayout) findViewById(R.id.tag_lay);
        profession_lay = (LinearLayout) findViewById(R.id.profession_lay);
        religion_lay = (LinearLayout) findViewById(R.id.religion_lay);
        religion_text = (TextView) findViewById(R.id.religion);
        profession_txt = (TextView) findViewById(R.id.profession);
        source_txt = (TextView) findViewById(R.id.source);
        annual_income_txt = (TextView) findViewById(R.id.annual_income);
        company_txt = (TextView) findViewById(R.id.company);
        designation_txt = (TextView) findViewById(R.id.designation);
        industry_txt = (TextView) findViewById(R.id.industry);
        show_result = (Button) findViewById(R.id.show_result);
        tag_txt = (TextView) findViewById(R.id.tag);
        add_contact = (ImageView) findViewById(R.id.add_contact);
        back = (ImageView) findViewById(R.id.back);
        industry_lay.setOnClickListener(this);
        designation_lay.setOnClickListener(this);
        annual_income_lay.setOnClickListener(this);
        source_lay.setOnClickListener(this);
        company_lay.setOnClickListener(this);
        tag_lay.setOnClickListener(this);
        profession_lay.setOnClickListener(this);
        religion_lay.setOnClickListener(this);
        show_result.setOnClickListener(this);
        add_contact.setOnClickListener(this);
        back.setOnClickListener(this);
        onGetRequest();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.industry_lay:
                showCommonDialog(this, industry, 8, 0);
                break;
            case R.id.designation_lay:
                showCommonDialog(this, designation_list, 7, 0);
                break;
            case R.id.annual_income_lay:
                showCommonDialog(this, annual_income, 6, 0);
                break;
            case R.id.company_lay:
                showCommonDialog(this, company, 4, 0);
                break;
            case R.id.tag_lay:
                showCommonDialog(this, tags, 3, 0);
                break;
            case R.id.profession_lay:
                showCommonDialog(this, profession, 2, 0);
                break;
            case R.id.religion_lay:
                showCommonDialog(this, religion, 1, 0);
                break;
            case R.id.source_lay:
                showCommonDialog(this, source, 5, 0);
                break;
            case R.id.show_result:
                onSyncRequest();
                break;
            case R.id.add_contact:
                Intent intent = new Intent(AdvanceFilterActivity.this, NewAddContactActivity.class);
                startActivity(intent);
                break;
            case R.id.back:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(ArrayList<StatusModel> statusModels) {
        stringBuilder = new StringBuilder();


        for (int i = 0; i < statusModels.size(); i++) {

            stringBuilder.append(statusModels.get(i).getName() + ",");
            String str = stringBuilder.substring(0, stringBuilder.length() - 1);

            if (statusModels.get(i).getPos() == 1) {
                religion_text.setText(str);
                id.add("ContactTerms/any(data:data/TermId+eq " + statusModels.get(i).getId() + ") or ");
            } else if (statusModels.get(i).getPos() == 2) {
                profession_txt.setText(str);
                id.add("ContactTerms/any(data:data/TermId+eq " + statusModels.get(i).getId() + ") or ");
            } else if (statusModels.get(i).getPos() == 5) {
                source_txt.setText(str);
                id.add("ContactTerms/any(data:data/TermId+eq " + statusModels.get(i).getId() + ") or ");
            } else if (statusModels.get(i).getPos() == 4) {
                company_txt.setText(str);
                id.add("ContactTerms/any(data:data/TermId+eq " + statusModels.get(i).getId() + ") or ");
            } else if (statusModels.get(i).getPos() == 3) {
                tag_txt.setText(str);
                id.add("ContactTerms/any(data:data/TermId+eq " + statusModels.get(i).getId() + ") or ");
            } else if (statusModels.get(i).getPos() == 6) {
                annual_income_txt.setText(str);
                id.add("ContactTerms/any(data:data/TermId+eq " + statusModels.get(i).getId() + ") or ");
            } else if (statusModels.get(i).getPos() == 7) {
                designation_txt.setText(str);
                id.add("ContactTerms/any(data:data/TermId+eq " + statusModels.get(i).getId() + ") or ");
            } else if (statusModels.get(i).getPos() == 8) {
                industry_txt.setText(str);
                id.add("ContactTerms/any(data:data/TermId+eq " + statusModels.get(i).getId() + ") or ");
            }
            String remove_braces = id.toString().replace("[", "").replace("]", "").replace(",", "");
            id_string = "(" + remove_braces.substring(0, remove_braces.length() - 3) + " )";

//            for (String termString : id) {
//                sb.append(termString);
//                Log.e("id_length", "id_length" + termString.toString());
//            }
//            Log.e("id_length", "id_length" + id.toString().substring(0,id.lastIndexOf(2)));
//        Log.e("stringBuilder","stringBuilder"+statusModels.get(i).getId()+"Name"+statusModels.get(i).getName());
        }

//    String str = stringBuilder.substring(0, stringBuilder.length() - 1);

    }

    private void onGetRequest() {

//
        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_TERM_TAXONOMY + "1/" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,
                ""), "", "Loading...",
                this, Urls.CONTACT_MANAGEMENT_URL,
                Constants.GET).execute();
//        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_TERM_TAXONOMY + Constants.COMPAN_ID+"/"+ PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, ""), "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();

//
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.CONTACT_MANAGEMENT_URL)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray projectsArray = jsonObject.getJSONArray("Result");
                        for (int index = 0; index < projectsArray.length(); index++) {

                            TaxonomyTermModel taxonomyTermModel = new TaxonomyTermModel();
                            taxonomyTermModel.setName(projectsArray.getJSONObject(index).getString("Name"));
                            JSONArray terms = projectsArray.getJSONObject(index).getJSONArray("Terms");
                            ArrayList<TaxonomyTermModel.TermData> termDataArrayList = new ArrayList();
                            for (int indexJ = 0; indexJ < terms.length(); indexJ++) {

                                TaxonomyTermModel.TermData termData = taxonomyTermModel.new TermData();

                                JSONObject dataObj = terms.getJSONObject(indexJ);

                                termData.setId(dataObj.getString("Id"));
                                termData.setName(dataObj.getString("Name"));
                                termDataArrayList.add(termData);
                            }
                            taxonomyTermModel.setTerms(termDataArrayList);
                            taxonomyTermModels.add(taxonomyTermModel);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_ALL_CONTACTS)) {
                contactsFirebaseModels.clear();
                try {


                    JSONArray projectsArray = new JSONArray(result);

//                    if (projectsArray.length() == 0) {
//                        contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
//
////                        Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    }
//                    else {


                    for (int index = 0; index < projectsArray.length(); index++) {
                        JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                        ContactsModel contactsModel = new ContactsModel();
                        contactsModel.setId(jsonObject1.getString("Id"));
                        contactsModel.setFirstName(jsonObject1.getString("FirstName"));
                        contactsModel.setLastName(jsonObject1.getString("LastName"));
                        contactsModel.setDateOfBirthStr(jsonObject1.getString("DateOfBirthStr"));


                        JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("Addresses");
                        ArrayList<AddressModel> addresses = new ArrayList();
                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                            AddressModel addresses1 = new AddressModel();
                            JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                            addresses1.setId(dataObj.getString("Id"));
                            addresses1.setContactId(dataObj.getString("ContactId"));
                            addresses1.setStateName(dataObj.getString("StateName"));
                            addresses1.setCountryName(dataObj.getString("CountryName"));
                            addresses1.setProjectName(dataObj.getString("ProjectName"));

                            addresses1.setCity_name(dataObj.getString("CityName"));
                            addresses1.setAddress1(dataObj.getString("Address1"));
                            addresses1.setAddress2(dataObj.getString("Address2"));
                            addresses1.setAddress3(dataObj.getString("Address3"));
                            addresses1.setDeveloper_name(dataObj.getString("DeveloperName"));
                            addresses1.setTower(dataObj.getString("Tower"));
                            addresses1.setFloor(dataObj.getString("Floor"));
                            addresses1.setUnitName(dataObj.getString("UnitNo"));
                            addresses1.setZipcode(dataObj.getString("ZipCode"));

                            addresses.add(addresses1);
                        }

                        JSONArray phonesJsonArray = projectsArray.getJSONObject(index).getJSONArray("Phones");
                        ArrayList<ContactsModel.Phones> phones = new ArrayList();
                        for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                            ContactsModel.Phones phones1 = contactsModel.new Phones();
                            JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                            phones1.setId(dataObj.getString("Id"));
                            phones1.setContactId(dataObj.getString("ContactId"));
                            phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
                         //   phones1.setPhoneType(dataObj.getString("Mode"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                            phones.add(phones1);
                        }
                        JSONArray emailJsonArray = projectsArray.getJSONObject(index).getJSONArray("EmailAddresses");
                        ArrayList<ContactsModel.Email> emails = new ArrayList();
                        for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                            ContactsModel.Email email = contactsModel.new Email();
                            JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                            email.setId(dataObj.getString("Id"));
                            email.setContactId(dataObj.getString("ContactId"));
                            email.setAddress(dataObj.getString("Address"));
                            email.setLabel(dataObj.getString("Label"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                            emails.add(email);
                        }


                        JSONArray contactFileJsonArray = projectsArray.getJSONObject(index).getJSONArray("ContactFiles");
                        ArrayList<ContactsModel.ContactFiles> contactFiles = new ArrayList();
                        for (int indexJ = 0; indexJ < contactFileJsonArray.length(); indexJ++) {

                            ContactsModel.ContactFiles contactFiles1 = contactsModel.new ContactFiles();
                            JSONObject dataObj = contactFileJsonArray.getJSONObject(indexJ);

                            contactFiles1.setId(dataObj.getString("Id"));
                            contactFiles1.setContactId(dataObj.getString("ContactId"));
                            contactFiles1.setName(dataObj.getString("Name"));
                            contactFiles1.setType(dataObj.getString("Type"));
                            contactFiles1.setPath(dataObj.getString("Path"));
                            contactFiles1.setCompanyId(dataObj.getString("CompanyId"));


                            contactFiles.add(contactFiles1);
                        }

                        contactsModel.setAddresses(addresses);
                        contactsModel.setPhones(phones);
                        contactsModel.setContactFiles(contactFiles);
                        contactsModel.setEmails(emails);
//                      contactsModel.setGroupses(groupses);

                        contactsFirebaseModels.add(contactsModel);

                       // Toast.makeText(this, "Folterrr", Toast.LENGTH_SHORT).show();
                    }


                    contactsFirebaseModelsNew = contactsFirebaseModels;
                    finish();

                } catch (JSONException e) {

                    e.printStackTrace();

                }


            }
        }
    }

    public void showCommonDialog(Context context,
                                 final ArrayList<CommonDialogModel> commonDialogModels,
                                 final int from, final int clickedPosition) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.check_dialog);
        dialog.setCancelable(true);



        listView = (ListView) dialog.findViewById(R.id.listView);
        Button registerBtn = (Button) dialog.findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();

            }
        });

        religion.clear();
        profession.clear();
        annual_income.clear();
        designation_list.clear();
        industry.clear();
        tags.clear();
        source.clear();
        company.clear();
        for (int i = 0; i < taxonomyTermModels.size(); i++) {
            if (from == 1) {
                if (taxonomyTermModels.get(i).getName().equalsIgnoreCase("Religion")) {
                    for (int k = 0; k < taxonomyTermModels.get(i).getTerms().size(); k++) {
                        CommonDialogModel commonDialogModel = new CommonDialogModel();

                        commonDialogModel.setId(taxonomyTermModels.get(i).getTerms().get(k).getId());
                        commonDialogModel.setTitle(taxonomyTermModels.get(i).getTerms().get(k).getName());
                        if(!taxonomyTermModels.get(i).getTerms().get(k).getName().equalsIgnoreCase("null"))
                        commonDialogModels.add(commonDialogModel);
                    }
                }
            } else if (from == 2) {

                if (taxonomyTermModels.get(i).getName().equalsIgnoreCase("Profession")) {
                    for (int k = 0; k < taxonomyTermModels.get(i).getTerms().size(); k++) {
                        CommonDialogModel commonDialogModel = new CommonDialogModel();

                        commonDialogModel.setId(taxonomyTermModels.get(i).getTerms().get(k).getId());
                        commonDialogModel.setTitle(taxonomyTermModels.get(i).getTerms().get(k).getName());
                        if(!taxonomyTermModels.get(i).getTerms().get(k).getName().equalsIgnoreCase("null"))
                        commonDialogModels.add(commonDialogModel);
                    }
                }
            } else if (from == 3) {

                if (taxonomyTermModels.get(i).getName().equalsIgnoreCase("Tags")) {
                    for (int k = 0; k < taxonomyTermModels.get(i).getTerms().size(); k++) {
                        CommonDialogModel commonDialogModel = new CommonDialogModel();

                        commonDialogModel.setId(taxonomyTermModels.get(i).getTerms().get(k).getId());
                        commonDialogModel.setTitle(taxonomyTermModels.get(i).getTerms().get(k).getName());
                        if(!taxonomyTermModels.get(i).getTerms().get(k).getName().equalsIgnoreCase("null"))
                        commonDialogModels.add(commonDialogModel);
                    }
                }
            } else if (from == 4) {

                if (taxonomyTermModels.get(i).getName().equalsIgnoreCase("Company")) {
                    for (int k = 0; k < taxonomyTermModels.get(i).getTerms().size(); k++) {
                        CommonDialogModel commonDialogModel = new CommonDialogModel();

                        commonDialogModel.setId(taxonomyTermModels.get(i).getTerms().get(k).getId());
                        commonDialogModel.setTitle(taxonomyTermModels.get(i).getTerms().get(k).getName());
                        if(!taxonomyTermModels.get(i).getTerms().get(k).getName().equalsIgnoreCase("null"))
                        commonDialogModels.add(commonDialogModel);
                    }
                }
            } else if (from == 5) {

                if (taxonomyTermModels.get(i).getName().equalsIgnoreCase("Source")) {
                    for (int k = 0; k < taxonomyTermModels.get(i).getTerms().size(); k++) {

                        CommonDialogModel commonDialogModel = new CommonDialogModel();

                        commonDialogModel.setId(taxonomyTermModels.get(i).getTerms().get(k).getId());
                        commonDialogModel.setTitle(taxonomyTermModels.get(i).getTerms().get(k).getName());
                        if(!taxonomyTermModels.get(i).getTerms().get(k).getName().equalsIgnoreCase("null"))
                        commonDialogModels.add(commonDialogModel);
                    }
                }
            } else if (from == 6) {

                if (taxonomyTermModels.get(i).getName().equalsIgnoreCase("AnnualIncome")) {
                    for (int k = 0; k < taxonomyTermModels.get(i).getTerms().size(); k++) {
                        CommonDialogModel commonDialogModel = new CommonDialogModel();

                        commonDialogModel.setId(taxonomyTermModels.get(i).getTerms().get(k).getId());
                        commonDialogModel.setTitle(taxonomyTermModels.get(i).getTerms().get(k).getName());
                        if(!taxonomyTermModels.get(i).getTerms().get(k).getName().equalsIgnoreCase("null"))
                        commonDialogModels.add(commonDialogModel);
                    }
                }
            } else if (from == 7) {

                if (taxonomyTermModels.get(i).getName().equalsIgnoreCase("Designation")) {
                    for (int k = 0; k < taxonomyTermModels.get(i).getTerms().size(); k++) {
                        CommonDialogModel commonDialogModel = new CommonDialogModel();

                        commonDialogModel.setId(taxonomyTermModels.get(i).getTerms().get(k).getId());
                        commonDialogModel.setTitle(taxonomyTermModels.get(i).getTerms().get(k).getName());
                        if(!taxonomyTermModels.get(i).getTerms().get(k).getName().equalsIgnoreCase("null"))
                        commonDialogModels.add(commonDialogModel);
                    }
                }
            } else if (from == 8) {

                if (taxonomyTermModels.get(i).getName().equalsIgnoreCase("Industry")) {
                    for (int k = 0; k < taxonomyTermModels.get(i).getTerms().size(); k++) {
                        CommonDialogModel commonDialogModel = new CommonDialogModel();

                        commonDialogModel.setId(taxonomyTermModels.get(i).getTerms().get(k).getId());
                        commonDialogModel.setTitle(taxonomyTermModels.get(i).getTerms().get(k).getName());
                        if(!taxonomyTermModels.get(i).getTerms().get(k).getName().equalsIgnoreCase("null"))
                        commonDialogModels.add(commonDialogModel);
                    }
                }
            }

        }
        if(commonDialogModels.size()!=0){
            dialog.show();
            adapterCommonDialog = new AdvanceCommonAdapter(this,
                    commonDialogModels, from);
            listView.setAdapter(adapterCommonDialog);
        }
        else{
            Toast.makeText(AdvanceFilterActivity.this, "No item found", Toast.LENGTH_SHORT).show();
        }



//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if (from == 1) {
//
//                    religion_text.setText(commonDialogModels.get(i).getTitle());
//                } else if (from == 2) {
//                    profession_txt.setText(commonDialogModels.get(i).getTitle());
//
//
//                } else if (from == 3) {
//                    tag_txt.setText(commonDialogModels.get(i).getTitle());
//
//                } else if (from == 4) {
//
//                    company_txt.setText(commonDialogModels.get(i).getTitle());
//                } else if (from == 5) {
//                    source_txt.setText(commonDialogModels.get(i).getTitle());
//
//                } else if (from == 6) {
//
//                    annual_income_txt.setText(commonDialogModels.get(i).getTitle());
//                } else if (from == 7) {
//
//                    designation_txt.setText(commonDialogModels.get(i).getTitle());
//                } else if (from == 8) {
//
//                    industry_txt.setText(commonDialogModels.get(i).getTitle());
//                }
//                dialog.cancel();
//            }
//        });


    }

    private void onSyncRequest() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");
//        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "")+ "/"+ Constants.APP_ID+"?$orderby=UpdatedOn+desc&$filter=IsDeleted+eq+false&UpdatedOn+gt+datetime'2017-11-17T00:00", "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();

        if (id_string == null) {
            top = top + 100;

            new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") + "/" + Constants.APP_ID + "?$orderby=UpdatedOn+desc&$skip=0&$top=" + top + "&$filter=IsDeleted+eq+false&UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00"), "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();


            // new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "")+ "/"+ Constants.APP_ID+"?$orderby=UpdatedOn+desc&$skip=0&$top=100"+"&$filter=IsDeleted+eq+false&UpdatedOn+gt+datetime'"+PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00"), "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();

        } else {
            new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") + "/" + Constants.APP_ID + "?$orderby=UpdatedOn+desc&$skip=0&$top=100" + "&$filter=IsDeleted+eq+false&UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + URLEncoder.encode(id_string), "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();

        }

//        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + "24/63?$filter=UpdatedOn+gt+datetime'2017-11-17T00:00"  + "'&IsDeleted+eq+'false'&skip=0&top=2"+ URLEncoder.encode(id_string), "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();
//        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+ Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted+eq+false&"+URLEncoder.encode(id_string), "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
