package com.bnhabitat.ui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.NewGroupItemsModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class NewGroup extends AppCompatActivity implements CommonAsync.OnAsyncResultListener,CommonSyncwithoutstatus.OnAsyncResultListener {
    private RecyclerView recycler_view;
    LinearLayoutManager linearLayoutManager;
    Button create_new_group, done_button;
    ImageView create_group;
    ImageView back_button;
    StringBuilder sb;
    String  id="",contact_ids="";
        private ArrayList<Boolean> contactListsClicked;
//    private DatabaseReference myRef;
//    private DatabaseReference myRef1;
//    private FirebaseDatabase database;
    ArrayList<NewGroupItemsModel> arrayList = new ArrayList<>();
    ArrayList<String> grouparraylist = new ArrayList<>();
    ArrayList<NewGroupItemsModel> arrayListgroupnames = new ArrayList<>();
    ArrayList<ContactsModel> contactsFirebaseModels=new ArrayList<>();
     EditText userInput;
    String selecteditem="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);
        create_new_group = (Button) findViewById(R.id.create_new_group);
        done_button = (Button) findViewById(R.id.done_button);
        back_button=(ImageView) findViewById(R.id.back_button);


        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(this);

//        Log.e("group", getIntent().getSerializableExtra("GroupList").toString());
        contactsFirebaseModels = (ArrayList<ContactsModel>) getIntent().getSerializableExtra(Constants.ARRAY_LIST);
//        arrayListgroupnames= (ArrayList<NewGroupItemsModel>) getIntent().getSerializableExtra(Constants.GROUP_NAMES);
//        Log.e("groupnames", arrayListgroupnames.toString());

        onSyncRequest();
        sb=new StringBuilder();
        for(int index=0;index<contactsFirebaseModels.size();index++) {
            id=contactsFirebaseModels.get(index).getId()+",";
            sb.append(id);
        }
        contact_ids = sb.substring(0, sb.length() - 1);
        done_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!selecteditem.equalsIgnoreCase("")){

                    onPostContacts();
                }else{
                    Toast.makeText(NewGroup.this, "Please select atleast one Group", Toast.LENGTH_SHORT).show();

                }



            }
        });
        create_new_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater li = LayoutInflater.from(NewGroup.this);
                View promptsView = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        NewGroup.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                 userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Submit",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        // edit text
                                        new SweetAlertDialog(NewGroup.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setContentText("New Group is Created succesfully!")
                                                .setConfirmText("OK") .setTitleText("Group Created")

                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        if(userInput.getText().toString().equalsIgnoreCase("")){
                                                            Toast.makeText(NewGroup.this, "Please enter group name", Toast.LENGTH_SHORT).show();
                                                        }else {
                                                            onGroupAdd();
                                                        }


                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();


                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();

                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();


            }
        });
    }

    int selectedPosition = -1;
    private void onGroupAdd() {

        new CommonAsync(this,  Urls.CONTACT_MANAGEMENT_URL+ Urls.URL_GROUP_ADD,
                getGroupInputs(),
                "loading...",
                this,
                Urls.URL_GROUP_ADD,
                Constants.POST).execute();
    }

    private String getGroupInputs() {
        String inputJson = "";


        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name",  userInput .getText().toString());
            jsonObject.put("Description", "");
            jsonObject.put("Slug", "1");
            jsonObject.put("TaxonomyId", "1");
            jsonObject.put("CompanyId", "1");
            inputJson=jsonObject.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return inputJson;
    }


    private String getLoginInputJson() {

        String inputJson = "";


        try {


                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("GroupIds",selecteditem );
                jsonObject1.put("ContactIds",contact_ids);


//                jsonArray.put(jsonObject1);



            inputJson = jsonObject1.toString();
        } catch (Exception e) {

        }
        return inputJson;
    }


    private void onPostContacts() {

        new CommonSyncwithoutstatus(this,  Urls.CONTACT_MANAGEMENT_URL+ Urls.URL_POST_CONTACTS_GROUP,
                getLoginInputJson(),
                "loading...",
                this,
                Urls.URL_POST_CONTACTS_GROUP,
                Constants.POST).execute();
    }

    private void onSyncRequest() {

        new CommonAsync(NewGroup.this,  Urls.CONTACT_MANAGEMENT_URL+ Urls.URL_GROUP_LISTING+ PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""),
                "",
                "Loading..",
                NewGroup.this,
                Urls.URL_GROUP_LISTING,
                Constants.GET).execute();
    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_GROUP_LISTING)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        if (projectsArray.length() == 0) {
                            Toast.makeText(NewGroup.this, "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                        }
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            NewGroupItemsModel newGroupItemsModel=new NewGroupItemsModel();

                            newGroupItemsModel.setId(jsonObject1.getString("Id"));
                            newGroupItemsModel.setName(jsonObject1.getString("GroupName"));
                            newGroupItemsModel.setDescription(jsonObject1.getString("Description"));
//                            newGroupItemsModel.setSlug(jsonObject1.getString("Slug"));
//                            newGroupItemsModel.setTaxonomyId(jsonObject1.getString("TaxonomyId"));

                            arrayListgroupnames.add(newGroupItemsModel);

                        }
                        NewGroupAdapter createNewGroupAdapter = new NewGroupAdapter(arrayListgroupnames, NewGroup.this);

                        recycler_view.setLayoutManager(linearLayoutManager);
                        recycler_view.setAdapter(createNewGroupAdapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            else if(which.equalsIgnoreCase(Urls.URL_POST_CONTACTS_GROUP)){
                PreferenceConnector.getInstance(this).savePreferences(Constants.DATE_TIME, "2015-01-01T00:00");

//                Toast.makeText(this, "Contacts successfully Added in the Group", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(NewGroup.this,AllContactActivity.class));
//                finish();
            }
            else if(which.equalsIgnoreCase(Urls.URL_GROUP_ADD)){

                Toast.makeText(this, "Group Created succesfully", Toast.LENGTH_SHORT).show();
                arrayListgroupnames.clear();


                onSyncRequest();
            }
        }



    }


    public class NewGroupAdapter extends RecyclerView.Adapter<NewGroupAdapter.ViewHolder> {
        ArrayList<NewGroupItemsModel> arrayList = new ArrayList<>();
        Context context;

        public NewGroupAdapter(ArrayList<NewGroupItemsModel> arrayList, Context context) {
            this.arrayList = arrayList;
            this.context = context;

            contactListsClicked = new ArrayList<>();

            for (NewGroupItemsModel contact : arrayList) {
                contactListsClicked.add(false);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_group_items, parent, false);
            ViewHolder viewHolder = new ViewHolder(itemView);


            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            int i = position;

            holder.contact_name.setText(arrayList.get(i).getName());

            holder.contact_select_image.setBackground(getResources().getDrawable(R.drawable.grey_tick));

            if (selectedPosition == position) {

                holder.contact_select_image.setBackground(getResources().getDrawable(R.drawable.blue_tick));

            }

            holder.contact_select_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selecteditem=arrayList.get(position).getId();
                    if (selectedPosition == position) {


                        selectedPosition = -1;
                    } else
                        selectedPosition = position;
                        NewGroupAdapter.this.notifyDataSetChanged();

                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView contact_name;
            ImageView contact_select_image;

            public ViewHolder(View itemView) {
                super(itemView);
                contact_name = (TextView) itemView.findViewById(R.id.contact_name);

                contact_select_image = (ImageView) itemView.findViewById(R.id.contact_select_image);
            }
        }
    }
    public static void showSuccessErrorMessage(String title,
                                               String message,
                                               String buttonName,
                                               final Context context) {

        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(buttonName)

                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                    }
                })
                .show();

    }
}