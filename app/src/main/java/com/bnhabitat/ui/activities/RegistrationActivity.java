package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.hbb20.CountryCodePicker;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class RegistrationActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener,CommonSyncwithoutstatus.OnAsyncResultListener {
    int count = 0;
    @BindView(R.id.register)
    ImageView register;
    //    @BindView(R.id.code_txt)
//    TextView code_txt;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.re_password)
    EditText re_password;
    //    @BindView(R.id.phone_code)
//    EditText phone_code;
//    @BindView(R.id.mySpinner)
//    TextView mySpinner;
    @BindView(R.id.layphone_code)
    LinearLayout layphone_code;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;



    String code_str;
    String name_txt, email_txt, phone_txt, password_txt, re_password_txt, provider = "", providerId = "";
    @OnClick(R.id.register)
    public void onRegister() {
        if (!provider.equalsIgnoreCase("")) {
            if (isValidWithoutPassword())

                callNextStep();
        } else {
            if (isValid())

                callNextStep();
        }


    }

    @OnClick(R.id.layphone_code)
    public void onCodeclick() {

    }

    @BindView(R.id.checked)
    ImageView checked;
    private boolean isSpinnerTouched = false;

    @OnClick(R.id.checked)
    public void onChecked() {
        if (count == 0) {
            checked.setImageResource(R.drawable.checked);
            count++;
        } else {
            count = 0;
            checked.setImageResource(R.drawable.unchecked);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        try {
            provider = getIntent().getStringExtra("provider") == null ? "" : getIntent().getStringExtra("provider");
            providerId = getIntent().getStringExtra("providerId") == null ? "" : getIntent().getStringExtra("providerId");
            name_txt = getIntent().getStringExtra("name");
            email_txt = getIntent().getStringExtra("email");
            email.setText(email_txt);
            name.setText(name_txt);
            if (!provider.equalsIgnoreCase("")) {
                password.setVisibility(View.GONE);
                re_password.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        code_str="91";
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                code_str=ccp.getSelectedCountryCode();

                Log.e("code_str",code_str);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void callNextStep() {

        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REGISTER,
                getRegisterInputJson(),
                "Register...",
                this,
                Urls.URL_REGISTER,
                Constants.POST).execute();
    }
    private void getToken() {

        new CommonSyncwithoutstatus(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_TOKEN,
                getTokenInputJson(),
                "Loading...",
                this,
                Urls.URL_GET_TOKEN,
                Constants.POST).execute();
    }

    private String getTokenInputJson() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));
            jsonObject.put("AuthKey", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_AUTH_KEY,""));

            inputJson = jsonObject.toString();
        } catch (Exception e) {

        }
        return inputJson;
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_REGISTER)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);
                    String msg = "";
                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        msg = resultObject.getString("Message");
                        if (!resultObject.getBoolean("IsError")) {

                            PreferenceConnector.getInstance(RegistrationActivity.this).savePreferences(Constants.USER_ID_REGISTER,
                                    resultObject.getString("UserId"));
//                            PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_CODE,resultObject.getString("PhoneCode"));
                            PreferenceConnector.getInstance(this).savePreferences(Constants.USER_AUTH_KEY, resultObject.getString("AuthKey"));

                            PreferenceConnector.getInstance(this).savePreferences(Constants.USERNAME, resultObject.getString("Name"));
                                  String phone=resultObject.getString("PhoneNumber");
//                            String phone1[]=phone.split("-");
//                            String phonenumber = phone.length() >= 10 ? phone.substring(phone.length() - 10): "";
////                            Log.e("strLasttenDi","strLasttenDi"+strLasttenDi);
////
//
//                            String phonecode = phone.substring(0,2);
//                            Log.e("strLasttenDi","strLasttenDi"+phonenumber+"sss"+phonecode);
                            PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_NUMBER, phone);
//
//                            PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_CODE,phonecode);
                            PreferenceConnector.getInstance(this).savePreferences(Constants.COMPANY_ID,resultObject.getString("CompanyId"));
//                            PreferenceConnector.getInstance(this).savePreferences(Constants.BROKERUSER_TYPE,resultObject.getString("BrokerUserType"));
                         getToken();

                        } else {
                            Utils.showWarningErrorMessage(getString(R.string.warning), msg, getString(R.string.ok), this);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(RegistrationActivity.this, "Not able to register. Server error", Toast.LENGTH_LONG).show();

                }

            }
            if(which.equalsIgnoreCase(Urls.URL_GET_TOKEN)){
                try {

                    JSONObject jsonObject = new JSONObject(result);

                    PreferenceConnector.getInstance(this).savePreferences(Constants.APP_AUTH_KEY,jsonObject.getString("AuthToken"));
                    PreferenceConnector.getInstance(this).savePreferences(Constants.EMAIL,jsonObject.getString("Email"));

                    Utils.showSuccessErrorMessage(getString(R.string.success), "User has been registered successfully", getString(R.string.ok), this);


                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }

    }

    private String getRegisterInputJson() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", name_txt);
            jsonObject.put("Email", email_txt);
            jsonObject.put("PhoneNumber", phone_txt);
            jsonObject.put("PhoneCode", code_str);
            jsonObject.put("Password", password_txt);
            jsonObject.put("GroupId", Integer.parseInt("5"));
            jsonObject.put("Provider", provider);
            jsonObject.put("ProviderId", providerId);
            jsonObject.put("DeviceId", Utils.getDeviceId(this));
            jsonObject.put("RegistrationId", "abbcccc");
            jsonObject.put("DeviceName", "Android");
            jsonObject.put("Platform", "Android");
            jsonObject.put("AppId", Integer.parseInt("1"));
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.CREATED_ID,""));
//          JSONObject jsonObject1= new JSONObject();
//            jsonObject1.put("CompanyId",214);
            jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);
//            jsonObject.put("Company", jsonObject1);
            inputJson = jsonObject.toString();
        } catch (Exception e) {
e.printStackTrace();
        }
        return inputJson;
    }

    private boolean isValid() {

        name_txt = name.getText().toString();
        email_txt = email.getText().toString();
        phone_txt = phone.getText().toString();
        name_txt = name.getText().toString();
        password_txt = password.getText().toString();
        re_password_txt = re_password.getText().toString();
//

        if (name_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_name), getString(R.string.ok), this);
            return false;
        } else if (name.length() <= 2) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.username_should_be_3_characters), getString(R.string.ok), this);
            return false;
        }
        else if (email_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_email), getString(R.string.ok), this);
            return false;
        } else if (!Utils.isEmailValid(email_txt)) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_email), getString(R.string.ok), this);
            return false;
        }else if (password_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_password), getString(R.string.ok), this);
            return false;
        } else if (re_password_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_confirm_password), getString(R.string.ok), this);
            return false;
        } else if (password_txt.length() < Constants.PASSWORD_MIN_LENGTH) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_six_digit), getString(R.string.ok), this);
            return false;
        } else if (!password_txt.equals(re_password_txt)) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.password_doesnt_match), getString(R.string.ok), this);
            return false;
        }  else if (phone_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_phone), getString(R.string.ok), this);
            return false;
        }else if (phone_txt.length()!=10) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_phone), getString(R.string.ok), this);
            return false;
        } else if (phone_txt.equalsIgnoreCase("0000000000")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_phone), getString(R.string.ok), this);
            return false;
        } else if (count == 0) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.agree_terms_cond), getString(R.string.ok), this);
            return false;
        }
        return true;
    }

    private boolean isValidWithoutPassword() {

        name_txt = name.getText().toString();
        email_txt = email.getText().toString();
        phone_txt = phone.getText().toString();

//

        if (name_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), "Enter your name", getString(R.string.ok), this);
            return false;
        }else if (!Utils.isEmailValid(email_txt)) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_valid_email), getString(R.string.ok), this);
            return false;
        }
        else if (name.length() <= 2) {
            Utils.showWarningErrorMessage(getString(R.string.warning), "Username should be atleast 3 characters long.", getString(R.string.ok), this);
            return false;
        } else if (email_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_email), getString(R.string.ok), this);
            return false;
        }  else if (phone_txt.equalsIgnoreCase("")) {
            Utils.showWarningErrorMessage(getString(R.string.warning), getString(R.string.enter_phone), getString(R.string.ok), this);
            return false;
        }
        return true;
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase(getString(R.string.ok))) {
//            PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_NUMBER, phone_txt);
//            PreferenceConnector.getInstance(this).savePreferences(Constants.COUNTRY_PHONE_CODE, code_str);
            Intent intent = new Intent(RegistrationActivity.this, OtpVerificationActivity.class);
           // intent.putExtra(Constants.PHONE_NUMBER,phone_txt);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finishAffinity();
        }
    }


}
