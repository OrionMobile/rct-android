/*
package com.bnhabitat.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.ProCategoryTypes;
import com.bnhabitat.township.fragment.Flat_apartment_fragment;
import com.bnhabitat.township.fragment.Pentahouse_Fragment;
import com.bnhabitat.township.fragment.Plot_lotLand_Fragment;
import com.bnhabitat.township.fragment.Residential_fragment;
import com.bnhabitat.township.fragment.RowHouse_Fragment;
import com.bnhabitat.township.fragment.ScoScf_fragment;
import com.bnhabitat.township.model.PropertyTypeModel;
import com.bnhabitat.ui.fragments.DetailFragment;
import com.bnhabitat.ui.fragments.FinancialandBankLoanFragment;
import com.bnhabitat.ui.fragments.LegalStatusFragment;
import com.bnhabitat.ui.fragments.LocationFragment;
import com.bnhabitat.ui.fragments.OtherExpensesFragment;
import com.bnhabitat.ui.fragments.OtherQuestionsFragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProjectNameSepecification extends FragmentActivity implements SendMessage {

    @OnClick(R.id.imgBack)
    public void onClickBack() {
        onBackPressed();
    }

    @BindView(R.id.tabs_projectname)
    TabLayout tabs_projectname;
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    public ArrayList<String> strings = new ArrayList<>();

    ArrayList<ProCategoryTypes> myList = new ArrayList<>();

    ArrayList<PropertyTypeModel> propertyTypeSelectedArray = new ArrayList<>();

    TabLayout.Tab Tab1, Tab2, Tab3, Tab4, Tab5, Tab6, Tab7, Tab8, Tab9, Tab10, Tab11, Tab12, Tab13, Tab14, Tab15, Tab16;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project_name_specifications);

        myList = (ArrayList<ProCategoryTypes>) getIntent().getSerializableExtra("fabList");
        propertyTypeSelectedArray = (ArrayList<PropertyTypeModel>) getIntent().getSerializableExtra("selectedList");
        tabLayout = (TabLayout) findViewById(R.id.tabs_projectname);

        if (propertyTypeSelectedArray != null)
            fetchStringFromList();


        ButterKnife.bind(this);


        setUpFrag();

    }

    private void setUpFrag() {

        Tab1 = tabLayout.newTab().setText("Flat/Apartment");
        Tab2 = tabLayout.newTab().setText("Plot/Lot/Land");
        Tab3 = tabLayout.newTab().setText("Row House Villa");
        Tab4 = tabLayout.newTab().setText("Pentahouse Duplex");
        Tab5 = tabLayout.newTab().setText("Residential Floor");
        Tab6 = tabLayout.newTab().setText("Studio Apartment");
        Tab7 = tabLayout.newTab().setText("Farm House");
        Tab8 = tabLayout.newTab().setText("SCO Shop Cum Office");
        Tab9 = tabLayout.newTab().setText("SCS Shop Cum Shop");
        Tab10 = tabLayout.newTab().setText("SCF Shop Cum Flat");
        Tab11 = tabLayout.newTab().setText("DSS Double Storey Shop");
        Tab12 = tabLayout.newTab().setText("Booth/Kiosk");
        Tab13 = tabLayout.newTab().setText("Office Space in Commercial Building");
        Tab14 = tabLayout.newTab().setText("Office in a Mall");
        Tab15 = tabLayout.newTab().setText("Shop in a Shopping Complex");
        Tab16 = tabLayout.newTab().setText("Commercial Plot");

        if (strings.contains("Flat/Apartment"))
            tabLayout.addTab(Tab1);
        else if (strings.contains("Plot/Lot/Land"))
            tabLayout.addTab(Tab2);
        else if (strings.contains("Row House Villa"))
            tabLayout.addTab(Tab3);
        else if (strings.contains("Pentahouse Duplex"))
            tabLayout.addTab(Tab4);
        else if (strings.contains("Residential Floor"))
            tabLayout.addTab(Tab5);
        else if (strings.contains("Studio Apartment"))
            tabLayout.addTab(Tab6);
        else if (strings.contains("Farm House"))
            tabLayout.addTab(Tab7);
        else if (strings.contains("SCO Shop Cum Office"))
            tabLayout.addTab(Tab8);
        else if (strings.contains("SCS Shop Cum Shop"))
            tabLayout.addTab(Tab9);
        else if (strings.contains("SCF Shop Cum Flat"))
            tabLayout.addTab(Tab10);
        else if (strings.contains("DSS Double Story Shop"))
            tabLayout.addTab(Tab11);
        else if (strings.contains("Booth/Kiosk"))
            tabLayout.addTab(Tab12);
        else if (strings.contains("Office Space in Commercial Building"))
            tabLayout.addTab(Tab13);
        else if (strings.contains("Office in a Mall"))
            tabLayout.addTab(Tab14);
        else if (strings.contains("Shop in a Shopping Complex"))
            tabLayout.addTab(Tab15);
        else if (strings.contains("Commercial Plot"))
            tabLayout.addTab(Tab16);

        Adapter adapter = new Adapter(getSupportFragmentManager());

        viewpager.setAdapter(adapter);
        tabs_projectname.setupWithViewPager(viewpager);

        //  bindWidgetsWithAnEvent();

    }

    public void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void fetchStringFromList() {

        strings.clear();

        for (int i = 0; i < propertyTypeSelectedArray.size(); i++) {

            strings.add(propertyTypeSelectedArray.get(i).getName());
        }
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }

    @Override
    public void sendData(JSONObject message, int position) {

    }

    @Override
    public void sendData(ArrayList<ContactsModel> contactsModels, int position) {

    }

    @Override
    public void sendData(int position) {

    }

    @Override
    public void sendData(String id, int position) {

    }

    @Override
    public void EditId(ArrayList<InventoryModel> inventoryModels, String EditId) {

    }

    class Adapter extends FragmentPagerAdapter {

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {


            switch (strings.get(position)) {
                case "Flat/Apartment":
                    return new Flat_apartment_fragment();
                case "Plot/Lot/Land":
                    return new Plot_lotLand_Fragment();
                case "Row House Villa":
                    return new RowHouse_Fragment();
                case "Pentahouse Duplex":
                    return new Pentahouse_Fragment();
                case "Residential Floor":
                    return new Residential_fragment();
                case "Studio Apartment":
                    return new Residential_fragment();
                case "Farm House":
                    return new Residential_fragment();
                case "SCO Shop Cum Office":
                    return new Residential_fragment();
                case "SCS Shop Cum Shop":
                    return new Residential_fragment();
                case "SCF Shop Cum Flat":
                    return new Residential_fragment();
                case "DSS Double Story Shop":
                    return new Residential_fragment();
                case "Booth/Kiosk":
                    return new Residential_fragment();
                case "Office Space in Commercial Building":
                    return new Residential_fragment();
                case "Office in a Mall":
                    return new Residential_fragment();
                case "Shop in a Shopping Complex":
                    return new Residential_fragment();
                case "Commercial Plot":
                    return new Residential_fragment();


            }
            return null;
        }

        @Override
        public int getCount() {
            return strings.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            title=strings.get(position);
//            for(int i=0;i<strings.size();i++){
//                title=strings.get(position);
//            }
//            if (position == 0)
//                title = strings.get(position);
//            if (position == 1)
//                title = "Plot/Lot/Land";
//            if (position == 2)
//                title = "Row House/Villa";
//            if (position == 3)
//                title = "PentaHouse/Duplex";
//            if (position == 4)
//                title = "Residential Floor";
//            if (position == 5)
//                title = "SCO/SCF";
//            if (position == 6)
//                title = "Studio Apartment";
//            if (position == 7)
//                title = "Farm House";
//            if (position == 8)
//                title = "SCO Shop Cum Office";
//            if (position == 9)
//                title = "SCS Shop Cum Shop";
//            if (position == 10)
//                title = "SCF Shop Cum Flat";
//            if (position == 11)
//                title = "DSS Double Storey Shop";
//            if (position == 12)
//                title = "Booth/Kiosk";
//            if (position == 13)
//                title = "Office Space in Commercial Building";
//            if (position == 14)
//                title = "Office in a Mall";
//            if (position == 15)
//                title = "Shop in a Shopping Complex";
//            if (position == 16)
//                title = "Commercial Plot";

            return title;
        }
    }

}
*/
