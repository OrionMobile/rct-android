package com.bnhabitat.ui.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.FieldModel;
import com.bnhabitat.models.MyinvitesModel;
import com.bnhabitat.models.NewGroupItemsModel;
import com.bnhabitat.models.PermissionsModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.adapters.GroupComAdapter;
import com.bnhabitat.ui.adapters.PermissionAdapter;
import com.bnhabitat.ui.views.MultiSelectionSpinner;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;


public class CreateNewGroupActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener,MultiSelectionSpinner.OnMultipleItemsSelectedListener {
    Dialog dialog;
    int from;
    String owner_id, parent_id, field_id_string, permission_id_string;
     String permissionid_string;
    StringBuilder stringBuilder, permission_builder;
    RecyclerView permissions;
    ArrayList<String> id = new ArrayList<>();
    List<Integer> ids = new ArrayList<>();
    GroupComAdapter adapterCommonDialog;
    CheckBox insert, update, delete, select_all_contact, private_data, view_data;
    TextView group_owner, parent_group, fields;
    EditText group_name_txt;
    Button create_group;
    ArrayList<NewGroupItemsModel> arrayListgroupnames = new ArrayList<>();
    ArrayList<FieldModel.ResultData> fieldModels = new ArrayList<>();
    ArrayList<String> fieldModelsNames = new ArrayList<>();
    ArrayList<MyinvitesModel> myinvitesModels = new ArrayList<>();
    ArrayList<PermissionsModel> permissionsModels;
    ArrayList<CommonDialogModel> group_name;
    ArrayList<CommonDialogModel> my_users;
    ArrayList<CommonDialogModel> field_model;
    LinearLayout parent_group_lay, group_owner_lay, field_lay;
    GridLayoutManager linearLayoutManager;
    int numberOfColumns = 2;
    PermissionAdapter permissionAdapter;
    //    private DatabaseReference myRef;
//    private FirebaseDatabase database;
    ImageView back_button;
    ArrayList<ContactsModel> stringarray = new ArrayList<>();
    ArrayList<NewGroupItemsModel> arrayList = new ArrayList<>();
    ArrayList<String> field_id = new ArrayList<>();
    ArrayList<String> permission_id = new ArrayList<>();
    MultiSelectionSpinner multiselectSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_group);
//        getSupportActionBar().hide();

        create_group = (Button) findViewById(R.id.create_group);

        permissions = (RecyclerView) findViewById(R.id.permissions);
//        select_all_contact = (CheckBox) findViewById(R.id.select_all);
//        private_data = (CheckBox) findViewById(R.id.private_data);
//        delete = (CheckBox) findViewById(R.id.delete);
//        update = (CheckBox) findViewById(R.id.update);
//        insert = (CheckBox) findViewById(R.id.insert);
        group_name_txt = (EditText) findViewById(R.id.group_name);
        group_owner = (TextView) findViewById(R.id.group_owner);
        parent_group = (TextView) findViewById(R.id.parent_group);
        fields = (TextView) findViewById(R.id.fields);
        parent_group_lay = (LinearLayout) findViewById(R.id.parent_group_lay);
        group_owner_lay = (LinearLayout) findViewById(R.id.group_owner_lay);
        field_lay = (LinearLayout) findViewById(R.id.field_lay);
        back_button = (ImageView) findViewById(R.id.back);
        multiselectSpinner = (MultiSelectionSpinner) findViewById(R.id.multiselectSpinner);
        multiselectSpinner.setListener(this);
        onGetPremissonsRequest();
        onGetFieldRequest();
        onGetGroupOwnerRequest();
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//

        create_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(group_owner.getText().toString().length()>0) {
//                    if (parent_group.getText().toString().length() > 0) {
//                    if (group_name_txt.getText().toString().length() > 0) {
//                        onSetGroup();
//                    } else {
//                        Utils.showWarningErrorMessage("Warning", "Please enter group name", "Ok", CreateNewGroupActivity.this);
//                    }
//                }else{
//                    Utils.showWarningErrorMessage("Warning","Please select parent owner","Ok",CreateNewGroupActivity.this);
//                }
//                }else{
//                    Utils.showWarningErrorMessage("Warning","Please select group owner","Ok",CreateNewGroupActivity.this);
//                }



                        if (group_name_txt.getText().toString().length() > 0) {
                            onSetGroup();
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Please enter group name", "Ok", CreateNewGroupActivity.this);
                        }



            }

        });
        parent_group_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                group_name = new ArrayList<>();
                if(!myinvitesModels.isEmpty()) {
                    showCommonDialog(CreateNewGroupActivity.this, group_name, 2, 0);
                }else {
                    Utils.showWarningErrorMessage("Warning","No Parent Group","Ok",CreateNewGroupActivity.this);
                }


            }

        });
        group_owner_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                my_users = new ArrayList<>();
                if(!arrayListgroupnames.isEmpty()) {
                    showCommonDialog(CreateNewGroupActivity.this, my_users, 1, 0);
                }else {
                    Utils.showWarningErrorMessage("Warning","No Group owner","Ok",CreateNewGroupActivity.this);
                }

            }

        });
        field_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                field_model = new ArrayList<>();
                if(!fieldModels.isEmpty()) {
                    showCommonDialog(CreateNewGroupActivity.this, field_model, 3, 0);
                }else {
                    Utils.showWarningErrorMessage("Warning","No Fields","Ok",CreateNewGroupActivity.this);
                }


            }

        });
        permissions.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
//
        onGetRequest();


    }


    public void showCommonDialog(Context context,
                                 final ArrayList<CommonDialogModel> commonDialogModels,
                                 final int from, final int clickedPosition) {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.check_dialog);
        dialog.setCancelable(false);
        dialog.show();


        ListView listView = (ListView) dialog.findViewById(R.id.listView);
        Button registerBtn = (Button) dialog.findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();

            }
        });
        if (from == 1) {
            for (int i = 0; i < arrayListgroupnames.size(); i++) {

                CommonDialogModel commonDialogModel = new CommonDialogModel();
                commonDialogModel.setId(arrayListgroupnames.get(i).getId());
                commonDialogModel.setTitle(arrayListgroupnames.get(i).getName());
                commonDialogModels.add(commonDialogModel);
            }
        } else if (from == 2) {
            for (int k = 0; k < myinvitesModels.size(); k++) {

                CommonDialogModel commonDialogModel = new CommonDialogModel();
                commonDialogModel.setId(myinvitesModels.get(k).getId());
                commonDialogModel.setTitle(myinvitesModels.get(k).getFirstName());
                commonDialogModels.add(commonDialogModel);

            }
        } else {
            registerBtn.setVisibility(View.VISIBLE);
            for (int k = 0; k < fieldModels.size(); k++) {

                CommonDialogModel commonDialogModel = new CommonDialogModel();
                commonDialogModel.setId(fieldModels.get(k).getId());
                commonDialogModel.setTitle(fieldModels.get(k).getName());
                commonDialogModels.add(commonDialogModel);

            }
        }
        adapterCommonDialog = new GroupComAdapter(this,
                commonDialogModels, from);
        listView.setAdapter(adapterCommonDialog);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int arg1, long l) {
//             if(from==1){
//                 group_name_txt.setText(myinvitesModels.get(arg1).getFirstName());
//             }else{
//                 parent_group.setText(arrayListgroupnames.get(arg1).getName());
//             }
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(StatusModel statusModels) {

        if (statusModels.getPos() == 1) {
            group_owner.setText(statusModels.getName());
            owner_id = statusModels.getId();
        } else if (statusModels.getPos() == 2) {
            parent_group.setText(statusModels.getName());
            parent_id = statusModels.getId();
        }

        dialog.cancel();
    }

    public void onEvent(ArrayList<StatusModel> statusModels) {

        stringBuilder = new StringBuilder();

        for (int i = 0; i < statusModels.size(); i++) {


                if (statusModels.get(i).getStatus().equalsIgnoreCase("permission")) {
//                permission_builder.append(statusModels.get(i).getName() + ",");
//                String str = permission_builder.substring(0, permission_builder.length() - 1);

                   stringBuilder.append(statusModels.get(i).getId()+",");


//                fields.setText(str);

            }


        }

        permission_id_string=stringBuilder.toString();


        permission_id_string=permission_id_string.substring(0,permission_id_string.length()-1);
//        permission_id_string = permission_id.toString().replace("[", "").replace("]", "");
        field_id_string = field_id.toString().replace("[", "").replace("]", "");
    }

    private void onGetRequest() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonAsync(CreateNewGroupActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GROUP_LISTING + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""),
                "",
                "Loading..",
                CreateNewGroupActivity.this,
                Urls.URL_GROUP_LISTING,
                Constants.GET).execute();

//
    }

    private void onGetGroupOwnerRequest() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonSyncwithoutstatus(CreateNewGroupActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_USERS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""),
                "",
                "Loading..",
                CreateNewGroupActivity.this,
                Urls.URL_ALL_USERS,
                Constants.GET).execute();

//
    }

    private void onGetFieldRequest() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonSyncwithoutstatus(CreateNewGroupActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_FIELD,
                "",
                "Loading..",
                CreateNewGroupActivity.this,
                Urls.URL_FIELD,
                Constants.GET).execute();

//
    }

    private void onGetPremissonsRequest() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");
        http:
//rctcontactsqaapi.realsynergy.in/api/group/GetPermission
        new CommonSyncwithoutstatus(CreateNewGroupActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PERMISSIONS,
                "",
                "Loading..",
                CreateNewGroupActivity.this,
                Urls.URL_PERMISSIONS,
                Constants.GET).execute();

//
    }

    private void onSetGroup() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonAsync(CreateNewGroupActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_GROUP,
                getGroupInputJson(),
                "Loading..",
                CreateNewGroupActivity.this,
                Urls.URL_CREATE_GROUP,
                Constants.POST).execute();

//
    }

    private String getGroupInputJson() {

        String inputJson = "";
//


//
        try {

            if(!ids.isEmpty()){

               StringBuilder stringBuilder = new StringBuilder();
                for(int i=0;i<ids.size();i++){

                    stringBuilder.append(String.valueOf(ids.get(i)+","));

                }
              field_id_string=stringBuilder.toString();
                field_id_string=field_id_string.substring(0,field_id_string.length()-1);
            }


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("GroupName", group_name_txt.getText().toString());
            jsonObject.put("ParentGroupId", parent_id);
            jsonObject.put("OwnerUserId", owner_id);
            jsonObject.put("permissionIds", permission_id_string);
            jsonObject.put("CompanyId", "1");
            jsonObject.put("CreatedBy", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));
            jsonObject.put("FieldIds", field_id_string);

            inputJson=jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_GROUP_LISTING)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        if (projectsArray.length() == 0) {
                         //   Toast.makeText(CreateNewGroupActivity.this, "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < projectsArray.length(); index++) {
                                JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                                NewGroupItemsModel newGroupItemsModel = new NewGroupItemsModel();

                                newGroupItemsModel.setId(jsonObject1.getString("Id"));
                                newGroupItemsModel.setName(jsonObject1.getString("GroupName"));
                                newGroupItemsModel.setDescription(jsonObject1.getString("Description"));
//

                                arrayListgroupnames.add(newGroupItemsModel);

                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_ALL_USERS)) {
//                contactsFirebaseModels.clear();
                try {

                    JSONArray projectsArray = new JSONArray(result);

                    if (projectsArray.length() == 0) {

//                        Toast.makeText(this, "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            MyinvitesModel myinvitesModel = new MyinvitesModel();
                            myinvitesModel.setId(jsonObject1.getString("Id"));
                            myinvitesModel.setUserName(jsonObject1.getString("UserName"));
                            myinvitesModel.setFirstName(jsonObject1.getString("FirstName"));
                            myinvitesModel.setLastName(jsonObject1.getString("LastName"));
                            myinvitesModel.setEmail(jsonObject1.getString("Email"));
                            myinvitesModel.setGroupName(jsonObject1.getString("GroupName"));
                            myinvitesModel.setPhone(jsonObject1.getString("Phone"));
                            myinvitesModels.add(myinvitesModel);


                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_PERMISSIONS)) {
//                contactsFirebaseModels.clear();
                try {

                    JSONArray projectsArray = new JSONArray(result);

                    if (projectsArray.length() == 0) {

//                        Toast.makeText(this, "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {
                        permissionsModels = new ArrayList<>();


                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            PermissionsModel permissionsModel = new PermissionsModel();
                            permissionsModel.setId(jsonObject1.getString("Id"));
                            permissionsModel.setPermissionName(jsonObject1.getString("PermissionName"));
                            permissionsModel.setGroupId(jsonObject1.getString("GroupId"));

                            permissionsModels.add(permissionsModel);


                        }

                        permissionAdapter = new PermissionAdapter(this, permissionsModels);
                        permissions.setAdapter(permissionAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_FIELD)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
//                        onGetGroupOwnerRequest();

                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        if (projectsArray.length() == 0) {
                        //    Toast.makeText(CreateNewGroupActivity.this, "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < projectsArray.length(); index++) {
                                JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                                FieldModel.ResultData fieldModel = new FieldModel().new ResultData();

                                fieldModel.setId(jsonObject1.getString("id"));
                                fieldModel.setName(jsonObject1.getString("name"));
                                fieldModelsNames.add(jsonObject1.getString("name"));
//                            newGroupItemsModel.setSlug(jsonObject1.getString("Slug"));
//                            newGroupItemsModel.setTaxonomyId(jsonObject1.getString("TaxonomyId"));

                                fieldModels.add(fieldModel);

                            }


                            multiselectSpinner.setItems(fieldModelsNames);

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if (which.equalsIgnoreCase(Urls.URL_CREATE_GROUP)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
//                        onGetGroupOwnerRequest();

                        Utils.showSuccessErrorMessage("Success","Group created","Ok",this);

                        }else{
                        Utils.showErrorMessage("Something wrong in entering the data",this);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public void selectedIndices(List<Integer> indices) {

        ids=indices;

    }

    @Override
    public void selectedStrings(List<String> strings) {

    }

    public void onEvent(DialogModel dialogModel){
        Intent intent=new Intent(this,GroupActivity.class);
        startActivity(intent);
        finish();
    }
}
