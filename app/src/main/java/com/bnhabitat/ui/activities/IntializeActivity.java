package com.bnhabitat.ui.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.data.category.CategoryTable;
import com.bnhabitat.data.category.CategoryType;
import com.bnhabitat.data.location.CityTable;
import com.bnhabitat.data.location.RegionTable;
import com.bnhabitat.data.location.StateTable;
import com.bnhabitat.data.sizeunit.SizeUnitTable;
import com.bnhabitat.data.sizeunit.UnitsTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.township.townactivities.LegalActivity;
import com.bnhabitat.township.townactivities.Township;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;

//import com.real.craft.realcatalyst.services.ContactService;

public class IntializeActivity extends Activity implements CommonAsync.OnAsyncResultListener {

    private String TIMEOUT_ERROR = "Failed to sync. Please retry..";
    private String INTIALIZE_WAIT_MESSAGE = "Please wait....";
    private String INTIALIZE_MESSAGE = "Preparing app for first use. ";
    private String DATA = "";
    private String PROJECTS = "";
    private String PARTNERS = "";


    private String userId;
    private TextView intializeTextVw;

    private Button retryBtn;
    private ProgressBar progressBar2;

    private ContactsReceiver backgroundReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intialize);
//        Utils.getInstance().setCommonTitle(this, getResources().getString(R.string.app_name), 0, 0, 0, 0, false);

        userId = PreferenceConnector.getInstance(getApplication()).loadSavedPreferences(Constants.USER_ID, "");
        companyId = PreferenceConnector.getInstance(getApplication()).loadSavedPreferences(Constants.COMPANY_ID, "1");

        intializeTextVw = (TextView) findViewById(R.id.intializeTextVw);
        retryBtn = (Button) findViewById(R.id.retryBtn);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);

        new CommonAsync(this,
                Urls.CONTACT_MANAGEMENT_URL + Urls.URL_MASTER,
                "",
                "",
                this,
                Urls.URL_MASTER,
                Constants.GET, false).execute();

        intializeTextVw.setText(INTIALIZE_MESSAGE + DATA + INTIALIZE_WAIT_MESSAGE);
        PreferenceConnector.getInstance(IntializeActivity.this).savePreferences(Constants.IS_INTIALIZED, true);


        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intializeTextVw.setText(INTIALIZE_MESSAGE + DATA + INTIALIZE_WAIT_MESSAGE);
                retryBtn.setVisibility(View.GONE);
                progressBar2.setVisibility(View.VISIBLE);
                new CommonAsync(IntializeActivity.this,
                        Urls.CONTACT_MANAGEMENT_URL + Urls.URL_MASTER,
                        "",
                        "",
                        IntializeActivity.this,
                        Urls.URL_MASTER,
                        Constants.GET, false).execute();


            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregisterReceiver(backgroundReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        backgroundReceiver = new ContactsReceiver();
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(ContactService.CONTACT_SERVICE);
//        registerReceiver(backgroundReceiver, intentFilter);
    }


    private class ContactsReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub

//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    Intent intent = new Intent(IntializeActivity.this, ChatActivity.class);
//                    intent.putExtra(Constants.FROM, "ina");
//                    startActivity(intent);
//
//                    PreferenceConnector.getInstance(IntializeActivity.this).savePreferences(Constants.IS_INTIALIZED, true);
//                    finish();
//
//                }
//            });
        }
    }

    @Override
    public void onResultListener(String result, String which) {


        if (null != result && !result.trim().equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_MASTER)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);


                    if (jsonObject.getString("StatusCode").equalsIgnoreCase("200")) {

                        JSONObject resultObject = jsonObject.getJSONObject("Result");


                        if (!resultObject.isNull("SizeUnits")) {
                            JSONArray sizeArray = resultObject.getJSONArray("SizeUnits");
                            for (int index = 0; index < sizeArray.length(); index++) {

                                if (!SizeUnitTable.getInstance().getSizeUnit(sizeArray.getJSONObject(index).getInt("Id")))
                                    SizeUnitTable.getInstance().write(
                                            sizeArray.getJSONObject(index).getInt("Id"),
                                            sizeArray.getJSONObject(index).getInt("SizeUnitId"),
                                            sizeArray.getJSONObject(index).getInt("StandardUnitId"),
                                            sizeArray.getJSONObject(index).getInt("StateId"),
                                            sizeArray.getJSONObject(index).getString("SizeUnitValue"),
                                            sizeArray.getJSONObject(index).getString("UnitName"),
                                            new Date(),
                                            new Date());
                                else {
                                    SizeUnitTable.getInstance().updateUnits(
                                            sizeArray.getJSONObject(index).getInt("Id"),
                                            sizeArray.getJSONObject(index).getInt("SizeUnitId"),
                                            sizeArray.getJSONObject(index).getInt("StandardUnitId"),
                                            sizeArray.getJSONObject(index).getInt("StateId"),
                                            sizeArray.getJSONObject(index).getString("SizeUnitValue"),
                                            sizeArray.getJSONObject(index).getString("UnitName"),
                                            new Date());
                                }
                            }
                        }

                        if (!resultObject.isNull("Units")) {
                            JSONArray unitsArray = resultObject.getJSONArray("Units");
                            for (int index = 0; index < unitsArray.length(); index++) {

                                if (!UnitsTable.getInstance().isUnitAvailable(unitsArray.getJSONObject(index).getInt("UnitId")))
                                    UnitsTable.getInstance().write(
                                            unitsArray.getJSONObject(index).getInt("UnitId"),
                                            unitsArray.getJSONObject(index).getInt("UnitTypeId"),
                                            unitsArray.getJSONObject(index).getInt("valuePerPivotUnit"),
                                            unitsArray.getJSONObject(index).getString("UnitName"),
                                            unitsArray.getJSONObject(index).getString("Description"),
                                            new Date(),
                                            new Date());
                                else {
                                    UnitsTable.getInstance().updateUnits(
                                            unitsArray.getJSONObject(index).getInt("UnitId"),
                                            unitsArray.getJSONObject(index).getInt("UnitTypeId"),
                                            unitsArray.getJSONObject(index).getInt("valuePerPivotUnit"),
                                            unitsArray.getJSONObject(index).getString("UnitName"),
                                            unitsArray.getJSONObject(index).getString("Description"),
                                            new Date());
                                }
                            }
                        }

                        if (!resultObject.isNull("States")) {
                            JSONArray statesArray = resultObject.getJSONArray("States");

                            if (statesArray.length() > 0)
                                StateTable.getInstance().deleteAllRecords();

                            for (int index = 0; index < statesArray.length(); index++) {

                                if (!StateTable.getInstance().isStateAvailable(statesArray.getJSONObject(index).getInt("sid")))
                                    StateTable.getInstance().write(statesArray.getJSONObject(index).getInt("sid"),
                                            statesArray.getJSONObject(index).getInt("countryId"),
                                            statesArray.getJSONObject(index).getString("description"),
                                            statesArray.getJSONObject(index).getString("isActive"),
                                            statesArray.getJSONObject(index).getString("statename"),
                                            new Date(),
                                            new Date());
                                else {
                                    StateTable.getInstance().updateState(statesArray.getJSONObject(index).getInt("sid"),
                                            statesArray.getJSONObject(index).getInt("countryId"),
                                            statesArray.getJSONObject(index).getString("description"),
                                            statesArray.getJSONObject(index).getString("isActive"),
                                            statesArray.getJSONObject(index).getString("statename"),
                                            new Date());
                                }
                            }
                        }

                        if (!resultObject.isNull("Cities")) {
                            JSONArray cityArray = resultObject.getJSONArray("Cities");

                            if (cityArray.length() > 0)
                                CityTable.getInstance().deleteAllRecords();


                            for (int index = 0; index < cityArray.length(); index++) {

                                int cityId = cityArray.getJSONObject(index).getInt("cityid");
                                String cityName = cityArray.getJSONObject(index).getString("cityname");

                                if (index == 0) {
                                    PreferenceConnector.getInstance(getApplication()).savePreferences(Constants.CITY_ID, String.valueOf(cityId));
                                    PreferenceConnector.getInstance(getApplication()).savePreferences(Constants.CITY_NAME, cityName);
                                }

                                if (!CityTable.getInstance().isCityAvailable(cityArray.getJSONObject(index).getInt("cityid")))
                                    CityTable.getInstance().write(
                                            cityArray.getJSONObject(index).getInt("cityid"),
                                            cityArray.getJSONObject(index).getString("cityname"),
                                            cityArray.getJSONObject(index).getInt("countryid"),
                                            cityArray.getJSONObject(index).getInt("stateId"),
                                            cityArray.getJSONObject(index).getString("citydescription"),
                                            cityArray.getJSONObject(index).getString("isActive"),
                                            cityArray.getJSONObject(index).getInt("District"),
                                            new Date(),
                                            new Date());
                                else {
                                    CityTable.getInstance().updateCity(
                                            cityArray.getJSONObject(index).getInt("cityid"),
                                            cityArray.getJSONObject(index).getString("cityname"),
                                            cityArray.getJSONObject(index).getInt("countryid"),
                                            cityArray.getJSONObject(index).getInt("stateId"),
                                            cityArray.getJSONObject(index).getString("citydescription"),
                                            cityArray.getJSONObject(index).getString("isActive"),
                                            cityArray.getJSONObject(index).getInt("District"),
                                            new Date());
                                }

                                if (!cityArray.getJSONObject(index).isNull("Regions")) {
                                    JSONArray regionArray = cityArray.getJSONObject(index).getJSONArray("Regions");
                                    for (int indexi = 0; indexi < regionArray.length(); indexi++) {
                                        String regionName = regionArray.getString(indexi);
                                        if (!RegionTable.getInstance().isRegionAvailable(regionName, cityId))
                                            RegionTable.getInstance().write(
                                                    cityId,
                                                    regionName,
                                                    1,
                                                    new Date(),
                                                    new Date());
                                    }
                                }
                            }
                        }


                        if (!resultObject.isNull("Categories")) {
                            JSONArray categoryArray = resultObject.getJSONArray("Categories");
                            for (int index = 0; index < categoryArray.length(); index++) {


                                if (!CategoryTable.getInstance().isCategoryAvailable(categoryArray.getJSONObject(index).getInt("id")))
                                    CategoryTable.getInstance().write(
                                            categoryArray.getJSONObject(index).getInt("id"),
                                            categoryArray.getJSONObject(index).getString("name"),
                                            categoryArray.getJSONObject(index).getString("description"),
                                            new Date(),
                                            new Date());
                                else {
                                    CategoryTable.getInstance().updateCategory(
                                            categoryArray.getJSONObject(index).getInt("id"),
                                            categoryArray.getJSONObject(index).getString("name"),
                                            categoryArray.getJSONObject(index).getString("description"),
                                            new Date());
                                }


                                JSONArray categoryTypeArray = categoryArray.getJSONObject(index).getJSONArray("CategoryTypes");


                                for (int indexJ = 0; indexJ < categoryTypeArray.length(); indexJ++) {

                                    if (!CategoryType.getInstance().isCategoryAvailable(categoryTypeArray.getJSONObject(indexJ).getInt("id"))) {
                                        CategoryType.getInstance().write(
                                                categoryTypeArray.getJSONObject(indexJ).getInt("id"),
                                                categoryArray.getJSONObject(index).getInt("id"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("name"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("description"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("Area1"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("Area2"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("Area3"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("Area4"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("DefaultUnitID"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("MinUnitRange"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("MaxUnitRange"),
                                                new Date(),
                                                new Date());

                                        Log.e("CategoryTypes", categoryTypeArray.toString());
                                    } else {
                                        CategoryType.getInstance().updateCategoryType(
                                                categoryTypeArray.getJSONObject(indexJ).getInt("id"),
                                                categoryArray.getJSONObject(index).getInt("id"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("name"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("description"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("Area1"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("Area2"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("Area3"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("Area4"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("DefaultUnitID"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("MinUnitRange"),
                                                categoryTypeArray.getJSONObject(indexJ).getString("MaxUnitRange"),
                                                new Date());
                                        Log.e("CategoryTypes", categoryTypeArray.toString());
                                    }


                                }


                            }
                        }
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
                startActivity(new Intent(IntializeActivity.this, NewDashboardActivity.class));
                //startActivity(new Intent(IntializeActivity.this, Township.class));
                //startActivity(new Intent(IntializeActivity.this, Township_Subarea_Project.class));
                //startActivity(new Intent(IntializeActivity.this, WelcomeScreen.class));
                //startActivity(new Intent(IntializeActivity.this, LegalActivity.class));

                finish();


                intializeTextVw.setText(INTIALIZE_MESSAGE + PROJECTS + INTIALIZE_WAIT_MESSAGE);


            } else if (which.equalsIgnoreCase(Urls.URL_CURRENCY_CONVERTOR)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);


                    if (jsonObject.getString("StatusCode").equalsIgnoreCase("200")) {

                        String resultString = jsonObject.getString("Result");
                        PreferenceConnector.getInstance(getApplication()).savePreferences(Constants.CURRENCY_VALUES, resultString);

                        if (PreferenceConnector.getInstance(IntializeActivity.this).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME, "").equalsIgnoreCase("")) {
                            PreferenceConnector.getInstance(IntializeActivity.this).savePreferences(Constants.DEFAULT_CURRENCY_NAME, "INR");
                            PreferenceConnector.getInstance(IntializeActivity.this).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, 1);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


//                Intent intent = new Intent(IntializeActivity.this, DashboardActivity.class);
//                intent.putExtra(Constants.FROM, "ina");
//                startActivity(intent);
//                PreferenceConnector.getInstance(IntializeActivity.this).savePreferences(Constants.IS_INTIALIZED, true);
//                finish();


            }
        } else if (which.equalsIgnoreCase(Constants.TIMEOUT)) {
            intializeTextVw.setText(TIMEOUT_ERROR);
            retryBtn.setVisibility(View.VISIBLE);
            progressBar2.setVisibility(View.GONE);
        }
    }

    String companyId = "";
    String cityId = "";

//    private void getSearchResults() {
//        companyId = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.COMPANY_ID, "");
//        cityId = PreferenceConnector.getInstance(getApplication()).loadSavedPreferences(Constants.CITY_ID, Constants.CITY_ID_CONST);
//        String urlCondition = "";
//
//        if (!PreferenceConnector.getInstance(this).loadBooleanPreferences(Constants.IS_STAFF))
//            urlCondition = "CityId eq " + cityId;
//        else
//            urlCondition = "builderCompanyId eq " + companyId;
//
//
//        new CommonAsync(this,
//                Urls.URL + (Urls.SEARCH_PROJECT) + userId + "?$filter=" + urlCondition.replace(" ", "%20"),
//                "",
//                "",
//                this,
//                Urls.URL_PREFERNCE_PROJECTS,
//                Constants.GET, false).execute();
//
//
//    }
}
