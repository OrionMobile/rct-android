package com.bnhabitat.ui.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.ui.fragments.GetFeelerFragment;
import com.bnhabitat.ui.fragments.PublishedProjectFragment;
import com.bnhabitat.ui.fragments.UnPublishedProjectFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ManageProjectActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, CommonAsync.OnAsyncResultListener {
    TabLayout tab_layout_manage_project;
    ViewPager manage_pager;
    PagerAdapter adapter;
    ImageView back;
    private ArrayList<ProjectsModel> searchProjectsModels = new ArrayList();
    List<String> tal = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_project);
        tab_layout_manage_project = (TabLayout) findViewById(R.id.tab_layout_manage_project);
        tab_layout_manage_project.addTab(tab_layout_manage_project.newTab().setText("PUBLISHED"));
        tab_layout_manage_project.addTab(tab_layout_manage_project.newTab().setText("UNPUBLISHED"));
        tab_layout_manage_project.addTab(tab_layout_manage_project.newTab().setText("GET FEELER"));

        tab_layout_manage_project.setTabGravity(TabLayout.GRAVITY_FILL);

        manage_pager = (ViewPager) findViewById(R.id.manage_pager);
        back = (ImageView) findViewById(R.id.back);
        adapter = new PagerAdapter
                (getSupportFragmentManager(), tab_layout_manage_project.getTabCount());

        manage_pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_layout_manage_project));
        tab_layout_manage_project.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                manage_pager.setCurrentItem(tab.getPosition());
                TabChangeListener fragment = (TabChangeListener) adapter.instantiateItem(manage_pager, tab.getPosition());
                if (fragment != null) {
                    fragment.onChangeTab(tab.getPosition());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getPublishProjectList();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_PUBLISH_PROJECT_LIST)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        if (projectsArray.length() == 0) {
                            Toast.makeText(this, "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                        }

                        searchProjectsModels.clear();

                        for (int index = 0; index < projectsArray.length(); index++) {
                            ProjectsModel projectsModel = new ProjectsModel();

                            projectsModel.setId(projectsArray.getJSONObject(index).getString("Id"));
                            projectsModel.setTitle(projectsArray.getJSONObject(index).getString("title"));
                            projectsModel.setLocality(projectsArray.getJSONObject(index).getString("Locality"));
                            projectsModel.setUpdatedOn(projectsArray.getJSONObject(index).getString("UpdatedOn"));
                            projectsModel.setArea(projectsArray.getJSONObject(index).getString("Area"));
                            projectsModel.setAreaUnitId(projectsArray.getJSONObject(index).getString("AreaUnit"));
                            projectsModel.setNumberOfTowers(projectsArray.getJSONObject(index).getString("NumberOfTowers"));
                            projectsModel.setAboutSubProject(projectsArray.getJSONObject(index).getString("aboutSubProject"));
                            projectsModel.setPossessionMonth(projectsArray.getJSONObject(index).getString("PossessionMonth"));
                            projectsModel.setPossessionYear(projectsArray.getJSONObject(index).getString("PossessionYear"));

                            projectsModel.setProjectStatus(projectsArray.getJSONObject(index).getString("ProjectStatus"));
                            projectsModel.setRegion(projectsArray.getJSONObject(index).getString("Region"));
                            projectsModel.setImagePath(projectsArray.getJSONObject(index).getString("logoimage"));
                            projectsModel.setTopImage1(projectsArray.getJSONObject(index).getString("topImage1"));
                            projectsModel.setTypeOfProject(projectsArray.getJSONObject(index).getString("TypeOfProject"));
                            projectsModel.setCompanyLogo(projectsArray.getJSONObject(index).getString("CompanyLogo"));
                            projectsModel.setCityName(projectsArray.getJSONObject(index).getString("CityName"));
                            projectsModel.setBuilderCompanyName(projectsArray.getJSONObject(index).getString("builderCompanyName"));


                            JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("PropertySize");
                            ArrayList<ProjectsModel.PropertySize> propertySizes = new ArrayList();
                            StringBuilder propertyStringBuilder = new StringBuilder();
                            String propertyString = "";
                            long minPrice = 10000000000l;
                            long maxPrice = 0;

                            for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                                ProjectsModel.PropertySize propertySize = projectsModel.new PropertySize();
                                JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                                propertySize.setId(dataObj.getString("Id"));
                                propertySize.setTitle(dataObj.getString("title"));
                                propertySize.setApplicationForm(dataObj.getString("applicationForm"));
                                propertySize.setProperty_typeIdlevel1(dataObj.getString("property_typeIdlevel1"));
                                propertySize.setProperty_typeIdlevel2(dataObj.getString("property_typeIdlevel2"));
                                propertySize.setProperty_typeIdlevel3(dataObj.getString("property_typeIdlevel3"));
                                propertySize.setFloorPlan(dataObj.getString("floorPlan"));
                                propertySize.setSize(dataObj.getString("size"));
                                propertySize.setSizeUnit(dataObj.getString("sizeUnit"));
                                propertySize.setBuiltarea(dataObj.getString("builtarea"));
                                propertySize.setCarpetarea(dataObj.getString("carpetarea"));
                                propertySize.setSizeUnit2(dataObj.getString("sizeUnit2"));
                                propertySize.setAboutProperty(dataObj.getString("AboutProperty"));
                                propertySize.setLength(dataObj.getString("Length"));
                                propertySize.setBreadth(dataObj.getString("Breadth"));
                                propertySize.setSizeUnit3(dataObj.getString("SizeUnit3"));
                                propertySize.setExtraArea(dataObj.getString("ExtraArea"));
                                if (minPrice > dataObj.getLong("MinimumPrice"))
                                    minPrice = dataObj.getLong("MinimumPrice");

                                if (maxPrice < dataObj.getLong("MaximumPrice"))
                                    maxPrice = dataObj.getLong("MaximumPrice");
                                propertySize.setMinimumPrice(String.valueOf(minPrice));
                                propertySize.setMaximumPrice(String.valueOf(maxPrice));
                                propertySize.setBathRooms(dataObj.getString("BathRooms"));
                                propertySize.setBedRooms(dataObj.getString("BedRooms"));
                                propertySize.setTowerId(dataObj.getString("TowerId"));
                                propertySize.setIsCostAppliedCarpetArea(dataObj.getString("isCostAppliedCarpetArea"));
                                propertySize.setExtraAreaLabel(dataObj.getString("ExtraAreaLabel"));
                                propertySize.setDiscounts(dataObj.getString("Discounts"));
                                propertySize.setLayoutPlans(dataObj.getString("LayoutPlans"));
                                propertySize.setDiscountGroups(dataObj.getString("DiscountGroups"));
                                propertySize.setTowers(dataObj.getString("Towers"));


                                propertySizes.add(propertySize);


                            }


                            JSONArray projectRelationshipManagerses = projectsArray.getJSONObject(index).getJSONArray("BrokerStaff");
                            ArrayList<ProjectsModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList();
                            if (projectRelationshipManagerses.length() != 0) {
                                for (int indexJ = 0; indexJ < projectRelationshipManagerses.length(); indexJ++) {

                                    ProjectsModel.ProjectRelationshipManagers projectRelationshipManagers = projectsModel.new ProjectRelationshipManagers();
                                    JSONObject dataObj = projectRelationshipManagerses.getJSONObject(indexJ);
                                    projectRelationshipManagers.setId(dataObj.getString("Id"));
                                    projectRelationshipManagers.setName(dataObj.getString("Name"));
                                    projectRelationshipManagers.setEmail(dataObj.getString("Email"));
                                    projectRelationshipManagers.setPhoto(dataObj.getString("Photo"));
                                    projectRelationshipManagers.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                    projectRelationshipManagers.setDesignation(dataObj.getString("Designation"));

                                    projectRelationshipManagersArrayList.add(projectRelationshipManagers);
                                }
                            }

                            JSONArray towersJson = projectsArray.getJSONObject(index).getJSONArray("Towers");
                            ArrayList<ProjectsModel.Towers> towerses = new ArrayList();
                            if (towersJson.length() != 0) {
                                for (int indexJ = 0; indexJ < towersJson.length(); indexJ++) {

                                    ProjectsModel.Towers towersProjectModel = projectsModel.new Towers();
                                    JSONObject dataObj = towersJson.getJSONObject(indexJ);
                                    towersProjectModel.setTowerId(dataObj.getString("TowerId"));
                                    towersProjectModel.setBasement(dataObj.getString("Basement"));
                                    towersProjectModel.setFloorType(dataObj.getString("floorType"));
                                    towersProjectModel.setProjectCategoryId(dataObj.getString("ProjectCategoryId"));
                                    towersProjectModel.setPropertyTypeId(dataObj.getString("PropertyTypeId"));
                                    towersProjectModel.setStories(dataObj.getString("Stories"));
                                    towersProjectModel.setTowerName(dataObj.getString("TowerName"));


                                    towerses.add(towersProjectModel);
                                }
                            }

                            projectsModel.setPropertySizes(propertySizes);
                            projectsModel.setProjectRelationshipManagerses(projectRelationshipManagersArrayList);
                            projectsModel.setTowerses(towerses);
                            searchProjectsModels.add(projectsModel);
                            manage_pager.setAdapter(adapter);

                        }


//                        for (ProjectsModel projectsModel : searchProjectsModels) {
//                            tal.add(projectsModel.getCityName());
//
//
//                        }
//                        HashSet<String> hashSet = new HashSet<String>();
//                        hashSet.addAll(tal);
//                        tal.clear();
//                        tal.addAll(hashSet);
//                        Collections.sort(tal, new Comparator<String>() {
//                            public int compare(String obj1, String obj2) {
//                                // ## Ascending order
//                                return obj1.compareToIgnoreCase(obj2); // To compare string values
//                                // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values
//
//                                // ## Descending order
//                                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
//                                // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
//                            }
//                        });
//                        tal.add(0, "All");

                    }
                } catch (Exception e) {

                }
            }
        }
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    PublishedProjectFragment tab1 = new PublishedProjectFragment();
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("projectModel",searchProjectsModels);
                    tab1.setArguments(bundle);
                    return tab1;
                case 1:
                    UnPublishedProjectFragment tab2 = new UnPublishedProjectFragment();
                    return tab2;
                case 2:
                    GetFeelerFragment tab3 = new GetFeelerFragment();
                    return tab3;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    private void getPublishProjectList() {
        new CommonAsync(this, Urls.URL + Urls.URL_PUBLISH_PROJECT_LIST + Constants.APP_USER_ID + "/" + Constants.APP_ID + "/true",
                "",
                "Loading..",
                this,
                Urls.URL_PUBLISH_PROJECT_LIST,
                Constants.GET).execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
