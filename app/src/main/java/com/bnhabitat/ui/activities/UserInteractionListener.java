package com.bnhabitat.ui.activities;

interface UserInteractionListener {

    void onUserInteraction();

}
