package com.bnhabitat.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.adapter.ProjectListingAdapterNew;
import com.bnhabitat.areaModule.model.ProjectListModel;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProjectsActivity extends AppCompatActivity implements CommonSyncwithoutstatus.OnAsyncResultListener, CommonAsync.OnAsyncResultListener {

    RecyclerView rvProjectList;
    TextView no_data;
    ArrayList<ProjectListModel> projectListingArrayList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    ProjectListingAdapterNew projectListingAdapter;
    String id, stateId, districtId;
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);
        init();
        getSupportActionBar().hide();

        onCLicks();
    }

    private void onCLicks() {

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
    }

    private void init() {

        rvProjectList = (RecyclerView) findViewById(R.id.rvProjectList);
        no_data = (TextView) findViewById(R.id.no_data);
        linearLayoutManager = new LinearLayoutManager(this);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
    }

    @Override
    public void onResume() {
        onProjectListing();
        super.onResume();
    }

    private void onProjectListing() {

        new CommonSyncwithoutstatus(this,
                Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_ALL_PROJECT_LISTING +
                        Constants.COMPANY_ID_BN,
                "",
                "Loading...", this,
                Urls.URL_PROJECT_LISTS,
                Constants.GET)
                .execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_PROJECT_LISTS)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.optJSONArray("Result");
                    projectListingArrayList.clear();
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            ProjectListModel projectListModel = new ProjectListModel();
                            projectListModel.setId(jsonObject1.getString("Id"));
                            projectListModel.setName(jsonObject1.getString("Name"));
                            projectListModel.setLocalityName(jsonObject1.getString("LocalityName"));
                            projectListModel.setZipCode(jsonObject1.getString("ZipCode"));
                            projectListModel.setStateName(jsonObject1.getString("StateName"));
                            projectListModel.setDistrictName(jsonObject1.getString("DistrictName"));
                            projectListModel.setTehsilName(jsonObject1.getString("TehsilName"));
                            projectListModel.setTownName(jsonObject1.getString("TownName"));

                            JSONArray jsonArray1 = jsonObject1.getJSONArray("ProjectCategoryTypes");
                            ArrayList<ProjectListModel.ProjectCategoryTypes> projectCategoryTypes = new ArrayList<>();
                            projectCategoryTypes.clear();
                            for (int index = 0; index < jsonArray1.length(); index++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(index);
                                ProjectListModel.ProjectCategoryTypes projectCategoryTypes1 = new ProjectListModel.ProjectCategoryTypes();
                                projectCategoryTypes1.setType(jsonObject2.getString("Type"));
                                projectCategoryTypes.add(projectCategoryTypes1);
                            }

                            projectListModel.setProjectCategoryTypes(projectCategoryTypes);

                            JSONArray jsonArray2 = jsonObject1.getJSONArray("ProjectFiles");
                            ArrayList<ProjectListModel.ProjectFiles> projectFiles = new ArrayList<>();
                            projectFiles.clear();
                            for (int i1 = 0; i1 < jsonArray2.length(); i1++) {
                                JSONObject jsonObject2 = jsonArray2.getJSONObject(i1);
                                ProjectListModel.ProjectFiles projectFiles1 = new ProjectListModel.ProjectFiles();
                                projectFiles1.setCategory(jsonObject2.getString("Category"));
                                projectFiles1.setFileUrl(Urls.BASE_CONTACT_IMAGE_URL + jsonObject2.getString("FileUrl"));
                                projectFiles.add(projectFiles1);
                            }

                            projectListModel.setProjectFiles(projectFiles);
                            projectListingArrayList.add(projectListModel);
                        }
                        projectListingAdapter = new ProjectListingAdapterNew(ProjectsActivity.this, projectListingArrayList);
                        rvProjectList.setLayoutManager(linearLayoutManager);
                        rvProjectList.setAdapter(projectListingAdapter);
                        //  rvProjectList.getLayoutManager().smoothScrollToPosition(rvProjectList,null, 0);
                        projectListingAdapter.notifyDataSetChanged();

                    } else {
                        rvProjectList.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
