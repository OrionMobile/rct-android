package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.NewGroupItemsModel;
import com.bnhabitat.models.SelectPropertyTypeModel;
import com.bnhabitat.models.StateModel;
import com.bnhabitat.ui.adapters.SelectPropertyAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SelectPropertyActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener {
    RecyclerView select_property_list;
    GridLayoutManager gridLayoutManager;
    int numberOfColumns = 3;
    SelectPropertyAdapter selectPropertyAdapter;
    String from = "";
    TextView property_type;
    ImageView back_btn;
    ArrayList<SelectPropertyTypeModel> selectPropertyTypeModels = new ArrayList<>();
    TextView show_draft;
    ArrayList<StateModel> stateModels =new ArrayList<>();
    ArrayList<String> area_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_property);
        select_property_list = (RecyclerView) findViewById(R.id.select_property_list);
        property_type = (TextView) findViewById(R.id.property_type);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        gridLayoutManager = new GridLayoutManager(this, numberOfColumns);
        select_property_list.setLayoutManager(gridLayoutManager);
        show_draft = (TextView) findViewById(R.id.show_draft);
        from = getIntent().getStringExtra(Constants.FROM);
        property_type.setText(from);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        onGetPropertyType();
        getStateListing();

        show_draft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SelectPropertyActivity.this, MyPropertyActivity.class);
                intent.putExtra("from_savedrafts","from_savedrafts");
                startActivity(intent);
            }
        });
    }

    private void getStateListing() {

        new CommonAsync(SelectPropertyActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_STATE_LISTING + "/1",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_STATE_LISTING + "/1",
                Constants.GET).execute();

    }

    private void onGetPropertyType() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonAsync(SelectPropertyActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_TYPE + from,
                "",
                "Loading..",
                SelectPropertyActivity.this,
                Urls.URL_PROPERTY_TYPE,
                Constants.GET).execute();

//
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_TYPE)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray selectpropertyType = jsonObject.getJSONArray("Result");

                        if (selectpropertyType.length() == 0) {
                            Toast.makeText(SelectPropertyActivity.this, "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                        }
                        for (int index = 0; index < selectpropertyType.length(); index++) {
                            JSONObject jsonObject1 = selectpropertyType.getJSONObject(index);

                            SelectPropertyTypeModel selectPropertyTypeModel = new SelectPropertyTypeModel();

                            selectPropertyTypeModel.setId(jsonObject1.getString("Id"));
                            selectPropertyTypeModel.setName(jsonObject1.getString("Name"));
                            selectPropertyTypeModel.setType(jsonObject1.getString("Type"));
//

                            selectPropertyTypeModels.add(selectPropertyTypeModel);


                        }
                        selectPropertyAdapter = new SelectPropertyAdapter(this, selectPropertyTypeModels, from);
                        select_property_list.setAdapter(selectPropertyAdapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_STATE_LISTING + "/1")) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {
//                  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                StateModel stateModel = new StateModel();

                                stateModel.setId(jsonObject1.getInt("Id"));
                                stateModel.setCountryId(jsonObject1.getInt("CountryId"));
                                stateModel.setName(jsonObject1.getString("Name"));

                                stateModels.add(stateModel);

                            }
                            area_list.clear();

                            for (int i = 0; i < stateModels.size(); i++) {
                                area_list.add(stateModels.get(i).getName());
                            }

                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(SelectPropertyActivity.this, SelectionPropertyTypeActivity.class);
//        startActivity(intent);
        finish();
    }
}
