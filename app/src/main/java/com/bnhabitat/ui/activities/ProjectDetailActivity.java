package com.bnhabitat.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.SyncService.UpdateProjectService;
import com.bnhabitat.data.projects.RecentViewProjectsTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.PaymentPlanModel;
import com.bnhabitat.models.PricingModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.models.UpdateProject;
import com.bnhabitat.ui.fragments.FeaturesFragment;
import com.bnhabitat.ui.fragments.FinanceFragment;
import com.bnhabitat.ui.fragments.GeneralInfoFragment;
import com.bnhabitat.ui.fragments.PricingUnitFragment;
import com.bnhabitat.ui.fragments.VarientFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import de.greenrobot.event.EventBus;

public class ProjectDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, CommonAsync.OnAsyncResultListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    PagerAdapter adapter;
    ImageView back;
    String id, projectId;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private final long MINIMUM_DEFAULT_VALUE = 100000000000000l;
    ArrayList<ProjectsDetailModel.LayoutPlans> layoutPlanses = new ArrayList<>();
    ArrayList<ProjectsDetailModel.KeyDistances> keyDistancesArrayList = new ArrayList<>();
    ArrayList<ProjectsDetailModel.Amenities> amenitiesArrayList = new ArrayList<>();
    ArrayList<ProjectsDetailModel.FAQs> faQses = new ArrayList<>();
    ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList();
    ArrayList<ProjectsDetailModel.SpecialFeatures> specialFeaturesArrayList = new ArrayList<>();
    ArrayList<ProjectsDetailModel.PropertySize.Specifications> specificationsArrayList = new ArrayList<>();
    ArrayList<ProjectsDetailModel.PropertySize> propertySizes = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.ProjectLenders> projectLendersesModel = new ArrayList<>();
    ProjectsDetailModel projectsDetailModel;
    ArrayList<ProjectsDetailModel> projectListingModelArrayList = new ArrayList<>();
    private ArrayList<PricingModel> pricingModels = new ArrayList<>();
    TextView name;
    int pos;
    private View actionB, action_A;
    private ArrayList<PaymentPlanModel> paymentPlanModels = new ArrayList<>();
    private double minBookingAmount = MINIMUM_DEFAULT_VALUE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);

        final FloatingActionsMenu menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        name = (TextView) findViewById(R.id.name);
        tabLayout.addTab(tabLayout.newTab().setText("General"));
        tabLayout.addTab(tabLayout.newTab().setText("Features"));
        tabLayout.addTab(tabLayout.newTab().setText("Varient"));
        tabLayout.addTab(tabLayout.newTab().setText("Payment"));
        tabLayout.addTab(tabLayout.newTab().setText("Miscellaneous"));
//        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.faq)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        pos = getIntent().getIntExtra("position", 0);
        Log.e("project_detail_position", String.valueOf(pos));
        menuMultipleActions.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

//                transparentLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {

//                transparentLayout.setVisibility(View.GONE);
            }
        });
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
                Intent intent = new Intent(ProjectDetailActivity.this, AddContatctActivity.class);
                startActivity(intent);

            }
        });
        action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "https://bnhabitat.com/app/" + PreferenceConnector.getInstance(ProjectDetailActivity.this).loadSavedPreferences("encrypt", "");
                ;
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
        PreferenceConnector.getInstance(this).savePreferences(Constants.TOWER_VALUE, "");
        PreferenceConnector.getInstance(this).savePreferences(Constants.UNIT_VALUE, "");
        PreferenceConnector.getInstance(this).savePreferences(Constants.EFFECTIVE_UNIT, "");
        PreferenceConnector.getInstance(this).savePreferences(Constants.EFFECTIVE_PRICE, "");

        try {
            projectListingModelArrayList = (ArrayList<ProjectsDetailModel>) getIntent().getSerializableExtra("notification_array");
            projectId = String.valueOf(projectListingModelArrayList.get(pos).getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewPager = (ViewPager) findViewById(R.id.pager1);
        back = (ImageView) findViewById(R.id.back);
        try {
            id = getIntent().getStringExtra("id") == null ? "" : getIntent().getStringExtra("id");

            if (RecentViewProjectsTable.getInstance().isProjectAvailable(projectId)) {

                onResultListener(RecentViewProjectsTable.getInstance().getProject(projectId),
                        Urls.URL_PROJECT_DETAILS);

            } else {

                if (id.equalsIgnoreCase("")) {
                    new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_DETAILS + Constants.COMPAN_ID + "/" + projectListingModelArrayList.get(pos).getId(),
                            "",
                            "Intializing..",
                            this,
                            Urls.URL_PROJECT_DETAILS,
                            Constants.GET).execute();
                } else {
                    new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_DETAILS + Constants.COMPAN_ID + "/" + id,
                            "",
                            "Intializing..",
                            this,
                            Urls.URL_PROJECT_DETAILS,
                            Constants.GET).execute();
                }
            }

//            Log.d("host", "host" + id);


        } catch (Exception e) {

        }
        String toolbar_title = getIntent().getStringExtra("ProjectName");
        name.setText(toolbar_title);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    GeneralInfoFragment generalInfoFragment = new GeneralInfoFragment();
                    Bundle bundle = new Bundle();

                    bundle.putSerializable("projectModel", projectsDetailModel);
                    bundle.putSerializable("propertySizes", propertySizes);

                    bundle.putSerializable("notification_array", projectListingModelArrayList);
                    bundle.putInt("position", pos);

                    generalInfoFragment.setArguments(bundle);

                    return generalInfoFragment;
                case 1:
                    FeaturesFragment tab2 = new FeaturesFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable("projectModel", projectsDetailModel);
                    bundle1.putSerializable("notification_array", projectListingModelArrayList);
                    bundle1.putInt("position", pos);
                    tab2.setArguments(bundle1);

                    return tab2;
                case 2:
                    VarientFragment varientFragment = new VarientFragment();
                    Bundle bundle_varient = new Bundle();
                    bundle_varient.putSerializable("projectModel", projectsDetailModel);
                    bundle_varient.putSerializable("notification_array", projectListingModelArrayList);
                    bundle_varient.putInt("position", pos);
                    varientFragment.setArguments(bundle_varient);

                    return varientFragment;
                case 3:
                    PricingUnitFragment tab3 = new PricingUnitFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putSerializable("projectModel", projectsDetailModel);
                    bundle2.putSerializable("propertySizes", propertySizes);
                    bundle2.putSerializable("notification_array", projectListingModelArrayList);
                    bundle2.putInt("position", pos);
                    tab3.setArguments(bundle2);
                    return tab3;
                case 4:
                    FinanceFragment tab4 = new FinanceFragment();
                    Bundle bundle3 = new Bundle();
                    bundle3.putSerializable("projectModel", projectsDetailModel);
                    bundle3.putSerializable("notification_array", projectListingModelArrayList);
                    bundle3.putInt("position", pos);
                    tab4.setArguments(bundle3);
                    return tab4;
//                case 5:
//                    FaqFragment faqFragment = new FaqFragment();
//                    Bundle bundle_faq=new Bundle();
//                    bundle_faq.putSerializable("projectModel",projectsDetailModel);
//                    bundle_faq.putSerializable("notification_array",projectListingModelArrayList);
//                    bundle_faq.putInt("position",pos);
//                    faqFragment.setArguments(bundle_faq);
//                    return faqFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    public void onEvent(UpdateProject updateProject) {

        if (id.equalsIgnoreCase("")) {
            new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_DETAILS + Constants.COMPAN_ID + "/" + projectListingModelArrayList.get(pos).getId(),
                    "",
                    "Intializing..",
                    this,
                    Urls.URL_PROJECT_DETAILS,
                    Constants.GET).execute();
        } else {
            new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_DETAILS + Constants.COMPAN_ID + "/" + id,
                    "",
                    "Intializing..",
                    this,
                    Urls.URL_PROJECT_DETAILS,
                    Constants.GET).execute();
        }
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROJECT_DETAILS)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        RecentViewProjectsTable.getInstance().deleteProjectRecords(projectId);
                        faQses.clear();
                        keyDistancesArrayList.clear();
                        propertySizes.clear();
                        paymentPlanModels.clear();
                        pricingModels.clear();
                        amenitiesArrayList.clear();
                        specialFeaturesArrayList.clear();
                        projectLendersesModel.clear();
                        projectRelationshipManagersArrayList.clear();

                        projectsDetailModel = new ProjectsDetailModel();
                        if (!RecentViewProjectsTable.getInstance().isProjectAvailable(projectId)) {


                            RecentViewProjectsTable.getInstance().write(projectId,
                                    result,
                                    result,
                                    new Date(),
                                    new Date());

                        } else {
                            RecentViewProjectsTable.getInstance().updateProject(projectId,
                                    result,
                                    result,
                                    new Date(),
                                    new Date());

                        }
//                        RecentViewProjectsTable.getInstance().write(projectId,
//                                result,
//                                result,
//                                new Date(),
//                                new Date());
//

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        startService(new Intent(this, UpdateProjectService.class).
                                putExtra("projectId", projectId).putExtra("modifiedDate", resultObject.getString("UpdatedOn")));

                        projectsDetailModel.setId(resultObject.getInt("Id"));
                        projectsDetailModel.setTitle(resultObject.getString("title"));
                        projectsDetailModel.setBuilderProjectId(resultObject.getInt("builderProjectId"));
                        projectsDetailModel.setLogoImage(resultObject.getString("logoimage"));
                        projectsDetailModel.setTopImage1(resultObject.getString("topImage1"));
                        projectsDetailModel.setTopImage2(resultObject.getString("topImage2"));
                        projectsDetailModel.setTopImage3(resultObject.getString("topImage3"));
                        projectsDetailModel.setAboutSubProject(resultObject.getString("aboutSubProject"));
                        projectsDetailModel.setSpecifications(resultObject.getString("specifications"));
                        projectsDetailModel.setBuilderCompanyId(resultObject.getInt("builderCompanyId"));
                        projectsDetailModel.setBuilderCompanyName(resultObject.getString("builderCompanyName"));
                        projectsDetailModel.setCountryId(resultObject.getInt("CountryId"));
                        projectsDetailModel.setCountryName(resultObject.getString("CountryName"));
                        projectsDetailModel.setCityName(resultObject.getString("CityName"));
                        projectsDetailModel.setAreaUnit(resultObject.getInt("AreaUnit"));
                        projectsDetailModel.setArea(resultObject.getString("Area"));
                        projectsDetailModel.setRegion(resultObject.getString("Region"));
                        projectsDetailModel.setLocality(resultObject.getString("Locality"));
                        projectsDetailModel.setTagline(resultObject.getString("Tagline"));
                        projectsDetailModel.setTypeofProject(resultObject.getInt("TypeofProject"));
                        projectsDetailModel.setProjectStatus(resultObject.getString("ProjectStatus"));
                        projectsDetailModel.setNumberOfParking(resultObject.getInt("NumberOfParking"));
                        projectsDetailModel.setNoOfUnits(resultObject.getString("NoOfUnits"));
                        projectsDetailModel.setNumberOfTowers(resultObject.getString("NumberOfTowers"));
                        projectsDetailModel.setBedRooms(resultObject.getString("BedRooms"));
                        projectsDetailModel.setTypeOfProject(resultObject.getString("TypeOfProject"));
                        projectsDetailModel.setCompanyLogo(resultObject.getString("CompanyLogo"));
                        projectsDetailModel.setCurrentOffers(resultObject.getString("currentOffers"));
                        String toolbar_title = projectsDetailModel.getTitle();
                        name.setText(toolbar_title);
                        if (!resultObject.isNull("LayoutPlans")) {
                            JSONArray loctionalJson = resultObject.getJSONArray("LayoutPlans");
                            for (int i = 0; i < loctionalJson.length(); i++) {
                                JSONObject jsonObject1 = loctionalJson.getJSONObject(i);
                                ProjectsDetailModel.LayoutPlans layoutPlans = projectsDetailModel.new LayoutPlans();
                                layoutPlans.setBigImage(jsonObject1.getString("BigImage"));
                                layoutPlanses.add(layoutPlans);

                            }
                        }
                        if (!resultObject.isNull("KeyDistances")) {
                            JSONArray keyDistancesJson = resultObject.getJSONArray("KeyDistances");
                            for (int i = 0; i < keyDistancesJson.length(); i++) {
                                JSONObject jsonObject1 = keyDistancesJson.getJSONObject(i);
                                ProjectsDetailModel.KeyDistances keyDistances = projectsDetailModel.new KeyDistances();
                                keyDistances.setImage(jsonObject1.getString("Image"));
                                keyDistances.setNameOfLocation(jsonObject1.getString("NameOfLocation"));
                                keyDistances.setDistance(jsonObject1.getString("Distance"));
                                keyDistances.setDistanceUnit(jsonObject1.getString("DistanceUnit"));
                                keyDistances.setKeyDistanceId(jsonObject1.getString("KeyDistanceId"));
                                keyDistancesArrayList.add(keyDistances);

                            }
                        }
                        JSONArray amenitiesJson = resultObject.getJSONArray("Amenities");
                        for (int i = 0; i < amenitiesJson.length(); i++) {
                            JSONObject jsonObject1 = amenitiesJson.getJSONObject(i);
                            ProjectsDetailModel.Amenities amenities = projectsDetailModel.new Amenities();
                            amenities.setAmenity(jsonObject1.getString("Amenity"));
                            amenities.setImage(jsonObject1.getString("Image"));
                            amenities.setDescription(jsonObject1.getString("Description"));
                            amenitiesArrayList.add(amenities);

                        }
                        JSONArray faqs = resultObject.getJSONArray("FAQs");
                        for (int i = 0; i < faqs.length(); i++) {
                            JSONObject jsonObject1 = faqs.getJSONObject(i);
                            ProjectsDetailModel.FAQs faQs = projectsDetailModel.new FAQs();
                            faQs.setId(jsonObject1.getInt("Id"));
                            faQs.setQuestion(jsonObject1.getString("Question"));
                            faQs.setAnswer(jsonObject1.getString("Answer"));
                            faQs.setProjectId(jsonObject1.getInt("ProjectId"));
                            faQses.add(faQs);

                        }

                        JSONArray projectRelationshipManagerses = resultObject.getJSONArray("BrokerStaff");

                        if (projectRelationshipManagerses.length() != 0) {
                            for (int indexJ = 0; indexJ < projectRelationshipManagerses.length(); indexJ++) {

                                ProjectsDetailModel.ProjectRelationshipManagers projectRelationshipManagers = projectsDetailModel.new ProjectRelationshipManagers();
                                JSONObject dataObj = projectRelationshipManagerses.getJSONObject(indexJ);
                                projectRelationshipManagers.setId(Integer.parseInt(dataObj.getString("Id")));
                                projectRelationshipManagers.setName(dataObj.getString("Name"));
                                projectRelationshipManagers.setEmail(dataObj.getString("Email"));
                                projectRelationshipManagers.setPhoto(dataObj.getString("Photo"));
                                projectRelationshipManagers.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                projectRelationshipManagers.setDesignation(dataObj.getString("Designation"));

                                projectRelationshipManagersArrayList.add(projectRelationshipManagers);
                            }
                        }

                        if (!resultObject.isNull("ProjectLenders")) {
                            JSONArray lenderArray = resultObject.getJSONArray("ProjectLenders");
                            for (int index = 0; index < lenderArray.length(); index++) {
                                ProjectsDetailModel.ProjectLenders projectLenders = projectsDetailModel.new ProjectLenders();

                                projectLenders.setLenderId(lenderArray.getJSONObject(index).getInt("LenderId"));
                                projectLenders.setDescription(lenderArray.getJSONObject(index).getString("Description"));
                                projectLenders.setName(lenderArray.getJSONObject(index).getString("Name"));
                                projectLenders.setAddress(lenderArray.getJSONObject(index).getString("Address"));
                                projectLenders.setContactDetails(lenderArray.getJSONObject(index).getString("ContactDetails"));
                                projectLenders.setLogo(lenderArray.getJSONObject(index).getString("Logo"));
                                projectLenders.setWebsite(lenderArray.getJSONObject(index).getString("Website"));
                                projectLendersesModel.add(projectLenders);
                            }
                        }
                        JSONArray resultArray = resultObject.getJSONArray("PriceDetails");
                        for (int index = 0; index < resultArray.length(); index++) {

                            PricingModel pricingModel = new PricingModel();
                            pricingModel.setSizeId(resultArray.getJSONObject(index).getInt("sizeId"));
                            JSONArray priceDetailArray = resultArray.getJSONObject(index).getJSONArray("priceDetails");

                            ArrayList<PricingModel.PriceDetails> priceDetailses = new ArrayList<>();
                            for (int indexj = 0; indexj < priceDetailArray.length(); indexj++) {

                                PricingModel.PriceDetails priceDetails = pricingModel.new PriceDetails();

                                priceDetails.setProjprprtycomponentID(priceDetailArray.getJSONObject(indexj).getInt("projprprtycomponentID"));
                                priceDetails.setName(priceDetailArray.getJSONObject(indexj).getString("name"));
                                priceDetails.setDescription(priceDetailArray.getJSONObject(indexj).getString("description"));
                                priceDetails.setActive(priceDetailArray.getJSONObject(indexj).getBoolean("isActive"));
                                priceDetails.setPriority(priceDetailArray.getJSONObject(indexj).getInt("Priority"));
                                priceDetails.setPaymentOptions(priceDetailArray.getJSONObject(indexj).getString("paymentOptions"));
                                priceDetails.setApplyMultipleSubComponents(priceDetailArray.getJSONObject(indexj).getBoolean("applyMultipleSubComponents"));
                                priceDetails.setServiceTax(priceDetailArray.getJSONObject(indexj).getDouble("ServiceTax"));


                                JSONArray jsonArray = priceDetailArray.getJSONObject(indexj).getJSONArray("ProjectPropertySubComponents");

                                ArrayList<PricingModel.PriceDetails.ProjectPropertySubComponents> projectPropertySubComponentses = new ArrayList<>();

                                for (int indexk = 0; indexk < jsonArray.length(); indexk++) {


                                    PricingModel.PriceDetails.ProjectPropertySubComponents projectPropertySubComponents = priceDetails.new ProjectPropertySubComponents();
                                    projectPropertySubComponents.setComponentId(jsonArray.getJSONObject(indexk).getInt("componentId"));
                                    projectPropertySubComponents.setId(jsonArray.getJSONObject(indexk).getInt("Id"));
                                    projectPropertySubComponents.setTitle(jsonArray.getJSONObject(indexk).getString("title"));
                                    projectPropertySubComponents.setDescription(jsonArray.getJSONObject(indexk).getString("description"));
                                    projectPropertySubComponents.setActive(jsonArray.getJSONObject(indexk).getBoolean("isActive"));
                                    projectPropertySubComponents.setFloor(jsonArray.getJSONObject(indexk).getBoolean("IsFloor"));
                                    projectPropertySubComponents.setPaymentOption(jsonArray.getJSONObject(indexk).getString("PaymentOption"));
                                    projectPropertySubComponents.setCompulsory(jsonArray.getJSONObject(indexk).getBoolean("compulsory"));
                                    projectPropertySubComponents.setAmount(jsonArray.getJSONObject(indexk).getString("amount"));
                                    projectPropertySubComponents.setNoOfParking(jsonArray.getJSONObject(indexk).getInt("NoOfParking"));
                                    projectPropertySubComponents.setOptions(jsonArray.getJSONObject(indexk).getString("Options"));
                                    projectPropertySubComponents.setServiceTax(jsonArray.getJSONObject(indexk).getDouble("ServiceTax"));
                                    projectPropertySubComponentses.add(projectPropertySubComponents);

                                }
                                priceDetails.setProjectPropertySubComponents(projectPropertySubComponentses);
                                priceDetailses.add(priceDetails);
                            }

                            pricingModel.setPriceDetails(priceDetailses);

                            pricingModels.add(pricingModel);
                        }
                        ////End of Pricing//////

                        //PaymentPlan///

                        resultArray = resultObject.getJSONArray("PaymentPlanDetails");
                        for (int index = 0; index < resultArray.length(); index++) {
                            PaymentPlanModel paymentPlanModel = new PaymentPlanModel();


                            paymentPlanModel.setId(resultArray.getJSONObject(index).getInt("Id"));
                            paymentPlanModel.setName(resultArray.getJSONObject(index).getString("name"));
                            paymentPlanModel.setDisclaimer(resultArray.getJSONObject(index).getString("Disclaimer"));
                            JSONArray propertysizesArray = resultArray.getJSONObject(index).getJSONArray("propertysizes");

                            ArrayList<PaymentPlanModel.PropertySizes> propertySizeslist = new ArrayList<>();

                            for (int indexi = 0; indexi < propertysizesArray.length(); indexi++) {

                                PaymentPlanModel.PropertySizes propertySizes = paymentPlanModel.new PropertySizes();
                                propertySizes.setId(propertysizesArray.getInt(indexi));
                                propertySizeslist.add(propertySizes);
                            }

                            ArrayList<PaymentPlanModel.Milestones> milestoneslist = new ArrayList<>();

                            JSONArray milestonesArray = resultArray.getJSONObject(index).getJSONArray("milestones");

                            for (int indexi = 0; indexi < milestonesArray.length(); indexi++) {

                                String milestoneNumber = milestonesArray.getJSONObject(indexi).getString("milestoneNumber");
                                boolean isLumsum = milestonesArray.getJSONObject(indexi).getBoolean("isLumpsum");
                                boolean isInsclusive = milestonesArray.getJSONObject(indexi).getBoolean("isInclusive");

                                PaymentPlanModel.Milestones milestones = paymentPlanModel.new Milestones();
                                milestones.setMilestoneNumber(milestoneNumber);
                                milestones.setLumsum(isLumsum);
                                milestones.setInclusive(isInsclusive);

                                JSONArray milestonesCompoArray = milestonesArray.getJSONObject(indexi).getJSONArray("PaymentPlanMilestoneComponents");

                                ArrayList<PaymentPlanModel.Milestones.PaymentPlanMilestoneComponents> milestonesComplist = new ArrayList<>();

                                for (int indexj = 0; indexj < milestonesCompoArray.length(); indexj++) {
                                    PaymentPlanModel.Milestones.PaymentPlanMilestoneComponents paymentPlanMilestoneComponents = milestones.new PaymentPlanMilestoneComponents();
                                    paymentPlanMilestoneComponents.setComponentId(milestonesCompoArray.getJSONObject(indexj).getInt("componentId"));
                                    paymentPlanMilestoneComponents.setSubComponentId(milestonesCompoArray.getJSONObject(indexj).getInt("subComponentId"));
                                    paymentPlanMilestoneComponents.setMilestoneValue(milestonesCompoArray.getJSONObject(indexj).getDouble("milestoneValue"));
                                    paymentPlanMilestoneComponents.setDiscountApplicable(milestonesCompoArray.getJSONObject(indexj).getBoolean("DiscountApplicable"));

                                    milestonesComplist.add(paymentPlanMilestoneComponents);

                                }

                                milestones.setPaymentPlanMilestoneComponentses(milestonesComplist);

                                milestoneslist.add(milestones);

                                if (isLumsum)
                                    if (milestoneNumber.equalsIgnoreCase("Booking Amount"))
                                        if (milestonesComplist.size() > 0)
                                            if (milestonesComplist.get(0).getMilestoneValue() < minBookingAmount)
                                                minBookingAmount = milestonesComplist.get(0).getMilestoneValue();


                            }


                            PaymentPlanModel.Discount discount = paymentPlanModel.new Discount();

                            if (!resultArray.getJSONObject(index).isNull("discount")) {
                                JSONObject discountObject = resultArray.getJSONObject(index).getJSONObject("discount");

                                if (discountObject != null) {
                                    discount.setDiscountName(discountObject.getString("discountName"));
                                    discount.setDiscountValue(discountObject.getString("discountValue"));
                                    discount.setId(discountObject.getInt("Id"));
                                    discount.setPaymentOption(discountObject.getString("paymentOption"));
                                }
                            }


                            paymentPlanModel.setDiscount(discount);
                            paymentPlanModel.setMilestones(milestoneslist);
                            paymentPlanModel.setPropertySizes(propertySizeslist);

                            paymentPlanModels.add(paymentPlanModel);
                        }

                        ///End of payment plan
                        projectsDetailModel.setPaymentPlanModels(paymentPlanModels);
                        projectsDetailModel.setPricingModels(pricingModels);
                        JSONArray specialFeaturesJson = resultObject.getJSONArray("SpecialFeatures");
                        for (int i = 0; i < specialFeaturesJson.length(); i++) {
                            JSONObject jsonObject1 = specialFeaturesJson.getJSONObject(i);
                            ProjectsDetailModel.SpecialFeatures specialFeatures = projectsDetailModel.new SpecialFeatures();
                            specialFeatures.setFeatureName(jsonObject1.getString("FeatureName"));
                            specialFeatures.setImage(jsonObject1.getString("Image"));

                            specialFeaturesArrayList.add(specialFeatures);

                        }
                        StringBuilder propertyStringBuilder = new StringBuilder();
                        String propertyString = "";
                        JSONArray propertySizeJson = resultObject.getJSONArray("PropertySize");
                        for (int i = 0; i < propertySizeJson.length(); i++) {
                            JSONObject jsonObject1 = propertySizeJson.getJSONObject(i);
                            ProjectsDetailModel.PropertySize propertySize = projectsDetailModel.new PropertySize();
                            propertySize.setId(jsonObject1.getInt("Id"));
                            propertySize.setTitle(jsonObject1.getString("title"));
                            propertySize.setProperty_typeIdlevel1(jsonObject1.getInt("property_typeIdlevel1"));
                            propertySize.setProperty_typeIdlevel2(jsonObject1.getInt("property_typeIdlevel2"));
                            propertySize.setCarpetarea(jsonObject1.getString("carpetarea"));
                            propertySize.setBuiltarea(jsonObject1.getString("builtarea"));
                            propertySize.setExtraArea(jsonObject1.getString("ExtraArea"));
                            propertySize.setSize(jsonObject1.getString("size"));
                            propertySize.setLength(jsonObject1.getString("Length"));
                            propertySize.setBreadth(jsonObject1.getString("Breadth"));
                            propertySize.setSizeUnit3(Integer.parseInt(jsonObject1.getString("SizeUnit3")));
                            propertySize.setApplicableOnCarpetArea(jsonObject1.getBoolean("isCostAppliedCarpetArea"));
                            propertySize.setSizeUnit(jsonObject1.getInt("sizeUnit"));
                            propertySize.setMaximumPrice(jsonObject1.getLong("MaximumPrice"));
                            propertySize.setMinimumPrice(jsonObject1.getLong("MinimumPrice"));
                            JSONArray specificationsJson = propertySizeJson.getJSONObject(i).getJSONArray("Specifications");
                            specificationsArrayList = new ArrayList<>();
                            for (int index = 0; index < specificationsJson.length(); index++) {
                                JSONObject jsonObjectspeci = specificationsJson.getJSONObject(index);
                                ProjectsDetailModel.PropertySize.Specifications specifications = propertySize.new Specifications();
                                specifications.setAreaName(jsonObjectspeci.getString("AreaName"));
                                specifications.setTitle(jsonObjectspeci.getString("Title"));
                                specifications.setDescription(jsonObjectspeci.getString("Description"));
                                specifications.setBSPType(jsonObjectspeci.getString("BSPType"));

                                specificationsArrayList.add(specifications);

                            }
                            ArrayList<ProjectsDetailModel.PropertySize.Discounts> discountses = new ArrayList<>();

                            JSONArray disArray = propertySizeJson.getJSONObject(i).getJSONArray("Discounts");

                            for (int indexi = 0; indexi < disArray.length(); indexi++) {

                                ProjectsDetailModel.PropertySize.Discounts discounts = propertySize.new Discounts();
                                discounts.setId(disArray.getJSONObject(indexi).getInt("Id"));
                                discounts.setPaymentOption(disArray.getJSONObject(indexi).getString("paymentOption"));
                                discounts.setDiscountName(disArray.getJSONObject(indexi).getString("discountName"));
                                discounts.setDiscountValue(disArray.getJSONObject(indexi).getString("discountValue"));
                                discounts.setDiscountType(disArray.getJSONObject(indexi).getString("discountType"));
                                discounts.setEndDate(disArray.getJSONObject(indexi).getString("EndDate"));
                                discounts.setStartDate(disArray.getJSONObject(indexi).getString("StartDate"));

                                ArrayList<ProjectsDetailModel.PropertySize.Discounts.Components> componentses = new ArrayList<>();


                                if (!disArray.getJSONObject(indexi).isNull("Components")) {
                                    JSONArray compoArray = disArray.getJSONObject(indexi).getJSONArray("Components");
                                    for (int indexj = 0; indexj < compoArray.length(); indexj++) {
                                        ProjectsDetailModel.PropertySize.Discounts.Components components = discounts.new Components();
                                        components.setId(compoArray.getJSONObject(indexj).getInt("Id"));

                                        ArrayList<Integer> subComponents = new ArrayList<>();
                                        JSONArray cast = compoArray.getJSONObject(indexj).getJSONArray("subComponents");
                                        for (int i1 = 0; i1 < cast.length(); i1++) {
                                            int name = cast.getInt(i1);
                                            subComponents.add(name);
                                        }

                                        components.setComponentIds(subComponents);
                                        componentses.add(components);
                                    }
                                }

                                discounts.setComponents(componentses);
                                discountses.add(discounts);
                            }
                            propertySize.setSpecifications(specificationsArrayList);
//                            propertySize.setMinimumPrice(jsonObject1.getInt("MinimumPrice"));
//                            propertySize.setMaximumPrice(jsonObject1.getInt("MaximumPrice"));
//                            propertySize.setApplicableOnCarpetArea(jsonObject1.getBoolean("isCostAppliedCarpetArea"));

                            propertySizes.add(propertySize);

                            ArrayList<Integer> towersList = new ArrayList<>();

                            if (!propertySizeJson.getJSONObject(i).isNull("Towers")) {

                                JSONArray tpwersArray = propertySizeJson.getJSONObject(i).getJSONArray("Towers");

                                for (int indexi = 0; indexi < tpwersArray.length(); indexi++) {

                                    towersList.add(tpwersArray.getInt(indexi));
                                }
                                propertySize.setTowers(towersList);
                            }
                            ArrayList<ProjectsDetailModel.Towers> towerses = new ArrayList<>();

                            if (!resultObject.isNull("Towers")) {

                                JSONArray towersArray = resultObject.getJSONArray("Towers");

                                for (int index = 0; index < towersArray.length(); index++) {

                                    ProjectsDetailModel.Towers towers = projectsDetailModel.new Towers();
                                    towers.setTowerId(towersArray.getJSONObject(index).getInt("TowerId"));
                                    towers.setTowerName(towersArray.getJSONObject(index).getString("TowerName"));
                                    towers.setStories(towersArray.getJSONObject(index).getInt("Stories"));
                                    towers.setBasement(towersArray.getJSONObject(index).getInt("Basement"));
                                    towers.setProjectCategoryId(towersArray.getJSONObject(index).getInt("ProjectCategoryId"));
                                    towers.setPropertyTypeId(towersArray.getJSONObject(index).getInt("PropertyTypeId"));
                                    towers.setFloorType(towersArray.getJSONObject(index).getString("floorType"));
                                    towerses.add(towers);
                                }
                            }

                            projectsDetailModel.setTowers(towerses);


                            ArrayList<ProjectsDetailModel.PropertySize.LayoutPlans> layoutPlanses = new ArrayList<>();


                            if (!propertySizeJson.getJSONObject(i).isNull("LayoutPlans")) {
                                JSONArray layoutArray = propertySizeJson.getJSONObject(i).getJSONArray("LayoutPlans");


                                for (int indexi = 0; indexi < layoutArray.length(); indexi++) {

                                    ProjectsDetailModel.PropertySize.LayoutPlans layoutPlans = propertySize.new LayoutPlans();
                                    layoutPlans.setId(layoutArray.getJSONObject(indexi).getInt("Id"));
                                    layoutPlans.setTitle(layoutArray.getJSONObject(indexi).getString("Title"));
                                    layoutPlans.setBigImage(layoutArray.getJSONObject(indexi).getString("BigImage"));
                                    layoutPlans.setDescription(layoutArray.getJSONObject(indexi).getString("Description"));
                                    layoutPlans.setType(layoutArray.getJSONObject(indexi).getString("Type"));
                                    layoutPlanses.add(layoutPlans);


                                }
                            }
                            propertySize.setLayoutPlans(layoutPlanses);
                        }

                        projectsDetailModel.setPropertyTitles(propertyStringBuilder.length() > 0 ? propertyStringBuilder.substring(0, propertyStringBuilder.length() - 1) : "");

                        projectsDetailModel.setLayoutPlans(layoutPlanses);
                        projectsDetailModel.setAmenities(amenitiesArrayList);
                        projectsDetailModel.setSpecialFeatures(specialFeaturesArrayList);
                        projectsDetailModel.setKeyDistances(keyDistancesArrayList);
                        projectsDetailModel.setFaqs(faQses);
                        projectsDetailModel.setProjectRelationshipManagers(projectRelationshipManagersArrayList);
                        projectsDetailModel.setProjectLenders(projectLendersesModel);
                        projectsDetailModel.setPropertySizes(propertySizes);
                        adapter = new PagerAdapter
                                (getSupportFragmentManager(), tabLayout.getTabCount());
                        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                viewPager.setCurrentItem(tab.getPosition());
                                TabChangeListener fragment = (TabChangeListener) adapter.instantiateItem(viewPager, tab.getPosition());
                                if (fragment != null) {
                                    fragment.onChangeTab(tab.getPosition());
                                }
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });
                        viewPager.setAdapter(adapter);


//                        propertySizes.add(projectsDetailModel);
                    } else if (jsonObject.getInt("StatusCode") == 404) {
                        Toast.makeText(this, "Whoops! No Projects", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(this, "Whoops! No Project found!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onEvent(StatusModel statusModel) {
        id = statusModel.getId();
        if (Build.VERSION.SDK_INT < 23) {
            Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + id));
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(in);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(ProjectDetailActivity.this, "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (!iscallAllowed()) {

                requestStoragePermission();

            } else {
                Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + id));
                try {
                    startActivity(in);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ProjectDetailActivity.this, "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
                }
//

            }
        }
//

    }

    private boolean iscallAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(ProjectDetailActivity.this, Manifest.permission.CALL_PHONE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {

            //If permission is not granted returning false
            return false;
        }
    }

    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(ProjectDetailActivity.this, Manifest.permission.CALL_PHONE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            ActivityCompat.requestPermissions(ProjectDetailActivity.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);


        } else {

            //And finally ask for the permission
            ActivityCompat.requestPermissions(ProjectDetailActivity.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + id));
                    try {
                        startActivity(in);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(ProjectDetailActivity.this, "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(ProjectDetailActivity.this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
}
