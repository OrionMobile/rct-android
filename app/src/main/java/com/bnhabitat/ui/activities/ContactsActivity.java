package com.bnhabitat.ui.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bnhabitat.R;
import com.bnhabitat.data.contacts.ContatctTable;
import com.bnhabitat.models.GroupModel;

import java.util.ArrayList;

public class ContactsActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout, tabLayout1;
    ImageView more_groups;
    ArrayList<String> stringArrayList = new ArrayList<>();
//    ArrayList<NewGroupItemsModel> arrayList = new ArrayList<>();
//    private DatabaseReference myRef, myRef1;
//    private FirebaseDatabase database;
    private String phonenumber;
    ArrayList<String> taskDesList;
    ArrayList<String> map = new ArrayList<>();
    ImageView back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout1 = (TabLayout) findViewById(R.id.tab_layout1);
        more_groups = (ImageView) findViewById(R.id.more_groups);
        back_button = (ImageView) findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ArrayList<GroupModel> groups = ContatctTable.getInstance().getUniquesGroups();
        tabLayout.addTab(tabLayout.newTab().setText("All"));
        for (GroupModel groupModel : groups) {
            tabLayout.addTab(tabLayout.newTab().setText(groupModel.getName()));
            Log.e("Phone", String.valueOf(groupModel.getName())+"");
        }



        tabLayout1.addTab(tabLayout1.newTab().setText("Contacts"));

        tabLayout1.addTab(tabLayout1.newTab().setText("Opportunities"));

        tabLayout1.addTab(tabLayout1.newTab().setText("Inventory"));

        tabLayout1.addTab(tabLayout1.newTab().setText("Campaign & Tasks"));

        tabLayout1.addTab(tabLayout1.newTab().setText("Reports"));

        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimaryDark));
        viewPager = (ViewPager) findViewById(R.id.pager);
        FragmentManager frm = getSupportFragmentManager();
        final MyAdapter adapter = new MyAdapter(frm);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e("helllooo","helllooo");
                if (tab.getPosition() == 0) {

                }
                FragmentChangeInterface fragment = (FragmentChangeInterface) adapter.instantiateItem(viewPager, tab.getPosition());
                if (fragment != null) {
                    fragment.userListfragmentBecameVisible(tab.getPosition(),viewPager);
                }
//                StatusModel statusModel = new StatusModel();
//                EventBus.getDefault().post(statusModel);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        ContactObserver myObserver = new ContactObserver(new Handler());
//        getApplicationContext().getContentResolver().registerContentObserver (Contacts.People.CONTENT_URI, true, myObserver);


    }


    class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fragmentManager) {

            super(fragmentManager);

        }

        @Override
        public Fragment getItem(int position) {

            Fragment frm = null;
//            if (position == 0) {

//                frm = new AllContactsFragment();

//            }
//             else {
////                frm = new ContactsFragment();
//            }

            return frm;
        }

        @Override
        public int getCount() {

            return 2;
        }
    }
    public interface FragmentChangeInterface {
        void userListfragmentBecameVisible(int position, View actionBarView);
        void onPriceChange();
    }
}
