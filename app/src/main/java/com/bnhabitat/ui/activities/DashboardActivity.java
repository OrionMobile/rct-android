package com.bnhabitat.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.projects.AllProjectsTable;
import com.bnhabitat.data.projects.RecentViewProjectsTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.adapters.ProjectListingAdapter;
import com.bnhabitat.ui.fragments.SettingsDialogFragment;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.SecurityEncryption;
import com.bnhabitat.utils.Urls;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.reginald.swiperefresh.CustomSwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import de.greenrobot.event.EventBus;

public class DashboardActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, NavigationView.OnNavigationItemSelectedListener, CommonAsync.OnAsyncResultListener {
    String id;
    private static final int PERMISSION_REQUEST_CODE = 1;
    RecyclerView project_listing_recycler;
    LinearLayoutManager linearLayoutManager;
    RelativeLayout lay_search;
    private ArrayList<ProjectsDetailModel> searchProjectsModels = new ArrayList();
    private ArrayList<ProjectsDetailModel> tempSearchProjectsModels = new ArrayList();
    private GoogleApiClient mGoogleApiClient;
    CustomSwipeRefreshLayout mSwipeRefreshLayout;
    TextView spinner_text, name, phone;
    RelativeLayout setting_menu;
    ImageView home;
    private Spinner search_edit_text;
    private View actionB,action_A;
    ProjectListingAdapter projectListingAdapter;
    String normalTextEnc,normalTextEnc_refer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        project_listing_recycler = (RecyclerView) findViewById(R.id.project_listing_recycler);
        search_edit_text = (Spinner) findViewById(R.id.search_edit_text);
        lay_search = (RelativeLayout) findViewById(R.id.lay_search);
        mSwipeRefreshLayout = (CustomSwipeRefreshLayout) findViewById(R.id.swipelayout);
        actionB = findViewById(R.id.action_b);
        action_A = findViewById(R.id.action_a);
        setting_menu = (RelativeLayout) findViewById(R.id.setting_menu);
        home = (ImageView) findViewById(R.id.home);
//        spinner_text=(TextView) findViewById(R.id.spinner_text);

        try {
            String refer_code= PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,"");
//          String refer_code_google=PreferenceConnector.getInstance(this).loadSavedPreferences("refer","");

            normalTextEnc = SecurityEncryption.encrypt("", refer_code);
            PreferenceConnector.getInstance(this).savePreferences("encrypt",normalTextEnc);

        }catch (Exception e){
         e.printStackTrace();
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);
        name = (TextView) hView.findViewById(R.id.name_user);
        phone = (TextView) hView.findViewById(R.id.phone_num);
//        try{
//            Log.e("Reffreeedddddd","Reffreeedddddd"+PreferenceConnector.getInstance(this).loadSavedPreferences("refer",""));
//        }catch (Exception e){
//
//        }
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        String name_user = PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USERNAME, "");
        name.setText(name_user);
        phone.setText(PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.PHONE_NUMBER, ""));
        if (PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.BROKERUSER_TYPE, "").equalsIgnoreCase("null")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.without_manage);
        } else {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_dashboard_drawer);
        }
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });
        setting_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingsDialogFragment filterDialogFragment = SettingsDialogFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Arraylist", searchProjectsModels);
                filterDialogFragment.setArguments(bundle);
                filterDialogFragment.show(DashboardActivity.this.getSupportFragmentManager(), "dialog_fragment");
            }
        });
        final FloatingActionsMenu menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);

//        tal.add("Mohali");
//
//        tal.add("Zirakpur");
        linearLayoutManager = new LinearLayoutManager(this);
        menuMultipleActions.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {

//                transparentLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {

//                transparentLayout.setVisibility(View.GONE);
            }
        });
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
                Intent intent = new Intent(DashboardActivity.this, AddContatctActivity.class);
                startActivity(intent);

            }
        });
        action_A.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuMultipleActions.collapse();
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "https://bnhabitat.com/app/"+normalTextEnc;
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
//        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this, R.layout.view_spinner_item, tal);
////        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        search_edit_text.setAdapter(arrayAdapter);
        search_edit_text.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView = (TextView) adapterView.getSelectedView();
                try {
                    textView.setTextColor(getResources().getColor(R.color.black));
                    textView.setTextColor(getResources().getColor(R.color.white));
                }catch (Exception e){
                    e.printStackTrace();
                }
                tempSearchProjectsModels.clear();


                if (i == 0) {
                    projectListingAdapter = new ProjectListingAdapter(DashboardActivity.this, DashboardActivity.this, searchProjectsModels);

                    project_listing_recycler.setLayoutManager(linearLayoutManager);

                    project_listing_recycler.setAdapter(projectListingAdapter);
                } else {
                    for (ProjectsDetailModel projectsModel : searchProjectsModels) {

                        if (projectsModel.getCityName().equalsIgnoreCase(adapterView.getSelectedItem().toString())) {

                            tempSearchProjectsModels.add(projectsModel);
                            projectListingAdapter = new ProjectListingAdapter(DashboardActivity.this, DashboardActivity.this, tempSearchProjectsModels);

                            project_listing_recycler.setLayoutManager(linearLayoutManager);

                            project_listing_recycler.setAdapter(projectListingAdapter);
                        }


                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new CustomSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProjectList();
                // do something here when it starts to refresh
                // e.g. to request data from server
            }
        });
//        mSwipeRefreshLayout.setCustomHeadview(new MyCustomHeadView(this));
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        toolbar.setNavigationIcon(null);

        if (AllProjectsTable.getInstance().getProjects()==null||AllProjectsTable.getInstance().getProjects().equalsIgnoreCase("")) {
            getProjectList();


        }else{
            onResultListener(AllProjectsTable.getInstance().getProjects(),
                    Urls.URL_PROJECT_LIST);
        }


        navigationView.setNavigationItemSelectedListener(this);
    }

    public void onEvent(String s) {

        getProjectList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    /*@Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            finish();
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.change_currency) {
//            ChangeCurrencyDialogFragment changeCurrencyDialogFragment = ChangeCurrencyDialogFragment.newInstance();
//            changeCurrencyDialogFragment.show(this.getSupportFragmentManager(), "dialog_fragment");
//            return true;
//        }
//        if (id == R.id.search) {
//            Intent intent=new Intent(DashboardActivity.this,SearchProjectsActivity.class);
//            startActivityForResult(intent, 11);
//            return true;
//        }
//        if (id == R.id.filter) {
//            FilterDialogFragment filterDialogFragment = FilterDialogFragment.newInstance();
//            Bundle bundle2=new Bundle();
//            bundle2.putSerializable("projectModel",searchProjectsModels);
//
//
//            filterDialogFragment.setArguments(bundle2);
//            filterDialogFragment.show(this.getSupportFragmentManager(), "dialog_fragment");
//            return true;
//        }
//        if (id == R.id.set_default_sizes) {
//            SetDefaultSizeDialogFragment filterDialogFragment = SetDefaultSizeDialogFragment.newInstance();
//            filterDialogFragment.show(this.getSupportFragmentManager(), "dialog_fragment");
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.edit_profile) {
            Intent intent = new Intent(DashboardActivity.this, EditProfileActivity.class);
            startActivity(intent);

        } else if (id == R.id.my_invites) {
            Intent intent = new Intent(DashboardActivity.this, MyInvitesActivity.class);
            startActivity(intent);

        }
        else if (id == R.id.calculator) {
            Intent intent = new Intent(DashboardActivity.this, CalculatorActivity.class);
            startActivity(intent);

        }
        else if (id == R.id.contacts_management) {

            Intent intent = new Intent(DashboardActivity.this, MainDashboardActivity.class);
            startActivity(intent);

        } else if (id == R.id.inventery_managment) {

            Intent intent = new Intent(DashboardActivity.this, InventoryDashboardActivity.class);
            startActivity(intent);

        }else if (id == R.id.share) {

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "https://bnhabitat.com/app/"+normalTextEnc;
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        }
        else if (id == R.id.manage_team) {

        } else if (id == R.id.manage_customers) {

        } else if (id == R.id.manage_collections) {

        } else if (id == R.id.reports) {

        } else if (id == R.id.my_account) {

        } else if (id == R.id.logout) {
            LoginManager.getInstance().logOut();
            signOut();
            PreferenceConnector.getInstance(this).savePreferences(Constants.USER_ID, "");
            PreferenceConnector.getInstance(this).savePreferences(Constants.EMAIL, "");
            PreferenceConnector.getInstance(this).savePreferences(Constants.PHONE_NUMBER, "");
            PreferenceConnector.getInstance(this).savePreferences(Constants.USERNAME, "");
            PreferenceConnector.getInstance(this).savePreferences(Constants.REGISTRATION_ID, "");
//
            Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
            startActivity(intent);


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onResultListener(String result, String which) {
        mSwipeRefreshLayout.refreshComplete();
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_PROJECT_LIST)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        if (projectsArray.length() == 0) {
                            Toast.makeText(this, "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                        }
                        lay_search.setVisibility(View.VISIBLE);
                        searchProjectsModels.clear();
                        AllProjectsTable.getInstance().write(
                                result,

                                new Date(),
                                new Date());
                        for (int index = 0; index < projectsArray.length(); index++) {
                            ProjectsDetailModel projectsModel = new ProjectsDetailModel();

                            projectsModel.setId(Integer.parseInt(projectsArray.getJSONObject(index).getString("Id")));
                            projectsModel.setTitle(projectsArray.getJSONObject(index).getString("title"));
                            projectsModel.setLocality(projectsArray.getJSONObject(index).getString("Locality"));
                            projectsModel.setUpdatedOn(projectsArray.getJSONObject(index).getString("UpdatedOn"));
                            projectsModel.setArea(projectsArray.getJSONObject(index).getString("Area"));
                            projectsModel.setAreaUnitId(projectsArray.getJSONObject(index).getString("AreaUnit"));
                            projectsModel.setNumberOfTowers(projectsArray.getJSONObject(index).getString("NumberOfTowers"));
                            projectsModel.setAboutSubProject(projectsArray.getJSONObject(index).getString("aboutSubProject"));
                            projectsModel.setPossessionMonth(Integer.parseInt(projectsArray.getJSONObject(index).getString("PossessionMonth")));
                            projectsModel.setPossessionYear(Integer.parseInt(projectsArray.getJSONObject(index).getString("PossessionYear")));
                            projectsModel.setNumberOfParking(Integer.parseInt(projectsArray.getJSONObject(index).getString("NumberOfParking")));
                            projectsModel.setProjectStatus(projectsArray.getJSONObject(index).getString("ProjectStatus"));
                            projectsModel.setRegion(projectsArray.getJSONObject(index).getString("Region"));
                            projectsModel.setImagePath(projectsArray.getJSONObject(index).getString("logoimage"));
                            projectsModel.setTopImage1(projectsArray.getJSONObject(index).getString("topImage1"));
                            projectsModel.setTypeOfProject(projectsArray.getJSONObject(index).getString("TypeOfProject"));
                            projectsModel.setCompanyLogo(projectsArray.getJSONObject(index).getString("CompanyLogo"));
                            projectsModel.setCityName(projectsArray.getJSONObject(index).getString("CityName"));
                            projectsModel.setBuilderCompanyName(projectsArray.getJSONObject(index).getString("builderCompanyName"));
                            projectsModel.setCurrentOffers(projectsArray.getJSONObject(index).getString("currentOffers"));


                            JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("PropertySize");
                            ArrayList<ProjectsDetailModel.PropertySize> propertySizes = new ArrayList();
                            StringBuilder propertyStringBuilder = new StringBuilder();
                            String propertyString = "";
                            long minPrice = 10000000000l;
                            long maxPrice = 0;

                            for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                                ProjectsDetailModel.PropertySize propertySize = projectsModel.new PropertySize();
                                JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                                propertySize.setId(Integer.parseInt(dataObj.getString("Id")));
                                propertySize.setTitle(dataObj.getString("title"));
                                propertySize.setApplicationForm(dataObj.getString("applicationForm"));
                                propertySize.setProperty_typeIdlevel1(Integer.parseInt(dataObj.getString("property_typeIdlevel1")));
                                propertySize.setProperty_typeIdlevel2(Integer.parseInt(dataObj.getString("property_typeIdlevel2")));
                                propertySize.setProperty_typeIdlevel3(Integer.parseInt(dataObj.getString("property_typeIdlevel3")));
                                propertySize.setFloorPlan(dataObj.getString("floorPlan"));
                                propertySize.setSize(dataObj.getString("size"));
                                propertySize.setSizeUnit(Integer.parseInt(dataObj.getString("sizeUnit")));
                                propertySize.setBuiltarea(dataObj.getString("builtarea"));
                                propertySize.setCarpetarea(dataObj.getString("carpetarea"));
                                propertySize.setSizeUnit2(Integer.parseInt(dataObj.getString("sizeUnit2")));
                                propertySize.setAboutProperty(dataObj.getString("AboutProperty"));
                                propertySize.setLength(dataObj.getString("Length"));
                                propertySize.setBreadth(dataObj.getString("Breadth"));
                                propertySize.setSizeUnit3(Integer.parseInt(dataObj.getString("SizeUnit3")));
                                propertySize.setExtraArea(dataObj.getString("ExtraArea"));
                                if (minPrice > dataObj.getLong("MinimumPrice"))
                                    minPrice = dataObj.getLong("MinimumPrice");

                                if (maxPrice < dataObj.getLong("MaximumPrice"))
                                    maxPrice = dataObj.getLong("MaximumPrice");
                                propertySize.setMinimumPrice(Long.parseLong(String.valueOf(minPrice)));
                                propertySize.setMaximumPrice(Long.parseLong(String.valueOf(maxPrice)));
                                propertySize.setBathRooms(Integer.parseInt(dataObj.getString("BathRooms")));
                                propertySize.setBedRooms(Integer.parseInt(dataObj.getString("BedRooms")));
                                propertySize.setTowerId(dataObj.getString("TowerId"));
                                propertySize.setIsCostAppliedCarpetArea(dataObj.getString("isCostAppliedCarpetArea"));
                                propertySize.setExtraAreaLabel(dataObj.getString("ExtraAreaLabel"));
                                propertySize.setDiscounts(dataObj.getString("Discounts"));
                                propertySize.setDiscountGroups(dataObj.getString("DiscountGroups"));
                                propertySize.setTowers(dataObj.getString("Towers"));


                                propertySizes.add(propertySize);


                            }


                            JSONArray projectRelationshipManagerses = projectsArray.getJSONObject(index).getJSONArray("BrokerStaff");
                            ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList();
                            if (projectRelationshipManagerses.length() != 0) {
                                for (int indexJ = 0; indexJ < projectRelationshipManagerses.length(); indexJ++) {

                                    ProjectsDetailModel.ProjectRelationshipManagers projectRelationshipManagers = projectsModel.new ProjectRelationshipManagers();
                                    JSONObject dataObj = projectRelationshipManagerses.getJSONObject(indexJ);
                                    projectRelationshipManagers.setId(Integer.parseInt(dataObj.getString("Id")));
                                    projectRelationshipManagers.setName(dataObj.getString("Name"));
                                    projectRelationshipManagers.setEmail(dataObj.getString("Email"));
                                    projectRelationshipManagers.setPhoto(dataObj.getString("Photo"));
                                    projectRelationshipManagers.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                    projectRelationshipManagers.setDesignation(dataObj.getString("Designation"));

                                    projectRelationshipManagersArrayList.add(projectRelationshipManagers);
                                }
                            }

                            JSONArray towersJson = projectsArray.getJSONObject(index).getJSONArray("Towers");
                            ArrayList<ProjectsDetailModel.Towers> towerses = new ArrayList();
                            if (towersJson.length() != 0) {
                                for (int indexJ = 0; indexJ < towersJson.length(); indexJ++) {

                                    ProjectsDetailModel.Towers towersProjectModel = projectsModel.new Towers();
                                    JSONObject dataObj = towersJson.getJSONObject(indexJ);
                                    towersProjectModel.setTowerId(Integer.parseInt(dataObj.getString("TowerId")));
                                    towersProjectModel.setBasement(Integer.parseInt(dataObj.getString("Basement")));
                                    towersProjectModel.setFloorType(dataObj.getString("floorType"));
                                    towersProjectModel.setProjectCategoryId(Integer.parseInt(dataObj.getString("ProjectCategoryId")));
                                    towersProjectModel.setPropertyTypeId(Integer.parseInt(dataObj.getString("PropertyTypeId")));
                                    towersProjectModel.setStories(Integer.parseInt(dataObj.getString("Stories")));
                                    towersProjectModel.setTowerName(dataObj.getString("TowerName"));


                                    towerses.add(towersProjectModel);
                                }
                            }

                            projectsModel.setPropertySizes(propertySizes);
                            projectsModel.setProjectRelationshipManagers(projectRelationshipManagersArrayList);
                            projectsModel.setTowers(towerses);
                            searchProjectsModels.add(projectsModel);


                        }

                        projectListingAdapter = new ProjectListingAdapter(DashboardActivity.this, DashboardActivity.this, tempSearchProjectsModels);

                        project_listing_recycler.setLayoutManager(linearLayoutManager);

                        project_listing_recycler.setAdapter(projectListingAdapter);
                        List<String> tal = new ArrayList<String>();
                        for (ProjectsDetailModel projectsModel : searchProjectsModels) {

                            tal.add(projectsModel.getCityName());


                        }
                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(tal);
                        tal.clear();
                        tal.addAll(hashSet);
                        Collections.sort(tal, new Comparator<String>() {
                            public int compare(String obj1, String obj2) {
                                // ## Ascending order
                                return obj1.compareToIgnoreCase(obj2); // To compare string values
                                // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values

                                // ## Descending order
                                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                                // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                            }
                        });
                        tal.add(0, "All Cities");
                        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this, R.layout.view_spinner_item, tal);
                        search_edit_text.setAdapter(arrayAdapter);
                        search_edit_text.setPrompt("Select city");
                        getConvertedCurrency();


//
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Error : No result found!!!", Toast.LENGTH_LONG).show();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_CURRENCY_CONVERTOR)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);


                    if (jsonObject.getString("StatusCode").equalsIgnoreCase("200")) {

                        String resultString = jsonObject.getString("Result");
                        PreferenceConnector.getInstance(getApplication()).savePreferences(Constants.CURRENCY_VALUES, resultString);

                        if (PreferenceConnector.getInstance(DashboardActivity.this).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME, "").equalsIgnoreCase("")) {
                            PreferenceConnector.getInstance(DashboardActivity.this).savePreferences(Constants.DEFAULT_CURRENCY_NAME, "INR");
                            PreferenceConnector.getInstance(DashboardActivity.this).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, 1);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        }

    }

    private void getProjectList() {
        new CommonAsync(this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROJECT_LIST + Constants.COMPAN_ID+ "/" + Constants.APP_ID,
                "",
                "Loading..",
                this,
                Urls.URL_PROJECT_LIST,
                Constants.GET).execute();
    }

    private void getConvertedCurrency() {


        new CommonAsync(DashboardActivity.this,
                Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CURRENCY_CONVERTOR,
                "",
                "",
                this,
                Urls.URL_CURRENCY_CONVERTOR,
                Constants.GET, false).execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 11) {

            if (resultCode == RESULT_OK) {

                lay_search.setVisibility(View.GONE);
                searchProjectsModels = (ArrayList<ProjectsDetailModel>) data.getSerializableExtra("Array_list");

                projectListingAdapter = new ProjectListingAdapter(DashboardActivity.this, DashboardActivity.this, searchProjectsModels);

                project_listing_recycler.setLayoutManager(linearLayoutManager);

                project_listing_recycler.setAdapter(projectListingAdapter);
            }
        }
    }

    public void onEvent(CommonDialogModel commonDialogModel) {

        tempSearchProjectsModels.clear();
        for (ProjectsDetailModel projectsModel : searchProjectsModels) {

            if (projectsModel.getProjectStatus().equalsIgnoreCase(commonDialogModel.getTitle()))
                tempSearchProjectsModels.add(projectsModel);
        }
        projectListingAdapter = new ProjectListingAdapter(DashboardActivity.this, DashboardActivity.this, tempSearchProjectsModels);

        project_listing_recycler.setLayoutManager(linearLayoutManager);

        project_listing_recycler.setAdapter(projectListingAdapter);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        AllProjectsTable.getInstance().deleteAllRecords();
                        RecentViewProjectsTable.getInstance().deleteAllRecords();
                        PreferenceConnector.getInstance(DashboardActivity.this).savePreferences(Constants.USER_ID, "");
                        PreferenceConnector.getInstance(DashboardActivity.this).savePreferences(Constants.EMAIL, "");
                        PreferenceConnector.getInstance(DashboardActivity.this).savePreferences(Constants.PHONE_NUMBER, "");
                        PreferenceConnector.getInstance(DashboardActivity.this).savePreferences(Constants.USERNAME, "");
                        PreferenceConnector.getInstance(DashboardActivity.this).savePreferences(Constants.REGISTRATION_ID, "");
                        Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                        startActivity(intent);
                        // [START_EXCLUDE]
//                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });

    }

    public void onEvent(StatusModel statusModel) {
        id = statusModel.getId();
        if (Build.VERSION.SDK_INT < 23) {
            Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + id));
            try {
                startActivity(in);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(DashboardActivity.this, "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (!iscallAllowed()) {

                requestStoragePermission();

            } else {
                Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + id));
                try {
                    startActivity(in);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(DashboardActivity.this, "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
                }
//

            }
        }
//        if(iscallAllowed()){
//            Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+statusModel.getId()));
//            try {
//                startActivity(in);
//            } catch (android.content.ActivityNotFoundException ex) {
//                Toast.makeText(getActivity(), "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
//            }
//            //If permission is already having then showing the toast
////                    Toast.makeText(context,"You already have the permission",Toast.LENGTH_LONG).show();
//            //Existing the method with return
//            return;
//        }

    }

    private boolean iscallAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.CALL_PHONE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {

            //If permission is not granted returning false
            return false;
        }
    }

    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(DashboardActivity.this, Manifest.permission.CALL_PHONE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);


        } else {

            //And finally ask for the permission
            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + id));
                    try {
                        startActivity(in);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(DashboardActivity.this, "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(DashboardActivity.this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

}
