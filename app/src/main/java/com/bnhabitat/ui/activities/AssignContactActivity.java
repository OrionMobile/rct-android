package com.bnhabitat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.AllFieldsModel;
import com.bnhabitat.models.AllGroupsModel;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.ui.views.MultiSelectionSpinner;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class AssignContactActivity extends AppCompatActivity implements CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener, MultiSelectionSpinner.OnMultipleItemsSelectedListener {

    private Spinner group_spinner,address_type_spinner;
    private MultiSelectionSpinner multiselect_spinner;
    ArrayList<String> allGroupsNames = new ArrayList<>();
    ArrayList<String> allFieldsNames = new ArrayList<>();
    ArrayList<String> allCountriesNames = new ArrayList<>();
    ArrayList<AllGroupsModel> allGroupsModels = new ArrayList<>();
    ArrayList<AllFieldsModel> allFieldsModels = new ArrayList<>();
    ArrayList<AllFieldsModel> allCountriesModels = new ArrayList<>();
    List<Integer> ids = new ArrayList<>();
    AutoCompleteTextView auto_complete_comapny;
    RadioButton group_member,team_member,people_company,can_edit,can_view;
    RadioGroup assign_radio_group;
    RelativeLayout multiselect_spinner_lay,group_spinner_lay,address_type_spinner_lay;
    LinearLayout assign_fields_lay,add_comapany_lay;
    ImageView back_btn;
    Button assign;
    int position;
    ArrayList<String>contact_ids=new ArrayList<>();
    String selectedItem="",contact_ids_string;
    EditText last_name,first_name,email;
    private String field_id_string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_contact);
        group_spinner = (Spinner) findViewById(R.id.group_spinner);
        address_type_spinner = (Spinner) findViewById(R.id.address_type_spinner);
        multiselect_spinner = (MultiSelectionSpinner) findViewById(R.id.multiselectSpinner);
        group_member=(RadioButton)findViewById(R.id.group_member);
        team_member=(RadioButton)findViewById(R.id.team_member);
        people_company=(RadioButton)findViewById(R.id.people_company);
        can_edit=(RadioButton)findViewById(R.id.can_edit);
        can_view=(RadioButton)findViewById(R.id.can_view);
        assign_radio_group=(RadioGroup)findViewById(R.id.assign_radio_group);
        multiselect_spinner_lay=(RelativeLayout)findViewById(R.id.multiselect_spinner_lay);
        group_spinner_lay=(RelativeLayout)findViewById(R.id.group_spinner_lay);
        address_type_spinner_lay=(RelativeLayout)findViewById(R.id.address_type_spinner_lay);
        assign_fields_lay=(LinearLayout)findViewById(R.id.assign_fields_lay);
        add_comapany_lay=(LinearLayout)findViewById(R.id.add_comapany_lay);
        auto_complete_comapny = (AutoCompleteTextView) findViewById(R.id.auto_complete_comapny);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        assign = (Button) findViewById(R.id.assign);
        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        email = (EditText) findViewById(R.id.email);
        multiselect_spinner.setListener(this);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
     selectedItem="GroupMember";
        try{
            contact_ids=getIntent().getStringArrayListExtra("contact_ids");
            contact_ids_string= contact_ids.toString().replace("[","").replace("]","");
            Log.e("contact_ids_string",String.valueOf(contact_ids_string));
        }catch (Exception e){
            e.printStackTrace();
        }
        onGetRequest();
        onFeildsReuest("0");
        onCountriesReuest();
        onUserRequest();
        assign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(selectedItem.equalsIgnoreCase("GroupMember")){

                    onPostGroup();

                }else if(selectedItem.equalsIgnoreCase("TeamMember")){

                    onPostUser();

                }else if(selectedItem.equalsIgnoreCase("PeopleCompany")){

                    onPostInvite();
                }

            }
        });


        group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                position=i;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        auto_complete_comapny.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {

                add_comapany_lay.setVisibility(View.VISIBLE);

            }
        });

        auto_complete_comapny.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                add_comapany_lay.setVisibility(View.GONE);
            }
        });

        assign_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId ) {

                if(checkedId == R.id.group_member) {
                    address_type_spinner_lay.setVisibility(View.VISIBLE);
                    group_spinner_lay.setVisibility(View.VISIBLE);
                    multiselect_spinner_lay.setVisibility(View.GONE);
                    auto_complete_comapny.setVisibility(View.GONE);
                    selectedItem="GroupMember";
                    can_edit.setClickable(false);
                    can_view.setClickable(false);

                } else if(checkedId == R.id.team_member) {
                    address_type_spinner_lay.setVisibility(View.GONE);
                    auto_complete_comapny.setVisibility(View.GONE);
                    multiselect_spinner_lay.setVisibility(View.VISIBLE);
                    group_spinner_lay.setVisibility(View.VISIBLE);
                    selectedItem="TeamMember";
                    can_edit.setClickable(true);
                    can_view.setClickable(true);

                } else {
                    group_spinner_lay.setVisibility(View.GONE);
                    address_type_spinner_lay.setVisibility(View.GONE);
                    multiselect_spinner_lay.setVisibility(View.VISIBLE);
                    auto_complete_comapny.setVisibility(View.VISIBLE);
                    selectedItem="PeopleCompany";
                    can_edit.setClickable(true);
                    can_view.setClickable(true);
                }

            }
        });
        group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(!allFieldsModels.isEmpty()){
                    String id =allGroupsModels.get(i).getId();

                    onFeildsReuest(id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void onPostGroup() {

         new CommonSyncwithoutstatus(AssignContactActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_POST_ASSIGN_GROUP,
                getGroupInputJson(),
                "Loading...",
                this,
                Urls.URL_POST_ASSIGN_GROUP,
                Constants.POST).execute();
    }

    private void onPostUser() {

        new CommonSyncwithoutstatus(AssignContactActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_POST_ASSIGN_USER,
                getUserInputJson(),
                "Loading...",
                this,
                Urls.URL_POST_ASSIGN_USER,
                Constants.POST).execute();
    }

    private void onPostInvite() {

        new CommonSyncwithoutstatus(AssignContactActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_POST_ASSIGN_COMPANY_INVITE,
                getCompanyInviteInputJson(),
                "Loading...",
                this,
                Urls.URL_POST_ASSIGN_COMPANY_INVITE,
                Constants.POST).execute();
    }


    private String getCompanyInviteInputJson() {




        String inputJson = "";
        try {
            if(!ids.isEmpty()){

                StringBuilder stringBuilder = new StringBuilder();
                for(int i=0;i<ids.size();i++){

                    stringBuilder.append(String.valueOf(ids.get(i)+","));

                }
                field_id_string=stringBuilder.toString();
                field_id_string=field_id_string.substring(0,field_id_string.length()-1);
            }
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("CompanyName", auto_complete_comapny.getText().toString());
            jsonObject.put("Email", email.getText().toString() );
            jsonObject.put("FirstName", first_name.getText().toString() );
            jsonObject.put("LastName", last_name.getText().toString());
            jsonObject.put("FromCompanyId", "1" );
            jsonObject.put("ContactIds",contact_ids_string );
            jsonObject.put("ContactFieldIds", field_id_string );
            jsonObject.put("PermissionId", assign_radio_group.getCheckedRadioButtonId() );
            jsonObject.put("UserId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));

//            jsonObject.put("Groups", grouparray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }


    private String getGroupInputJson() {

        String inputJson = "";
        try {

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("ContactIds", contact_ids_string);
            jsonObject.put("GroupIds", allGroupsModels.get(position).getId() );

//            jsonObject.put("Groups", grouparray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

    private String getUserInputJson() {

        String inputJson = "";
        try {

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("ContactIds", contact_ids_string);
            jsonObject.put("AssignUserId",group_spinner.getSelectedItem().toString());
            jsonObject.put("UserId", PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""));
            jsonObject.put("PermissionId", assign_radio_group.getCheckedRadioButtonId());
            jsonObject.put("CompanyId", "1");
            JSONArray jsonArray=new JSONArray();
            for(int i=0;i<ids.size();i++){
                jsonArray.put(ids);
            }

            jsonObject.put("FieldIds", jsonArray);

//            jsonObject.put("Groups", grouparray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }


    private void onGetRequest() {

        new CommonAsync(AssignContactActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_ALL_GROUPS+PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""), "", "Loading...", this, Urls.URL_GET_ALL_GROUPS, Constants.GET).execute();

    }

    private void onUserRequest() {

        new CommonAsync(AssignContactActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_ALL_USERS+PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID,""), "", "Loading...", this, Urls.URL_GET_ALL_USERS, Constants.GET).execute();

    }

    private void onFeildsReuest(String id) {

        new CommonSyncwithoutstatus(AssignContactActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_ALL_FIELDS + "/" + id, "", "Loading...", this, Urls.URL_GET_ALL_FIELDS, Constants.GET).execute();

    }

    private void onCountriesReuest() {

        new CommonAsync(AssignContactActivity.this, Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_COMPANY_NAMES , "", "Loading...", this, Urls.URL_GET_COMPANY_NAMES, Constants.GET).execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_GET_ALL_GROUPS)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONArray projectsArray = jsonObject.getJSONArray("Result");


                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            AllGroupsModel allGroupsModel = new AllGroupsModel();
                            allGroupsModel.setId(jsonObject1.getString("Id"));
                            allGroupsModel.setName(jsonObject1.getString("name"));
                            allGroupsModel.setDescription(jsonObject1.getString("Description"));
                            allGroupsNames.add(allGroupsModel.getName());
                            allGroupsModels.add(allGroupsModel);
                        }

                        if (!allGroupsNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(AssignContactActivity.this, android.R.layout.simple_spinner_item, allGroupsNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            group_spinner.setAdapter(aa);
                        }
                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            else if(which.equalsIgnoreCase(Urls.URL_GET_ALL_FIELDS)){

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        allFieldsNames.clear();
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);
                            AllFieldsModel allFieldsModel = new AllFieldsModel();
                            allFieldsModel.setId(jsonObject1.getString("id"));
                            allFieldsModel.setName(jsonObject1.getString("name"));
                            allFieldsModel.setActive(jsonObject1.getString("Active"));
                            allFieldsModel.setEntityId(jsonObject1.getString("EntityId"));
                            allFieldsModel.setDisplayOrder(jsonObject1.getString("DisplayOrder"));
                            allFieldsModels.add(allFieldsModel);
                            allFieldsNames.add(allFieldsModel.getName());
                        }
                        if (!allFieldsNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(AssignContactActivity.this, android.R.layout.simple_spinner_item, allFieldsNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            address_type_spinner.setAdapter(aa);
                            multiselect_spinner.setItems(allFieldsNames);

                        }
                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else if(which.equalsIgnoreCase(Urls.URL_GET_COMPANY_NAMES)){

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        allCountriesNames.clear();
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);
                            AllFieldsModel allFieldsModel = new AllFieldsModel();
                            allFieldsModel.setId(jsonObject1.getString("Id"));
                            allFieldsModel.setName(jsonObject1.getString("Name"));
                            allCountriesModels.add(allFieldsModel);
                            allCountriesNames.add(allFieldsModel.getName());
                        }
                        if (!allCountriesNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(AssignContactActivity.this, android.R.layout.simple_spinner_item, allCountriesNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            auto_complete_comapny.setAdapter(aa);

                        }
                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }else if(which.equalsIgnoreCase(Urls.URL_GET_ALL_USERS)){

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        allCountriesNames.clear();
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);
                            AllFieldsModel allFieldsModel = new AllFieldsModel();
                            allFieldsModel.setId(jsonObject1.getString("Id"));
                            allFieldsModel.setName(jsonObject1.getString("Name"));
                            allCountriesModels.add(allFieldsModel);
                            allCountriesNames.add(allFieldsModel.getName());
                        }
                        if (!allCountriesNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(AssignContactActivity.this, android.R.layout.simple_spinner_item, allCountriesNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            auto_complete_comapny.setAdapter(aa);

                        }
                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            else if(which.equalsIgnoreCase(Urls.URL_POST_ASSIGN_GROUP)){

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    Utils.showSuccessErrorMessage("Success",jsonObject.getString("Message"),"OK",this);

//                    Toast.makeText(this, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            else if(which.equalsIgnoreCase(Urls.URL_POST_ASSIGN_USER)){

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    Utils.showSuccessErrorMessage("Success",jsonObject.getString("Message"),"OK",this);

//                    Toast.makeText(this, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                        e.printStackTrace();
                }

            }

            else if(which.equalsIgnoreCase(Urls.URL_POST_ASSIGN_COMPANY_INVITE)){

                try {
                    JSONObject jsonObject = new JSONObject(result);

//                    Toast.makeText(this, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                    Utils.showSuccessErrorMessage("Success",jsonObject.getString("Message"),"OK",this);
                } catch (JSONException e) {
                        e.printStackTrace();
                }

            }

        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent (DialogModel dialogModel){

    Intent intent =new Intent(this,MainDashboardActivity.class);
    intent.putExtra(Constants.FROM,"AssignContactActivity");
    startActivity(intent);
}

    @Override
    public void selectedIndices(List<Integer> indices) {
        ids=indices;
    }

    @Override
    public void selectedStrings(List<String> strings) {

    }
}
