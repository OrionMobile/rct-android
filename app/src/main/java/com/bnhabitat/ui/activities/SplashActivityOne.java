package com.bnhabitat.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.StateListing;
import com.bnhabitat.areaModule.areaActivities.Township_Subarea_Project;
import com.bnhabitat.areaModule.areaActivities.WelcomeScreen;
import com.bnhabitat.township.townactivities.LegalActivity;
import com.bnhabitat.township.townactivities.Township;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;


public class SplashActivityOne extends AppCompatActivity {

    private static final int REQUEST_PHONE_CALL = 198;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SplashActivityOne.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
        } else {
            Thread th = new Thread(r);
            th.start();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        Thread th = new Thread(r);
                        th.start();
                    }

                }
        }
    }

    Runnable r = new Runnable() {
        @Override
        public void run() {


            try {
                Thread.sleep(2000);
                if (!PreferenceConnector.getInstance(SplashActivityOne.this).loadSavedPreferences(Constants.USER_ID, "").equalsIgnoreCase("")) {

                    if (!PreferenceConnector.getInstance(SplashActivityOne.this).loadBooleanPreferences(Constants.IS_INTIALIZED)) {
                        Intent i = new Intent(SplashActivityOne.this, IntializeActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else {
                        //Intent i = new Intent(SplashActivityOne.this, DashboardActivity.class);
                        Intent i = new Intent(SplashActivityOne.this, NewDashboardActivity.class);
                        //Intent i = new Intent(SplashActivityOne.this, Township.class);
                        //Intent i = new Intent(SplashActivityOne.this, Township_Subarea_Project.class);
                        //Intent i = new Intent(SplashActivityOne.this, WelcomeScreen.class);
                        //Intent i = new Intent(SplashActivityOne.this, LegalActivity.class);

                        startActivity(i);
                        finish();
                    }


                }
//                    else if (!PreferenceConnector.getInstance(SplashActivityOne.this).loadSavedPreferences(Constants.PIN_SENT, "").equalsIgnoreCase("")) {
//                        Intent i = new Intent(SplashActivityOne.this, OtpVerificationActivity.class);
//                        startActivity(i);
//                        finish();
//                    }
//                else if (!PreferenceConnector.getInstance(SplashActivityOne.this).loadBooleanPreferences(Constants.IS_INTIALIZED)) {
//                    Intent i = new Intent(SplashActivityOne.this, IntializeActivity.class);
//                    startActivity(i);
//                    finish();
//                }

                else {
                    Intent i = new Intent(SplashActivityOne.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    };

}
