package com.bnhabitat.ui.fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.AccomDetailModel;
import com.bnhabitat.models.BankLoanModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.CountryModel;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.PropAreaModel;
import com.bnhabitat.models.PropertyAreasModel;
import com.bnhabitat.models.PropertyLoansModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.activities.MyPropertyActivity;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.MultiTextWatcher;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.greenrobot.event.EventBus;

import static com.bnhabitat.ui.fragments.DetailFragment.accomDetailModel;
import static com.bnhabitat.ui.fragments.DetailFragment.imgModel;
import static com.bnhabitat.ui.fragments.DetailFragment.propAreaModel;
import static com.bnhabitat.ui.fragments.LocationFragment.inventoryModelProp;


public class FinancialandBankLoanFragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener {
    View view, view_bank;
    String price_unit;
    String plot_area_str, edit_property;
    SpinnerAdapter adapter, bankadapter;
    private ArrayList<LinearLayout> linearlayoutArray = new ArrayList();
    private ArrayList<BankLoanModel> bankLoanModels = new ArrayList();
    LinearLayout bank_inflate_layout;
    ImageView next_btn_financial;
    EditText demand_price, price;
    // loan_amount, loan_tenure, total_outstanding
    Spinner price_select;
    // bank_select
    CheckBox all_price_check, if_its_negociable_check, bank_loan_check;
    RadioGroup radio_group_disbursed;
    RadioButton radio_disbursed_yes_no;
    LinearLayout any_other_financial;
    //last_installment
    JSONObject jsonq;
    ArrayList<PropertyUnitsModel> propertyUnitsModels;
    ArrayList<PropertyUnitsModel> bankList = new ArrayList<>();
    SendMessage SM;
    int mYear, mMonth, mDay;
    int selectedradioId, index = 0;
    BankLoanModel currentBankModel = new BankLoanModel();
    String InclusivePrice = "false", IsNegociable = "false", priceunit_txt, bank_id = "", full_disbursed_yes = "false", bank_txt, PropertyId, lastPaidDate = "", loanAmount="", loanTenure = "";
    ArrayList<String> property_units = new ArrayList<>();
    ArrayList<String> bankArray = new ArrayList<>();
    ArrayList<InventoryModel> json_Data;
    private String blockCharacterSet = "~#^|$%&/*!'";
    private JSONObject jsonObject22;
    private JSONObject jsonObject;
    private JSONObject jsonObject1;
    private JSONArray jsonArray1;
    private JSONArray jsonArray13;
    String pid;
    String PropertyTypeId;
    private JSONObject jsonSpecification;
    private ArrayList<PropAreaModel> financeArrayList;
    private String Editid;
    int priceunit_id;
    RecyclerView rv_addmorebankloan;
    AddMoreFinancialAdapter addMoreFinancialAdapter;
    LinearLayoutManager linearLayoutManager;
    LinearLayout linear_layout;
    NestedScrollView scroll_view;
    boolean isDateSelected = false, isBankSelected = false, isLoanAmount = false, isLoanTenure = false;
    int iskeyboaropen=0;

    String PropFinaId =" ";
    private ArrayList<PropertyLoansModel> propLoansList = new ArrayList();
   // private ArrayList<PropertyLoansModel> propLoansLocal = new ArrayList();

    public FinancialandBankLoanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_financialand_bank_loan, container, false);
        onIntializeView(view);
      //  OnAdding();
        blockChar();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            try {
                pid = bundle.getString("pid");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (bundle != null) {
            try {
                PropertyId = bundle.getString("PropertyId") == null ? "" : bundle.getString("PropertyId");
                PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");
                propertyUnitsModels = (ArrayList<PropertyUnitsModel>) bundle.getSerializable("propertyunits");

                jsonq = new JSONObject(bundle.getString("jsonObject"));
                Log.e("jsonqjsonq", "jsonq" + jsonq.toString());

            } catch (Exception e) {

            }
            try {
                edit_property = bundle.getString("edit") == null ? "" : bundle.getString("edit");
                if (!edit_property.equals("")) {
                    Editid = bundle.getString("editId");

                    json_Data = (ArrayList<InventoryModel>) bundle.getSerializable("json_Data");
                    for (int i = 0; i < json_Data.size(); i++) {

                        if (json_Data.get(i).getId().equalsIgnoreCase(Editid)) {

                            demand_price.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyFinancialses().get(0).getDemandPrice()));
                            price.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyFinancialses().get(0).getPrice()));
                            all_price_check.setChecked(json_Data.get(i).getPropertyFinancialses().get(0).getInclusivePrice());
                            if_its_negociable_check.setChecked(json_Data.get(i).getPropertyFinancialses().get(0).isNegociable());
                            PropFinaId = json_Data.get(i).getPropertyFinancialses().get(0).getId();

                            for (int k = 0; k < propertyUnitsModels.size(); k++) {

                                if (json_Data.get(i).getPropertyFinancialses().get(0).getPriceUnitId() == propertyUnitsModels.get(k).getPriceUnitId()) {

                                    price_select.setSelection(k);

                                }

                            }

                            if (json_Data.get(i).getPropertyLoanses() != null && json_Data.get(i).getPropertyLoanses().size() > 0) {

                                bank_loan_check.setChecked(true);

                                rv_addmorebankloan.setVisibility(View.VISIBLE);
                                any_other_financial.setVisibility(View.VISIBLE);

                                propLoansList.clear();

                                for (int k = 0; k < json_Data.get(i).getPropertyLoanses().size(); k++) {

                                    PropertyLoansModel propertyLoans = new PropertyLoansModel();
                                    propertyLoans.setId(String.valueOf(json_Data.get(i).getPropertyLoanses().get(k).getId()));
                                    propertyLoans.setBankId("1");
                                    propertyLoans.setLoanAmount(json_Data.get(i).getPropertyLoanses().get(k).getLoanAmount());
                                    propertyLoans.setLoanTenure(json_Data.get(i).getPropertyLoanses().get(k).getLoanTenure());
                                    propertyLoans.setFullDisbursed(Boolean.parseBoolean(json_Data.get(i).getPropertyLoanses().get(k).getFullDisbursed()));
                                    propertyLoans.setLastInstallmentPaidOn(json_Data.get(i).getPropertyLoanses().get(k).getLastInstallmentPaidOn());
                                    propertyLoans.setTotalOutstandingAfterLastInstallment(json_Data.get(i).getPropertyLoanses().get(k).getTotalOutstandingAfterLastInstallment());
                                    propLoansList.add(propertyLoans);

                                }

                                addMoreFinancialAdapter = new AddMoreFinancialAdapter(getActivity(),propLoansList);
                                rv_addmorebankloan.setAdapter(addMoreFinancialAdapter);

                            } else {

                                bank_loan_check.setChecked(false);
                                rv_addmorebankloan.setVisibility(View.GONE);
                                any_other_financial.setVisibility(View.GONE);

                            }
                        }

                    }
                }
            } catch (Exception e) {

                e.printStackTrace();

            }

            linearLayoutManager = new LinearLayoutManager(getActivity());
            rv_addmorebankloan.setLayoutManager(linearLayoutManager);
            rv_addmorebankloan.setAdapter(addMoreFinancialAdapter);

            for (int i = 0; i < propertyUnitsModels.size(); i++) {
                property_units.add(propertyUnitsModels.get(i).getSizeUnitId());
            }

            adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
            adapter.addAll(property_units);
            adapter.add(getString(R.string.select_type));
            price_select.setAdapter(adapter);
            price_select.setSelection(adapter.getCount());
            if (price_unit != null) {
                int spinnerPos = adapter.getPosition(price_unit);
                price_select.setSelection(spinnerPos);
            }
        }

        demand_price.addTextChangedListener(onTextChangedListener());
        price.addTextChangedListener(onTextChangedListener1());
        String plot_unit_str = PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_UNIT_AREA, "");
        plot_area_str = PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_AREA, "");

        demand_price.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                    //do something
                    try {
                        double price_val = Double.parseDouble(demand_price.getText().toString()) / Double.parseDouble(plot_area_str);
                        price.setText(Utils.getConvertedPrice(price_val));
                    } catch (Exception e) {

                    }
                }
                return false;
            }
        });

        getAllbank();

        if (plot_unit_str != null) {
            int spinnerPos = adapter.getPosition(plot_unit_str);

            price_select.setSelection(spinnerPos);
        }

        price_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (price_select.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    priceunit_txt = price_select.getSelectedItem().toString();
                    priceunit_id = propertyUnitsModels.get(position).getPriceUnitId();
                    Log.e("chooseText", "chooseText" + priceunit_txt.toLowerCase());

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        bank_loan_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    //   bank_inflate_layout.setVisibility(View.VISIBLE);
                    rv_addmorebankloan.setVisibility(View.VISIBLE);
                    any_other_financial.setVisibility(View.VISIBLE);

                    if (propLoansList.size()==0)
                        OnAdding();
                } else {
                    // bank_inflate_layout.setVisibility(View.GONE);
                    any_other_financial.setVisibility(View.GONE);
                    rv_addmorebankloan.setVisibility(View.GONE);

                }
            }
        });

        any_other_financial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bank_loan_check.isChecked()) {

                    if (propLoansList.size()>0){

                        if(propLoansList.get(propLoansList.size()-1).getBankId().equalsIgnoreCase(""))
                            Utils.showErrorMessage("Select the Bank Please", getActivity());
                        else if (propLoansList.get(propLoansList.size()-1).getLoanAmount().equalsIgnoreCase(""))
                            Utils.showErrorMessage("Fill Loan Amount Please", getActivity());
                        else if(propLoansList.get(propLoansList.size()-1).getLoanTenure().equalsIgnoreCase(""))
                            Utils.showErrorMessage("Fill Loan Tenure Please", getActivity());
                        else if (propLoansList.get(propLoansList.size()-1).getLastInstallmentPaidOn().equalsIgnoreCase(""))
                            Utils.showErrorMessage("Select the Last Installment Paid Date Please", getActivity());
                        else if(propLoansList.get(propLoansList.size()-1).getTotalOutstandingAfterLastInstallment().equalsIgnoreCase(""))
                            Utils.showErrorMessage("Fill the Total Outstanding after Last Installment Please", getActivity());
                        else{
                            isDateSelected = false;
                            isBankSelected = false;
                            OnAdding();
                        }
                    }

                    else
                        OnAdding();


                }

                }
        });

/*
        next_btn_financial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (demand_price != null && demand_price.getText().toString().trim().equalsIgnoreCase("")) {

                    Utils.showErrorMessage("Enter your demand price", getActivity());

                } else if (demand_price.getText().toString().trim().equalsIgnoreCase("0")) {

                    Utils.showErrorMessage("Demand price can't be zero", getActivity());
                } else if (price.getText().toString().trim().equalsIgnoreCase("0")) {

                    Utils.showErrorMessage("Price can't be zero", getActivity());

                }

                else if (price.getText().toString().trim().equalsIgnoreCase("0.0")) {

                    Utils.showErrorMessage("Price can't be zero", getActivity());

                }
                else if(bank_loan_check.isChecked()) {

                    if(propLoansList!=null && propLoansList.size()>0){

                        if(!propLoansList.get(propLoansList.size()-1).getLoanAmount().equalsIgnoreCase("") ||
                                !propLoansList.get(propLoansList.size()-1).getLoanTenure().equalsIgnoreCase("")
                                || !propLoansList.get(propLoansList.size()-1).getTotalOutstandingAfterLastInstallment().equalsIgnoreCase("")){
                            if(isBankSelected && isDateSelected )
                            {

                                checkBoxData();
                            }
                            onFinanaceBankdata();

                        }

                     else{
                            if(isBankSelected && isDateSelected) {
                                PropertyLoansModel propertyLoansModel = new PropertyLoansModel();
                                propertyLoansModel.setBankId(bank_id);
                                propertyLoansModel.setLoanAmount("");
                                propertyLoansModel.setLoanTenure("");
                                propertyLoansModel.setFullDisbursed(false);
                                propertyLoansModel.setLastInstallmentPaidOn(lastPaidDate);
                                propertyLoansModel.setTotalOutstandingAfterLastInstallment("");
                                propLoansList.set(propLoansList.size()-1, propertyLoansModel);
                                checkBoxData();
                                onFinanaceBankdata();
                            }


                            else if(isBankSelected && !isDateSelected){
                                Utils.showErrorMessage("Please Select the Last Installment Paid Date", getActivity());
                            }else if(!isBankSelected && isDateSelected){
                                Utils.showErrorMessage("Please Select the Bank First", getActivity());
                            }


                        }
                    }

                    else {


                        checkBoxData();
                        onFinanaceBankdata();

                    }
                }
                else{

                    checkBoxData();
                    onFinanaceBankdata();

                }

            }
        });
*/

next_btn_financial.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        onFinanaceBankdata();

    }
});

        return view;
    }

    private void checkBoxData() {
        if (all_price_check.isChecked()) {
            InclusivePrice = "true";
        }
        if (if_its_negociable_check.isChecked()) {
            IsNegociable = "true";
        }
    }

    private void blockChar() {
        demand_price.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(12)});
        price.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(12)});
    }

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
            ((PropertyItemActivity) getActivity()).save_draft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (demand_price != null && demand_price.getText().toString().trim().equalsIgnoreCase("")) {

                        Utils.showErrorMessage("Enter your demand price", getActivity());

                    } else if (demand_price.getText().toString().trim().equalsIgnoreCase("0")) {

                        Utils.showErrorMessage("Demand price can't be zero", getActivity());
                    } else if (price.getText().toString().trim().equalsIgnoreCase("0")) {

                        Utils.showErrorMessage("Price can't be zero", getActivity());

                    } else {

                        checkBoxData();
                        onFinanaceDraftBankdata();

                    }
                }
            });
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    private void onIntializeView(View view) {
        addMoreFinancialAdapter = new AddMoreFinancialAdapter(getActivity());
        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);
        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);
        next_btn_financial = (ImageView) view.findViewById(R.id.next_btn_financial);
        price = (EditText) view.findViewById(R.id.price);
        demand_price = (EditText) view.findViewById(R.id.demand_price);
        any_other_financial = (LinearLayout) view.findViewById(R.id.any_other_financial);
        all_price_check = (CheckBox) view.findViewById(R.id.all_price_check);
        if_its_negociable_check = (CheckBox) view.findViewById(R.id.if_its_negociable_check);
        bank_loan_check = (CheckBox) view.findViewById(R.id.bank_loan_check);
        price_select = (Spinner) view.findViewById(R.id.price_select);
        rv_addmorebankloan = (RecyclerView) view.findViewById(R.id.rv_addmorebankloan);

   /*     linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(iskeyboaropen==1) {
                    hideSoftKeyboard(getActivity());
                    iskeyboaropen=0;
                }
            }
        });
*/
        linear_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = linear_layout.getRootView().getHeight() - linear_layout.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");

                    iskeyboaropen=1;
                } else {
                    Log.e("MyActivity", "keyboard closed");
                    iskeyboaropen=0;
                }
            }
        });

/*
        scroll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              hideSoftKeyboard(getActivity());
            }
        });
*/


    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    public void onFinanaceBankdata() {


        try {

            jsonObject1 = new JSONObject();

            jsonSpecification = new JSONObject();
            jsonSpecification.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("IsDraft", 0);

            if (!edit_property.equals("")) {

                jsonObject1.put("IsModify", true);

            } else {
                jsonObject1.put("IsModify", true);

            }

            if (!edit_property.equals("")) {

                jsonSpecification.put("Id", Editid);

            } else {

                jsonSpecification.put("Id", pid);

            }

            JSONArray jsonArray = new JSONArray();
            JSONArray bankArray = new JSONArray();

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("DemandPrice", demand_price.getText().toString());
            jsonObject1.put("InclusivePrice", InclusivePrice);
            jsonObject1.put("IsNegociable", IsNegociable);
            jsonObject1.put("Price", price.getText().toString());
            jsonObject1.put("PriceUnitId", priceunit_id);
            if (PropFinaId.equalsIgnoreCase(""))
            jsonObject1.put("Id","0" );
            else
                jsonObject1.put("Id", PropFinaId);

            jsonObject1.put("IsModify", true);

            if (!edit_property.equals("")) {

                jsonObject1.put("IsModify", true);

            } else {
                jsonObject1.put("IsModify", false);

            }
            jsonArray.put(jsonObject1);

            // sending data in api

            if (bank_loan_check.isChecked()) {

                for (int i = 0; i < propLoansList.size(); i++) {

                    JSONObject bankJson = new JSONObject();
                    bankJson.put("Id", propLoansList.get(i).getId());
                    bankJson.put("BankId", propLoansList.get(i).getBankId());
                    bankJson.put("LoanAmount", propLoansList.get(i).getLoanAmount());
                    bankJson.put("LoanTenure", propLoansList.get(i).getLoanTenure());
                    bankJson.put("FullDisbursed", propLoansList.get(i).getFullDisbursed());
                    bankJson.put("LastInstallmentPaidOn", propLoansList.get(i).getLastInstallmentPaidOn());
                    bankJson.put("TotalOutstandingAfterLastInstallment", propLoansList.get(i).getTotalOutstandingAfterLastInstallment());
                    bankJson.put("IsModify", true);


                    bankArray.put(bankJson);
                }

            }

            jsonSpecification.put("PropertyFinancials", jsonArray);
            jsonSpecification.put("PropertyLoans", bankArray);

            postProperty();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onFinanaceDraftBankdata() {

        try {

            jsonSpecification = new JSONObject();
            if (!edit_property.equals("")) {

                jsonSpecification.put("Id", Editid);

            } else {

                jsonSpecification.put("Id", pid);

            }
            jsonSpecification.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("IsDraft", 1);

            JSONArray bankArray = new JSONArray();

            if (bank_loan_check.isChecked()) {

                for (int i = 0; i < propLoansList.size(); i++) {

                    JSONObject bankJson = new JSONObject();
                    bankJson.put("Id", 0);
                    bankJson.put("BankId", bankLoanModels.get(i).getBank_id());
                    bankJson.put("LoanAmount", bankLoanModels.get(i).getLoan_amount());
                    bankJson.put("LoanTenure", bankLoanModels.get(i).getLoan_tenure());
                    bankJson.put("FullDisbursed", bankLoanModels.get(i).getDisbursed());
                    bankJson.put("LastInstallmentPaidOn", bankLoanModels.get(i).getLast_instalment());
                    bankJson.put("TotalOutstandingAfterLastInstallment", bankLoanModels.get(i).getTotal_outstanding());

                    if (!edit_property.equals("")) {

                        jsonObject1.put("IsModify", true);

                    } else {
                        jsonObject1.put("IsModify", false);

                    }
                    bankArray.put(bankJson);
                }

            }


            jsonSpecification.put("PropertyLoans", bankArray);

            postProperty_draft();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void postProperty() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_POST,
                Constants.POST).execute();

    }

    private void postProperty_draft() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Constants.URL_PROPERTY_POST_DRAFT,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = jsonSpecification.toString();
        } catch (Exception e) {

        }

        return inputStr;
    }

    private void getAllbank() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_BANKS,
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_BANKS,
                Constants.GET).execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_BANKS)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {

                        } else {


                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                PropertyUnitsModel bankdata = new PropertyUnitsModel();

                                bankdata.setId(jsonObject1.getString("Id"));
                                bankdata.setSizeUnitId(jsonObject1.getString("Name"));
                                bankList.add(bankdata);
                                bankadapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);

                                bankArray.add(bankList.get(index).getSizeUnitId());
                                bankadapter.addAll(bankArray);
                                bankadapter.add(getString(R.string.select_type));
                                addMoreFinancialAdapter.notifyDataSetChanged();

                            }

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Constants.URL_PROPERTY_POST_DRAFT)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");
                            String message = jsonObject1.getString("Message");
                            Utils.showSuccessErrorMessage("Success", "Draft added successfully", "Ok", getActivity());


                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");
                            String message = jsonObject1.getString("Message");
                            Toast.makeText(getActivity(), "Inventory updated successfully", Toast.LENGTH_SHORT).show();
                            if (PropertyTypeId.equals("1") || PropertyTypeId.equalsIgnoreCase("7") || PropertyTypeId.equalsIgnoreCase("8"))

                                SM.sendData(pid, 3);
                            else {
                                SM.sendData(pid, 5);
                            }


                        } else {

                           Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private TextWatcher onTextChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                demand_price.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    demand_price.setText(formattedString);
                    demand_price.setSelection(demand_price.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                demand_price.addTextChangedListener(this);
            }
        };
    }

    private TextWatcher onTextChangedListener1() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                price.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    price.setText(formattedString);
                    price.setSelection(price.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                price.addTextChangedListener(this);
            }
        };
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(getActivity(), MyPropertyActivity.class);
            intent.putExtra("from_savedrafts", "from_savedrafts");

            startActivity(intent);
        }
    }

    public void onEvent(ArrayList<PropertyLoansModel> propertyLoansModel ) {

        //propLoansLocal.clear();

        this.propLoansList= propertyLoansModel;

/*
        for (int i = 0; i < propertyLoansModel.size(); i++) {
            PropertyLoansModel featureModel = new PropertyLoansModel();
            featureModel.setId(propertyLoansModel.get(i).getId());
            featureModel.setBankId(propertyLoansModel.get(i).getBankId());
            featureModel.setLoanAmount(propertyLoansModel.get(i).getLoanAmount());
            featureModel.setLoanTenure(propertyLoansModel.get(i).getLoanTenure());
            featureModel.setFullDisbursed(propertyLoansModel.get(i).getFullDisbursed());
            featureModel.setLastInstallmentPaidOn(propertyLoansModel.get(i).getLastInstallmentPaidOn());
            featureModel.setTotalOutstandingAfterLastInstallment(propertyLoansModel.get(i).getTotalOutstandingAfterLastInstallment());

            propLoansList.add(featureModel);

        }
*/

    }

    public void OnAdding() {

        PropertyLoansModel propertyLoans = new PropertyLoansModel();
        propertyLoans.setBankId("1");
        propertyLoans.setId("0");
        propertyLoans.setLoanAmount("");
        propertyLoans.setLoanTenure("");
        propertyLoans.setFullDisbursed(false);
        propertyLoans.setLastInstallmentPaidOn("");
        propertyLoans.setTotalOutstandingAfterLastInstallment("");
        propLoansList.add(propertyLoans);
        addMoreFinancialAdapter = new AddMoreFinancialAdapter(getActivity(), propLoansList);
        rv_addmorebankloan.setAdapter(addMoreFinancialAdapter);
      //addMoreFinancialAdapter.notifyDataSetChanged();

    }



    public class AddMoreFinancialAdapter extends RecyclerView.Adapter<AddMoreFinancialAdapter.ViewHolder> {

        Context context;
        ArrayList<PropertyLoansModel> propLoansList = new ArrayList<>();
        ArrayList<PropertyLoansModel> propLoansLocalList = new ArrayList<>();

        public AddMoreFinancialAdapter(FragmentActivity activity) {
            this.context = activity;

        }

        public AddMoreFinancialAdapter(FragmentActivity activity, ArrayList<PropertyLoansModel> propLoansList1) {
            this.context = activity;
            this.propLoansList = propLoansList1;
           this.propLoansLocalList = propLoansList1;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.bank_inflate_layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.loan_amount.setText(propLoansList.get(position).getLoanAmount());
            holder.loan_tenure.setText(propLoansList.get(position).getLoanTenure());
            holder.total_outstanding.setText(propLoansList.get(position).getTotalOutstandingAfterLastInstallment());

            holder.bank_select.setAdapter(bankadapter);

            if (propLoansList.get(position).getLastInstallmentPaidOn().matches("([0-9]{2})-([0-9]{2})-([0-9]{4})"))
                holder.last_installment.setText(propLoansList.get(position).getLastInstallmentPaidOn());
            else
                holder.last_installment.setText(Utils.getMessageDate(propLoansList.get(position).getLastInstallmentPaidOn()));


            if (propLoansList.get(position).getFullDisbursed()) {
                holder.radio_disbursed_yes.setChecked(true);
                holder.radio_disbursed_no.setChecked(false);

            } else {
                holder.radio_disbursed_yes.setChecked(false);
                holder.radio_disbursed_no.setChecked(true);

            }

            if (bank_loan_check.isChecked()) {

                if (!holder.loan_amount.getText().toString().trim().equalsIgnoreCase("")) {

                    if (holder.loan_amount.getText().toString().trim().equalsIgnoreCase("0")) {

                        Utils.showErrorMessage("Loan Amount can't be zero", getActivity());
                    }
                }
                if (!holder.loan_tenure.getText().toString().trim().equalsIgnoreCase("")) {

                    if (holder.loan_tenure.getText().toString().trim().equalsIgnoreCase("0")) {

                        Utils.showErrorMessage("Loan Tenure can't be zero", getActivity());
                    }
                }


            }

            holder.last_installment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Calendar mcurrentDate1 = Calendar.getInstance();
                    mYear = mcurrentDate1.get(Calendar.YEAR);
                    mMonth = mcurrentDate1.get(Calendar.MONTH);
                    mDay = mcurrentDate1.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog mDatePicker1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                            isDateSelected = true;
                            holder.last_installment.setText((selectedmonth + 1) + "/" + selectedday + "/" + selectedyear);
                            lastPaidDate = (selectedmonth + 1) + "/" + selectedday + "/" + selectedyear;
                        }
                    }, mYear, mMonth, mDay);
                    mDatePicker1.setTitle("Last installment paid");
                    mDatePicker1.getDatePicker().setMaxDate(System.currentTimeMillis());
                    mDatePicker1.show();
                }
            });


            holder.bank_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    // TODO Auto-generated method stub

                    if (holder.bank_select.getSelectedItem() == getString(R.string.select_type)) {

                        //Do nothing.
                    } else {
                        isBankSelected = true;
                        bank_txt = holder.bank_select.getSelectedItem().toString();
                        bank_id = bankList.get(position).getId();

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

            ////////////////////// send data  //////////////////////

            holder.loan_amount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    isLoanAmount = true;
                    loanAmount = holder.loan_amount.getText().toString();
                    propLoansLocalList.get(position).setId(propLoansList.get(position).getId());
                    propLoansLocalList.get(position).setBankId(propLoansList.get(position).getBankId());
                    propLoansLocalList.get(position).setLoanAmount(holder.loan_amount.getText().toString());
                    propLoansLocalList.get(position).setLoanTenure(holder.loan_tenure.getText().toString());
                  //  propLoansLocalList.get(position).setFullDisbursed(holder..getText().toString());
                    propLoansLocalList.get(position).setLastInstallmentPaidOn(holder.last_installment.getText().toString());
                    propLoansLocalList.get(position).setTotalOutstandingAfterLastInstallment(holder.total_outstanding.getText().toString());

                    EventBus.getDefault().post(propLoansLocalList);

                }
            });


            holder.loan_tenure.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    isLoanTenure= true;
                    loanTenure = holder.loan_tenure.getText().toString();
                    propLoansLocalList.get(position).setId(propLoansList.get(position).getId());
                    propLoansLocalList.get(position).setBankId(propLoansList.get(position).getBankId());
                    propLoansLocalList.get(position).setLoanAmount(holder.loan_amount.getText().toString());
                    propLoansLocalList.get(position).setLoanTenure(holder.loan_tenure.getText().toString());
                    //  propLoansLocalList.get(position).setFullDisbursed(holder..getText().toString());
                    propLoansLocalList.get(position).setLastInstallmentPaidOn(holder.last_installment.getText().toString());
                    propLoansLocalList.get(position).setTotalOutstandingAfterLastInstallment(holder.total_outstanding.getText().toString());

                    EventBus.getDefault().post(propLoansLocalList);

                }
            });


            holder.last_installment.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    propLoansLocalList.get(position).setId(propLoansList.get(position).getId());
                    propLoansLocalList.get(position).setBankId(propLoansList.get(position).getBankId());
                    propLoansLocalList.get(position).setLoanAmount(holder.loan_amount.getText().toString());
                    propLoansLocalList.get(position).setLoanTenure(holder.loan_tenure.getText().toString());
                    //  propLoansLocalList.get(position).setFullDisbursed(holder..getText().toString());
                    propLoansLocalList.get(position).setLastInstallmentPaidOn(holder.last_installment.getText().toString());
                    propLoansLocalList.get(position).setTotalOutstandingAfterLastInstallment(holder.total_outstanding.getText().toString());

                    EventBus.getDefault().post(propLoansLocalList);

                }
            });

            holder.total_outstanding.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    propLoansLocalList.get(position).setId(propLoansList.get(position).getId());
                    propLoansLocalList.get(position).setBankId(propLoansList.get(position).getBankId());
                    propLoansLocalList.get(position).setLoanAmount(holder.loan_amount.getText().toString());
                    propLoansLocalList.get(position).setLoanTenure(holder.loan_tenure.getText().toString());
                    //  propLoansLocalList.get(position).setFullDisbursed(holder..getText().toString());
                    propLoansLocalList.get(position).setLastInstallmentPaidOn(holder.last_installment.getText().toString());
                    propLoansLocalList.get(position).setTotalOutstandingAfterLastInstallment(holder.total_outstanding.getText().toString());

                    EventBus.getDefault().post(propLoansLocalList);

                }
            });

        }

        @Override
        public int getItemCount() {

            return propLoansList.size();

        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            Spinner bank_select;
            EditText loan_amount, loan_tenure, total_outstanding;
            RadioButton radio_disbursed_yes, radio_disbursed_no;
            TextView last_installment;

            public ViewHolder(View item) {
                super(item);
                bank_select = (Spinner) item.findViewById(R.id.bank_select);
                loan_amount = (EditText) item.findViewById(R.id.loan_amount);
                loan_tenure = (EditText) item.findViewById(R.id.loan_tenure);
                radio_disbursed_yes = (RadioButton) item.findViewById(R.id.radio_disbursed_yes);
                radio_disbursed_no = (RadioButton) item.findViewById(R.id.radio_disbursed_no);
                last_installment = (TextView) item.findViewById(R.id.last_installment);
                total_outstanding = (EditText) item.findViewById(R.id.total_outstanding);
            }
        }

    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

}
