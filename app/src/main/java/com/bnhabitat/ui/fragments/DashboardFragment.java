package com.bnhabitat.ui.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.DashboardGraphModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.eazegraph.lib.charts.BarChart;
import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.charts.StackedBarChart;
import org.eazegraph.lib.charts.ValueLineChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.PieModel;
import org.eazegraph.lib.models.ValueLinePoint;
import org.eazegraph.lib.models.ValueLineSeries;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//import org.eazegraph.lib.charts.BarChart;

/**
 *
 */
public class DashboardFragment extends Fragment implements CommonAsync.OnAsyncResultListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    TextView contact_source_name;
    LinearLayout color_code;
    //    private CombinedChart mChart;
    private final int itemcount = 12;
    ArrayList<String> areaLineDataDays = new ArrayList();
    ArrayList<String> areaLineDatasContacts = new ArrayList();
    ArrayList<String> sourceTitleDatas = new ArrayList();
    ArrayList<String> SourceValueDatas = new ArrayList();
    ArrayList<String> simpleBarDatas = new ArrayList();
    String[] colorCodes = {
            "#0044AA",
            "#449DEF",
            "#2F6699",
            "#70C656",
            "#53933F",
            "#F3AE1B",
            "#BB6008",
            "#000000"
    };

    DashboardGraphModel dashboardGraphModel;
    BarChart mBarChart;
    PieChart mPieChart;
    ValueLineChart mCubicValueLineChart;
    StackedBarChart mStackedBarChart;
    View view;
    //    BarChart barChart ;
    TextView total_contact_pipeline, total_contact_funnel;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        mBarChart = (BarChart) view.findViewById(R.id.barchart);
        mPieChart = (PieChart) view.findViewById(R.id.piechart);
        mCubicValueLineChart = (ValueLineChart) view.findViewById(R.id.cubiclinechart);
        total_contact_funnel = (TextView) view.findViewById(R.id.total_contact_funnel);
        total_contact_pipeline = (TextView) view.findViewById(R.id.total_contact_pipeline);
        mStackedBarChart = (StackedBarChart) view.findViewById(R.id.stackedbarchart);
//        barChart = (BarChart) view.findViewById(R.id.bar_chart);

        onSyncRequest();
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.e("activitycreatedCalled", "activitycreatedCalled");
    }

    private void onSourceAdd(String name, String value, int color) {

        LayoutInflater inflator = LayoutInflater.from(getActivity());
        LinearLayout parentPanel = (LinearLayout) view.findViewById(R.id.contact_source_lay);


//        parentPanel.setId(i);
//
//                for(String data : myList) {
        // inflate child
        View item = inflator.inflate(R.layout.contact_source_items, null);
        // initialize review UI
        contact_source_name = (TextView) item.findViewById(R.id.contact_source_name);
        color_code = (LinearLayout) item.findViewById(R.id.color_code);
        contact_source_name.setText(name + " " + value);
        color_code.setBackgroundColor(color);

//                    // set data
//                    dataText.setText(data);
        // add child


        parentPanel.addView(item);
//        linearlayoutArray.add(parentPanel);

//                }

    }

    private void onSyncRequest() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_DASHBOARD_GRAPH_DATA + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""), "", "Loading...", this, Urls.URL_GET_DASHBOARD_GRAPH_DATA, Constants.GET).execute();

//
    }


    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_GET_DASHBOARD_GRAPH_DATA)) {


                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {

                        String msg = "";


                        msg = jsonObject.getString("Message");
//                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        dashboardGraphModel = new DashboardGraphModel();
                        String TotalContacts = jsonObject1.getString("TotalContacts");
                        String ContactsLast7days = jsonObject1.getString("ContactsLast7days");
                        String ContactsLast30days = jsonObject1.getString("ContactsLast30days");
                        String TotalUsers = jsonObject1.getString("TotalUsers");
                        dashboardGraphModel.setTotalContacts(TotalContacts);
                        dashboardGraphModel.setContactsLast7days(ContactsLast7days);
                        dashboardGraphModel.setContactsLast30days(ContactsLast30days);
                        dashboardGraphModel.setTotalUsers(TotalUsers);
                        JSONObject areaLineDataObject = jsonObject1.getJSONObject("AreaLineData");

                        JSONArray sizeJsonArrayDays = areaLineDataObject.getJSONArray("Days");
                        areaLineDataDays = new ArrayList<>();
                        for (int indexJ = 0; indexJ < sizeJsonArrayDays.length(); indexJ++) {

                            areaLineDataDays.add(sizeJsonArrayDays.getString(indexJ));

                        }
                        JSONArray sizeJsonArray = areaLineDataObject.getJSONArray("Contacts");
                        areaLineDatasContacts = new ArrayList<>();
                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                            areaLineDatasContacts.add(sizeJsonArray.getString(indexJ));

                        }
                        JSONObject simpleBarDataObject = jsonObject1.getJSONObject("SimpleBarData");
                        JSONArray simpleBarDataJsonArray = simpleBarDataObject.getJSONArray("ScoreValue");
                        simpleBarDatas = new ArrayList<>();
                        for (int indexJ = 0; indexJ < simpleBarDataJsonArray.length(); indexJ++) {

                            simpleBarDatas.add(simpleBarDataJsonArray.getString(indexJ));

                        }

                        JSONObject simplePieDataObject = jsonObject1.getJSONObject("SimplePieData");
                        JSONArray sourceTitleJsonArray = simplePieDataObject.getJSONArray("SourceTitle");
                        sourceTitleDatas = new ArrayList<>();
                        for (int indexJ = 0; indexJ < sourceTitleJsonArray.length(); indexJ++) {

                            sourceTitleDatas.add(sourceTitleJsonArray.getString(indexJ));

                        }
                        JSONArray SourceValueJsonArray = simplePieDataObject.getJSONArray("SourceValue");
                        SourceValueDatas = new ArrayList<>();
                        for (int indexJ = 0; indexJ < SourceValueJsonArray.length(); indexJ++) {

                            SourceValueDatas.add(SourceValueJsonArray.getString(indexJ));

                        }
                        mCubicValueLineChart.setVisibility(View.VISIBLE);
                        ValueLineSeries series = new ValueLineSeries();
                        series.setColor(0xFF56B7F1);
                        series.addPoint(new ValueLinePoint(areaLineDataDays.get(0), Float.valueOf(areaLineDatasContacts.get(0))));
                        series.addPoint(new ValueLinePoint(areaLineDataDays.get(1), Float.valueOf(areaLineDatasContacts.get(1))));
                        series.addPoint(new ValueLinePoint(areaLineDataDays.get(2), Float.valueOf(areaLineDatasContacts.get(2))));
                        series.addPoint(new ValueLinePoint(areaLineDataDays.get(3), Float.valueOf(areaLineDatasContacts.get(3))));
                        series.addPoint(new ValueLinePoint(areaLineDataDays.get(4), Float.valueOf(areaLineDatasContacts.get(4))));
                        series.addPoint(new ValueLinePoint(areaLineDataDays.get(5), Float.valueOf(areaLineDatasContacts.get(5))));
                        series.addPoint(new ValueLinePoint(areaLineDataDays.get(6), Float.valueOf(areaLineDatasContacts.get(6))));
                        mCubicValueLineChart.addSeries(series);
                        mCubicValueLineChart.startAnimation();


//                        StackedBarModel s1 = new StackedBarModel("10%");

//                        s1.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(0)), 0xFF63CBB0));

//                        StackedBarModel s2 = new StackedBarModel("20%");
//                        s2.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(1)), 0xFF63CBB0));


//                        StackedBarModel s3 = new StackedBarModel("30%");
//
//                        s3.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(2)), 0xFF63CBB0));
//
//
//                        StackedBarModel s4 = new StackedBarModel("40%");
//                        s4.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(3)), 0xFF63CBB0));
//
//                        StackedBarModel s5 = new StackedBarModel("50%");
//                        s5.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(4)), 0xFF63CBB0));
//
//                        StackedBarModel s6 = new StackedBarModel("60%");
//                        s6.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(5)), 0xFF63CBB0));
//
//                        StackedBarModel s7 = new StackedBarModel("70%");
//                        s7.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(6)), 0xFF63CBB0));
//
//                        StackedBarModel s8= new StackedBarModel("80%");
//                        s8.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(7)), 0xFF63CBB0));
//
//                        StackedBarModel s9= new StackedBarModel("90%");
//                        s9.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(8)), 0xFF63CBB0));
//
//
//                        StackedBarModel s10= new StackedBarModel("100%");
//                        s10.addBar(new BarModel(Float.valueOf(simpleBarDatas.get(9)), 0xFF63CBB0));
//
//
//                        mStackedBarChart.addBar(s1);
//                        mStackedBarChart.addBar(s2);
//                        mStackedBarChart.addBar(s3);
//                        mStackedBarChart.addBar(s4);
//                        mStackedBarChart.addBar(s5);
//                        mStackedBarChart.addBar(s6);
//                        mStackedBarChart.addBar(s7);
//                        mStackedBarChart.addBar(s8);
//                        mStackedBarChart.addBar(s9);
//                        mStackedBarChart.addBar(s10);
//
//                        mStackedBarChart.startAnimation();
//

//
                        mBarChart.addBar(new BarModel("10%", Float.valueOf(simpleBarDatas.get(0)), 0xFF343456));
                        mBarChart.addBar(new BarModel("20%", Float.valueOf(simpleBarDatas.get(1)), 0xFF123456));
                        mBarChart.addBar(new BarModel("30%", Float.valueOf(simpleBarDatas.get(2)), 0xFF343456));
                        mBarChart.addBar(new BarModel("40%", Float.valueOf(simpleBarDatas.get(3)), 0xFF563456));
                        mBarChart.addBar(new BarModel("50%", Float.valueOf(simpleBarDatas.get(4)), 0xFF873F56));
                        mBarChart.addBar(new BarModel("60%", Float.valueOf(simpleBarDatas.get(5)), 0xFF56B7F1));
                        mBarChart.addBar(new BarModel("70%", Float.valueOf(simpleBarDatas.get(6)), 0xFF343456));
                        mBarChart.addBar(new BarModel("80%", Float.valueOf(simpleBarDatas.get(7)), 0xFF343456));
                        mBarChart.addBar(new BarModel("90%", Float.valueOf(simpleBarDatas.get(8)), 0xFF343456));
                        mBarChart.addBar(new BarModel("100%", Float.valueOf(simpleBarDatas.get(9)), 0xFF343456));
                        mBarChart.startAnimation();

//                        List<ChartData> value=new ArrayList<>();
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(0)), "10%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(1)), "20%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(2)), "30%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(3)), "40%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(4)), "50%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(5)), "60%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(6)), "70%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(7)), "80%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(8)), "90%"));
//                        value.add(new ChartData( Float.valueOf(simpleBarDatas.get(9)), "100%"));
//
//                        barChart.setData(value);

                        if (!sourceTitleDatas.isEmpty())
                            for (int i = 0; i < sourceTitleDatas.size(); i++) {
                                mPieChart.addPieSlice(new PieModel(sourceTitleDatas.get(i), Float.valueOf(SourceValueDatas.get(i)), Color.parseColor(colorCodes[i])));
                                mPieChart.setLegendTextSize(20);
                                mPieChart.setLegendColor(getResources().getColor(R.color.black));
                                mPieChart.startAnimation();
                                onSourceAdd(sourceTitleDatas.get(i), SourceValueDatas.get(i), Color.parseColor(colorCodes[i]));
                            }


                        total_contact_funnel.setText(TotalContacts + " Contacts");
                        total_contact_pipeline.setText(TotalContacts + " Contacts");
//                        else {
//                            msg = jsonObject.getString("Message");
//
//                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
//                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}