package com.bnhabitat.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Financial_Bank_viewFragment extends Fragment implements TabChangeListener {
    TextView demand_price_txt, price_txt, bank_loan, amount, tenure, loan_outstanding, account_no;
    View view;
    ArrayList<InventoryModel> property_data;
    public Financial_Bank_viewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_financial__bank_view, container, false);

        try {
            Bundle bundle = this.getArguments();
            if (bundle != null)
                property_data = (ArrayList<InventoryModel>) bundle.getSerializable("property_data");



        }catch (Exception e){
            e.printStackTrace();
        }
        onIntailizeView(view);
        return view;
    }
    public void onIntailizeView(View view){
        demand_price_txt=(TextView)view.findViewById(R.id.demand_price_txt);
        price_txt=(TextView)view.findViewById(R.id.price_txt);
        bank_loan=(TextView)view.findViewById(R.id.bank_loan);
        amount=(TextView)view.findViewById(R.id.amount);
        tenure=(TextView)view.findViewById(R.id.tenure);
        loan_outstanding=(TextView)view.findViewById(R.id.loan_outstanding);
        account_no=(TextView)view.findViewById(R.id.account_no);
        try {
            demand_price_txt.setText(Utils.getEmptyValue("" + property_data.get(0).getPropertyFinancialses().get(0).getDemandPrice()));
            price_txt.setText(Utils.getEmptyValue("" + property_data.get(0).getPropertyFinancialses().get(0).getPrice()));
            tenure.setText(Utils.getEmptyValue(property_data.get(0).getPropertyLoanses().get(0).getLoanTenure()));
            amount.setText(Utils.getEmptyValue(property_data.get(0).getPropertyLoanses().get(0).getLoanAmount()));
            loan_outstanding.setText(Utils.getEmptyValue(property_data.get(0).getPropertyLoanses().get(0).getTotalOutstandingAfterLastInstallment()));
        }catch (Exception e){

        }

        }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
