package com.bnhabitat.ui.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.ui.activities.ProjectDetailActivity;
import com.bnhabitat.ui.adapters.FaqAdapter;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FinanceFragment extends Fragment implements TabChangeListener {

    private EditText rent_sqft;
    private EditText ror_txt;
    private EditText price_per_sq;
    ImageView info_icon,share_icon,notification_icon,user_icon;
    ProjectsDetailModel projectsDetailModel;
    int position;
    private ImageView calculated;
    TextView assuredcal_text,assuredlayout,faqtext_lay;
    LinearLayout assured_returnLayout,faqLayout;
    FaqAdapter faqAdapter;
    LinearLayoutManager faqlinearLayoutManager;
    TextView no_faq;
    RecyclerView faq_list;
    private ArrayList<ProjectsDetailModel.FAQs> faQses = new ArrayList<>();
    TextView symbol,symbol1;
    ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList=new ArrayList<>();


    private ArrayList<ProjectsDetailModel.ProjectLenders> projectLendersesModel = new ArrayList<>();
    ArrayList<ProjectsModel> projectListingModelArrayList = new ArrayList<>();
    public FinanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_finance, container, false);
        rent_sqft = (EditText) view.findViewById(R.id.rent_sqft);
        price_per_sq = (EditText) view.findViewById(R.id.price_per_sq);
        ror_txt = (EditText) view.findViewById(R.id.ror_txt);
        symbol = (TextView)view. findViewById(R.id.symbol);
        symbol1 = (TextView)view. findViewById(R.id.symbol1);
        faqtext_lay = (TextView)view. findViewById(R.id.faqtext_lay);
        assuredlayout = (TextView)view. findViewById(R.id.assuredlayout);
        assuredcal_text = (TextView) view.findViewById(R.id.assuredcal_text);
        final Bundle bundle = this.getArguments();
        if (bundle != null)
            projectsDetailModel = (ProjectsDetailModel) bundle.getSerializable("projectModel");
        position = bundle.getInt("position");
        Log.e("position", String.valueOf(position));
        projectListingModelArrayList = (ArrayList<ProjectsModel>) bundle.getSerializable("notification_array");

        calculated = (ImageView) view.findViewById(R.id.calculated);
        info_icon=(ImageView)view.findViewById(R.id.info_icon);
        assured_returnLayout=(LinearLayout) view.findViewById(R.id.assured_returnLayout);
        faqLayout=(LinearLayout) view.findViewById(R.id.faqLayout);
        share_icon=(ImageView)view.findViewById(R.id.share_icon);
        notification_icon=(ImageView)view.findViewById(R.id.notification_icon);
        user_icon=(ImageView)view.findViewById(R.id.user_icon);
        faq_list = (RecyclerView) view.findViewById(R.id.faq_list);
        no_faq = (TextView) view.findViewById(R.id.no_faq);
        faqlinearLayoutManager = new LinearLayoutManager(getActivity());
        faQses = projectsDetailModel.getFaqs();
        faqAdapter = new FaqAdapter(getActivity(), faQses);

        faq_list.setLayoutManager(faqlinearLayoutManager);
        faq_list.setAdapter(faqAdapter);

        symbol.setText(Utils.getSymbol(getActivity()));
        symbol1.setText(Utils.getSymbol(getActivity()));
        assuredlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assured_returnLayout.setVisibility(View.VISIBLE);
                faqLayout.setVisibility(View.GONE);
                faqtext_lay.setBackgroundColor(getResources().getColor(R.color.light_blue));
                assuredlayout.setBackgroundColor(getResources().getColor(R.color.blue));
                if(projectsDetailModel.getTypeofProject()==32){

                    assured_returnLayout.setVisibility(View.VISIBLE);
                    assuredcal_text.setVisibility(View.GONE);
                }
                else{
                    assured_returnLayout.setVisibility(View.GONE);
                    assuredcal_text.setVisibility(View.VISIBLE);
                }
                no_faq.setVisibility(View.GONE);
            }
        });
        faqtext_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (faQses.isEmpty()) {
                    faq_list.setVisibility(View.GONE);
                    faqLayout.setVisibility(View.GONE);
                    no_faq.setVisibility(View.VISIBLE);

                }else{
                    faqLayout.setVisibility(View.VISIBLE);
                }
                assuredcal_text.setVisibility(View.GONE);
                assured_returnLayout.setVisibility(View.GONE);

                assuredlayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                faqtext_lay.setBackgroundColor(getResources().getColor(R.color.blue));
            }
        });


        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                projectRelationshipManagersArrayList=projectsDetailModel.getProjectRelationshipManagers();
                Log.e("project_array", String.valueOf(projectRelationshipManagersArrayList.size()));
                if(!projectRelationshipManagersArrayList.isEmpty()){
                    CustomDialogFragment fragment = CustomDialogFragment.newInstance();
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("Arraylist", projectRelationshipManagersArrayList);
                    fragment.setArguments(bundle);

                    fragment.show(((ProjectDetailActivity)getActivity()).getSupportFragmentManager(), "dialog_fragment");
                }
                else{
                    Toast.makeText(getActivity(), "No RM associated with the project.", Toast.LENGTH_SHORT).show();
                }           }
        });

        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationDialogProjectDetail notificationDialogFragment = NotificationDialogProjectDetail.newInstance();

                Bundle bundle = new Bundle();
//                bundle.putSerializable("Arraylist", projectListingModelArrayList);
//                bundle.putSerializable("propertySizes", propertySizes);
//                bundle.putSerializable("towers", projectsDetailModel.getTowers());
                bundle.putSerializable("projectModel", projectsDetailModel);
//                bundle.putSerializable("status", "1");
//                bundle.putInt("Position", position);
                notificationDialogFragment.setArguments(bundle);

                notificationDialogFragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "notification_dialog_fragment");

            }
        });
        share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, projectsDetailModel.getTitle());

                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://bnhabitat.com/?"+"p="+projectsDetailModel.getId());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });

        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(projectsDetailModel.getCurrentOffers().equalsIgnoreCase("null")||projectsDetailModel.getCurrentOffers().equalsIgnoreCase("")||projectsDetailModel.getCurrentOffers()==null){
                    Toast.makeText(getActivity(), "No offers yet", Toast.LENGTH_SHORT).show();
                }
                else{
                    new AlertDialog.Builder(getActivity())
                            .setMessage(projectsDetailModel.getCurrentOffers())
                            .setPositiveButton("NOTED", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();
                }

            }
        });

        calculated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rent_sqft.getText().toString().equals("") && ror_txt.getText().toString().equals("") && price_per_sq.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Please fill atleast two fields", Toast.LENGTH_SHORT).show();
                } else {

                    if (rent_sqft.getText().toString().length() > 0 && ror_txt.getText().toString().length() > 0) {
                        double PricePerSQft = (Double.parseDouble(rent_sqft.getText().toString()) * 12 * 100) / Double.parseDouble(ror_txt.getText().toString());
                        price_per_sq.setText(Utils.getSymbol(getActivity())+"" + PricePerSQft);
                    } else if (rent_sqft.getText().toString().length() > 0 && price_per_sq.getText().toString().length() > 0) {
                        double ror = (Double.parseDouble(rent_sqft.getText().toString()) * 12 * 100) / Double.parseDouble(price_per_sq.getText().toString());
                        ror_txt.setText("" + ror);
                    } else if (price_per_sq.getText().toString().length() > 0 && ror_txt.getText().toString().length() > 0) {
                        double rent = ((Double.parseDouble(price_per_sq.getText().toString()) * Double.parseDouble(ror_txt.getText().toString())) / (12 * 100));
                       Log.e("rent","rent"+rent);

                        rent_sqft.setText(Utils.getSymbol(getActivity())+"" + rent);
                    } else {
                        Toast.makeText(getActivity(), "Please fill atleast two fields", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        return view;
    }


    @Override
    public void onChangeTab(int position) {


    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
