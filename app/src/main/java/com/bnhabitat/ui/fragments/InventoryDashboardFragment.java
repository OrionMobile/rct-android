package com.bnhabitat.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.activities.SelectionPropertyTypeActivity;

/**
 *
 */
public class InventoryDashboardFragment extends Fragment {
    LinearLayout search_property_lay,sell_property_lay,rent_in_out_lay,pg_lay,submit_requirement_lay,fav_lay;
    View view;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    SendMessage SM;
    public InventoryDashboardFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_inventory_dashboard, container, false);
        onIntialize(view);

        search_property_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SM.sendData(1);
                ForyouFragment foryouFragment = new ForyouFragment();



                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_inventory_container, foryouFragment).commit();

            }
        });
        sell_property_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(),SelectionPropertyTypeActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();

        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }
  public void onIntialize(View view){
      search_property_lay=(LinearLayout)view.findViewById(R.id.search_property_lay);
      sell_property_lay=(LinearLayout)view.findViewById(R.id.sell_property_lay);
      rent_in_out_lay=(LinearLayout)view.findViewById(R.id.rent_in_out_lay);
      pg_lay=(LinearLayout)view.findViewById(R.id.pg_lay);
      submit_requirement_lay=(LinearLayout)view.findViewById(R.id.submit_requirement_lay);
      fav_lay=(LinearLayout)view.findViewById(R.id.fav_lay);

  }
    // TODO: Rename method, update argument and hook method into UI event







}
