package com.bnhabitat.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;

import de.greenrobot.event.EventBus;

/**
 * Created by Android on 5/23/2017.
 */

public class ChangeCurrencyDialogFragment extends DialogFragment {

    TextView inr,usa,can,uk,aus,uae;

    String currencyString;
    String currencyName;
    String currencyPrice;
    double price;

    public static ChangeCurrencyDialogFragment newInstance() {
        return new ChangeCurrencyDialogFragment();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.change_currency_dialog_fragment, container, false);
        inr=(TextView)v.findViewById(R.id.inr);
        usa=(TextView)v.findViewById(R.id.usa);
        can=(TextView)v.findViewById(R.id.can);
        uk=(TextView)v.findViewById(R.id.uk);
        aus=(TextView)v.findViewById(R.id.aus);
        uae=(TextView)v.findViewById(R.id.uae);
        currencyString = PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.CURRENCY_VALUES, "");

            inr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != currencyString && !currencyString.equalsIgnoreCase("")) {

                    String[] currencyArray = currencyString.replaceAll("\\{", "").replaceAll("\\}", "").split(",");


                    for (String currency : currencyArray) {

                        currencyName = currency.split(":")[0];
                        currencyPrice = currency.split(":")[1];
                        price = Double.parseDouble(currencyPrice);

                        if (currencyName.equalsIgnoreCase("INR")) {
                            PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DEFAULT_CURRENCY_NAME,currencyName);
                            PreferenceConnector.getInstance(getActivity()).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, Float.parseFloat(currencyPrice));
                        }
                    }

                }


                dialogDismiss();
            }
        });

        usa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (null != currencyString && !currencyString.equalsIgnoreCase("")) {

                    String[] currencyArray = currencyString.replaceAll("\\{", "").replaceAll("\\}", "").split(",");


                    for (String currency : currencyArray) {

                        currencyName = currency.split(":")[0];
                        currencyPrice = currency.split(":")[1];
                        price = Double.parseDouble(currencyPrice);

                        if (currencyName.equalsIgnoreCase("USD")) {
                            PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DEFAULT_CURRENCY_NAME,currencyName);
                            PreferenceConnector.getInstance(getActivity()).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, Float.parseFloat(currencyPrice));
                        }
                    }

                }

                dialogDismiss();
            }
        });
        can.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (null != currencyString && !currencyString.equalsIgnoreCase("")) {

                    String[] currencyArray = currencyString.replaceAll("\\{", "").replaceAll("\\}", "").split(",");


                    for (String currency : currencyArray) {

                        currencyName = currency.split(":")[0];
                        currencyPrice = currency.split(":")[1];
                        price = Double.parseDouble(currencyPrice);

                        if (currencyName.equalsIgnoreCase("CAD")) {
                            PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DEFAULT_CURRENCY_NAME,currencyName);
                            PreferenceConnector.getInstance(getActivity()).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, Float.parseFloat(currencyPrice));
                        }
                    }

                }

                dialogDismiss();
            }
        });
        uk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (null != currencyString && !currencyString.equalsIgnoreCase("")) {

                    String[] currencyArray = currencyString.replaceAll("\\{", "").replaceAll("\\}", "").split(",");


                    for (String currency : currencyArray) {

                        currencyName = currency.split(":")[0];
                        currencyPrice = currency.split(":")[1];
                        price = Double.parseDouble(currencyPrice);
                        if (currencyName.equalsIgnoreCase("GBP")) {
                            PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DEFAULT_CURRENCY_NAME,currencyName);
                            PreferenceConnector.getInstance(getActivity()).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, Float.parseFloat(currencyPrice));
                        }
                    }

                }



                dialogDismiss();
            }
        });
        aus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (null != currencyString && !currencyString.equalsIgnoreCase("")) {

                    String[] currencyArray = currencyString.replaceAll("\\{", "").replaceAll("\\}", "").split(",");


                    for (String currency : currencyArray) {

                        currencyName = currency.split(":")[0];
                        currencyPrice = currency.split(":")[1];
                        price = Double.parseDouble(currencyPrice);

                        if (currencyName.equalsIgnoreCase("AUD")) {
                            PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DEFAULT_CURRENCY_NAME,currencyName);
                            PreferenceConnector.getInstance(getActivity()).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, Float.parseFloat(currencyPrice));
                        }
                    }

                }

                dialogDismiss();
            }
        });
        uae.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (null != currencyString && !currencyString.equalsIgnoreCase("")) {

                    String[] currencyArray = currencyString.replaceAll("\\{", "").replaceAll("\\}", "").split(",");


                    for (String currency : currencyArray) {

                        currencyName = currency.split(":")[0];
                        currencyPrice = currency.split(":")[1];
                        price = Double.parseDouble(currencyPrice);

                        if (currencyName.equalsIgnoreCase("EUR")) {
                            PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DEFAULT_CURRENCY_NAME,currencyName);
                            PreferenceConnector.getInstance(getActivity()).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, Float.parseFloat(currencyPrice));
                        }

                    }

                }
                dialogDismiss();

            }
        });
        return v;
    }

    public void dialogDismiss(){

        EventBus.getDefault().post("setAdapter");

        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog_fragment");
        if (prev != null) {
            ChangeCurrencyDialogFragment df = (ChangeCurrencyDialogFragment) prev;
            df.dismiss();
        }
    }
}
