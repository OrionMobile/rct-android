package com.bnhabitat.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.AllGroupsModel;
import com.bnhabitat.models.ContactSocialDetails;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.ui.activities.MainDashboardActivity;
import com.bnhabitat.ui.activities.ProfileActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bnhabitat.ui.activities.CompleteContactInfoActivity.viewPager;


public class ProfessionalContactInfo extends Fragment implements CommonAsync.OnAsyncResultListener {
    Bitmap bitmap;
    AutoCompleteTextView profession, designation,company_name;
    Spinner annual_spinner, industry_spinner;
    Button save_and_continue;
    private ArrayList<AllGroupsModel> allGroupsModels;
    ArrayList<String> professionNames = new ArrayList<>();
    ArrayList<String> desginationNames = new ArrayList<>();
    ArrayList<String> annualIncomeNames = new ArrayList<>();
    ArrayList<String> industryNames = new ArrayList<>();
    ArrayList<ContactsModel> contactsModels = new ArrayList<>();

    SendMessage SM;
    String edit_data;

    public ProfessionalContactInfo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_professional_contact_info, container, false);
        profession = (AutoCompleteTextView) rootView.findViewById(R.id.profession);
        designation = (AutoCompleteTextView) rootView.findViewById(R.id.designation);
        save_and_continue = (Button) rootView.findViewById(R.id.save_and_continue);
        annual_spinner = (Spinner) rootView.findViewById(R.id.annual_spinner);
        industry_spinner = (Spinner) rootView.findViewById(R.id.industry_spinner);
        company_name= (AutoCompleteTextView) rootView.findViewById(R.id.company_name);
        onGetRequest("3");
        onGetRequest("6");
        onGetRequest("7");
        onGetRequest("5");


        Bundle bundle = this.getArguments();
        if (bundle != null) {

            try {

                contactsModels = (ArrayList<ContactsModel>) bundle.getSerializable("ModelName");
                edit_data = bundle.getString("edit") == null ? "" : bundle.getString("edit");

                if (!contactsModels.isEmpty()) {
                    int posIndustry = 0;
                    int posAnnualIncome = 0;
                    profession.setText(contactsModels.get(0).getProfession());
                    designation.setText(contactsModels.get(0).getDesignation());

//                for (int i = 0; i <= industryNames.size(); i++) {
//                    if (industryNames.get(i).equalsIgnoreCase(contactsModels.get(0).getIndustryName())) {
//                        posIndustry = i;
//                    }
//                }
//                industry_spinner.setSelection(posIndustry);
//
//
//                for (int i = 0; i <= industryNames.size(); i++) {
//                    if (annualIncomeNames.get(i).equalsIgnoreCase(contactsModels.get(0).getAnnualIncome())) {
//                        posAnnualIncome = i;
//                    }
//
//                industry_spinner.setSelection(posAnnualIncome);
//
//            }
                }
            } catch (Exception e) {

            }
        }


        save_and_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onPostContacts();

            }
        });
        return rootView;


    }

    private void onGetRequest(String taxonomyId) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_TERMS_DATA + taxonomyId + "/1", "", "Loading...",
                this,
                Urls.URL_GET_TERMS_DATA, Constants.GET).execute();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();


        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    private void onPostContacts() {


        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
                getInputJson(),
                "Loading...",
                this,
                Urls.URL_CREATE_CONTACTS,
                Constants.POST).execute();


    }

//    private String getLoginInputJson() {
//
//        String inputJson = "";
////
//
//
////
//        try {
//
//
//            JSONObject jsonObject = new JSONObject();
//
//
//            jsonObject.put("Prefix", contactArraylist.get(0).getPrefix());
//            jsonObject.put("FirstName", contactArraylist.get(0).getFirstName());
//            jsonObject.put("LastName", contactArraylist.get(0).getLastName());
//            jsonObject.put("MiddleName", contactArraylist.get(0).getMiddleName());
////            jsonObject.put("Id", contactArraylist.get(0).getId());
//
//
//
//            jsonObject.put("DateOfBirth", contactArraylist.get(0).getDateOfBirthStr());
//            jsonObject.put("Gender", contactArraylist.get(0).getGender());
//            jsonObject.put("CompanyId", "1");
//            if(contactArraylist.get(0).getImageUrl()==null) {
//                jsonObject.put("ImageUrl", "");
//            }else {
//                jsonObject.put("ImageUrl", contactArraylist.get(0).getImageUrl());
//            }
//
//
//            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
//            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
//
//
//            jsonObject.put("Profession", profession.getText().toString());
//            jsonObject.put("Designation", designation.getText().toString());
//            jsonObject.put("AnnualIncome", annual_spinner.getSelectedItem());
//            jsonObject.put("IndustryName", industry_spinner.getSelectedItem());
//            jsonObject.put("CompanyId", "1");
//            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
//            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
//
//            JSONArray jsonArray = new JSONArray();
//            for (int i = 0; i < contactArraylist.get(0).getPhones().size(); i++) {
//
//                JSONObject jsonObject2 = new JSONObject();
//
//                jsonObject2.put("PhoneNumber", contactArraylist.get(0).getPhones().get(i).getPhoneNumber());
//                jsonObject2.put("PhoneCode", contactArraylist.get(0).getPhones().get(i).getPhoneCode());
////                    jsonObject1.put("PhoneType", 1);
//                jsonObject2.put("Mode", "");
//
//                jsonArray.put(jsonObject2);
//
//            }
//
//            JSONArray socialjsonArray = new JSONArray();
//
//            for (int i = 0; i < contactArraylist.get(0).getContactAttributes().size(); i++) {
//
//
//
//
//                JSONObject Twitter = new JSONObject();
//
//                Twitter.put("Key", "IsSecure");
//
////
//                Twitter.put("Value", contactArraylist.get(0).getContactAttributes().get(i).getValue());
//
//                socialjsonArray.put(Twitter);
//
//
//
//
//            }
//
//
//            JSONArray emailArray = new JSONArray();
//            for (int i = 0; i < contactArraylist.get(0).getEmails().size(); i++) {
//
//                JSONObject jsonObject3 = new JSONObject();
//
//                jsonObject3.put("Address", contactArraylist.get(0).getEmails().get(i).getAddress());
//
////                    jsonObject1.put("PhoneType", 1);
//                jsonObject3.put("Label", "Email Label");
//
//                emailArray.put(jsonObject3);
//
//            }
//
//
//            JSONArray grouparray = new JSONArray();
//            JSONObject jsonObject4 = new JSONObject();
//            jsonObject4.put("Id", "");
//            grouparray.put(jsonObject4);
//            jsonObject.put("Phones", jsonArray);
//            jsonObject.put("EmailAddresses", emailArray);
//            jsonObject.put("ContactAttributes", socialjsonArray);
////            jsonObject.put("Groups", grouparray);
//
//            inputJson = jsonObject.toString();
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return inputJson;
//    }


    private String getInputJson() {

        String inputJson = "";
//


//
        try {


            JSONObject jsonObject = new JSONObject();


            jsonObject.put("Prefix", contactsModels.get(0).getPrefix());
            jsonObject.put("FirstName", contactsModels.get(0).getFirstName());
            jsonObject.put("LastName", contactsModels.get(0).getLastName());
            jsonObject.put("MiddleName", contactsModels.get(0).getMiddleName());
            jsonObject.put("Id", contactsModels.get(0).getId());

            jsonObject.put("CompanyId", Constants.COMPAN_ID);
            if (contactsModels.get(0).getImageUrl() == null) {
                jsonObject.put("ImageUrl", "");
            } else {
                jsonObject.put("ImageUrl", contactsModels.get(0).getImageUrl());
            }
            jsonObject.put("DateOfBirth", contactsModels.get(0).getDateOfBirthStr());
            jsonObject.put("Gender", contactsModels.get(0).getGender());

            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));


            jsonObject.put("Profession", profession.getText().toString());
            jsonObject.put("Designation", designation.getText().toString());
            jsonObject.put("Company", company_name.getText().toString());

            jsonObject.put("AnnualIncome", annual_spinner.getSelectedItem());
            jsonObject.put("IndustryName", industry_spinner.getSelectedItem());
            jsonObject.put("CompanyId", "1");
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getPhones().size(); i++) {

                JSONObject jsonObject2 = new JSONObject();

                jsonObject2.put("Id", contactsModels.get(0).getPhones().get(i).getId());
                jsonObject2.put("PhoneNumber", contactsModels.get(0).getPhones().get(i).getPhoneNumber());
                jsonObject2.put("PhoneCode", contactsModels.get(0).getPhones().get(i).getPhoneCode());


//                    jsonObject1.put("PhoneType", 1);
                jsonObject2.put("Mode", "");

                jsonArray.put(jsonObject2);

            }

          /*  JSONArray socialjsonArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getContactAttributes().size(); i++) {


                JSONObject Twitter = new JSONObject();

                Twitter.put("Key", "IsSecure");
                Twitter.put("Id", contactsModels.get(0).getContactAttributes().get(i).getId());
                Twitter.put("Value", contactsModels.get(0).getContactAttributes().get(i).getValue());

                socialjsonArray.put(Twitter);


            }*/
            JSONArray emailArray = new JSONArray();
            if (!contactsModels.get(0).getEmails().isEmpty())
                for (int i = 0; i < contactsModels.get(0).getEmails().size(); i++) {

                    JSONObject jsonObject3 = new JSONObject();

                    jsonObject3.put("Id", contactsModels.get(0).getEmails().get(i).getId());
                    jsonObject3.put("Address", contactsModels.get(0).getEmails().get(i).getAddress());

//                    jsonObject1.put("PhoneType", 1);
                    jsonObject3.put("Label", "Email Label");

                    emailArray.put(jsonObject3);

                }

            JSONArray addressArray = new JSONArray();
//                 getPhones=contactsFirebaseModels.get(index).getPhones();
//
//            for (int i = 0; i < addresses.size(); i++) {

            JSONObject jsonObject1 = new JSONObject();
            if (!contactsModels.get(0).getAddresses().isEmpty()) {
                for (int i = 0; i < contactsModels.get(0).getAddresses().size(); i++) {
                    jsonObject1.put("StateName", contactsModels.get(0).getAddresses().get(i).getStateName());

//                    jsonObject1.put("PhoneType", 1);
                    jsonObject1.put("CountryId", contactsModels.get(0).getAddresses().get(i).getCountryId());
                    jsonObject1.put("CityName", contactsModels.get(0).getAddresses().get(i).getCity_name());
//                jsonObject1.put("Address1", addresses.get(i).getAddress1());
//                jsonObject1.put("Address2", addresses.get(i).getAddress2());
//                jsonObject1.put("Address3", addresses.get(i).getAddress3());

                    jsonObject1.put("DeveloperName", contactsModels.get(0).getAddresses().get(i).getDeveloper_name());
                    jsonObject1.put("Tower", contactsModels.get(0).getAddresses().get(i).getTower());
                    jsonObject1.put("Floor", contactsModels.get(0).getAddresses().get(i).getFloor());
                    jsonObject1.put("UnitName", contactsModels.get(0).getAddresses().get(i).getUnitName());
                    jsonObject1.put("ZipCode", contactsModels.get(0).getAddresses().get(i).getZipcode());
                    addressArray.put(jsonObject1);
                }
            }


            JSONArray grouparray = new JSONArray();
            JSONObject jsonObject4 = new JSONObject();
            jsonObject4.put("Id", "");
            grouparray.put(jsonObject4);
            jsonObject.put("Phones", jsonArray);
            jsonObject.put("Addresses", addressArray);
            jsonObject.put("EmailAddresses", emailArray);
         //   jsonObject.put("ContactAttributes", socialjsonArray);
//            jsonObject.put("Groups", grouparray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_CREATE_CONTACTS)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {

                        ContactsModel contactModel = new ContactsModel();
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String msg = "";
                        if (jsonObject1.getBoolean("Success")) {

                            msg = jsonObject1.getString("Message");

//                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String id = jsonObject2.getString("Id");
                            String FirstName = jsonObject2.getString("FirstName");
                            String LastName = jsonObject2.getString("LastName");
                            String CompanyId = jsonObject2.getString("CompanyId");
                            String CompanyName = jsonObject2.getString("CompanyName");
                            String DateOfBirthStr = jsonObject2.getString("DateOfBirthStr");
                            String Prefix = jsonObject2.getString("Prefix");
                            String MiddleName = jsonObject2.getString("MiddleName");

                            String Sufix = jsonObject2.getString("Sufix");
                            String MaritalStatus = jsonObject2.getString("MaritalStatus");
                            String ExistingImageUrl = jsonObject2.getString("ExistingImageUrl");
                            String Religion = jsonObject2.getString("Religion");
                            String Gender = jsonObject2.getString("Gender");
                            String Designation = jsonObject2.getString("Designation");
                            String Profession = jsonObject2.getString("Profession");
                            contactModel.setId(id);
                            contactModel.setFirstName(FirstName);
                            contactModel.setLastName(LastName);
                            contactModel.setCompanyName(CompanyName);
                            contactModel.setDateOfBirthStr(DateOfBirthStr);
                            contactModel.setPrefix(Prefix);
                            contactModel.setMiddleName(MiddleName);
                            contactModel.setSufix(Sufix);
                            contactModel.setCompanyId(CompanyId);
                            contactModel.setMaritalStatus(MaritalStatus);
                            contactModel.setExistingImageUrl(ExistingImageUrl);
                            contactModel.setReligion(Religion);
                            contactModel.setGender(Gender);
                            contactModel.setDesignation(Designation);
                            contactModel.setProfession(Profession);
                            JSONArray sizeJsonArray = jsonObject2.getJSONArray("Addresses");
                            ArrayList<AddressModel> addresses = new ArrayList();
                            ContactsModel contactsModel = new ContactsModel();
                            for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                                AddressModel addresses1 = new AddressModel();
                                JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                                addresses1.setId(dataObj.getString("Id"));

                                addresses1.setCountryId(dataObj.getString("CountryId"));
                                addresses1.setStateName(dataObj.getString("StateName"));
                                addresses1.setProjectName(dataObj.getString("ProjectName"));
//                                addresses1.setLastName(dataObj.getString("LastName"));
//                                addresses1.setEmail(dataObj.getString("Email"));
//                                addresses1.setCompany(dataObj.getString("Company"));
                                addresses1.setCity_name(dataObj.getString("CityName"));
                                addresses1.setAddress1(dataObj.getString("Address1"));
                                addresses1.setAddress2(dataObj.getString("Address2"));
//                                addresses1.setPhoneNumber(dataObj.getString("PhoneNumber"));
//                                addresses1.setFaxNumber(dataObj.getString("FaxNumber"));
                                addresses.add(addresses1);
                            }
                            JSONArray phonesJsonArray = jsonObject2.getJSONArray("Phones");
                            ArrayList<ContactsModel.Phones> phones = new ArrayList();
                            for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                                ContactsModel.Phones phones1 = contactsModel.new Phones();
                                JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                                phones1.setId(dataObj.getString("Id"));
                                phones1.setContactId(dataObj.getString("ContactId"));
                                phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                phones1.setPhoneType(dataObj.getString("Mode"));
//
                                phones.add(phones1);

                            }
                            JSONArray emailJsonArray = jsonObject2.getJSONArray("EmailAddresses");
                            ArrayList<ContactsModel.Email> emails = new ArrayList();
                            for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                                ContactsModel.Email email = contactsModel.new Email();
                                JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                                email.setId(dataObj.getString("Id"));
                                email.setContactId(dataObj.getString("ContactId"));
                                email.setAddress(dataObj.getString("Address"));
                                email.setLabel(dataObj.getString("Label"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                emails.add(email);
                            }

                          /*  JSONArray socialJsonArray = jsonObject2.getJSONArray("ContactAttributes");
                            ArrayList<ContactsModel.ContactAttributes> socialArray = new ArrayList();
                            for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                                ContactsModel.ContactAttributes contactAttributes = contactsModel.new ContactAttributes();
                                JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                                contactAttributes.setKey(dataObj.getString("Key"));
                                contactAttributes.setValue(dataObj.getString("Value"));

//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                socialArray.add(contactAttributes);
                            }*/

                            JSONArray socialJsonArray = jsonObject2.getJSONArray("ContactSocialDetails");
                            ArrayList<ContactSocialDetails> socialArray = new ArrayList();
                            for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                                ContactSocialDetails contactAttributes = new ContactSocialDetails();
                                JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                                contactAttributes.setType(dataObj.getString("Type"));
                                contactAttributes.setDetail(dataObj.getString("Detail"));

                                socialArray.add(contactAttributes);
                            }
                            contactModel.setPhones(phones);
                            contactModel.setAddresses(addresses);
                            contactModel.setEmails(emails);
                            contactModel.setContactSocialDetails(socialArray);
                            contactsModels = new ArrayList<>();
                            contactsModels.add(contactModel);
                            Toast.makeText(getActivity(), "Professional contact information updated Successfully", Toast.LENGTH_SHORT).show();

                            if (edit_data.equalsIgnoreCase("")) {
                                SM.sendData(contactsModels, 2);
                            } else {
                                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                                intent.putExtra("contact_id", contactsModels.get(0).getId());
                                startActivity(intent);
                                getActivity().finish();

                            }

                        } else {
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_GET_TERMS_DATA)) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        allGroupsModels = new ArrayList<>();
                        JSONArray projectsArray = jsonObject.getJSONArray("Result");
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            AllGroupsModel allGroupsModel = new AllGroupsModel();
                            allGroupsModel.setId(jsonObject1.getString("Id"));
                            allGroupsModel.setName(jsonObject1.getString("name"));
                            allGroupsModel.setDescription(jsonObject1.getString("Description"));
                            allGroupsModels.add(allGroupsModel);
                            if (jsonObject1.getString("TaxonomyId").equalsIgnoreCase("3")) {
                                professionNames.add(jsonObject1.getString("name"));
                            } else if (jsonObject1.getString("TaxonomyId").equalsIgnoreCase("6")) {
                                desginationNames.add(jsonObject1.getString("name"));
                            } else if (jsonObject1.getString("TaxonomyId").equalsIgnoreCase("7")) {
                                annualIncomeNames.add(jsonObject1.getString("name"));
                            } else {
                                industryNames.add(jsonObject1.getString("name"));
                            }

                        }
                        if (!professionNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, professionNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            profession.setAdapter(aa);


                        }
                        if (!desginationNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, desginationNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            designation.setAdapter(aa);

                        }
                        if (!annualIncomeNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, annualIncomeNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            annual_spinner.setAdapter(aa);

                        }
                        if (!industryNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, industryNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            industry_spinner.setAdapter(aa);

                        }

                        int posIndustry = 0;

                        if (!contactsModels.isEmpty()){

                            for (int i = 0; i <= industryNames.size(); i++) {
                                if (industryNames.get(i).equalsIgnoreCase(contactsModels.get(0).getIndustryName())) {
                                    posIndustry = i;
                                }
                            }
                            industry_spinner.setSelection(posIndustry);


                        }

/*
                        for (int i = 0; i <= industryNames.size(); i++) {
                            if (annualIncomeNames.get(i).equalsIgnoreCase(contactsModels.get(0).getAnnualIncome())) {
                                posAnnualIncome = i;
                            }

                            industry_spinner.setSelection(posAnnualIncome);



                            }
*/

                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }
    }
}