package com.bnhabitat.ui.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.ContactSocialDetails;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.RelationModel;
import com.bnhabitat.models.TagModel;
import com.bnhabitat.ui.activities.CompleteContactInfoActivity;
import com.bnhabitat.ui.activities.ProfileActivity;
import com.bnhabitat.ui.adapters.AddressAdapter;
import com.bnhabitat.ui.adapters.RelationAdapter;
import com.bnhabitat.ui.adapters.TagsAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.bnhabitat.ui.activities.ProfileActivity.contact_id;

/**
 * A simple {@link Fragment} subclass.
 */

public class LeadContactFragment extends Fragment  implements CommonAsync.OnAsyncResultListener{
    View view;
    ArrayList<ContactsModel> contactsModels = new ArrayList<>();
    ImageView edit_contact_source, edit_profession, edit_address, add_contact, back, photo_edit;
    CircleImageView profile_img;
    TextView name, age, domain, email, phn, verified, profession, industry, designation,
            annual_income, developer_name,
            contact_source, date, campaign, tag, owner, notes;
    //relation_name, relation;
    ImageLoader imageLoader;
    ArrayList<RelationModel> relationArrayList = new ArrayList<>();
    ArrayList<AddressModel> addressArrayList = new ArrayList<>();

    RelationAdapter relationAdapter;
    AddressAdapter addressAdapter;
    RecyclerView rv_relation,rv_address,rv_tag;
    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager linearLayoutManager1,linearLayoutManager2;

    DisplayImageOptions options;
    RelativeLayout rl_edit;
    String twitterLink= "" , fbLink = "" ;
    ImageView iv_fb, iv_twitter;
    TagsAdapter tagsAdapter;
    ArrayList<String> tagsList = new ArrayList<>();

    public LeadContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_lead_contact, container, false);

        init();

        onClicks();

        return view;

    }

    private void  init(){

        iv_twitter = (ImageView) view.findViewById(R.id.iv_twitter);
        iv_fb = (ImageView) view.findViewById(R.id.iv_fb);
        photo_edit = (ImageView) view.findViewById(R.id.photo_edit);
        domain = (TextView) view.findViewById(R.id.domain);
        relationAdapter = new RelationAdapter(getActivity(), relationArrayList);
        addressAdapter = new AddressAdapter(getActivity(), addressArrayList);
        tagsAdapter = new TagsAdapter(getActivity(), tagsList);

        rv_relation = (RecyclerView) view.findViewById(R.id.rv_relation);
        rv_address = (RecyclerView) view.findViewById(R.id.rv_address);
        rv_tag = (RecyclerView) view.findViewById(R.id.rv_tag);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        relationAdapter = new RelationAdapter(getActivity(), relationArrayList);
        rv_relation.setLayoutManager(linearLayoutManager);
        rv_relation.setAdapter(relationAdapter);

        linearLayoutManager1 = new LinearLayoutManager(getActivity());
        addressAdapter = new AddressAdapter(getActivity(), addressArrayList);
        rv_address.setLayoutManager(linearLayoutManager1);
        rv_address.setAdapter(addressAdapter);


        linearLayoutManager2 = new LinearLayoutManager(getActivity());
        tagsAdapter = new TagsAdapter(getActivity(), tagsList);
        rv_tag.setLayoutManager(linearLayoutManager2);
        rv_tag.setAdapter(addressAdapter);

        edit_contact_source = (ImageView) view.findViewById(R.id.edit_contact_source);
        edit_profession = (ImageView) view.findViewById(R.id.edit_profession);
        edit_address = (ImageView) view.findViewById(R.id.edit_address);
        name = (TextView) view.findViewById(R.id.name);
        age = (TextView) view.findViewById(R.id.age);
        domain = (TextView) view.findViewById(R.id.domain);
        email = (TextView) view.findViewById(R.id.email);
        phn = (TextView) view.findViewById(R.id.phn);
        rl_edit = (RelativeLayout) view.findViewById(R.id.rl_edit);
        verified = (TextView) view.findViewById(R.id.verified);
      /*  relation = (TextView) view.findViewById(R.id.relation);
        relation_name = (TextView) view.findViewById(R.id.relation_name);*/
        profession = (TextView) view.findViewById(R.id.profession);
        industry = (TextView) view.findViewById(R.id.industry);
        designation = (TextView) view.findViewById(R.id.designation);

        developer_name = (TextView) view.findViewById(R.id.developer_name);
        annual_income = (TextView) view.findViewById(R.id.annual_income);
        contact_source = (TextView) view.findViewById(R.id.contact_source);
        date = (TextView) view.findViewById(R.id.date);
        campaign = (TextView) view.findViewById(R.id.campaign);
        tag = (TextView) view.findViewById(R.id.tag);
        owner = (TextView) view.findViewById(R.id.owner);
        // group = (TextView) findViewById(R.id.group);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        notes = (TextView) view.findViewById(R.id.notes);


        rv_relation = (RecyclerView) view.findViewById(R.id.rv_relation);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        relationAdapter = new RelationAdapter(getActivity(), relationArrayList);
        rv_relation.setLayoutManager(linearLayoutManager);
        rv_relation.setAdapter(relationAdapter);
        edit_contact_source = (ImageView) view.findViewById(R.id.edit_contact_source);
        edit_profession = (ImageView) view.findViewById(R.id.edit_profession);
        photo_edit = (ImageView) view.findViewById(R.id.photo_edit);
        profile_img = (CircleImageView) view.findViewById(R.id.profile_img);
    }

    private void onClicks(){

        rl_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(getActivity(), CompleteContactInfoActivity.class);
                    intent.putExtra("position", "0");
                    intent.putExtra("ModelName", contactsModels);
                    intent.putExtra("edit", "edit_data");
                    intent.putExtra("contact_id", contact_id);
                    getActivity().startActivity(intent);
                  //  getActivity().finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        edit_contact_source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{

                    Intent intent = new Intent(getActivity(), CompleteContactInfoActivity.class);
                    intent.putExtra("position", "3");
                    intent.putExtra("ModelName", contactsModels);
                    intent.putExtra("contact_id", contact_id);
                    intent.putExtra("edit", "edit_data");
                    getActivity().startActivity(intent);
                    getActivity().finish();

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        edit_profession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{

                    Intent intent = new Intent(getActivity(), CompleteContactInfoActivity.class);
                    intent.putExtra("position", "1");
                    intent.putExtra("ModelName", contactsModels);
                    intent.putExtra("contact_id", contact_id);
                    intent.putExtra("edit", "edit_data");
                    getActivity().startActivity(intent);
                    getActivity().finish();
                }


                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        edit_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{

                    Intent intent = new Intent(getActivity(), CompleteContactInfoActivity.class);
                    intent.putExtra("position", "2");
                    intent.putExtra("ModelName", contactsModels);
                    intent.putExtra("contact_id", contact_id);
                    intent.putExtra("edit", "edit_data");
                    getActivity().startActivity(intent);
                    getActivity().finish();
                }
                catch (Exception e){

                    e.printStackTrace();
                }

            }
        });

        iv_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fbLink.equalsIgnoreCase("" ) && fbLink.equalsIgnoreCase("https://www.facebook.com/") ){

                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                }

                else {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+fbLink));
                    getActivity().startActivity(browserIntent);
                }
            }
        });
        iv_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (twitterLink.equalsIgnoreCase("") && twitterLink.equalsIgnoreCase("https://twitter.com/")){

                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                }

                else {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+twitterLink));
                    getActivity().startActivity(browserIntent);
                }

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
           onGetDetail();
    }

    private void onGetDetail() {

//      String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CONTACT_DETAIL + contact_id + "/" + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""), "", "Loading...", this, Urls.URL_CONTACT_DETAIL, Constants.GET).execute();


    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {

                    ContactsModel contactDetailModel = new ContactsModel();
                    if (which.equalsIgnoreCase(Urls.URL_CONTACT_DETAIL)) {

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                    contactDetailModel.setId(Utils.getFilteredValue(jsonObject1.getString("Id")));
                    contactDetailModel.setFirstName(Utils.getFilteredValue(jsonObject1.getString("FirstName")));
                    contactDetailModel.setLastName(Utils.getFilteredValue(jsonObject1.getString("LastName")));
                    contactDetailModel.setPrefix(Utils.getFilteredValue(jsonObject1.getString("Prefix")));
                    contactDetailModel.setMiddleName(Utils.getFilteredValue(jsonObject1.getString("MiddleName")));
                    contactDetailModel.setSufix(Utils.getFilteredValue(jsonObject1.getString("Sufix")));
                    contactDetailModel.setMaritalStatus(Utils.getFilteredValue(jsonObject1.getString("MaritalStatus")));
                    contactDetailModel.setMarriageAnniversaryDate(Utils.getFilteredValue(jsonObject1.getString("MarriageAnniversaryDate")));
                    contactDetailModel.setMarriageAnniversaryDateString(Utils.getFilteredValue(jsonObject1.getString("MarriageAnniversaryDateString")));
                    contactDetailModel.setSourceName(Utils.getFilteredValue(jsonObject1.getString("SourceName")));
                    contactDetailModel.setCampaign(Utils.getFilteredValue(jsonObject1.getString("Campaign")));
                    contactDetailModel.setSourceDate(Utils.getFilteredValue(jsonObject1.getString("SourceDateString")));
                    contactDetailModel.setOwnerShip(Utils.getFilteredValue(jsonObject1.getString("OwnerShip")));
                    contactDetailModel.setOwnerUserId(Utils.getFilteredValue(jsonObject1.getString("OwnerUserId")));
                    contactDetailModel.setIndustryName(Utils.getFilteredValue(jsonObject1.getString("IndustryName")));
                    contactDetailModel.setDesignation(Utils.getFilteredValue(jsonObject1.getString("Designation")));
                    contactDetailModel.setAnnualIncome(Utils.getFilteredValue(jsonObject1.getString("IndustryName")));
                    contactDetailModel.setAnnualIncome(Utils.getFilteredValue(jsonObject1.getString("AnnualIncome")));
                    contactDetailModel.setCompanyId(Utils.getFilteredValue(jsonObject1.getString("CompanyId")));
                    contactDetailModel.setCompanyName(Utils.getFilteredValue(jsonObject1.getString("CompanyName")));
                    contactDetailModel.setProfession(Utils.getFilteredValue(jsonObject1.getString("Profession")));
                    contactDetailModel.setReligion(Utils.getFilteredValue(jsonObject1.getString("Religion")));
                    contactDetailModel.setDateOfBirthStr(Utils.getFilteredValue(jsonObject1.getString("DateOfBirthStr")));
                    contactDetailModel.setGender(Utils.getFilteredValue(jsonObject1.getString("Gender")));
                    if ((jsonObject1.getString("ImageUrl").equalsIgnoreCase("None"))){

                        contactDetailModel.setImageUrl("");

                    }

                    else
                        contactDetailModel.setImageUrl(jsonObject1.getString("ImageUrl"));

                    if (!contactDetailModel.getLastName().equalsIgnoreCase("None"))
                        name.setText(contactDetailModel.getFirstName() + " " + contactDetailModel.getLastName());
                    else
                        name.setText(contactDetailModel.getFirstName());
                    if (!contactDetailModel.getImageUrl().equalsIgnoreCase("None"))

                        imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + contactDetailModel.getImageUrl(), profile_img, options);


                    else
                        profile_img.setImageResource(R.drawable.user_profile_img);
                    age.setText(contactDetailModel.getGender());
                    JSONArray sizeJsonArray = jsonObject1.getJSONArray("Addresses");
                  //  ArrayList<ContactsModel.Addresses> addresses = new ArrayList();
                //  ArrayList<AddressModel> addresses = new ArrayList();
                            addressArrayList.clear();
                      ContactsModel contactsModel = new ContactsModel();
                    if (sizeJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                         // ContactsModel.Addresses addresses1 = contactsModel.new Addresses();
                            AddressModel addresses1 = new AddressModel();

                            JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                            addresses1.setId(Utils.getFilteredValue(dataObj.getString("Id")));
                            addresses1.setAddressType(Utils.getFilteredValue(dataObj.getString("AddressType")));
                            addresses1.setFloor(Utils.getFilteredValue(dataObj.getString("Floor")));
                            addresses1.setUnitName(Utils.getFilteredValue(dataObj.getString("UnitName")));
                            addresses1.setZipcode(Utils.getFilteredValue(dataObj.getString("ZipCode")));
                            addresses1.setCountryId(Utils.getFilteredValue(dataObj.getString("CountryId")));
                            addresses1.setCountryName(Utils.getFilteredValue(dataObj.getString("CountryName")));
                            addresses1.setLocality(Utils.getFilteredValue(dataObj.getString("LocalityName")));

                            addresses1.setStateName(Utils.getFilteredValue(dataObj.getString("StateName")));
                            addresses1.setBuilding_name(Utils.getFilteredValue(dataObj.getString("ProjectName")));
                            addresses1.setLocality(Utils.getFilteredValue(dataObj.getString("LocalityName")));
                            addresses1.setTower(Utils.getFilteredValue(dataObj.getString("Tower")));
                            addresses1.setProjectName(Utils.getFilteredValue(dataObj.getString("ProjectName")));
                            addresses1.setTownName(Utils.getFilteredValue(dataObj.getString("TownName")));
                            addresses1.setDeveloper_name(Utils.getFilteredValue(dataObj.getString("DeveloperName")));
                            addressArrayList.add(addresses1);

                        }
                            addressAdapter = new AddressAdapter(getActivity(), addressArrayList);
                            rv_address.setLayoutManager(linearLayoutManager1);
                            rv_address.setAdapter(addressAdapter);

                    JSONArray phonesJsonArray = jsonObject1.getJSONArray("Phones");
                    ArrayList<ContactsModel.Phones> phones = new ArrayList();
                    if (phonesJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                            ContactsModel.Phones phones1 = contactsModel.new Phones();
                            JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                            phones1.setId(Utils.getFilteredValue(dataObj.getString("Id")));
                            phones1.setContactId(Utils.getFilteredValue(dataObj.getString("ContactId")));
                            phones1.setPhoneNumber(Utils.getFilteredValue(dataObj.getString("PhoneNumber")));
                            phones1.setPhoneCode(Utils.getFilteredValue(dataObj.getString("PhoneCode")));
                            phones1.setPhoneType(Utils.getFilteredValue(dataObj.getString("Mode")));

                            phones.add(phones1);

                        }
                    JSONArray contactRelationsArray = jsonObject1.getJSONArray("ContactRelations");
                    ArrayList<ContactsModel.ContactRelations> contactRelations = new ArrayList();
                    if (contactRelationsArray.length() != 0)
                        for (int indexJ = 0; indexJ < contactRelationsArray.length(); indexJ++) {

                            ContactsModel.ContactRelations contactRelations1 = contactsModel.new ContactRelations();
                            JSONObject dataObj = contactRelationsArray.getJSONObject(indexJ);

                            contactRelations1.setRelation(Utils.getFilteredValue(dataObj.getString("Relation")));
                            contactRelations1.setRelationFirstName(Utils.getFilteredValue(dataObj.getString("RelationFirstName")));
                            contactRelations1.setRelationLastName(Utils.getFilteredValue(dataObj.getString("RelationLastName")));
                            contactRelations1.setId(Utils.getFilteredValueInt(dataObj.getInt("ContactId")));
                            contactRelations.add(contactRelations1);

                        }
                    JSONArray emailJsonArray = jsonObject1.getJSONArray("EmailAddresses");
                    ArrayList<ContactsModel.Email> emails = new ArrayList();
                    if (emailJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                            ContactsModel.Email email = contactsModel.new Email();
                            JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                            email.setId(Utils.getFilteredValue(dataObj.getString("Id")));
                            email.setContactId(Utils.getFilteredValue(dataObj.getString("ContactId")));
                            email.setAddress(Utils.getFilteredValue(dataObj.getString("Address")));
                            email.setLabel(Utils.getFilteredValue(dataObj.getString("Label")));
                            emails.add(email);
                        }

                        // tagssssssss
                         JSONArray tag = jsonObject1.getJSONArray("Tags");

                        for(int t=0;t<tag.length();t++){


                            tagsList.add(tag.get(t).toString().replace("\"", ""));

                        }
                        tagsAdapter = new TagsAdapter(getActivity(), tagsList);
                        rv_tag.setAdapter(tagsAdapter);
                            contactDetailModel.setTags(tagsList);
                  /*  JSONArray socialJsonArray = jsonObject1.getJSONArray("ContactAttributes");
                    ArrayList<ContactsModel.ContactAttributes> socialArray = new ArrayList();
                    if (socialJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                            ContactsModel.ContactAttributes contactAttributes = contactsModel.new ContactAttributes();
                            JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);
                            contactAttributes.setKey(Utils.getFilteredValue(dataObj.getString("Key")));
                            contactAttributes.setValue(Utils.getFilteredValue(dataObj.getString("Value")));

//                          phones1.setIsPrimary(dataObj.getString("IsPrimary"));

                            socialArray.add(contactAttributes);
                        }*/

                    JSONArray socialJsonArray = jsonObject1.getJSONArray("ContactSocialDetails");
                    ArrayList<ContactSocialDetails> socialArray = new ArrayList();
                    for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                        ContactSocialDetails contactAttributes = new ContactSocialDetails();
                        JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                        contactAttributes.setType(dataObj.getString("Type"));
                        contactAttributes.setDetail(dataObj.getString("Detail"));

                        socialArray.add(contactAttributes);
                    }

                    contactDetailModel.setPhones(phones);

                    contactDetailModel.setAddresses(addressArrayList);
                    contactDetailModel.setEmails(emails);
                    contactDetailModel.setContactRelationses(contactRelations);
                    contactDetailModel.setContactSocialDetails(socialArray);
                    contactsModels.add(contactDetailModel);

                    JSONArray relationArray = jsonObject1.getJSONArray("ContactRelations");

                    relationArrayList.clear();

                    if (!(relationArray.length() == 0)) {

                        for (int i = 0; i < relationArray.length(); i++) {

                            JSONObject r1 = relationArray.getJSONObject(i);

                            RelationModel relationModel = new RelationModel();

                            relationModel.setRelationFirstName(r1.getString("RelationFirstName"));
                            relationModel.setRelationLastName(r1.getString("RelationLastName"));
                            relationModel.setRelation(r1.getString("Relation"));


                            relationArrayList.add(relationModel);


                        }


                    } else {
                      //  relation.setText("None");
                      //  relation_name.setText("None");
                    }
                    relationAdapter = new RelationAdapter(getActivity(), relationArrayList);
                    rv_relation.setLayoutManager(linearLayoutManager);
                    rv_relation.setAdapter(relationAdapter);
                    if (!contactsModels.get(0).getEmails().isEmpty()) {
                        email.setText(contactsModels.get(0).getEmails().get(0).getAddress());
                    } else {
                        email.setText("None");
                    }

                  /*  if (!contactsModels.get(0).getContactSocialDetails().isEmpty()) {

                        domain.setText(contactsModels.get(0).getContactSocialDetails().get(0).getDetail());
                    } else {
                        domain.setText("None");
                    }*/

                    if (!contactsModels.get(0).getContactSocialDetails().isEmpty()) {

                        for (int i=0;i<contactsModels.get(0).getContactSocialDetails().size();i++){

                            if (contactsModels.get(0).getContactSocialDetails().get(i).getType().equalsIgnoreCase("website")){

                                domain.setText(contactsModels.get(0).getContactSocialDetails().get(i).getDetail());

                            }

                            if (contactsModels.get(0).getContactSocialDetails().get(i).getType().equalsIgnoreCase("facebook")){

                                fbLink = contactsModels.get(0).getContactSocialDetails().get(i).getDetail();

                            }

                            if (contactsModels.get(0).getContactSocialDetails().get(i).getType().equalsIgnoreCase("twitter")){

                                twitterLink = contactsModels.get(0).getContactSocialDetails().get(i).getDetail();

                            }

                        }

                    }


                    if (!contactsModels.get(0).getPhones().isEmpty()) {
                        for (int i = 0; i < contactsModels.get(0).getPhones().size(); i++) {

                            phn.setText(contactDetailModel.getPhones().get(0).getPhoneNumber());
                        }
                    } else {
                        phn.setText("None");
                    }

                    if (!contactsModels.get(0).getEmails().isEmpty()) {
                        for (int i = 0; i < contactsModels.get(0).getEmails().size(); i++) {

                            email.setText(contactDetailModel.getEmails().get(0).getAddress());
                        }


                    } else {
                        email.setText("None");
                    }


                    profession.setText(contactDetailModel.getProfession());
                    designation.setText(contactDetailModel.getDesignation());
                    industry.setText(contactDetailModel.getIndustryName());
                    annual_income.setText(contactDetailModel.getAnnualIncome());
                    contact_source.setText(contactDetailModel.getSourceName());

                    date.setText(Utils.getMessageDate(contactDetailModel.getSourceName()));


                    campaign.setText(contactDetailModel.getCampaign());
                    if (!contactDetailModel.getContactNotes().isEmpty())
                        notes.setText(contactDetailModel.getContactNotes().get(0).getNote());

/*
                    if (!contactDetailModel.getTags().isEmpty()){

                        String alpha = contactDetailModel.getTags().get(0).toString();

                        //Remove whitespace and split by comma
                        List<String> result1 = Arrays.asList(alpha.split("\\s*,\\s*"));

                        System.out.println(result1);

                        for (int i=0; i<result1.size(); i++){

                            tagsList.add(result1.get(i));

                        }

                    }
*/

                          //  contactDetailModel.setTags(tagsList);

                            tagsAdapter.notifyDataSetChanged();



//                    /*
//                     *
//                     * remove null values
//                     *
//                     * */
//                    ArrayList<ContactsModel.ContactRelations> tempRelationsArray = contactDetailModel.getContactRelationses();
//
//                    for(ContactsModel.ContactRelations contactRelations1:tempRelationsArray){
//
//                        if(contactRelations1.getRelationFirstName().equalsIgnoreCase("")){
//
//                            contactDetailModel.getContactRelationses().remove(contactRelations1)  ;
//
//                        }
//                    }

/*                    if (!contactDetailModel.getContactRelationses().isEmpty()) {

                        if (!contactDetailModel.getContactRelationses().get(0).getRelationFirstName().equalsIgnoreCase("")) {
                            relation.setText(contactDetailModel.getContactRelationses().get(0).getRelation());
                            relation_name.setText(contactDetailModel.getContactRelationses().get(0).getRelationFirstName() + " "
                                    + contactDetailModel.getContactRelationses().get(0).getRelationLastName());

                        } else {

                            relation.setText(contactDetailModel.getContactRelationses().get(1).getRelation());
                            relation_name.setText(contactDetailModel.getContactRelationses().get(1).getRelationFirstName() + " "
                                    + contactDetailModel.getContactRelationses().get(1).getRelationLastName());
                        }

                    } else {
                        relation.setText("None");
                        relation_name.setText("None");
                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
