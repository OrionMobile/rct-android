package com.bnhabitat.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.data.sizeunit.UnitsModel;
import com.bnhabitat.data.sizeunit.UnitsTable;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;

import java.util.ArrayList;

/**
 * Created by Android on 5/22/2017.
 */

public class SetDefaultSizeDialogFragment extends DialogFragment {
    ArrayList<String> plot_dimesion_unit_names=new ArrayList<>();
    String plotSizeUnitId,plotSizeUnitName;
    Button done;
    ArrayList<UnitsModel> setDefaultSizes=new ArrayList<>();
    ArrayList<String> unit_names=new ArrayList<>();

    Spinner plot_dimensions,plot_Sizes,built_up_area;

    String sizeUnitId,sizeUnitName;
    String plot_dimensionsUnitId,plot_dimensionsUnitName;

    public static SetDefaultSizeDialogFragment newInstance() {
        return new SetDefaultSizeDialogFragment();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
       setDefaultSizes= UnitsTable.getInstance().getUnits();
        View v = inflater.inflate(R.layout.set_defualt_size_dialog, container, false);

        built_up_area=(Spinner)v.findViewById(R.id.built_up_area);
        plot_Sizes=(Spinner)v.findViewById(R.id.plot_Sizes);
        plot_dimensions=(Spinner)v.findViewById(R.id.plot_dimensions);
//        land_parcel=(Spinner)v.findViewById(R.id.land_parcel);

        for(UnitsModel unitsModel:setDefaultSizes){

            if(unitsModel.getUnitTypeId()==18)
                unit_names.add(unitsModel.getUnitName());

            if(unitsModel.getUnitTypeId()==21)
                plot_dimesion_unit_names.add(unitsModel.getUnitName());


        }

        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_item, unit_names);
        built_up_area.setAdapter(arrayAdapter);
        plot_Sizes.setAdapter(arrayAdapter);

//        land_parcel.setAdapter(arrayAdapter);

        if(!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("built_up_Area","").equalsIgnoreCase("")){

            built_up_area.setSelection(Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("built_up_Area","")));

        }
        else{
            built_up_area.setSelection(1);
        }

        if(!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("plot_sizes","").equalsIgnoreCase("")){

            plot_Sizes.setSelection(Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("plot_sizes","")));

        }
        else{
            plot_Sizes.setSelection(1);
        }
        ArrayAdapter plotDimensionAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_item, plot_dimesion_unit_names);
        plot_dimensions.setAdapter(plotDimensionAdapter);
        built_up_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                TextView textView= (TextView) view.findViewById(R.id.text_view);

                for(UnitsModel unitsModel:setDefaultSizes){
                    if(unitsModel.getUnitName().equalsIgnoreCase(textView.getText().toString())){

                        PreferenceConnector.getInstance(getActivity()).savePreferences("built_up_Area",String.valueOf(i));
                        sizeUnitId= String.valueOf(unitsModel.getUnitId());
                        sizeUnitName=textView.getText().toString();
                    }

                }




            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        plot_Sizes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                TextView textView= (TextView) view.findViewById(R.id.text_view);

                for(UnitsModel unitsModel:setDefaultSizes){
                    if(unitsModel.getUnitName().equalsIgnoreCase(textView.getText().toString())){

                        PreferenceConnector.getInstance(getActivity()).savePreferences("plot_sizes",String.valueOf(i));
                        plotSizeUnitId = String.valueOf(unitsModel.getUnitId());
                        plotSizeUnitName = textView.getText().toString();
                    }

                }




            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if(!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("plot_dimensions","").equalsIgnoreCase("")){

            plot_dimensions.setSelection(Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences("plot_dimensions","")));

        }




        plot_dimensions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textView= (TextView) view.findViewById(R.id.text_view);

                for(UnitsModel unitsModel:setDefaultSizes){
                    if(unitsModel.getUnitName().equalsIgnoreCase(textView.getText().toString())){

                        PreferenceConnector.getInstance(getActivity()).savePreferences("plot_dimensions",String.valueOf(i));
                        plot_dimensionsUnitId= String.valueOf(unitsModel.getUnitId());
                        plot_dimensionsUnitName=textView.getText().toString();
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        done=(Button)v.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.PLOT_SIZE_UNIT_ID,plotSizeUnitId);
                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.PLOT_SIZE_UNIT_NAME,plotSizeUnitName);
                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.PLOT_DIMENSION_UNIT_ID,plot_dimensionsUnitId);
                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.PLOT_DIMENSION_UNIT_NAME,plot_dimensionsUnitName);
                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.SIZE_UNIT_ID,sizeUnitId);
                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.SIZE_UNIT_NAME,sizeUnitName);
                Log.e("unit_name",sizeUnitId+ sizeUnitName);

                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog_fragment");
                if (prev != null) {
                    SetDefaultSizeDialogFragment df = (SetDefaultSizeDialogFragment) prev;
                    df.dismiss();
                }
            }
        });
        return v;
    }


}
