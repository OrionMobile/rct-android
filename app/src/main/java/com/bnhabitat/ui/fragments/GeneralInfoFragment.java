package com.bnhabitat.ui.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.StickyScrollView;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.SitePlanModel;
import com.bnhabitat.ui.activities.ProjectDetailActivity;
import com.bnhabitat.ui.activities.ZoomActivity;
import com.bnhabitat.ui.adapters.ThumbListAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralInfoFragment extends Fragment implements CommonAsync.OnAsyncResultListener, View.OnClickListener, StickyScrollView.OnStickChangeListener, TabChangeListener {

    private TextView overview_txt,siteplanLayout,overViewLayout_plan,locationalLayout;
    ArrayList<ProjectsDetailModel.LayoutPlans> layoutPlanses = new ArrayList<>();
    ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList<>();
    EditText search_faq_ques;
    LinearLayout overViewLayout;
    ThumbListAdapter thumbListAdapter;
    LinearLayoutManager linearLayoutManager;



    CardView overview_txtview;
    LinearLayout view_sitepaln;
    CardView layplan;
    LinearLayout view_locationalpaln;
    CardView view_faq;

    ImageView dummy_image;



    ViewPager viewPager;
    RecyclerView thumb_list;
    //    ViewPager locationalviewPager;
    ViewPager layoutplan_image;
    RecyclerView faq_list;
    ImageView info_icon, share_icon, notification_icon, user_icon;
    int overview_count = 0;
    int site_count = 0;
    int location_count = 0;
    int faq_count = 0;
    CircleIndicator indicator;
    //    CircleIndicator locational_indicator;
    String overview;
    private StickyScrollView stickyScrollView;
    String projectId;

    ProjectsDetailModel projectsDetailModel;
    private static final Integer[] IMAGES = {R.drawable.demo_project_image, R.drawable.demo_project_image, R.drawable.demo_project_image, R.drawable.demo_project_image};
    //    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private ArrayList<SitePlanModel> ImagesArray = new ArrayList<SitePlanModel>();
    private ArrayList<ProjectsDetailModel.FAQs> faQses = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.PropertySize> propertySizes = new ArrayList<>();
    ArrayList<ProjectsDetailModel> projectListingModelArrayList = new ArrayList<>();
    int position;
    private LinearLayout locationDisLayout;

    TextView site_and_master_plan_txt;

    public GeneralInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_general_info, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null)
            projectsDetailModel = (ProjectsDetailModel) bundle.getSerializable("projectModel");
        propertySizes = (ArrayList<ProjectsDetailModel.PropertySize>) bundle.getSerializable("propertySizes");
        position = bundle.getInt("position");
        Log.e("position", String.valueOf(position));
        projectListingModelArrayList = (ArrayList<ProjectsDetailModel>) bundle.getSerializable("notification_array");
        faQses = projectsDetailModel.getFaqs();
        stickyScrollView = (StickyScrollView) view.findViewById(R.id.stickyScrollView);
        overview_txtview = (CardView) view.findViewById(R.id.overview_txtview);
        view_sitepaln = (LinearLayout) view.findViewById(R.id.view_sitepaln);
        view_faq = (CardView) view.findViewById(R.id.view_faq);
        thumb_list = (RecyclerView) view.findViewById(R.id.thumb_list);
        layplan = (CardView) view.findViewById(R.id.layplan);
        locationDisLayout = (LinearLayout) view.findViewById(R.id.locationDisLayout);
        overViewLayout_plan = (TextView) view.findViewById(R.id.overViewLayout_plan);
        view_locationalpaln = (LinearLayout) view.findViewById(R.id.view_locationalpaln);

        dummy_image = (ImageView) view.findViewById(R.id.dummy_image);


        overview_txt = (TextView) view.findViewById(R.id.overview_txt);

//        search_faq_ques = (EditText) view.findViewById(R.id.search_faq_ques);
        info_icon = (ImageView) view.findViewById(R.id.info_icon);
        share_icon = (ImageView) view.findViewById(R.id.share_icon);
        notification_icon = (ImageView) view.findViewById(R.id.notification_icon);
        user_icon = (ImageView) view.findViewById(R.id.user_icon);


        overViewLayout = (LinearLayout) view.findViewById(R.id.overViewLayout);

        locationalLayout = (TextView) view.findViewById(R.id.locationalLayout);
        siteplanLayout = (TextView) view.findViewById(R.id.siteplanLayout);
        viewPager = (ViewPager) view.findViewById(R.id.imagesPager);
//
        layoutplan_image = (ViewPager) view.findViewById(R.id.layoutplan_image);
        indicator = (CircleIndicator) view.findViewById(R.id.indicator);
        linearLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
//        locational_indicator = (CircleIndicator) view.findViewById(R.id.locational_indicator);
        stickyScrollView.setOnStickChangeListener(this);
        projectId = getActivity().getIntent().getStringExtra("Id");
//        for(int i=0;i<IMAGES.length;i++)
//            ImagesArray.add(IMAGES[i]);
        overViewLayout_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overViewLayout.setVisibility(View.VISIBLE);
                view_sitepaln.setVisibility(View.GONE);
                siteplanLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                locationalLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                overViewLayout_plan.setBackgroundColor(getResources().getColor(R.color.blue));

            }
        });
        siteplanLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_sitepaln.setVisibility(View.VISIBLE);
                overViewLayout.setVisibility(View.GONE);
                view_locationalpaln.setVisibility(View.GONE);
                siteplanLayout.setBackgroundColor(getResources().getColor(R.color.blue));
                locationalLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                overViewLayout_plan.setBackgroundColor(getResources().getColor(R.color.light_blue));
                indicator.setViewPager(viewPager);

//

            }
        });
        locationalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_locationalpaln.setVisibility(View.VISIBLE);
                view_sitepaln.setVisibility(View.GONE);
                overViewLayout.setVisibility(View.GONE);
                siteplanLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                locationalLayout.setBackgroundColor(getResources().getColor(R.color.blue));
                overViewLayout_plan.setBackgroundColor(getResources().getColor(R.color.light_blue));
//

            }
        });

//
        setLocationDisLayout();
        if (projectsDetailModel.getLayoutPlans().isEmpty()) {
            dummy_image.setVisibility(View.VISIBLE);
            thumb_list.setVisibility(View.GONE);
        } else {
            dummy_image.setVisibility(View.GONE);
            layoutplan_image.setAdapter(new LayoutImagePagerAdapter(getActivity(), projectsDetailModel.getLayoutPlans()));
            thumb_list.setLayoutManager(linearLayoutManager);
            thumb_list.setAdapter(new ThumbListAdapter(getActivity(), projectsDetailModel.getLayoutPlans()));


        }
        if (projectsDetailModel.getAboutSubProject().equalsIgnoreCase("")) {

            overview_txtview.setVisibility(View.GONE);

        }
        else{
            overview_txtview.setVisibility(View.VISIBLE);
        }
        if(projectsDetailModel.getLogoImage().equalsIgnoreCase("")&&projectsDetailModel.getTopImage3().equalsIgnoreCase("")){
            siteplanLayout.setVisibility(View.GONE);
        }

        if(projectsDetailModel.getKeyDistances().isEmpty()){
            locationDisLayout.setVisibility(View.GONE);
        }
        else{
            locationDisLayout.setVisibility(View.VISIBLE);
        }

        overview_txt.setText(Html.fromHtml(projectsDetailModel.getAboutSubProject()));
        SitePlanModel sitePlanModel = new SitePlanModel();
        sitePlanModel.setImage(projectsDetailModel.getLogoImage());
        ImagesArray.add(sitePlanModel);
//
        SitePlanModel sitePlanModel2 = new SitePlanModel();
        sitePlanModel2.setImage(projectsDetailModel.getTopImage3());
        ImagesArray.add(sitePlanModel2);


//        locationalviewPager.setAdapter(new LocationalImagePagerAdapter(getActivity(), projectsDetailModel.getKeyDistances()));
        viewPager.setAdapter(new ImagePagerAdapter(getActivity(), ImagesArray));
//        locationalviewPager.setAdapter(new ImagePagerAdapter(getActivity(),ImagesArray));

        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                projectRelationshipManagersArrayList = projectsDetailModel.getProjectRelationshipManagers();
                Log.e("project_array", String.valueOf(projectRelationshipManagersArrayList.size()));
                if (!projectRelationshipManagersArrayList.isEmpty()) {
                    CustomDialogFragment fragment = CustomDialogFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Arraylist", projectRelationshipManagersArrayList);
                    fragment.setArguments(bundle);

                    fragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "dialog_fragment");
                } else {
                    Toast.makeText(getActivity(), "No RM associated with the project.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationDialogProjectDetail notificationDialogFragment = NotificationDialogProjectDetail.newInstance();

                Bundle bundle = new Bundle();
//
                bundle.putSerializable("projectModel", projectsDetailModel);
//
                notificationDialogFragment.setArguments(bundle);

                notificationDialogFragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "notification_dialog_fragment");

            }
        });
        share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, projectsDetailModel.getTitle());

                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://bnhabitat.com/?" + "p=" + projectsDetailModel.getId());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (projectsDetailModel.getCurrentOffers().equalsIgnoreCase("null") || projectsDetailModel.getCurrentOffers().equalsIgnoreCase("") || projectsDetailModel.getCurrentOffers() == null) {
                    Toast.makeText(getActivity(), "No offers yet", Toast.LENGTH_SHORT).show();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(projectsDetailModel.getCurrentOffers())
                            .setPositiveButton("NOTED", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();
                }
            }
        });
//

        return view;

    }

    private void setLocationDisLayout() {

        locationDisLayout.removeAllViews();

        for (int index = 0; index < projectsDetailModel.getKeyDistances().size(); index++) {

            View view;
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.location_distances_item, null);
            ImageView locationImg = (ImageView) view.findViewById(R.id.locationImg);
            TextView distanceTxt = (TextView) view.findViewById(R.id.distanceTxt);
            TextView distanceTitleTxt = (TextView) view.findViewById(R.id.distanceTitleTxt);

            distanceTitleTxt.setText(projectsDetailModel.getKeyDistances().get(index).getNameOfLocation());
            distanceTxt.setText(projectsDetailModel.getKeyDistances().get(index).getDistance() + " "
                    + projectsDetailModel.getKeyDistances().get(index).getDistanceUnit());

            String url = Urls.BASE_IMAGE_URL
                    + projectsDetailModel.getKeyDistances().get(index).getImage().replace("\\", "/");


            if (null == url || url.equalsIgnoreCase("null")) {

                locationImg.setImageResource(R.drawable.logo);

            } else
                Picasso.with(getActivity())
                        .load(url.replace(" ", "%20"))
                        .placeholder(R.drawable.logo)
                        .into(locationImg);

            locationDisLayout.addView(view);
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROJECT_DETAILS)) {

                try {

                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

//
//
                        ProjectsDetailModel projectsDetailModel = new ProjectsDetailModel();

                        JSONObject resultObject = jsonObject.getJSONObject("Result");
                        projectsDetailModel.setId(resultObject.getInt("Id"));
                        projectsDetailModel.setTitle(resultObject.getString("title"));
                        projectsDetailModel.setBuilderProjectId(resultObject.getInt("builderProjectId"));
                        projectsDetailModel.setLogoImage(resultObject.getString("logoimage"));
                        projectsDetailModel.setTopImage1(resultObject.getString("topImage1"));
                        projectsDetailModel.setTopImage2(resultObject.getString("topImage2"));
                        projectsDetailModel.setTopImage3(resultObject.getString("topImage3"));
                        projectsDetailModel.setAboutSubProject(resultObject.getString("aboutSubProject"));
                        projectsDetailModel.setSpecifications(resultObject.getString("specifications"));
                        projectsDetailModel.setBuilderCompanyId(resultObject.getInt("builderCompanyId"));
                        projectsDetailModel.setBuilderCompanyName(resultObject.getString("builderCompanyName"));
                        projectsDetailModel.setCountryId(resultObject.getInt("CountryId"));
                        projectsDetailModel.setCountryName(resultObject.getString("CountryName"));
                        projectsDetailModel.setCityName(resultObject.getString("CityName"));
                        projectsDetailModel.setAreaUnit(resultObject.getInt("AreaUnit"));
                        projectsDetailModel.setArea(resultObject.getString("Area"));
                        projectsDetailModel.setRegion(resultObject.getString("Region"));
                        projectsDetailModel.setLocality(resultObject.getString("Locality"));
                        projectsDetailModel.setTagline(resultObject.getString("Tagline"));
                        projectsDetailModel.setTypeofProject(resultObject.getInt("TypeofProject"));
                        projectsDetailModel.setProjectStatus(resultObject.getString("ProjectStatus"));
                        projectsDetailModel.setNumberOfParking(resultObject.getInt("NumberOfParking"));
                        projectsDetailModel.setNoOfUnits(resultObject.getString("NoOfUnits"));
                        projectsDetailModel.setNumberOfTowers(resultObject.getString("NumberOfTowers"));
                        projectsDetailModel.setBedRooms(resultObject.getString("BedRooms"));
                        projectsDetailModel.setTypeOfProject(resultObject.getString("TypeOfProject"));
                        projectsDetailModel.setCompanyLogo(resultObject.getString("CompanyLogo"));

                        JSONArray loctionalJson = resultObject.getJSONArray("LayoutPlans");
                        for (int i = 0; i < loctionalJson.length(); i++) {
                            JSONObject jsonObject1 = loctionalJson.getJSONObject(i);
                            ProjectsDetailModel.LayoutPlans layoutPlans = projectsDetailModel.new LayoutPlans();
                            layoutPlans.setBigImage(jsonObject1.getString("BigImage"));
                            layoutPlanses.add(layoutPlans);

                        }


//
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onStickChangeListener(final boolean isSticked) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int margin = 0;


                if (!isSticked) {
                    margin = 10;

                } //else



                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, margin, getResources().getDisplayMetrics());

                params.setMargins(margin, margin, margin, 0);
                layplan.setLayoutParams(params);
            }
        });
    }

    @Override
    public void onStickyScrollChangeListener(int position) {

    }

    @Override
    public void onChangeTab(int position) {

    }

    public class ImagePagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        private ArrayList<SitePlanModel> IMAGES;

// VIEWPAGER ADAPTER------------------------------------------------

        public ImagePagerAdapter(Context context, ArrayList<SitePlanModel> IMAGES) {
            mContext = context;
            this.IMAGES = IMAGES;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        }

        @Override
        public int getCount() {
            return IMAGES.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.site_view_pager, container, false);


            ImageView planImg = (ImageView) itemView.findViewById(R.id.planImg);
            CardView plan = (CardView) itemView.findViewById(R.id.plan);
            plan.setVisibility(View.VISIBLE);
            site_and_master_plan_txt = (TextView) itemView.findViewById(R.id.site_and_master_plan_txt);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }
            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();
            imageLoader.displayImage(Urls.BASE_IMAGE_URL + IMAGES.get(position).getImage(), planImg, options);
//            planImg.setImageResource(IMAGES.get(position));
            planImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    startActivity(new Intent(getActivity(), ZoomActivity.class).putExtra(Constants.ZOOM_LIST, IMAGES));
                    Intent intent=new Intent(getActivity(),ZoomActivity.class);

                    intent.putExtra(Constants.ZOOM_LIST,IMAGES);
                    intent.putExtra("position",String.valueOf(position));
                    startActivity(intent);

                }
            });
            if (position == 0) {
                site_and_master_plan_txt.setText("SitePlan");
            } else {
                site_and_master_plan_txt.setText("MasterPlan");
            }

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

//                    Log.e("imagesPager", "imagesPager" + car_id);
                }

                @Override
                public void onPageSelected(int position) {
//
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


//


            container.addView(itemView, 0);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    //
    public class LayoutImagePagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
         ArrayList<ProjectsDetailModel.LayoutPlans> IMAGES;

        public LayoutImagePagerAdapter(Context context, ArrayList<ProjectsDetailModel.LayoutPlans> IMAGES) {
            mContext = context;
            this.IMAGES = IMAGES;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        }

        @Override
        public int getCount() {

            return IMAGES.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.all_project_item, container, false);


            ImageView planImg = (ImageView) itemView.findViewById(R.id.planImg);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }
            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();
//
            imageLoader.displayImage(Urls.BASE_IMAGE_URL + IMAGES.get(position).getBigImage(), planImg, options);
//            }

//            planImg.setImageResource(IMAGES.get(position));


            layoutplan_image.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


//                    Log.e("imagesPager", "imagesPager" + car_id);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            planImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(getActivity(),ZoomActivity.class);
                    intent.putExtra("layout_plan","layout_planImage");
                    intent.putExtra(Constants.ZOOM_LIST,IMAGES);
                    intent.putExtra("position",String.valueOf(position));
                    startActivity(intent);
//                    getActivity().finish();
                }
            });

//


            container.addView(itemView, 0);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

}
