package com.bnhabitat.ui.fragments;


import android.Manifest;
import android.app.Dialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.contacts.AddressTable;
import com.bnhabitat.data.contacts.ContatctTable;
import com.bnhabitat.data.contacts.EmailTable;
import com.bnhabitat.data.contacts.LocalContatctTable;
import com.bnhabitat.data.contacts.LocalPhoneTable;
import com.bnhabitat.data.contacts.PhoneTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.interfaces.ContactCall;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.GroupModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.activities.AdvanceFilterActivity;
import com.bnhabitat.ui.activities.AssignContactActivity;
import com.bnhabitat.ui.adapters.ContactsAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.onegravity.contactpicker.contact.Contact;
import com.onegravity.contactpicker.group.Group;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static com.bnhabitat.ui.activities.AdvanceFilterActivity.contactsFirebaseModelsNew;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment implements ContactCall, TabChangeListener, CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener, ContactsAdapter.OnItemClick, ContactsAdapter.IsCheckBoxShown {
    private Dialog dialog;
    int counter;
    String userCountryCode = "91";
    ArrayList<ContactsModel.Phones> phones1;
    ArrayList<String> contactList;
    private List<Contact> mContacts;
    ArrayList<String> contact_ids = new ArrayList<>();
    Cursor cursor;
    ArrayList<ContactsModel> contactsFirebaseModels = new ArrayList<>();

    public static ArrayList<ContactsModel> contactsModelsGlobal = new ArrayList<>();

    private ContactsAdapter contactAdapter;
    int top = 0;
    //    private HorilistAdapter horilistAdapter;
    ArrayList<ContactsModel> contactsFirebaseModelsTemp = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager linearLayoutManager1;
    private List<me.everything.providers.android.contacts.Contact> contactLists = new ArrayList<>();
    private IntentFilter mIntentFilter;
    private List<Group> mGroups;
    public static final String mBroadcastStringAction = "com.truiton.broadcast.string";
    private static final int REQUEST_CONTACT = 0;
    String phone_num;
    Button sync_contacts;
    ImageView sync_btn, filter;
    private static final int PERMISSION_REQUEST_CODE = 200;
    LinearLayout no_contact_found, assign_lay, delete_layout;
    CheckBox checkDelete;
    RecyclerView contactsView;
    RecyclerView hori_recycler;
    private boolean isShow = false;
    static EditText search_contacts;
    String inputJson = "";
    TextView more_contact, assign_btn, contact_result;
    int totalRecords;

    View view;
    private int pageSize = 20;


    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_contact, container, false);
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastStringAction);
        initilize(view);
//      getAllContacts();
        isShow = true;

        //  onSyncRequest();
        try {
            PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.IS_SYNC, "");
            ArrayList<ContactsModel.Phones> phones = PhoneTable.getInstance().getPhones();
            ArrayList<GroupModel> groups = ContatctTable.getInstance().getUniquesGroups();
            for (GroupModel groupModel : groups) {
                Log.e("Phone", String.valueOf(groupModel.getName()) + "");
            }
        } catch (Exception e) {

        }
        Bundle bundle = this.getArguments();
        if (bundle != null) {

            if (bundle.getString("AssignContact").equalsIgnoreCase("Assign")) {

//                contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
               if(ContatctTable.getInstance().getContacts().size()!=0)
                for (int i = 0; i < pageSize; i++) {
                    if(i<ContatctTable.getInstance().getContacts().size())
                    contactsFirebaseModels.add(ContatctTable.getInstance().getContacts().get(i));
                }

                if (pageSize >= ContatctTable.getInstance().getContacts().size()) {
                    more_contact.setVisibility(View.GONE);

                } else {
                    more_contact.setVisibility(View.VISIBLE);
                }
                Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
                    public int compare(ContactsModel s1, ContactsModel s2) {
                        return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
                    }
                });
                if (contactsFirebaseModels.isEmpty()) {

                    no_contact_found.setVisibility(View.VISIBLE);

                } else {
                    no_contact_found.setVisibility(View.GONE);
                    adapterMethod();
                }

            } else {

                try {

                    contactAdapter.notifyDataSetChanged();

//                    contactAdapter = new ContactsAdapter(getActivity(), contactsFirebaseModels, this, this);
//                    contactsView.setAdapter(contactAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        else {
//            contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());

            if(ContatctTable.getInstance().getContacts().size()!=0)
            for (int i = 0; i < pageSize; i++) {
                if(i<ContatctTable.getInstance().getContacts().size())
                contactsFirebaseModels.add(ContatctTable.getInstance().getContacts().get(i));
            }
//            Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
//                public int compare(ContactsModel s1, ContactsModel s2) {
//                    return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
//                }
//            });
            if (pageSize >= ContatctTable.getInstance().getContacts().size()) {
                more_contact.setVisibility(View.GONE);

            } else {
                more_contact.setVisibility(View.VISIBLE);
            }
            if (contactsFirebaseModels.isEmpty()) {

                no_contact_found.setVisibility(View.VISIBLE);

            } else {
                no_contact_found.setVisibility(View.GONE);
                adapterMethod();
            }
        }
        PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00");
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AdvanceFilterActivity.class);
                startActivity(intent);

            }
        });
        more_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                contactsFirebaseModels.clear();
                int differece = ContatctTable.getInstance().getContacts().size() - pageSize;
                if (differece > 20) {
                    pageSize = pageSize + 20;
                } else {
                    pageSize = pageSize + differece;
                }


                for (int i = 0; i < pageSize; i++) {
                    if (ContatctTable.getInstance().getContacts().get(i) != null)
                        contactsFirebaseModels.add(ContatctTable.getInstance().getContacts().get(i));
                }
                Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
                    public int compare(ContactsModel s1, ContactsModel s2) {
                        return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
                    }
                });

                if (pageSize >= ContatctTable.getInstance().getContacts().size()) {
                    more_contact.setVisibility(View.GONE);

                } else {
                    more_contact.setVisibility(View.VISIBLE);
                }
                 adapterMethod();
                contactAdapter.notifyDataSetChanged();
//                onSyncRequest();


            }
        });

        sync_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                myRef.removeValue();
                // Android version is lesser than 6.0 or the permission is already granted.


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);
                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
                } else {
                    //    getAllContacts();
                    // Android version is lesser than 6.0 or the permission is already granted.

                }


//
//}
                no_contact_found.setVisibility(View.GONE);


                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.IS_SYNC, "True");


            }
        });
        assign_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AssignContactActivity.class);
                intent.putExtra("contact_ids", contact_ids);
                startActivity(intent);

            }
        });
        sync_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                myRef.removeValue();
                // Android version is lesser than 6.0 or the permission is already granted.


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);
                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
                } else {
                    //       getAllContacts();
                    // Android version is lesser than 6.0 or the permission is already granted.

                }


//
//f
//}
                no_contact_found.setVisibility(View.GONE);


//

//



//

            }
        });


        checkDelete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    delete_layout.setVisibility(View.VISIBLE);

                    if (contactAdapter != null) {

                        contactAdapter.setChange(true);
                    }
                } else {
                    delete_layout.setVisibility(View.GONE);

                    if (contactAdapter != null) {

                        contactAdapter.setChange(false);
                    }
                }
            }
        });

        search_contacts.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                contactsFirebaseModelsTemp.clear();
                List<ContactsModel> list = filter(s.toString(), contactsFirebaseModels, true);


                contactsFirebaseModelsTemp.addAll(list);
                if (contactsFirebaseModelsTemp.isEmpty()) {
                    contactsView.setVisibility(View.GONE);
                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
                    contactsView.setVisibility(View.VISIBLE);
                    no_contact_found.setVisibility(View.GONE);
                    contactsView.setAdapter(new ContactsAdapter(getActivity(), contactsFirebaseModelsTemp, ContactFragment.this, ContactFragment.this));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    @Subscribe
    public void onEvent(String s) {
        phone_num = s;

        Log.e("call=", String.valueOf(s));
        if (isPermissionGranted()) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);

            callIntent.setData(Uri.parse("tel:" + s));
            startActivity(callIntent);
        }
    }


    //    public void onEvent(ArrayList<String> s) {
//
//        Log.e("arraySize",String.valueOf(s.size()));
//
//
//    }
    @Subscribe
    public void onEvent(StatusModel s) {
        no_contact_found.setVisibility(View.GONE);
//        if (s.getPos() == 0) {
//
//            contactsFirebaseModels = ContatctTable.getInstance().getContacts();
//
//
//        } else {
//
//            contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getGroupContacts(s.getId()));
//
//
//        }

        Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
            public int compare(ContactsModel s1, ContactsModel s2) {
                return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
            }
        });
        contactAdapter = new ContactsAdapter(getActivity(), contactsFirebaseModels, this, this);
        contactsView.setAdapter(contactAdapter);

        contactAdapter.notifyDataSetChanged();

//        Intent callIntent = new Intent(Intent.ACTION_CALL);
//
//        callIntent.setData(Uri.parse("tel:" + s));
//        startActivity(callIntent);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        getActivity().registerReceiver(mReceiver, mIntentFilter);
        //  onSyncRequest();

        // show loader just once
        if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.IS_SYNC, "").equalsIgnoreCase("True")) {
            if (contactsModelsGlobal.size() > 0) {

                new CommonSyncwithoutstatus(getActivity(), Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, "") + "/" + Constants.APP_ID + "/" + "0" + "/" +
                        Constants.PAGENUMBER + "/" + pageSize, "", "Loading...",
                        this, Urls.URL_ALL_CONTACTS, Constants.GET, false).execute();



                isShow = false;
            } else {

                new CommonSyncwithoutstatus(getActivity(), Urls.CONTACT_MANAGEMENT_URL +
                        Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, "") + "/" + Constants.APP_ID + "/" + "0" + "/" +
                        Constants.PAGENUMBER + "/" + pageSize, "", "Loading...",
                        this, Urls.URL_ALL_CONTACTS, Constants.GET, true).execute();


            }

            if (contactsFirebaseModelsNew.size() != 0) {

                if (contactAdapter != null) {
                    contactAdapter.removeAllValues();
                }


                contactAdapter = new ContactsAdapter(getActivity(), contactsFirebaseModelsNew, this, this);
                contactsView.setAdapter(contactAdapter);

                contactsFirebaseModelsNew.clear();

            }
        }

        else {
//            contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());

            contactsFirebaseModels.clear();

            if(ContatctTable.getInstance().getContacts().size()!=0)
                for (int i = 0; i < pageSize; i++) {
                    if(i<ContatctTable.getInstance().getContacts().size())
                        contactsFirebaseModels.add(ContatctTable.getInstance().getContacts().get(i));
                }
//            Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
//                public int compare(ContactsModel s1, ContactsModel s2) {
//                    return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
//                }
//            });
            if (pageSize >= ContatctTable.getInstance().getContacts().size()) {
                more_contact.setVisibility(View.GONE);

            } else {
                more_contact.setVisibility(View.VISIBLE);
            }
            if (contactsFirebaseModels.isEmpty()) {

                no_contact_found.setVisibility(View.VISIBLE);

            } else {
                no_contact_found.setVisibility(View.GONE);
                adapterMethod();
            }
        }

    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            hideProgress();
            for (int i = 0; i < pageSize; i++) {

                contactsFirebaseModels.add(ContatctTable.getInstance().getContacts().get(i));
            }
//            contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
            if (contactsFirebaseModels.size() > 0)
                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.IS_SYNC, "True");

            Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
                public int compare(ContactsModel s1, ContactsModel s2) {
                    return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
                }
            });
            adapterMethod();
//            if (contactAdapter == null) {
//                adapterMethod();
//            } else {
//                contactAdapter.updateContactsAdapter(removeDuplicates(contactsFirebaseModels));
//            }
        }
    };

    private void hideProgress() {
        try {
            if ((this.dialog != null) && this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            this.dialog = null;
        }

    }

    private void initilize(View view) {


        sync_contacts = (Button) view.findViewById(R.id.sync_contacts);
        sync_btn = (ImageView) view.findViewById(R.id.sync_btn);
        filter = (ImageView) view.findViewById(R.id.filter);

        contactsView = (RecyclerView) view.findViewById(R.id.contacts);
        hori_recycler = (RecyclerView) view.findViewById(R.id.hori_recycler);
        more_contact = (TextView) view.findViewById(R.id.more_contact);
//        newcontacts = (ListView) view.findViewById(R.id.newcontacts);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        contactsView.setLayoutManager(linearLayoutManager);
        contactsView.setItemAnimator(new DefaultItemAnimator());
//        hori_recycler.setLayoutManager(linearLayoutManager1);
        no_contact_found = (LinearLayout) view.findViewById(R.id.no_contact_found);
        assign_lay = (LinearLayout) view.findViewById(R.id.assign_lay);
        assign_btn = (TextView) view.findViewById(R.id.assign_btn);
        contact_result = (TextView) view.findViewById(R.id.contact_result);
        delete_layout = (LinearLayout) view.findViewById(R.id.delete_layout);
        checkDelete = (CheckBox) view.findViewById(R.id.checkDelete);
        search_contacts = (EditText) view.findViewById(R.id.search_contacts);

//        contactsFirebaseModels=ContatctTable.getInstance().getContacts();
       /* if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.IS_SYNC, "").equalsIgnoreCase("")) {

            no_contact_found.setVisibility(View.GONE);
        } else {
           AlertMessage();
            no_contact_found.setVisibility(View.VISIBLE);
        }*/
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
//            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);
//            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
//        } else {
        // Android version is lesser than 6.0 or the permission is already granted.
//                ContactsProvider contactsProvider = new ContactsProvider(getActivity());
//                datacontacts = contactsProvider.getContacts();
//                contactLists = datacontacts.getList();
//            if (!PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.IS_SYNC, "").equalsIgnoreCase(""))


//
//

//
//        }
//


    }

    private void onSyncRequest() {


        new CommonSyncwithoutstatus(getActivity(), Urls.CONTACT_MANAGEMENT_URL +
                Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, "") + "/" + Constants.APP_ID + "/" + "0" + "/" +
                Constants.PAGENUMBER + "/" + pageSize, "", "Loading...",
                this, Urls.URL_ALL_CONTACTS, Constants.GET, false).execute();


    }


    static ArrayList<ContactsModel> removeDuplicates(List<ContactsModel> list) {

        // Store unique items in result.
        ArrayList<ContactsModel> result = new ArrayList<>();

        // Record encountered Strings in HashSet.
        ArrayList<String> set = new ArrayList<>();

        // Loop over argument list.
        for (ContactsModel item : list) {

            // If String is not in set, add it to the list and the set.
            if (!set.contains(item.getPhonenumber())) {
                result.add(item);
                set.add(item.getPhonenumber());
            }
        }
        return result;
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void contactListCall(List<Contact> countryList, int i) {
        Log.e("call=", String.valueOf(i));
        if (isPermissionGranted()) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);

            callIntent.setData(Uri.parse(countryList.get(i).getPhone(1)));
            startActivity(callIntent);
        }
    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    public static List<ContactsModel> filter(String string,
                                             List<ContactsModel> iterable, boolean byName) {
        if (iterable == null)
            return new LinkedList<>();
        else {
            List<ContactsModel> collected = new LinkedList<>();
            Iterator<ContactsModel> iterator = iterable.iterator();
            if (iterator == null)
                return collected;
            while (iterator.hasNext()) {
                ContactsModel item = iterator.next();

                if (item.getFirstName().toLowerCase().contains(string.toLowerCase()))
                    collected.add(item);

            }
            return collected;
        }
    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_ALL_CONTACTS)) {
                contactsFirebaseModels.clear();
                contactsModelsGlobal.clear();
                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.IS_SYNC, "True");
                try {


                    JSONArray projectsArray = new JSONArray(result);


//                    else {


                    for (int index = 0; index < projectsArray.length(); index++) {
                        JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                        ContactsModel contactsModel = new ContactsModel();
                        contactsModel.setId(jsonObject1.getString("Id"));
                        contactsModel.setFirstName(jsonObject1.getString("FirstName"));
                        contactsModel.setLastName(jsonObject1.getString("LastName"));
                        contactsModel.setMiddleName(jsonObject1.getString("MiddleName"));
                        contactsModel.setDateOfBirthStr(jsonObject1.getString("DateOfBirthStr"));
                        contactsModel.setImageUrl(jsonObject1.getString("ImageUrl"));
                        contactsModel.setTotalRecords(jsonObject1.getString("TotalRecords"));

                        totalRecords = jsonObject1.getInt("TotalRecords");

                        JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("Addresses");
                        ArrayList<AddressModel> addresses = new ArrayList();
                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                            AddressModel addresses1 = new AddressModel();
                            JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                            addresses1.setId(dataObj.getString("Id"));
                            addresses1.setContactId(dataObj.getString("ContactId"));
                            addresses1.setStateName(dataObj.getString("StateName"));
                            addresses1.setCountryId(dataObj.getString("CountryId"));

                            addresses1.setCity_name(dataObj.getString("CityName"));
                            addresses1.setAddress1(dataObj.getString("Address1"));
                            addresses1.setAddress2(dataObj.getString("Address2"));
                            addresses1.setAddress3(dataObj.getString("Address3"));
                            addresses1.setDeveloper_name(dataObj.getString("DeveloperName"));
                            addresses1.setTower(dataObj.getString("Tower"));
                            addresses1.setFloor(dataObj.getString("Floor"));
                            addresses1.setUnitName(dataObj.getString("UnitName"));
                            addresses1.setZipcode(dataObj.getString("ZipCode"));

                            addresses.add(addresses1);
                        }

                        JSONArray phonesJsonArray = projectsArray.getJSONObject(index).getJSONArray("Phones");
                        ArrayList<ContactsModel.Phones> phones = new ArrayList();
                        for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                            ContactsModel.Phones phones1 = contactsModel.new Phones();
                            JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                            phones1.setId(dataObj.getString("Id"));
                            phones1.setContactId(dataObj.getString("ContactId"));
                            phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
                            phones1.setPhoneCode(dataObj.getString("PhoneCode"));
                            phones1.setPhoneType(dataObj.getString("Mode"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                            phones.add(phones1);
                        }
                        JSONArray emailJsonArray = projectsArray.getJSONObject(index).getJSONArray("EmailAddresses");
                        ArrayList<ContactsModel.Email> emails = new ArrayList();
                        for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                            ContactsModel.Email email = contactsModel.new Email();
                            JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                            email.setId(dataObj.getString("Id"));
                            email.setContactId(dataObj.getString("ContactId"));
                            email.setAddress(dataObj.getString("Address"));
                            email.setLabel(dataObj.getString("Label"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                            emails.add(email);
                        }


                        JSONArray contactFileJsonArray = projectsArray.getJSONObject(index).getJSONArray("ContactFiles");
                        ArrayList<ContactsModel.ContactFiles> contactFiles = new ArrayList();
                        for (int indexJ = 0; indexJ < contactFileJsonArray.length(); indexJ++) {

                            ContactsModel.ContactFiles contactFiles1 = contactsModel.new ContactFiles();
                            JSONObject dataObj = contactFileJsonArray.getJSONObject(indexJ);

                            contactFiles1.setId(dataObj.getString("Id"));
                            contactFiles1.setContactId(dataObj.getString("ContactId"));
                            contactFiles1.setName(dataObj.getString("Name"));
                            contactFiles1.setType(dataObj.getString("Type"));
                            contactFiles1.setPath(dataObj.getString("Path"));
                            contactFiles1.setCompanyId(dataObj.getString("CompanyId"));


                            contactFiles.add(contactFiles1);
                        }
//                        JSONArray contactTermJsonArray = projectsArray.getJSONObject(index).getJSONArray("ContactTerms");
//                        ArrayList<ContactsModel.ContactTerms> contactTermses = new ArrayList();
//                        for (int indexJ = 0; indexJ < contactTermJsonArray.length(); indexJ++) {
//                            ContactsModel.ContactTerms contactTerms = contactsModel.new ContactTerms();
//                            JSONObject dataObj = contactTermJsonArray.getJSONObject(indexJ);
//                            contactTerms.setId(dataObj.getString("Id"));
//
//                            contactTerms.setTermId(dataObj.getString("TermId"));
//
//                            JSONObject jsonObject11=dataObj.getJSONObject("Term");
//                            ContactsModel.ContactTerms.Term term=contactsModel.new ContactTerms().new Term();
//                            term.setName(jsonObject11.getString("Name"));
////                            term.set(jsonObject11.getString("Name"));
//                            contactsModel.setGroup_id(jsonObject11.getString("Id"));
//                            contactsModel.setGroup_name(jsonObject11.getString("Name"));
//                            Log.e("jsonObject11","jsonObject11"+jsonObject11.getString("Id"));
////
//                        }

                        JSONArray GroupsJsonArray = projectsArray.getJSONObject(index).getJSONArray("Groups");
                        ArrayList<ContactsModel.Groups> groupses = new ArrayList();
                        for (int indexJ = 0; indexJ < GroupsJsonArray.length(); indexJ++) {
                            ContactsModel.Groups groups = contactsModel.new Groups();
                            JSONObject dataObj = GroupsJsonArray.getJSONObject(indexJ);
//                            groups.setId(dataObj.getString("Id"));
//
//                            groups.setGroupName(dataObj.getString("GroupName"));


//                            term.set(jsonObject11.getString("Name"));
                            contactsModel.setGroup_id(dataObj.getString("Id"));
                            contactsModel.setGroup_name(dataObj.getString("GroupName"));

                        }

                        contactsModel.setAddresses(addresses);
                        contactsModel.setPhones(phones);
                        contactsModel.setContactFiles(contactFiles);
                        contactsModel.setEmails(emails);
                        contactsModel.setGroupses(groupses);

                        contactsFirebaseModels.add(contactsModel);

                        Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
                            public int compare(ContactsModel s1, ContactsModel s2) {
                                return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
                            }
                        });
                        // copy tjis list to another static list
                        contactsModelsGlobal.add(contactsModel);

                    }
                    try {
                        if (totalRecords <= contactsFirebaseModels.size() + 1) {

                            more_contact.setVisibility(View.GONE);

                        } else {

//                            pageSize = pageSize + 20;

                            more_contact.setVisibility(View.VISIBLE);
//                        adapterMethod();
                        }
/*
                        if (contactsFirebaseModels.get(0).getTotalRecords() == contactsFirebaseModels.size() + 1) {

                            more_contact.setVisibility(View.GONE);

                        }
*/

                    } catch (Exception e) {

                    }
                    for (int index = 0; index < contactsFirebaseModels.size(); index++) {
                        ContactsModel contactsModel = contactsFirebaseModels.get(index);


                        String phone = "";
                        String email_address = "";
                        if (contactsModel.getPhones().size() > 0) {
                            phone = contactsModel.getPhones().get(0).getPhoneNumber();
                            if (LocalContatctTable.getInstance().isPhoneAvailable(phone)) {
                                LocalContatctTable.getInstance().updateSync(phone);
                            }
                        }
                        if (contactsModel.getEmails().size() > 0) {
                            email_address = contactsModel.getEmails().get(0).getAddress();
//
                        }


                        if (ContatctTable.getInstance().isContactAvailable(contactsModel.getId())) {
                            ContatctTable.getInstance().updateCategory(Integer.parseInt(contactsModel.getId()), contactsModel.getFirstName(), contactsModel.getLastName(), contactsModel.getCompanyName(), phone, email_address, 1, contactsModel.getGroup_id(), contactsModel.getGroup_name(), contactsModel.getDateOfBirthStr(), contactsModel.getImageUrl(), "");

                            for (int indexJ = 0; indexJ < contactsModel.getPhones().size(); indexJ++) {
                                if (PhoneTable.getInstance().isPhoneAvailable(contactsModel.getPhones().get(indexJ).getId())) {
                                    PhoneTable.getInstance().updateCategory(
                                            Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()),
                                            contactsModel.getPhones().get(indexJ).getPhoneNumber(), "");
                                } else {
                                    PhoneTable.getInstance().write(Integer.parseInt(contactsModel.getPhones().get(indexJ).getContactId()), Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()), contactsModel.getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");

                                }
                            }
//
                            for (int indexJ1 = 0; indexJ1 < contactsModel.getEmails().size(); indexJ1++) {
                                if (EmailTable.getInstance().isEmailAvailable(contactsModel.getEmails().get(indexJ1).getId())) {
                                    EmailTable.getInstance().updateEmail(Integer.parseInt(contactsModel.getEmails().get(indexJ1).getId()), contactsModel.getEmails().get(indexJ1).getAddress(), "");
                                } else {
                                    EmailTable.getInstance().write(Integer.parseInt(contactsModel.getEmails().get(indexJ1).getContactId()), Integer.parseInt(contactsModel.getEmails().get(indexJ1).getId()), contactsModel.getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");

                                }
                            }
                            for (int indexJ2 = 0; indexJ2 < contactsModel.getAddresses().size(); indexJ2++) {
                                if (AddressTable.getInstance().isAddressAvailable(contactsModel.getAddresses().get(indexJ2).getId())) {
                                    AddressTable.getInstance().updateAddress(Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(), contactsModel.getAddresses().get(indexJ2).getAddress2(), contactsModel.getAddresses().get(indexJ2).getAddress3(), contactsModel.getAddresses().get(indexJ2).getDeveloper(), contactsModel.getAddresses().get(indexJ2).getTower(), contactsModel.getAddresses().get(indexJ2).getFloor(), contactsModel.getAddresses().get(indexJ2).getUnitName(), contactsModel.getAddresses().get(indexJ2).getCity_name(), contactsModel.getAddresses().get(indexJ2).getZipcode(), contactsModel.getAddresses().get(indexJ2).getCountryId(), contactsModel.getAddresses().get(indexJ2).getStateName(), "01-01-2017");
                                } else {
                                    AddressTable.getInstance().write(Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getContactId()), Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(), contactsModel.getAddresses().get(indexJ2).getAddress2(), contactsModel.getAddresses().get(indexJ2).getAddress3(), contactsModel.getAddresses().get(indexJ2).getDeveloper(), contactsModel.getAddresses().get(indexJ2).getTower(), contactsModel.getAddresses().get(indexJ2).getFloor(), contactsModel.getAddresses().get(indexJ2).getUnitName(), contactsModel.getAddresses().get(indexJ2).getCity_name(), contactsModel.getAddresses().get(indexJ2).getZipcode(), contactsModel.getAddresses().get(indexJ2).getCountryId(), contactsModel.getAddresses().get(indexJ2).getStateName(), "01-01-2017", "01-01-2017");

                                }

                            }
                        } else {
                            ContatctTable.getInstance().write(Integer.parseInt(contactsModel.getId()), contactsModel.getFirstName(), contactsModel.getLastName(), contactsModel.getCompanyName(), phone, email_address, 1, contactsModel.getGroup_id(), contactsModel.getGroup_name(), contactsModel.getDateOfBirthStr(), contactsModel.getImageUrl(), "01-01-2017", "01-01-2017");
                            for (int indexJ = 0; indexJ < contactsModel.getPhones().size(); indexJ++) {
                                PhoneTable.getInstance().write(Integer.parseInt(contactsModel.getPhones().get(indexJ).getContactId()), Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()), contactsModel.getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");

                            }
                            for (int indexJ1 = 0; indexJ1 < contactsModel.getEmails().size(); indexJ1++) {
                                EmailTable.getInstance().write(Integer.parseInt(contactsModel.getEmails().get(indexJ1).getContactId()), Integer.parseInt(contactsModel.getEmails().get(indexJ1).getId()), contactsModel.getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");

                            }
                            for (int indexJ2 = 0; indexJ2 < contactsModel.getAddresses().size(); indexJ2++) {
                                AddressTable.getInstance().write(Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getContactId()), Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(), contactsModel.getAddresses().get(indexJ2).getAddress2(), contactsModel.getAddresses().get(indexJ2).getAddress3(), contactsModel.getAddresses().get(indexJ2).getDeveloper(), contactsModel.getAddresses().get(indexJ2).getTower(), contactsModel.getAddresses().get(indexJ2).getFloor(), contactsModel.getAddresses().get(indexJ2).getUnitName(), contactsModel.getAddresses().get(indexJ2).getCity_name(), contactsModel.getAddresses().get(indexJ2).getZipcode(), contactsModel.getAddresses().get(indexJ2).getCountryId(), contactsModel.getAddresses().get(indexJ2).getStateName(), "01-01-2017", "01-01-2017");

                            }

                        }
                    }
//                    contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());


                    if (contactsFirebaseModels.isEmpty()) {
//                        contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
                        no_contact_found.setVisibility(View.VISIBLE);
                        more_contact.setVisibility(View.GONE);
//                        Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    }

                    // sorting

/*
                    Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
                        public int compare(ContactsModel s1, ContactsModel s2) {
                            return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
                        }
                    });
*/
                    if (contactsFirebaseModels.isEmpty()) {

                        no_contact_found.setVisibility(View.VISIBLE);

                    } else {
                        no_contact_found.setVisibility(View.GONE);
                        adapterMethod();
                    }


                    Log.e("check", LocalContatctTable.getInstance().getNoSynced().size() + "--");

                    if (LocalContatctTable.getInstance().getNoSynced().size() > 0) {
                        ArrayList<ContactsModel> contactsModels = LocalContatctTable.getInstance().getNoSynced();
                        for (int index = 0; index < contactsModels.size(); index++) {
                            contactsModels.get(index).
                                    setPhones(LocalPhoneTable.getInstance().getPhones(contactsModels.get(index).getId()));

                        }
                        pushContacts(contactsModels);

                    }

                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DATE_TIME, Utils.getInstance().getDateTime());
//                    Intent intent=new Intent(getActivity(), ContactsActivity.class);
//                    getActivity().startActivity(intent);

//
//                    ArrayList<GroupModel> groups = ContatctTable.getInstance().getUniquesGroups();
//                    GroupModel groupModel = new GroupModel();
//                    groupModel.setName("All");
//
//                    groups.add(0, groupModel);
//
////                    horilistAdapter = new HorilistAdapter(getActivity(), groups);
//                    hori_recycler.setAdapter(horilistAdapter);
//// }
                } catch (JSONException e) {

                    e.printStackTrace();

                }


            } else if (which.equalsIgnoreCase(Urls.URL_POST_CONTACTS)) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);
                    JSONObject resultInsert = jsonObject.getJSONObject("Result");
                    JSONArray projectsArray = resultInsert.getJSONArray("Result");

                    if (projectsArray.length() == 0) {
//                        Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                    } else {


                    }


                    LocalContatctTable.getInstance().updateAllSync();
                    Toast.makeText(getActivity(), "Syncing finished ", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {

                    e.printStackTrace();

                }


            }
        }

    }

    private void pushContacts(ArrayList<ContactsModel> contactsModels) {
        try {

            JSONArray jsonArrayTop = new JSONArray();

            for (int i = 0; i < contactsModels.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("FirstName", contactsModels.get(i).getFirstName());
                jsonObject.put("LastName", "");
                jsonObject.put("CompanyId", "1");
                jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));

                JSONArray jsonArray = new JSONArray();
                ArrayList<ContactsModel.Phones> phones = contactsModels.get(i).getPhones();

                for (int j = 0; j < phones.size(); j++) {

                    JSONObject jsonObject1 = new JSONObject();

                    jsonObject1.put("PhoneNumber", phones.get(j).getPhoneNumber());

                    jsonObject1.put("Mode", "");

                    jsonArray.put(jsonObject1);

                }
                JSONArray grouparray = new JSONArray();
                JSONObject jsonObject4 = new JSONObject();
                jsonObject4.put("Id", "");
                grouparray.put(jsonObject4);
                jsonObject.put("Phones", jsonArray);
                jsonArrayTop.put(jsonObject);
            }


            inputJson = jsonArrayTop.toString();

            new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_POST_CONTACTS,
                    inputJson,
                    "Loading...",
                    this,
                    Urls.URL_POST_CONTACTS,
                    Constants.POST).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void adapterMethod() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
              // contactAdapter = new ContactsAdapter(getActivity(), contactsFirebaseModels);
//              if(contactAdapter==null) {
                contactAdapter = new ContactsAdapter(getActivity(), contactsFirebaseModels, ContactFragment.this, ContactFragment.this);
                contactsView.setAdapter(contactAdapter);
                contactAdapter.notifyDataSetChanged();
//                }else{
//                    contactAdapter=null;
//                    contactsView=null;
//                    contactAdapter = new ContactsAdapter(getActivity(), removeDuplicates(contactsFirebaseModels));
//                    contactsView.setAdapter(contactAdapter);
//                }
                // contactAdapter.notifyDataSetChanged();
            }
        });

    }

    public void getAllContacts() {
        contactList = new ArrayList<String>();
        String phoneNumber = null;
        String email = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;
        String LAST_UPDATED_TIME = ContactsContract.CommonDataKinds.Email.CONTACT_LAST_UPDATED_TIMESTAMP;
        StringBuffer output;
        ContentResolver contentResolver = getActivity().getContentResolver();
        String savedDateTime = PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.CONTACT_MODIFIED, "15028907728");
        cursor = contentResolver.query(CONTENT_URI, null, LAST_UPDATED_TIME + ">='" + savedDateTime + "'", null, null);
        ArrayList<ContactsModel> contactsModels = new ArrayList<>();
        // Iterate every contact in the phone
        if (cursor.getCount() > 0) {
            counter = 0;
            while (cursor.moveToNext()) {
                output = new StringBuffer();


                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                String lastUpdated = cursor.getString(cursor.getColumnIndex(LAST_UPDATED_TIME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                Log.e("last updated", lastUpdated + "--" + name);
                if (hasPhoneNumber > 0) {

                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);


                    // Read every email id associated with the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);
                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        output.append("\n Email:" + email);
                    }


                    // Add the contact to the ArrayList
                    Log.e("output", "output" + output.toString());


                    if (!LocalContatctTable.getInstance().isContactAvailable(contact_id)) {

                        ContactsModel contactsModel = new ContactsModel();
                        contactsModel.setFirstName(name);


                        while (phoneCursor.moveToNext()) {

                            ContactsModel.Phones phone = contactsModel.new Phones();
                            phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));


                            String str = phoneNumber.replaceAll("\\D+", "");


                            if (phoneNumber.startsWith("+")) {
                                str = phoneNumber.replaceAll("\\D+", "");
                            } else if (str.startsWith("00")) {
                                str = str.replaceFirst("^0+(?!$)", "");
                            } else if (str.startsWith("0")) {
                                str = str.replaceFirst("^0+(?!$)", userCountryCode);
                            } else {
                                str = userCountryCode + str;
                            }

                            phone.setPhoneNumber(str);
                            Log.e("name", name + " " + str);
                            // phones.add(phone);
                            if (!LocalContatctTable.getInstance().isContactAvailable(contact_id))
                                LocalContatctTable.getInstance().write(Integer.parseInt(contact_id), name, "", "Bn-habitat", str, 0, PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, ""));

                            LocalPhoneTable.getInstance().write(Integer.parseInt(contact_id), 0, str, PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, ""));
                        }
                    }


                    emailCursor.close();
                    phoneCursor.close();
                    Log.e("output", "output" + output.toString());
                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.CONTACT_MODIFIED, String.valueOf(System.currentTimeMillis()));
                }
            }


        }

    }

    private void AlertMessage() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Would you like to Sync your Phone Book Contacts. These contacts will be private to you until you share contact with groups.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);

                            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
                        } else {
                            //     getAllContacts();
                            // Android version is lesser than 6.0 or the permission is already granted.

                        }
                        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.IS_SYNC, "True");
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.IS_SYNC, "True");

                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 2);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onItemClick(ArrayList<String> s) {
        contact_ids = s;
        if (contact_ids.isEmpty()) {
            assign_lay.setVisibility(View.GONE);
        } else {
            assign_lay.setVisibility(View.VISIBLE);
            contact_result.setText("Contacts Result(" + contact_ids.size() + ")");


        }
        Log.e("arraySize", String.valueOf(contact_ids.size()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //    getAllContacts();
                Toast.makeText(getActivity(), " permission granted", Toast.LENGTH_LONG).show();

            } else {


                Toast.makeText(getActivity(), " permission denied", Toast.LENGTH_LONG).show();

            }
        }
    }

    @Override
    public void onCheckClick(boolean isClick) {

        if (isClick) {
            delete_layout.setVisibility(View.VISIBLE);
            checkDelete.setChecked(true);
        } else {
            delete_layout.setVisibility(View.GONE);
            checkDelete.setChecked(false);
        }

    }

    public static void setContactSize(int size) {
        search_contacts.setHint("All contacts " + size + " |Search contact by Name ");


    }

}
