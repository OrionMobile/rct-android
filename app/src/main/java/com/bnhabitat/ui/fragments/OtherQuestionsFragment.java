package com.bnhabitat.ui.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.FeatureModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.ui.activities.InventoryDashboardActivity;
import com.bnhabitat.ui.activities.MyPropertyActivity;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

import static com.bnhabitat.ui.fragments.DetailFragment.imgModel;
import static com.bnhabitat.ui.fragments.DetailFragment.propAreaModel;
import static com.bnhabitat.ui.fragments.LocationFragment.inventoryModelProp;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtherQuestionsFragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener {
    View view;
    RadioButton radio_agent_for_customer, radio_referral_agent, radio_associated_broker, anyother_broker_radio, radio_myself, radio_relative;
    CheckBox accept_check;
    Button other_ques_next;
    EditText  contact_no, email, first_name, last_name;
    LinearLayout relation_lay;
    Spinner relation;
    String submit_befalf, relation_txt, edit_property="";
    ArrayList<PropertyUnitsModel> propertyUnitsModels;
    ArrayList<String> property_units = new ArrayList<>();
    ArrayList<InventoryModel> json_Data;
    SpinnerAdapter adapter;
    String PropertyTypeId;
    private String pid;
    private JSONObject jsonSpecification;
    String Editid;
    LinearLayout linear_layout;
    ScrollView scrollView;
    private String Id =" ";
    int iskeyboaropen=0;

    public OtherQuestionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_other_questions, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        onIntializeView(view);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        property_units.add("Aunt");
        property_units.add("Brother");
        property_units.add("Daughter");
        property_units.add("Father");
        property_units.add("Grandfather");
        property_units.add("Grandmother");
        property_units.add("Husband");
        property_units.add("Mother");
        property_units.add("Sister");
        property_units.add("Son");
        property_units.add("Uncle");
        property_units.add("Wife");
        adapter.addAll(property_units);
        adapter.add(getString(R.string.select_type));
        relation.setAdapter(adapter);
        relation.setSelection(adapter.getCount());
        Bundle bundle = this.getArguments();
        edit_property = bundle.getString("edit") == null ? "" : bundle.getString("edit");

        if (bundle != null) {
            try {

                PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");

                if (!edit_property.equals("")) {
                    Editid = bundle.getString("editId");

                    json_Data = (ArrayList<InventoryModel>) bundle.getSerializable("json_Data");
                    for (int i = 0; i < json_Data.size(); i++) {

                        if (json_Data.get(i).getId().equalsIgnoreCase(Editid)) {

                            Id= json_Data.get(i).getPropertyOtherQuestionses().get(0).getId();

                            if (json_Data.get(i).getPropertyOtherQuestionses().get(0).getSubmitOfBehalfOf().equalsIgnoreCase("Myself")){
                                radio_myself.setChecked(true);
                                radio_relative.setChecked(false);
                                radio_agent_for_customer.setChecked(false);
                                radio_referral_agent.setChecked(false);
                                radio_associated_broker.setChecked(false);
                                anyother_broker_radio.setChecked(false);

                                relation_lay.setVisibility(View.GONE);
                                accept_check.setChecked(true);

                            }

                            else if (json_Data.get(i).getPropertyOtherQuestionses().get(0).getSubmitOfBehalfOf().equalsIgnoreCase("Relative/Assitant/Employee")){
                                radio_myself.setChecked(false);
                                relation_lay.setVisibility(View.VISIBLE);
                                for(int s=0 ; s< property_units.size(); s++){

                                    String com= json_Data.get(i).getPropertyOtherQuestionses().get(0).getRelation();

                                    if (com.equalsIgnoreCase(property_units.get(s)))
                                        relation.setSelection(s);

                                }
                                if (!json_Data.get(i).getPropertyOtherQuestionses().get(0).getFirstName().equalsIgnoreCase("null"))
                                    first_name.setText(json_Data.get(i).getPropertyOtherQuestionses().get(0).getFirstName());
                                if (!json_Data.get(i).getPropertyOtherQuestionses().get(0).getLastName().equalsIgnoreCase("null"))
                                    last_name.setText(json_Data.get(i).getPropertyOtherQuestionses().get(0).getLastName());
                                if (!json_Data.get(i).getPropertyOtherQuestionses().get(0).getContactNo().equalsIgnoreCase("null"))
                                    contact_no.setText(json_Data.get(i).getPropertyOtherQuestionses().get(0).getContactNo());
                                if (!json_Data.get(i).getPropertyOtherQuestionses().get(0).getEmail().equalsIgnoreCase("null"))
                                    email.setText(json_Data.get(i).getPropertyOtherQuestionses().get(0).getEmail());
                                radio_relative.setChecked(true);
                                radio_agent_for_customer.setChecked(false);
                                radio_referral_agent.setChecked(false);
                                radio_associated_broker.setChecked(false);
                                anyother_broker_radio.setChecked(false);
                                accept_check.setChecked(true);

                            }

                            else if (json_Data.get(i).getPropertyOtherQuestionses().get(0).getSubmitOfBehalfOf().equalsIgnoreCase("BN habitat Agents for a Customer")){
                                radio_myself.setChecked(false);
                                radio_relative.setChecked(false);
                                radio_agent_for_customer.setChecked(true);
                                radio_referral_agent.setChecked(false);
                                radio_associated_broker.setChecked(false);
                                anyother_broker_radio.setChecked(false);

                                relation_lay.setVisibility(View.GONE);
                                accept_check.setChecked(true);

                            }

                            else if (json_Data.get(i).getPropertyOtherQuestionses().get(0).getSubmitOfBehalfOf().equalsIgnoreCase("BN habitat Referral Program Agent")){
                                radio_myself.setChecked(false);
                                radio_relative.setChecked(false);
                                radio_agent_for_customer.setChecked(false);
                                radio_referral_agent.setChecked(true);
                                radio_associated_broker.setChecked(false);
                                anyother_broker_radio.setChecked(false);

                                relation_lay.setVisibility(View.GONE);
                                accept_check.setChecked(true);

                            }

                            else if (json_Data.get(i).getPropertyOtherQuestionses().get(0).getSubmitOfBehalfOf().equalsIgnoreCase("BN habitat Associated Broker")){
                                radio_myself.setChecked(false);
                                radio_relative.setChecked(false);
                                radio_agent_for_customer.setChecked(false);
                                radio_referral_agent.setChecked(false);
                                radio_associated_broker.setChecked(true);
                                anyother_broker_radio.setChecked(false);

                                relation_lay.setVisibility(View.GONE);
                                accept_check.setChecked(true);

                            }

                            else if (json_Data.get(i).getPropertyOtherQuestionses().get(0).getSubmitOfBehalfOf().equalsIgnoreCase("AnyOther Broker is not authorised to add Property\nhere if he wants to add then he is directed to Real Catalyst APP")){
                                radio_myself.setChecked(false);
                                radio_relative.setChecked(false);
                                radio_agent_for_customer.setChecked(false);
                                radio_referral_agent.setChecked(false);
                                radio_associated_broker.setChecked(false);
                                anyother_broker_radio.setChecked(true);

                                relation_lay.setVisibility(View.GONE);
                                accept_check.setChecked(true);

                            }

                            else {

                                radio_myself.setChecked(true);
                                radio_relative.setChecked(false);
                                radio_agent_for_customer.setChecked(false);
                                radio_referral_agent.setChecked(false);
                                radio_associated_broker.setChecked(false);
                                anyother_broker_radio.setChecked(false);

                                relation_lay.setVisibility(View.GONE);
                                accept_check.setChecked(true);
                            }

                        }


                    }
                }

            } catch (Exception e) {
                 e.printStackTrace();
            }
            if (bundle != null) {
                try {
                    pid = bundle.getString("pid");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            propertyUnitsModels = (ArrayList<PropertyUnitsModel>) bundle.getSerializable("propertyunits");
        }


        relation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (relation.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    relation_txt = relation.getSelectedItem().toString();

                    Log.e("chooseText", "chooseText" + relation_txt.toLowerCase());

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

/*
        linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(iskeyboaropen==1) {
                  hideSoftKeyboard(getActivity());
                    iskeyboaropen=0;
                }
            }
        });
*/

        linear_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = linear_layout.getRootView().getHeight() - linear_layout.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");

                    iskeyboaropen=1;
                } else {
                    Log.e("MyActivity", "keyboard closed");
                    iskeyboaropen=0;
                }
            }
        });

/*
        scrollView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              hideSoftKeyboard(getActivity());
            }
        });
*/

        return view;
    }

    private void onIntializeView(View view) {
        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        relation_lay = (LinearLayout) view.findViewById(R.id.relation_lay);
        accept_check = (CheckBox) view.findViewById(R.id.accept_check);

        other_ques_next = (Button) view.findViewById(R.id.other_ques_next);
        contact_no = (EditText) view.findViewById(R.id.contact_no);
        email = (EditText) view.findViewById(R.id.email);
        first_name = (EditText) view.findViewById(R.id.first_name);
        last_name = (EditText) view.findViewById(R.id.last_name);
        relation = (Spinner) view.findViewById(R.id.relation);

        radio_myself = (RadioButton) view.findViewById(R.id.radio_myself);
        radio_relative = (RadioButton) view.findViewById(R.id.radio_relative);
        radio_agent_for_customer = (RadioButton) view.findViewById(R.id.radio_agent_for_customer);
        radio_referral_agent = (RadioButton) view.findViewById(R.id.radio_referral_agent);
        radio_associated_broker = (RadioButton) view.findViewById(R.id.radio_associated_broker);
        anyother_broker_radio = (RadioButton) view.findViewById(R.id.anyother_broker_radio);
        radio_myself.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_agent_for_customer.setChecked(false);
                    anyother_broker_radio.setChecked(false);
                    radio_referral_agent.setChecked(false);
                    radio_associated_broker.setChecked(false);
                    radio_relative.setChecked(false);
                    relation_lay.setVisibility(View.GONE);
                    submit_befalf = radio_myself.getText().toString();
                }

            }
        });
        radio_agent_for_customer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_myself.setChecked(false);
                    anyother_broker_radio.setChecked(false);
                    radio_referral_agent.setChecked(false);
                    radio_associated_broker.setChecked(false);
                    radio_relative.setChecked(false);
                    relation_lay.setVisibility(View.GONE);
                    submit_befalf = radio_agent_for_customer.getText().toString();
                }

            }
        });
        radio_associated_broker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_myself.setChecked(false);
                    anyother_broker_radio.setChecked(false);
                    radio_referral_agent.setChecked(false);
                    radio_agent_for_customer.setChecked(false);
                    radio_relative.setChecked(false);
                    relation_lay.setVisibility(View.GONE);
                    submit_befalf = radio_associated_broker.getText().toString();
                }

            }
        });
        radio_referral_agent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_myself.setChecked(false);
                    anyother_broker_radio.setChecked(false);
                    radio_associated_broker.setChecked(false);
                    radio_agent_for_customer.setChecked(false);
                    radio_relative.setChecked(false);
                    relation_lay.setVisibility(View.GONE);
                    submit_befalf = radio_referral_agent.getText().toString();
                }

            }
        });
        radio_relative.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_myself.setChecked(false);
                    anyother_broker_radio.setChecked(false);
                    radio_associated_broker.setChecked(false);
                    radio_agent_for_customer.setChecked(false);
                    radio_referral_agent.setChecked(false);
                    relation_lay.setVisibility(View.VISIBLE);
                    submit_befalf = radio_relative.getText().toString();
                }

            }
        });
        anyother_broker_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_myself.setChecked(false);
                    radio_referral_agent.setChecked(false);
                    radio_associated_broker.setChecked(false);
                    radio_agent_for_customer.setChecked(false);
                    radio_relative.setChecked(false);
                    relation_lay.setVisibility(View.GONE);
                    submit_befalf = anyother_broker_radio.getText().toString();
                }

            }
        });

        other_ques_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (accept_check.isChecked()) {

                  /*  if (radio_relative.isChecked()){

                     *//*   if(!email.getText().toString().equalsIgnoreCase("") && !email.getText().toString().equalsIgnoreCase("null")){

                            if (validateEmail(email.getText().toString()))
                                onOtherExpensesdata();
                            else
                                Toast.makeText(getActivity(), "Please add correct email", Toast.LENGTH_SHORT).show();

                        }else
                            onOtherExpensesdata();*//*

                    }

                    else*/
                    onOtherExpensesdata();

                }

                else {

                    Utils.showErrorMessage("Please accept brokerage policy", getActivity());

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");

                            JSONArray propJsonArray = jsonObject2.getJSONArray("PropertyAreas");

                            String message = jsonObject1.getString("Message");

                                //Utils.showSuccessErrorMessage("Success", "Inventory is added successfully", "Ok", getActivity());
                            Utils.showSuccessErrorMessage("Success", message, "Ok", getActivity());

                        } else {

                           Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(getActivity(), MyPropertyActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

            //getActivity().finish();
        }
    }

    public void onOtherExpensesdata() {

        try {

            // location frag data
            jsonSpecification = new JSONObject();
            // jsonSpecification.put("Id", pid);
            jsonSpecification.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("IsDraft", 0);
            jsonSpecification.put("Cam", OtherExpensesFragment.cam_string);

            if (!edit_property.equals("")) {

                jsonSpecification.put("Id", Editid);

            } else {

                jsonSpecification.put("Id", pid);

            }

            // other ques
            JSONArray otherQuesArray = new JSONArray();
            JSONObject otherQuestionJson = new JSONObject();
            if (Id.equalsIgnoreCase(" "))
            otherQuestionJson.put("Id", "0");
            else
            otherQuestionJson.put("Id", Id);

            otherQuestionJson.put("SubmitOfBehalfOf", submit_befalf);
            otherQuestionJson.put("Relation", relation_txt);
            otherQuestionJson.put("FirstName", first_name.getText().toString());
            otherQuestionJson.put("LastName", last_name.getText().toString());
            otherQuestionJson.put("ContactNo", contact_no.getText().toString());
            otherQuestionJson.put("Email", email.getText().toString());
            otherQuestionJson.put("IsDeletable", false);
            otherQuestionJson.put("IsModify", true);

            otherQuesArray.put(otherQuestionJson);

            jsonSpecification.put("PropertyOtherQuestions", otherQuesArray);

            postProperty();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void postProperty() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_POST,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = jsonSpecification.toString();
        } catch (Exception e) {

        }

        return inputStr;
    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    private boolean validateEmail(String data){
        Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher emailMatcher = emailPattern.matcher(data);
        return emailMatcher.matches();
    }
}
