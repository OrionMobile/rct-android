package com.bnhabitat.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.FeatureModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.PropAreaModel;
import com.bnhabitat.models.PropRoomModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.SpecificationListModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.activities.MyPropertyActivity;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.adapters.FurnitureAdapter;
import com.bnhabitat.ui.adapters.SoftFurnishingAdapter;
import com.bnhabitat.ui.adapters.SpecificationAdapter;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.ui.adapters.WhiteGoodsAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import de.greenrobot.event.EventBus;


public class CommercialSpecificationFragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener {
    ArrayList<PropertyBedroomModel> room_list = new ArrayList<>();
    ArrayList<String> room_name = new ArrayList<>();
    SpinnerAdapter spinnerAdapter;
    RecyclerView rv_specification, furniture_listing, soft_furniture_listing, white_goods_listing;
//    int insertIndex = 7;
//    int insertIndex1 = 1;
//    int insertIndex2 = 1;
//    int insertIndex3 = 1;
    int iskeyboaropen=0;
    SpecificationAdapter specificationAdapter;
    FurnitureAdapter furnitureAdapter;
    SoftFurnishingAdapter softFurnishingAdapter;
    WhiteGoodsAdapter whiteGoodsAdapter;

    TextView  specification_name, specification_name_1, specification_name_soft, specification_name_other, specification_name_white_other;
    Spinner attched_room_balcony, attched_room_balcony_1, attached_room;
    ArrayList<SpecificationListModel> specificationListModels = new ArrayList<>();
   // ArrayList<SpecificationListModel> specification_array = new ArrayList<>();

    ArrayList<StatusModel> temp_specification_array = new ArrayList<>();
    ArrayList<StatusModel> temp_specification_array1 = new ArrayList<>();
    ArrayList<SpecificationListModel> furnitureListModels = new ArrayList<>();
    ArrayList<SpecificationListModel> softfurnitureListModels = new ArrayList<>();
    ArrayList<SpecificationListModel> whiteGoodsListModels = new ArrayList<>();
   // ArrayList<SpecificationListModel> furniture_array = new ArrayList<>();
    ArrayList<StatusModel> temp_furniture_array = new ArrayList<>();
    ArrayList<String> soft_furnishing_Models = new ArrayList<>();
  //  ArrayList<SpecificationListModel> soft_furnishing_array = new ArrayList<>();
    ArrayList<StatusModel> temp_soft_furnishing_array = new ArrayList<>();
  //  ArrayList<SpecificationListModel> whitegoods_array = new ArrayList<>();
    ArrayList<SpecificationListModel> temp_whitegoods_array = new ArrayList<>();

    LinearLayout furnitutre_list, soft_furnishing_list, white_goods_list, furnitutre_lay, soft_furnishing_lay, white_goods_lay;
    SendMessage SM;
    public ImageView next_specification_btn;
    View view, furniture_view, soft_furnishing_view, white_goods_view;
    String PropertyId;
    CheckBox soft_furnishing_check, furnishing_check, specification_check, white_goods_check;
    LinearLayoutManager linearLayoutManager, linearLayoutManager1, linearLayoutManager2, linearLayoutManager3;
    String pid;
    String PropertyTypeId;
    private JSONObject jsonSpecification;
    private JSONArray jsonSpecific = new JSONArray();
    private ArrayList<PropAreaModel> propArrayList = new ArrayList<>();
    String edit_property;
    private String Editid;
    private ArrayList<InventoryModel> json_Data;
    LinearLayout linear_layout,any_other_specification ,any_other_furniture, any_other_soft_furnishing, any_other_white_goods;
    NestedScrollView scroll_view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_commercial_specification, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);
        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);
        next_specification_btn = (ImageView) view.findViewById(R.id.next_specification_btn);
        rv_specification = (RecyclerView) view.findViewById(R.id.specification_list);
        furniture_listing = (RecyclerView) view.findViewById(R.id.furniture_listing);
        soft_furniture_listing = (RecyclerView) view.findViewById(R.id.soft_furniture_listing);
        white_goods_listing = (RecyclerView) view.findViewById(R.id.white_goods_listing);
        furnitutre_list = (LinearLayout) view.findViewById(R.id.furnitutre_list);
        furnitutre_lay = (LinearLayout) view.findViewById(R.id.furnitutre_lay);
        white_goods_list = (LinearLayout) view.findViewById(R.id.white_goods_list);
        white_goods_lay = (LinearLayout) view.findViewById(R.id.white_goods_lay);
        soft_furnishing_list = (LinearLayout) view.findViewById(R.id.soft_furnishing_list);
        soft_furnishing_lay = (LinearLayout) view.findViewById(R.id.soft_furnishing_lay);
        any_other_specification = (LinearLayout) view.findViewById(R.id.any_other_specification);
        any_other_furniture = (LinearLayout) view.findViewById(R.id.any_other_furniture);
        any_other_white_goods = (LinearLayout) view.findViewById(R.id.any_other_white_goods);
        any_other_soft_furnishing = (LinearLayout) view.findViewById(R.id.any_other_soft_furnishing);
        soft_furnishing_check = (CheckBox) view.findViewById(R.id.soft_furnishing_check);
        furnishing_check = (CheckBox) view.findViewById(R.id.furnishing_check);
        specification_check = (CheckBox) view.findViewById(R.id.specification_check);
        white_goods_check = (CheckBox) view.findViewById(R.id.white_goods_check);
        Bundle bundle = this.getArguments();
        pid = LocationFragment.pid;
        getBedroomList();
        ArrayList<SpecificationListModel.PropertyBedroom> propertyBedrooms = new ArrayList<>();


    //  addDataToList();

        if (bundle != null) {
            try {
                PropertyId = bundle.getString("PropertyId") == null ? "" : bundle.getString("PropertyId");
                PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");

            } catch (Exception e) {

            }
        }

        try {
            edit_property = bundle.getString("edit") == null ? "" : bundle.getString("edit");
            if (!edit_property.equals("")) {
                Editid = bundle.getString("editId");
                json_Data = (ArrayList<InventoryModel>) bundle.getSerializable("json_Data");
                PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");

                for (int i = 0; i < json_Data.size(); i++) {

                    if (json_Data.get(i).getId().equalsIgnoreCase(Editid)) {


                        if (json_Data.get(i).getPropertySpecificationses().size()>0){

                            specificationListModels.clear();
                            furnitureListModels.clear();
                            softfurnitureListModels.clear();
                            whiteGoodsListModels.clear();

                        }

                        else {

                            addDataToList();
                            addFurnitureDataToList();
                            addSoftFurnitureDataToList();
                            addWhiteDataToList();
                        }

                        // set feature list and parking list

                        for (int a=0;a<json_Data.get(i).getPropertySpecificationses().size();a++){

                            if (json_Data.get(i).getPropertySpecificationses().get(a).getCategory().equalsIgnoreCase("Specification")){

                                SpecificationListModel specificationListModel = new SpecificationListModel();
                                specificationListModel.setId(Integer.parseInt(json_Data.get(i).getPropertySpecificationses().get(a).getId()));
                                specificationListModel.setName(json_Data.get(i).getPropertySpecificationses().get(a).getName());
                                specificationListModel.setCategory(json_Data.get(i).getPropertySpecificationses().get(a).getCategory());
                                specificationListModel.setDescription(json_Data.get(i).getPropertySpecificationses().get(a).getDescription());

                                for (int ss=0; ss<json_Data.get(i).getPropertySpecificationses().get(a).getPropertyBedrooms().size();ss++){

                                    SpecificationListModel.PropertyBedroom propRoomModel = new SpecificationListModel.PropertyBedroom();
                                    propRoomModel.setId(json_Data.get(i).getPropertySpecificationses().get(a).getPropertyBedrooms().get(ss).getId());

                                    propertyBedrooms.add(propRoomModel);

                                }
                                specificationListModel.setPropertyBedrooms(propertyBedrooms);

                                specificationListModels.add(specificationListModel);

                            }

                            else if (json_Data.get(i).getPropertySpecificationses().get(a).getCategory().equalsIgnoreCase("Furniture")){

                                SpecificationListModel specificationListModel = new SpecificationListModel();
                                specificationListModel.setId(Integer.parseInt(json_Data.get(i).getPropertySpecificationses().get(a).getId()));
                                specificationListModel.setName(json_Data.get(i).getPropertySpecificationses().get(a).getName());
                                specificationListModel.setCategory(json_Data.get(i).getPropertySpecificationses().get(a).getCategory());
                                specificationListModel.setDescription(json_Data.get(i).getPropertySpecificationses().get(a).getDescription());
                                // specificationListModel.setPropertyBedrooms(json_Data.get(i).getPropertySpecificationses().get(a).get());

                                furnitureListModels.add(specificationListModel);

                            }

                            else  if (json_Data.get(i).getPropertySpecificationses().get(a).getCategory().equalsIgnoreCase("Softfurniture")){

                                SpecificationListModel specificationListModel = new SpecificationListModel();
                                specificationListModel.setId(Integer.parseInt(json_Data.get(i).getPropertySpecificationses().get(a).getId()));
                                specificationListModel.setName(json_Data.get(i).getPropertySpecificationses().get(a).getName());
                                specificationListModel.setCategory(json_Data.get(i).getPropertySpecificationses().get(a).getCategory());
                                specificationListModel.setDescription(json_Data.get(i).getPropertySpecificationses().get(a).getDescription());
                                // specificationListModel.setPropertyBedrooms(json_Data.get(i).getPropertySpecificationses().get(a).get());

                                softfurnitureListModels.add(specificationListModel);

                            }
                            else
                            {

                                SpecificationListModel specificationListModel = new SpecificationListModel();
                                specificationListModel.setId(Integer.parseInt(json_Data.get(i).getPropertySpecificationses().get(a).getId()));
                                specificationListModel.setName(json_Data.get(i).getPropertySpecificationses().get(a).getName());
                                specificationListModel.setCategory(json_Data.get(i).getPropertySpecificationses().get(a).getCategory());
                                specificationListModel.setDescription(json_Data.get(i).getPropertySpecificationses().get(a).getDescription());
                                specificationListModel.setCapacity(json_Data.get(i).getPropertySpecificationses().get(a).getCapacity());

                                whiteGoodsListModels.add(specificationListModel);
                            }

                        }

                        setSpecificationAdapter();
                        setFurnitureAdapter();
                        setSoftFurnitureAdapter();
                        setwhiteGoodsAdapter();

                    }

                }

            }


            else {

                addDataToList();
                addFurnitureDataToList();
                addSoftFurnitureDataToList();
                addWhiteDataToList();

            }

        } catch (Exception e) {

        }

        linearLayoutManager = new LinearLayoutManager(getActivity());
        rv_specification.setLayoutManager(linearLayoutManager);

        linearLayoutManager1 = new LinearLayoutManager(getActivity());
        furniture_listing.setLayoutManager(linearLayoutManager1);

        linearLayoutManager2 = new LinearLayoutManager(getActivity());
        soft_furniture_listing.setLayoutManager(linearLayoutManager2);

        linearLayoutManager3 = new LinearLayoutManager(getActivity());
        white_goods_listing.setLayoutManager(linearLayoutManager3);

/*
        linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(iskeyboaropen==1) {
                    hideSoftKeyboard(getActivity());
                    iskeyboaropen=0;
                }
            }
        });
*/

        linear_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = linear_layout.getRootView().getHeight() - linear_layout.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");

                    iskeyboaropen=1;
                } else {
                    Log.e("MyActivity", "keyboard closed");
                    iskeyboaropen=0;
                }
            }
        });

/*
        scroll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(getActivity());
            }
        });
*/

        furnishing_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    furnitutre_lay.setVisibility(View.VISIBLE);


                } else {
                    furnitutre_lay.setVisibility(View.GONE);
                }
            }
        });

        next_specification_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addSpecifications();

            }
        });

        soft_furnishing_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {


                    soft_furnishing_lay.setVisibility(View.VISIBLE);

                } else {
                    soft_furnishing_lay.setVisibility(View.GONE);
                }
            }
        });

        white_goods_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    white_goods_lay.setVisibility(View.VISIBLE);

                } else {
                    white_goods_lay.setVisibility(View.GONE);
                }
            }
        });
        specification_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rv_specification.setVisibility(View.VISIBLE);
                } else {
                    rv_specification.setVisibility(View.GONE);
                }
            }
        });

        furnishing_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    furnitutre_lay.setVisibility(View.VISIBLE);


                } else {
                    furnitutre_lay.setVisibility(View.GONE);
                }
            }
        });

        soft_furnishing_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {


                    soft_furnishing_lay.setVisibility(View.VISIBLE);

                } else {
                    soft_furnishing_lay.setVisibility(View.GONE);
                }
            }
        });
        white_goods_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    white_goods_lay.setVisibility(View.VISIBLE);

                } else {
                    white_goods_lay.setVisibility(View.GONE);
                }
            }
        });
        specification_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    rv_specification.setVisibility(View.VISIBLE);
                } else {
                    rv_specification.setVisibility(View.GONE);
                }
            }
        });

        any_other_specification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!room_list.isEmpty()) {
                    SpecificationListModel specificationListModel = new SpecificationListModel();
                    specificationListModel.setDescription("Others");
                    specificationListModel.setId(0);
                    specificationListModel.setCategory("Specification");
                    specificationListModel.setName("Other");

                    specificationListModels.add(specificationListModel);
                    specificationAdapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(getActivity(), "there is no room", Toast.LENGTH_SHORT).show();
                }


            }
        });

        any_other_furniture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!room_list.isEmpty()) {
                    SpecificationListModel specificationListModel = new SpecificationListModel();
                    specificationListModel.setDescription("Others");
                    specificationListModel.setId(0);
                        specificationListModel.setCategory("Furniture");
                    specificationListModel.setName("");

                    furnitureListModels.add(specificationListModel);
                  //  furnitureAdapter.notifyItemInserted(insertIndex);
                    furnitureAdapter.notifyDataSetChanged();

                   // setFurnitureAdapter();

                }

                else {
                    Toast.makeText(getActivity(), "there is no room", Toast.LENGTH_SHORT).show();
                }


            }
        });

        any_other_soft_furnishing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!room_list.isEmpty()) {
                    SpecificationListModel specificationListModel = new SpecificationListModel();
                    specificationListModel.setDescription("Others");
                    specificationListModel.setId(0);
                    specificationListModel.setCategory("Softfurniture");
                    specificationListModel.setName("");
                    softfurnitureListModels.add(specificationListModel);

                //    softFurnishingAdapter.notifyItemInserted(insertIndex);
                    softFurnishingAdapter.notifyDataSetChanged();
                //   setSoftFurnitureAdapter();

                }
                else {
                    Toast.makeText(getActivity(), "there is no room", Toast.LENGTH_SHORT).show();
                }


            }
        });

        any_other_white_goods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!room_list.isEmpty()) {
                    SpecificationListModel specificationListModel = new SpecificationListModel();
                    specificationListModel.setDescription("Others");
                    specificationListModel.setId(0);
                    specificationListModel.setCategory("WhiteGood");
                    specificationListModel.setName("");
                    specificationListModel.setCapacity("");

                    whiteGoodsListModels.add(specificationListModel);

                 //   whiteGoodsAdapter.notifyItemInserted(insertIndex);
                    whiteGoodsAdapter.notifyDataSetChanged();
                //    setwhiteGoodsAdapter();

                }
                else {
                    Toast.makeText(getActivity(), "there is no room", Toast.LENGTH_SHORT).show();
                }


            }
        });


        return view;
    }

    private void setFurnitureAdapter() {

        furnitureAdapter = new FurnitureAdapter(getActivity(), furnitureListModels, room_list);
        furniture_listing.setAdapter(furnitureAdapter);
    }

    private void setSoftFurnitureAdapter() {

        softFurnishingAdapter = new SoftFurnishingAdapter(getActivity(), softfurnitureListModels, room_list);
        soft_furniture_listing.setAdapter(softFurnishingAdapter);
    }
    private void setwhiteGoodsAdapter() {

        whiteGoodsAdapter = new WhiteGoodsAdapter(getActivity(), whiteGoodsListModels, room_list);
        white_goods_listing.setAdapter(whiteGoodsAdapter);
    }

    private void setSpecificationAdapter() {

        specificationAdapter = new SpecificationAdapter(getActivity(), specificationListModels, room_list);
        rv_specification.setAdapter(specificationAdapter);

    }

    private void addDataToList() {

        specificationListModels.clear();

        SpecificationListModel specificationListModel = new SpecificationListModel();
        specificationListModel.setDescription("Flooring");
        specificationListModel.setCategory("Specification");
        specificationListModel.setName("");
        specificationListModel.setId(0);

        specificationListModels.add(specificationListModel);

        SpecificationListModel specificationListModel1 = new SpecificationListModel();
        specificationListModel1.setDescription("Walls");
        specificationListModel1.setCategory("Specification");
        specificationListModel1.setName("");
        specificationListModel1.setId(0);
        specificationListModels.add(specificationListModel1);

        SpecificationListModel specificationListModel2 = new SpecificationListModel();
        specificationListModel2.setDescription("Ceiling");
        specificationListModel2.setCategory("Specification");
        specificationListModel2.setName("");
        specificationListModel2.setId(0);
        specificationListModels.add(specificationListModel2);

        SpecificationListModel specificationListModel3 = new SpecificationListModel();
        specificationListModel3.setDescription("Windows");
        specificationListModel3.setCategory("Specification");
        specificationListModel3.setName("");
        specificationListModel3.setId(0);
        specificationListModels.add(specificationListModel3);

        SpecificationListModel specificationListModel4 = new SpecificationListModel();
        specificationListModel4.setDescription("Doors");
        specificationListModel4.setCategory("Specification");
        specificationListModel4.setName("");
        specificationListModel4.setId(0);
        specificationListModels.add(specificationListModel4);

        SpecificationListModel specificationListModel5 = new SpecificationListModel();
        specificationListModel5.setDescription("Storage");
        specificationListModel5.setCategory("Specification");
        specificationListModel5.setName("");
        specificationListModel5.setId(0);
        specificationListModels.add(specificationListModel5);

        SpecificationListModel specificationListModel6 = new SpecificationListModel();
        specificationListModel6.setDescription("Sanitary Ware");
        specificationListModel6.setCategory("Specification");
        specificationListModel6.setName("");
        specificationListModel6.setId(0);
        specificationListModels.add(specificationListModel6);

        setSpecificationAdapter();


    }

    private void addFurnitureDataToList() {

        SpecificationListModel specificationListModel = new SpecificationListModel();
        specificationListModel.setDescription("Modular Kitchen");
        specificationListModel.setCategory("Furniture");
        specificationListModel.setName("");
        specificationListModel.setId(0);
        furnitureListModels.add(specificationListModel);

        setFurnitureAdapter();

    }

    private void addSoftFurnitureDataToList() {

        SpecificationListModel specificationListModel = new SpecificationListModel();
        specificationListModel.setDescription("Curtains");
        specificationListModel.setCategory("Softfurniture");
        specificationListModel.setName("");
        specificationListModel.setId(0);
        softfurnitureListModels.add(specificationListModel);

        setSoftFurnitureAdapter();

    }

    private void addWhiteDataToList() {

        SpecificationListModel specificationListModel = new SpecificationListModel();
        specificationListModel.setDescription("Air Conditioner");
        specificationListModel.setCategory("WhiteGood");
        specificationListModel.setName("");
        specificationListModel.setId(0);
        specificationListModel.setCapacity("");

        whiteGoodsListModels.add(specificationListModel);

        setwhiteGoodsAdapter();

    }


    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
    String cateroryString="";
    public void onEvent(String categry){
        cateroryString=categry;

    }

    // Specification
    public void onEvent(ArrayList<SpecificationListModel> specificationListModels) {

        if(cateroryString.equalsIgnoreCase("Specification")){
            this.specificationListModels=specificationListModels;

        }
        else if(cateroryString.equalsIgnoreCase("Furniture")){
            this.furnitureListModels=specificationListModels;
            }
        else if(cateroryString.equalsIgnoreCase("Softfurniture")){
            this.softfurnitureListModels=specificationListModels;

        }
        else  if(cateroryString.equalsIgnoreCase("Whitegood")){
            this.whiteGoodsListModels=specificationListModels;

        }
/*
        if (specificationListModels!=null){

            for (int i = 0; i < specificationListModels.size(); i++) {

                if (specificationListModels.get(i).getCategory().equals("Specification")) {

                    if (!specificationListModels.get(i).getName().equals("")) {

                        SpecificationListModel specificationListModel = new SpecificationListModel();
                        specificationListModel.setName(specificationListModels.get(i).getName());
                        specificationListModel.setDescription(specificationListModels.get(i).getDescription());
                        specificationListModel.setCategory(specificationListModels.get(i).getCategory());
                        specificationListModel.setPropertyBedrooms(specificationListModels.get(i).getPropertyBedrooms());
                        specification_array.add(specificationListModel);

                    }
                }

            }

            for (int i = 0; i < specificationListModels.size(); i++) {

                if (specificationListModels.get(i).getCategory().equals("Furniture")) {
                    if (!specificationListModels.get(i).getName().equals("")) {

                        SpecificationListModel specificationListModel = new SpecificationListModel();
                        specificationListModel.setName(specificationListModels.get(i).getName());
                        specificationListModel.setDescription(specificationListModels.get(i).getDescription());
                        specificationListModel.setCategory(specificationListModels.get(i).getCategory());
                        specificationListModel.setPropertyBedrooms(specificationListModels.get(i).getPropertyBedrooms());
                        furniture_array.add(specificationListModel);

                    }
                }

            }

            for (int i = 0; i < specificationListModels.size(); i++) {


                if (specificationListModels.get(i).getCategory().equals("Softfurniture")) {
                    if (!specificationListModels.get(i).getName().equals("")) {

                        SpecificationListModel specificationListModel = new SpecificationListModel();
                        specificationListModel.setName(specificationListModels.get(i).getName());
                        specificationListModel.setDescription(specificationListModels.get(i).getDescription());
                        specificationListModel.setCategory(specificationListModels.get(i).getCategory());
                        specificationListModel.setPropertyBedrooms(specificationListModels.get(i).getPropertyBedrooms());
                        soft_furnishing_array.add(specificationListModel);

                    }
                }

            }

            for (int i = 0; i < specificationListModels.size(); i++) {

                if (specificationListModels.get(i).getCategory().equals("Whitegood")) {
                    if (!specificationListModels.get(i).getName().equals("")) {

                        SpecificationListModel specificationListModel = new SpecificationListModel();
                        specificationListModel.setName(specificationListModels.get(i).getName());
                        specificationListModel.setDescription(specificationListModels.get(i).getDescription());
                        specificationListModel.setCategory(specificationListModels.get(i).getCategory());
                        specificationListModel.setPropertyBedrooms(specificationListModels.get(i).getPropertyBedrooms());
                        whitegoods_array.add(specificationListModel);

                    }
                }
            }

        }
*/

    }

    private void getBedroomList() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_BEDROOM_ALL + pid + "/0" + "/room",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_BEDROOM_ALL,
                Constants.GET).execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_BEDROOM_ALL)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {

                                linear_layout.setEnabled(false);

                                openDialogBox();


//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                            room_list.clear();
                            linear_layout.setEnabled(true);
//                          addRoom();

                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();

                                propertyBedroomModel.setId(jsonObject1.getString("Id"));
                                propertyBedroomModel.setFloor(jsonObject1.getString("FloorNo"));
                                propertyBedroomModel.setRoom_name(jsonObject1.getString("BedRoomName"));
                                propertyBedroomModel.setGoundtype(jsonObject1.getString("GroundType"));
                                propertyBedroomModel.setRoom_type(jsonObject1.getString("BedRoomType"));
                                propertyBedroomModel.setRoom_size(jsonObject1.getString("BedRoomSize"));
                                propertyBedroomModel.setLength(jsonObject1.getString("Length"));
                                propertyBedroomModel.setWidth(jsonObject1.getString("Width"));
                                propertyBedroomModel.setLengthWidthUnitId(jsonObject1.getString("LengthWidthUnitId"));
                                propertyBedroomModel.setCategory(jsonObject1.getString("Category"));
                                room_list.add(propertyBedroomModel);

                                room_name.add(room_list.get(index).getRoom_name());

                            }

                            setSpecificationAdapter();
                            setFurnitureAdapter();
                            setSoftFurnitureAdapter();
                            setwhiteGoodsAdapter();



                            spinnerAdapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
                            spinnerAdapter.addAll(room_name);
                            attched_room_balcony.setAdapter(spinnerAdapter);
                            attched_room_balcony.setSelection(spinnerAdapter.getCount());
                            attached_room.setAdapter(spinnerAdapter);
                            attached_room.setSelection(spinnerAdapter.getCount());
                            attched_room_balcony_1.setAdapter(spinnerAdapter);
                            attched_room_balcony_1.setSelection(spinnerAdapter.getCount());

                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            // String pid = jsonObject2.getString("Id");

                            JSONArray propJsonArray = jsonObject2.getJSONArray("PropertySpecifications");

                            String message = jsonObject1.getString("Message");


                            Toast.makeText(getActivity(), "Inventory updated successfully", Toast.LENGTH_SHORT).show();

                            SM.sendData(pid, 4);


                        } else {
                          //  SM.sendData(pid, 4);

                           Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.URL_PROPERTY_POST_DRAFT)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");

                            JSONArray propJsonArray = jsonObject2.getJSONArray("PropertySpecifications");

                            String message = jsonObject1.getString("Message");


                            Utils.showSuccessErrorMessage("Success", "Draft added successfully", "Ok", getActivity());

                            //  SM.sendData(pid, 4);


                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private void openDialogBox() {

        new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setMessage(getString(R.string.add_check_over_specification))
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        SM.sendData(pid, 2);
                        dialog.dismiss();
                    }
                })

                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        SM.sendData(pid, 4);

                        dialog.dismiss();

                    }
                })
                .show();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
            ((PropertyItemActivity) getActivity()).save_draft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addDraftSpecifications();
                }
            });
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    private void addSpecifications() {

        try {

            // location frag data
            jsonSpecification = new JSONObject();
            // jsonSpecification.put("Id", pid);
            jsonSpecification.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("IsDraft", 0);

            if (!edit_property.equals("")) {

                jsonSpecification.put("Id", Editid);

            } else {

                jsonSpecification.put("Id", pid);

            }



            for (int i = 0; i < specificationListModels.size(); i++) {
                JSONArray specRoomArray = new JSONArray();

                JSONObject jsonSpecificObject = new JSONObject();
                jsonSpecificObject.put("Id", specificationListModels.get(i).getId());
                jsonSpecificObject.put("Name", specificationListModels.get(i).getName());
                jsonSpecificObject.put("Description", specificationListModels.get(i).getDescription());
                jsonSpecificObject.put("Category", specificationListModels.get(i).getCategory());
                jsonSpecificObject.put("Capacity", specificationListModels.get(i).getCapacity());
                jsonSpecificObject.put("IsDeletable", false);
                jsonSpecificObject.put("IsModify",true);

                for (int j=0; j<specificationListModels.get(i).getPropertyBedrooms().size(); j++ ){
                    JSONObject jsonObjectSpeci = new JSONObject();

                    jsonObjectSpeci.put("Id", specificationListModels.get(i).getPropertyBedrooms().get(j).getId());

                    specRoomArray.put(jsonObjectSpeci);
                }

                jsonSpecificObject.put("PropertyBedrooms",specRoomArray);

                jsonSpecific.put(jsonSpecificObject);
            }

            for (int i = 0; i < furnitureListModels.size(); i++) {
                JSONArray specRoomArray = new JSONArray();

                JSONObject jsonSpecificObject = new JSONObject();
                jsonSpecificObject.put("Id", furnitureListModels.get(i).getId());
                jsonSpecificObject.put("Name", furnitureListModels.get(i).getName());
                jsonSpecificObject.put("Description", furnitureListModels.get(i).getDescription());
                jsonSpecificObject.put("Category", furnitureListModels.get(i).getCategory());
                jsonSpecificObject.put("Capacity", furnitureListModels.get(i).getCapacity());
                jsonSpecificObject.put("IsDeletable", false);
                jsonSpecificObject.put("IsModify",true);

                for (int j=0; j<furnitureListModels.get(i).getPropertyBedrooms().size(); j++ ){
                    JSONObject jsonObjectSpeci = new JSONObject();

                    jsonObjectSpeci.put("Id", furnitureListModels.get(i).getPropertyBedrooms().get(j).getId());

                    specRoomArray.put(jsonObjectSpeci);
                }

                jsonSpecificObject.put("PropertyBedrooms",specRoomArray);

                jsonSpecific.put(jsonSpecificObject);
            }

            for (int i = 0; i < softfurnitureListModels.size(); i++) {
                JSONArray specRoomArray = new JSONArray();

                JSONObject jsonSpecificObject = new JSONObject();
                jsonSpecificObject.put("Id", softfurnitureListModels.get(i).getId());
                jsonSpecificObject.put("Name", softfurnitureListModels.get(i).getName());
                jsonSpecificObject.put("Description", softfurnitureListModels.get(i).getDescription());
                jsonSpecificObject.put("Category", softfurnitureListModels.get(i).getCategory());
                jsonSpecificObject.put("Capacity", softfurnitureListModels.get(i).getCapacity());
                jsonSpecificObject.put("IsDeletable", false);
                jsonSpecificObject.put("IsModify",true);

                for (int j=0; j<softfurnitureListModels.get(i).getPropertyBedrooms().size(); j++ ){
                    JSONObject jsonObjectSpeci = new JSONObject();

                    jsonObjectSpeci.put("Id", softfurnitureListModels.get(i).getPropertyBedrooms().get(j).getId());

                    specRoomArray.put(jsonObjectSpeci);
                }

                jsonSpecificObject.put("PropertyBedrooms",specRoomArray);

                jsonSpecific.put(jsonSpecificObject);
            }

            for (int i = 0; i < whiteGoodsListModels.size(); i++) {
                JSONArray specRoomArray = new JSONArray();

                JSONObject jsonSpecificObject = new JSONObject();
                jsonSpecificObject.put("Id", whiteGoodsListModels.get(i).getId());
                jsonSpecificObject.put("Name", whiteGoodsListModels.get(i).getName());
                jsonSpecificObject.put("Description", whiteGoodsListModels.get(i).getDescription());
                jsonSpecificObject.put("Category", whiteGoodsListModels.get(i).getCategory());
                jsonSpecificObject.put("Capacity", whiteGoodsListModels.get(i).getCapacity());
                jsonSpecificObject.put("IsDeletable", false);
                jsonSpecificObject.put("IsModify",true);

                for (int j=0; j<whiteGoodsListModels.get(i).getPropertyBedrooms().size(); j++ ){
                    JSONObject jsonObjectSpeci = new JSONObject();

                    jsonObjectSpeci.put("Id", whiteGoodsListModels.get(i).getPropertyBedrooms().get(j).getId());

                    specRoomArray.put(jsonObjectSpeci);
                }

                jsonSpecificObject.put("PropertyBedrooms",specRoomArray);

                jsonSpecific.put(jsonSpecificObject);
            }

            jsonSpecification.put("PropertySpecifications", jsonSpecific);

            postProperty();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addDraftSpecifications() {

        try {

            // location frag data
            jsonSpecification = new JSONObject();

            jsonSpecification.put("Id", pid);

            jsonSpecification.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("IsDraft", 1);


            jsonSpecification.put("PropertySpecifications", jsonSpecific);

            postProperty_draft();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void postProperty_draft() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Constants.URL_PROPERTY_POST_DRAFT,
                Constants.POST).execute();

    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(getActivity(), MyPropertyActivity.class);
            intent.putExtra("from_savedrafts", "from_savedrafts");

            startActivity(intent);
        }
    }

    private void postProperty() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_POST,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = jsonSpecification.toString();
        } catch (Exception e) {

        }

        return inputStr;
    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }
}
