package com.bnhabitat.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.AccomDetailModel;
import com.bnhabitat.models.CapturePlcModel;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.FeatureModel;
import com.bnhabitat.models.ImageModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.InventoryModelProp;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.models.PicsModel;
import com.bnhabitat.models.PropAreaModel;
import com.bnhabitat.models.PropBedroomModel;
import com.bnhabitat.models.PropertyAreasModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.models.Result_;
import com.bnhabitat.township.model.PlotLandLot;
import com.bnhabitat.ui.activities.MyPropertyActivity;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.adapters.BuildUpAreaAdapter;
import com.bnhabitat.ui.adapters.CapturePlcAddAdapter;
import com.bnhabitat.ui.adapters.FeatureAddAdapter;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utility;
import com.bnhabitat.utils.Utils;

import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

import static com.bnhabitat.ui.fragments.LocationFragment.inventoryModelProp;

public class DetailFragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener {
    View view, image_view_lay, other_side_view;
    private String blockCharacterSet = "~#^|$%&/*!'";
    int iskeyboaropen = 0;

    String userChoosenTask;
    SpinnerAdapter adapter, adapter1;
    LinearLayout rectangle, assymetric, upload_photos_detail, other_side_lay, plc_lay, image_view_infate, built_up_lay, built_area_super_lay, plot_lay, plot_shape_lay, front_lay, remove_other_layout, plotArea;
    ImageView next_btn, asmetric_off_on, rectangle_off_on, remove_others;
    EditText plot_area, front, facing_street_road, facing_street_road_width, other_side, depth, super_area, carpet, built_up;
    Spinner plot_area_select, super_area_select, built_up_select, front_select, carpet_select, facing_street_road_select, other_side_select;
    CheckBox capture_plc_checked, cornor, front_park, parkon_the_left, back_open, raod_onright_side, road_onfront_side, radi_behind_park, park_on_right_side, front_open, road_on_left_side, road_on_back_side, other;
    LinearLayout add_other_side;
    RecyclerView area_detail_list;
    RadioButton radio_east, radio_north, radio_north_west, radio_south_east, radio_north_east, radio_west, south, south_west, complete_multi, complete_single, independent_flor, triplex;
    ArrayList<PropertyUnitsModel> propertyUnitsModels;
    ArrayList<PropertyUnitsModel> propertyUnitsModels_length;
    ArrayList<PhotosModel> photosModels = new ArrayList<>();
    ArrayList<InventoryModel.PropertyImage> propertyImages = new ArrayList<>();
    ArrayList<String> strings = new ArrayList<>();
    int number_bed_count = 0, position = 0;

    ArrayList<String> plcModels = new ArrayList<>();
    String plotArea_txt, front_areatxt, otherArea_txt, super_area_ext, built_up_text, carpet_text, facing_street_road_txt, plotArea_id = "", front_area_id, other_area_id, facing_street_road_id,
    super_area_id, built_up_area_id, carpet_area_unit_id, depth_areatxt, depth_area_id, enterence_radio_txt, plot_shape_txt = "rectangleorsquare";
    ArrayList<String> property_units = new ArrayList<>();
    ArrayList<String> property_units_length = new ArrayList<>();
    JSONObject jsonObject1;

    ArrayList<String> areaList = new ArrayList<>();
    BuildUpAreaAdapter buildUpAreaAdapter;
    LinearLayoutManager area_listing;
    Dialog  dialog_feature;


    String encodedImageData, built, PropertyId, edit_property;
    SendMessage SM;
    private static int RESULT_LOAD_IMAGE = 1;
    private static int REQUEST_CAMERA = 101;
    private static int SELECT_FILE = 102;
    private static final int REQUEST_CAMERA_PERMISSION = 100;
    int id = 0;
    ArrayList<InventoryModel> json_Data;
    ArrayList<PropertyAreasModel> propArrayList = new ArrayList<>();
   // public static PropAreaModel propAreaModel = new PropAreaModel();
    public static PropertyAreasModel propAreaModel = new PropertyAreasModel();

    public static ArrayList<PicsModel> PropPics = new ArrayList<>();
    public static ArrayList<ImageModel> imgModel = new ArrayList<>();

    public static AccomDetailModel accomDetailModel;
    public static PropBedroomModel propBedroomModel;

    public static PicsModel picsModel = new PicsModel();
    public ImageModel imageModel = new ImageModel();
    String pid = "";
    JSONObject locationObj;
    String PropertyTypeId, Editid;

    LinearLayout linear_layout;
    NestedScrollView scroll_view;
    int numberOfColumns_feature = 2;

    RecyclerView rv_capture_plc;
    LinearLayout ll_addmore_capture;
    CapturePlcAddAdapter capturePlcAddAdapter;
    ArrayList<CapturePlcModel> capturePlcList = new ArrayList<>();
    private Context applicationContext;
    Button add_feature;
    EditText edit_feature_txt;
    ImageView iv_close_feature;

    public DetailFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_detail, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        onIntializeView(view);
        onClicks();
        blockChar();

     //   radio_east.setChecked(true);
        //othersideinflateview();
        pid = LocationFragment.pid;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            try {

                propertyUnitsModels = (ArrayList<PropertyUnitsModel>) bundle.getSerializable("propertyunits");
                propertyUnitsModels_length = (ArrayList<PropertyUnitsModel>) bundle.getSerializable("propertyunits_length");
                for (int i = 0; i < propertyUnitsModels.size(); i++) {
                    property_units.add(propertyUnitsModels.get(i).getSizeUnitId());
                }
                for (int i = 0; i < propertyUnitsModels_length.size(); i++) {
                    property_units_length.add(propertyUnitsModels_length.get(i).getSizeUnitId());
                }

                adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
                adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
                adapter.addAll(property_units);
                adapter1.addAll(property_units_length);
                adapter.add(getString(R.string.select_type));
                adapter1.add(getString(R.string.select_type));
                plot_area_select.setAdapter(adapter);
                facing_street_road_select.setAdapter(adapter);
                super_area_select.setAdapter(adapter);
                carpet_select.setAdapter(adapter);
                built_up_select.setAdapter(adapter);
                front_select.setAdapter(adapter1);
                try {
                    // jsonObject1 = new JSONObject(bundle.getString("jsonObject"));
                    PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");

                } catch (Exception e) {

                    e.printStackTrace();
                }
                try {
                    pid = LocationFragment.pid;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(built!=null) {

                    built = bundle.getString("built");

                    if (built.equalsIgnoreCase("")) {
                        built_up_lay.setVisibility(View.GONE);
                    } else {

                        built_up_lay.setVisibility(View.VISIBLE);
                        complete_single.setChecked(true);
                        if (complete_single.isChecked()) {
                            built_area_super_lay.setVisibility(View.VISIBLE);
                        }
                    }
                }

                try {

                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.MULTI_STOREY, "single_storey");
                    edit_property = bundle.getString("edit") == null ? "" : bundle.getString("edit");
                    if (!edit_property.equals("")) {
                        Editid = bundle.getString("editId");
                        json_Data = (ArrayList<InventoryModel>) bundle.getSerializable("json_Data");
                        PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");

                        for (int i = 0; i < json_Data.size(); i++) {

                            if (json_Data.get(i).getId().equalsIgnoreCase(Editid)) {
                                if (json_Data.get(i).getPropertyAreas() != null) {

                                    if (json_Data.get(i).getPropertyAreas().size() > 0 && json_Data.get(i).getPropertyAreas() != null) {

                                        if (Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getPlotShape()).equalsIgnoreCase("rectangleorsquare")) {

                                            complete_single.setChecked(true);
                                            asmetric_off_on.setImageDrawable(getResources().getDrawable(R.drawable.assymetric_off));
                                            rectangle_off_on.setImageDrawable(getResources().getDrawable(R.drawable.rectangle_shape));
                                            other_side_lay.setVisibility(View.GONE);
                                            add_other_side.setVisibility(View.GONE);
                                            plotArea.setVisibility(View.VISIBLE);


                                            area_detail_list.setVisibility(View.GONE);

                                        } else {

                                            complete_single.setChecked(true);
                                            asmetric_off_on.setImageDrawable(getResources().getDrawable(R.drawable.assymetric_shape));
                                            rectangle_off_on.setImageDrawable(getResources().getDrawable(R.drawable.rectangle_off));
                                            other_side_lay.setVisibility(View.GONE);
                                            add_other_side.setVisibility(View.GONE);
                                            plotArea.setVisibility(View.VISIBLE);
                                            area_detail_list.setVisibility(View.VISIBLE);

                                        }

                                    }

                                    front.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getFrontSize()));
                                    depth.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getDepthSize()));
                                    plot_area.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getPlotArea()));
                                    super_area.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getSuperArea()));
                                    built_up.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getBuiltUpArea()));
                                    facing_street_road.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getStreetOrRoadType()));
                                    facing_street_road_width.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getRoadWidth()));
                                    carpet.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyAreas().get(0).getCarpetArea()));

                                    if ( json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing()!=null){

                                        try{
                                            if (  json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing().equalsIgnoreCase("East")){

                                                radio_east.setChecked(true);
                                                radio_north_east.setChecked(false);
                                                radio_north.setChecked(false);
                                                radio_west.setChecked(false);
                                                radio_north_west.setChecked(false);
                                                south.setChecked(false);
                                                radio_south_east.setChecked(false);
                                                south_west.setChecked(false);

                                               }
                                            else if (  json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing().equalsIgnoreCase("North East")){
                                                radio_east.setChecked(false);
                                                radio_north_east.setChecked(true);
                                                radio_north.setChecked(false);
                                                radio_west.setChecked(false);
                                                radio_north_west.setChecked(false);
                                                south.setChecked(false);
                                                radio_south_east.setChecked(false);
                                                south_west.setChecked(false);
                                            }

                                            else if (  json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing().equalsIgnoreCase("North")){
                                                radio_east.setChecked(false);
                                                radio_north_east.setChecked(false);
                                                radio_north.setChecked(true);
                                                radio_west.setChecked(false);
                                                radio_north_west.setChecked(false);
                                                south.setChecked(false);
                                                radio_south_east.setChecked(false);
                                                south_west.setChecked(false);

                                            }

                                            else if (  json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing().equalsIgnoreCase("West")){
                                                radio_east.setChecked(false);
                                                radio_north_east.setChecked(false);
                                                radio_north.setChecked(false);
                                                radio_west.setChecked(true);
                                                radio_north_west.setChecked(false);
                                                south.setChecked(false);
                                                radio_south_east.setChecked(false);
                                                south_west.setChecked(false);
                                            }

                                            else if (  json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing().equalsIgnoreCase("North West")){
                                                radio_east.setChecked(false);
                                                radio_north_east.setChecked(false);
                                                radio_north.setChecked(false);
                                                radio_west.setChecked(false);
                                                radio_north_west.setChecked(true);
                                                south.setChecked(false);
                                                radio_south_east.setChecked(false);
                                                south_west.setChecked(false);
                                            }

                                            else if (  json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing().equalsIgnoreCase("South")){

                                                radio_east.setChecked(false);
                                                radio_north_east.setChecked(false);
                                                radio_north.setChecked(false);
                                                radio_west.setChecked(false);
                                                radio_north_west.setChecked(false);
                                                south.setChecked(true);
                                                radio_south_east.setChecked(false);
                                                south_west.setChecked(false);

                                            }

                                            else if (  json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing().equalsIgnoreCase("South East")){
                                                radio_east.setChecked(false);
                                                radio_north_east.setChecked(false);
                                                radio_north.setChecked(false);
                                                radio_west.setChecked(false);
                                                radio_north_west.setChecked(false);
                                                south.setChecked(false);
                                                radio_south_east.setChecked(true);
                                                south_west.setChecked(false);
                                            }

                                            else if (  json_Data.get(i).getPropertyAreas().get(0).getEnteranceDoorFacing().equalsIgnoreCase("South West")){
                                                radio_east.setChecked(false);
                                                radio_north_east.setChecked(false);
                                                radio_north.setChecked(false);
                                                radio_west.setChecked(false);
                                                radio_north_west.setChecked(false);
                                                south.setChecked(false);
                                                radio_south_east.setChecked(false);
                                                south_west.setChecked(true);
                                            }


                                        }catch (Exception e){

                                            e.printStackTrace();

                                        }

                                    }

                                  for(int l=0;l<propertyUnitsModels_length.size();l++){

                                       if (Integer.parseInt(propertyUnitsModels_length.get(l).getId())==json_Data.get(i).getPropertyAreas().get(0).getFrontSizeUnitId()){

                                          front_select.setSelection(l);

                                       }
                                    }

                                    for(int l=0;l<propertyUnitsModels.size();l++){

                                        if (Integer.parseInt(propertyUnitsModels.get(l).getId())==json_Data.get(i).getPropertyAreas().get(0).getPlotAreaUnitId()){

                                            plot_area_select.setSelection(l);

                                        }
                                    }

                                    for(int l=0;l<propertyUnitsModels.size();l++){

                                        if (Integer.parseInt(propertyUnitsModels.get(l).getId())==json_Data.get(i).getPropertyAreas().get(0).getRoadWidthUnitId()){

                                            facing_street_road_select.setSelection(l);

                                        }
                                    }


                                 /*   String currentId1 = (propertyUnitsModels.get(json_Data.get(i).getPropertyAreas().get(0).getPlotAreaUnitId()-1).getId());

                                    plot_area_select.setSelection(Integer.parseInt(currentId1)-1);

                                    String currentId2 = (propertyUnitsModels.get(json_Data.get(i).getPropertyAreas().get(0).getRoadWidthUnitId()-1).getId());

                                    facing_street_road_select.setSelection(Integer.parseInt(currentId2)-1);

*/
/*



                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
*/


                                    if (json_Data.get(i).getPropertyImages().size()> 0){

                                        image_view_infate.setVisibility(View.VISIBLE);

                                        for (int index = 0; index < json_Data.get(i).getPropertyImages().size(); index++) {
//
//                                         JSONObject jsonObject1 = json_Data.get(i).getPropertyImages().getJSONObject(index);

                                            InventoryModel.PropertyImage propertyImage = new InventoryModel.PropertyImage();

                                            propertyImage.setId(json_Data.get(i).getPropertyImages().get(index).getId());
                                            propertyImage.setName(json_Data.get(i).getPropertyImages().get(index).getName());
                                            propertyImage.setUrl(json_Data.get(i).getPropertyImages().get(index).getUrl());
                                            propertyImages.add(propertyImage);

                                        }

                                        imageinflateview(propertyImages);

                                    }

                                    if (json_Data.get(i).getPropertyPlcs().size()> 0){

                                        capture_plc_checked.setChecked(true);
                                        plc_lay.setVisibility(View.VISIBLE);
                                        capturePlcList.clear();

                                        for (int k = 0; k < json_Data.get(i).getPropertyPlcs().size(); k++){

                                            CapturePlcModel capturePlcModel = new CapturePlcModel();
                                            capturePlcModel.setId(json_Data.get(i).getPropertyPlcs().get(k).getId());
                                            capturePlcModel.setName(json_Data.get(i).getPropertyPlcs().get(k).getName());
                                            capturePlcModel.setChecked(json_Data.get(i).getPropertyPlcs().get(k).isChecked());

                                            capturePlcList.add(capturePlcModel);

                                        }
                                        setCapturePlcAdapter();

/*
                                        for (int k = 0; k < json_Data.get(i).getPropertyPlcs().size(); k++) {

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Corner")) {

                                                cornor.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Front Park")) {

                                                front_park.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Park on the left")) {

                                                parkon_the_left.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Back Open")) {

                                                back_open.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Road on right side")) {

                                                raod_onright_side.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Road on front side")) {

                                                road_onfront_side.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Behind Park")) {

                                                radi_behind_park.setChecked(true);
                                            }


                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Park on the right side")) {

                                                park_on_right_side.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Front open")) {

                                                front_open.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Road on left side")) {

                                                road_on_left_side.setChecked(true);
                                            }

                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Road on Back side")) {

                                                road_on_back_side.setChecked(true);
                                            }


                                            if (json_Data.get(i).getPropertyPlcs().get(k).getName().equalsIgnoreCase("Other")) {

                                                other.setChecked(true);
                                            }

                                        }
*/

                                    }

                                    else {
                                        capture_plc_checked.setChecked(false);
                                        plc_lay.setVisibility(View.GONE);
                                        addDataToCapturePlcList();
                                    }


/*
                                    if (json_Data.get(i).getPropertyAreas().size() > 0) {

                                        for (int k = 0; k < propertyUnitsModels_length.size(); k++) {

                                            if (json_Data.get(i).getPropertyAreas().get(0).getFrontSizeUnitId() == (propertyUnitsModels_length.get(k).getId())) {

                                                front_select.setSelection(k);

                                            }

                                        }

                                        for (int k = 0; k < propertyUnitsModels.size(); k++) {

                                            if (json_Data.get(i).getPropertyAreas().get(0).getPlotAreaUnitId().equalsIgnoreCase(propertyUnitsModels.get(k).getId())) {

                                                plot_area_select.setSelection(k);

                                            }
                                            if (json_Data.get(i).getPropertyAreas().get(0).getSuperAreaUnitId().equalsIgnoreCase(propertyUnitsModels.get(k).getId())) {

                                                super_area_select.setSelection(k);

                                            }
                                            if (json_Data.get(i).getPropertyAreas().get(0).getBuiltUpAreaUnitId().equalsIgnoreCase(propertyUnitsModels.get(k).getId())) {

                                                built_up_select.setSelection(k);

                                            }
                                            if (json_Data.get(i).getPropertyAreas().get(0).getCarpetAreaUnitId().equalsIgnoreCase(propertyUnitsModels.get(k).getId())) {

                                                carpet_select.setSelection(k);

                                            }
                                            if (json_Data.get(i).getPropertyAreas().get(0).getRoadWidthUnitId().equalsIgnoreCase(propertyUnitsModels.get(k).getId())) {

                                                facing_street_road_select.setSelection(k);

                                            }
                                        }


                                    }
*/

                                }


                            }

                        }

                    } else {

                        addDataToCapturePlcList();

                        plot_area_select.setSelection(adapter.getCount());

                        front_select.setSelection(adapter1.getCount());
                        //other_side_select.setAdapter(adapter);
                        // other_side_select.setSelection(adapter.getCount());

                        facing_street_road_select.setSelection(adapter.getCount());

                        super_area_select.setSelection(adapter.getCount());

                        carpet_select.setSelection(adapter.getCount());

                        built_up_select.setSelection(adapter.getCount());

                        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.MULTI_STOREY, "single_storey");

                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();

            }


            area_listing = new LinearLayoutManager(getActivity());

            PropertyId = bundle.getString("PropertyId") == null ? "" : bundle.getString("PropertyId");

            Log.e("propertyUnitsModels", "propertyUnitsModels" + propertyUnitsModels.size());
        }

       // areaList.add("");
        buildUpAreaAdapter = new BuildUpAreaAdapter(getActivity(), areaList, property_units);
        area_detail_list.setVisibility(View.VISIBLE);
        area_detail_list.setAdapter(buildUpAreaAdapter);

        try {
            if (PropertyTypeId.equals("3")) {
                plot_lay.setVisibility(View.GONE);
                plot_shape_lay.setVisibility(View.GONE);
                front_lay.setVisibility(View.GONE);


            } else {
                plot_lay.setVisibility(View.VISIBLE);
                plot_shape_lay.setVisibility(View.VISIBLE);
                front_lay.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

        }

/*
        linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(iskeyboaropen==1) {
                    hideSoftKeyboard(getActivity());
                    iskeyboaropen=0;
                }
            }
        });
*/

        linear_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = linear_layout.getRootView().getHeight() - linear_layout.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");

                    iskeyboaropen=1;
                } else {
                    Log.e("MyActivity", "keyboard closed");
                    iskeyboaropen=0;
                }
            }
        });

/*
        scroll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               hideSoftKeyboard(getActivity());
            }
        });
*/
        ll_addmore_capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number_bed_count = number_bed_count + 1;
                add_capture();

            }
        });

        assymetric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                complete_single.setChecked(true);
                plot_shape_txt = "asymmetric";
                asmetric_off_on.setImageDrawable(getResources().getDrawable(R.drawable.assymetric_shape));
                rectangle_off_on.setImageDrawable(getResources().getDrawable(R.drawable.rectangle_off));
                other_side_lay.setVisibility(View.VISIBLE);
                add_other_side.setVisibility(View.VISIBLE);
                plotArea.setVisibility(View.GONE);
                area_detail_list.setVisibility(View.VISIBLE);
            }
        });

        rectangle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                complete_single.setChecked(true);
                plot_shape_txt = "rectangleorsquare";
                asmetric_off_on.setImageDrawable(getResources().getDrawable(R.drawable.assymetric_off));
                rectangle_off_on.setImageDrawable(getResources().getDrawable(R.drawable.rectangle_shape));
                other_side_lay.setVisibility(View.GONE);
                add_other_side.setVisibility(View.GONE);
                plotArea.setVisibility(View.VISIBLE);
                area_detail_list.setVisibility(View.GONE);
            }
        });
        capture_plc_checked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    plc_lay.setVisibility(View.VISIBLE);
                } else {
                    plc_lay.setVisibility(View.GONE);
                }
            }
        });

        plot_area_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (plot_area_select.getSelectedItem() == getString(R.string.select_type)) {
                    plot_area_select.setSelection(1);
                    //Do nothing.
                } else {
//                  int choose = spin_type.getSelectedItemPosition();
                    plotArea_txt = plot_area_select.getSelectedItem().toString();
                    plotArea_id = propertyUnitsModels.get(position).getId();
                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.PLOT_UNIT_AREA, plotArea_txt);
//
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        add_other_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //othersideinflateview1();
                //other_side_select.setAdapter(adapter);
                //other_side_select.setSelection(adapter.getCount());
                area_detail_list.setLayoutManager(area_listing);
                areaList.add("");
                buildUpAreaAdapter.notifyDataSetChanged();
            }
        });

/*
        other_side_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (other_side_select.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//                    int choose = spin_type.getSelectedItemPosition();
                    otherArea_txt = other_side_select.getSelectedItem().toString();
                    other_area_id = propertyUnitsModels.get(position).getId();
//                    support_id = getsupportTypeModels.get(position).get_id();
                    Log.e("chooseText", "chooseText" + otherArea_txt.toLowerCase());
//                    Toast.makeText(HelpSupportActivity.this, spin_type.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
*/

        facing_street_road_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (facing_street_road_select.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//                    int choose = spin_type.getSelectedItemPosition();
                    facing_street_road_txt = facing_street_road_select.getSelectedItem().toString();
                    facing_street_road_id = propertyUnitsModels.get(position).getId();
//                    support_id = getsupportTypeModels.get(position).get_id();
                    Log.e("chooseText", "chooseText" + facing_street_road_txt.toLowerCase());
//                    Toast.makeText(HelpSupportActivity.this, spin_type.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        super_area_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (super_area_select.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//                    int choose = spin_type.getSelectedItemPosition();
                    super_area_ext = super_area_select.getSelectedItem().toString();
                    super_area_id = propertyUnitsModels.get(position).getId();
//                    support_id = getsupportTypeModels.get(position).get_id();
//                    Toast.makeText(HelpSupportActivity.this, spin_type.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        built_up_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (built_up_select.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//                    int choose = spin_type.getSelectedItemPosition();
                    built_up_text = built_up_select.getSelectedItem().toString();
                    built_up_area_id = propertyUnitsModels.get(position).getId();
//                    support_id = getsupportTypeModels.get(position).get_id();
//                    Toast.makeText(HelpSupportActivity.this, spin_type.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        carpet_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (carpet_select.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//                    int choose = spin_type.getSelectedItemPosition();
                    carpet_text = carpet_select.getSelectedItem().toString();
                    carpet_area_unit_id = propertyUnitsModels.get(position).getId();
//                  support_id = getsupportTypeModels.get(position).get_id();
//                  Toast.makeText(HelpSupportActivity.this, spin_type.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        front_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (front_select.getSelectedItem() == getString(R.string.select_type)) {
                    front_select.setSelection(0);
                    //Do nothing.
                } else {
//                    int choose = spin_type.getSelectedItemPosition();
                    front_areatxt = front_select.getSelectedItem().toString();
                    front_area_id = propertyUnitsModels_length.get(position).getId();
//                    support_id = getsupportTypeModels.get(position).get_id();
                    Log.e("chooseText", "chooseText" + front_areatxt.toLowerCase());
//                    Toast.makeText(HelpSupportActivity.this, spin_type.getSelectedItem().toString(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        front.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!front.getText().toString().isEmpty() ? front.getText().toString() : "0");
                Double v2 = Double.parseDouble(!depth.getText().toString().isEmpty() ?
                        depth.getText().toString() : "0");
                Double value = v1 * v2;
                plot_area.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        depth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!front.getText().toString().isEmpty() ? front.getText().toString() : "0");
                Double v2 = Double.parseDouble(!depth.getText().toString().isEmpty() ?
                        depth.getText().toString() : "0");
                Double value = v1 * v2;
                plot_area.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        depth_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                // TODO Auto-generated method stub
//
//                if (depth_select.getSelectedItem() == getString(R.string.select_type)) {
//
//                    //Do nothing.
//                } else {
////                    int choose = spin_type.getSelectedItemPosition();
//                    depth_areatxt = depth_select.getSelectedItem().toString();
//                    depth_area_id = propertyUnitsModels.get(position).getId();
////                    support_id = getsupportTypeModels.get(position).get_id();
//                    Log.e("chooseText", "chooseText" + depth_areatxt.toLowerCase());
////                    Toast.makeText(HelpSupportActivity.this, spin_type.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
//
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//
//            }
//        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(plot_area.getText().toString().equals("")&&plotArea_txt==null){
//                    Utils.showErrorMessage("Please enter Plot area and Plot area unit",getActivity());
//                }else {
                if (capture_plc_checked.isChecked()) {
                    checkboxSelected();
                }
                onRadioclick();

                onDetaildata();

            }
        });

        upload_photos_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDrawerPermision();
//              Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//              startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        complete_multi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    built_area_super_lay.setVisibility(View.VISIBLE);
                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.MULTI_STOREY, "multi_storey");

                    complete_single.setChecked(false);
                }
            }
        });


        complete_single.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.MULTI_STOREY, "single_storey");

                    built_area_super_lay.setVisibility(View.VISIBLE);

                    complete_multi.setChecked(false);
                }
            }
        });


        return view;
    }

    private void add_capture() {
        dialog_feature = new Dialog(getActivity());
        dialog_feature.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_feature.setContentView(R.layout.add_more_capture);
        add_feature = (Button) dialog_feature.findViewById(R.id.add_feature);
        edit_feature_txt = (EditText) dialog_feature.findViewById(R.id.edit_feature_txt);
        iv_close_feature = (ImageView) dialog_feature.findViewById(R.id.iv_close);

        add_feature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edit_feature_txt.getText().toString().trim().equalsIgnoreCase("")) {

                    CapturePlcModel featureModel = new CapturePlcModel();
                    featureModel.setName(edit_feature_txt.getText().toString());
                    featureModel.setId("0");
                    featureModel.setChecked(false);

                    capturePlcList.add(featureModel);

                    setCapturePlcAdapter();
                    dialog_feature.cancel();

                } else {

                    Utils.showErrorMessage("Enter your feature name", getActivity());
                }

            }
        });

        iv_close_feature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_feature.cancel();
            }
        });

        dialog_feature.show();
    }


    private void onClicks() {

        radio_east.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_east.isChecked()){

                    radio_east.setChecked(true);
                    radio_north_east.setChecked(false);
                    radio_north.setChecked(false);
                    radio_west.setChecked(false);
                    radio_north_west.setChecked(false);
                    south.setChecked(false);
                    radio_south_east.setChecked(false);
                    south_west.setChecked(false);

                }
            }
        });

        radio_north_east.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_north_east.isChecked()){

                    radio_east.setChecked(false);
                    radio_north_east.setChecked(true);
                    radio_north.setChecked(false);
                    radio_west.setChecked(false);
                    radio_north_west.setChecked(false);
                    south.setChecked(false);
                    radio_south_east.setChecked(false);
                    south_west.setChecked(false);

                }
            }
        });

        radio_north.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_north.isChecked()){

                    radio_east.setChecked(false);
                    radio_north_east.setChecked(false);
                    radio_north.setChecked(true);
                    radio_west.setChecked(false);
                    radio_north_west.setChecked(false);
                    south.setChecked(false);
                    radio_south_east.setChecked(false);
                    south_west.setChecked(false);

                }
            }
        });

        radio_west.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_west.isChecked()){

                    radio_east.setChecked(false);
                    radio_north_east.setChecked(false);
                    radio_north.setChecked(false);
                    radio_west.setChecked(true);
                    radio_north_west.setChecked(false);
                    south.setChecked(false);
                    radio_south_east.setChecked(false);
                    south_west.setChecked(false);

                }
            }
        });

        radio_north_west.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_north_west.isChecked()){

                    radio_east.setChecked(false);
                    radio_north_east.setChecked(false);
                    radio_north.setChecked(false);
                    radio_west.setChecked(false);
                    radio_north_west.setChecked(true);
                    south.setChecked(false);
                    radio_south_east.setChecked(false);
                    south_west.setChecked(false);

                }
            }
        });

        south.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (south.isChecked()){

                    radio_east.setChecked(false);
                    radio_north_east.setChecked(false);
                    radio_north.setChecked(false);
                    radio_west.setChecked(false);
                    radio_north_west.setChecked(false);
                    south.setChecked(true);
                    radio_south_east.setChecked(false);
                    south_west.setChecked(false);

                }
            }
        });

        radio_south_east.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_south_east.isChecked()){

                    radio_east.setChecked(false);
                    radio_north_east.setChecked(false);
                    radio_north.setChecked(false);
                    radio_west.setChecked(false);
                    radio_north_west.setChecked(false);
                    south.setChecked(false);
                    radio_south_east.setChecked(true);
                    south_west.setChecked(false);

                }
            }
        });

        south_west.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (south_west.isChecked()){

                    radio_east.setChecked(false);
                    radio_north_east.setChecked(false);
                    radio_north.setChecked(false);
                    radio_west.setChecked(false);
                    radio_north_west.setChecked(false);
                    south.setChecked(false);
                    radio_south_east.setChecked(false);
                    south_west.setChecked(true);

                }
            }
        });

    }


    private void blockChar() {
        front.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(6)});
        depth.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(6)});
        super_area.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(6)});
        built_up.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(6)});
        carpet.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(6)});
        plot_area.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(6)});
        facing_street_road.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(100)});
        facing_street_road_width.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(6)});
    }

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };


    private void checkDrawerPermision() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        } else {

            selectImage();

        }

    }

    private void onRadioclick() {
        if (radio_east.isChecked()) {
            enterence_radio_txt = radio_east.getText().toString();
        } else if (radio_north_east.isChecked()) {
            enterence_radio_txt = radio_north_east.getText().toString();
        } else if (radio_north.isChecked()) {
            enterence_radio_txt = radio_north.getText().toString();
        } else if (radio_west.isChecked()) {
            enterence_radio_txt = radio_west.getText().toString();
        } else if (radio_north_west.isChecked()) {
            enterence_radio_txt = radio_north_west.getText().toString();
        } else if (south.isChecked()) {
            enterence_radio_txt = south.getText().toString();
        } else if (radio_south_east.isChecked()) {
            enterence_radio_txt = radio_south_east.getText().toString();
        } else if (south_west.isChecked()) {
            enterence_radio_txt = south_west.getText().toString();
        }

    }

    private void onIntializeView(View onDetailview) {
        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);
        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);
        assymetric = (LinearLayout) view.findViewById(R.id.assymetric);
        rectangle = (LinearLayout) view.findViewById(R.id.rectangle);
        upload_photos_detail = (LinearLayout) view.findViewById(R.id.upload_photos_detail);
        other_side_lay = (LinearLayout) view.findViewById(R.id.other_side_lay);
        plc_lay = (LinearLayout) view.findViewById(R.id.plc_lay);
        image_view_infate = (LinearLayout) view.findViewById(R.id.image_view_infate);
        built_up_lay = (LinearLayout) view.findViewById(R.id.built_up_lay);
        built_area_super_lay = (LinearLayout) view.findViewById(R.id.built_area_super_lay);
        plot_shape_lay = (LinearLayout) view.findViewById(R.id.plot_shape_lay);
        front_lay = (LinearLayout) view.findViewById(R.id.front_lay);
        plot_lay = (LinearLayout) view.findViewById(R.id.plot_lay);
        next_btn = (ImageView) view.findViewById(R.id.next_btn);
        plotArea = (LinearLayout) view.findViewById(R.id.plotArea);
        rectangle_off_on = (ImageView) view.findViewById(R.id.rectangle_off_on);
        asmetric_off_on = (ImageView) view.findViewById(R.id.asmetric_off_on);
        add_other_side = (LinearLayout) view.findViewById(R.id.add_other_side);
        area_detail_list = (RecyclerView) view.findViewById(R.id.area_detail_list);
        plot_area = (EditText) view.findViewById(R.id.plot_area);
        front = (EditText) view.findViewById(R.id.front);
        depth = (EditText) view.findViewById(R.id.depth);
        super_area = (EditText) view.findViewById(R.id.super_area);
        carpet = (EditText) view.findViewById(R.id.carpet);
        built_up = (EditText) view.findViewById(R.id.built_up);
        facing_street_road = (EditText) view.findViewById(R.id.facing_street_road);
        rv_capture_plc= (RecyclerView) view.findViewById(R.id.rv_capture_plc);
        ll_addmore_capture = (LinearLayout)view.findViewById(R.id.ll_addmore_capture);
        rv_capture_plc.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns_feature));
        addDataTostring();
        facing_street_road_width = (EditText) view.findViewById(R.id.facing_street_road_width);
        capture_plc_checked = (CheckBox) view.findViewById(R.id.capture_plc_checked);
        plot_area_select = (Spinner) view.findViewById(R.id.plot_area_select);
        front_select = (Spinner) view.findViewById(R.id.front_select);
//      depth_select = (Spinner) view.findViewById(R.id.depth_select);
        carpet_select = (Spinner) view.findViewById(R.id.carpet_select);
        super_area_select = (Spinner) view.findViewById(R.id.super_area_select);
        built_up_select = (Spinner) view.findViewById(R.id.built_up_select);
        facing_street_road_select = (Spinner) view.findViewById(R.id.facing_street_road_select);
        image_view_lay = view.findViewById(R.id.image_view_infate);

        complete_multi = (RadioButton) view.findViewById(R.id.complete_multi);
        complete_single = (RadioButton) view.findViewById(R.id.complete_single);
//      triplex = (RadioButton) view.findViewById(R.id.triplex);
//      independent_flor = (RadioButton) view.findViewById(R.id.independent_flor);
        radio_east = (RadioButton) view.findViewById(R.id.radio_east);
        radio_north = (RadioButton) view.findViewById(R.id.radio_north);
        radio_north_west = (RadioButton) view.findViewById(R.id.radio_north_west);
        radio_south_east = (RadioButton) view.findViewById(R.id.radio_south_east);
        radio_north_east = (RadioButton) view.findViewById(R.id.radio_north_east);
        radio_west = (RadioButton) view.findViewById(R.id.radio_west);
        south = (RadioButton) view.findViewById(R.id.south);
        south_west = (RadioButton) view.findViewById(R.id.south_west);

        cornor = (CheckBox) view.findViewById(R.id.cornor);
        front_open = (CheckBox) view.findViewById(R.id.front_open);
        front_park = (CheckBox) view.findViewById(R.id.front_park);
        parkon_the_left = (CheckBox) view.findViewById(R.id.parkon_the_left);

        back_open = (CheckBox) view.findViewById(R.id.back_open);
        road_on_back_side = (CheckBox) view.findViewById(R.id.road_on_back_side);
        raod_onright_side = (CheckBox) view.findViewById(R.id.raod_onright_side);
        road_onfront_side = (CheckBox) view.findViewById(R.id.road_onfront_side);
        radi_behind_park = (CheckBox) view.findViewById(R.id.radi_behind_park);
        park_on_right_side = (CheckBox) view.findViewById(R.id.park_on_right_side);
        road_on_left_side = (CheckBox) view.findViewById(R.id.road_on_left_side);
        other = (CheckBox) view.findViewById(R.id.other);


    }

    private void addDataTostring() {

        strings.add("Corner");
        strings.add("Behind Park");
        strings.add("Front Park");

        strings.add("Park on the right side");
        strings.add("Park on the left");
        strings.add("Front open");

        strings.add("Back Open");
        strings.add("Road on left side");
        strings.add("Road on right side");

        strings.add("Road on Back side");
        strings.add("Road on front side");

    }

    private void checkboxSelected() {
        if (cornor.isChecked()) {
            plcModels.add(cornor.getText().toString());
        }
        else
            plcModels.remove(cornor.getText().toString());

        if (front_open.isChecked()) {
            plcModels.add(front_open.getText().toString());
        }
        else
        plcModels.remove(front_open.getText().toString());

        if (back_open.isChecked()) {
            plcModels.add(back_open.getText().toString());
        }
        else
        plcModels.remove(back_open.getText().toString());

        if (front_park.isChecked()) {
            plcModels.add(front_park.getText().toString());
        }
        else
        plcModels.remove(front_park.getText().toString());

        if (parkon_the_left.isChecked()) {
            plcModels.add(parkon_the_left.getText().toString());
        }
        else
        plcModels.remove(parkon_the_left.getText().toString());

        if (road_on_back_side.isChecked()) {
            plcModels.add(road_on_back_side.getText().toString());
        }
        else
        plcModels.remove(road_on_back_side.getText().toString());

        if (raod_onright_side.isChecked()) {
            plcModels.add(raod_onright_side.getText().toString());
        }
        else
            plcModels.remove(raod_onright_side.getText().toString());

        if (road_onfront_side.isChecked()) {
            plcModels.add(road_onfront_side.getText().toString());
        }
        else
            plcModels.remove(road_onfront_side.getText().toString());

        if (radi_behind_park.isChecked()) {
            plcModels.add(radi_behind_park.getText().toString());
        }
        else
            plcModels.remove(radi_behind_park.getText().toString());

        if (park_on_right_side.isChecked()) {
            plcModels.add(park_on_right_side.getText().toString());
        }
        else
            plcModels.remove(park_on_right_side.getText().toString());

        if (road_on_left_side.isChecked()) {
            plcModels.add(road_on_left_side.getText().toString());
        }
        else
            plcModels.remove(road_on_left_side.getText().toString());

        if (other.isChecked()) {
            plcModels.add(other.getText().toString());
        }
        else
            plcModels.remove(other.getText().toString());

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
            ((PropertyItemActivity) getActivity()).save_draft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    onDetailDraftdata();

                }
            });

        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void postPropertyImages() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_PHOTOS,
                photoInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_PHOTOS,
                Constants.POST).execute();

    }

    private void deletePropertyImages(String position) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REMOVE_PROPERTY_PHOTOS,
                deletephotoInput(position),
                "Loading..",
                this,
                Urls.URL_REMOVE_PROPERTY_PHOTOS,
                Constants.POST).execute();

    }

    private String deletephotoInput(String id) {
        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject2 = new JSONObject();
        try {
            jsonObject2.put("Id", id);

            jsondeleteArray.put(jsonObject2);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }

    private String photoInput() {
        String inputStr = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject2 = new JSONObject();
        try {
            jsonObject2.put("CompanyId", "1");
            jsonObject2.put("Name", new Date().getTime());
            jsonObject2.put("Type", "png");
            jsonObject2.put("ImageCode", encodedImageData);
            jsonArray.put(jsonObject2);
        } catch (Exception e) {

        }
        inputStr = jsonArray.toString();


        return inputStr;
    }

    public void onDetailDraftdata() {

        try {

            // location frag data
            locationObj = new JSONObject();
            locationObj.put("Id", pid);
            locationObj.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            locationObj.put("CompanyId", Constants.APP_COMPANY_ID);
            locationObj.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            locationObj.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            locationObj.put("IsDraft", 1);

            // detail frag data

            JSONArray jsonDetailArray = new JSONArray();

            JSONObject jsonObjectDet = new JSONObject();
            jsonObjectDet.put("Id", pid);
            jsonObjectDet.put("PlotShape", plot_shape_txt);
            jsonObjectDet.put("PlotArea", plot_area.getText().toString());
            if (plotArea_id == null)
                jsonObjectDet.put("PlotAreaUnitId", "");
            else {
                jsonObjectDet.put("PlotAreaUnitId", plotArea_id);
            }
            jsonObjectDet.put("FrontSize", front.getText().toString());
            if (front_area_id == null)
                jsonObjectDet.put("FrontSizeUnitId", "");
            else {
                jsonObjectDet.put("FrontSizeUnitId", front_area_id);
            }

            jsonObjectDet.put("DepthSize", depth.getText().toString());
            if (depth_area_id == null)
                jsonObjectDet.put("DepthSizeUnitId", "");
            else {
                jsonObjectDet.put("DepthSizeUnitId", depth_area_id);
            }

            jsonObjectDet.put("StreetOrRoadType", facing_street_road.getText().toString());
            jsonObjectDet.put("RoadWidth", facing_street_road_width.getText().toString());
            jsonObjectDet.put("DepthSize", depth.getText().toString());

            if (facing_street_road_id == null)
                jsonObjectDet.put("RoadWidthUnitId", "");
            else {
                jsonObjectDet.put("RoadWidthUnitId", facing_street_road_id);
            }

            jsonObjectDet.put("SuperArea", super_area.getText().toString());
            jsonObjectDet.put("BuiltUpArea", built_up.getText().toString());
            jsonObjectDet.put("CarpetArea", carpet.getText().toString());

            // new param

            if (super_area_id == null)
                jsonObjectDet.put("SuperAreaUnitId", "");
            else {
                jsonObjectDet.put("SuperAreaUnitId", super_area_id);
            }
            if (built_up_area_id == null)
                jsonObjectDet.put("BuiltUpAreaUnitId", "");
            else {
                jsonObjectDet.put("BuiltUpAreaUnitId", built_up_area_id);
            }
            if (carpet_area_unit_id == null)
                jsonObjectDet.put("CarpetAreaUnitId", "");
            else {
                jsonObjectDet.put("CarpetAreaUnitId", carpet_area_unit_id);
            }

            jsonObjectDet.put("EnteranceDoorFacing", enterence_radio_txt);
            jsonObjectDet.put("IsDeletable", false);
            jsonObjectDet.put("IsModify", true);

            jsonDetailArray.put(jsonObjectDet);

            JSONArray imageJsonArray = new JSONArray();
            JSONArray plcJsonArray = new JSONArray();

            if (!edit_property.equals("")) {

                for (int i = 0; i < propertyImages.size(); i++) {
                    JSONObject picJsonobject = new JSONObject();
                    picJsonobject.put("Id", propertyImages.get(i).getId());

                    imageJsonArray.put(picJsonobject);
                }

            } else {

                for (int i = 0; i < photosModels.size(); i++) {
                    JSONObject picJsonobject = new JSONObject();
                    picJsonobject.put("Id", photosModels.get(i).getId());
                    imageJsonArray.put(picJsonobject);
                }

            }


            for (int i = 0; i < plcModels.size(); i++) {
                JSONObject plcJsonobject = new JSONObject();
                plcJsonobject.put("Id", 0);
                plcJsonobject.put("Name", plcModels.get(i));
                plcJsonArray.put(plcJsonobject);
            }


            locationObj.put("PropertyAreas", jsonDetailArray);
            locationObj.put("PropertyImages", imageJsonArray);
            locationObj.put("PropertyPlcs", plcJsonArray);

            postProperty_draft();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onDetaildata() {

        try {

            locationObj = new JSONObject();

            locationObj.put("IsDraft", 0);

            locationObj.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            locationObj.put("CompanyId", Constants.APP_COMPANY_ID);
            locationObj.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            locationObj.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            if (!edit_property.equals("")) {

                locationObj.put("Id", Editid);

            } else {

                locationObj.put("Id", pid);

            }

            // detail frag data

            JSONArray jsonDetailArray = new JSONArray();

            JSONObject jsonObjectDet = new JSONObject();

            jsonObjectDet.put("PlotShape", plot_shape_txt);
            jsonObjectDet.put("PlotArea", plot_area.getText().toString());
            if (plotArea_id == null)
                jsonObjectDet.put("PlotAreaUnitId", "");
            else {
                jsonObjectDet.put("PlotAreaUnitId", plotArea_id);
            }
            jsonObjectDet.put("FrontSize", front.getText().toString());
            if (front_area_id == null)
                jsonObjectDet.put("FrontSizeUnitId", "");
            else {
                jsonObjectDet.put("FrontSizeUnitId", front_area_id);
            }

            jsonObjectDet.put("DepthSize", depth.getText().toString());
            if (depth_area_id == null)
                jsonObjectDet.put("DepthSizeUnitId", "");
            else {
                jsonObjectDet.put("DepthSizeUnitId", depth_area_id);
            }

            jsonObjectDet.put("StreetOrRoadType", facing_street_road.getText().toString());
            jsonObjectDet.put("RoadWidth", facing_street_road_width.getText().toString());
            jsonObjectDet.put("DepthSize", depth.getText().toString());
            if (facing_street_road_id == null)
                jsonObjectDet.put("RoadWidthUnitId", "");
            else {
                jsonObjectDet.put("RoadWidthUnitId", facing_street_road_id);
            }

            jsonObjectDet.put("SuperArea", super_area.getText().toString());
            jsonObjectDet.put("BuiltUpArea", built_up.getText().toString());
            jsonObjectDet.put("CarpetArea", carpet.getText().toString());

            // new param

            if (super_area_id == null)
                jsonObjectDet.put("SuperAreaUnitId", "");
            else {
                jsonObjectDet.put("SuperAreaUnitId", super_area_id);
            }
            if (built_up_area_id == null)
                jsonObjectDet.put("BuiltUpAreaUnitId", "");
            else {
                jsonObjectDet.put("BuiltUpAreaUnitId", built_up_area_id);
            }
            if (carpet_area_unit_id == null)
                jsonObjectDet.put("CarpetAreaUnitId", "");
            else {
                jsonObjectDet.put("CarpetAreaUnitId", carpet_area_unit_id);
            }

            jsonObjectDet.put("EnteranceDoorFacing", enterence_radio_txt);
            jsonObjectDet.put("IsDeletable", false);

            if (!edit_property.equals("")) {

                jsonObjectDet.put("IsModify", true);

            } else {

                jsonObjectDet.put("IsModify", false);

            }

            jsonDetailArray.put(jsonObjectDet);

            JSONArray imageJsonArray = new JSONArray();
            JSONArray plcJsonArray = new JSONArray();

            if (!edit_property.equals("")) {

                for (int i = 0; i < propertyImages.size(); i++) {
                    JSONObject picJsonobject = new JSONObject();
                    picJsonobject.put("Id", propertyImages.get(i).getId());
                    imageJsonArray.put(picJsonobject);
                }

            } else {

                for (int i = 0; i < photosModels.size(); i++) {
                    JSONObject picJsonobject = new JSONObject();
                    picJsonobject.put("Id", photosModels.get(i).getId());
                    imageJsonArray.put(picJsonobject);
                }

            }

            for (int i = 0; i < capturePlcList.size(); i++) {
                JSONObject plcJsonobject = new JSONObject();
                plcJsonobject.put("Id", capturePlcList.get(i).getId());
                plcJsonobject.put("Name", capturePlcList.get(i).getName());
                plcJsonobject.put("IsChecked", capturePlcList.get(i).isChecked());
                plcJsonobject.put("IsModify", true);

                plcJsonArray.put(plcJsonobject);
            }

            locationObj.put("PropertyAreas", jsonDetailArray);
            locationObj.put("PropertyImages", imageJsonArray);
            locationObj.put("PropertyPlcs", plcJsonArray);

            postProperty();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
//                    if (userChoosenTask.equals("Take Photo"))
//                        cameraIntent();
//                    else if (userChoosenTask.equals("Choose from Library"))
//                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodedImageData = getEncoded64ImageStringFromBitmap(bm);
                postPropertyImages();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ivImage.setImageBitmap(bm);
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
     //  File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
       File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encodedImageData = getEncoded64ImageStringFromBitmap(thumbnail);
        postPropertyImages();
//        ivImage.setImageBitmap(thumbnail);
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    private void othersideinflateview() {

        LayoutInflater inflater =
                (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        other_side_lay = (LinearLayout) view.findViewById(R.id.other_side_lay);
        other_side_view = inflater.inflate(R.layout.otherside_view_inflate, null);

        other_side_select = (Spinner) other_side_view.findViewById(R.id.other_side_select);
        other_side = (EditText) other_side_view.findViewById(R.id.other_side);

        other_side_lay.addView(other_side_view);

    }

    private void othersideinflateview1() {

        LayoutInflater inflater =
                (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        other_side_lay = (LinearLayout) view.findViewById(R.id.other_side_lay);
        other_side_view = inflater.inflate(R.layout.otherside_view_inflate1, null);

        other_side_select = (Spinner) other_side_view.findViewById(R.id.other_side_select);
        other_side = (EditText) other_side_view.findViewById(R.id.other_side);
        remove_other_layout = (LinearLayout) other_side_view.findViewById(R.id.remove_other_layout);
        //remove_others = (ImageView) other_side_view.findViewById(R.id.remove_other);

        other_side_lay.addView(other_side_view);

        remove_other_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                other_side_lay.removeView(other_side_view);
            }
        });


    }

    private void imageinflateview(final ArrayList<InventoryModel.PropertyImage> propertyImages) {
        image_view_infate.removeAllViews();

        for (int i = 0; i < propertyImages.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + propertyImages.get(i).getUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + propertyImages.get(i).getUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + propertyImages.get(i).getUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   id=(Integer)view.getTag();

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(propertyImages.get(id).getId());
                                    propertyImages.remove(id);
                                    imageinflateview(propertyImages);
                                }
                            })
                            .show();
                }
            });

            image_view_infate.addView(view);

        }

    }

    private void imageinflateview() {
        image_view_infate.removeAllViews();

        for (int i = 0; i < photosModels.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   id=(Integer)view.getTag();

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(photosModels.get(id).getId());
                                    photosModels.remove(id);
                                    imageinflateview();
                                }
                            })
                            .show();
                }
            });

            image_view_infate.addView(view);

        }

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_PHOTOS)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        JSONArray propertyPhotos = jsonObject_photo.getJSONArray("Result");

                        if (propertyPhotos.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < propertyPhotos.length(); index++) {
                                JSONObject jsonObject1 = propertyPhotos.getJSONObject(index);

                                PhotosModel photosModel = new PhotosModel();

                                photosModel.setId(jsonObject1.getString("Id"));
                                photosModel.setName(jsonObject1.getString("Name"));
                                photosModel.setImageUrl(jsonObject1.getString("Url"));
                                photosModels.add(photosModel);

                            }
                            imageinflateview();

                        }
                    }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_REMOVE_PROPERTY_PHOTOS)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        Utils.showSuccessErrorMessage("Success", "Image deleted successfully", "Ok", getActivity());
                    }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            propAreaModel = new PropertyAreasModel();
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            // String pid = jsonObject2.getString("Id");

                            SM.sendData(pid, 2);

                            Toast.makeText(getActivity(), "Inventory updated successfully", Toast.LENGTH_SHORT).show();

                            JSONArray propJsonArray = jsonObject2.getJSONArray("PropertyAreas");

                            String message = jsonObject1.getString("Message");

/*
                            for (int i = 0; i < propJsonArray.length(); i++) {


                                JSONObject dataObj = propJsonArray.getJSONObject(i);

                                propAreaModel.setId(dataObj.getString("Id"));
                                propAreaModel.setPlotShape(dataObj.getString("PlotShape"));
                                propAreaModel.setPlotArea(dataObj.getString("PlotArea"));
                                propAreaModel.setPlotAreaUnitId(dataObj.getString("PlotAreaUnitId"));
                                propAreaModel.setFrontSize(dataObj.getString("FrontSize"));
                                propAreaModel.setFrontSizeUnitId(dataObj.getString("FrontSizeUnitId"));
                                propAreaModel.setDepthSize(dataObj.getString("DepthSize"));
                                if (dataObj.getString("DepthSizeUnitId") != null)
                                    propAreaModel.setDepthSizeUnitId(dataObj.getString("DepthSizeUnitId"));
                                propAreaModel.setStreetOrRoadType(dataObj.getString("StreetOrRoadType"));
                                propAreaModel.setRoadWidth(dataObj.getString("RoadWidth"));
                                propAreaModel.setRoadWidthUnitId(dataObj.getString("RoadWidthUnitId"));
                                propAreaModel.setEnteranceDoorFacing(dataObj.getString("EnteranceDoorFacing"));

                                propAreaModel.setSuperArea(dataObj.getString("SuperArea"));
                                propAreaModel.setBuiltUpArea(dataObj.getString("BuiltUpArea"));
                                propAreaModel.setCarpetArea(dataObj.getString("CarpetArea"));

                                propAreaModel.isDeletable(dataObj.getBoolean("IsDeletable"));
                                propAreaModel.isModify(dataObj.getBoolean("IsModify"));

                                propArrayList.add(propAreaModel);


                            }
*/

                           /* if (propArrayList.size() != 0){

                                Result_ result_ = new Result_();
                                result_.setPropAreasList(propArrayList);

                            }
*/

                            // setting PropertyPlcs data
                            JSONArray propPics = jsonObject2.getJSONArray("PropertyPlcs");

                            for (int index = 0; index < propPics.length(); index++) {
                                JSONObject propPicsJSONObject = propPics.getJSONObject(index);

                                if (propPicsJSONObject.getInt("Id") == 0)
                                    picsModel.setId(propPicsJSONObject.getInt("Id"));
                                else
                                    picsModel.setId(0);
                                if (propPicsJSONObject.getString("Name") == null)
                                    picsModel.setName(propPicsJSONObject.getString("Name"));
                                else
                                    picsModel.setName(" ");

                                picsModel.isDeletable(propPicsJSONObject.getBoolean("IsDeletable"));
                                picsModel.isModify(propPicsJSONObject.getBoolean("IsModify"));
                                PropPics.add(picsModel);

                            }

                            // setting PropertyImages

                            JSONArray propImages = jsonObject2.getJSONArray("PropertyImages");

                            for (int index = 0; index < propImages.length(); index++) {
                                JSONObject propPicsJSONObject = propImages.getJSONObject(index);

                                //imageModel.setId(0);

                                if (propPicsJSONObject.getInt("Id") == 0)
                                    imageModel.setId(0);
                                else
                                    imageModel.setId(propPicsJSONObject.getInt("Id"));

                                imageModel.setDeletable(propPicsJSONObject.getBoolean("IsDeletable"));
                                imgModel.add(imageModel);
                            }


                        } else {
                         //   SM.sendData(pid, 2);

                           Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.URL_PROPERTY_POST_DRAFT)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {


                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");



                            Utils.showSuccessErrorMessage("Success", "Draft added successfully", "Ok", getActivity());


                        } else {

                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setCapturePlcAdapter() {

        capturePlcAddAdapter = new CapturePlcAddAdapter(getActivity(), capturePlcList);

        rv_capture_plc.setAdapter(capturePlcAddAdapter);

    }

    public  void onEvent(ArrayList<CapturePlcModel> capturePlcList){

        this.capturePlcList = capturePlcList;

    }

    private void addDataToCapturePlcList() {

        capturePlcList.clear();

        for (int i=0; i<strings.size();i++){

            CapturePlcModel capturePlcModel = new CapturePlcModel();
            capturePlcModel.setId("0");
            capturePlcModel.setName(strings.get(i));
            capturePlcModel.setChecked(false);
            capturePlcList.add(capturePlcModel);
        }

        setCapturePlcAdapter();
    }

    private void postProperty_draft() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Constants.URL_PROPERTY_POST_DRAFT,
                Constants.POST).execute();

    }

    private void postProperty() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_POST,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = locationObj.toString();
        } catch (Exception e) {

        }


        return inputStr;
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(getActivity(), MyPropertyActivity.class);
            intent.putExtra("from_savedrafts", "from_savedrafts");

            startActivity(intent);
        }

    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }


    public Context getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(Context applicationContext) {
        this.applicationContext = applicationContext;
    }
}
