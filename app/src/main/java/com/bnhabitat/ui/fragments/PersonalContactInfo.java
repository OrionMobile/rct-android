package com.bnhabitat.ui.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.contacts.AddressTable;
import com.bnhabitat.data.contacts.ContatctTable;
import com.bnhabitat.data.contacts.EmailTable;
import com.bnhabitat.data.contacts.LocalContatctTable;
import com.bnhabitat.data.contacts.PhoneTable;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.OnEditTextChanged;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.AllGroupsModel;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ContactSocialDetails;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.CountryModel;
import com.bnhabitat.models.RelationModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.activities.MainDashboardActivity;
import com.bnhabitat.ui.activities.ProfileActivity;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.adapters.EmailAdapter;
import com.bnhabitat.ui.adapters.RecyclerAdapter;
import com.bnhabitat.ui.adapters.RelationAdapter;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.MultiTextWatcher;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utility;
import com.bnhabitat.utils.Utils;
import com.codetroopers.betterpickers.datepicker.DatePickerBuilder;
import com.codetroopers.betterpickers.datepicker.DatePickerDialogFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.Manifest.permission.CAMERA;
import static android.support.v4.content.ContextCompat.checkSelfPermission;
import static com.bnhabitat.ui.activities.CompleteContactInfoActivity.viewPager;

public class PersonalContactInfo extends Fragment implements CommonAsync.OnAsyncResultListener, OnEditTextChanged {
    //  int selectedradioId;
    String sex = "Male";

    ArrayList<ContactsModel> contactsModels = new ArrayList<>();
    private ArrayList<ContactsModel.ContactRelations> moreRelationsArray = new ArrayList();
    private ArrayList<ContactsModel.ContactRelations> relationList = new ArrayList();
    ArrayList<ContactsModel.Phones> phonesArrayList = new ArrayList<>();
    ArrayList<ContactsModel.Email> email_ArrayList = new ArrayList<>();
    ArrayList<ContactSocialDetails> socialArrayList = new ArrayList<>();

    ContactsModel.Email email_array = new ContactsModel().new Email();
    ArrayList<ContactsModel.Email> emails = new ArrayList<>();
    private static final int PICK_IMAGE_CAMERA = 1;
    private static final int PICK_IMAGE_GALLERY = 11;
    private static final int REQUEST_CAMERA_PERMISSION = 100;
    private TextView years, month, day;
    View rootView;
    SendMessage SM;
    String userChoosenTask;
    String edit = "";
    private ArrayList<String> moreEmailsArray = new ArrayList();
    private ArrayList<String> morePhonesArray = new ArrayList();
    private ArrayList<String> morePhonesCodesArray = new ArrayList();

    private ArrayList<LinearLayout> linearlayoutArray = new ArrayList();
    private Button save_and_continue;
    RecyclerView more_phones_lay, more_emails_lay;
    private LinearLayout add_more_phones_lay, add_more_emails, relation_more_lay;
    private EditText first_name, middle_name, last_name, website, facebook, twitter, other_social_profile;
    private ImageView profile_image;
    Bitmap bitmap;
    private String imgPath;
    File destination;

    int yearSet, monthSet, daySet;
    private static int i = 0;
    private ArrayList<AllGroupsModel> allGroupsModels;
    private ArrayList<AllGroupsModel> religionNames;
    private static final int REQUEST_CAMERA = 100;
    private static final int SELECT_FILE = 101;

    AutoCompleteTextView prefix_auto_complt, suffix_auto_complt;
    AutoCompleteTextView religion;
    EditText email, phone, Phone_code, relation_first_name, relation_last_name;
    Spinner relationSpinner;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    RadioButton radioMale, radioFemale;
    String edit_data;
    ArrayList<HashMap<Integer, ContactsModel.Phones>> arrayList = new ArrayList<>();
    ArrayList<HashMap<Integer, ContactsModel.Email>> emailarrayList = new ArrayList<>();
    int count, email_count;
    private RecyclerAdapter recyclerAdapter;
    RadioGroup radioSex;
    RadioButton radio_sex_btn;
    TextView tv_website,tv_facebook,tv_twitter;
    private EmailAdapter emailAdapter;
    Spinner marital_spinner;
    RecyclerView rl_add_more_relation;
    RelationAdapter relationAdapter;
    ArrayList<RelationModel> addresses = new ArrayList<>();
    ArrayList<LinearLayout> relationLayout = new ArrayList<>();
    ArrayList<ContactsModel.ContactRelations> relationarraylist = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;

    public PersonalContactInfo() {
        // Required empty public constructor
    }

    int getDay, getMonth, getYear;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_personal_contact_info, container, false);
        rl_add_more_relation = (RecyclerView) rootView.findViewById(R.id.rl_add_more_relation);
        tv_twitter = (TextView)rootView.findViewById(R.id.tv_twitter);
        tv_facebook = (TextView)rootView.findViewById(R.id.tv_facebook);
        tv_website = (TextView)rootView.findViewById(R.id.tv_website);

        marital_spinner = (Spinner) rootView.findViewById(R.id.marital_spinner);
        years = (TextView) rootView.findViewById(R.id.year);
        month = (TextView) rootView.findViewById(R.id.month);
        day = (TextView) rootView.findViewById(R.id.day);
        radioSex = (RadioGroup) rootView.findViewById(R.id.radioSex);
        radioMale = (RadioButton) rootView.findViewById(R.id.radioMale);
        radioFemale = (RadioButton) rootView.findViewById(R.id.radioFemale);
        more_phones_lay = (RecyclerView) rootView.findViewById(R.id.more_phones_lay);
        more_emails_lay = (RecyclerView) rootView.findViewById(R.id.more_emails_lay);
        add_more_emails = (LinearLayout) rootView.findViewById(R.id.add_more_emails);
        relation_more_lay = (LinearLayout) rootView.findViewById(R.id.relation_more_lay);
        add_more_phones_lay = (LinearLayout) rootView.findViewById(R.id.add_more_phones_lay);
        save_and_continue = (Button) rootView.findViewById(R.id.save_and_continue);
        profile_image = (ImageView) rootView.findViewById(R.id.profile_image);
        prefix_auto_complt = (AutoCompleteTextView) rootView.findViewById(R.id.prefix_auto_complt);
        suffix_auto_complt = (AutoCompleteTextView) rootView.findViewById(R.id.suffix_auto_complt);
        religion = (AutoCompleteTextView) rootView.findViewById(R.id.religion);
//      prefix = (EditText) rootView.findViewById(R.id.prefix);
        first_name = (EditText) rootView.findViewById(R.id.first_name);
        middle_name = (EditText) rootView.findViewById(R.id.middle_name);
        last_name = (EditText) rootView.findViewById(R.id.last_name);
        relation_first_name = (EditText) rootView.findViewById(R.id.relation_first_name);
        relation_last_name = (EditText) rootView.findViewById(R.id.relation_last_name);
        website = (EditText) rootView.findViewById(R.id.website);
        facebook = (EditText) rootView.findViewById(R.id.facebook);

        twitter = (EditText) rootView.findViewById(R.id.twitter);
        other_social_profile = (EditText) rootView.findViewById(R.id.other_social_profile);
        more_phones_lay.setLayoutManager(new LinearLayoutManager(getActivity()));
        more_emails_lay.setLayoutManager(new LinearLayoutManager(getActivity()));
/*
        facebook.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("http://")){
                    facebook.setText("www.facebook.com/");
                    Selection.setSelection(facebook.getText(), facebook.getText().length());

                }

            }
        });
*/
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.select_dialog_item, getActivity().getResources().getStringArray(R.array.prefix));
        prefix_auto_complt.setThreshold(1);
        prefix_auto_complt.setAdapter(adapter);


        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>
                (getActivity(), android.R.layout.select_dialog_item, getActivity().getResources().getStringArray(R.array.suffix));
        suffix_auto_complt.setThreshold(1);
        suffix_auto_complt.setAdapter(adapter1);


        linearlayoutArray.clear();
        onClicks();


        onGetRequest("2");
        onGetRequest("8");
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            contactsModels = (ArrayList<ContactsModel>) bundle.getSerializable("ModelName");
            edit_data = bundle.getString("edit") == null ? "" : bundle.getString("edit");
        }
        if (contactsModels == null || contactsModels.isEmpty()) {
            onEmailAdd();
            onPhoneNumberAdd();
            //  onRelationAdd();
        } else {
            int posIndustry = 0;
            int posAnnualIncome = 0;
            first_name.setText(contactsModels.get(0).getFirstName());
            middle_name.setText(contactsModels.get(0).getMiddleName());
            prefix_auto_complt.setText(contactsModels.get(0).getPrefix());
            suffix_auto_complt.setText(contactsModels.get(0).getSufix());
            religion.setText(contactsModels.get(0).getReligion());
            last_name.setText(contactsModels.get(0).getLastName());


            if (contactsModels.get(0).getGender().equalsIgnoreCase("Male")) {
                radioMale.setChecked(true);
                radioFemale.setChecked(false);
            } else {
                radioMale.setChecked(false);
                radioFemale.setChecked(true);
            }
            String dob = contactsModels.get(0).getDateOfBirthStr();
            try {
                if (dob == null || dob.equalsIgnoreCase("None") || dob.equalsIgnoreCase("null")) {
                    day.setText("");
                    month.setText("");
                    years.setText("");
                } else {
                    String dob1[] = dob.split("/");
                    getDay = Integer.valueOf(dob1[0]);
                    getMonth = Integer.valueOf(dob1[1]);
                    getYear = Integer.valueOf(dob1[2]);

                    day.setText(dob1[0]);
                    month.setText(dob1[1]);
                    years.setText(dob1[2]);
                }
            } catch (Exception e) {

            }
            if (contactsModels.get(0).getImageUrl() == null || contactsModels.get(0).getImageUrl().equalsIgnoreCase("None"))
                profile_image.setImageResource(R.drawable.user_profile_img);


            else
                imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + contactsModels.get(0).getImageUrl(), profile_image, options);

            if (!contactsModels.get(0).getContactSocialDetails().isEmpty()) {

                for (int i=0;i<contactsModels.get(0).getContactSocialDetails().size();i++){

                    if (contactsModels.get(0).getContactSocialDetails().get(i).getType().equalsIgnoreCase("website")){

                        website.setText(contactsModels.get(0).getContactSocialDetails().get(0).getDetail());

                    }
                    else  if (contactsModels.get(0).getContactSocialDetails().get(i).getType().equalsIgnoreCase("facebook")){

                            facebook.setText(contactsModels.get(0).getContactSocialDetails().get(0).getDetail());

                    }
                    else  if (contactsModels.get(0).getContactSocialDetails().get(i).getType().equalsIgnoreCase("twitter")){

                        twitter.setText(contactsModels.get(0).getContactSocialDetails().get(0).getDetail());

                    }

                }

            }

            if (!contactsModels.get(0).getEmails().isEmpty()) {
                for (int i = 0; i < contactsModels.get(0).getEmails().size(); i++) {
                    ContactsModel.Email email = contactsModels.get(0).getEmails().get(i);

                    HashMap<Integer, ContactsModel.Email> hashMap = new HashMap<>();
                    hashMap.put(email_count++, email);
                    emailarrayList.add(hashMap);
                }
                emailAdapter = new EmailAdapter(getActivity(), emailarrayList, this);
                more_emails_lay.setAdapter(emailAdapter);
//                    for(int i=0;i<contactsModels.get(0).getEmails().size();i++){
//                         email_array.setAddress(contactsModels.get(0).getEmails().get(i).getAddress());
//                        emails.add(email_array);
//                        if(!emails.get(0).getAddress().equalsIgnoreCase("None")
//                                ||!emails.get(0).getAddress().equalsIgnoreCase("")){
////                            email.setText(contactsModels.get(0).getEmails().get(i).getAddress());
//                            onEmailAdd(emails.get(i).getAddress());
//                        }
//                    }

            } else {
                onEmailAdd();
            }
            if (!contactsModels.get(0).getPhones().isEmpty()) {
                for (int i = 0; i < contactsModels.get(0).getPhones().size(); i++) {
                    ContactsModel.Phones phones = contactsModels.get(0).getPhones().get(i);

                    HashMap<Integer, ContactsModel.Phones> hashMap = new HashMap<>();
                    hashMap.put(count++, phones);
                    arrayList.add(hashMap);
                }
                recyclerAdapter = new RecyclerAdapter(getActivity(), arrayList, this);
                more_phones_lay.setAdapter(recyclerAdapter);


            } else {
                onPhoneNumberAdd();
            }

          /*  if (!contactsModels.get(0).getContactRelationses().isEmpty()) {
                for (int i = 0; i < contactsModels.get(0).getContactRelationses().size(); i++) {
                    if (!contactsModels.get(0).getContactRelationses().get(0).getRelationFirstName().equalsIgnoreCase("")) {

                        onRelationAdd(contactsModels.get(0).getContactRelationses().get(i).getRelationFirstName(), contactsModels.get(0).getContactRelationses().get(i).getRelationLastName(), contactsModels.get(0).getContactRelationses().get(i).getRelation());


                    }


                }

            } else {
                onRelationAdd();
            }*/
        }


        marital_spinner.setPrompt("Title");

        radioSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);

                sex = checkedRadioButton.getText().toString();

            }
        });


//                for (int i = 0; i <= industryNames.size(); i++) {
//                    if (industryNames.get(i).equalsIgnoreCase(contactsModels.get(0).getIndustryName())) {
//                        posIndustry = i;
//                    }
//                }
//                industry_spinner.setSelection(posIndustry);
//
//
//                for (int i = 0; i <= industryNames.size(); i++) {
//                    if (annualIncomeNames.get(i).equalsIgnoreCase(contactsModels.get(0).getAnnualIncome())) {
//                        posAnnualIncome = i;
//                    }
//
//                industry_spinner.setSelection(posAnnualIncome);
//
//            }

        linearLayoutManager = new LinearLayoutManager(getActivity());
        rl_add_more_relation.setLayoutManager(linearLayoutManager);
        if (contactsModels.size() == 0 && contactsModels.isEmpty()) {

            relationAdapter = new RelationAdapter(getActivity(), relationList);

        } else {

            relationAdapter = new RelationAdapter(getActivity(), contactsModels.get(0).getContactRelationses());

        }

        rl_add_more_relation.setAdapter(relationAdapter);
        if(contactsModels.size()>0){

            if (contactsModels.get(0).getContactRelationses().size() > 0 ) {

                relationarraylist = contactsModels.get(0).getContactRelationses();
                relationAdapter.notifyDataSetChanged();


            } else {

                OnAdding();
            }
        }

        return rootView;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();


        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    private void onGetRequest(String taxonomyId) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_TERMS_DATA + taxonomyId + "/1", "", "Loading...", this, Urls.URL_GET_TERMS_DATA, Constants.GET).execute();

    }

    private void onPhoneNumberAdd() {


        ContactsModel.Phones phones = new ContactsModel().new Phones();
        phones.setPhoneNumber("");
        phones.setPhoneCode("");
        phones.setId("0");
        HashMap<Integer, ContactsModel.Phones> hashMap = new HashMap<>();
        hashMap.put(count++, phones);
        arrayList.add(hashMap);

        recyclerAdapter = new RecyclerAdapter(getActivity(), arrayList, this);
        more_phones_lay.setAdapter(recyclerAdapter);
//        LayoutInflater inflator = LayoutInflater.from(getActivity());
//        LinearLayout parentPanel = (LinearLayout) rootView.findViewById(R.id.more_phones_lay);
//
////
////                for(String data : myList) {
//        // inflate child
//        View item = inflator.inflate(R.layout.more_phone_layout, null);
//        // initialize review UI
//         phone = (EditText) item.findViewById(R.id.phone);
//        Phone_code = (EditText) item.findViewById(R.id.Phone_code);
////                    // set data
////                    dataText.setText(data);
//        // add child
//        parentPanel.addView(item);
//        morePhonesArray.add(phone.getText().toString());
//        morePhonesCodesArray.add(Phone_code.getText().toString());
    }

    private void onEmailAdd() {
        ContactsModel.Email email = new ContactsModel().new Email();
        email.setAddress("");
        email.setId("0");

        HashMap<Integer, ContactsModel.Email> hashMap = new HashMap<>();
        hashMap.put(email_count++, email);
        emailarrayList.add(hashMap);
        emailAdapter = new EmailAdapter(getActivity(), emailarrayList, this);
        more_emails_lay.setAdapter(emailAdapter);
//        LayoutInflater inflator = LayoutInflater.from(getActivity());
//        LinearLayout parentPanel = (LinearLayout) rootView.findViewById(R.id.more_emails_lay);
//        i = i + 1;
//
////        parentPanel.setId(i);
////
////                for(String data : myList) {
//        // inflate child
//        View item = inflator.inflate(R.layout.more_emails_layout, null);
//        // initialize review UI
//        email = (EditText) item.findViewById(R.id.email);
//        email.setText(email_txt);
////                    // set data
////                    dataText.setText(data);
//        // add child
//
//
//        parentPanel.addView(item);
//        linearlayoutArray.add(parentPanel);

//                }

    }

    private void onRelationAdd() {
        LayoutInflater inflator = LayoutInflater.from(getActivity());
        LinearLayout parentPanel = (LinearLayout) rootView.findViewById(R.id.relation_lay);
        i = i + 1;

        View item = inflator.inflate(R.layout.more_relation_layout, null);
        // initialize review UI
        relationSpinner = (Spinner) item.findViewById(R.id.relation_spinner);

        relationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        relation_first_name = (EditText) item.findViewById(R.id.relation_first_name);
        relation_last_name = (EditText) item.findViewById(R.id.relation_last_name);
//                    // set data
//                    dataText.setText(data);
        // add child

        parentPanel.addView(item);
        linearlayoutArray.add(parentPanel);


    }

    private void onRelationAdd(String firstname, String lastname, String relation) {


        LayoutInflater inflator = LayoutInflater.from(getActivity());
        LinearLayout parentPanel = (LinearLayout) rootView.findViewById(R.id.relation_lay);
        i = i + 1;

//        parentPanel.setId(i);
//
//                for(String data : myList) {
        // inflate child
        View item = inflator.inflate(R.layout.more_relation_layout, null);
        // initialize review UI
        relationSpinner = (Spinner) item.findViewById(R.id.relation_spinner);
        String[] relationArray = getResources().getStringArray(R.array.Relation);
        for (int i = 0; i < relationArray.length; i++) {

            if (relationArray[i].equalsIgnoreCase(relation)) {
                relationSpinner.setSelection(i);
            }
        }
        relationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        relation_first_name = (EditText) item.findViewById(R.id.relation_first_name);
        relation_last_name = (EditText) item.findViewById(R.id.relation_last_name);
//                    // set data
//                    dataText.setText(data);
        // add child

        relation_first_name.setText(firstname);
        relation_last_name.setText(lastname);


        parentPanel.addView(item);
        linearlayoutArray.add(parentPanel);




    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void onClicks() {
//        try {
//            selectedradioId = radioSex.getCheckedRadioButtonId();
//            radio_sex_btn = (RadioButton) rootView.findViewById(selectedradioId);
//            if (radio_sex_btn.getText().equals("Male")) {
//                sex = "Male";
//            } else {
//                sex = "Female";
//            }
//        } catch (Exception e) {
//
//        }


        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDrawerPermision();
//              selectImage();
            }
        });

//           day.seton
        day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDate(getDay, getMonth, getYear);

            }
        });


        add_more_emails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isEmailValid()) {
                    ContactsModel.Email email = new ContactsModel().new Email();
                    email.setAddress("");
                    email.setId("0");

                    HashMap<Integer, ContactsModel.Email> hashMap = new HashMap<>();
                    hashMap.put(email_count++, email);
                    emailarrayList.add(hashMap);

                    emailAdapter.updateList(emailarrayList);
                } else {
                    Toast.makeText(getActivity(), "Enter valid Email", Toast.LENGTH_SHORT).show();
                }


//                if(!contactsModels.isEmpty()){
//                    if(!email.getText().toString().equalsIgnoreCase("")){
//                        email_array=new ContactsModel().new Email();
//                        email_array.setAddress(email.getText().toString());
//                        emails.add(email_array);
//                        onEmailAdd("");
//                    }
////                        ContactsModel.Email email=new ContactsModel().new Email();
////                        email.setAddress();
//                }else{
//                    if(!email.getText().toString().equalsIgnoreCase("")){
//
//                        email_array.setAddress(email.getText().toString());
//                        emails.add(email_array);
//                        onEmailAdd("");
//                    }
//                }
//                if(email.getText().toString().length()>0){
//                    moreEmailsArray.add(email.getText().toString());
//                }


            }
        });

        add_more_phones_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ContactsModel.Phones phones = new ContactsModel().new Phones();
                phones.setPhoneNumber("");
                phones.setPhoneCode("");
                phones.setId("0");
                HashMap<Integer, ContactsModel.Phones> hashMap = new HashMap<>();
                hashMap.put(count++, phones);
                arrayList.add(hashMap);

                recyclerAdapter.updateList(arrayList);
//              onPhoneNumberAdd();
            }
        });


        relation_more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             /*   if (relationSpinner.getSelectedItem().toString().equalsIgnoreCase("Select Item")) {

                    Toast.makeText(getActivity(), "Please select relation", Toast.LENGTH_SHORT).show();

                } else if (relation_first_name.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select first name", Toast.LENGTH_SHORT).show();
                } else if (relation_last_name.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please select last name", Toast.LENGTH_SHORT).show();
                } else {
                    final ContactsModel.ContactRelations relation = new ContactsModel().new ContactRelations();
                    relation.setRelationFirstName(relation_first_name.getText().toString());
                    relation.setRelationLastName(relation_last_name.getText().toString());
                    relation.setRelation(relationSpinner.getSelectedItem().toString());
                    moreRelationsArray.add(relation);

                    onRelationAdd();
                }*/

                OnAdding();


            }
        });


        save_and_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                socialArrayList.clear();
                // set social data if any
                if (!website.getText().toString().trim().equalsIgnoreCase("")){

                    ContactSocialDetails contactSocialDetails = new ContactSocialDetails();
                    contactSocialDetails.setType(tv_website.getText().toString());
                    contactSocialDetails.setDetail(website.getText().toString());
                    socialArrayList.add(contactSocialDetails);
                }
                if (!facebook.getText().toString().trim().equalsIgnoreCase("")){

                    ContactSocialDetails contactSocialDetails = new ContactSocialDetails();
                    contactSocialDetails.setType(tv_facebook.getText().toString());
                    contactSocialDetails.setDetail(facebook.getText().toString());
                    socialArrayList.add(contactSocialDetails);
                }

                if (!twitter.getText().toString().trim().equalsIgnoreCase("")){

                    ContactSocialDetails contactSocialDetails = new ContactSocialDetails();
                    contactSocialDetails.setType(tv_twitter.getText().toString());
                    contactSocialDetails.setDetail(twitter.getText().toString());
                    socialArrayList.add(contactSocialDetails);
                }


                for (int i = 0; i < arrayList.size(); i++) {

                    HashMap<Integer, ContactsModel.Phones> hashMap = arrayList.get(i);
                    ContactsModel.Phones phones = hashMap.get(i);
                    Log.e("Heloo", " value== " + phones.getPhoneNumber());
                    phonesArrayList.add(phones);

                }
                for (int i = 0; i < emailarrayList.size(); i++) {

                    HashMap<Integer, ContactsModel.Email> hashMap = emailarrayList.get(i);
                    ContactsModel.Email email = hashMap.get(i);
                    email_ArrayList.add(email);

                    Log.e("Heloo", " value== " + email.getAddress());

                }

                if (isValid()) {
                    onPostContacts();
                }

                if (first_name.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please enter first name", Toast.LENGTH_SHORT).show();
                } else {
                    if (first_name.getText().toString().equalsIgnoreCase("None")) {
                        Toast.makeText(getActivity(), "Enter valid name", Toast.LENGTH_SHORT).show();
                    } else {

                    }

                }

//                if(email.getText().toString().length()>0){
//                    moreEmailsArray.add(email.getText().toString());
//                }
//               if(phone.getText().toString().length()>0){
//                   morePhonesArray.add(phone.getText().toString());
//               }
//               if(Phone_code.getText().toString().length()>0){
//                   morePhonesCodesArray.add(Phone_code.getText().toString());
//               }
//
//
//                if (first_name.getText().toString().equalsIgnoreCase("")) {
//                    Toast.makeText(getActivity(), "Please enter first name", Toast.LENGTH_SHORT).show();
//                } else if (phone.getText().toString().length()==10) {
//                    Toast.makeText(getActivity(), "Enter valid phone number", Toast.LENGTH_SHORT).show();
//                }
//                else {
//                    if(!contactsModels.isEmpty()){
//                        if(!email.getText().toString().equalsIgnoreCase("")){
//
//                            email_array.setAddress(email.getText().toString());
//                            emails.add(email_array);
//                        }
////                        ContactsModel.Email email=new ContactsModel().new Email();
////                        email.setAddress();
//                    }
//                    onPostContacts();
//                }

            }
        });
    }

    public void OnAdding() {

        final ContactsModel.ContactRelations relation1 = new ContactsModel().new ContactRelations();
        relation1.setRelationFirstName("");
        relation1.setId(0);
        relation1.setRelationLastName("");
        relation1.setRelation("");
        relationList.add(relation1);
        relationAdapter.notifyDataSetChanged();

    }

    private void checkDrawerPermision() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        } else {

            selectImage();

        }

    }

    private void onPostContacts() {

//        if (contactsModels == null || contactsModels.isEmpty()) {
//            new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
//                    getLoginInputJson(),
//                    "Loading...",
//                    this,
//                    Urls.URL_CREATE_CONTACTS,
//                    Constants.POST).execute();
//
//
//        } else {
        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
                getInputJson(),
                "Loading...",
                this,
                Urls.URL_CREATE_CONTACTS,
                Constants.POST).execute();

    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
//                    if (userChoosenTask.equals("Take Photo"))
//                        cameraIntent();
//                    else if (userChoosenTask.equals("Choose from Library"))
//                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    public void showDate(int day, int month, int year) {

        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();

        if (day == 0 && month == 0 && year == 0) {
            args.putInt("year", calender.get(Calendar.YEAR));
            args.putInt("month", calender.get(Calendar.MONTH));
            args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        } else {
            args.putInt("year", day);
            args.putInt("month", month);
            args.putInt("day", year);
        }


        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");

//        DatePickerBuilder dpb = new DatePickerBuilder()
//                .setFragmentManager(getChildFragmentManager())
//                .setStyleResId(R.style.BetterPickersDialogFragment)
//                .setYearOptional(true)
//                .setTargetFragment(PersonalContactInfo.this);
//        dpb.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year1, int monthOfYear,
                              int dayOfMonth) {

            day.setText(String.valueOf(dayOfMonth));
            month.setText(String.valueOf(monthOfYear + 1));
            years.setText(String.valueOf(year1));

        }
    };

    private String getInputJson() {

        String inputJson = "";

        try {


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Id", contactsModels.get(0).getId());
            jsonObject.put("Prefix", prefix_auto_complt.getText().toString());
            jsonObject.put("Sufix", suffix_auto_complt.getText().toString());
            jsonObject.put("FirstName", first_name.getText().toString());
            jsonObject.put("LastName", last_name.getText().toString());
            jsonObject.put("MiddleName", middle_name.getText().toString());
            jsonObject.put("MaritalStatus", marital_spinner.getSelectedItem().toString());
            jsonObject.put("CompanyId", Constants.COMPAN_ID);
            jsonObject.put("DateOfBirth", day.getText().toString() + "/" + month.getText().toString() + "/" + years.getText().toString());

            jsonObject.put("DateOfBirthStr", day.getText().toString() + "/" + month.getText().toString() + "/" + years.getText().toString());
            jsonObject.put("Gender", sex);
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));


            jsonObject.put("Profession", contactsModels.get(0).getProfession());
            jsonObject.put("Designation", contactsModels.get(0).getDesignation());

            if (contactsModels.size()>0){

                if (bitmap == null) {
                    if (contactsModels.get(0).getExistingImageUrl() == null) {
                        jsonObject.put("ImageUrl", "");
                    } else {
                        jsonObject.put("ImageUrl", contactsModels.get(0).getExistingImageUrl());
                    }
                } else if (edit.equalsIgnoreCase("")) {
                    try {
                        jsonObject.put("ImageUrl", encodeImage(bitmap));
                    } catch (Exception e) {

                    }
                } else {
                    jsonObject.put("ImageUrl", encodeImage(bitmap));

                }

            }


            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < phonesArrayList.size(); i++) {
                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put("Id", phonesArrayList.get(i).getId());

                try {
                    jsonObject2.put("PhoneNumber", phonesArrayList.get(i).getPhoneNumber());
                    jsonObject2.put("PhoneCode", phonesArrayList.get(i).getPhoneCode());
                    if (phonesArrayList.get(i).getId().equalsIgnoreCase("0")) {
                        jsonObject2.put("IsModify", "false");
                    } else {
                        jsonObject2.put("IsModify", "true");
                    }
                } catch (Exception e) {

                }
                jsonObject2.put("Mode", "Work");
                jsonArray.put(jsonObject2);

            }

            // social array
            JSONArray socialjsonArray = new JSONArray();

            for (int i = 0; i < socialArrayList.size(); i++) {

                JSONObject jsonObject1 = new JSONObject();

                jsonObject1.put("Type",socialArrayList.get(i).getType() );
                jsonObject1.put("Detail",socialArrayList.get(i).getDetail() );

                socialjsonArray.put(jsonObject1);

            }
            /*
             * if user does not click add more then relation will add here
             * */


//            final ContactsModel.ContactRelations relation = new ContactsModel().new ContactRelations();
//            relation.setRelationFirstName(relation_first_name.getText().toString());
//            relation.setRelationLastName(relation_last_name.getText().toString());
//            relation.setRelation(relationSpinner.getSelectedItem().toString());
//            relationList.add(relation);

            // adding relationArray data
            contactsModels.get(0).setContactRelationses(relationList);
            JSONArray relationArray = new JSONArray();

            if (contactsModels.size()>0){

                if (!contactsModels.get(0).getContactRelationses().isEmpty()) {

                    for (int i = 0; i < relationList.size(); i++) {
                        JSONObject jsonObjectRelation = new JSONObject();

                        jsonObjectRelation.put("Relation", contactsModels.get(0).getContactRelationses().get(i).getRelation());

                        jsonObjectRelation.put("RelationFirstName", contactsModels.get(0).getContactRelationses().get(i).getRelationFirstName());
                        jsonObjectRelation.put("RelationLastName", contactsModels.get(0).getContactRelationses().get(i).getRelationLastName());

                        if (relationList.get(i).getId() != 0) {
                            jsonObjectRelation.put("Id", contactsModels.get(0).getContactRelationses().get(i).getId());
                            jsonObjectRelation.put("IsModify", true);
                        } else {
                            jsonObjectRelation.put("IsModify", false);
                            jsonObjectRelation.put("Id", 0);

                        }
                        relationArray.put(jsonObjectRelation);
                    }

                }

            }

            JSONArray emailArray = new JSONArray();

            for (int i = 0; i < email_ArrayList.size(); i++) {
                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("Id", email_ArrayList.get(i).getId());
                jsonObject3.put("Address", email_ArrayList.get(i).getAddress());
                if (email_ArrayList.get(i).getId().equalsIgnoreCase("0")) {
                    jsonObject3.put("IsModify", "false");
                } else {
                    jsonObject3.put("IsModify", "true");
                }
                jsonObject3.put("Label", "Email Label");
                emailArray.put(jsonObject3);

            }

            JSONArray addressArray = new JSONArray();
            JSONObject jsonObject1 = new JSONObject();

            if (contactsModels.size()>0){

                if (!contactsModels.get(0).getAddresses().isEmpty()) {
                    for (int i = 0; i < contactsModels.get(0).getAddresses().size(); i++) {
                        jsonObject1.put("StateName", contactsModels.get(0).getAddresses().get(i).getStateName());
                        jsonObject1.put("CountryId", contactsModels.get(0).getAddresses().get(i).getCountryId());
                        jsonObject1.put("CityName", contactsModels.get(0).getAddresses().get(i).getCity_name());
                        jsonObject1.put("DeveloperName", contactsModels.get(0).getAddresses().get(i).getDeveloper_name());
                        jsonObject1.put("Tower", contactsModels.get(0).getAddresses().get(i).getTower());
                        jsonObject1.put("Floor", contactsModels.get(0).getAddresses().get(i).getFloor());
                        jsonObject1.put("UnitName", contactsModels.get(0).getAddresses().get(i).getUnitName());
                        jsonObject1.put("ZipCode", contactsModels.get(0).getAddresses().get(i).getZipcode());
                        addressArray.put(jsonObject1);
                    }

                }

            }

/*
            for (int i = 0; i < moreRelationsArray.size(); i++) {
                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("Relation", moreRelationsArray.get(i).getRelation());
                jsonObject3.put("RelationFirstName", moreRelationsArray.get(i).getRelationFirstName());
                jsonObject3.put("RelationLastName", moreRelationsArray.get(i).getRelationLastName());

                if (moreRelationsArray.get(i).getRelation().equalsIgnoreCase("0")) {
                    jsonObject3.put("IsModify", "false");
                } else {
                    jsonObject3.put("IsModify", "true");
                }
                relationArray.put(jsonObject3);

            }
*/

            JSONArray grouparray = new JSONArray();
            JSONObject jsonObject4 = new JSONObject();
            jsonObject4.put("Id", "");
            grouparray.put(jsonObject4);
            jsonObject.put("Phones", jsonArray);
            jsonObject.put("Addresses", addressArray);
            jsonObject.put("EmailAddresses", emailArray);
            jsonObject.put("ContactSocialDetails", socialjsonArray);
            jsonObject.put("ContactRelations", relationArray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

//    private String getLoginInputJson() {
//
//        String inputJson = "";
////
//
//        try {
//
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("Prefix", prefix_spinner.getSelectedItem());
//            jsonObject.put("FirstName", first_name.getText().toString());
//            jsonObject.put("LastName", last_name.getText().toString());
//            jsonObject.put("MiddleName", middle_name.getText().toString());
//            jsonObject.put("CompanyId", "1");
//            jsonObject.put("DateOfBirth", day.getText().toString() + "/" + month.getText().toString() + "/" + years.getText().toString());
//            jsonObject.put("Gender", "male");
//            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
//            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
//            if (bitmap == null) {
//                jsonObject.put("ImageUrl", "");
//            } else {
//                jsonObject.put("ImageUrl", encodeImage(bitmap));
//            }
//            JSONArray jsonArray = new JSONArray();
//            for (int i = 0; i < phonesArrayList.size(); i++) {
//                if (!phonesArrayList.get(i).getPhoneNumber().equalsIgnoreCase("") || phonesArrayList.get(i).getPhoneNumber().equalsIgnoreCase("None") && phonesArrayList.get(i).getPhoneCode().equalsIgnoreCase("") || phonesArrayList.get(i).getPhoneCode().equalsIgnoreCase("None")) {
//                    JSONObject jsonObject2 = new JSONObject();
//                    jsonObject2.put("PhoneNumber", phonesArrayList.get(i).getPhoneNumber());
//                    jsonObject2.put("PhoneCode", phonesArrayList.get(i).getPhoneCode());
//                    jsonObject2.put("Mode", "Work");
//                    jsonArray.put(jsonObject2);
//                }
//
//
//            }
//
//            JSONArray socialjsonArray = new JSONArray();
//
//
//
//
//
//            JSONArray emailArray = new JSONArray();
//            for (int i = 0; i < email_ArrayList.size(); i++) {
//                if (!email_ArrayList.get(i).getAddress().equalsIgnoreCase("") || email_ArrayList.get(i).getAddress().equalsIgnoreCase("None")) {
//                    JSONObject jsonObject3 = new JSONObject();
//                    jsonObject3.put("Address", email_ArrayList.get(i).getAddress());
//                    jsonObject3.put("Label", "Email Label");
//                    emailArray.put(jsonObject3);
//                }
//            }
//            JSONArray grouparray = new JSONArray();
//            JSONObject jsonObject4 = new JSONObject();
//            jsonObject4.put("Id", "");
//            grouparray.put(jsonObject4);
//            jsonObject.put("Phones", jsonArray);
//            jsonObject.put("EmailAddresses", emailArray);
//            jsonObject.put("ContactAttributes", socialjsonArray);
//
//            inputJson = jsonObject.toString();
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return inputJson;
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        profile_image.setImageBitmap(bitmap);
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
      //  File destination = new File(Environment.getExternalStorageDirectory(),System.currentTimeMillis() + ".jpg");

        File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        profile_image.setImageBitmap(bitmap);
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_CREATE_CONTACTS)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {

                        ContactsModel contactModel = new ContactsModel();
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String msg = "";
                        if (jsonObject1.getBoolean("Success")) {

                            msg = jsonObject1.getString("Message");
//                            Utils.


                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String id = jsonObject2.getString("Id");
                            String FirstName = jsonObject2.getString("FirstName");
                            String LastName = jsonObject2.getString("LastName");
                            String MiddleName = jsonObject2.getString("MiddleName");
                            String CompanyId = jsonObject2.getString("CompanyId");
                            String CompanyName = jsonObject2.getString("CompanyName");
                            String DateOfBirthStr = jsonObject2.getString("DateOfBirthStr");
                            String Prefix = jsonObject2.getString("Prefix");
                            String Sufix = jsonObject2.getString("Sufix");
                            String MaritalStatus = jsonObject2.getString("MaritalStatus");
                            String ExistingImageUrl = jsonObject2.getString("ExistingImageUrl");
                            String Religion = jsonObject2.getString("Religion");
                            String Gender = jsonObject2.getString("Gender");
                            contactModel.setId(id);
                            contactModel.setFirstName(FirstName);
                            contactModel.setLastName(LastName);
                            contactModel.setMiddleName(MiddleName);
                            contactModel.setCompanyName(CompanyName);
                            contactModel.setDateOfBirthStr(DateOfBirthStr);
                            contactModel.setPrefix(Prefix);
                            contactModel.setSufix(Sufix);
                            contactModel.setCompanyId(CompanyId);
                            contactModel.setMaritalStatus(MaritalStatus);
                            contactModel.setExistingImageUrl(ExistingImageUrl);
                            contactModel.setReligion(Religion);
                            contactModel.setGender(Gender);
                            JSONArray sizeJsonArray = jsonObject2.getJSONArray("Addresses");
                            ArrayList<AddressModel> addresses = new ArrayList();
                            ContactsModel contactsModel = new ContactsModel();
                            for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                                AddressModel addresses1 = new AddressModel();
                                JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                                addresses1.setId(dataObj.getString("Id"));

                                addresses1.setCountryId(dataObj.getString("CountryId"));
                                addresses1.setStateName(dataObj.getString("StateName"));
//                                addresses1.setFirstName(dataObj.getString("FirstName"));
//                                addresses1.setLastName(dataObj.getString("LastName"));
//                                addresses1.setEmail(dataObj.getString("Email"));
//                                addresses1.setCompany(dataObj.getString("Company"));
                                addresses1.setCity_name(dataObj.getString("CityName"));
                                addresses1.setAddress1(dataObj.getString("Address1"));
                                addresses1.setAddress2(dataObj.getString("Address2"));
//                              addresses1.setPhoneNumber(dataObj.getString("PhoneNumber"));
//                              addresses1.setFaxNumber(dataObj.getString("FaxNumber"));
                                addresses.add(addresses1);
                            }
                            JSONArray phonesJsonArray = jsonObject2.getJSONArray("Phones");
                            ArrayList<ContactsModel.Phones> phones = new ArrayList();
                            for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                                ContactsModel.Phones phones1 = contactsModel.new Phones();
                                JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                                phones1.setId(dataObj.getString("Id"));
                                phones1.setContactId(dataObj.getString("ContactId"));
                                phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                phones1.setPhoneCode(dataObj.getString("PhoneCode"));
                                phones1.setPhoneType(dataObj.getString("Mode"));
//
                                phones.add(phones1);

                            }
                            JSONArray emailJsonArray = jsonObject2.getJSONArray("EmailAddresses");
                            ArrayList<ContactsModel.Email> emails = new ArrayList();
                            for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                                ContactsModel.Email email = contactsModel.new Email();
                                JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                                email.setId(dataObj.getString("Id"));
                                email.setContactId(dataObj.getString("ContactId"));
                                email.setAddress(dataObj.getString("Address"));
                                email.setLabel(dataObj.getString("Label"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                emails.add(email);
                            }

                            JSONArray socialJsonArray = jsonObject2.getJSONArray("ContactSocialDetails");
                            ArrayList<ContactSocialDetails> socialArray = new ArrayList();
                            for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                                ContactSocialDetails contactAttributes = new ContactSocialDetails();
                                JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                                contactAttributes.setType(dataObj.getString("Type"));
                                contactAttributes.setDetail(dataObj.getString("Detail"));

                                socialArray.add(contactAttributes);
                            }


                            JSONArray contactRelation = jsonObject2.getJSONArray("ContactRelations");
                            ArrayList<ContactsModel.ContactRelations> contactRelationses = new ArrayList();
                            relationarraylist.clear();

                            for (int indexJ = 0; indexJ < contactRelation.length(); indexJ++) {

                                ContactsModel.ContactRelations contactRelations = contactsModel.new ContactRelations();
                                JSONObject dataObj = contactRelation.getJSONObject(indexJ);

                                contactRelations.setRelation(dataObj.getString("Relation"));
                                contactRelations.setRelationFirstName(dataObj.getString("RelationFirstName"));
                                contactRelations.setRelationLastName(dataObj.getString("RelationLastName"));
                                contactRelations.setId(dataObj.getInt("ContactId"));


//                              phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                contactRelationses.add(contactRelations);
                                relationarraylist.add(contactRelations);

                            }

                            relationAdapter = new RelationAdapter(getActivity(), relationarraylist);
                            rl_add_more_relation.setLayoutManager(linearLayoutManager);
                            rl_add_more_relation.setAdapter(relationAdapter);
                            contactModel.setContactRelationses(contactRelationses);
                            contactModel.setPhones(phones);
                            contactModel.setAddresses(addresses);
                            contactModel.setEmails(emails);
                            contactModel.setContactSocialDetails(socialArray);
                            contactsModels = new ArrayList<>();
                            contactsModels.add(contactModel);
                            Toast.makeText(getActivity(), "Personal contact information updated Successfully", Toast.LENGTH_SHORT).show();

                            String phone = "";
                            String email_address = "";
                            if (contactsModels.get(i).getPhones().size() > 0) {
                                phone = contactsModels.get(i).getPhones().get(0).getPhoneNumber();
                                if (LocalContatctTable.getInstance().isPhoneAvailable(phone)) {
                                    LocalContatctTable.getInstance().updateSync(phone);
                                }
                            }
                            if (contactsModels.get(i).getEmails().size() > 0) {
                                email_address = contactsModels.get(i).getEmails().get(0).getAddress();
//
                            }
                            if (ContatctTable.getInstance().isContactAvailable(contactsModels.get(i).getId())) {
                                ContatctTable.getInstance().updateCategory(Integer.parseInt(contactsModels.get(i).getId()),
                                        contactsModels.get(i).getFirstName(), contactsModels.get(i).getLastName(), contactsModels.get(i).getCompanyName(), phone, email_address, 1, contactsModel.getGroup_id(), contactsModel.getGroup_name(), contactsModel.getDateOfBirthStr(), contactsModel.getImageUrl(), "");

                                for (int indexJ = 0; indexJ < contactsModels.get(i).getPhones().size(); indexJ++) {
                                    if (PhoneTable.getInstance().isPhoneAvailable(contactsModels.get(i).getPhones().get(indexJ).getId())) {
                                        PhoneTable.getInstance().updateCategory(
                                                Integer.parseInt(contactsModels.get(i).getPhones().get(indexJ).getId()),
                                                contactsModels.get(i).getPhones().get(indexJ).getPhoneNumber(), "");
                                    } else {
                                        PhoneTable.getInstance().write(Integer.parseInt(contactsModels.get(i).getPhones().get(indexJ).getContactId()),
                                                Integer.parseInt(contactsModels.get(i).getPhones().get(indexJ).getId()), contactsModels.get(i).getPhones().get(indexJ).getPhoneNumber(),
                                                "01-01-2017", "01-01-2017");

                                    }
                                }
//
                                for (int indexJ1 = 0; indexJ1 < contactsModels.get(i).getEmails().size(); indexJ1++) {
                                    if (EmailTable.getInstance().isEmailAvailable(contactsModels.get(i).getEmails().get(indexJ1).getId())) {
                                        EmailTable.getInstance().updateEmail(Integer.parseInt(contactsModels.get(i).getEmails().get(indexJ1).getId()),
                                                contactsModels.get(i).getEmails().get(indexJ1).getAddress(), "");
                                    } else {
                                        EmailTable.getInstance().write(Integer.parseInt(contactsModels.get(i).getEmails().get(indexJ1).getContactId()),
                                                Integer.parseInt(contactsModels.get(i).getEmails().get(indexJ1).getId()), contactsModels.get(i).getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");

                                    }
                                }

                            } else {
                                ContatctTable.getInstance().write(Integer.parseInt(contactsModels.get(i).getId()), contactsModels.get(i).getFirstName(),
                                        contactsModels.get(i).getLastName(), contactsModels.get(i).getCompanyName(), phone, email_address, 1,
                                        contactsModels.get(i).getGroup_id(), contactsModels.get(i).getGroup_name(), contactsModels.get(i).getDateOfBirthStr(),
                                        contactsModels.get(i).getImageUrl(), "01-01-2017", "01-01-2017");
                                for (int indexJ = 0; indexJ < contactsModels.get(i).getPhones().size(); indexJ++) {
                                    PhoneTable.getInstance().write(Integer.parseInt(contactsModels.get(i).getPhones().get(indexJ).getContactId()),
                                            Integer.parseInt(contactsModels.get(i).getPhones().get(indexJ).getId()),
                                            contactsModels.get(i).getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");

                                }
                                for (int indexJ1 = 0; indexJ1 < contactsModels.get(i).getEmails().size(); indexJ1++) {
                                    EmailTable.getInstance().write(Integer.parseInt(contactsModels.get(i).getEmails().get(indexJ1).getContactId()),
                                            Integer.parseInt(contactsModels.get(i).getEmails().get(indexJ1).getId()),
                                            contactsModels.get(i).getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");

                                }


                            }
                            if (edit_data.equalsIgnoreCase("")) {
                                SM.sendData(contactsModels, 1);

                            } else {
                                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                                intent.putExtra("contact_id", contactsModels.get(0).getId());
                                startActivity(intent);
                                getActivity().finish();
                            }


                        } else {
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_GET_TERMS_DATA)) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        allGroupsModels = new ArrayList<>();
                        JSONArray projectsArray = jsonObject.getJSONArray("Result");
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            AllGroupsModel allGroupsModel = new AllGroupsModel();
                            allGroupsModel.setId(jsonObject1.getString("Id"));
                            allGroupsModel.setName(jsonObject1.getString("name"));
                            allGroupsModel.setDescription(jsonObject1.getString("Description"));
                            if (jsonObject1.getString("TaxonomyId").equalsIgnoreCase("2")) {

                                allGroupsModels.add(allGroupsModel);
                            } else if (jsonObject1.getString("TaxonomyId").equalsIgnoreCase("8")) {
                                religionNames.add(allGroupsModel);

                            }

                        }

                        if (!religionNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, religionNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            religion.setAdapter(aa);

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static String encodeImage(Bitmap thumbnail) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    @Override
    public void onTextChange(ArrayList<HashMap<Integer, ContactsModel.Phones>> arrayList) {
        this.arrayList = arrayList;
        for (int i = 0; i < arrayList.size(); i++) {

            HashMap<Integer, ContactsModel.Phones> hashMap = arrayList.get(i);
            ContactsModel.Phones phones = hashMap.get(i);
            if (phones == null) {

            } else {
                Log.e("Helo", " value== " + phones.getPhoneNumber());
            }

        }
    }

    @Override
    public void onTextChange1(ArrayList<HashMap<Integer, ContactsModel.Email>> emailArrayList) {
        this.emailarrayList = emailArrayList;
        for (int i = 0; i < emailarrayList.size(); i++) {

            HashMap<Integer, ContactsModel.Email> hashMap = emailarrayList.get(i);
            ContactsModel.Email email = hashMap.get(i);
            if (email == null) {

            } else {
                Log.e("Heloo", " value== " + email.getAddress());
            }

        }
    }

    public boolean isValid() {

        if (first_name.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please enter first name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (last_name.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please enter first name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (first_name.getText().toString().equalsIgnoreCase("None")) {
            Toast.makeText(getActivity(), "Enter valid name", Toast.LENGTH_SHORT).show();
            return false;
        }else if (last_name.getText().toString().equalsIgnoreCase("None")) {
            Toast.makeText(getActivity(), "Enter valid name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!website.getText().toString().equalsIgnoreCase("") && !Patterns.WEB_URL.matcher(website.getText().toString()).matches()) {
            Toast.makeText(getActivity(), getString(R.string.enter_valid_website), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public boolean isEmailValid() {

        boolean isEmailValid = false;
        if (EmailAdapter.isEmailValid != null) {
            if (EmailAdapter.isEmailValid.equalsIgnoreCase("true") || EmailAdapter.isEmailValid.equalsIgnoreCase("")) {

                isEmailValid = true;
                return isEmailValid;

            } else {

                isEmailValid = false;
                return isEmailValid;

            }

        } else {
            isEmailValid = false;
            return isEmailValid;
        }

    }

    public class RelationAdapter extends RecyclerView.Adapter<RelationAdapter.ViewHolder> {

        Context context;


        public RelationAdapter(FragmentActivity activity) {
            this.context = activity;
        }

        public RelationAdapter(Context context, ArrayList<ContactsModel.ContactRelations> relationLists) {
            this.context = context;
            relationList = relationLists;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.more_relation_layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.relation_first_name.setText(relationList.get(position).getRelationFirstName());
            holder.relation_last_name.setText(relationList.get(position).getRelationLastName());
            String[] relationArray = getResources().getStringArray(R.array.Relation);

            if (!relationList.get(position).getRelation().equalsIgnoreCase("") || relationList.get(position).getRelation() != null) {
                for (int i = 0; i < relationArray.length; i++) {

                    if (relationArray[i].equalsIgnoreCase(relationList.get(position).getRelation())) {
                        holder.relation_spinner.setSelection(i);
                    }
                }
            }

            holder.relation_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    int pos = position;
                    final ContactsModel.ContactRelations relation1 = new ContactsModel().new ContactRelations();
                    relation1.setRelationFirstName(holder.relation_first_name.getText().toString());
                    relation1.setRelationLastName(holder.relation_last_name.getText().toString());
                    relation1.setRelation(holder.relation_spinner.getSelectedItem().toString());
                    if (relationList.get(pos).getId() == 0) {
                        relation1.setId(0);
                    } else {
                        relation1.setId(relationList.get(pos).getId());
                    }

                    relationList.set(pos, relation1);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            new MultiTextWatcher()
                    .registerEditText(holder.relation_first_name)
                    .registerEditText(holder.relation_last_name)
                    .setCallback(new MultiTextWatcher.TextWatcherWithInstance() {
                        @Override
                        public void beforeTextChanged(EditText editText, CharSequence s, int start, int count, int after) {
                            // TODO: Do some thing with editText
                        }

                        @Override
                        public void onTextChanged(EditText editText, CharSequence s, int start, int before, int count) {
                            // TODO: Do some thing with editText
                        }

                        @Override
                        public void afterTextChanged(EditText editText, Editable editable) {
                            // TODO: Do some thing with editText

                            int pos = position;
                            final ContactsModel.ContactRelations relation1 = new ContactsModel().new ContactRelations();
                            relation1.setRelationFirstName(holder.relation_first_name.getText().toString());
                            relation1.setRelationLastName(holder.relation_last_name.getText().toString());
                            relation1.setRelation(holder.relation_spinner.getSelectedItem().toString());
                            if (relationList.get(pos).getId() == 0) {
                                relation1.setId(0);
                            } else {
                                relation1.setId(relationList.get(pos).getId());
                            }

                            relationList.set(pos, relation1);


                        }
                    });


        }

        @Override
        public int getItemCount() {
            return relationList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            Spinner relation_spinner;
            EditText relation_first_name, relation_last_name;

            public ViewHolder(View item) {
                super(item);
                relation_spinner = (Spinner) item.findViewById(R.id.relation_spinner);
                relation_first_name = (EditText) item.findViewById(R.id.relation_first_name);
                relation_last_name = (EditText) item.findViewById(R.id.relation_last_name);
            }
        }
    }

}