package com.bnhabitat.ui.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.sizeunit.UnitsTable;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.CategoryTypeModel;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.ui.activities.ProjectDetailActivity;
import com.bnhabitat.ui.adapters.AdapterCommonDialog;
import com.bnhabitat.ui.adapters.AmentiesAdpater;
import com.bnhabitat.ui.adapters.LayoutPagerAdapter;
import com.bnhabitat.ui.adapters.SpecialFeaturesAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeaturesFragment extends Fragment implements View.OnClickListener ,TabChangeListener {
    TextView amentiesLayout;
    TextView special_feautureLayout;
    TextView specificationLayout;
    LinearLayoutManager linearLayoutManager,spcial_linearLayoutManager;
    LinearLayout amenties_view;
    LinearLayout view_special_feauture;
    LinearLayout view_specification;
    AmentiesAdpater amentiesAdapter;
    SpecialFeaturesAdapter specialFeaturesAdapter;
    ImageView amentiesDownArrow;
    ImageView special_feautureArrow;
    ImageView specificationDownArrow;
    int amenties_count=0;
    int special_count=0;
    int prefrenceId ;
    int specification_count=0;
//    ScrollView detailParentscroll;
    RecyclerView amenties_list,special_feauture_list;
    ArrayList<ProjectsDetailModel.Amenities> amenitiesArrayList=new ArrayList<>();
    ImageView info_icon,share_icon,notification_icon,user_icon;
    ProjectsDetailModel projectsDetailModel;
    ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList=new ArrayList<>();
    ArrayList<ProjectsModel> projectListingModelArrayList=new ArrayList<>();
    int position;
    private boolean isDataSet = false;
    private ArrayList<CommonDialogModel> propertyTypeModel = new ArrayList<>();
    private TextView unitTxt;
    private TextView noSpecsTxtVw;
    private TextView extraAreaTxtVw;
    private TextView carpetAreaTxtVw;
    private TextView builtUpAreaTxtVw;
    private TextView superAreaTxtVw;
    private TextView superAreaTitleTxtVw;
    private TextView builtUpAreaTitleTxtVw;
    private TextView carpetAreaTitleTxtVw;
    private TextView extraAreaTitleTxtVw;
    private TextView planTitleTxt;
    private TextView extraAreaUnitTxtVw;
    private TextView carpetAreaUnitTxtVw;
    private TextView builtUpUnitTxtVw;
    private TextView superAreaUnitTxtVw;
    private ImageView specsDownArrow;
    private RelativeLayout specsDPLayout;
    private ArrayList<ProjectsDetailModel.PropertySize.LayoutPlans> layoutPlanses = new ArrayList<>();

    private LinearLayout layoutPlanParentLayout;
    private CardView layoutPlanCardView;
    private LinearLayout unitsLayout;
    private LinearLayout messageLayout;
    private LinearLayout superAreaLayout;
    private LinearLayout builtAreaLayout;
    private LinearLayout carpetAreaLayout;
    private LinearLayout extraAreaLayout;
    private LinearLayout specification_lay;
    private LinearLayout specificationLayout_1;
    private View view;
    private boolean isSpecsOpen=true;
    private ViewPager layoutPlanPager;
    private ImageView rightIcon;
    private ImageView leftIcon;

    private TextView specification_bredth;
    private TextView specification_lenght;

    ArrayList<String> plot_dimesion_unit_names=new ArrayList<>();
    String plotSizeUnitId,plotSizeUnitName;

    public FeaturesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_features, container, false);
        Bundle bundle=this.getArguments();
        if(bundle!=null)
            projectsDetailModel=(ProjectsDetailModel)bundle.getSerializable("projectModel");
        position=bundle.getInt("position");
        Log.e("position", String.valueOf(position));
        projectListingModelArrayList= (ArrayList<ProjectsModel>) bundle.getSerializable("notification_array");
        view_specification=(LinearLayout) view.findViewById(R.id.view_specification);
        view_special_feauture=(LinearLayout) view.findViewById(R.id.view_special_feauture);
        amenties_view=(LinearLayout) view.findViewById(R.id.amenties_view);

        amenties_list=(RecyclerView) view.findViewById(R.id.amenties_list);
        special_feauture_list=(RecyclerView) view.findViewById(R.id.special_feauture_list);
        linearLayoutManager=new LinearLayoutManager(getActivity());
        spcial_linearLayoutManager=new LinearLayoutManager(getActivity());

        amentiesLayout=(TextView)view.findViewById(R.id.amentiesLayout);

        special_feautureLayout=(TextView)view.findViewById(R.id.special_feautureLayout);
        specificationLayout=(TextView)view.findViewById(R.id.specificationLayout);
        info_icon=(ImageView)view.findViewById(R.id.info_icon);
        share_icon=(ImageView)view.findViewById(R.id.share_icon);
        notification_icon=(ImageView)view.findViewById(R.id.notification_icon);
        user_icon=(ImageView)view.findViewById(R.id.user_icon);
        initComponents(view);


        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                projectRelationshipManagersArrayList=projectsDetailModel.getProjectRelationshipManagers();
                Log.e("project_array", String.valueOf(projectRelationshipManagersArrayList.size()));
                if(!projectRelationshipManagersArrayList.isEmpty()){
                    CustomDialogFragment fragment = CustomDialogFragment.newInstance();
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("Arraylist", projectRelationshipManagersArrayList);
                    fragment.setArguments(bundle);

                    fragment.show(((ProjectDetailActivity)getActivity()).getSupportFragmentManager(), "dialog_fragment");
                }
                else{
                    Toast.makeText(getActivity(), "No RM associated with the project.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationDialogProjectDetail notificationDialogFragment = NotificationDialogProjectDetail.newInstance();

                Bundle bundle = new Bundle();
//                bundle.putSerializable("Arraylist", projectListingModelArrayList);
//                bundle.putSerializable("propertySizes", propertySizes);
//                bundle.putSerializable("towers", projectsDetailModel.getTowers());
                  bundle.putSerializable("projectModel", projectsDetailModel);
//                bundle.putSerializable("status", "1");
//                bundle.putInt("Position", position);
                 notificationDialogFragment.setArguments(bundle);

                 notificationDialogFragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "notification_dialog_fragment");

            }
        });
        share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, projectsDetailModel.getTitle());

                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://bnhabitat.com/?"+"p="+projectsDetailModel.getId());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(projectsDetailModel.getCurrentOffers().equalsIgnoreCase("null")||projectsDetailModel.getCurrentOffers().equalsIgnoreCase("")
                        ||projectsDetailModel.getCurrentOffers()==null){
                    Toast.makeText(getActivity(), "No offers yet", Toast.LENGTH_SHORT).show();
                }
                else{
                    new AlertDialog.Builder(getActivity())
                            .setMessage(projectsDetailModel.getCurrentOffers())
                            .setPositiveButton("NOTED", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();
                }
            }
        });
        amentiesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_special_feauture.setVisibility(View.GONE);
                amenties_view.setVisibility(View.VISIBLE);
                view_specification.setVisibility(View.GONE);
                special_feautureLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                amentiesLayout.setBackgroundColor(getResources().getColor(R.color.blue));
                specificationLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));

            }
        });
        special_feautureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    view_special_feauture.setVisibility(View.VISIBLE);
                    amenties_view.setVisibility(View.GONE);
                    view_specification.setVisibility(View.GONE);
                special_feautureLayout.setBackgroundColor(getResources().getColor(R.color.blue));
                amentiesLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                specificationLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));



            }
        });
        specificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view_special_feauture.setVisibility(View.GONE);
                amenties_view.setVisibility(View.GONE);
                view_specification.setVisibility(View.VISIBLE);
                special_feautureLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                amentiesLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                specificationLayout.setBackgroundColor(getResources().getColor(R.color.blue));

            }
        });

        if(projectsDetailModel.getSpecialFeatures().isEmpty()){

            special_feautureLayout.setVisibility(View.GONE);
        }

        if(projectsDetailModel.getAmenities().isEmpty()){

            amentiesLayout.setVisibility(View.GONE);
        }

        amentiesAdapter=new AmentiesAdpater(getActivity(),projectsDetailModel.getAmenities());
        specialFeaturesAdapter=new SpecialFeaturesAdapter(getActivity(),projectsDetailModel.getSpecialFeatures());

        amenties_list.setLayoutManager(linearLayoutManager);
        amenties_list.setAdapter(amentiesAdapter);
        special_feauture_list.setLayoutManager(spcial_linearLayoutManager);
        special_feauture_list.setAdapter(specialFeaturesAdapter);
        setData();
        return view;
    }
    private void initComponents(View view) {

        layoutPlanPager = (ViewPager) view.findViewById(R.id.layoutPlanPager);
        superAreaLayout = (LinearLayout) view.findViewById(R.id.superAreaLayout);
        builtAreaLayout = (LinearLayout) view.findViewById(R.id.builtAreaLayout);
        carpetAreaLayout = (LinearLayout) view.findViewById(R.id.carpetAreaLayout);
        extraAreaLayout = (LinearLayout) view.findViewById(R.id.extraAreaLayout);
        specification_lay = (LinearLayout) view.findViewById(R.id.specification_lay);
        specificationLayout_1 = (LinearLayout) view.findViewById(R.id.specificationLayout_1);
        layoutPlanParentLayout = (LinearLayout) view.findViewById(R.id.layoutPlanParentLayout);
        layoutPlanCardView = (CardView) view.findViewById(R.id.layoutPlanCardView);
        unitTxt = (TextView) view.findViewById(R.id.unitTxt);
        noSpecsTxtVw = (TextView) view.findViewById(R.id.noSpecsTxtVw);
        planTitleTxt = (TextView) view.findViewById(R.id.planTitleTxt);
        extraAreaTxtVw = (TextView) view.findViewById(R.id.extraAreaTxtVw);
        carpetAreaTxtVw = (TextView) view.findViewById(R.id.carpetAreaTxtVw);
        builtUpAreaTxtVw = (TextView) view.findViewById(R.id.builtUpAreaTxtVw);
        superAreaTxtVw = (TextView) view.findViewById(R.id.superAreaTxtVw);
        extraAreaUnitTxtVw = (TextView) view.findViewById(R.id.extraAreaUnitTxtVw);
        carpetAreaUnitTxtVw = (TextView) view.findViewById(R.id.carpetAreaUnitTxtVw);
        builtUpUnitTxtVw = (TextView) view.findViewById(R.id.builtUpUnitTxtVw);
        superAreaUnitTxtVw = (TextView) view.findViewById(R.id.superAreaUnitTxtVw);
        specification_lenght = (TextView) view.findViewById(R.id.specification_lenght);
        specification_bredth = (TextView) view.findViewById(R.id.specification_bredth);
        specsDownArrow= (ImageView) view.findViewById(R.id.specsDownArrow);
        specsDPLayout= (RelativeLayout) view.findViewById(R.id.specsDPLayout);
        superAreaTitleTxtVw = (TextView) view.findViewById(R.id.superAreaTitleTxtVw);
        builtUpAreaTitleTxtVw = (TextView) view.findViewById(R.id.builtUpAreaTitleTxtVw);
        carpetAreaTitleTxtVw = (TextView) view.findViewById(R.id.carpetAreaTitleTxtVw);
        extraAreaTitleTxtVw = (TextView) view.findViewById(R.id.extraAreaTitleTxtVw);
        rightIcon = (ImageView) view.findViewById(R.id.rightIcon);
        leftIcon = (ImageView) view.findViewById(R.id.leftIcon);
        unitsLayout = (LinearLayout) view.findViewById(R.id.unitsLayout);
        messageLayout=(LinearLayout) view.findViewById(R.id.messageLayout);
//      detailParentscroll=(ScrollView) view.findViewById(R.id.detailParentLayout);
        unitsLayout.setOnClickListener(this);
        specsDPLayout.setOnClickListener(this);


        layoutPlanPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                leftIcon.setVisibility(View.VISIBLE);
                rightIcon.setVisibility(View.VISIBLE);

                if (position == 0)
                    leftIcon.setVisibility(View.GONE);

                if (position == (layoutPlanses.size() - 1))
                    rightIcon.setVisibility(View.GONE);

                if (layoutPlanses.size() > 0)
                    planTitleTxt.setText(layoutPlanses.get(position).getTitle());
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.unitsLayout:

                showCommonDialog(getActivity(), propertyTypeModel, 0, 0);

                break;

            case R.id.specsDPLayout:


                if (isSpecsOpen) {
                    specificationLayout_1.setVisibility(View.GONE);
                    specsDownArrow.setImageResource(R.drawable.down_arrow);
                    isSpecsOpen = false;
                } else {
                    specificationLayout_1.setVisibility(View.VISIBLE);
                    specsDownArrow.setImageResource(R.drawable.up_arrow);
                    isSpecsOpen = true;
                }


                break;
//            case R.id.view_specification:
//
//
////                if (isSpecsOpen) {
////                    specificationLayout.setVisibility(View.GONE);
////                    specsDownArrow.setImageResource(R.drawable.down_arrow);
////                    isSpecsOpen = false;
////                } else {
////                    specificationLayout.setVisibility(View.VISIBLE);
////                    specsDownArrow.setImageResource(R.drawable.up_arrow);
////                    isSpecsOpen = true;
////                }
//
//
//                break;
        }
    }
    public void showCommonDialog(Context context,
                                 final ArrayList<CommonDialogModel> commonDialogModels,
                                 final int from, final int clickedPosition) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_common);
        dialog.show();


        ListView listView = (ListView) dialog.findViewById(R.id.listView);
        Button registerBtn = (Button) dialog.findViewById(R.id.registerBtn);


        listView.setAdapter(new AdapterCommonDialog(getActivity(), commonDialogModels));


        listView.setItemsCanFocus(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {


                if (from == 0) {


                    unitTxt.setText(propertyTypeModel.get(arg2).getTitle());
                    setSpecsLayout(arg2);

                }
                dialog.cancel();
            }
        });
    }
    private void setSpecsLayout(int size) {


        String areaName = "";
        specificationLayout_1.removeAllViews();
        layoutPlanses.clear();

        ProjectsDetailModel.PropertySize propertySize = projectsDetailModel.getPropertySizes().get(size);

        String sizeUnitName = UnitsTable.getInstance().getUnitName(propertySize.getSizeUnit());

        if (propertySize.getSpecifications().size() == 0)
            noSpecsTxtVw.setVisibility(View.VISIBLE);
        else
            noSpecsTxtVw.setVisibility(View.GONE);


        for (ProjectsDetailModel.PropertySize.LayoutPlans layoutPlans : projectsDetailModel.getPropertySizes().get(size).getLayoutPlans()) {

            ProjectsDetailModel.PropertySize.LayoutPlans plans = propertySize.new LayoutPlans();
            plans.setType(layoutPlans.getType());
            plans.setBigImage(layoutPlans.getBigImage());
            plans.setDescription(layoutPlans.getDescription());
            plans.setTitle(layoutPlans.getTitle());
            layoutPlanses.add(plans);
        }


        for (ProjectsDetailModel.PropertySize.Specifications specifications : projectsDetailModel.getPropertySizes().get(size).getSpecifications()) {

            View view;
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.specifications_items, null);

            TextView titleTxtVw = (TextView) view.findViewById(R.id.titleTxtVw);
            TextView descriptionTxtVw = (TextView) view.findViewById(R.id.descriptionTxtVw);

            titleTxtVw.setText(specifications.getTitle());
            descriptionTxtVw.setText(specifications.getDescription());

            if (!areaName.equalsIgnoreCase(specifications.getAreaName())) {

                LayoutInflater inflaterArea = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View viewArea = inflaterArea.inflate(R.layout.specs_header_layout, null);
                TextView areaNameTxtVw = (TextView) viewArea.findViewById(R.id.areaNameTxtVw);
                areaNameTxtVw.setText(specifications.getAreaName());
                areaName = specifications.getAreaName();
                specificationLayout_1.addView(viewArea);
            }


            specificationLayout_1.addView(view);


        }


        if (Utils.getFilteredValue(propertySize.getSize()).equalsIgnoreCase("NA")||propertySize.getSize().equalsIgnoreCase("0"))
            superAreaLayout.setVisibility(View.GONE);

        if (Utils.getFilteredValue(propertySize.getBuiltarea()).equalsIgnoreCase("NA")||propertySize.getBuiltarea().equalsIgnoreCase("0"))
            builtAreaLayout.setVisibility(View.GONE);

        if (Utils.getFilteredValue(propertySize.getExtraArea()).equalsIgnoreCase("NA")||propertySize.getExtraArea().equalsIgnoreCase("0"))
            extraAreaLayout.setVisibility(View.GONE);

        if (Utils.getFilteredValue(propertySize.getCarpetarea()).equalsIgnoreCase("NA")||propertySize.getCarpetarea().equalsIgnoreCase("0"))
            carpetAreaLayout.setVisibility(View.GONE);

//        int prefrenceId=Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_SIZE_UNIT_ID,""));
//        int DimensionSizeId=propertySize.getSizeUnit3();
//        if(DimensionSizeId!= Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_SIZE_UNIT_ID,""))){
//            String unitName=PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_SIZE_UNIT_NAME,"");
//
//            if(prefrenceId==66){
//                int breadth= Integer.parseInt(propertySize.getBreadth());
//                int lenght= Integer.parseInt(propertySize.getLength());
//
//                specification_bredth.setText(Utils.getFilteredValue((breadth/3)+" "+unitName));
//                specification_lenght.setText(Utils.getFilteredValue(lenght/3+" "+unitName));
//
//            }
//            else if(prefrenceId==67){
//                int breadth= Integer.parseInt(propertySize.getBreadth());
//                int lenght= Integer.parseInt(propertySize.getLength());
//
//                specification_bredth.setText(Utils.getFilteredValue((breadth*3)+" "+unitName));
//                specification_lenght.setText(Utils.getFilteredValue(lenght*3+" "+unitName));
//            }
//
//
//        }
//        else{


        if(!propertySize.getBreadth().equalsIgnoreCase("")&&!propertySize.getLength().equalsIgnoreCase("")){

            specification_lay.setVisibility(View.VISIBLE);
            String unitName;
            int DimensionSizeId=propertySize.getSizeUnit3();
            if(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_DIMENSION_UNIT_NAME,"").equalsIgnoreCase("")){

                unitName= UnitsTable.getInstance().getUnitName(DimensionSizeId);

            }
            else{

                unitName= PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_DIMENSION_UNIT_NAME,"");

            }


            if(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_DIMENSION_UNIT_ID,"").equalsIgnoreCase("")) {
                DimensionSizeId=prefrenceId;
            }else {
                prefrenceId = Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_DIMENSION_UNIT_ID, ""));
            }    if(DimensionSizeId==prefrenceId){
                specification_bredth.setText(Utils.getFilteredValue(propertySize.getBreadth()+" "+unitName));
                specification_lenght.setText(Utils.getFilteredValue(propertySize.getLength()+" "+unitName));
            }
            else{

                if(prefrenceId==66){
                    int breadth= Integer.parseInt(propertySize.getBreadth());
                    int lenght= Integer.parseInt(propertySize.getLength());

                    specification_bredth.setText(Utils.getFilteredValue((breadth/3)+" "+unitName));
                    specification_lenght.setText(Utils.getFilteredValue(lenght/3+" "+unitName));

                }
                else if(prefrenceId==67){
                    int breadth= Integer.parseInt(propertySize.getBreadth());
                    int lenght= Integer.parseInt(propertySize.getLength());

                    specification_bredth.setText(Utils.getFilteredValue((breadth*3)+" "+unitName));
                    specification_lenght.setText(Utils.getFilteredValue(lenght*3+" "+unitName));
                }
            }
        }
        else{
            specification_lay.setVisibility(View.GONE);
        }




//        }
        extraAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getExtraArea()));
        carpetAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getCarpetarea()));
        builtUpAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getBuiltarea()));
        superAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getSize()));

        extraAreaUnitTxtVw.setText(sizeUnitName);
        carpetAreaUnitTxtVw.setText(sizeUnitName);
        builtUpUnitTxtVw.setText(sizeUnitName);
        superAreaUnitTxtVw.setText(sizeUnitName);

        CategoryTypeModel categoryTypeModel =new CategoryTypeModel();
//        categoryTypeModel.setArea1(String.valueOf(propertySize.getProperty_typeIdlevel2()));
        Log.d("",""+propertySize.getProperty_typeIdlevel2());
//       if(propertySize.getSize()==null){
//           superAreaTitleTxtVw.setText
//       }
        superAreaTitleTxtVw.setText(categoryTypeModel.getArea1());
        carpetAreaTitleTxtVw.setText(categoryTypeModel.getArea2());
        builtUpAreaTitleTxtVw.setText(categoryTypeModel.getArea3());
        extraAreaTitleTxtVw.setText(categoryTypeModel.getArea4());


        if (layoutPlanses.size() > 0) {
            layoutPlanPager.setAdapter(new LayoutPagerAdapter(getActivity(), layoutPlanses));
            layoutPlanParentLayout.setVisibility(View.VISIBLE);
            layoutPlanCardView.setVisibility(View.VISIBLE);
        } else {
            layoutPlanParentLayout.setVisibility(View.GONE);
            layoutPlanCardView.setVisibility(View.GONE);
        }
    }

    public void setData() {

        messageLayout.setVisibility(View.GONE);
//        detailParentscroll.setVisibility(View.VISIBLE);

        if (null != projectsDetailModel) {
            if (!isDataSet) {

                propertyTypeModel.clear();


                for (ProjectsDetailModel.PropertySize propertySizes : projectsDetailModel.getPropertySizes()) {
                    CommonDialogModel commonDialogModel = new CommonDialogModel();
                    commonDialogModel.setId(String.valueOf(propertySizes.getId()));
                    commonDialogModel.setTitle(propertySizes.getTitle());
                    if (null != propertySizes.getSize())
                    commonDialogModel.setAreaSize(Double.parseDouble(propertySizes.getSize()));
                    else
                        commonDialogModel.setAreaSize(0);

                    if (null != String.valueOf(propertySizes.getProperty_typeIdlevel2()))
                        commonDialogModel.setPropertyTypeId(propertySizes.getProperty_typeIdlevel2());
                    else
                        commonDialogModel.setPropertyTypeId(0);
//                    commonDialogModel.setPropertyTypeId(propertySizes.getProperty_typeIdlevel2());

                    if (null != propertySizes.getCarpetarea())
                        commonDialogModel.setCarpetArea(propertySizes.getCarpetarea());
                    else
                        commonDialogModel.setCarpetArea("");
//
                    if (null != propertySizes.getBuiltarea())
                        commonDialogModel.setBuiltupArea(propertySizes.getBuiltarea());
                    else
                        commonDialogModel.setBuiltupArea("");

                    if (null != propertySizes.getSize())
                        commonDialogModel.setSuperArea(propertySizes.getSize());
                    else
                        commonDialogModel.setSuperArea("");


                    propertyTypeModel.add(commonDialogModel);
                }





                if (propertyTypeModel.size() > 0) {
                    unitTxt.setText(propertyTypeModel.get(0).getTitle());
                    setSpecsLayout(0);
                    isDataSet = true;
                }
            }
        } else {
            messageLayout.setVisibility(View.VISIBLE);
//            detailParentscroll.setVisibility(View.GONE);
//
        }
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
