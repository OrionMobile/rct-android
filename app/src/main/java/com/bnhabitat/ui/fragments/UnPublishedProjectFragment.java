package com.bnhabitat.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.ui.adapters.UnPublishedProjectAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class UnPublishedProjectFragment extends Fragment implements CommonAsync.OnAsyncResultListener,TabChangeListener {
    RecyclerView unpublished_list;
    LinearLayoutManager linearLayoutManager;
    private ArrayList<ProjectsModel> projectsModels = new ArrayList();
    EditText search_city;
    TextView no_project;

    UnPublishedProjectAdapter unPublishedProjectAdapter;

    public UnPublishedProjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_un_published_project, container, false);

        unpublished_list = (RecyclerView) view.findViewById(R.id.unpublished_list);
        search_city = (EditText) view.findViewById(R.id.search_city);
        no_project = (TextView) view.findViewById(R.id.no_project);
        linearLayoutManager = new LinearLayoutManager(getActivity());

//


        search_city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

//
                if(unPublishedProjectAdapter!=null)
                unPublishedProjectAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void getUnpublishProjectList() {
        new CommonAsync(getActivity(), Urls.URL + Urls.URL_PUBLISH_PROJECT_LIST + Constants.APP_USER_ID + "/" + Constants.APP_ID + "/false",
                "",
                "Loading..",
                this,
                Urls.URL_PUBLISH_PROJECT_LIST,
                Constants.GET).execute();
    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    @Override
    public void onResultListener(String result, String which) {
        if(which.equalsIgnoreCase("time_out")){
            no_project.setVisibility(View.VISIBLE);
            unpublished_list.setVisibility(View.GONE);
        }



        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_PUBLISH_PROJECT_LIST)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {
                        no_project.setVisibility(View.GONE);
                        unpublished_list.setVisibility(View.VISIBLE);
                        JSONArray projectsArray = jsonObject.getJSONArray("Result");

                        if (projectsArray.length() == 0) {
                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                        }

                        projectsModels.clear();

                        for (int index = 0; index < projectsArray.length(); index++) {
                            ProjectsModel projectsModel = new ProjectsModel();

                            projectsModel.setId(projectsArray.getJSONObject(index).getString("Id"));
                            projectsModel.setTitle(projectsArray.getJSONObject(index).getString("title"));
                            projectsModel.setLocality(projectsArray.getJSONObject(index).getString("Locality"));
                            projectsModel.setUpdatedOn(projectsArray.getJSONObject(index).getString("UpdatedOn"));
                            projectsModel.setArea(projectsArray.getJSONObject(index).getString("Area"));
                            projectsModel.setNumberOfTowers(projectsArray.getJSONObject(index).getString("NumberOfTowers"));
                            projectsModel.setAboutSubProject(projectsArray.getJSONObject(index).getString("aboutSubProject"));
                            projectsModel.setPossessionMonth(projectsArray.getJSONObject(index).getString("PossessionMonth"));
                            projectsModel.setPossessionYear(projectsArray.getJSONObject(index).getString("PossessionYear"));

                            projectsModel.setProjectStatus(projectsArray.getJSONObject(index).getString("ProjectStatus"));
                            projectsModel.setRegion(projectsArray.getJSONObject(index).getString("Region"));
                            projectsModel.setImagePath(projectsArray.getJSONObject(index).getString("logoimage"));
                            projectsModel.setTopImage1(projectsArray.getJSONObject(index).getString("topImage1"));
                            projectsModel.setTypeOfProject(projectsArray.getJSONObject(index).getString("TypeOfProject"));
                            projectsModel.setCompanyLogo(projectsArray.getJSONObject(index).getString("CompanyLogo"));
                            projectsModel.setCityName(projectsArray.getJSONObject(index).getString("CityName"));
                            projectsModel.setBuilderCompanyName(projectsArray.getJSONObject(index).getString("builderCompanyName"));


                            JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("PropertySize");
                            ArrayList<ProjectsModel.PropertySize> propertySizes = new ArrayList();
                            StringBuilder propertyStringBuilder = new StringBuilder();
                            String propertyString = "";
                            long minPrice = 10000000000l;
                            long maxPrice = 0;

                            for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                                ProjectsModel.PropertySize propertySize = projectsModel.new PropertySize();
                                JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                                propertySize.setId(dataObj.getString("Id"));
                                propertySize.setTitle(dataObj.getString("title"));
                                propertySize.setApplicationForm(dataObj.getString("applicationForm"));
                                propertySize.setProperty_typeIdlevel1(dataObj.getString("property_typeIdlevel1"));
                                propertySize.setProperty_typeIdlevel2(dataObj.getString("property_typeIdlevel2"));
                                propertySize.setProperty_typeIdlevel3(dataObj.getString("property_typeIdlevel3"));
                                propertySize.setFloorPlan(dataObj.getString("floorPlan"));
                                propertySize.setSize(dataObj.getString("size"));
                                propertySize.setSizeUnit(dataObj.getString("sizeUnit"));
                                propertySize.setBuiltarea(dataObj.getString("builtarea"));
                                propertySize.setCarpetarea(dataObj.getString("carpetarea"));
                                propertySize.setSizeUnit2(dataObj.getString("sizeUnit2"));
                                propertySize.setAboutProperty(dataObj.getString("AboutProperty"));
                                propertySize.setLength(dataObj.getString("Length"));
                                propertySize.setBreadth(dataObj.getString("Breadth"));
                                propertySize.setSizeUnit3(dataObj.getString("SizeUnit3"));
                                propertySize.setExtraArea(dataObj.getString("ExtraArea"));
                                if (minPrice > dataObj.getLong("MinimumPrice"))
                                    minPrice = dataObj.getLong("MinimumPrice");

                                if (maxPrice < dataObj.getLong("MaximumPrice"))
                                    maxPrice = dataObj.getLong("MaximumPrice");
                                propertySize.setMinimumPrice(String.valueOf(minPrice));
                                propertySize.setMaximumPrice(String.valueOf(maxPrice));
                                propertySize.setBathRooms(dataObj.getString("BathRooms"));
                                propertySize.setBedRooms(dataObj.getString("BedRooms"));
                                propertySize.setTowerId(dataObj.getString("TowerId"));
                                propertySize.setIsCostAppliedCarpetArea(dataObj.getString("isCostAppliedCarpetArea"));
                                propertySize.setExtraAreaLabel(dataObj.getString("ExtraAreaLabel"));
                                propertySize.setDiscounts(dataObj.getString("Discounts"));
                                propertySize.setLayoutPlans(dataObj.getString("LayoutPlans"));
                                propertySize.setDiscountGroups(dataObj.getString("DiscountGroups"));
                                propertySize.setTowers(dataObj.getString("Towers"));


                                propertySizes.add(propertySize);


                            }


                            JSONArray projectRelationshipManagerses = projectsArray.getJSONObject(index).getJSONArray("BrokerStaff");
                            ArrayList<ProjectsModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList();
                            if (projectRelationshipManagerses.length() != 0) {
                                for (int indexJ = 0; indexJ < projectRelationshipManagerses.length(); indexJ++) {

                                    ProjectsModel.ProjectRelationshipManagers projectRelationshipManagers = projectsModel.new ProjectRelationshipManagers();
                                    JSONObject dataObj = projectRelationshipManagerses.getJSONObject(indexJ);
                                    projectRelationshipManagers.setId(dataObj.getString("Id"));
                                    projectRelationshipManagers.setName(dataObj.getString("Name"));
                                    projectRelationshipManagers.setEmail(dataObj.getString("Email"));
                                    projectRelationshipManagers.setPhoto(dataObj.getString("Photo"));
                                    projectRelationshipManagers.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                    projectRelationshipManagers.setDesignation(dataObj.getString("Designation"));

                                    projectRelationshipManagersArrayList.add(projectRelationshipManagers);
                                }
                            }

                            JSONArray towersJson = projectsArray.getJSONObject(index).getJSONArray("Towers");
                            ArrayList<ProjectsModel.Towers> towerses = new ArrayList();
                            if (towersJson.length() != 0) {
                                for (int indexJ = 0; indexJ < towersJson.length(); indexJ++) {

                                    ProjectsModel.Towers towersProjectModel = projectsModel.new Towers();
                                    JSONObject dataObj = towersJson.getJSONObject(indexJ);
                                    towersProjectModel.setTowerId(dataObj.getString("TowerId"));
                                    towersProjectModel.setBasement(dataObj.getString("Basement"));
                                    towersProjectModel.setFloorType(dataObj.getString("floorType"));
                                    towersProjectModel.setProjectCategoryId(dataObj.getString("ProjectCategoryId"));
                                    towersProjectModel.setPropertyTypeId(dataObj.getString("PropertyTypeId"));
                                    towersProjectModel.setStories(dataObj.getString("Stories"));
                                    towersProjectModel.setTowerName(dataObj.getString("TowerName"));


                                    towerses.add(towersProjectModel);
                                }
                            }

                            projectsModel.setPropertySizes(propertySizes);
                            projectsModel.setProjectRelationshipManagerses(projectRelationshipManagersArrayList);
                            projectsModel.setTowerses(towerses);
                            projectsModels.add(projectsModel);
                            unpublished_list.setLayoutManager(linearLayoutManager);

                                unPublishedProjectAdapter = new UnPublishedProjectAdapter(getActivity(), projectsModels);
                                unpublished_list.setAdapter(unPublishedProjectAdapter);
                                no_project.setVisibility(View.GONE);


                        }


//                        for (ProjectsModel projectsModel : searchProjectsModels) {
//                            tal.add(projectsModel.getCityName());
//
//
//                        }
//                        HashSet<String> hashSet = new HashSet<String>();
//                        hashSet.addAll(tal);
//                        tal.clear();
//                        tal.addAll(hashSet);
//                        Collections.sort(tal, new Comparator<String>() {
//                            public int compare(String obj1, String obj2) {
//                                // ## Ascending order
//                                return obj1.compareToIgnoreCase(obj2); // To compare string values
//                                // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values
//
//                                // ## Descending order
//                                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
//                                // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
//                            }
//                        });
//                        tal.add(0, "All");

                    }
                } catch (Exception e) {

                }
            }
        }
    }

    @Override
    public void onChangeTab(int position) {
        getUnpublishProjectList();
    }
public void onEvent(String  s){
    getUnpublishProjectList();
}
}
