package com.bnhabitat.ui.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.category.CategoryType;
import com.bnhabitat.data.category.CategoryTypeModel;
import com.bnhabitat.data.sizeunit.SizeUnitTable;
import com.bnhabitat.data.sizeunit.UnitsTable;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.activities.ProjectDetailActivity;
import com.bnhabitat.ui.adapters.AdapterCommonDialog;
import com.bnhabitat.ui.adapters.PricingLayoutPlanAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

/**

 */
public class VarientFragment extends Fragment implements TabChangeListener {
    private ArrayList<ProjectsDetailModel.PropertySize> propertySizes = new ArrayList<>();
    ArrayList<ProjectsDetailModel> projectListingModelArrayList = new ArrayList<>();
    ImageView info_icon, share_icon, notification_icon, user_icon;
    ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList<>();
    ProjectsDetailModel projectsDetailModel;
    private LinearLayout unitVariantLayout;
    private LinearLayout view_unit_varient,unitsLayout;
    int position1=0;
    AdapterCommonDialog adapterCommonDialog;
    PricingLayoutPlanAdapter pricingLayoutPlanAdapter;
    ArrayList<ProjectsDetailModel.PropertySize.LayoutPlans> layoutPlanses;
    private ImageView rightIcon;
    private ImageView leftIcon;
    private ViewPager layoutPlanPager;
    private LinearLayout layoutPlanParentLayout;
    private LinearLayout towerLayout;
    TextView unitTxt,towerTxt,toweravailableAreaTxtVw;

    private ArrayList<CommonDialogModel> propertyTypeModel = new ArrayList<>();
    private ArrayList<CommonDialogModel> towersModel = new ArrayList<>();
    private ArrayList<CommonDialogModel> propertyTypeModelOriginal = new ArrayList<>();
    public static int clickedPropertyItem = 0;
    ViewPager tower_detail_pager;
    private ImageView leftIcon1;
    private ImageView rightIcon1;
    private CardView layoutPlanCardView;
    TextView Tower_detail_layout,unit_varientLayout;
    LinearLayout tower_view;
    private DecimalFormat sizeFormat = new DecimalFormat("#####0.00");
    int position;
    String unitName = "";
    private int sizeUnit;
    private int SELECTED_SIZE_POSITION = 0;
    private ArrayList<Integer> bedrooms = new ArrayList<>();
    private String numberOfBedrooms = "";
    public ArrayList<Boolean> checkedItemsListed = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.PropertySize> propertySizesModels = new ArrayList<>();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    public VarientFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_varient, container, false);
        share_icon = (ImageView) view.findViewById(R.id.share_icon);
        notification_icon = (ImageView) view.findViewById(R.id.notification_icon);
        user_icon = (ImageView) view.findViewById(R.id.user_icon);
        tower_view = (LinearLayout) view.findViewById(R.id.tower_view);
        view_unit_varient = (LinearLayout) view.findViewById(R.id.view_unit_varient);
        unitsLayout = (LinearLayout) view.findViewById(R.id.unitsLayout);
        info_icon = (ImageView) view.findViewById(R.id.info_icon);
        unitVariantLayout = (LinearLayout) view.findViewById(R.id.unitVariantLayout);
        towerLayout = (LinearLayout) view.findViewById(R.id.towerLayout);
        tower_detail_pager = (ViewPager) view.findViewById(R.id.tower_detail);
        Tower_detail_layout = (TextView) view.findViewById(R.id.Tower_detail_layout);
        unit_varientLayout = (TextView) view.findViewById(R.id.unit_varientLayout);
        leftIcon1 = (ImageView) view.findViewById(R.id.leftIcon1);
        rightIcon1 = (ImageView) view.findViewById(R.id.rightIcon1);
        unitTxt = (TextView) view.findViewById(R.id.unitTxt);
        towerTxt = (TextView) view.findViewById(R.id.towerTxt);

        Bundle bundle = this.getArguments();
        if (bundle != null)
            projectsDetailModel = (ProjectsDetailModel) bundle.getSerializable("projectModel");
        propertySizes = (ArrayList<ProjectsDetailModel.PropertySize>) bundle.getSerializable("propertySizes");
        position = bundle.getInt("position");
        Log.e("position", String.valueOf(position));
        projectListingModelArrayList = (ArrayList<ProjectsDetailModel>) bundle.getSerializable("notification_array");
        if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.SIZE_UNIT_NAME, "").equalsIgnoreCase("")) {

            unitName = PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.SIZE_UNIT_NAME, "");
        }else{
            unitName = "sqft";
        }
        if (projectsDetailModel.getTowers().isEmpty()) {
            Tower_detail_layout.setVisibility(View.GONE);
        } else {
            tower_detail_pager.setAdapter(new TowerPagerAdapter(getActivity(), projectsDetailModel.getTowers()));
        }
        unit_varientLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unit_varientLayout.setBackgroundColor(getResources().getColor(R.color.blue));
                Tower_detail_layout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                tower_view.setVisibility(View.GONE);
                view_unit_varient.setVisibility(View.VISIBLE);
                setUnitVariantLayout();
            }
        });
        Tower_detail_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unit_varientLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                Tower_detail_layout.setBackgroundColor(getResources().getColor(R.color.blue));
                tower_view.setVisibility(View.VISIBLE);
                view_unit_varient.setVisibility(View.GONE);
            }
        });
        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                projectRelationshipManagersArrayList = projectsDetailModel.getProjectRelationshipManagers();
                Log.e("project_array", String.valueOf(projectRelationshipManagersArrayList.size()));
                if (!projectRelationshipManagersArrayList.isEmpty()) {
                    CustomDialogFragment fragment = CustomDialogFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Arraylist", projectRelationshipManagersArrayList);
                    fragment.setArguments(bundle);

                    fragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "dialog_fragment");
                } else {
                    Toast.makeText(getActivity(), "No RM associated with the project.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationDialogProjectDetail notificationDialogFragment = NotificationDialogProjectDetail.newInstance();

                Bundle bundle = new Bundle();
//
                bundle.putSerializable("projectModel", projectsDetailModel);
//
                notificationDialogFragment.setArguments(bundle);

                notificationDialogFragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "notification_dialog_fragment");

            }
        });
        share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, projectsDetailModel.getTitle());

                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://bnhabitat.com/?" + "p=" + projectsDetailModel.getId());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (projectsDetailModel.getCurrentOffers().equalsIgnoreCase("null") || projectsDetailModel.getCurrentOffers().equalsIgnoreCase("") || projectsDetailModel.getCurrentOffers() == null) {
                    Toast.makeText(getActivity(), "No offers yet", Toast.LENGTH_SHORT).show();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(projectsDetailModel.getCurrentOffers())
                            .setPositiveButton("NOTED", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();
                }
            }
        });
        towersModel.clear();
        propertyTypeModel.clear();
        for (ProjectsDetailModel.Towers towers : projectsDetailModel.getTowers()) {
            CommonDialogModel commonDialogModel = new CommonDialogModel();
            commonDialogModel.setId(String.valueOf(towers.getTowerId()));
            commonDialogModel.setTitle(towers.getTowerName());
            towersModel.add(commonDialogModel);
        }
        for (ProjectsDetailModel.PropertySize propertySizes : projectsDetailModel.getPropertySizes()) {
            CommonDialogModel commonDialogModel = new CommonDialogModel();
            commonDialogModel.setId(String.valueOf(propertySizes.getId()));
            commonDialogModel.setTitle(propertySizes.getTitle());
            if (null != propertySizes.getSize())
                commonDialogModel.setAreaSize(Double.parseDouble(propertySizes.getSize()));
            else
                commonDialogModel.setAreaSize(0);

            if (null != String.valueOf(propertySizes.getProperty_typeIdlevel2()))
                commonDialogModel.setPropertyTypeId(propertySizes.getProperty_typeIdlevel2());
            else
                commonDialogModel.setPropertyTypeId(0);
//                    commonDialogModel.setPropertyTypeId(propertySizes.getProperty_typeIdlevel2());

            if (null != propertySizes.getCarpetarea())
                commonDialogModel.setCarpetArea(propertySizes.getCarpetarea());
            else
                commonDialogModel.setCarpetArea("");
//
            if (null != propertySizes.getBuiltarea())
                commonDialogModel.setBuiltupArea(propertySizes.getBuiltarea());
            else
                commonDialogModel.setBuiltupArea("");

            if (null != propertySizes.getSize())
                commonDialogModel.setSuperArea(propertySizes.getSize());
            else
                commonDialogModel.setSuperArea("");


            propertyTypeModel.add(commonDialogModel);
            propertyTypeModelOriginal.add(commonDialogModel);
        }
//        unitTxt.setText(propertyTypeModel.get(0).getTitle());
//        towerTxt.setText(towersModel.get(0).getTitle());
        unitsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCommonDialog(getActivity(), propertyTypeModel, 0);
            }
        });
        towerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCommonDialog(getActivity(), towersModel, 10);
            }
        });
        return view;
    }

    private void setUnitVariantLayout() {

        unitVariantLayout.removeAllViews();

        long minPriceL = 100000000000000l;
        long maxPriceL = 0;

        long minSizeL = 100000000000000l;
        long maxSizeL = 0;

        String startsFrom = "NA";
//        String unitName = "";
        propertySizesModels = projectsDetailModel.getPropertySizes();

        for (int index = 0; index < propertySizesModels.size(); index++) {

            int sizeUnitId = propertySizesModels.get(index).getSizeUnit();

//            unitName = UnitsTable.getInstance().getUnitName(sizeUnitId);
//

            View view;
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.unit_items_list, null);
//            TextView sizeTxt = (TextView) view.findViewById(R.id.sizeTxt);
//            TextView titleTxt = (TextView) view.findViewById(R.id.titleTxt);
//            TextView rangeTxt = (TextView) view.findViewById(R.id.rangeTxt);
            TextView detailBtn = (TextView) view.findViewById(R.id.detailBtn);
            rightIcon = (ImageView) view.findViewById(R.id.rightIcon);

            leftIcon = (ImageView) view.findViewById(R.id.leftIcon);

            layoutPlanPager = (ViewPager) view.findViewById(R.id.layoutPager);
            layoutPlanParentLayout = (LinearLayout) view.findViewById(R.id.layoutPlanParentLayout);
            layoutPlanCardView = (CardView) view.findViewById(R.id.layoutPlanCardView);
            toweravailableAreaTxtVw = (TextView) view.findViewById(R.id.toweravailableAreaTxtVw);
            TextView extraAreaTxtVw = (TextView) view.findViewById(R.id.extraAreaTxtVw);
            TextView carpetAreaTxtVw = (TextView) view.findViewById(R.id.carpetAreaTxtVw);
            TextView builtUpAreaTxtVw = (TextView) view.findViewById(R.id.builtUpAreaTxtVw);
            TextView superAreaTxtVw = (TextView) view.findViewById(R.id.superAreaTxtVw);
            TextView superAreaTitleTxtVw = (TextView) view.findViewById(R.id.superAreaTitleTxtVw);
            TextView builtUpAreaTitleTxtVw = (TextView) view.findViewById(R.id.builtUpAreaTitleTxtVw);
            TextView carpetAreaTitleTxtVw = (TextView) view.findViewById(R.id.carpetAreaTitleTxtVw);
            TextView extraAreaTitleTxtVw = (TextView) view.findViewById(R.id.extraAreaTitleTxtVw);
            TextView extraAreaUnitTxtVw = (TextView) view.findViewById(R.id.extraAreaUnitTxtVw);
            TextView carpetAreaUnitTxtVw = (TextView) view.findViewById(R.id.carpetAreaUnitTxtVw);
            TextView builtUpUnitTxtVw = (TextView) view.findViewById(R.id.builtUpUnitTxtVw);
            TextView superAreaUnitTxtVw = (TextView) view.findViewById(R.id.superAreaUnitTxtVw);
            LinearLayout superAreaLayout = (LinearLayout) view.findViewById(R.id.superAreaLayout);
            LinearLayout builtAreaLayout = (LinearLayout) view.findViewById(R.id.builtAreaLayout);
            LinearLayout carpetAreaLayout = (LinearLayout) view.findViewById(R.id.carpetAreaLayout);
            LinearLayout extraAreaLayout = (LinearLayout) view.findViewById(R.id.extraAreaLayout);
            ProjectsDetailModel.PropertySize propertySize = projectsDetailModel.getPropertySizes().get(index);

            String sizeUnitName = UnitsTable.getInstance().getUnitName(propertySize.getSizeUnit());

            if (Utils.getFilteredValue(propertySize.getSize()).equalsIgnoreCase("NA") || propertySize.getSize().equalsIgnoreCase("0"))
                superAreaLayout.setVisibility(View.GONE);

            if (Utils.getFilteredValue(propertySize.getBuiltarea()).equalsIgnoreCase("NA") || propertySize.getBuiltarea().equalsIgnoreCase("0"))
                builtAreaLayout.setVisibility(View.GONE);

            if (Utils.getFilteredValue(propertySize.getExtraArea()).equalsIgnoreCase("NA") || propertySize.getExtraArea().equalsIgnoreCase("0"))
                extraAreaLayout.setVisibility(View.GONE);

            if (Utils.getFilteredValue(propertySize.getCarpetarea()).equalsIgnoreCase("NA") || propertySize.getCarpetarea().equalsIgnoreCase("0"))
                carpetAreaLayout.setVisibility(View.GONE);

            extraAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getExtraArea()));
            carpetAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getCarpetarea()));
            builtUpAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getBuiltarea()));
            superAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getSize()));

            extraAreaUnitTxtVw.setText(sizeUnitName);
            carpetAreaUnitTxtVw.setText(sizeUnitName);
            builtUpUnitTxtVw.setText(sizeUnitName);
            superAreaUnitTxtVw.setText(sizeUnitName);

            CategoryTypeModel categoryTypeModel = CategoryType.getInstance().getAreaCategory(propertySize.getProperty_typeIdlevel2());
//
            superAreaTitleTxtVw.setText(categoryTypeModel.getArea1());
            carpetAreaTitleTxtVw.setText(categoryTypeModel.getArea2());
            builtUpAreaTitleTxtVw.setText(categoryTypeModel.getArea3());
            extraAreaTitleTxtVw.setText(categoryTypeModel.getArea4());

            ArrayList<ProjectsDetailModel.PropertySize> propertysizeList = new ArrayList<>();

            propertysizeList = projectsDetailModel.getPropertySizes();
            try{
                String text = towersModel.get(index).getTitle();
                toweravailableAreaTxtVw.setText(text);
            }catch (Exception e){
                e.printStackTrace();
                toweravailableAreaTxtVw.setText("N/A");
            }


//            layoutPlanses.clear();
            layoutPlanses = new ArrayList<>();
            ArrayList<ProjectsDetailModel.PropertySize.LayoutPlans> layoutPlan = propertysizeList.get(index).getLayoutPlans();
            for (int j = 0; j < layoutPlan.size(); j++) {
                ProjectsDetailModel.PropertySize.LayoutPlans plans = new ProjectsDetailModel().new PropertySize().new LayoutPlans();
                plans.setType(layoutPlan.get(j).getType());
                plans.setBigImage(layoutPlan.get(j).getBigImage());
                plans.setDescription(layoutPlan.get(j).getDescription());
                plans.setTitle(layoutPlan.get(j).getTitle());
                layoutPlanses.add(plans);



            }
try {
    if (layoutPlanses.size() > 0) {
        pricingLayoutPlanAdapter = new PricingLayoutPlanAdapter(getActivity(), layoutPlanses);

        layoutPlanPager.setAdapter(pricingLayoutPlanAdapter);
        ((PagerAdapter)layoutPlanPager.getAdapter()).notifyDataSetChanged();

//                    pricingLayoutPlanAdapter.notifyDataSetChanged();


        layoutPlanParentLayout.setVisibility(View.VISIBLE);
        layoutPlanCardView.setVisibility(View.VISIBLE);
    } else {
//        pricingLayoutPlanAdapter = new PricingLayoutPlanAdapter(getActivity(), layoutPlanses);
//        layoutPlanPager.setAdapter(pricingLayoutPlanAdapter);
//        Utils.showWarningErrorMessage("Oops!", "Layout plans not loaded by builder.", "Ok", getActivity());
        layoutPlanParentLayout.setVisibility(View.GONE);
        layoutPlanCardView.setVisibility(View.GONE);

    }

}catch (Exception e){
    e.printStackTrace();
}
            layoutPlanPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    leftIcon.setVisibility(View.VISIBLE);
                    rightIcon.setVisibility(View.VISIBLE);

                    if (position == 0)
                        leftIcon.setVisibility(View.GONE);

                    if (position == (layoutPlanses.size() - 1))
                        rightIcon.setVisibility(View.GONE);

//                    if (layoutPlanses.size() > 0)
//                        planTitleTxt.setText(layoutPlanses.get(position).getTitle());
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


//            ImageView layout_plan = (ImageView) view.findViewById(R.id.layout_plan);
//            ImageView area_varient = (ImageView) view.findViewById(R.id.area_varient);

//            sizeTxt.setText(getUnitPrice(propertySizesModels.get(index).getSize()) + " " + unitName);
//            titleTxt.setText(propertySizesModels.get(index).getTitle());
//            String minPrice = Utils.getUnit(String.valueOf(propertySizesModels.get(index).getMinimumPrice()), getActivity());
//            //  String maxPrice = GlobalManager.getUnit(String.valueOf(propertySizesModels.get(index).getMaximumPrice()));
//
//            rangeTxt.setText(minPrice + " onwards");
//            layout_plan.setTag(index);
//            area_varient.setTag(index);
//            layout_plan.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    position1 = (Integer) view.getTag();
//                    showLayoutPlanDialog();
//                }
//            });
//            area_varient.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    position1 = (Integer) view.getTag();
//                    showVariantDialog();
//                }
//            });

            detailBtn.setTag(index);
            detailBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    PricingUnitFragment.IS_VIEW_INFLATED = false;
                    position1 = (Integer) view.getTag();
                    clickedPropertyItem = position1;

                    ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.pager1);
                    viewPager.setCurrentItem(3);



//


//

                }
            });


            if (minSizeL > Double.parseDouble(propertySizesModels.get(index).getSize()))
                minSizeL = (long) Double.parseDouble(propertySizesModels.get(index).getSize());

            if (maxSizeL < Double.parseDouble(propertySizesModels.get(index).getSize()))
                maxSizeL = (long) Double.parseDouble(propertySizesModels.get(index).getSize());


            if (minPriceL > propertySizesModels.get(index).getMinimumPrice())
                minPriceL = propertySizesModels.get(index).getMinimumPrice();

            if (maxPriceL < propertySizesModels.get(index).getMaximumPrice())
                maxPriceL = propertySizesModels.get(index).getMaximumPrice();

            unitVariantLayout.addView(view);
        }





        if (minSizeL != maxSizeL)
            startsFrom = minSizeL + "-" + maxSizeL;
        else
            startsFrom = String.valueOf(minSizeL);

        if (!numberOfBedrooms.equalsIgnoreCase("")) {

            StringBuilder bhk = new StringBuilder();
            Collections.sort(bedrooms);

            for (Integer room : bedrooms) {
                bhk.append(room + ",");
            }

            String bhks = bhk.toString();
            bhks = bhks.substring(0, bhks.length() - 1);
//            sizesTxt.setText(bhks + " BHK");


        } else {

            if (propertySizesModels.size() > 0) {
//                sizesTxt.setText(propertySizesModels.get(0).getTitle());
            }
        }
        if (propertySizesModels.size() > 0) {
//            proertyTypeTxt.setText(CategoryTable.getInstance().getCategoryName(propertySizesModels.get(0).getProperty_typeIdlevel1()) + ": " + CategoryType.getInstance().getCategoryName(propertySizesModels.get(0).getProperty_typeIdlevel2()));
        }

//        rangeTxt.setText(Utils.getUnit(String.valueOf(minPriceL), getActivity()) + "  onwards");
//        startFromTxt.setText(startsFrom + " " + unitName);
    }

    @Override
    public void onChangeTab(int position) {

    }
    @Override
    public void onPause() {

        //since our data might change (for example, in log out the data list is empty)
        //we need to notify the adapter so it wouldn't cause IllegalStateException
        if(pricingLayoutPlanAdapter != null) {
            pricingLayoutPlanAdapter.notifyDataSetChanged();
        }

        super.onPause();
    }
    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    private String getUnitPrice(String sum) {
        if (!sum.equalsIgnoreCase("")) {
            String output = sum;

            int unitId = 0;
            if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.SIZE_UNIT_ID, "").equalsIgnoreCase("")) {
                unitId = Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.SIZE_UNIT_ID, ""));

            } else {
                unitId = 19;

            }

            if (sizeUnit == unitId) {

                return sum;

            } else {
                float carpetAreaCal = Float.parseFloat(sum);
                if (sizeUnit != 19) {


                    carpetAreaCal = Float.parseFloat(sum);

                }

                String sizeUnitValue = SizeUnitTable.getInstance()
                        .readCategories(unitId, 1675);

                if (!sizeUnitValue.equalsIgnoreCase("")) {

                    return String.valueOf(sizeFormat.format(carpetAreaCal / Float.parseFloat(sizeUnitValue)));

                } else {

//                    Toast.makeText(getActivity(),
//                            "No value is available", Toast.LENGTH_LONG)
//                            .show();
                }


            }


            return output;
        } else {
            return sum;
        }
    }

    public class TowerPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        ArrayList<ProjectsDetailModel.Towers> towers;

        public TowerPagerAdapter(Context context, ArrayList<ProjectsDetailModel.Towers> towers) {
            mContext = context;
            this.towers = towers;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        }

        @Override
        public int getCount() {
            return towers.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.tower_detail, container, false);
            LinearLayout lay_no_stories = (LinearLayout) itemView.findViewById(R.id.lay_no_stories);
            LinearLayout lay_passenger = (LinearLayout) itemView.findViewById(R.id.lay_passenger);
            LinearLayout lay_floortype = (LinearLayout) itemView.findViewById(R.id.lay_floortype);
            LinearLayout lay_basement = (LinearLayout) itemView.findViewById(R.id.lay_basement);
            LinearLayout lay_unitonfloor = (LinearLayout) itemView.findViewById(R.id.lay_unitonfloor);
            LinearLayout lay_unitSizes = (LinearLayout) itemView.findViewById(R.id.lay_unitSizes);
            LinearLayout lay_unittype = (LinearLayout) itemView.findViewById(R.id.lay_unittype);
            LinearLayout lay_penthouse_floor = (LinearLayout) itemView.findViewById(R.id.lay_penthouse_floor);
            TextView penthouse_floor = (TextView) itemView.findViewById(R.id.penthouse_floor);
            TextView units_type = (TextView) itemView.findViewById(R.id.units_type);
            TextView units_sizes = (TextView) itemView.findViewById(R.id.units_sizes);
            TextView units_floor = (TextView) itemView.findViewById(R.id.units_floor);
            TextView basement = (TextView) itemView.findViewById(R.id.basement);
            TextView floortype = (TextView) itemView.findViewById(R.id.floortype);
            TextView Passenger = (TextView) itemView.findViewById(R.id.Passenger);
            TextView no_stories = (TextView) itemView.findViewById(R.id.no_stories);
            TextView tower_name = (TextView) itemView.findViewById(R.id.tower_name);
            basement.setText("" + towers.get(position).getBasement());
            tower_name.setText("" + towers.get(position).getTowerName());

            if (towers.get(position).getStories() == 0) {
                lay_no_stories.setVisibility(View.GONE);
            } else {
                no_stories.setText("" + towers.get(position).getStories());
            }
            if (towers.get(position).getFloorType().equalsIgnoreCase("")) {
                lay_floortype.setVisibility(View.GONE);
            } else {
                floortype.setText(towers.get(position).getFloorType());
            }


//


            tower_detail_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    leftIcon1.setVisibility(View.VISIBLE);
                    rightIcon1.setVisibility(View.VISIBLE);
                    if (position == 0)
                        leftIcon1.setVisibility(View.GONE);

                    if (position == (projectsDetailModel.getTowers().size() - 1))
                        rightIcon1.setVisibility(View.GONE);

//
                }

                @Override
                public void onPageSelected(int position) {
//
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


//


            container.addView(itemView, 0);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    public void showCommonDialog(Context context,
                                 final ArrayList<CommonDialogModel> commonDialogModels,
                                 final int from) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_common);
        dialog.show();


        ListView listView = (ListView) dialog.findViewById(R.id.listView);





            listView.setAdapter(new AdapterCommonDialog(getActivity(), commonDialogModels));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                            @Override
                                            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                                    long arg3) {


                                                if (from == 0) {


                                                    unitTxt.setText(propertyTypeModel.get(arg2).getTitle());
                                                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.UNIT_VALUE, unitTxt.getText().toString());


                                                    SELECTED_SIZE_POSITION = arg2;
                                                    setSelectList(arg2);
//


                                                } else if (from == 10) {


                                                    towerTxt.setText(towersModel.get(arg2).getTitle());
                                                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.TOWER_VALUE, towerTxt.getText().toString());


                                                    int id = Integer.parseInt(towersModel.get(arg2).getId());

                                                    propertyTypeModel.clear();

                                                    int index = 0;
                                                    for (ProjectsDetailModel.PropertySize propertySize : projectsDetailModel.getPropertySizes()) {
                                                        for (int towerId : propertySize.getTowers()) {
                                                            if (towerId == id) {
                                                                propertyTypeModel.add(propertyTypeModelOriginal.get(index));
//                                                                toweravailableAreaTxtVw.setText(propertyTypeModelOriginal.get(index).getTitle());
//                                                                Log.e("Property","Property"+propertyTypeModelOriginal.get(index).getTitle());
                                                                break;
                                                            }
                                                        }
                                                        index++;
                                                    }


                                                    if (propertyTypeModel.size() == 0) {

                                                    }


                                                }


//
                                                dialog.cancel();
                                            }
                                        }
        );






    }
    private void setSelectList(int size) {
        unitVariantLayout.removeAllViews();

        long minPriceL = 100000000000000l;
        long maxPriceL = 0;

        long minSizeL = 100000000000000l;
        long maxSizeL = 0;

        String startsFrom = "NA";
//        String unitName = "";
        propertySizesModels = projectsDetailModel.getPropertySizes();
        View view;
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.unit_items_list, null);
//            TextView sizeTxt = (TextView) view.findViewById(R.id.sizeTxt);
//            TextView titleTxt = (TextView) view.findViewById(R.id.titleTxt);
//            TextView rangeTxt = (TextView) view.findViewById(R.id.rangeTxt);
        TextView detailBtn = (TextView) view.findViewById(R.id.detailBtn);
        rightIcon = (ImageView) view.findViewById(R.id.rightIcon);

        leftIcon = (ImageView) view.findViewById(R.id.leftIcon);
        toweravailableAreaTxtVw = (TextView) view.findViewById(R.id.toweravailableAreaTxtVw);
        layoutPlanPager = (ViewPager) view.findViewById(R.id.layoutPager);
        layoutPlanParentLayout = (LinearLayout) view.findViewById(R.id.layoutPlanParentLayout);
        layoutPlanCardView = (CardView) view.findViewById(R.id.layoutPlanCardView);
        TextView extraAreaTxtVw = (TextView) view.findViewById(R.id.extraAreaTxtVw);
        TextView carpetAreaTxtVw = (TextView) view.findViewById(R.id.carpetAreaTxtVw);
        TextView builtUpAreaTxtVw = (TextView) view.findViewById(R.id.builtUpAreaTxtVw);
        TextView superAreaTxtVw = (TextView) view.findViewById(R.id.superAreaTxtVw);
        TextView superAreaTitleTxtVw = (TextView) view.findViewById(R.id.superAreaTitleTxtVw);
        TextView builtUpAreaTitleTxtVw = (TextView) view.findViewById(R.id.builtUpAreaTitleTxtVw);
        TextView carpetAreaTitleTxtVw = (TextView) view.findViewById(R.id.carpetAreaTitleTxtVw);
        TextView extraAreaTitleTxtVw = (TextView) view.findViewById(R.id.extraAreaTitleTxtVw);
        TextView extraAreaUnitTxtVw = (TextView) view.findViewById(R.id.extraAreaUnitTxtVw);
        TextView carpetAreaUnitTxtVw = (TextView) view.findViewById(R.id.carpetAreaUnitTxtVw);
        TextView builtUpUnitTxtVw = (TextView) view.findViewById(R.id.builtUpUnitTxtVw);
        TextView superAreaUnitTxtVw = (TextView) view.findViewById(R.id.superAreaUnitTxtVw);
        LinearLayout superAreaLayout = (LinearLayout) view.findViewById(R.id.superAreaLayout);
        LinearLayout builtAreaLayout = (LinearLayout) view.findViewById(R.id.builtAreaLayout);
        LinearLayout carpetAreaLayout = (LinearLayout) view.findViewById(R.id.carpetAreaLayout);
        LinearLayout extraAreaLayout = (LinearLayout) view.findViewById(R.id.extraAreaLayout);
        ProjectsDetailModel.PropertySize propertySize = projectsDetailModel.getPropertySizes().get(size);

        String sizeUnitName = UnitsTable.getInstance().getUnitName(propertySize.getSizeUnit());

        if (Utils.getFilteredValue(propertySize.getSize()).equalsIgnoreCase("NA") || propertySize.getSize().equalsIgnoreCase("0"))
            superAreaLayout.setVisibility(View.GONE);

        if (Utils.getFilteredValue(propertySize.getBuiltarea()).equalsIgnoreCase("NA") || propertySize.getBuiltarea().equalsIgnoreCase("0"))
            builtAreaLayout.setVisibility(View.GONE);

        if (Utils.getFilteredValue(propertySize.getExtraArea()).equalsIgnoreCase("NA") || propertySize.getExtraArea().equalsIgnoreCase("0"))
            extraAreaLayout.setVisibility(View.GONE);

        if (Utils.getFilteredValue(propertySize.getCarpetarea()).equalsIgnoreCase("NA") || propertySize.getCarpetarea().equalsIgnoreCase("0"))
            carpetAreaLayout.setVisibility(View.GONE);

        extraAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getExtraArea()));
        carpetAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getCarpetarea()));
        builtUpAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getBuiltarea()));
        superAreaTxtVw.setText(Utils.getFilteredValue(propertySize.getSize()));

        extraAreaUnitTxtVw.setText(sizeUnitName);
        carpetAreaUnitTxtVw.setText(sizeUnitName);
        builtUpUnitTxtVw.setText(sizeUnitName);
        superAreaUnitTxtVw.setText(sizeUnitName);

        CategoryTypeModel categoryTypeModel = CategoryType.getInstance().getAreaCategory(propertySize.getProperty_typeIdlevel2());
//
        superAreaTitleTxtVw.setText(categoryTypeModel.getArea1());
        carpetAreaTitleTxtVw.setText(categoryTypeModel.getArea2());
        builtUpAreaTitleTxtVw.setText(categoryTypeModel.getArea3());
        extraAreaTitleTxtVw.setText(categoryTypeModel.getArea4());

        ArrayList<ProjectsDetailModel.PropertySize> propertysizeList = new ArrayList<>();

        propertysizeList = projectsDetailModel.getPropertySizes();


//            layoutPlanses.clear();
        layoutPlanses = new ArrayList<>();
        ArrayList<ProjectsDetailModel.PropertySize.LayoutPlans> layoutPlan = propertysizeList.get(size).getLayoutPlans();
        for (int j = 0; j < layoutPlan.size(); j++) {
            ProjectsDetailModel.PropertySize.LayoutPlans plans = new ProjectsDetailModel().new PropertySize().new LayoutPlans();
            plans.setType(layoutPlan.get(j).getType());
            plans.setBigImage(layoutPlan.get(j).getBigImage());
            plans.setDescription(layoutPlan.get(j).getDescription());
            plans.setTitle(layoutPlan.get(j).getTitle());
            layoutPlanses.add(plans);


        }
        try {
            if (layoutPlanses.size() > 0) {
                pricingLayoutPlanAdapter = new PricingLayoutPlanAdapter(getActivity(), layoutPlanses);

                layoutPlanPager.setAdapter(pricingLayoutPlanAdapter);
                ((PagerAdapter) layoutPlanPager.getAdapter()).notifyDataSetChanged();

//                    pricingLayoutPlanAdapter.notifyDataSetChanged();


                layoutPlanParentLayout.setVisibility(View.VISIBLE);
                layoutPlanCardView.setVisibility(View.VISIBLE);
            } else {
//        pricingLayoutPlanAdapter = new PricingLayoutPlanAdapter(getActivity(), layoutPlanses);
//        layoutPlanPager.setAdapter(pricingLayoutPlanAdapter);
//        Utils.showWarningErrorMessage("Oops!", "Layout plans not loaded by builder.", "Ok", getActivity());
                layoutPlanParentLayout.setVisibility(View.GONE);
                layoutPlanCardView.setVisibility(View.GONE);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        layoutPlanPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                leftIcon.setVisibility(View.VISIBLE);
                rightIcon.setVisibility(View.VISIBLE);

                if (position == 0)
                    leftIcon.setVisibility(View.GONE);

                if (position == (layoutPlanses.size() - 1))
                    rightIcon.setVisibility(View.GONE);

//                    if (layoutPlanses.size() > 0)
//                        planTitleTxt.setText(layoutPlanses.get(position).getTitle());
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


//            ImageView layout_plan = (ImageView) view.findViewById(R.id.layout_plan);
//            ImageView area_varient = (ImageView) view.findViewById(R.id.area_varient);

//            sizeTxt.setText(getUnitPrice(propertySizesModels.get(index).getSize()) + " " + unitName);
//            titleTxt.setText(propertySizesModels.get(index).getTitle());
//            String minPrice = Utils.getUnit(String.valueOf(propertySizesModels.get(index).getMinimumPrice()), getActivity());
//            //  String maxPrice = GlobalManager.getUnit(String.valueOf(propertySizesModels.get(index).getMaximumPrice()));
//
//            rangeTxt.setText(minPrice + " onwards");
//            layout_plan.setTag(index);
//            area_varient.setTag(index);
//            layout_plan.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    position1 = (Integer) view.getTag();
//                    showLayoutPlanDialog();
//                }
//            });
//            area_varient.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    position1 = (Integer) view.getTag();
//                    showVariantDialog();
//                }
//            });
        String text = towersModel.get(size).getTitle();
        toweravailableAreaTxtVw.setText(text);
////
//        for (ProjectsDetailModel.PropertySize propertySize1 : projectsDetailModel.getPropertySizes()) {
//            for (int k=0; k<propertySize1.getTowers().size();k++) {
//
//                   toweravailableAreaTxtVw.setText(propertySize1.getTowers().get(k).g);
////
//               }
////                                                                toweravailableAreaTxtVw.setText(propertyTypeModelOriginal.get(index).getTitle());
////                                                                Log.e("Property","Property"+propertyTypeModelOriginal.get(index).getTitle());
//                    break;
//                }
//            }

        detailBtn.setTag(size);
        detailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PricingUnitFragment.IS_VIEW_INFLATED = false;
                position1 = (Integer) view.getTag();
                clickedPropertyItem = position1;

                ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.pager1);
                viewPager.setCurrentItem(3);


//


//

            }
        });


        if (minSizeL > Double.parseDouble(propertySizesModels.get(size).getSize()))
            minSizeL = (long) Double.parseDouble(propertySizesModels.get(size).getSize());

        if (maxSizeL < Double.parseDouble(propertySizesModels.get(size).getSize()))
            maxSizeL = (long) Double.parseDouble(propertySizesModels.get(size).getSize());


        if (minPriceL > propertySizesModels.get(size).getMinimumPrice())
            minPriceL = propertySizesModels.get(size).getMinimumPrice();

        if (maxPriceL < propertySizesModels.get(size).getMaximumPrice())
            maxPriceL = propertySizesModels.get(size).getMaximumPrice();

        unitVariantLayout.addView(view);
    }

}
