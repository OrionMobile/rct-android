package com.bnhabitat.ui.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtherQuestionViewFragment extends Fragment implements TabChangeListener {
    View view;

    ImageView call, msg;
    TextView submited_behalf, transfer_txt, owner_ship_status_txt, owner_ship_type_txt, transfer_by_txt, owned_by_txt, first_last_name, professional_txt;

    ArrayList<InventoryModel> property_data;

    public OtherQuestionViewFragment() {
        // Required empty public constructor
    }


    private static final int REQUEST_PHONE_CALL = 198;

    static String contactNumber = "";
    static String email = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_other_question_view, container, false);
        onIntializeView(view);
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null)
                property_data = (ArrayList<InventoryModel>) bundle.getSerializable("property_data");

            submited_behalf.setText(Utils.getEmptyValue(property_data.get(0).getPropertyOtherQuestionses().get(0).getSubmitOfBehalfOf()));
//            owner_ship_status_txt.setText(Utils.getEmptyValue(property_data.get(0).getPropertyLegalStatuses().get(0).getOwnerShipStatus()));
//            owner_ship_type_txt.setText(Utils.getEmptyValue(property_data.get(0).getPropertyLegalStatuses().get(0).getOwnerShipType()));
//            transfer_by_txt.setText(Utils.getEmptyValue(property_data.get(0).getPropertyLegalStatuses().get(0).getTransferByWayOf()));
//            owned_by_txt.setText(Utils.getEmptyValue(property_data.get(0).getPropertyLegalStatuses().get(0).getOwnedBy()));
            first_last_name.setText(Utils.getEmptyValue(property_data.get(0).getPropertyOtherQuestionses().get(0).getFirstName()) + " " + Utils.getEmptyValue(property_data.get(0).getPropertyOwnerses().get(0).getLastName()));
            contactNumber = Utils.getEmptyValue(property_data.get(0).getPropertyOtherQuestionses().get(0).getContactNo());
            email =Utils.getEmptyValue(property_data.get(0).getPropertyOtherQuestionses().get(0).getEmail());


        } catch (Exception e) {

        }


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                } else {
                    callIntent.setData(Uri.parse("tel:" + contactNumber));
                    startActivity(callIntent);
                }


            }
        });

        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);

                intent.setType("text/html");
               // intent.putExtra(Intent.EXTRA_SUBJECT, "Download app");
                intent.putExtra(Intent.EXTRA_TEXT,email);
                startActivity(Intent.createChooser(intent, "Choose"));
            }
        });

        return view;
    }

    public void onIntializeView(View view) {
//
        first_last_name = (TextView) view.findViewById(R.id.first_last_name);
        professional_txt = (TextView) view.findViewById(R.id.professional_txt);
        submited_behalf = (TextView) view.findViewById(R.id.submited_behalf);
        call = (ImageView) view.findViewById(R.id.call);
        msg = (ImageView) view.findViewById(R.id.msg);

    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contactNumber));

                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        startActivity(intent);
                    }

                }
        }
    }
}
