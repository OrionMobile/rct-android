package com.bnhabitat.ui.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.helper.CommonSyncwithoutstatus;
import com.bnhabitat.models.AllFieldsModel;
import com.bnhabitat.models.AllGroupsModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.NotesModel;
import com.bnhabitat.models.TagModel;
import com.bnhabitat.ui.activities.AssignContactActivity;
import com.bnhabitat.ui.activities.MainDashboardActivity;
import com.bnhabitat.ui.adapters.AddNotesAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.codetroopers.betterpickers.datepicker.DatePickerBuilder;
import com.codetroopers.betterpickers.datepicker.DatePickerDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class ContactSourceInfo extends Fragment implements CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener, DatePickerDialogFragment.DatePickerDialogHandler {
    Button save_and_continue;
    Spinner group_type_spinner;
    private TextView years, month, day;
    private EditText campaign_title, tag;
    View rootView;

    String ownership_str = "Myself";
    Spinner ownership_type_spinner;
    int i = 0;
    ArrayList<String> allusers = new ArrayList<>();
    private ArrayList<String> moreNotesArraay = new ArrayList();
    private ArrayList<LinearLayout> linearlayoutArray = new ArrayList();
    LinearLayout add_more_notes, remove_more_notes;
    ArrayList<NotesModel> notesList = new ArrayList<>();
    AddNotesAdapter addNotesAdapter;
    private ArrayList<AllGroupsModel> allGroupsModels;
    private ArrayList<AllGroupsModel> allGroupsModelsdata = new ArrayList<>();
    AutoCompleteTextView source;
    RelativeLayout ownership_type_spinner_lay;
    private ArrayList<String> sourceNames = new ArrayList<>();
    ArrayList<ContactsModel> contactsModels = new ArrayList<>();
    ArrayList<ContactsModel> contactArraylist = new ArrayList<>();
    RadioButton my_self, own, Other;
    RadioGroup radioSex;
    RecyclerView notes_recycler_view;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Integer> notesLay = new ArrayList<>();
    ArrayList<String> notesLayValues = new ArrayList<>();
    ArrayList<Boolean> isPrivate = new ArrayList<>();
    int count = 1;
    ArrayList<AllFieldsModel> allusersModels = new ArrayList<>();
    TextView notes;
    String id, group_id;
    LinearLayout parentLinearLayout;
    ArrayList<String> allGroupsNames = new ArrayList<>();
    String tags = "";
    private String blockCharacterSet = "@()%[]{}+-=;~Δ€ƒ„…†‡ˆ‰Š‹ŒŽ•–—˜™š›œžŸ¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ";
    int getDay, getMonth, getYear;

    public ContactSourceInfo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_contact_source_info, container, false);
        years = (TextView) rootView.findViewById(R.id.year);
        month = (TextView) rootView.findViewById(R.id.month);
        day = (TextView) rootView.findViewById(R.id.day);
        notes = (EditText) rootView.findViewById(R.id.notes);
        campaign_title = (EditText) rootView.findViewById(R.id.campaign_title);
        tag = (EditText) rootView.findViewById(R.id.tag);
        save_and_continue = (Button) rootView.findViewById(R.id.save_and_continue);
        group_type_spinner = (Spinner) rootView.findViewById(R.id.group_type_spinner);
        ownership_type_spinner = (Spinner) rootView.findViewById(R.id.ownership_type_spinner);
        add_more_notes = (LinearLayout) rootView.findViewById(R.id.add_more_notes);
        remove_more_notes = (LinearLayout) rootView.findViewById(R.id.remove_more_notes);
        source = (AutoCompleteTextView) rootView.findViewById(R.id.source);
        source.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(100)});
        notes_recycler_view = (RecyclerView) rootView.findViewById(R.id.notes_recycler_view);
        radioSex = (RadioGroup) rootView.findViewById(R.id.radioSex);
        my_self = (RadioButton) rootView.findViewById(R.id.my_self);
        own = (RadioButton) rootView.findViewById(R.id.own);
        Other = (RadioButton) rootView.findViewById(R.id.other);
        ownership_type_spinner_lay = (RelativeLayout) rootView.findViewById(R.id.ownership_type_spinner_lay);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        addNotesAdapter = new AddNotesAdapter(getActivity(), notesList);
        notes_recycler_view.setLayoutManager(linearLayoutManager);
        notes_recycler_view.setAdapter(addNotesAdapter);
        /*notesAdapter = new NotesAdapter();
        notes_recycler_view.setLayoutManager(linearLayoutManager);
        notes_recycler_view.setAdapter(notesAdapter);*/
        onClicks();
        onGetRequest("4");

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            contactsModels = (ArrayList<ContactsModel>) bundle.getSerializable("ModelName");

        }

        if(contactsModels.size()>0){

            if (contactsModels.get(0).getSourceName()!=null)
                source.setText(contactsModels.get(0).getSourceName());

                campaign_title.setText(contactsModels.get(0).getCampaign());


                try {
                    String dob = null;
                    if (contactsModels != null) {
                        dob = contactsModels.get(0).getSourceDate();
                    }
                    if (dob == null) {
                        day.setText("");
                        month.setText("");
                        years.setText("");
                    } else if (dob.equalsIgnoreCase("None")) {
                        day.setText("");
                        month.setText("");
                        years.setText("");
                    } else {
                        String dob1[] = dob.split("/");
                        day.setText(dob1[0]);
                        month.setText(dob1[1]);
                        years.setText(dob1[2]);

                        getDay = Integer.valueOf(dob1[0]);
                        getMonth = Integer.valueOf(dob1[1]);
                        getYear = Integer.valueOf(dob1[2]);
                    }


                } catch (Exception e) {

                }

              if (contactsModels.get(0).getTags().size()>0){
                  String text="";
                  for(int i=0;i<contactsModels.get(0).getTags().size();i++){

                      if(text!="")
                      {
                          text=text+","+contactsModels.get(0).getTags().get(i);
                      }else{
                          text=contactsModels.get(0).getTags().get(i);
                      }



                  }

//                  String s = TextUtils.join(", ", contactsModels.get(0).getTags());
                  tag.setText(text);

                /*  ArrayList<TagModel> str = contactsModels.get(0).getTags();
                  ArrayList aList= new ArrayList(Arrays.asList(str.split(",")));
                  for(int i=0;i<aList.size();i++)
                  {
                      System.out.println(" -->"+aList.get(i));
                  }*/
              }


            else {

                addNote();
            }
        }
        else {

            addNote();
        }
        ownership_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {

                    id = allusersModels.get(i).getId();
                    Log.e("id", "id" + id);


                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        group_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    group_id = allGroupsModelsdata.get(i).getId();
                    Log.e("id", "id" + id);
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        radioSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);

                ownership_str = checkedRadioButton.getText().toString();
                if (ownership_str.equalsIgnoreCase("Myself")) {
                    ownership_type_spinner_lay.setVisibility(View.GONE);
                } else if (ownership_str.equalsIgnoreCase("Own")) {
                    ownership_type_spinner_lay.setVisibility(View.GONE);
                } else {
                    ownership_type_spinner_lay.setVisibility(View.VISIBLE);
                    ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, allusers);
                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ownership_type_spinner.setAdapter(aa);
                    if (allusersModels.isEmpty()) {
                        ownership_type_spinner_lay.setVisibility(View.GONE);
//                        Utils.showErrorMessage("There is no data found",getActivity());
                    } else {
                        id = allusersModels.get(0).getName();
                    }

                }

            }
        });
//        ownership_type_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.e("pos","pos"+i);
//            }
//        });
        onUserRequest();
        onGetRequest();
        return rootView;


    }

    public void showDate(int day, int month, int year) {

        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();

        if (day == 0 && month == 0 && year == 0) {
            args.putInt("year", calender.get(Calendar.YEAR));
            args.putInt("month", calender.get(Calendar.MONTH));
            args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        } else {
            args.putInt("year", day);
            args.putInt("month", month);
            args.putInt("day", year);
        }


        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");

//        DatePickerBuilder dpb = new DatePickerBuilder()
//                .setFragmentManager(getChildFragmentManager())
//                .setStyleResId(R.style.BetterPickersDialogFragment)
//                .setYearOptional(true)
//                .setTargetFragment(PersonalContactInfo.this);
//        dpb.show();

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year1, int monthOfYear,
                              int dayOfMonth) {

            day.setText(String.valueOf(dayOfMonth));
            month.setText(String.valueOf(monthOfYear + 1));
            years.setText(String.valueOf(year1));

        }
    };


    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    private void onUserRequest() {

        new CommonSyncwithoutstatus(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_ALL_USERS + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""), "", "Loading...", this, Urls.URL_GET_ALL_USERS, Constants.GET).execute();

    }

    public void showDate() {

        DatePickerBuilder dpb = new DatePickerBuilder()
                .setFragmentManager(getChildFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment)
                .setYearOptional(true)
                .setTargetFragment(ContactSourceInfo.this);
        dpb.show();

    }

    /**
     *
     */

    private void onNotesAdd() {
        LayoutInflater inflator = LayoutInflater.from(getActivity());
        LinearLayout parentPanel = (LinearLayout) rootView.findViewById(R.id.notes_lay);

        View item = inflator.inflate(R.layout.more_notes_add, null);
        // initialize review UI
        notes = (EditText) item.findViewById(R.id.notes);
        parentPanel.addView(item);
        linearlayoutArray.add(parentPanel);

    }

    private void removeNotesAdd() {
        LayoutInflater inflator = LayoutInflater.from(getActivity());
        LinearLayout parentPanel = (LinearLayout) rootView.findViewById(R.id.notes_lay);

        View item = inflator.inflate(R.layout.more_notes_add, null);

        View namebar = item.findViewById(R.id.notes);
        ViewGroup parent = (ViewGroup) namebar.getParent();
        if (parent != null) {
            parent.removeView(namebar);
        }

    }

    //
    @Override
    public void onDialogDateSet(int reference, int year, int monthOfYear, int dayOfMonth) {
        day.setText(String.valueOf(dayOfMonth));
        month.setText(String.valueOf(monthOfYear + 1));

        years.setText(String.valueOf(year));
    }

    private void onClicks() {

        day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDate(getDay, getMonth, getYear);

            }
        });
        month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDate(getDay, getMonth, getYear);

            }
        });
        years.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDate(getDay, getMonth, getYear);

            }
        });

        save_and_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // notesLayValues.add(notes.getText().toString());
                onPostContacts();
            }
        });

        add_more_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               addNote();
            }
        });

        /*add_more_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //onNotesAdd();

                LayoutInflater inflator = LayoutInflater.from(getActivity());
                LinearLayout parentPanel = (LinearLayout) rootView.findViewById(R.id.notes_lay);

                View item = inflator.inflate(R.layout.more_notes_add, null);
                // initialize review UI
                notes = (EditText) item.findViewById(R.id.notes);
                parentPanel.addView(item);
                linearlayoutArray.add(parentPanel);
            }
        });*/

        /*remove_more_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });*/

//        day.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDate();
//
//            }
//        });
//        month.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDate();
//
//            }
//        });
//
//        years.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDate();
//
//            }
//        });

    }

    private void addNote() {

        notes_recycler_view.setVisibility(View.VISIBLE);
        NotesModel notesModel = new NotesModel();
        notesModel.setCreatedDate("");
        notesModel.setNote("");
        notesModel.setPrivate(false);
        notesList.add(notesModel);
        addNotesAdapter.notifyDataSetChanged();

    }

    private void onGetRequest(String taxonomyId) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_TERMS_DATA + taxonomyId + "/1",
                "", "Loading...",
                this,
                Urls.URL_GET_TERMS_DATA, Constants.GET).execute();

    }

    private void onGetRequest() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_ALL_GROUPS + "1", "", "Loading...", this, Urls.URL_GET_ALL_GROUPS, Constants.GET).execute();

    }

    private void onPostContacts() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
                getInputJson(),
                "Loading...",
                this,
                Urls.URL_CREATE_CONTACTS,
                Constants.POST).execute();

    }


    private String getInputJson() {

        String inputJson = "";

        try {


            JSONObject jsonObject = new JSONObject();


            jsonObject.put("Prefix", contactsModels.get(0).getPrefix());
            jsonObject.put("FirstName", contactsModels.get(0).getFirstName());
            jsonObject.put("LastName", contactsModels.get(0).getLastName());
            jsonObject.put("MiddleName", contactsModels.get(0).getMiddleName());
            jsonObject.put("Id", contactsModels.get(0).getId());

            jsonObject.put("CompanyId", Constants.COMPAN_ID);
            if (contactsModels.get(0).getImageUrl() == null) {
                jsonObject.put("ImageUrl", "");
            } else {
                jsonObject.put("ImageUrl", contactsModels.get(0).getImageUrl());
            }
            jsonObject.put("DateOfBirth", contactsModels.get(0).getDateOfBirthStr());
            jsonObject.put("Gender", contactsModels.get(0).getGender());

            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));


            jsonObject.put("Profession", contactsModels.get(0).getProfession());
            jsonObject.put("Designation", contactsModels.get(0).getDesignation());
            jsonObject.put("OwnerShip", ownership_str);
            if (ownership_str.equalsIgnoreCase("Other")) {
                jsonObject.put("OwnerUserId", id);
            }

            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("SourceName", source.getText().toString());
            jsonObject.put("SourceDate", day.getText().toString() + "/" + month.getText().toString() + "/" + years.getText().toString());
            jsonObject.put("Campaign", campaign_title.getText().toString());


            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getPhones().size(); i++) {

                JSONObject jsonObject2 = new JSONObject();

                jsonObject2.put("Id", contactsModels.get(0).getPhones().get(i).getId());
                jsonObject2.put("PhoneNumber", contactsModels.get(0).getPhones().get(i).getPhoneNumber());
                jsonObject2.put("PhoneCode", contactsModels.get(0).getPhones().get(i).getPhoneCode());

//                    jsonObject1.put("PhoneType", 1);
                jsonObject2.put("Mode", "Work");

                jsonArray.put(jsonObject2);

            }

          /*  // social array
            JSONArray socialjsonArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getContactAttributes().size(); i++) {


                JSONObject Twitter = new JSONObject();

                Twitter.put("Key", "IsSecure");
                Twitter.put("Id", contactsModels.get(0).getContactAttributes().get(i).getId());
                Twitter.put("Value", contactsModels.get(0).getContactAttributes().get(i).getValue());

                socialjsonArray.put(Twitter);
            }*/

            JSONArray emailArray = new JSONArray();
            if (!contactsModels.get(0).getEmails().isEmpty())
                for (int i = 0; i < contactsModels.get(0).getEmails().size(); i++) {

                    JSONObject jsonObject3 = new JSONObject();

                    jsonObject3.put("Id", contactsModels.get(0).getEmails().get(i).getId());
                    jsonObject3.put("Address", contactsModels.get(0).getEmails().get(i).getAddress());

//                    jsonObject1.put("PhoneType", 1);
                    jsonObject3.put("Label", "Email Label");

                    emailArray.put(jsonObject3);

                }

            JSONArray addressArray = new JSONArray();
//                 getPhones=contactsFirebaseModels.get(index).getPhones();
//
//            for (int i = 0; i < addresses.size(); i++) {

            JSONObject jsonObject1 = new JSONObject();
            if (!contactsModels.get(0).getAddresses().isEmpty()) {
                for (int i = 0; i < contactsModels.get(0).getAddresses().size(); i++) {
                    jsonObject1.put("Id", contactsModels.get(0).getAddresses().get(i).getId());
                    jsonObject1.put("StateName", contactsModels.get(0).getAddresses().get(i).getStateName());

//                  jsonObject1.put("PhoneType", 1);
                    jsonObject1.put("CountryName", contactsModels.get(0).getAddresses().get(i).getCountryName());
                    jsonObject1.put("CityName", contactsModels.get(0).getAddresses().get(i).getCity_name());

                    jsonObject1.put("DeveloperName", contactsModels.get(0).getAddresses().get(i).getDeveloper_name());
                    jsonObject1.put("Tower", contactsModels.get(0).getAddresses().get(i).getTower());
                    jsonObject1.put("Floor", contactsModels.get(0).getAddresses().get(i).getFloor());
                    jsonObject1.put("UnitNo", contactsModels.get(0).getAddresses().get(i).getUnitName());
                    jsonObject1.put("ZipCode", contactsModels.get(0).getAddresses().get(i).getZipcode());
                    addressArray.put(jsonObject1);
                }
            }


            contactsModels.get(0).setNotesModelArrayList(notesList);

            JSONArray notesArray = new JSONArray();
            if (!contactsModels.get(0).getNotesModelArrayList().isEmpty()) {

                for (int i = 0; i < notesList.size(); i++) {
                    JSONObject notes = new JSONObject();

                    notes.put("Note", contactsModels.get(0).getNotesModelArrayList().get(i).getNote());
                    notes.put("IsPrivate", contactsModels.get(0).getNotesModelArrayList().get(i).isPrivate());
                    notes.put("CreatedDate", contactsModels.get(0).getNotesModelArrayList().get(i).getCreatedDate());
                    notesArray.put(notes);
                }

            }


            tags = tag.getText().toString();

            String[] tagsArray = tags.split(",");

            JSONArray tagArray = new JSONArray();

            for (int i = 0; i < tagsArray.length; i++) {

                tagArray.put(tagsArray[i]);

            }

            JSONArray grouparray = new JSONArray();
            JSONObject jsonObject4 = new JSONObject();
            jsonObject4.put("Id", group_id);
            grouparray.put(jsonObject4);
            jsonObject.put("Phones", jsonArray);
            jsonObject.put("EmailAddresses", emailArray);
         // jsonObject.put("ContactSocialDetails", socialjsonArray);
            jsonObject.put("ContactNotes", notesArray);
            jsonObject.put("Addresses", addressArray);
            jsonObject.put("Groups", grouparray);
            jsonObject.put("Tags", tagArray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }


    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_CREATE_CONTACTS)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {

                        Toast.makeText(getActivity(), "Contact Sources information updated successfully.", Toast.LENGTH_SHORT).show();
                        if (!contactsModels.isEmpty()) {
                            Intent intent = new Intent(getActivity(), MainDashboardActivity.class);
                            intent.putExtra(Constants.FROM, "EditQuery");
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            getActivity().finish();
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_GET_ALL_GROUPS)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONArray projectsArray = jsonObject.getJSONArray("Result");


                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            AllGroupsModel allGroupsModel = new AllGroupsModel();
                            allGroupsModel.setId(jsonObject1.getString("Id"));
                            allGroupsModel.setName(jsonObject1.getString("name"));
                            allGroupsModel.setDescription(jsonObject1.getString("Description"));
                            allGroupsNames.add(allGroupsModel.getName());
                            allGroupsModelsdata.add(allGroupsModel);
                        }

                        if (!allGroupsNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, allGroupsNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            group_type_spinner.setAdapter(aa);
                            group_id = allGroupsModelsdata.get(i).getId();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_GET_ALL_USERS)) {

                try {
//                    JSONObject jsonObject = new JSONObject(result);
//                    if (jsonObject.getInt("StatusCode") == 200) {


                    JSONArray projectsArray = new JSONArray(result);

                    allusers.clear();
                    for (int index = 0; index < projectsArray.length(); index++) {
                        JSONObject jsonObject1 = projectsArray.getJSONObject(index);
                        AllFieldsModel allFieldsModel = new AllFieldsModel();
                        allFieldsModel.setId(jsonObject1.getString("Id"));
                        allFieldsModel.setName(jsonObject1.getString("FirstName"));
                        allusersModels.add(allFieldsModel);
                        allusers.add(allFieldsModel.getName());
                    }
                    if (!allusers.isEmpty()) {
//                            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, allusers);
//                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            auto_complete_comapny.setAdapter(aa);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_GET_TERMS_DATA)) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        allGroupsModels = new ArrayList<>();
                        JSONArray projectsArray = jsonObject.getJSONArray("Result");
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            AllGroupsModel allGroupsModel = new AllGroupsModel();
                            allGroupsModel.setId(jsonObject1.getString("Id"));
                            allGroupsModel.setName(jsonObject1.getString("name"));
                            allGroupsModel.setDescription(jsonObject1.getString("Description"));
                            allGroupsModels.add(allGroupsModel);
                            sourceNames.add(jsonObject1.getString("name"));
                        }

                        if (!sourceNames.isEmpty()) {
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, sourceNames);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            source.setAdapter(aa);


                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

    }

}