package com.bnhabitat.ui.fragments;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.projects.AllProjectsTable;
import com.bnhabitat.data.projects.RecentViewProjectsTable;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.ui.activities.DashboardActivity;
import com.bnhabitat.ui.activities.LoginActivity;
import com.bnhabitat.ui.activities.MyProfile;
import com.bnhabitat.ui.activities.ProfileActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Utils;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SettingsFragment extends android.support.v4.app.Fragment implements GoogleApiClient.OnConnectionFailedListener {
    private GoogleApiClient mGoogleApiClient;

    LinearLayout profile, measurement, calculator, becomeAgent, faq, emailNotification, signOut;
    View view;

    SendMessage sm;
    public SettingsFragment(){
        //required an empty construtor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){/*

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() *//* FragmentActivity *//*, (GoogleApiClient.OnConnectionFailedListener) getContext() *//* OnConnectionFailedListener *//*)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();*/

        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.setting_fragment, container, false);
        onIntialize(view);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MyProfile.class));
                //Toast.makeText(getActivity(), "Profile clicked", Toast.LENGTH_SHORT).show();
            }
        });

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Warning!!!")
                        .setContentText("Do you want to logout?")
                        .setConfirmText("Yes")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                LoginManager.getInstance().logOut();
                                //signOut();
                                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.USER_ID, "");
                                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.USERNAME, "");
                                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.EMAIL, "");
                                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.PHONE_NUMBER, "");
                                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.USERNAME, "");
                                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.REGISTRATION_ID, "");

                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setCancelText("No")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .show();


            }
        });

        return view;
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        AllProjectsTable.getInstance().deleteAllRecords();
                        RecentViewProjectsTable.getInstance().deleteAllRecords();
                        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.USER_ID, "");
                        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.EMAIL, "");
                        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.PHONE_NUMBER, "");
                        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.USERNAME, "");
                        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.REGISTRATION_ID, "");
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                        // [START_EXCLUDE]
//                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void onIntialize(View view) {

        profile = (LinearLayout) view.findViewById(R.id.profile);
        measurement = (LinearLayout) view.findViewById(R.id.measurement);
        calculator = (LinearLayout) view.findViewById(R.id.calculator);
        becomeAgent = (LinearLayout) view.findViewById(R.id.becomeAgent);
        faq = (LinearLayout) view.findViewById(R.id.faq);
        emailNotification = (LinearLayout) view.findViewById(R.id.emailNotification);
        signOut = (LinearLayout) view.findViewById(R.id.signOut);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
