package com.bnhabitat.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.areaActivities.StateListing;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.ui.activities.DashboardActivity;
import com.bnhabitat.ui.activities.InventoryDashboardActivity;
import com.bnhabitat.ui.activities.MainDashboardActivity;
import com.bnhabitat.ui.activities.ProjectsActivity;
import com.bnhabitat.ui.activities.SelectionPropertyTypeActivity;
import com.nostra13.universalimageloader.utils.L;

public class NewDashboardFragment extends Fragment {
    LinearLayout projects, contacts, area, leads, inventory, recommendfor, guests, report;
    View view;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    SendMessage SM;
    public NewDashboardFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.new_fragment_project, container, false);
        onIntialize(view);

        area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), StateListing.class));
            }
        });

        projects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ProjectsActivity.class));
            }
        });

        contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), MainDashboardActivity.class));

            }
        });

        inventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), InventoryDashboardActivity.class));
            }
        });
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();

        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    public void onIntialize(View view){
        projects = (LinearLayout) view.findViewById(R.id.projects);
        contacts = (LinearLayout) view.findViewById(R.id.contacts);
        area = (LinearLayout) view.findViewById(R.id.area);
        leads = (LinearLayout) view.findViewById(R.id.leads);
        inventory = (LinearLayout) view.findViewById(R.id.inventory);
        recommendfor = (LinearLayout) view.findViewById(R.id.recommendfor);
        guests = (LinearLayout) view.findViewById(R.id.guests);
        report = (LinearLayout) view.findViewById(R.id.reports);

    }
    // TODO: Rename method, update argument and hook method into UI event
}
