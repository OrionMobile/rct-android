package com.bnhabitat.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.AllGroupsModel;
import com.bnhabitat.models.ContactSocialDetails;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.CountryModel;
import com.bnhabitat.models.FeatureParkModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.StateModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.activities.MainDashboardActivity;
import com.bnhabitat.ui.activities.ProfileActivity;

import com.bnhabitat.ui.adapters.SpecificationAdapter;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.ui.views.AutoCompleteEditext;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.MultiTextWatcher;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static com.bnhabitat.ui.activities.CompleteContactInfoActivity.viewPager;


public class AddressContactInfo extends Fragment implements CommonAsync.OnAsyncResultListener {
    ArrayList<CountryModel> country_list = new ArrayList<>();
    ArrayList<String> country_name = new ArrayList<>();
    View rootView;
    LinearLayout add_more_Addresses_lay;
    Button save_and_continue;
    SpinnerAdapter spinnerAdapter;
    SpinnerAdapter countrtySpinnerAdapter;
    Spinner country_spinner;
 // EditText state_type_spinner,country_type_spinner;
    SendMessage SM;
    private ArrayList<AddressModel> addressesArray = new ArrayList();
    RecyclerView add_more_Addresses_recycler;
    AddMoreAddressAdapter addMoreAddressAdapter;
    ArrayList<AddressModel> addresses = new ArrayList<>();
    ArrayList<LinearLayout> addressesLayouts = new ArrayList<>();
    private ArrayList<AllGroupsModel> allGroupsModels;
    ArrayList<ContactsModel> contactsModels = new ArrayList<>();
    //    ArrayList<ContactsModel> contactArraylist = new ArrayList<>();
    String edit_data;
    int pos;
    ArrayList<StateModel> stateModels =new ArrayList<>();
    ArrayList<StateModel> stateModelsTemp =new ArrayList<>();
    private AutocompleteAdapter autocompleteAdapter;
    ArrayList<String> area_list = new ArrayList<>();

    public AddressContactInfo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_address_contact_info, container, false);
        add_more_Addresses_lay = (LinearLayout) rootView.findViewById(R.id.add_more_Addresses_lay);
        save_and_continue = (Button) rootView.findViewById(R.id.save_and_continue);
        add_more_Addresses_recycler = (RecyclerView) rootView.findViewById(R.id.add_more_Addresses_recycler);
        country_spinner = (Spinner) rootView.findViewById(R.id.country_spinner);
        onAreaEntitiy();

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            contactsModels = (ArrayList<ContactsModel>) bundle.getSerializable("ModelName");
            edit_data = bundle.getString("edit") == null ? "" : bundle.getString("edit");
        }
        onClicks();
        getCountryList();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        add_more_Addresses_recycler.setLayoutManager(linearLayoutManager);
        addMoreAddressAdapter = new AddMoreAddressAdapter(getActivity());
        add_more_Addresses_recycler.setAdapter(addMoreAddressAdapter);
        if(contactsModels.size()>0){

            if (!contactsModels.get(0).getAddresses().isEmpty()) {

                addressesArray = contactsModels.get(0).getAddresses();
                addMoreAddressAdapter.notifyDataSetChanged();

            }
            else {

                OnAdding();
            }
        }
        else {

            OnAdding();
        }





//      onAddressAdd();
        return rootView;


    }

    private void getCountryList() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_COUNTRY_LIST,
                "",
                "Loading..",
                this,
                Urls.URL_GET_COUNTRY_LIST,
                Constants.GET).execute();

    }
    private void onAreaEntitiy() {

/*
        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_AREA_ENTITY + entity_id,
                "",
                "Loading..",
                this,
                Urls.URL_AREA_ENTITY,
                Constants.GET, false).execute();
*/

        Constants.ENTITY_ID= "1";

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_STATE_LISTING_FINAL +"/"+ Constants.ENTITY_ID+

                "/"+ Constants.STATE_ID+  "/" +Constants.DISTRCT_ID +"/"+ Constants.SUB_DIVISION_ID +"/"+ Constants.TEHSIL_ID +"/"+ Constants.TOWNID
                +"/"+ Constants.TOWNSHIP_ID +"/"+ Constants.LOCALITY_ID,
                "",
                "Loading..",
                this,
                Urls.URL_STATE_LISTING_FINAL +"/"+ Constants.ENTITY_ID+

                        "/"+ Constants.STATE_ID+  "/" +Constants.DISTRCT_ID +"/"+ Constants.SUB_DIVISION_ID +"/"+ Constants.TEHSIL_ID +"/"+ Constants.TOWNID
                        +"/"+ Constants.TOWNSHIP_ID +"/"+ Constants.LOCALITY_ID,
                Constants.GET).execute();

    }

    public void OnAdding() {

        final AddressModel addresses = new  AddressModel();
        addresses.setAddressType("");
        addresses.setTower("");
        addresses.setFloor("");
        addresses.setUnitName("");
        addresses.setZipcode("");
        addresses.setStateName("");
        addresses.setCountryId("");
        addresses.setTehsil("");
        addresses.setDistrictName("");
        addresses.setTownName("");
        addresses.setVillageName("");
        addresses.setTownName("");
        addresses.setLocality("");
        addresses.setProjectName("");
       // addresses.setLandmark("");
        addresses.setDeveloper_name("");
        addressesArray.add(addresses);
        addMoreAddressAdapter.notifyDataSetChanged();

    }

    public static List<StateModel> filter(String string,
                                          ArrayList<StateModel> iterable, boolean byName) {
        if (iterable == null)
            return new LinkedList<>();
        else {
            List<StateModel> collected = new LinkedList<>();
            Iterator<StateModel> iterator = iterable.iterator();
            if (iterator == null)
                return collected;
            while (iterator.hasNext()) {
                StateModel item = iterator.next();

                if (item.getName().toLowerCase().contains(string.toLowerCase()))
                    collected.add(item);

            }
            return collected;
        }
    }


    private void onClicks() {


        add_more_Addresses_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                final ContactsModel.Addresses addressModel = new ContactsModel().new Addresses();
//
//                addressModel.setAddressType(address_type_spinner.getSelectedItem().toString());
//                addressModel.setStateName(state_type_spinner.getText().toString());
//                addressModel.setCountryName(country_type_spinner.getText().toString());
//                addressModel.setTower(tower.getText().toString());
//                addressModel.setFloor(floor.getText().toString());
//                addressModel.setUnitNo(unit_no.getText().toString());
//                addressModel.setCityName(city.getText().toString());
//                addressModel.setZipPostalCode(pincode.getText().toString());
//                addressModel.setLocalityName(building_name.getText().toString());
//                addressModel.setLocalityName(locality.getText().toString());
//                addressModel.setTehsilName(tehsil.getText().toString());
//                addressModel.setDeveloperName(developer_name.getText().toString());
//                addressesArray.add(addressModel);
                OnAdding();

//              onAddressAdd();

            }
        });

        save_and_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                final ContactsModel.Addresses addressModel = new ContactsModel().new Addresses();
//
//                addressModel.setAddressType(address_type_spinner.getSelectedItem().toString());
//                addressModel.setStateName(state_type_spinner.getText().toString());
//                addressModel.setCountryName(country_type_spinner.getText().toString());
//                addressModel.setTower(tower.getText().toString());
//                addressModel.setFloor(floor.getText().toString());
//                addressModel.setUnitNo(unit_no.getText().toString());
//                addressModel.setCityName(city.getText().toString());
//                addressModel.setZipPostalCode(pincode.getText().toString());
//                addressModel.setLocalityName(building_name.getText().toString());
//                addressModel.setLocalityName(locality.getText().toString());
//                addressModel.setTehsilName(tehsil.getText().toString());
//                addressModel.setDeveloperName(developer_name.getText().toString());
//                addressesArray.add(addressModel);
                onPostContacts();


            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();


        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

//    private void onAddressAdd() {
//
//
//        LayoutInflater inflator = LayoutInflater.from(getActivity());
//        LinearLayout parentPanel = (LinearLayout) rootView.findViewById(R.id.addresses_lay);
//
////
////                for(String data : myList) {
//        // inflate child
//        View item = inflator.inflate(R.layout.more_addresses_layout, null);
//        // initialize review UI
//        address_type_spinner = (Spinner) item.findViewById(R.id.address_type_spinner);
//        state_type_spinner = (EditText) item.findViewById(R.id.state_type_spinner);
//        country_type_spinner = (EditText) item.findViewById(R.id.country_type_spinner);
//        tower = (EditText) item.findViewById(R.id.tower);
//        floor = (EditText) item.findViewById(R.id.floor);
//        unit_no = (EditText) item.findViewById(R.id.unit_no);
//        city = (EditText) item.findViewById(R.id.city);
//        pincode = (EditText) item.findViewById(R.id.pincode);
//        building_name = (EditText) item.findViewById(R.id.building_name);
//        locality = (EditText) item.findViewById(R.id.locality);
//        tehsil = (EditText) item.findViewById(R.id.tehsil);
//        developer_name = (EditText) item.findViewById(R.id.developer_name);
//
//        try {
//            if (!contactsModels.get(0).getAddresses().isEmpty()) {
//
//                tower.setText(contactsModels.get(0).getAddresses().get(0).getTower());
//                floor.setText(contactsModels.get(0).getAddresses().get(0).getFloor());
//                unit_no.setText(contactsModels.get(0).getAddresses().get(0).getUnitNo());
//                city.setText(contactsModels.get(0).getAddresses().get(0).getCityName());
//                pincode.setText(contactsModels.get(0).getAddresses().get(0).getZipPostalCode());
//                developer_name.setText(contactsModels.get(0).getAddresses().get(0).getDeveloperName());
//
//
//            }
//        } catch (Exception e) {
//
//        }
//
//        // add child
//        parentPanel.addView(item);
//        AddressModel addressModel = new AddressModel();
//        addressModel.setAddressType(address_type_spinner.getSelectedItem().toString());
//        addressModel.setStateName(state_type_spinner.getText().toString());
//        addressModel.setCountryName(country_type_spinner.getText().toString());
//        addressModel.setTower(tower.getText().toString());
//        addressModel.setFloor(floor.getText().toString());
//        addressModel.setUnit_no(unit_no.getText().toString());
//        addressModel.setCity_name(city.getText().toString());
//        addressModel.setPincode(pincode.getText().toString());
//        addressModel.setBuilding_name(building_name.getText().toString());
//        addressModel.setLocality(locality.getText().toString());
//        addressModel.setTehsil(tehsil.getText().toString());
//        addressModel.setDeveloper_name(developer_name.getText().toString());
//
//        addresses.add(addressModel);
//
//        addressesLayouts.add(parentPanel);
//
////       morePhonesArray.add(dataText.getText().toString());
//
//    }

    public class AutocompleteAdapter extends ArrayAdapter<StateModel> {
        private ArrayList<StateModel> fullList;
        private ArrayList<StateModel> mOriginalValues;
        int layoutResourceId;
        Context mContext;
        String text;

        public AutocompleteAdapter(Context mContext, int layoutResourceId, ArrayList<StateModel> entity, String text) {
            super(mContext, layoutResourceId, entity);
            this.fullList = entity;
            this.mContext = mContext;
            this.text = text;
            mOriginalValues = new ArrayList<StateModel>(fullList);
            this.layoutResourceId = layoutResourceId;
        }

        @Override
        public int getCount() {
            return fullList.size();
        }

        public StateModel getItem(int position) {
            return fullList.get(position);
        }
        @Override
        public Filter getFilter() {
            return nameFilter;
        }

        Filter nameFilter = new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                String str = ((StateModel)(resultValue)).getName();
                return str;
            }
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if(constraint != null) {
                    mOriginalValues.clear();
                    for (StateModel customer : fullList) {
                        if(customer.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                            mOriginalValues.add(customer);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mOriginalValues;
                    filterResults.count = mOriginalValues.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                ArrayList<StateModel> filteredList = (ArrayList<StateModel>) results.values;
                if(results != null && results.count > 0) {
                    clear();
                    for (StateModel c : filteredList) {
                        add(c);
                    }
                    notifyDataSetChanged();
                }
            }
        };

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                // inflate the layout
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = inflater.inflate(R.layout.entity_item, parent, false);

            }

            //get Country

            // get the TextView and then set the text (item name) and tag (item ID) values

            StateModel stateModel = getItem(position);

            final TextView countryName = (TextView) convertView.findViewById(R.id.area);

            countryName.setText(stateModel.getName());

            setSearchTextHighlightSimpleHtml(countryName, fullList.get(position).getName(), text);

            return convertView;
        }


        public void setSearchTextHighlightSimpleHtml(TextView textView, String fullText, String searchText) {
            searchText = searchText.replace("'", "");
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    fullText = fullText.replaceAll("(?i)(" + searchText + ")", "<b><big><font color='#000000'>$1</font></big></b>");
                    textView.setText(Html.fromHtml(fullText, Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
                } else {
                    fullText = fullText.replaceAll("(?i)(" + searchText + ")", "<b><big><font color='red'>$1</font></big></b>");
                    textView.setText(Html.fromHtml(fullText), TextView.BufferType.SPANNABLE);
                }
            } catch (Exception e) {
                textView.setText(fullText);
            }
        }

    }

    private void onPostContacts() {

        if (contactsModels.size()>0){

            new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
                    getInputJson(),
                    "Loading...",
                    this,
                    Urls.URL_CREATE_CONTACTS,
                    Constants.POST).execute();
        }
       else {

            new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CREATE_CONTACTS,
                    getEditInputJson(),
                    "Loading...",
                    this,
                    Urls.URL_CREATE_CONTACTS,
                    Constants.POST).execute();

        }
    }

    private String getInputJson() {

        String inputJson = "";

        try {


            JSONObject jsonObject = new JSONObject();

            jsonObject.put("Id", contactsModels.get(0).getId());
            jsonObject.put("Prefix", contactsModels.get(0).getPrefix());
            jsonObject.put("FirstName", contactsModels.get(0).getFirstName());
            jsonObject.put("LastName", contactsModels.get(0).getLastName());
            jsonObject.put("MiddleName", contactsModels.get(0).getMiddleName());
            //jsonObject.put("Id", contactsModels.get(0).getId());

            jsonObject.put("DateOfBirth", contactsModels.get(0).getDateOfBirthStr());
            jsonObject.put("Gender", contactsModels.get(0).getGender());
            jsonObject.put("CompanyId", Constants.COMPAN_ID);

           /* if (contactsModels.get(0).getAddresses().equals(0)) {
                jsonObject.put("IsModify", "false");
            } else {
                jsonObject.put("IsModify", "true");
            }*/

            if (contactsModels.get(0).getImageUrl() == null) {
                jsonObject.put("ImageUrl", "");
            } else {
                jsonObject.put("ImageUrl", contactsModels.get(0).getImageUrl());
            }
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));


            jsonObject.put("Profession", contactsModels.get(0).getProfession());
            jsonObject.put("Designation", contactsModels.get(0).getDesignation());

            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getPhones().size(); i++) {

                JSONObject jsonObject2 = new JSONObject();

                jsonObject2.put("Id", contactsModels.get(0).getPhones().get(i).getId());
                jsonObject2.put("PhoneNumber", contactsModels.get(0).getPhones().get(i).getPhoneNumber());
                jsonObject2.put("PhoneCode", contactsModels.get(0).getPhones().get(i).getPhoneCode());
                jsonObject2.put("Mode", "");

                jsonArray.put(jsonObject2);

            }

          /*  JSONArray socialjsonArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getContactAttributes().size(); i++) {


                JSONObject Twitter = new JSONObject();

                Twitter.put("Key", "IsSecure");
                Twitter.put("Id", contactsModels.get(0).getContactAttributes().get(i).getId());
                Twitter.put("Value", contactsModels.get(0).getContactAttributes().get(i).getValue());

                socialjsonArray.put(Twitter);


            }*/
            JSONArray emailArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getEmails().size(); i++) {

                JSONObject jsonObject3 = new JSONObject();

                jsonObject3.put("Id", contactsModels.get(0).getEmails().get(i).getId());
                jsonObject3.put("Address", contactsModels.get(0).getEmails().get(i).getAddress());

//              jsonObject1.put("PhoneType", 1);
                jsonObject3.put("Label", "Email Label");

                emailArray.put(jsonObject3);

            }

            JSONArray addressArray = new JSONArray();

            for (int i = 0; i < addressesArray.size(); i++) {
                JSONObject addressObj = new JSONObject();

                addressObj.put("AddressType", addressesArray.get(i).getAddressType());
                addressObj.put("StateName", addressesArray.get(i).getStateName());
                addressObj.put("CountryId", addressesArray.get(i).getCountryId());
                addressObj.put("DistrictName", addressesArray.get(i).getDistrictName());
                addressObj.put("TehsilName", addressesArray.get(i).getTehsilName());
                addressObj.put("TownName", addressesArray.get(i).getTownName());
                addressObj.put("VillageName", addressesArray.get(i).getVillageName());
                addressObj.put("TownshipName", addressesArray.get(i).getTownName());
                addressObj.put("TownName", addressesArray.get(i).getTownName());
              /*addressObj.put("CountryName", addressesArray.get(i).getCountryName());
                addressObj.put("CityName", addressesArray.get(i).getCityName());*/
                addressObj.put("DeveloperName", addressesArray.get(i).getDeveloper_name());
                addressObj.put("Address1", addressesArray.get(i).getAddress1());
                addressObj.put("Address2", addressesArray.get(i).getAddress2());
                addressObj.put("Address3", addressesArray.get(i).getAddress3());
                addressObj.put("Tower", addressesArray.get(i).getTower());
                addressObj.put("Floor", addressesArray.get(i).getFloor());
                addressObj.put("UnitName", addressesArray.get(i).getUnitName());
                addressObj.put("ZipCode", addressesArray.get(i).getZipcode());
                addressObj.put("LocalityName", addressesArray.get(i).getLocality());
                addressObj.put("ProjectName", addressesArray.get(i).getProjectName());

                addressArray.put(addressObj);
            }

            JSONArray grouparray = new JSONArray();
            JSONObject jsonObject4 = new JSONObject();
            jsonObject4.put("Id", "");
            grouparray.put(jsonObject4);
            jsonObject.put("Phones", jsonArray);
            jsonObject.put("EmailAddresses", emailArray);
         //   jsonObject.put("ContactAttributes", socialjsonArray);
            jsonObject.put("Addresses", addressArray);
//          jsonObject.put("Groups", grouparray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

    private String getEditInputJson() {

        String inputJson = "";

        try {


            JSONObject jsonObject = new JSONObject();

            jsonObject.put("Id", contactsModels.get(0).getId());
            jsonObject.put("Prefix", contactsModels.get(0).getPrefix());
            jsonObject.put("FirstName", contactsModels.get(0).getFirstName());
            jsonObject.put("LastName", contactsModels.get(0).getLastName());
            jsonObject.put("MiddleName", contactsModels.get(0).getMiddleName());
            jsonObject.put("Id", contactsModels.get(0).getId());
            jsonObject.put("DateOfBirth", contactsModels.get(0).getDateOfBirthStr());
            jsonObject.put("Gender", contactsModels.get(0).getGender());
            jsonObject.put("CompanyId", Constants.COMPAN_ID);

            if (contactsModels.get(0).getAddresses().equals(0)) {
                jsonObject.put("IsModify", "false");
            } else {
                jsonObject.put("IsModify", "true");
            }

            if (contactsModels.get(0).getImageUrl() == null) {
                jsonObject.put("ImageUrl", "");
            } else {
                jsonObject.put("ImageUrl", contactsModels.get(0).getImageUrl());
            }
            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));


            jsonObject.put("Profession", contactsModels.get(0).getProfession());
            jsonObject.put("Designation", contactsModels.get(0).getDesignation());

            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getPhones().size(); i++) {

                JSONObject jsonObject2 = new JSONObject();

                jsonObject2.put("Id", contactsModels.get(0).getPhones().get(i).getId());
                jsonObject2.put("PhoneNumber", contactsModels.get(0).getPhones().get(i).getPhoneNumber());
                jsonObject2.put("PhoneCode", contactsModels.get(0).getPhones().get(i).getPhoneCode());
                jsonObject2.put("Mode", "");

                jsonArray.put(jsonObject2);

            }

           /* JSONArray socialjsonArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getContactAttributes().size(); i++) {


                JSONObject Twitter = new JSONObject();

                Twitter.put("Key", "IsSecure");
                Twitter.put("Id", contactsModels.get(0).getContactAttributes().get(i).getId());
//                    jsonObject1.put("PhoneType", 1);
                Twitter.put("Value", contactsModels.get(0).getContactAttributes().get(i).getValue());

                socialjsonArray.put(Twitter);


            }*/
            JSONArray emailArray = new JSONArray();
            for (int i = 0; i < contactsModels.get(0).getEmails().size(); i++) {

                JSONObject jsonObject3 = new JSONObject();

                jsonObject3.put("Id", contactsModels.get(0).getEmails().get(i).getId());
                jsonObject3.put("Address", contactsModels.get(0).getEmails().get(i).getAddress());

//                    jsonObject1.put("PhoneType", 1);
                jsonObject3.put("Label", "Email Label");

                emailArray.put(jsonObject3);

            }

            JSONArray addressArray = new JSONArray();

            for (int i = 0; i < addressesArray.size(); i++) {

                JSONObject addressObj = new JSONObject();

                addressObj.put("AddressType", addressesArray.get(i).getAddressType());
                addressObj.put("StateName", addressesArray.get(i).getStateName());
                addressObj.put("CountryId", addressesArray.get(i).getCountryId());
                addressObj.put("DistrictName", addressesArray.get(i).getDistrictName());
                addressObj.put("TehsilName", addressesArray.get(i).getTehsilName());
                addressObj.put("TownName", addressesArray.get(i).getTownName());
                addressObj.put("VillageName", addressesArray.get(i).getVillageName());
                addressObj.put("TownshipName", addressesArray.get(i).getTownName());
                addressObj.put("TownName", addressesArray.get(i).getTownName());
                addressObj.put("ProjectName", addressesArray.get(i).getProjectName());


              /*  addressObj.put("CountryName", addressesArray.get(i).getCountryName());
                addressObj.put("CityName", addressesArray.get(i).getCityName());*/
                addressObj.put("DeveloperName", addressesArray.get(i).getDeveloper_name());
                addressObj.put("Address1", addressesArray.get(i).getAddress1());
                addressObj.put("Address2", addressesArray.get(i).getAddress2());
                addressObj.put("Address3", addressesArray.get(i).getAddress3());
                addressObj.put("Tower", addressesArray.get(i).getTower());
                addressObj.put("Floor", addressesArray.get(i).getFloor());
                addressObj.put("UnitName", addressesArray.get(i).getUnitName());
                addressObj.put("ZipCode", addressesArray.get(i).getZipcode());
                addressObj.put("LocalityName", addressesArray.get(i).getLocality());
                addressObj.put("ProjectName", addressesArray.get(i).getProjectName());

                addressArray.put(addressObj);
            }

            JSONArray grouparray = new JSONArray();
            JSONObject jsonObject4 = new JSONObject();
            jsonObject4.put("Id", "");
            grouparray.put(jsonObject4);
            jsonObject.put("Phones", jsonArray);
            jsonObject.put("EmailAddresses", emailArray);
          //  jsonObject.put("ContactAttributes", socialjsonArray);
            jsonObject.put("Addresses", addressArray);
//            jsonObject.put("Groups", grouparray);

            inputJson = jsonObject.toString();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputJson;
    }

//    private String getLoginInputJson() {
//
//        String inputJson = "";
////
//
//
////
//        try {
//
//
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("Prefix", contactArraylist.get(0).getPrefix());
//            jsonObject.put("FirstName", contactArraylist.get(0).getFirstName());
//            jsonObject.put("LastName", contactArraylist.get(0).getLastName());
//            jsonObject.put("MiddleName", contactArraylist.get(0).getMiddleName());
////            jsonObject.put("Id", contactArraylist.get(0).getId());
//            jsonObject.put("Profession", contactArraylist.get(0).getProfession());
//            jsonObject.put("Designation", contactArraylist.get(0).getDesignation());
//
//            jsonObject.put("CompanyId", "1");
//            if(contactArraylist.get(0).getImageUrl()==null) {
//                jsonObject.put("ImageUrl", "");
//            }else {
//                jsonObject.put("ImageUrl", contactArraylist.get(0).getImageUrl());
//            }
//            jsonObject.put("DateOfBirth",contactArraylist.get(0).getDateOfBirthStr());
//            jsonObject.put("Gender", contactArraylist.get(0).getGender());
//
//            jsonObject.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID,""));
//            jsonObject.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID,""));
//
//
//            JSONArray addressArray = new JSONArray();
////                 getPhones=contactsFirebaseModels.get(index).getPhones();
////
////            for (int i = 0; i < addresses.size(); i++) {
//
//                JSONObject jsonObject1 = new JSONObject();
//
//                jsonObject1.put("StateName",state_type_spinner.getText().toString() );
//
////                    jsonObject1.put("PhoneType", 1);
//                jsonObject1.put("CountryName", country_type_spinner.getText().toString());
//                jsonObject1.put("CityName",city.getText().toString());
////                jsonObject1.put("Address1", addresses.get(i).getAddress1());
////                jsonObject1.put("Address2", addresses.get(i).getAddress2());
////                jsonObject1.put("Address3", addresses.get(i).getAddress3());
//
//                jsonObject1.put("DeveloperName", developer_name.getText().toString());
//                jsonObject1.put("Tower", tower.getText().toString());
//                jsonObject1.put("Floor", floor.getText().toString());
//                jsonObject1.put("UnitNo",unit_no.getText().toString());
//                jsonObject1.put("ZipCode", pincode.getText().toString());
//
//                addressArray.put(jsonObject1);
//
////            }
//            JSONArray jsonArray = new JSONArray();
//            for (int i = 0; i < contactArraylist.get(0).getPhones().size(); i++) {
//
//                JSONObject jsonObject2 = new JSONObject();
//
//                jsonObject2.put("PhoneNumber", contactArraylist.get(0).getPhones().get(i).getPhoneNumber());
//                jsonObject2.put("PhoneCode", contactArraylist.get(0).getPhones().get(i).getPhoneCode());
//
////                    jsonObject1.put("PhoneType", 1);
//                jsonObject2.put("Mode", "Work");
//
//                jsonArray.put(jsonObject2);
//
//            }
//
//            JSONArray socialjsonArray = new JSONArray();
//            for (int i = 0; i < contactArraylist.get(0).getContactAttributes().size(); i++) {
//
//
//                JSONObject Twitter = new JSONObject();
//
//                Twitter.put("Key", "IsSecure");
//
////                    jsonObject1.put("PhoneType", 1);
//                Twitter.put("Value", contactArraylist.get(0).getContactAttributes().get(i).getValue());
//
//                socialjsonArray.put(Twitter);
//
//            }
//
//
//
//
//
//
//
//
//
//            JSONArray emailArray = new JSONArray();
//            for (int i = 0; i < contactArraylist.get(0).getEmails().size(); i++) {
//
//                JSONObject jsonObject3 = new JSONObject();
//
//                jsonObject3.put("Address", contactArraylist.get(0).getEmails().get(i).getAddress());
//
////                    jsonObject1.put("PhoneType", 1);
//                jsonObject3.put("Label", "Email Label");
//
//                emailArray.put(jsonObject3);
//
//            }
//
//            JSONArray grouparray = new JSONArray();
//            JSONObject jsonObject4 = new JSONObject();
//            jsonObject4.put("Id", "");
//            grouparray.put(jsonObject4);
//            jsonObject.put("Phones", jsonArray);
//            jsonObject.put("EmailAddresses", emailArray);
//            jsonObject.put("Addresses", addressArray);
//            jsonObject.put("ContactAttributes", socialjsonArray);
//
////            jsonObject.put("Groups", grouparray);
//
//            inputJson = jsonObject.toString();
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return inputJson;
//    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {


            if (which.equalsIgnoreCase(Urls.URL_STATE_LISTING_FINAL  +"/"+ Constants.ENTITY_ID+

                    "/"+ Constants.STATE_ID+  "/" +Constants.DISTRCT_ID +"/"+ Constants.SUB_DIVISION_ID +"/"+ Constants.TEHSIL_ID +"/"+ Constants.TOWNID
                    +"/"+ Constants.TOWNSHIP_ID +"/"+ Constants.LOCALITY_ID )) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {

                            //  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();

                        } else {



                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                StateModel stateModel = new StateModel();

                                stateModel.setId(jsonObject1.getInt("Id"));
                                // stateModel.setCountryId(jsonObject1.getInt("CountryId"));
                                stateModel.setName(jsonObject1.getString("Name"));

                                stateModels.add(stateModel);

                            }
                            area_list.clear();


                            for (int i = 0; i < stateModels.size(); i++) {
                                area_list.add(stateModels.get(i).getName());
                            }
                           /* adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);

                            adapter1.addAll(area_list);
                            adapter1.add(getString(R.string.select_type));*/

                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if (which.equalsIgnoreCase(Urls.URL_CREATE_CONTACTS)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {
//                           contactArraylist.clear();
                        ContactsModel contactModel = new ContactsModel();
                        JSONObject jsonObject2 = jsonObject.getJSONObject("Result");
                        String msg = "";
                        if (jsonObject2.getBoolean("Success")) {

                            msg = jsonObject2.getString("Message");

                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObject1 = jsonObject2.getJSONObject("Result");

                            String id = jsonObject1.getString("Id");
                            String FirstName = jsonObject1.getString("FirstName");
                            String LastName = jsonObject1.getString("LastName");
                            String CompanyId = jsonObject1.getString("CompanyId");
//                            String CompanyName = jsonObject1.getString("CompanyName");
                            String DateOfBirthStr = jsonObject1.getString("DateOfBirthStr");
                            String Prefix = jsonObject1.getString("Prefix");
                            String MiddleName = jsonObject1.getString("MiddleName");

                            String Sufix = jsonObject1.getString("Sufix");
                            String MaritalStatus = jsonObject1.getString("MaritalStatus");
                            String ExistingImageUrl = jsonObject1.getString("ExistingImageUrl");


                            String Religion = jsonObject1.getString("Religion");
                            String Gender = jsonObject1.getString("Gender");
                            String Profession = jsonObject1.getString("Profession");
                            String Designation = jsonObject1.getString("Designation");
                            contactModel.setId(id);
                            contactModel.setFirstName(FirstName);
                            contactModel.setLastName(LastName);
                            contactModel.setMiddleName(MiddleName);
                            contactModel.setDateOfBirthStr(DateOfBirthStr);
                            contactModel.setPrefix(Prefix);
                            contactModel.setSufix(Sufix);
                            contactModel.setCompanyId(CompanyId);
                            contactModel.setMaritalStatus(MaritalStatus);

                            contactModel.setExistingImageUrl(ExistingImageUrl);
                            contactModel.setReligion(Religion);
                            contactModel.setGender(Gender);
                            contactModel.setProfession(Profession);
                            contactModel.setDesignation(Designation);
                            JSONArray sizeJsonArray = jsonObject1.getJSONArray("Addresses");
                            ArrayList<AddressModel> addresses = new ArrayList();
                            ContactsModel contactsModel = new ContactsModel();
                            for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                                AddressModel addresses1 = new AddressModel();
                                JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                                addresses1.setId(dataObj.getString("Id"));

                                addresses1.setCountryId(dataObj.getString("CountryId"));
                                addresses1.setStateName(dataObj.getString("StateName"));
                                addresses1.setTower(dataObj.getString("Tower"));
                                addresses1.setFloor(dataObj.getString("Floor"));
                                addresses1.setUnitName(dataObj.getString("UnitNo"));
                                addresses1.setZipcode(dataObj.getString("ZipCode"));
                                addresses1.setCountryId(dataObj.getString("CountryId"));
                                addresses1.setProjectName(dataObj.getString("ProjectName"));

//                                addresses1.setFirstName(dataObj.getString("FirstName"));
//                                addresses1.setLastName(dataObj.getString("LastName"));
//                                addresses1.setEmail(dataObj.getString("Email"));
//                                addresses1.setCompany(dataObj.getString("Company"));
//                                addresses1.setCityName(dataObj.getString("CityName"));
                                addresses1.setAddress1(dataObj.getString("Address1"));
                                addresses1.setAddress2(dataObj.getString("Address2"));
                                addresses1.setDeveloper_name(dataObj.getString("DeveloperName"));
//                                addresses1.setPhoneNumber(dataObj.getString("PhoneNumber"));
//                                addresses1.setFaxNumber(dataObj.getString("FaxNumber"));
                                addresses.add(addresses1);
                            }
                            JSONArray phonesJsonArray = jsonObject1.getJSONArray("Phones");
                            ArrayList<ContactsModel.Phones> phones = new ArrayList();
                            for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                                ContactsModel.Phones phones1 = contactsModel.new Phones();
                                JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                                phones1.setId(dataObj.getString("Id"));
                                phones1.setContactId(dataObj.getString("ContactId"));
                                phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
                                phones1.setPhoneType(dataObj.getString("Mode"));
                                phones.add(phones1);

                            }
                            JSONArray emailJsonArray = jsonObject1.getJSONArray("EmailAddresses");
                            ArrayList<ContactsModel.Email> emails = new ArrayList();
                            for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                                ContactsModel.Email email = contactsModel.new Email();
                                JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                                email.setId(dataObj.getString("Id"));
                                email.setContactId(dataObj.getString("ContactId"));
                                email.setAddress(dataObj.getString("Address"));
                                email.setLabel(dataObj.getString("Label"));
//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                emails.add(email);
                            }

                         /*   JSONArray socialJsonArray = jsonObject1.getJSONArray("ContactAttributes");
                            ArrayList<ContactsModel.ContactAttributes> socialArray = new ArrayList();
                            for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                                ContactsModel.ContactAttributes contactAttributes = contactsModel.new ContactAttributes();
                                JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                                contactAttributes.setKey(dataObj.getString("Key"));
                                contactAttributes.setValue(dataObj.getString("Value"));

//                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));


                                socialArray.add(contactAttributes);
                            }*/

                           /* JSONArray socialJsonArray = jsonObject2.getJSONArray("ContactSocialDetails");
                            ArrayList<ContactSocialDetails> socialArray = new ArrayList();
                            for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                                ContactSocialDetails contactAttributes = new ContactSocialDetails();
                                JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                                contactAttributes.setType(dataObj.getString("Type"));
                                contactAttributes.setDetail(dataObj.getString("Detail"));

                                socialArray.add(contactAttributes);
                            }*/
                            contactModel.setPhones(phones);
                            contactModel.setAddresses(addresses);
                            contactModel.setEmails(emails);
                       //    contactModel.setContactSocialDetails(socialArray);
                            contactsModels = new ArrayList<>();
                            contactsModels.add(contactModel);
                            Toast.makeText(getActivity(), "Address contact information updated Successfully", Toast.LENGTH_SHORT).show();

                            if (edit_data.equalsIgnoreCase("")) {
                                SM.sendData(contactsModels, 3);
                            } else {
                                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                                intent.putExtra("contact_id", contactsModels.get(0).getId());
                                startActivity(intent);
                                getActivity().finish();

                            }

                        } else {

                            msg = jsonObject2.getString("Message");

                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

/*
            else if (which.equalsIgnoreCase(Urls.URL_GET_TERMS_DATA)) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        allGroupsModels = new ArrayList<>();
                        JSONArray projectsArray = jsonObject.getJSONArray("Result");
                        for (int index = 0; index < projectsArray.length(); index++) {
                            JSONObject jsonObject1 = projectsArray.getJSONObject(index);

                            AllGroupsModel allGroupsModel = new AllGroupsModel();
                            allGroupsModel.setId(jsonObject1.getString("Id"));
                            allGroupsModel.setName(jsonObject1.getString("name"));
                            allGroupsModel.setDescription(jsonObject1.getString("Description"));
                            allGroupsModels.add(allGroupsModel);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
*/

            if (which.equalsIgnoreCase(Urls.URL_GET_COUNTRY_LIST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                            country_list.clear();
                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                CountryModel countryModel = new CountryModel();

                                countryModel.setId(jsonObject1.getString("Id"));
                                countryModel.setName(jsonObject1.getString("Name"));
                                countryModel.setDescription(jsonObject1.getString("Description"));
                                country_list.add(countryModel);
                                country_name.add(country_list.get(index).getDescription());

                            }

                            addMoreAddressAdapter = new AddMoreAddressAdapter(getActivity(), country_name, country_list);
                            add_more_Addresses_recycler.setAdapter(addMoreAddressAdapter);
                            countrtySpinnerAdapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
                            countrtySpinnerAdapter.addAll(country_name);

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {
                            String message = jsonObject1.getString("Message");

                            Utils.showSuccessErrorMessage("Success", "Draft saved successfully", "Ok", getActivity());
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

//
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public class AddMoreAddressAdapter extends RecyclerView.Adapter<AddMoreAddressAdapter.ViewHolder> {

        Context context;
        private ArrayList<String> specificationModels = new ArrayList();
        private ArrayList<StatusModel> editTextArrayList = new ArrayList();
        SpinnerAdapter spinnerAdapter;
        ArrayList<String> room_name = new ArrayList<>();
        ArrayList<CountryModel> propertyBedroomModels = new ArrayList<>();

        public AddMoreAddressAdapter(FragmentActivity activity) {
//          this.addresses=addresses;
            this.context = activity;

        }

        public AddMoreAddressAdapter(Context context, ArrayList<String> specificationModels, ArrayList<CountryModel> propertyBedroomModels) {
            this.context = context;
            this.specificationModels = specificationModels;
            this.propertyBedroomModels = propertyBedroomModels;
            for (int i = 0; i < specificationModels.size(); i++) {
                CountryModel statusModel = new CountryModel();
                statusModel.setDescription(specificationModels.get(i));
                statusModel.setName("");
            }
            for (int i = 0; i < propertyBedroomModels.size(); i++) {
                room_name.add(propertyBedroomModels.get(i).getDescription());
            }

            spinnerAdapter = new SpinnerAdapter(context, R.layout.new_spinner_item);
            spinnerAdapter.addAll(room_name);

            spinnerAdapter.add(context.getString(R.string.select_type));


        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.more_addresses_layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            if (addressesArray.get(position).getAddressType().equalsIgnoreCase("Home"))
            holder.address_type_spinner.setSelection(1);
            else if (addressesArray.get(position).getAddressType().equalsIgnoreCase("Office"))
                holder.address_type_spinner.setSelection(2);
            else
                holder.address_type_spinner.setSelection(0);
            holder.state.setText(addressesArray.get(position).getStateName());
            holder.et_projectname.setText(addressesArray.get(position).getProjectName());

            if (!addressesArray.get(position).getCountryId().equalsIgnoreCase(""))
            holder.country_spinner.setSelection(Integer.parseInt(addressesArray.get(position).getCountryId()));
            holder.tower.setText(addressesArray.get(position).getTower());
            holder.floor.setText(addressesArray.get(position).getFloor());
            holder.unit_no.setText(addressesArray.get(position).getUnitName());
            holder.city.setText(addressesArray.get(position).getTownName());
            holder.pincode.setText(addressesArray.get(position).getZipcode());
            holder.locality.setText(addressesArray.get(position).getLocality());
            holder.et_townshipname.setText(addressesArray.get(position).getTehsilName());
            holder.developer_name.setText(addressesArray.get(position).getDeveloper_name());
            holder.country_spinner.setAdapter(countrtySpinnerAdapter);
            holder.country_type_spinner.setAdapter(countrtySpinnerAdapter);
            holder.state.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                    // stateareaEntitiyModelsTemp.clear();
                    stateModelsTemp.clear();
                    List<StateModel> list = filter(s.toString(), stateModels, true);

                    stateModelsTemp.addAll(list);
                    if (stateModelsTemp.isEmpty()) {
//                    Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
                    } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
                        autocompleteAdapter = new AutocompleteAdapter(getActivity(), R.layout.entity_item, stateModelsTemp, s.toString());
                        holder.state.setAdapter(autocompleteAdapter);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {


                }
            });

            new MultiTextWatcher()
                    .registerEditText(holder.tower)
                    .registerEditText(holder.floor)
                    .registerEditText(holder.unit_no)
                    .registerEditText(holder.city)
                    .registerEditText(holder.pincode)
                    .registerEditText(holder.locality)
                    .registerEditText(holder.et_townshipname)
                    .registerEditText(holder.state)
                    .registerEditText(holder.developer_name)
                    .setCallback(new MultiTextWatcher.TextWatcherWithInstance() {
                        @Override
                        public void beforeTextChanged(EditText editText, CharSequence s, int start, int count, int after) {
                            // TODO: Do some thing with editText
                        }

                        @Override
                        public void onTextChanged(EditText editText, CharSequence s, int start, int before, int count) {
                            // TODO: Do some thing with editText
                        }

                        @Override
                        public void afterTextChanged(EditText editText, Editable editable) {
                            // TODO: Do some thing with editText

                            final AddressModel addressModel = new AddressModel();

                            addressModel.setAddressType(holder.address_type_spinner.getSelectedItem().toString());
                            addressModel.setCountryId(country_list.get(holder.country_spinner.getSelectedItemPosition()).getId());

                            for (CountryModel countryModel : country_list) {

                                if (countryModel.getDescription().equalsIgnoreCase(holder.country_spinner.getSelectedItem().toString())) {

                                    addressModel.setCountryId(countryModel.getId());

                                }
                            }

                            addressModel.setTower(holder.tower.getText().toString());
                            addressModel.setFloor(holder.floor.getText().toString());
                            addressModel.setUnitName(holder.unit_no.getText().toString());
                            addressModel.setStateName(holder.state.getText().toString());
                            addressModel.setZipcode(holder.pincode.getText().toString());
                            addressModel.setBuilding_name(holder.et_projectname.getText().toString());
                            addressModel.setLocality(holder.locality.getText().toString());
                            addressModel.setTehsil(holder.et_townshipname.getText().toString());
                            addressModel.setTownName(holder.et_townshipname.getText().toString());
                            addressModel.setDeveloper_name(holder.developer_name.getText().toString());
                            addressModel.setProjectName(holder.et_projectname.getText().toString());

                            addressesArray.set(position, addressModel);
                        }
                    });


        }

        @Override
        public int getItemCount() {
            return addressesArray.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            EditText tower, floor, unit_no, city, pincode, et_projectname, locality, et_townshipname, developer_name;
            Spinner address_type_spinner, country_spinner;
            EditText state_type_spinner;
            AutoCompleteTextView country_type_spinner;
            AutoCompleteEditext state ;

            public ViewHolder(View item) {
                super(item);
                address_type_spinner = (Spinner) item.findViewById(R.id.address_type_spinner);
                country_spinner = (Spinner) item.findViewById(R.id.country_spinner);
                state_type_spinner = (EditText) item.findViewById(R.id.state_type_spinner);
                country_type_spinner = (AutoCompleteTextView) item.findViewById(R.id.country_type_spinner);
                tower = (EditText) item.findViewById(R.id.tower);
                floor = (EditText) item.findViewById(R.id.floor);
                unit_no = (EditText) item.findViewById(R.id.unit_no);
                city = (EditText) item.findViewById(R.id.city);
                pincode = (EditText) item.findViewById(R.id.pincode);
                et_projectname = (EditText) item.findViewById(R.id.et_projectname);
                locality = (EditText) item.findViewById(R.id.locality);
                et_townshipname = (EditText) item.findViewById(R.id.et_townshipname);
                developer_name = (EditText) item.findViewById(R.id.developer_name);
                state = (AutoCompleteEditext) item.findViewById(R.id.state);

            }
        }

    }

}