package com.bnhabitat.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.ui.activities.SearchProjectsActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;

import java.util.ArrayList;

/**
 * Created by Android on 6/30/2017.
 */

public class SettingsDialogFragment extends DialogFragment {
    private  ImageView flag_image;

    TextView change_currency, search, filter, set_default_sizes;

    private ArrayList<ProjectsModel> searchProjectsModels = new ArrayList();

    public static SettingsDialogFragment newInstance() {
        return new SettingsDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.setting_dialog_fragment, container, false);
        change_currency = (TextView) v.findViewById(R.id.change_currency);
        search = (TextView) v.findViewById(R.id.search);
        flag_image = (ImageView) v.findViewById(R.id.flag_image);
        filter = (TextView) v.findViewById(R.id.filter);
        set_default_sizes = (TextView) v.findViewById(R.id.set_default_sizes);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            searchProjectsModels = (ArrayList<ProjectsModel>) bundle.getSerializable("Arraylist");
        }

        try{
            if(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME,"").equalsIgnoreCase("USD")){
                flag_image.setImageDrawable(getResources().getDrawable(R.drawable.us_flag));
            }else if(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME,"").equalsIgnoreCase("CAD")){
                flag_image.setImageDrawable(getResources().getDrawable(R.drawable.can_flag));
            }else if(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME,"").equalsIgnoreCase("EUR")){
                flag_image.setImageDrawable(getResources().getDrawable(R.drawable.uae_flag));
            }else if(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME,"").equalsIgnoreCase("AUD")){
                flag_image.setImageDrawable(getResources().getDrawable(R.drawable.aus_flag));
            }else if(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DEFAULT_CURRENCY_NAME,"").equalsIgnoreCase("GBP")){
                flag_image.setImageDrawable(getResources().getDrawable(R.drawable.uk_flag));
            }else{
                flag_image.setImageDrawable(getResources().getDrawable(R.drawable.small_flag));
            }
        }catch (Exception e){

        }
        change_currency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ChangeCurrencyDialogFragment filterDialogFragment = ChangeCurrencyDialogFragment.newInstance();
                filterDialogFragment.show(getActivity().getSupportFragmentManager(), "dialog_fragment");
                dialogDismiss();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SearchProjectsActivity.class);
                startActivityForResult(intent, 11);
                dialogDismiss();
            }
        });

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FilterDialogFragment filterDialogFragment = FilterDialogFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Arraylist", searchProjectsModels);
                filterDialogFragment.setArguments(bundle);
                filterDialogFragment.show(getActivity().getSupportFragmentManager(), "dialog_fragment");

                dialogDismiss();

            }
        });

        set_default_sizes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SetDefaultSizeDialogFragment filterDialogFragment = SetDefaultSizeDialogFragment.newInstance();
                filterDialogFragment.show(getActivity().getSupportFragmentManager(), "dialog_fragment");

                dialogDismiss();
            }
        });


        return v;
    }

    public void dialogDismiss() {


        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog_fragment");
        if (prev != null) {
            SettingsDialogFragment df = (SettingsDialogFragment) prev;
            df.dismiss();
        }
    }
}