package com.bnhabitat.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bnhabitat.R;
import com.bnhabitat.models.CustomDialogModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.adapters.DialogFragmentAdapter;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Android on 5/16/2017.
 */

public class CustomDialogFragment extends DialogFragment {
    private RecyclerView mRecyclerView;
    private DialogFragmentAdapter adapter;
    TextView cancel;
    String id;
    int mYear,mMonth,mDay;
    ImageView callback_rqst,request;

    private int STORAGE_PERMISSION_CODE = 23;
    ArrayList<CustomDialogModel> customDialogModels = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagers = new ArrayList();

    public static CustomDialogFragment newInstance() {
        return new CustomDialogFragment();
    }

    // this method create view for your Dialog
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflate layout with recycler view
        View v = inflater.inflate(R.layout.fragment_dialog, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.dialog_recycler_view);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        cancel = (TextView) v.findViewById(R.id.cancel);
        callback_rqst=(ImageView)v.findViewById(R.id.callback_rqst);
        request=(ImageView)v.findViewById(R.id.request);
        Bundle bundle = this.getArguments();
        if (bundle != null) {

            projectRelationshipManagers = (ArrayList<ProjectsDetailModel.ProjectRelationshipManagers>) bundle.getSerializable("Arraylist");
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            //setadapter
            DialogFragmentAdapter adapter = new DialogFragmentAdapter(getActivity(), projectRelationshipManagers);
            mRecyclerView.setAdapter(adapter);
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog_fragment");
                if (prev != null) {
                    CustomDialogFragment df = (CustomDialogFragment) prev;
                    df.dismiss();
                }
            }
        });
        callback_rqst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRequestDialog();
            }
        });
        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRequestDialog();
            }
        });
        //get your recycler view and populate it.
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();


    }
    private void showRequestDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.request_popup);
        dialog.show();
        RadioGroup request_group=(RadioGroup)dialog.findViewById(R.id.request_group);
        RadioButton specific_time=(RadioButton)dialog.findViewById(R.id.specific_time);
        RadioButton asap=(RadioButton)dialog.findViewById(R.id.asap);
        RadioButton any_time=(RadioButton)dialog.findViewById(R.id.any_time);
        Button done=(Button) dialog.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        request_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if(i==R.id.specific_time){
                    showtimeDialog();
                    dialog.cancel();
                }
            }
        });

    }
    private void showtimeDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.date_time_popup);
        dialog.show();

        final TextView date_set=(TextView)dialog.findViewById(R.id.date_set);
        final TextView time_between=(TextView)dialog.findViewById(R.id.time_between);
        final TextView time_to=(TextView)dialog.findViewById(R.id.time_to);
        Button done_specific=(Button) dialog.findViewById(R.id.done_specific);
        done_specific.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        date_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentDate=Calendar.getInstance();
                mYear=mcurrentDate.get(Calendar.YEAR);
                mMonth=mcurrentDate.get(Calendar.MONTH)+1;
                mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                        date_set.setText(selectedday+"/"+selectedmonth+"/"+selectedyear);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();  }

        });
        time_between.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time_between.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        time_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time_to.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

    }
}

