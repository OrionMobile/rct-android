package com.bnhabitat.ui.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.ContactSocialDetails;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.RelationModel;
import com.bnhabitat.ui.activities.CompleteContactInfoActivity;
import com.bnhabitat.ui.adapters.RelationAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.bnhabitat.ui.activities.ProfileActivity.contact_id;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterContactFragment extends Fragment implements CommonAsync.OnAsyncResultListener {
    View view;
    ArrayList<ContactsModel> contactsModels = new ArrayList<>();
    ImageView edit_contact_source, edit_profession, edit_address, add_contact, back, photo_edit;
    CircleImageView profile_img;
    TextView name, age, domain, email, phn, verified, profession, industry, designation,
            annual_income, tower, unit_no, city, pincode, state, country, building_name, tehsil, developer_name,
            contact_source, date, campaign, tag, owner, notes;
    //relation_name, relation;
    ImageLoader imageLoader;
    ArrayList<RelationModel> relationArrayList = new ArrayList<>();
    RelationAdapter relationAdapter;
    RecyclerView rv_relation;
    LinearLayoutManager linearLayoutManager;
    DisplayImageOptions options;
    RelativeLayout rl_edit;
    ImageView iv_fb, iv_twitter;
    String twitterLink= "" , fbLink = "" ;

    public RegisterContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_register_contact, container, false);

        init();

        onClicks();

        return view;
    }
    private void  init(){

        iv_twitter = (ImageView) view.findViewById(R.id.iv_twitter);
        iv_fb = (ImageView) view.findViewById(R.id.iv_fb);
        photo_edit = (ImageView) view.findViewById(R.id.photo_edit);
        domain = (TextView) view.findViewById(R.id.domain);
        relationAdapter = new RelationAdapter(getActivity(), relationArrayList);
        rv_relation = (RecyclerView) view.findViewById(R.id.rv_relation);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        relationAdapter = new RelationAdapter(getActivity(), relationArrayList);
        rv_relation.setLayoutManager(linearLayoutManager);
        rv_relation.setAdapter(relationAdapter);
        edit_contact_source = (ImageView) view.findViewById(R.id.edit_contact_source);
        edit_profession = (ImageView) view.findViewById(R.id.edit_profession);
        edit_address = (ImageView) view.findViewById(R.id.edit_address);
        name = (TextView) view.findViewById(R.id.name);
        age = (TextView) view.findViewById(R.id.age);
        domain = (TextView) view.findViewById(R.id.domain);
        email = (TextView) view.findViewById(R.id.email);
        phn = (TextView) view.findViewById(R.id.phn);
        verified = (TextView) view.findViewById(R.id.verified);
        rl_edit = (RelativeLayout) view.findViewById(R.id.rl_edit);

      /*  relation = (TextView) view.findViewById(R.id.relation);
        relation_name = (TextView) view.findViewById(R.id.relation_name);*/
        profession = (TextView) view.findViewById(R.id.profession);
        industry = (TextView) view.findViewById(R.id.industry);
        designation = (TextView) view.findViewById(R.id.designation);
        tower = (TextView) view.findViewById(R.id.tower);
        unit_no = (TextView) view.findViewById(R.id.unit_no);
        city = (TextView) view.findViewById(R.id.city);
        pincode = (TextView) view.findViewById(R.id.pincode);
        state = (TextView) view.findViewById(R.id.state);
        country = (TextView) view.findViewById(R.id.country);
        building_name = (TextView) view.findViewById(R.id.building_name);
        tehsil = (TextView) view.findViewById(R.id.tehsil);
        developer_name = (TextView) view.findViewById(R.id.developer_name);
        annual_income = (TextView) view.findViewById(R.id.annual_income);
        contact_source = (TextView) view.findViewById(R.id.contact_source);
        date = (TextView) view.findViewById(R.id.date);
        campaign = (TextView) view.findViewById(R.id.campaign);
        tag = (TextView) view.findViewById(R.id.tag);
        owner = (TextView) view.findViewById(R.id.owner);
        // group = (TextView) findViewById(R.id.group);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        notes = (TextView) view.findViewById(R.id.notes);


        rv_relation = (RecyclerView) view.findViewById(R.id.rv_relation);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        relationAdapter = new RelationAdapter(getActivity(), relationArrayList);
        rv_relation.setLayoutManager(linearLayoutManager);
        rv_relation.setAdapter(relationAdapter);
        edit_contact_source = (ImageView) view.findViewById(R.id.edit_contact_source);
        edit_profession = (ImageView) view.findViewById(R.id.edit_profession);
        photo_edit = (ImageView) view.findViewById(R.id.photo_edit);
        profile_img = (CircleImageView) view.findViewById(R.id.profile_img);
    }

    private void onClicks(){

        rl_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(getActivity(), CompleteContactInfoActivity.class);
                    intent.putExtra("position", "0");
                    intent.putExtra("ModelName", contactsModels);
                    intent.putExtra("edit", "edit_data");
                    intent.putExtra("contact_id", contact_id);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        edit_contact_source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CompleteContactInfoActivity.class);
                intent.putExtra("position", "3");
                intent.putExtra("ModelName", contactsModels);
                intent.putExtra("contact_id", contact_id);
                intent.putExtra("edit", "edit_data");
                startActivity(intent);
                getActivity().finish();
            }
        });
        edit_profession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CompleteContactInfoActivity.class);
                intent.putExtra("position", "1");
                intent.putExtra("ModelName", contactsModels);
                intent.putExtra("contact_id", contact_id);
                intent.putExtra("edit", "edit_data");
                startActivity(intent);
                getActivity().finish();
            }
        });
        edit_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CompleteContactInfoActivity.class);
                intent.putExtra("position", "2");
                intent.putExtra("ModelName", contactsModels);
                intent.putExtra("contact_id", contact_id);
                intent.putExtra("edit", "edit_data");
                startActivity(intent);
                getActivity().finish();
            }
        });

        iv_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fbLink.equalsIgnoreCase("" ) && fbLink.equalsIgnoreCase("https://www.facebook.com/") ){

                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                }

                else {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+fbLink));
                    getActivity().startActivity(browserIntent);
                }
            }
        });
        iv_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (twitterLink.equalsIgnoreCase("") && twitterLink.equalsIgnoreCase("https://twitter.com/")){

                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
                }

                else {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://"+twitterLink));
                    getActivity().startActivity(browserIntent);
                }

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        onGetDetail();
    }

    private void onGetDetail() {

//        String url = URLEncoder.encode( Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.USER_ID, "") +"/"+Constants.APP_ID+ "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(this).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'&IsDeleted eq false");

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_CONTACT_DETAIL + contact_id + "/" + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""), "", "Loading...", this, Urls.URL_CONTACT_DETAIL, Constants.GET).execute();


    }

    @Override
    public void onResultListener(String result, String which) {

        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_CONTACT_DETAIL)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                    ContactsModel contactDetailModel = new ContactsModel();
                    contactDetailModel.setId(Utils.getFilteredValue(jsonObject1.getString("Id")));
                    contactDetailModel.setFirstName(Utils.getFilteredValue(jsonObject1.getString("FirstName")));
                    contactDetailModel.setLastName(Utils.getFilteredValue(jsonObject1.getString("LastName")));
                    contactDetailModel.setPrefix(Utils.getFilteredValue(jsonObject1.getString("Prefix")));
                    contactDetailModel.setMiddleName(Utils.getFilteredValue(jsonObject1.getString("MiddleName")));
                    contactDetailModel.setSufix(Utils.getFilteredValue(jsonObject1.getString("Sufix")));
                    contactDetailModel.setMaritalStatus(Utils.getFilteredValue(jsonObject1.getString("MaritalStatus")));
                    contactDetailModel.setMarriageAnniversaryDate(Utils.getFilteredValue(jsonObject1.getString("MarriageAnniversaryDate")));
                    contactDetailModel.setMarriageAnniversaryDateString(Utils.getFilteredValue(jsonObject1.getString("MarriageAnniversaryDateString")));
                    contactDetailModel.setSourceName(Utils.getFilteredValue(jsonObject1.getString("SourceName")));
                    contactDetailModel.setCampaign(Utils.getFilteredValue(jsonObject1.getString("Campaign")));
                    contactDetailModel.setSourceDate(Utils.getFilteredValue(jsonObject1.getString("SourceDateString")));
                    contactDetailModel.setOwnerShip(Utils.getFilteredValue(jsonObject1.getString("OwnerShip")));
                    contactDetailModel.setOwnerUserId(Utils.getFilteredValue(jsonObject1.getString("OwnerUserId")));
                    contactDetailModel.setIndustryName(Utils.getFilteredValue(jsonObject1.getString("IndustryName")));
                    contactDetailModel.setDesignation(Utils.getFilteredValue(jsonObject1.getString("Designation")));
                    contactDetailModel.setAnnualIncome(Utils.getFilteredValue(jsonObject1.getString("IndustryName")));
                    contactDetailModel.setAnnualIncome(Utils.getFilteredValue(jsonObject1.getString("AnnualIncome")));
                    contactDetailModel.setCompanyId(Utils.getFilteredValue(jsonObject1.getString("CompanyId")));
                    contactDetailModel.setCompanyName(Utils.getFilteredValue(jsonObject1.getString("CompanyName")));
                    contactDetailModel.setProfession(Utils.getFilteredValue(jsonObject1.getString("Profession")));
                    contactDetailModel.setReligion(Utils.getFilteredValue(jsonObject1.getString("Religion")));
                    contactDetailModel.setDateOfBirthStr(Utils.getFilteredValue(jsonObject1.getString("DateOfBirthStr")));
                    contactDetailModel.setGender(Utils.getFilteredValue(jsonObject1.getString("Gender")));
                    if ((jsonObject1.getString("ImageUrl").equalsIgnoreCase("None"))){

                        contactDetailModel.setImageUrl("");

                    }

                    else
                        contactDetailModel.setImageUrl(jsonObject1.getString("ImageUrl"));

                    if (!contactDetailModel.getLastName().equalsIgnoreCase("None"))
                        name.setText(contactDetailModel.getFirstName() + " " + contactDetailModel.getLastName());
                    else
                        name.setText(contactDetailModel.getFirstName());
                    if (!contactDetailModel.getImageUrl().equalsIgnoreCase("None"))

                        imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + contactDetailModel.getImageUrl(), profile_img, options);


                    else
                        profile_img.setImageResource(R.drawable.user_profile_img);
                    age.setText(contactDetailModel.getGender());
                    JSONArray sizeJsonArray = jsonObject1.getJSONArray("Addresses");
                    ArrayList<AddressModel> addresses = new ArrayList();
                    ContactsModel contactsModel = new ContactsModel();
                    if (sizeJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {

                            AddressModel addresses1 = new AddressModel();
                            JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);

                            addresses1.setId(Utils.getFilteredValue(dataObj.getString("Id")));

                            addresses1.setCountryId(Utils.getFilteredValue(dataObj.getString("CountryId")));
                            addresses1.setStateName(Utils.getFilteredValue(dataObj.getString("StateName")));
                            addresses1.setTower(Utils.getFilteredValue(dataObj.getString("Tower")));
                            addresses1.setFloor(Utils.getFilteredValue(dataObj.getString("Floor")));
                            addresses1.setZipcode(Utils.getFilteredValue(dataObj.getString("ZipCode")));
                            addresses1.setUnitName(Utils.getFilteredValue(dataObj.getString("UnitNo")));
                            addresses1.setCity_name(Utils.getFilteredValue(dataObj.getString("CityName")));
                            addresses1.setAddress1(Utils.getFilteredValue(dataObj.getString("Address1")));
                            addresses1.setAddress2(Utils.getFilteredValue(dataObj.getString("Address2")));
                            addresses1.setDeveloper_name(Utils.getFilteredValue(dataObj.getString("DeveloperName")));
//                                addresses1.setPhoneNumber(dataObj.getString("PhoneNumber"));
//                                addresses1.setFaxNumber(dataObj.getString("FaxNumber"));
                            addresses.add(addresses1);
                        }
                    JSONArray phonesJsonArray = jsonObject1.getJSONArray("Phones");
                    ArrayList<ContactsModel.Phones> phones = new ArrayList();
                    if (phonesJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {

                            ContactsModel.Phones phones1 = contactsModel.new Phones();
                            JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);

                            phones1.setId(Utils.getFilteredValue(dataObj.getString("Id")));
                            phones1.setContactId(Utils.getFilteredValue(dataObj.getString("ContactId")));
                            phones1.setPhoneNumber(Utils.getFilteredValue(dataObj.getString("PhoneNumber")));
                            phones1.setPhoneCode(Utils.getFilteredValue(dataObj.getString("PhoneCode")));
                            phones1.setPhoneType(Utils.getFilteredValue(dataObj.getString("Mode")));
//
                            phones.add(phones1);

                        }
                    JSONArray contactRelationsArray = jsonObject1.getJSONArray("ContactRelations");
                    ArrayList<ContactsModel.ContactRelations> contactRelations = new ArrayList();
                    if (contactRelationsArray.length() != 0)
                        for (int indexJ = 0; indexJ < contactRelationsArray.length(); indexJ++) {

                            ContactsModel.ContactRelations contactRelations1 = contactsModel.new ContactRelations();
                            JSONObject dataObj = contactRelationsArray.getJSONObject(indexJ);

                            contactRelations1.setRelation(Utils.getFilteredValue(dataObj.getString("Relation")));
                            contactRelations1.setRelationFirstName(Utils.getFilteredValue(dataObj.getString("RelationFirstName")));
                            contactRelations1.setRelationLastName(Utils.getFilteredValue(dataObj.getString("RelationLastName")));
                            contactRelations1.setId(Utils.getFilteredValueInt(dataObj.getInt("ContactId")));
                            contactRelations.add(contactRelations1);

                        }
                    JSONArray emailJsonArray = jsonObject1.getJSONArray("EmailAddresses");
                    ArrayList<ContactsModel.Email> emails = new ArrayList();
                    if (emailJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {

                            ContactsModel.Email email = contactsModel.new Email();
                            JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);

                            email.setId(Utils.getFilteredValue(dataObj.getString("Id")));
                            email.setContactId(Utils.getFilteredValue(dataObj.getString("ContactId")));
                            email.setAddress(Utils.getFilteredValue(dataObj.getString("Address")));
                            email.setLabel(Utils.getFilteredValue(dataObj.getString("Label")));
                            emails.add(email);
                        }

                  /*  JSONArray socialJsonArray = jsonObject1.getJSONArray("ContactAttributes");
                    ArrayList<ContactsModel.ContactAttributes> socialArray = new ArrayList();
                    if (socialJsonArray.length() != 0)
                        for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                            ContactsModel.ContactAttributes contactAttributes = contactsModel.new ContactAttributes();
                            JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);
                            contactAttributes.setKey(Utils.getFilteredValue(dataObj.getString("Key")));
                            contactAttributes.setValue(Utils.getFilteredValue(dataObj.getString("Value")));

//                          phones1.setIsPrimary(dataObj.getString("IsPrimary"));

                            socialArray.add(contactAttributes);
                        }
*/
                    JSONArray socialJsonArray = jsonObject1.getJSONArray("ContactSocialDetails");
                    ArrayList<ContactSocialDetails> socialArray = new ArrayList();
                    for (int indexJ = 0; indexJ < socialJsonArray.length(); indexJ++) {

                        ContactSocialDetails contactAttributes = new ContactSocialDetails();
                        JSONObject dataObj = socialJsonArray.getJSONObject(indexJ);

                        contactAttributes.setType(dataObj.getString("Type"));
                        contactAttributes.setDetail(dataObj.getString("Detail"));

                        socialArray.add(contactAttributes);
                    }
                    contactDetailModel.setPhones(phones);
                    contactDetailModel.setAddresses(addresses);
                    contactDetailModel.setEmails(emails);
                    contactDetailModel.setContactRelationses(contactRelations);
                    contactDetailModel.setContactSocialDetails(socialArray);
                    contactsModels.add(contactDetailModel);


                    if (!contactsModels.get(0).getAddresses().isEmpty()) {
                        tower.setText(contactsModels.get(0).getAddresses().get(0).getTower());
                        unit_no.setText(contactsModels.get(0).getAddresses().get(0).getUnitName());
                        city.setText(contactsModels.get(0).getAddresses().get(0).getTownName());
                        pincode.setText(contactsModels.get(0).getAddresses().get(0).getZipcode());
                        state.setText(contactsModels.get(0).getAddresses().get(0).getStateName());
                        country.setText(contactsModels.get(0).getAddresses().get(0).getCountryName());
                        developer_name.setText(contactsModels.get(0).getAddresses().get(0).getDeveloper_name());
                        tehsil.setText(contactsModels.get(0).getAddresses().get(0).getTehsilName());
                        building_name.setText(contactsModels.get(0).getAddresses().get(0).getBuilding_name());

                    } else {
                        tower.setText("None");
                        unit_no.setText("None");
                        // city.setText("None");
                        pincode.setText("None");
                        state.setText("None");
                        country.setText("None");
                        developer_name.setText("None");
                        tehsil.setText("None");
                        building_name.setText("None");


                    }

                    JSONArray relationArray = jsonObject1.getJSONArray("ContactRelations");

                    relationArrayList.clear();

                    if (!(relationArray.length() == 0)) {

                        for (int i = 0; i < relationArray.length(); i++) {

                            JSONObject r1 = relationArray.getJSONObject(i);

                            RelationModel relationModel = new RelationModel();

                            relationModel.setRelationFirstName(r1.getString("RelationFirstName"));
                            relationModel.setRelationLastName(r1.getString("RelationLastName"));
                            relationModel.setRelation(r1.getString("Relation"));


                            relationArrayList.add(relationModel);


                        }


                    } else {
                        //  relation.setText("None");
                        //  relation_name.setText("None");
                    }
                    relationAdapter = new RelationAdapter(getActivity(), relationArrayList);
                    rv_relation.setLayoutManager(linearLayoutManager);
                    rv_relation.setAdapter(relationAdapter);
                    if (!contactsModels.get(0).getEmails().isEmpty()) {
                        email.setText(contactsModels.get(0).getEmails().get(0).getAddress());
                    } else {
                        email.setText("None");
                    }


                 /*   if (!contactsModels.get(0).getContactAttributes().isEmpty()) {
                        domain.setText(contactsModels.get(0).getContactAttributes().get(0).getValue());
                    } else {
                        domain.setText("None");
                    }
*/

                    if (!contactsModels.get(0).getContactSocialDetails().isEmpty()) {

                        for (int i=0;i<contactsModels.get(0).getContactSocialDetails().size();i++){

                            if (contactsModels.get(0).getContactSocialDetails().get(i).getType().equalsIgnoreCase("website")){

                                domain.setText(contactsModels.get(0).getContactSocialDetails().get(0).getDetail());

                            }

                        }

                    }

                    if (!contactsModels.get(0).getPhones().isEmpty()) {
                        for (int i = 0; i < contactsModels.get(0).getPhones().size(); i++) {

                            phn.setText(contactDetailModel.getPhones().get(0).getPhoneNumber());
                        }
                    } else {
                        phn.setText("None");
                    }

                    if (!contactsModels.get(0).getEmails().isEmpty()) {
                        for (int i = 0; i < contactsModels.get(0).getEmails().size(); i++) {

                            email.setText(contactDetailModel.getEmails().get(0).getAddress());
                        }


                    } else {
                        email.setText("None");
                    }


                    profession.setText(contactDetailModel.getProfession());
                    designation.setText(contactDetailModel.getDesignation());
                    industry.setText(contactDetailModel.getIndustryName());
                    annual_income.setText(contactDetailModel.getAnnualIncome());
                    contact_source.setText(contactDetailModel.getSourceName());

                    date.setText(contactDetailModel.getSourceDate());


                    campaign.setText(contactDetailModel.getCampaign());
                    if (!contactDetailModel.getContactNotes().isEmpty())
                        notes.setText(contactDetailModel.getContactNotes().get(0).getNote());


//                    /*
//                     *
//                     * remove null values
//                     *
//                     * */
//                    ArrayList<ContactsModel.ContactRelations> tempRelationsArray = contactDetailModel.getContactRelationses();
//
//                    for(ContactsModel.ContactRelations contactRelations1:tempRelationsArray){
//
//                        if(contactRelations1.getRelationFirstName().equalsIgnoreCase("")){
//
//                            contactDetailModel.getContactRelationses().remove(contactRelations1)  ;
//
//                        }
//                    }

/*                    if (!contactDetailModel.getContactRelationses().isEmpty()) {

                        if (!contactDetailModel.getContactRelationses().get(0).getRelationFirstName().equalsIgnoreCase("")) {
                            relation.setText(contactDetailModel.getContactRelationses().get(0).getRelation());
                            relation_name.setText(contactDetailModel.getContactRelationses().get(0).getRelationFirstName() + " "
                                    + contactDetailModel.getContactRelationses().get(0).getRelationLastName());

                        } else {

                            relation.setText(contactDetailModel.getContactRelationses().get(1).getRelation());
                            relation_name.setText(contactDetailModel.getContactRelationses().get(1).getRelationFirstName() + " "
                                    + contactDetailModel.getContactRelationses().get(1).getRelationLastName());
                        }

                    } else {
                        relation.setText("None");
                        relation_name.setText("None");
                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
