package com.bnhabitat.ui.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.ActivityCommunicator;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.AreaEntitiyModel;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.InventoryModelProp;
import com.bnhabitat.models.PincodeDetailModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.models.Result_;
import com.bnhabitat.models.StateModel;
import com.bnhabitat.models.TabFragment;
import com.bnhabitat.township.fragment.Plot_lotLand_Fragment;
import com.bnhabitat.ui.activities.MyPropertyActivity;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.activities.SelectPropertyActivity;
import com.bnhabitat.ui.adapters.ContactsAdapter;
import com.bnhabitat.ui.adapters.SelectPropertyAdapter;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.ui.views.AutoCompleteEditext;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */

public class LocationFragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener {
    View view;
    private String blockCharacterSet = "~#^|$%&/*!'+-:()<>_`€…•™£¥¤§©®¶µ[]αβ√M{}=?";
    public static String pid;
    AutoCompleteEditext developer, project, city, sub_area, sector, sector_area;
    EditText pincode, unit_no, google_plus_code, tower_no, floor_no;
    SendMessage SM;
    LinearLayout floor_lay, tower_lay;
    JSONObject locationObj;
    ImageView next;
    String entity_id = "", project_id = "", developer_id = "", PropertyId = "", edit_property;
    ArrayList<AreaEntitiyModel> developerareaEntitiyModels = new ArrayList<>();
    ArrayList<AreaEntitiyModel> developerareaEntitiyModelsTemp = new ArrayList<>();
  /*  ArrayList<AreaEntitiyModel> stateareaEntitiyModels = new ArrayList<>();
    ArrayList<AreaEntitiyModel> stateareaEntitiyModelsTemp = new ArrayList<>();*/
    ArrayList<AreaEntitiyModel> cityareaEntitiyModels = new ArrayList<>();
    ArrayList<AreaEntitiyModel> cityareaEntitiyModelsTemp = new ArrayList<>();
    ArrayList<AreaEntitiyModel> subareaEntitiyModels = new ArrayList<>();
    ArrayList<AreaEntitiyModel> subareaEntitiyModelsTemp = new ArrayList<>();
    ArrayList<AreaEntitiyModel> projectareaEntitiyModels = new ArrayList<>();
    ArrayList<AreaEntitiyModel> projectareaEntitiyModelsTemp = new ArrayList<>();
    ArrayList<AreaEntitiyModel> sectorareaEntitiyModels = new ArrayList<>();
    ArrayList<AreaEntitiyModel> sectorEntitiyModels = new ArrayList<>();
    ArrayList<AreaEntitiyModel> sectorEntitiyModelsTemp = new ArrayList<>();
    ArrayList<AreaEntitiyModel> sectorareaEntitiyModelsTemp = new ArrayList<>();
    ArrayList<PincodeDetailModel> pincodeDetailModels = new ArrayList<>();
    AutocompleteAdapter autocompleteAdapter;
    ArrayList<PropertyUnitsModel> propertyUnitsModels;
    ArrayList<StateModel> stateModels =new ArrayList<>();
    ArrayList<StateModel> stateModelsTemp =new ArrayList<>();

    SpinnerAdapter  adapter1;
    int iskeyboaropen = 0;
    ArrayList<InventoryModel> json_Data;
    ArrayList<InventoryModelProp> propArrayList = new ArrayList<>();
    public static InventoryModelProp inventoryModelProp;
    public static String PropertyTypeId;
    String Editid;
    LinearLayout linear_layout;
    ScrollView scroll_view;
    ArrayList<String> area_list = new ArrayList<>();
 // private Spinner spinner_state;
    String states ="";
    AutoCompleteEditext state ;

    public LocationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_location, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        onIntializeView(view);
        onAreaEntitiy();

      //  getStateListing();
        blockChar();

        Bundle bundle = this.getArguments();
        if (bundle != null)
            PropertyId = bundle.getString("PropertyId") == null ? "" : bundle.getString("PropertyId");
        PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");

        propertyUnitsModels = (ArrayList<PropertyUnitsModel>) bundle.getSerializable("propertySizes");
        try {
            edit_property = bundle.getString("edit") == null ? "" : bundle.getString("edit");
            if (!edit_property.equals("")) {
                Editid = bundle.getString("editId");
                json_Data = (ArrayList<InventoryModel>) bundle.getSerializable("json_Data");

                for (int i = 0; i < json_Data.size(); i++) {

                    if (json_Data.get(i).getId().equalsIgnoreCase(Editid)) {

                        developer.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getDeveloper()));
                        project.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getProject()));
                        pincode.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getZipCode()));
                        states = json_Data.get(i).getPropertyLocations().get(0).getState();

                        city.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getDistrict()));
                        sector.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getTownName()));
                        sector_area.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getSector()));
                        sub_area.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getSubArea()));
                        unit_no.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getUnitName()));
                        google_plus_code.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLocations().get(0).getGooglePlusCode()));

                    }

                }

            }


        } catch (Exception e) {

        }

        if (PropertyTypeId.equalsIgnoreCase("3")) {
            floor_lay.setVisibility(View.VISIBLE);
            tower_lay.setVisibility(View.VISIBLE);
        } else {
            floor_lay.setVisibility(View.GONE);
            tower_lay.setVisibility(View.GONE);
        }

        return view;
    }

    private void blockChar() {
        developer.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
        project.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
        pincode.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
        city.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
        sector.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
        sub_area.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(100)});
        tower_no.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
        floor_no.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
        unit_no.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
        google_plus_code.setFilters(new InputFilter[]{filter});
    }

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();

            ((PropertyItemActivity) getActivity()).save_draft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (isValid()) {
                        onLocationDraftdata();
                    } else {
                        Utils.showErrorMessage("Please input one of the field", getActivity());
                    }


                }
            });
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    private void onIntializeView(View view) {
        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);
        scroll_view = (ScrollView) view.findViewById(R.id.scroll_view);
        developer = (AutoCompleteEditext) view.findViewById(R.id.developer);
        project = (AutoCompleteEditext) view.findViewById(R.id.project);
        pincode = (EditText) view.findViewById(R.id.pincode);
        unit_no = (EditText) view.findViewById(R.id.unit_no);
        google_plus_code = (EditText) view.findViewById(R.id.google_plus_code);
        tower_no = (EditText) view.findViewById(R.id.tower_no);
        floor_no = (EditText) view.findViewById(R.id.floor_no);
        city = (AutoCompleteEditext) view.findViewById(R.id.city);
        sector = (AutoCompleteEditext) view.findViewById(R.id.sector);
        sub_area = (AutoCompleteEditext) view.findViewById(R.id.sub_area);
        sector_area = (AutoCompleteEditext) view.findViewById(R.id.sector_area);
        state = (AutoCompleteEditext) view.findViewById(R.id.state);


        floor_lay = (LinearLayout) view.findViewById(R.id.floor_lay);
        tower_lay = (LinearLayout) view.findViewById(R.id.tower_lay);
        next = (ImageView) view.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!floor_no.getText().toString().equalsIgnoreCase(""))
                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.FLOOR_NUMBER, floor_no.getText().toString());

                onLocationdata();

                  /*  if (!city.getText().toString().trim().equalsIgnoreCase("")) {
                        if (!sector.getText().toString().trim().equalsIgnoreCase("")) {

                            if (!sector_area.getText().toString().trim().equalsIgnoreCase("")){

                                if (!project.getText().toString().trim().equalsIgnoreCase("")) {

                                    onLocationdata();
                                } else {
                                    Utils.showWarningErrorMessage(getString(R.string.warning), "Please enter your Project Name", "Ok", getActivity());
                                }


                            }
                            else {
                                Utils.showWarningErrorMessage(getString(R.string.warning), "Please enter your Locality/Sector", "Ok", getActivity());

                            }

                        } else {
                            Utils.showWarningErrorMessage(getString(R.string.warning), "Please enter your Town/City", "Ok", getActivity());
                        }
                    } else {
                        Utils.showWarningErrorMessage(getString(R.string.warning), "Please enter your District", "Ok", getActivity());
                    }*/

            }
        });

        linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(iskeyboaropen==1) {
                    hideSoftKeyboard(getActivity());
                    iskeyboaropen=0;
                }
            }
        });

        linear_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = linear_layout.getRootView().getHeight() - linear_layout.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");

                    iskeyboaropen=1;
                } else {
                    Log.e("MyActivity", "keyboard closed");
                    iskeyboaropen=0;
                }
            }
        });

/*
        scroll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(getActivity());
            }
        });
*/


/*
        developer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (developerareaEntitiyModels.isEmpty()) {
                    // developer.setFocusable(false);
                    entity_id = "10";

                    onAreaEntitiy();
                }

*/
/*
                else {
                    developer.setFocusable(true);
                    developer.setFocusableInTouchMode(true);
                }
*//*

            }
        });
*/


        floor_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floor_no.setFocusable(true);
                floor_no.setFocusableInTouchMode(true);
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            }
        });

        google_plus_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                google_plus_code.setFocusable(true);
                google_plus_code.setFocusableInTouchMode(true);
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }
        });

        unit_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unit_no.setFocusable(true);
                unit_no.setFocusableInTouchMode(true);
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }
        });

        pincode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pincode.setFocusable(true);
                pincode.setFocusableInTouchMode(true);
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }
        });

/*
        state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stateModelsTemp.isEmpty()) {
                    state.setFocusable(false);
                    Constants.ENTITY_ID = "1";
                    onAreaEntitiy();
                } else {
                    state.setFocusable(true);
                    state.setFocusableInTouchMode(true);
                }

            }
        });
*/

        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityareaEntitiyModels.isEmpty()) {
                    city.setFocusable(true);
                    entity_id = "2";
                  //  onAreaEntitiy();
                } else {
                    city.setFocusable(true);
                    city.setFocusableInTouchMode(true);
                }
            }
        });
        sub_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (subareaEntitiyModels.isEmpty()) {
                    sub_area.setFocusable(false);
                    entity_id = "9";

                    onAreaEntitiy();
                } else {
                    sub_area.setFocusable(true);
                    sub_area.setFocusableInTouchMode(true);
                }

            }
        });
        sector_area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sectorareaEntitiyModels.isEmpty()) {
                    sector_area.setFocusable(false);
                    entity_id = "6";

                    onAreaEntitiy();
                } else {
                    sector_area.setFocusable(true);
                    sector_area.setFocusableInTouchMode(true);
                }

            }
        });

        sector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sectorEntitiyModels.isEmpty()) {
                    sector.setFocusable(true);
                    entity_id = "1";

                    onAreaEntitiy();
                } else {
                    sector.setFocusable(true);
                    sector.setFocusableInTouchMode(true);
                }

            }
        });
        project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (projectareaEntitiyModels.isEmpty()) {
                    // project.setFocusable(false);
                    entity_id = "11";
                    onAreaEntitiy();
                }
/*
                else {
                    project.setFocusable(true);
                    project.setFocusableInTouchMode(true);
                }
*/

            }
        });
/*
        developer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                developerareaEntitiyModelsTemp.clear();
                List<AreaEntitiyModel> list = filter(s.toString(), developerareaEntitiyModels, true);


                developerareaEntitiyModelsTemp.addAll(list);
                if (developerareaEntitiyModelsTemp.isEmpty()) {
//                    Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
                    autocompleteAdapter = new AutocompleteAdapter(getActivity(), R.layout.entity_item, developerareaEntitiyModelsTemp, s.toString());
                    developer.setAdapter(autocompleteAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
*/

        state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
               // stateareaEntitiyModelsTemp.clear();
                stateModelsTemp.clear();
                List<StateModel> list = filter(s.toString(), stateModels, true);

                stateModelsTemp.addAll(list);
                if (stateModelsTemp.isEmpty()) {
//                    Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
                    autocompleteAdapter = new AutocompleteAdapter(getActivity(), R.layout.entity_item, stateModelsTemp, s.toString());
                    state.setAdapter(autocompleteAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
/*
        city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                cityareaEntitiyModelsTemp.clear();
                List<AreaEntitiyModel> list = filter(s.toString(), cityareaEntitiyModels, true);


                cityareaEntitiyModelsTemp.addAll(list);
                if (cityareaEntitiyModelsTemp.isEmpty()) {
//                    Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
                    autocompleteAdapter = new AutocompleteAdapter(getActivity(), R.layout.entity_item, cityareaEntitiyModelsTemp, s.toString());
                    city.setAdapter(autocompleteAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
*/
/*
        sub_area.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                subareaEntitiyModelsTemp.clear();
                List<AreaEntitiyModel> list = filter(s.toString(), subareaEntitiyModels, true);


                subareaEntitiyModelsTemp.addAll(list);
                if (subareaEntitiyModelsTemp.isEmpty()) {
//                    Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
                    autocompleteAdapter = new AutocompleteAdapter(getActivity(), R.layout.entity_item, subareaEntitiyModelsTemp, s.toString());
                    sub_area.setAdapter(autocompleteAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
*/
/*
        project.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                projectareaEntitiyModelsTemp.clear();
                List<AreaEntitiyModel> list = filter(s.toString(), projectareaEntitiyModels, true);


                projectareaEntitiyModelsTemp.addAll(list);
                if (projectareaEntitiyModelsTemp.isEmpty()) {
//                    Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
                    autocompleteAdapter = new AutocompleteAdapter(getActivity(), R.layout.entity_item, projectareaEntitiyModelsTemp, s.toString());
                    project.setAdapter(autocompleteAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
*/
/*
        sector_area.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                sectorareaEntitiyModelsTemp.clear();
                List<AreaEntitiyModel> list = filter(s.toString(), sectorareaEntitiyModels, true);


                sectorareaEntitiyModelsTemp.addAll(list);
                if (sectorareaEntitiyModelsTemp.isEmpty()) {
//                    Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
                    autocompleteAdapter = new AutocompleteAdapter(getActivity(), R.layout.entity_item, sectorareaEntitiyModelsTemp, s.toString());
                    sector_area.setAdapter(autocompleteAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
*/

/*
        sector.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                sectorEntitiyModelsTemp.clear();
                List<AreaEntitiyModel> list = filter(s.toString(), sectorEntitiyModels, true);


                sectorEntitiyModelsTemp.addAll(list);
                if (sectorEntitiyModelsTemp.isEmpty()) {
//                    Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
                } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
                    autocompleteAdapter = new AutocompleteAdapter(getActivity(), R.layout.entity_item, sectorEntitiyModelsTemp, s.toString());
                    sector.setAdapter(autocompleteAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
*/
        pincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (pincode.getText().length() == 6) {
                    onPinDetail();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
/*
        developer.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {

//                LinearLayout rl = (LinearLayout) arg1;
//                TextView tv = (TextView) rl.getChildAt(0);
                String area = autocompleteAdapter.getItem(pos).getName();
                developer_id = autocompleteAdapter.getItem(pos).getId();
                developer.setText(area);


            }

        });
*/
/*
        project.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {

//                LinearLayout rl = (LinearLayout) arg1;
//                TextView tv = (TextView) rl.getChildAt(0);
                String area = autocompleteAdapter.getItem(pos).getName();
                project_id = autocompleteAdapter.getItem(pos).getId();
                project.setText(area);


            }

        });
*/
        city.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {

//
                String area = autocompleteAdapter.getItem(pos).getName();
                city.setText(area);


            }

        });
        sub_area.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {

//
                String area = autocompleteAdapter.getItem(pos).getName();
                sub_area.setText(area);


            }

        });
        sector.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {

//
                String area = autocompleteAdapter.getItem(pos).getName();
                sector.setText(area);


            }

        });
        sector_area.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {

//
                String area = autocompleteAdapter.getItem(pos).getName();
                sector_area.setText(area);


            }

        });
    }

    @Override
    public void onChangeTab(int position) {
        Log.e("position", "no.." + position);
    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    private void onAreaEntitiy() {

/*
        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_AREA_ENTITY + entity_id,
                "",
                "Loading..",
                this,
                Urls.URL_AREA_ENTITY,
                Constants.GET, false).execute();
*/

        Constants.ENTITY_ID= "1";

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_STATE_LISTING_FINAL +"/"+ Constants.ENTITY_ID+

                "/"+ Constants.STATE_ID+  "/" +Constants.DISTRCT_ID +"/"+ Constants.SUB_DIVISION_ID +"/"+ Constants.TEHSIL_ID +"/"+ Constants.TOWNID
                +"/"+ Constants.TOWNSHIP_ID +"/"+ Constants.LOCALITY_ID,
                "",
                "Loading..",
                this,
                Urls.URL_STATE_LISTING_FINAL +"/"+ Constants.ENTITY_ID+

                        "/"+ Constants.STATE_ID+  "/" +Constants.DISTRCT_ID +"/"+ Constants.SUB_DIVISION_ID +"/"+ Constants.TEHSIL_ID +"/"+ Constants.TOWNID
                        +"/"+ Constants.TOWNSHIP_ID +"/"+ Constants.LOCALITY_ID,
                Constants.GET).execute();

    }

    private void onPinDetail() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PIN_DETAIL + pincode.getText().toString().trim(),
                "",
                "Loading..",
                this,
                Urls.URL_PIN_DETAIL,
                Constants.GET).execute();

    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {


            if (which.equalsIgnoreCase(Urls.URL_STATE_LISTING_FINAL  +"/"+ Constants.ENTITY_ID+

                    "/"+ Constants.STATE_ID+  "/" +Constants.DISTRCT_ID +"/"+ Constants.SUB_DIVISION_ID +"/"+ Constants.TEHSIL_ID +"/"+ Constants.TOWNID
                    +"/"+ Constants.TOWNSHIP_ID +"/"+ Constants.LOCALITY_ID )) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {

                        //  Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();

                        } else {



                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                StateModel stateModel = new StateModel();

                                stateModel.setId(jsonObject1.getInt("Id"));
                              // stateModel.setCountryId(jsonObject1.getInt("CountryId"));
                                stateModel.setName(jsonObject1.getString("Name"));

                                stateModels.add(stateModel);

                            }
                            area_list.clear();


                            for (int i = 0; i < stateModels.size(); i++) {
                                area_list.add(stateModels.get(i).getName());
                            }
                            adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);

                            adapter1.addAll(area_list);
                            adapter1.add(getString(R.string.select_type));

                          //  spinner_state.setAdapter(adapter1);

//                            if (!edit_property.equalsIgnoreCase("")){
//
//                                for (int s=0 ; s<area_list.size();s++){
//
//                                    if (states.equalsIgnoreCase(area_list.get(s)))
//                                        spinner_state.setSelection(s);
//
//                                }
//
//                            }
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            else if (which.equalsIgnoreCase(Urls.URL_PIN_DETAIL)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject result1 = jsonObject.getJSONObject("Result");


                        PincodeDetailModel pincodeDetailModel = new PincodeDetailModel();

                        pincodeDetailModel.setStateName(result1.getString("StateName"));
                        pincodeDetailModel.setDistrictName(result1.getString("DistrictName"));

                        pincodeDetailModels.add(pincodeDetailModel);


                        //state.setText(pincodeDetailModels.get(0).getStateName());
                        city.setText(pincodeDetailModels.get(0).getDistrictName());


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            pid = jsonObject2.getString("Id");

                            SM.sendData(pid, 1);
                            if (!Editid.equalsIgnoreCase("")) {
                                SM.EditId(json_Data, Editid);
                                Toast.makeText(getActivity(), "Inventory updated successfully", Toast.LENGTH_SHORT).show();

                            }
                            else
                                Toast.makeText(getActivity(), "Inventory added successfully", Toast.LENGTH_SHORT).show();
                            JSONArray propJsonArray = jsonObject2.getJSONArray("PropertyLocations");

                            for (int i = 0; i < propJsonArray.length(); i++) {

                                inventoryModelProp = new InventoryModelProp();
                                JSONObject dataObj = propJsonArray.getJSONObject(i);
                                inventoryModelProp.setId(dataObj.getInt("Id"));
                                inventoryModelProp.setPropertyId(dataObj.getInt("PropertyId"));
                                inventoryModelProp.setDeveloper(dataObj.getString("Developer"));
                                inventoryModelProp.setDeveloperId(dataObj.getString("DeveloperId"));
                                inventoryModelProp.setProject(dataObj.getString("Project"));
                                inventoryModelProp.setZipCode(dataObj.getString("ZipCode"));
                                inventoryModelProp.setState(dataObj.getString("State"));
                                inventoryModelProp.setDistrict(dataObj.getString("District"));
                                inventoryModelProp.setLocalityId(dataObj.getInt("LocalityId"));
                                inventoryModelProp.setSector(dataObj.getString("Sector"));
                                inventoryModelProp.setTownId(dataObj.getInt("TownId"));
                                inventoryModelProp.setTownName(dataObj.getString("TownName"));
                                inventoryModelProp.setSubArea(dataObj.getString("SubArea"));
                                inventoryModelProp.setSubDivision(dataObj.getString("SubDivision"));
                                inventoryModelProp.setTowerName(dataObj.getString("TowerName"));
                                inventoryModelProp.setFloorNo(dataObj.getString("FloorNo"));
                                inventoryModelProp.setGooglePlusCode(dataObj.getString("GooglePlusCode"));
                                inventoryModelProp.setUnitNo(dataObj.getString("UnitNo"));
                                inventoryModelProp.setStateId(dataObj.getInt("StateId"));
                                inventoryModelProp.setDistrictId(dataObj.getInt("DistrictId"));
                                inventoryModelProp.isDeletable(dataObj.getBoolean("IsDeletable"));
                                inventoryModelProp.isModify(dataObj.getBoolean("IsModify"));

                                propArrayList.add(inventoryModelProp);

                            }

                            Result_ result_ = new Result_();
                            result_.setPropertyLocations(propArrayList);


                        } else {
                          Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());

                          //  SM.sendData(pid, 1);

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Constants.URL_PROPERTY_POST_DRAFT)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            pid = jsonObject2.getString("Id");

                            JSONArray propJsonArray = jsonObject2.getJSONArray("PropertyLocations");

                            for (int i = 0; i < propJsonArray.length(); i++) {

                                inventoryModelProp = new InventoryModelProp();
                                JSONObject dataObj = propJsonArray.getJSONObject(i);
                                inventoryModelProp.setId(dataObj.getInt("Id"));
                                inventoryModelProp.setPropertyId(dataObj.getInt("PropertyId"));
                                inventoryModelProp.setDeveloper(dataObj.getString("Developer"));
                                inventoryModelProp.setDeveloperId(dataObj.getString("DeveloperId"));
                                inventoryModelProp.setProject(dataObj.getString("Project"));
                                inventoryModelProp.setZipCode(dataObj.getString("ZipCode"));
                                inventoryModelProp.setState(dataObj.getString("State"));
                                inventoryModelProp.setDistrict(dataObj.getString("District"));
                                inventoryModelProp.setLocalityId(dataObj.getInt("LocalityId"));
                                inventoryModelProp.setSector(dataObj.getString("Sector"));
                                inventoryModelProp.setTownId(dataObj.getInt("TownId"));
                                inventoryModelProp.setTownName(dataObj.getString("TownName"));
                                inventoryModelProp.setSubArea(dataObj.getString("SubArea"));
                                inventoryModelProp.setSubDivision(dataObj.getString("SubDivision"));
                                inventoryModelProp.setTowerName(dataObj.getString("TowerName"));
                                inventoryModelProp.setFloorNo(dataObj.getString("FloorNo"));
                                inventoryModelProp.setGooglePlusCode(dataObj.getString("GooglePlusCode"));
                                inventoryModelProp.setUnitNo(dataObj.getString("UnitNo"));
                                inventoryModelProp.setStateId(dataObj.getInt("StateId"));
                                inventoryModelProp.setDistrictId(dataObj.getInt("DistrictId"));
                                inventoryModelProp.isDeletable(dataObj.getBoolean("IsDeletable"));
                                inventoryModelProp.isModify(dataObj.getBoolean("IsModify"));

                                propArrayList.add(inventoryModelProp);

                            }

                            Result_ result_ = new Result_();
                            result_.setPropertyLocations(propArrayList);

                           /* SM.sendData(pid, 1);

                            if (!Editid.equalsIgnoreCase("")) {
                                SM.EditId(json_Data, Editid);

                            }*/

                            Utils.showSuccessErrorMessage("Success", "Draft added successfully", "Ok", getActivity());

                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    public static List<StateModel> filter(String string,
                                                ArrayList<StateModel> iterable, boolean byName) {
        if (iterable == null)
            return new LinkedList<>();
        else {
            List<StateModel> collected = new LinkedList<>();
            Iterator<StateModel> iterator = iterable.iterator();
            if (iterator == null)
                return collected;
            while (iterator.hasNext()) {
                StateModel item = iterator.next();

                if (item.getName().toLowerCase().contains(string.toLowerCase()))
                    collected.add(item);

            }
            return collected;
        }
    }

    public class AutocompleteAdapter extends ArrayAdapter<StateModel> {
        private ArrayList<StateModel> fullList;
        private ArrayList<StateModel> mOriginalValues;
        int layoutResourceId;
        Context mContext;
        String text;
        //  private ArrayFilter mFilter;


        public AutocompleteAdapter(Context mContext, int layoutResourceId, ArrayList<StateModel> entity, String text) {
            super(mContext, layoutResourceId, entity);
            this.fullList = entity;
            this.mContext = mContext;
            this.text = text;
            mOriginalValues = new ArrayList<StateModel>(fullList);
            this.layoutResourceId = layoutResourceId;
        }

        @Override
        public int getCount() {
            return fullList.size();
        }

        public StateModel getItem(int position) {
            return fullList.get(position);
        }
        @Override
        public Filter getFilter() {
            return nameFilter;
        }

        Filter nameFilter = new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                String str = ((StateModel)(resultValue)).getName();
                return str;
            }
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if(constraint != null) {
                    mOriginalValues.clear();
                    for (StateModel customer : fullList) {
                        if(customer.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                            mOriginalValues.add(customer);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mOriginalValues;
                    filterResults.count = mOriginalValues.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                ArrayList<StateModel> filteredList = (ArrayList<StateModel>) results.values;
                if(results != null && results.count > 0) {
                    clear();
                    for (StateModel c : filteredList) {
                        add(c);
                    }
                    notifyDataSetChanged();
                }
            }
        };

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                // inflate the layout
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = inflater.inflate(R.layout.entity_item, parent, false);

            }

            //get Country

            // get the TextView and then set the text (item name) and tag (item ID) values

            StateModel stateModel = getItem(position);

            final TextView countryName = (TextView) convertView.findViewById(R.id.area);

            countryName.setText(stateModel.getName());

            setSearchTextHighlightSimpleHtml(countryName, fullList.get(position).getName(), text);

            return convertView;
        }


        public void setSearchTextHighlightSimpleHtml(TextView textView, String fullText, String searchText) {
            searchText = searchText.replace("'", "");
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    fullText = fullText.replaceAll("(?i)(" + searchText + ")", "<b><big><font color='#000000'>$1</font></big></b>");
                    textView.setText(Html.fromHtml(fullText, Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
                } else {
                    fullText = fullText.replaceAll("(?i)(" + searchText + ")", "<b><big><font color='red'>$1</font></big></b>");
                    textView.setText(Html.fromHtml(fullText), TextView.BufferType.SPANNABLE);
                }
            } catch (Exception e) {
                textView.setText(fullText);
            }
        }

    }

    public JSONObject onLocationdata() {

        try {

            locationObj = new JSONObject();
            locationObj.put("IsDraft", 0);

            if (!edit_property.equals("")) {

                locationObj.put("Id", Editid);

            } else {

                locationObj.put("Id", 0);

            }

            locationObj.put("PropertyTypeId", PropertyTypeId);
            locationObj.put("CompanyId", Constants.APP_COMPANY_ID);
            locationObj.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            locationObj.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));

            JSONArray jsonArray = new JSONArray();

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("Developer", developer.getText().toString());
            jsonObject1.put("Project", project.getText().toString());
            jsonObject1.put("ZipCode", pincode.getText().toString());
            jsonObject1.put("State", state.getText().toString().trim());
            jsonObject1.put("District", city.getText().toString());
            jsonObject1.put("SubArea", sub_area.getText().toString());
            jsonObject1.put("TownName", sector.getText().toString());
            jsonObject1.put("Sector", sector_area.getText().toString());
            jsonObject1.put("FloorNo", floor_no.getText().toString());
            jsonObject1.put("GooglePlusCode", google_plus_code.getText().toString());
            jsonObject1.put("UnitNo", unit_no.getText().toString());
            jsonArray.put(jsonObject1);

            locationObj.put("PropertyLocations", jsonArray);

            postProperty();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationObj;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public JSONObject onLocationDraftdata() {
        try {

            locationObj = new JSONObject();

            locationObj.put("PropertyTypeId", PropertyTypeId);
            locationObj.put("CompanyId", Constants.APP_COMPANY_ID);
            locationObj.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            locationObj.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            locationObj.put("IsDraft", 1);

            JSONArray jsonArray = new JSONArray();

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("Developer", developer.getText().toString());
            jsonObject1.put("Project", project.getText().toString());
            jsonObject1.put("ZipCode", pincode.getText().toString());
         //   jsonObject1.put("State", spinner_state.getSelectedItem().toString());
            jsonObject1.put("District", city.getText().toString());
            jsonObject1.put("SubArea", sub_area.getText().toString());
            jsonObject1.put("TownName", sector.getText().toString());
            jsonObject1.put("Sector", sector_area.getText().toString());
            jsonObject1.put("FloorNo", floor_no.getText().toString());
            jsonObject1.put("GooglePlusCode", google_plus_code.getText().toString());
            jsonObject1.put("UnitNo", unit_no.getText().toString());
            jsonArray.put(jsonObject1);

            locationObj.put("PropertyLocations", jsonArray);

            postPropertyDraft();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationObj;
    }

    private void postPropertyDraft() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Constants.URL_PROPERTY_POST_DRAFT,
                Constants.POST).execute();

    }

    private void postProperty() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_POST,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = locationObj.toString();
        } catch (Exception e) {

        }


        return inputStr;
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(getActivity(), MyPropertyActivity.class);
            //  intent.putExtra("from_savedrafts","from_savedrafts");
            startActivity(intent);
        }
    }

    public boolean isValid() {

        if (!developer.getText().toString().equalsIgnoreCase("")) {
            return true;
        } else if (!project.getText().toString().equalsIgnoreCase("")) {
            return true;
        } else if (!pincode.getText().toString().equalsIgnoreCase("")) {
            return true;
        } else if (!city.getText().toString().equalsIgnoreCase("")) {
            return true;
        } else if (!sector.getText().toString().equalsIgnoreCase("")) {
            return true;
        } else if (!sector_area.getText().toString().equalsIgnoreCase("")) {
            return true;
        } else if (!sub_area.getText().toString().equalsIgnoreCase("")) {
            return true;
        } else if (!unit_no.getText().toString().equalsIgnoreCase("")) {
            return true;
        } else if (!google_plus_code.getText().toString().equalsIgnoreCase("")) {
            return true;
        }

        return false;
    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

}
