package com.bnhabitat.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.ChildItemModel;
import com.bnhabitat.models.ExpandableAccomodationModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.ui.adapters.ExpandableAccomdationAdapter;
import com.bnhabitat.ui.adapters.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccomodationViewFragment extends Fragment implements TabChangeListener{
    RecyclerView accomodation_expa_list;
    View view;
    ArrayList<InventoryModel.PropertySpecifications> white_goods=new ArrayList<>();
    ArrayList<InventoryModel.PropertySpecifications> specificationsArrayList=new ArrayList<>();
    ArrayList<InventoryModel.PropertySpecifications> soft_furnishing_list=new ArrayList<>();
    ArrayList<InventoryModel> property_data;
    ArrayList<InventoryModel.PropertySpecifications> childpropertySpecificationses=new ArrayList<>();
    List<ChildItemModel> childItemModels=new ArrayList<>();
    ExpandableListAdapter expandableListAdapter;
    ExpandableAccomdationAdapter expandableAccomdationAdapter;
    InventoryModel.PropertyBedrooms propertyBedrooms;
    InventoryModel.PropertySpecifications propertySpecifications;
    ArrayList<InventoryModel.PropertyBedrooms> listDataHeader=new ArrayList<>();
    List<ExpandableAccomodationModel> expandableAccomodationModels=new ArrayList<>();
    RelativeLayout rltv_hdr;
    public AccomodationViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       view=inflater.inflate(R.layout.fragment_accomodation_view, container, false);
        accomodation_expa_list=(RecyclerView)view.findViewById(R.id.accomodation_expa_list);
//      accomodation_expa_list.setHasFixedSize(true);
        accomodation_expa_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        rltv_hdr = (RelativeLayout) view.findViewById(R.id.rltv_hdr);
        propertyBedrooms=new InventoryModel().new PropertyBedrooms();
        propertySpecifications=new InventoryModel().new PropertySpecifications();
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null)
                property_data = (ArrayList<InventoryModel>) bundle.getSerializable("property_data");

            listDataHeader = property_data.get(0).getPropertyBedroomses();
//            ChildItemModel childItemModel=new ChildItemModel(property_data.get(0).getPropertyBedroomses());


            childpropertySpecificationses = property_data.get(0).getPropertySpecificationses();


            for(int i=0;i<childpropertySpecificationses.size();i++) {
                try {

                    if (childpropertySpecificationses.get(i).getCategory().equalsIgnoreCase("White Goods")) {
                        InventoryModel.PropertySpecifications propertySpecifications_Goods = new InventoryModel().new PropertySpecifications();
                        propertySpecifications_Goods.setName(childpropertySpecificationses.get(i).getName());
                        propertySpecifications_Goods.setDescription(childpropertySpecificationses.get(i).getDescription());
                        white_goods.add(propertySpecifications_Goods);


                    }
                    if (childpropertySpecificationses.get(i).getCategory().equalsIgnoreCase("Specifications")) {
                        InventoryModel.PropertySpecifications propertySpecifications_list = new InventoryModel().new PropertySpecifications();
                        propertySpecifications_list.setName(childpropertySpecificationses.get(i).getName());
                        propertySpecifications_list.setDescription(childpropertySpecificationses.get(i).getDescription());
                        specificationsArrayList.add(propertySpecifications_list);


                    }
                    if (childpropertySpecificationses.get(i).getCategory().equalsIgnoreCase("Soft Furnishings")) {
                        InventoryModel.PropertySpecifications propertySpecifications_Soft = new InventoryModel().new PropertySpecifications();
                        propertySpecifications_Soft.setName(childpropertySpecificationses.get(i).getName());
                        propertySpecifications_Soft.setDescription(childpropertySpecificationses.get(i).getDescription());
                        soft_furnishing_list.add(propertySpecifications_Soft);


                    }
                                 } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

//
////        for(int i=0;i<property_data.get(0).getPropertyBedroomses().size();i++){
////            propertyBedrooms.setGroundType(property_data.get(0).getPropertyBedroomses().get(i).getGroundType());
////        }
////        for(int i=0;i<property_data.get(0).getPropertySpecificationses().size();i++){
////            propertySpecifications.setName(property_data.get(0).getPropertySpecificationses().get(i).getName());
////        }
////
////        childpropertySpecificationses.add(propertySpecifications);
////        propertyBedrooms.setChildDataItems(childpropertySpecificationses);
////        listDataHeader.add(childpropertySpecificationses);
     /*   expandableListAdapter = new ExpandableListAdapter(getActivity(),listDataHeader,specificationsArrayList,soft_furnishing_list,white_goods);
        accomodation_expa_list.setAdapter(expandableListAdapter);*/
        if(listDataHeader.size()>0 || specificationsArrayList.size()>0 || soft_furnishing_list.size()>0 || white_goods.size()>0) {
            expandableListAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, specificationsArrayList, soft_furnishing_list, white_goods);
            accomodation_expa_list.setAdapter(expandableListAdapter);
        }
        else if(listDataHeader.size()==0 && specificationsArrayList.size()==0 && soft_furnishing_list.size()==0 && white_goods.size()==0){
            accomodation_expa_list.setVisibility(View.GONE);
            rltv_hdr.setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
