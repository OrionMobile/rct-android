package com.bnhabitat.ui.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.activities.ProjectDetailActivity;
import com.bnhabitat.ui.adapters.FaqAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends Fragment implements TabChangeListener {
    FaqAdapter faqAdapter;
    ProjectsDetailModel projectsDetailModel;
    int position;
    LinearLayout faqLayout;
    LinearLayoutManager faqlinearLayoutManager;
    TextView no_faq;
    RecyclerView faq_list;
    private ArrayList<ProjectsDetailModel.FAQs> faQses = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.PropertySize> propertySizes = new ArrayList<>();
    ArrayList<ProjectsDetailModel> projectListingModelArrayList = new ArrayList<>();
    ImageView info_icon, share_icon, notification_icon, user_icon;
    ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList<>();

    public FaqFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


                View view = inflater.inflate(R.layout.fragment_blank, container, false);
        faqLayout = (LinearLayout) view.findViewById(R.id.faqLayout);
        info_icon = (ImageView) view.findViewById(R.id.info_icon);
        share_icon = (ImageView) view.findViewById(R.id.share_icon);
        notification_icon = (ImageView) view.findViewById(R.id.notification_icon);
        user_icon = (ImageView) view.findViewById(R.id.user_icon);
        no_faq = (TextView) view.findViewById(R.id.no_faq);

        Bundle bundle = this.getArguments();
        if (bundle != null)
            projectsDetailModel = (ProjectsDetailModel) bundle.getSerializable("projectModel");
        propertySizes = (ArrayList<ProjectsDetailModel.PropertySize>) bundle.getSerializable("propertySizes");
        position = bundle.getInt("position");
        Log.e("position", String.valueOf(position));
        projectListingModelArrayList = (ArrayList<ProjectsDetailModel>) bundle.getSerializable("notification_array");
        faQses = projectsDetailModel.getFaqs();
        faqlinearLayoutManager = new LinearLayoutManager(getActivity());

        faq_list = (RecyclerView) view.findViewById(R.id.faq_list);

        if (faQses.isEmpty()) {
            faq_list.setVisibility(View.GONE);
            faqLayout.setVisibility(View.GONE);
            no_faq.setVisibility(View.VISIBLE);
        }
        faqAdapter = new FaqAdapter(getActivity(), faQses);

        faq_list.setLayoutManager(faqlinearLayoutManager);
        faq_list.setAdapter(faqAdapter);
        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                projectRelationshipManagersArrayList = projectsDetailModel.getProjectRelationshipManagers();
                Log.e("project_array", String.valueOf(projectRelationshipManagersArrayList.size()));
                if (!projectRelationshipManagersArrayList.isEmpty()) {
                    CustomDialogFragment fragment = CustomDialogFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Arraylist", projectRelationshipManagersArrayList);
                    fragment.setArguments(bundle);

                    fragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "dialog_fragment");
                } else {
                    Toast.makeText(getActivity(), "No RM associated with the project.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationDialogProjectDetail notificationDialogFragment = NotificationDialogProjectDetail.newInstance();

                Bundle bundle = new Bundle();
//                bundle.putSerializable("Arraylist", projectListingModelArrayList);
//                bundle.putSerializable("propertySizes", propertySizes);
//                bundle.putSerializable("towers", projectsDetailModel.getTowers());
                bundle.putSerializable("projectModel", projectsDetailModel);
//                bundle.putSerializable("status", "1");
//                bundle.putInt("Position", position);
                notificationDialogFragment.setArguments(bundle);

                notificationDialogFragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "notification_dialog_fragment");

            }
        });
        share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, projectsDetailModel.getTitle());

                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://bnhabitat.com/?" + "p=" + projectsDetailModel.getId());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (projectsDetailModel.getCurrentOffers().equalsIgnoreCase("null") || projectsDetailModel.getCurrentOffers().equalsIgnoreCase("") || projectsDetailModel.getCurrentOffers() == null) {
                    Toast.makeText(getActivity(), "No offers yet", Toast.LENGTH_SHORT).show();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(projectsDetailModel.getCurrentOffers())
                            .setPositiveButton("NOTED", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();
                }
            }
        });
        return view;
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
