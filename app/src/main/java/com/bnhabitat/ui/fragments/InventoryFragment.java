package com.bnhabitat.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.ui.adapters.InventoryAdapter;
import com.bnhabitat.ui.adapters.RoomListAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class InventoryFragment extends Fragment implements TabChangeListener{
    View view;
    RecyclerView inventoryList;
    ArrayList<InventoryModel> inventoryModelArrayList = new ArrayList<>();
    InventoryAdapter inventoryAdapter;
    LinearLayoutManager linearLayoutManager;

    String from_savedrafts;
    ArrayList<InventoryModel> inventoryArrayList = new ArrayList<>();

    public InventoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_inventory, container, false);
        inventoryList = (RecyclerView) view.findViewById(R.id.inventory_list);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        Bundle bundle = this.getArguments();
        try {
            if (bundle != null) {
                inventoryModelArrayList = (ArrayList<InventoryModel>) bundle.getSerializable("property_data");
            //    from_savedrafts = (String) bundle.getSerializable("from_savedrafts");
                for (int i = 0; i < inventoryModelArrayList.size(); i++) {

                    InventoryModel model = inventoryModelArrayList.get(i);
                    boolean isDraft = model.isDraft();

                    if (!inventoryModelArrayList.get(i).isDraft()) {
                        inventoryArrayList.add(model);
                    }
                }

                inventoryAdapter = new InventoryAdapter(getActivity(), inventoryArrayList);
                inventoryList.setLayoutManager(linearLayoutManager);
                inventoryList.setAdapter(inventoryAdapter);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onChangeTab(int position) {
        Log.e("tabchange","hhahah");
    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }


}
