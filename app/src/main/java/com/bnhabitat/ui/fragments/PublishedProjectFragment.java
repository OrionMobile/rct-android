package com.bnhabitat.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.activities.ManageProjectActivity;
import com.bnhabitat.ui.adapters.PublishedProjectAdapter;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class PublishedProjectFragment extends Fragment implements TabChangeListener {
    RecyclerView published_list;
    PublishedProjectAdapter publishedProjectAdapter;
    LinearLayoutManager linearLayoutManager;
    EditText search_city;
    TextView no_project;

    private ArrayList<ProjectsModel> projectsModels = new ArrayList();

    public PublishedProjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_published_project, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null)

            projectsModels = (ArrayList<ProjectsModel>) bundle.getSerializable("projectModel");
        linearLayoutManager=new LinearLayoutManager(getActivity());
        search_city=(EditText) view.findViewById(R.id.search_city);
        no_project=(TextView) view.findViewById(R.id.no_project);
        published_list=(RecyclerView)view.findViewById(R.id.published_list);


            publishedProjectAdapter=new PublishedProjectAdapter(getActivity(),projectsModels);
            published_list.setLayoutManager(linearLayoutManager);

            published_list.setAdapter(publishedProjectAdapter);


//        ProjectsModel projectListingModel=new ProjectsModel("");
//        projectListingModel.setTitle("Sushma chandigarh grande");
//        projectListingModel.setLocality("Zirakpur");
//        projectsModels.add(projectListingModel);
//
//        ProjectsModel projectListingModel1=new ProjectsModel("");
//        projectListingModel1.setTitle("Sushma chandigarh grande");
//        projectListingModel1.setLocality("Ambala");
//        projectsModels.add(projectListingModel1);
//
//        ProjectsModel projectListingModel2=new ProjectsModel("");
//        projectListingModel2.setTitle("Sushma chandigarh grande");
//        projectListingModel2.setLocality("Mohali");
//        projectsModels.add(projectListingModel2);
//
//        ProjectsModel projectListingModel3=new ProjectsModel("");
//        projectListingModel3.setTitle("Sushma chandigarh grande");
//        projectListingModel3.setLocality("Shimla");
//        projectsModels.add(projectListingModel3);
//
//        ProjectsModel projectListingModel4=new ProjectsModel("");
//        projectListingModel4.setTitle("Sushma chandigarh grande");
//        projectListingModel4.setLocality("Manali");
//        projectsModels.add(projectListingModel4);
//        ProjectsModel projectListingModel5=new ProjectsModel("");
//        projectListingModel5.setTitle("Sushma chandigarh grande");
//        projectListingModel5.setLocality("Ludhiana");
//        projectsModels.add(projectListingModel5);

        search_city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                publishedProjectAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });





        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
    @Override
    public void onChangeTab(int position) {
        Intent intent=new Intent(getActivity(), ManageProjectActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(0,0);
        getActivity().finish();

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    public void onEvent(StatusModel statusModel){
        Intent intent=new Intent(getActivity(), ManageProjectActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(0,0);
        getActivity().finish();
    }
}
