package com.bnhabitat.ui.fragments;//package com.bnhabitat.ui.fragments;
//
//
//import android.Manifest;
//import android.app.Dialog;
//import android.content.BroadcastReceiver;
//import android.content.ContentResolver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.PackageManager;
//import android.database.Cursor;
//import android.database.MatrixCursor;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Handler;
//import android.provider.Contacts;
//import android.provider.ContactsContract;
//import android.provider.Settings;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.Toast;
//
//import com.bnhabitat.ContactObserver;
//import com.bnhabitat.R;
//import com.bnhabitat.data.contacts.AddressTable;
//import com.bnhabitat.data.contacts.ContatctTable;
//import com.bnhabitat.data.contacts.EmailTable;
//import com.bnhabitat.data.contacts.LocalContatctTable;
//import com.bnhabitat.data.contacts.LocalPhoneTable;
//import com.bnhabitat.data.contacts.PhoneTable;
//import com.bnhabitat.helper.CommonAsync;
//import com.bnhabitat.helper.CommonSyncwithoutstatus;
//import com.bnhabitat.interfaces.ContactCall;
//import com.bnhabitat.interfaces.TabChangeListener;
//import com.bnhabitat.models.ContactsModel;
//import com.bnhabitat.models.GroupModel;
//import com.bnhabitat.models.StatusModel;
//import com.bnhabitat.ui.activities.ContactsActivity;
//import com.bnhabitat.ui.activities.IntializeActivity;
//import com.bnhabitat.ui.activities.LoginActivity;
//import com.bnhabitat.ui.adapters.ContactsAdapter;
//import com.bnhabitat.ui.adapters.NewContactsAdapter;
//import com.bnhabitat.utils.Constants;
//import com.bnhabitat.utils.PreferenceConnector;
//import com.bnhabitat.utils.Urls;
//import com.bnhabitat.utils.Utils;
//import com.onegravity.contactpicker.group.Group;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Locale;
//
//import me.everything.providers.android.contacts.Contact;
//import me.everything.providers.android.contacts.ContactsProvider;
//import me.everything.providers.core.Data;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class AllContactsFragment extends Fragment implements ContactCall, TabChangeListener, CommonAsync.OnAsyncResultListener, CommonSyncwithoutstatus.OnAsyncResultListener,ContactsActivity.FragmentChangeInterface {
//    public static final String mBroadcastStringAction = "com.truiton.broadcast.string";
//    Data<Contact> datacontacts;
//    private Dialog dialog;
//    int counter;
//    String userCountryCode = "91";
//    ArrayList<ContactsModel.Phones> phones1;
//    ArrayList<String> contactList;
//    private List<com.onegravity.contactpicker.contact.Contact> mContacts;
//    Cursor cursor;
//    ArrayList<ContactsModel> contactsFirebaseModels = new ArrayList<>();
//    private ContactsAdapter contactAdapter;
//    private NewContactsAdapter newContactsAdapter;
//    ArrayList<ContactsModel> contactsFirebaseModelsTemp = new ArrayList<>();
//    LinearLayoutManager linearLayoutManager;
//    private List<Contact> contactLists = new ArrayList<>();
//    private IntentFilter mIntentFilter;
//    private List<Group> mGroups;
//
//    private static final int REQUEST_CONTACT = 0;
//
//    Button sync_contacts;
//
//    LinearLayout no_contact_found;
//
//    RecyclerView contactsView;
//
//    EditText search_contacts;
//    String inputJson = "";
//public AllContactsFragment(){
//
//}
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        View view = inflater.inflate(R.layout.fragment_all_contacts, container, false);
//        mIntentFilter = new IntentFilter();
//        mIntentFilter.addAction(mBroadcastStringAction);
//        initilize(view);
//
//        onClicks();
//        try {
//            PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.IS_SYNC, "");
//            ArrayList<ContactsModel.Phones> phones = PhoneTable.getInstance().getPhones();
//            ArrayList<GroupModel> groups = ContatctTable.getInstance().getUniquesGroups();
//
//
//            for (GroupModel groupModel : groups) {
//                Log.e("Phone", String.valueOf(groupModel.getName()) + "");
//            }
//        }catch (Exception e){
//
//        }
//        return view;
//    }
//
//
//    private void onClicks() {
//
//        search_contacts.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
//                contactsFirebaseModelsTemp.clear();
//                List<ContactsModel> list = filter(s.toString(), contactsFirebaseModels, true);
//
//
//                contactsFirebaseModelsTemp.addAll(list);
//                if (contactsFirebaseModelsTemp.isEmpty()) {
//                    contactsView.setVisibility(View.GONE);
//                    no_contact_found.setVisibility(View.VISIBLE);
//                } else {
//                    contactsView.setVisibility(View.VISIBLE);
//                    no_contact_found.setVisibility(View.GONE);
//                    contactsView.setAdapter(new ContactsAdapter(getActivity(), removeDuplicates(contactsFirebaseModelsTemp)));
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//        PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00");
//        sync_contacts.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                myRef.removeValue();
//                // Android version is lesser than 6.0 or the permission is already granted.
//
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
//                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);
//                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
//                } else {
//                    getAllContacts();
//                    // Android version is lesser than 6.0 or the permission is already granted.
//
//                }
//
//
//
////
////for(int j=0;j<contactsFirebaseModels.size();j++){
////
//////    contactsFirebaseModels.get(j).setSync(true);
////    ContatctTable.getInstance().updateSync(contactsFirebaseModels.get(j).getPhonenumber());
////
////}
//                no_contact_found.setVisibility(View.GONE);
//
//
////
//
////
//
//
//                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.IS_SYNC, "True");
////                contactsView.setAdapter(new ContactsAdapter(getActivity(), removeDuplicates(contactsFirebaseModels)));
//
//
////
//
////                myRef.setValue(contactsArrays);
//
//            }
//        });
//
//    }
//
//    private void onSyncRequest() {
//        new CommonSyncwithoutstatus(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + "?$filter=UpdatedOn+gt+datetime'" + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, "2015-01-01T00:00") + "'", "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();
//
////
//    }
//
//
////    private void onPostContacts() {
////
////        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_POST_CONTACTS,
////                getLoginInputJson(),
////                "Login...",
////                this,
////                Urls.URL_POST_CONTACTS,
////                Constants.POST).execute();
////    }
////
////
////    private String getLoginInputJson() {
////
////        String inputJson = "";
////
////
////        try {
////
////            JSONArray jsonArrayTop = new JSONArray();
////
////            for (int index = 0; index < contactsFirebaseModels.size(); index++) {
////                JSONObject jsonObject = new JSONObject();
////                jsonObject.put("FirstName", contactsFirebaseModels.get(index).getFirstName());
////                jsonObject.put("LastName", "");
////                jsonObject.put("CompanyId", "1");
////                jsonObject.put("CreatedById", "2");
////
////                JSONArray jsonArray = new JSONArray();
////                ArrayList<ContactsModel.Phones> phones = contactsFirebaseModels.get(index).getPhones();
////
////                for (int i = 0; i < phones.size(); i++) {
////
////                    JSONObject jsonObject1 = new JSONObject();
////
////                    jsonObject1.put("PhoneNumber", phones.get(i).getPhoneNumber());
////
//////                    jsonObject1.put("PhoneType", 1);
////                    jsonObject1.put("Mode", "");
////
////                    jsonArray.put(jsonObject1);
////
////                }
////                jsonObject.put("Phones", jsonArray);
////                jsonArrayTop.put(jsonObject);
////            }
////
////
////            inputJson = jsonArrayTop.toString();
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        return inputJson;
////    }
//
//    private void initilize(View view) {
//
//
//        sync_contacts = (Button) view.findViewById(R.id.sync_contacts);
//
//        contactsView = (RecyclerView) view.findViewById(R.id.contacts);
////        newcontacts = (ListView) view.findViewById(R.id.newcontacts);
//        linearLayoutManager = new LinearLayoutManager(getActivity());
//        contactsView.setLayoutManager(linearLayoutManager);
//        no_contact_found = (LinearLayout) view.findViewById(R.id.no_contact_found);
//
//        search_contacts = (EditText) view.findViewById(R.id.search_contacts);
////        contactsFirebaseModels=ContatctTable.getInstance().getContacts();
//        if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.IS_SYNC, "").equalsIgnoreCase("")) {
//            no_contact_found.setVisibility(View.GONE);
//        }else{
//            no_contact_found.setVisibility(View.VISIBLE);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
//            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1);
//            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
//        } else {
//            // Android version is lesser than 6.0 or the permission is already granted.
////                ContactsProvider contactsProvider = new ContactsProvider(getActivity());
////                datacontacts = contactsProvider.getContacts();
////                contactLists = datacontacts.getList();
//            if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.IS_SYNC, "").equalsIgnoreCase(""))
//
//                getAllContacts();
////
////
//            contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
//            Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
//                public int compare(ContactsModel s1, ContactsModel s2) {
//                    return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
//                }
//            });
//            adapterMethod();
////
//        }
////
//        if (contactsFirebaseModels.isEmpty()) {
//
//            no_contact_found.setVisibility(View.VISIBLE);
//
//        }
//    }
//
//    static ArrayList<ContactsModel> removeDuplicates(List<ContactsModel> list) {
//
//        // Store unique items in result.
//        ArrayList<ContactsModel> result = new ArrayList<>();
//
//        // Record encountered Strings in HashSet.
//        ArrayList<String> set = new ArrayList<>();
//
//        // Loop over argument list.
//        for (ContactsModel item : list) {
//
//            // If String is not in set, add it to the list and the set.
//            if (!set.contains(item.getFirstName())) {
//                result.add(item);
//                set.add(item.getFirstName());
//            }
//        }
//        return result;
//    }
//
//
//    public static List<ContactsModel> filter(String string,
//                                             List<ContactsModel> iterable, boolean byName) {
//        if (iterable == null)
//            return new LinkedList<>();
//        else {
//            List<ContactsModel> collected = new LinkedList<>();
//            Iterator<ContactsModel> iterator = iterable.iterator();
//            if (iterator == null)
//                return collected;
//            while (iterator.hasNext()) {
//                ContactsModel item = iterator.next();
//
//                if (item.getFirstName().toLowerCase().contains(string.toLowerCase()))
//                    collected.add(item);
//
//            }
//            return collected;
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                                           int[] grantResults) {
//        if (requestCode == 1) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // Permission is granted
//
//
//            } else {
//                Toast.makeText(getActivity(), "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//
//
//    @Subscribe
//    public void onEvent(String s) {
//
//        Log.e("call=", String.valueOf(s));
//        Intent callIntent = new Intent(Intent.ACTION_CALL);
//
//        callIntent.setData(Uri.parse("tel:" + s));
//        startActivity(callIntent);
//    }
//
//
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        EventBus.getDefault().unregister(this);
//        getActivity().unregisterReceiver(mReceiver);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        EventBus.getDefault().register(this);
//        getActivity().registerReceiver(mReceiver, mIntentFilter);
//    }
//
//    @Override
//    public void contactListCall(List<com.onegravity.contactpicker.contact.Contact> countryList, int i) {
//        Log.e("call=", String.valueOf(i));
//        Intent callIntent = new Intent(Intent.ACTION_CALL);
//
//        callIntent.setData(Uri.parse(countryList.get(i).getPhone(1)));
//        startActivity(callIntent);
//    }
//    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            hideProgress();
//            contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
//            if (contactsFirebaseModels.size() > 0)
//                PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.IS_SYNC, "True");
//
//            Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
//                public int compare(ContactsModel s1, ContactsModel s2) {
//                    return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
//                }
//            });
//            adapterMethod();
////            if (contactAdapter == null) {
////                adapterMethod();
////            } else {
////                contactAdapter.updateContactsAdapter(removeDuplicates(contactsFirebaseModels));
////            }
//        }
//    };
//    private void hideProgress() {
//        try {
//            if ((this.dialog != null) && this.dialog.isShowing()) {
//                this.dialog.dismiss();
//            }
//        } catch (final IllegalArgumentException e) {
//            // Handle or log or ignore
//        } catch (final Exception e) {
//            // Handle or log or ignore
//        } finally {
//            this.dialog = null;
//        }
//
//    }
//    @Override
//    public void onChangeTab(int position) {
//
//
//    }
//
//    @Override
//    public void onChangeTab(ArrayList<String> contacts) {
//
//
//    }
//
//    @Override
//    public void onResultListener(String result, String which) {
//
//        if (null != result && !result.equalsIgnoreCase("")) {
//
//            if (which.equalsIgnoreCase(Urls.URL_ALL_CONTACTS)) {
//                contactsFirebaseModels.clear();
//                try {
//
//
//                    JSONArray projectsArray = new JSONArray(result);
//
////                    if (projectsArray.length() == 0) {
////                        contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
////
//////                        Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
////                    }
////                    else {
//
//
//
//
//                    for (int index = 0; index < projectsArray.length(); index++) {
//                        JSONObject jsonObject1 = projectsArray.getJSONObject(index);
//
//                        ContactsModel contactsModel = new ContactsModel();
//                        contactsModel.setId(jsonObject1.getString("Id"));
//                        contactsModel.setFirstName(jsonObject1.getString("FirstName"));
//                        contactsModel.setLastName(jsonObject1.getString("LastName"));
//
//
//                        JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("Addresses");
//                        ArrayList<ContactsModel.Addresses> addresses = new ArrayList();
//                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {
//
//                            ContactsModel.Addresses addresses1 = contactsModel.new Addresses();
//                            JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);
//
//                            addresses1.setId(dataObj.getString("Id"));
//
//                            addresses1.setCityName(dataObj.getString("CityName"));
//                            addresses1.setAddress1(dataObj.getString("Address1"));
//                            addresses1.setAddress2(dataObj.getString("Address2"));
//
//                            addresses.add(addresses1);
//                        }
//
//                        JSONArray phonesJsonArray = projectsArray.getJSONObject(index).getJSONArray("Phones");
//                        ArrayList<ContactsModel.Phones> phones = new ArrayList();
//                        for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {
//
//                            ContactsModel.Phones phones1 = contactsModel.new Phones();
//                            JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);
//
//                            phones1.setId(dataObj.getString("Id"));
//                            phones1.setContactId(dataObj.getString("ContactId"));
//                            phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
//                            phones1.setPhoneType(dataObj.getString("Mode"));
////                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));
//
//
//                            phones.add(phones1);
//                        }
//                        JSONArray emailJsonArray = projectsArray.getJSONObject(index).getJSONArray("EmailAddresses");
//                        ArrayList<ContactsModel.Email> emails = new ArrayList();
//                        for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {
//
//                            ContactsModel.Email email = contactsModel.new Email();
//                            JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);
//
//                            email.setId(dataObj.getString("Id"));
//                            email.setContactId(dataObj.getString("ContactId"));
//                            email.setAddress(dataObj.getString("Address"));
//                            email.setLabel(dataObj.getString("Label"));
////                                phones1.setIsPrimary(dataObj.getString("IsPrimary"));
//
//
//                            emails.add(email);
//                        }
//
//
//                        JSONArray contactFileJsonArray = projectsArray.getJSONObject(index).getJSONArray("ContactFiles");
//                        ArrayList<ContactsModel.ContactFiles> contactFiles = new ArrayList();
//                        for (int indexJ = 0; indexJ < contactFileJsonArray.length(); indexJ++) {
//
//                            ContactsModel.ContactFiles contactFiles1 = contactsModel.new ContactFiles();
//                            JSONObject dataObj = contactFileJsonArray.getJSONObject(indexJ);
//
//                            contactFiles1.setId(dataObj.getString("Id"));
//                            contactFiles1.setContactId(dataObj.getString("ContactId"));
//                            contactFiles1.setName(dataObj.getString("Name"));
//                            contactFiles1.setType(dataObj.getString("Type"));
//                            contactFiles1.setPath(dataObj.getString("Path"));
//                            contactFiles1.setCompanyId(dataObj.getString("CompanyId"));
//
//
//                            contactFiles.add(contactFiles1);
//                        }
//                        JSONArray contactTermJsonArray = projectsArray.getJSONObject(index).getJSONArray("ContactTerms");
//                        ArrayList<ContactsModel.ContactTerms> contactTermses = new ArrayList();
//                        for (int indexJ = 0; indexJ < contactTermJsonArray.length(); indexJ++) {
//                            ContactsModel.ContactTerms contactTerms = contactsModel.new ContactTerms();
//                            JSONObject dataObj = contactTermJsonArray.getJSONObject(indexJ);
//                            contactTerms.setId(dataObj.getString("Id"));
//
//                            contactTerms.setTermId(dataObj.getString("TermId"));
//
//                            JSONObject jsonObject11=dataObj.getJSONObject("Term");
//                            ContactsModel.ContactTerms.Term term=contactsModel.new ContactTerms().new Term();
//                            term.setName(jsonObject11.getString("Name"));
////                            term.set(jsonObject11.getString("Name"));
//                            contactsModel.setGroup_id(jsonObject11.getString("Id"));
//                            contactsModel.setGroup_name(jsonObject11.getString("Name"));
//                            Log.e("jsonObject11","jsonObject11"+jsonObject11.getString("Id"));
////                            term.setDescription(jsonObject11.getString("Description"));
////                            term.setTaxonomyId(jsonObject11.getString("TaxonomyId"));
////                            term.setCompanyId(jsonObject11.getString("CompanyId"));
//                        }
////
////                            ContactsModel.ContactTerms contactTerms = contactsModel.new ContactTerms();
////                            JSONObject dataObj = contactFileJsonArray.getJSONObject(indexJ);
////
////                            contactTerms.setId(dataObj.getString("Id"));
////                            contactTerms.setTermId(dataObj.getString("TermId"));
//////                            JSONObject jsonObject11=dataObj.getJSONObject("Term");
//////                            ContactsModel.ContactTerms.Term term=contactsModel.new ContactTerms().new Term();
//////                            term.setName(jsonObject11.getString("Name"));
//////                            term.setDescription(jsonObject11.getString("Description"));
//////                            term.setTaxonomyId(jsonObject11.getString("TaxonomyId"));
//////                            term.setCompanyId(jsonObject11.getString("CompanyId"));
////
////
////
////                            contactTermses.add(contactTerms);
////                        }
//
////                            contactsModel.setState(state);
////                            contactsModel.setCountry(country);
//                        contactsModel.setAddresses(addresses);
//                        contactsModel.setPhones(phones);
//                        contactsModel.setContactFiles(contactFiles);
//                        contactsModel.setEmails(emails);
//                        contactsModel.setContactTermses(contactTermses);
//
//                        contactsFirebaseModels.add(contactsModel);
//
//                    }
//
//
//                    for (int index = 0; index < contactsFirebaseModels.size(); index++) {
//                        ContactsModel contactsModel = contactsFirebaseModels.get(index);
//
//
//                        String phone = "";
//                        if (contactsModel.getPhones().size() > 0) {
//                            phone = contactsModel.getPhones().get(0).getPhoneNumber();
//                            if (LocalContatctTable.getInstance().isPhoneAvailable(phone)) {
//                                LocalContatctTable.getInstance().updateSync(phone);
//                            }
//                        }
//
//
//                        if (ContatctTable.getInstance().isContactAvailable(contactsModel.getId())) {
//                            ContatctTable.getInstance().updateCategory(Integer.parseInt(contactsModel.getId()), contactsModel.getFirstName(), contactsModel.getLastName(), contactsModel.getCompanyName(), phone, 1,contactsModel.getGroup_id(),contactsModel.getGroup_name(), "");
//
//                            for (int indexJ = 0; indexJ < contactsModel.getPhones().size(); indexJ++) {
//                                PhoneTable.getInstance().updateCategory(
//                                        Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()),
//                                        contactsModel.getPhones().get(indexJ).getPhoneNumber(), "");
//                            }
//
//                            for (int indexJ1 = 0; indexJ1 < contactsModel.getEmails().size(); indexJ1++) {
//                                EmailTable.getInstance().updateEmail(Integer.parseInt(contactsModel.getEmails().get(indexJ1).getId()), contactsModel.getEmails().get(indexJ1).getAddress(), "01-01-2017");
//
//                            }
//                            for (int indexJ2 = 0; indexJ2 < contactsModel.getEmails().size(); indexJ2++) {
//                                AddressTable.getInstance().updateAddress( Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(),contactsModel.getAddresses().get(indexJ2).getAddress2(),contactsModel.getAddresses().get(indexJ2).getCityName(), contactsModel.getAddresses().get(indexJ2).getZipPostalCode(),contactsModel.getAddresses().get(indexJ2).getCountryName(),contactsModel.getAddresses().get(indexJ2).getStateName(),"01-01-2017");
//
//                            }
//                        } else {
//                            ContatctTable.getInstance().write(Integer.parseInt(contactsModel.getId()), contactsModel.getFirstName(), contactsModel.getLastName(), contactsModel.getCompanyName(), phone, 1,contactsModel.getGroup_id(),contactsModel.getGroup_name(), "01-01-2017", "01-01-2017");
//                            for (int indexJ = 0; indexJ < contactsModel.getPhones().size(); indexJ++) {
//                                PhoneTable.getInstance().write(Integer.parseInt(contactsModel.getPhones().get(indexJ).getContactId()), Integer.parseInt(contactsModel.getPhones().get(indexJ).getId()), contactsModel.getPhones().get(indexJ).getPhoneNumber(), "01-01-2017", "01-01-2017");
//
//                            }
//                            for (int indexJ1 = 0; indexJ1 < contactsModel.getEmails().size(); indexJ1++) {
//                                EmailTable.getInstance().write(Integer.parseInt(contactsModel.getEmails().get(indexJ1).getContactId()), Integer.parseInt(contactsModel.getEmails().get(indexJ1).getId()), contactsModel.getEmails().get(indexJ1).getAddress(), "01-01-2017", "01-01-2017");
//
//                            }
//                            for (int indexJ2 = 0; indexJ2 < contactsModel.getEmails().size(); indexJ2++) {
//                                AddressTable.getInstance().write(Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getContactId()), Integer.parseInt(contactsModel.getAddresses().get(indexJ2).getId()), contactsModel.getAddresses().get(indexJ2).getAddress1(),contactsModel.getAddresses().get(indexJ2).getAddress2(),contactsModel.getAddresses().get(indexJ2).getCityName(), contactsModel.getAddresses().get(indexJ2).getZipPostalCode(),contactsModel.getAddresses().get(indexJ2).getCountryName(),contactsModel.getAddresses().get(indexJ2).getStateName(),"01-01-2017", "01-01-2017");
//
//                            }
//
//                        }
//                    }
//                    contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
//
//
//                    Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
//                        public int compare(ContactsModel s1, ContactsModel s2) {
//                            return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
//                        }
//                    });
//                    adapterMethod();
//
//
//                    Log.e("check", LocalContatctTable.getInstance().getNoSynced().size() + "--");
//
//                    if (LocalContatctTable.getInstance().getNoSynced().size() > 0) {
//                        ArrayList<ContactsModel> contactsModels = LocalContatctTable.getInstance().getNoSynced();
//                        for (int index = 0; index < contactsModels.size(); index++) {
//                            contactsModels.get(index).
//                                    setPhones(LocalPhoneTable.getInstance().getPhones(contactsModels.get(index).getId()));
//
//                        }
//                        pushContacts(contactsModels);
//
//                    }
//
//                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DATE_TIME, Utils.getInstance().getDateTime());
////                    Intent intent=new Intent(getActivity(), ContactsActivity.class);
////                    getActivity().startActivity(intent);
//
////
//// }
//                } catch (JSONException e) {
//
//                    e.printStackTrace();
//
//                }
//
//
//            } else if (which.equalsIgnoreCase(Urls.URL_POST_CONTACTS)) {
//
//                JSONObject jsonObject = null;
//                try {
//                    jsonObject = new JSONObject(result);
//                    JSONObject resultInsert = jsonObject.getJSONObject("Result");
//                    JSONArray projectsArray = resultInsert.getJSONArray("Result");
//
//                    if (projectsArray.length() == 0) {
////                        Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
//                    } else {
//
//                    }
//
//
////                    for (int index = 0; index < projectsArray.length(); index++) {
////                        JSONObject jsonObject1 = projectsArray.getJSONObject(index);
////
////                        ContactsModel contactsModel = new ContactsModel();
////                        contactsModel.setId(jsonObject1.getString("Id"));
////                        contactsModel.setFirstName(jsonObject1.getString("FirstName"));
////                        contactsModel.setLastName(jsonObject1.getString("LastName"));
//////
////
////                        JSONArray sizeJsonArray = projectsArray.getJSONObject(index).getJSONArray("Addresses");
////                        ArrayList<ContactsModel.Addresses> addresses = new ArrayList();
////                        for (int indexJ = 0; indexJ < sizeJsonArray.length(); indexJ++) {
////
////                            ContactsModel.Addresses addresses1 = contactsModel.new Addresses();
////                            JSONObject dataObj = sizeJsonArray.getJSONObject(indexJ);
////
////                            addresses1.setId(dataObj.getString("Id"));
//////
////                            addresses1.setCityName(dataObj.getString("CityName"));
////                            addresses1.setAddress1(dataObj.getString("Address1"));
////                            addresses1.setAddress2(dataObj.getString("Address2"));
//////
////                            addresses.add(addresses1);
////                        }
////
////                        JSONArray phonesJsonArray = projectsArray.getJSONObject(index).getJSONArray("Phones");
////                        ArrayList<ContactsModel.Phones> phones = new ArrayList();
////                        for (int indexJ = 0; indexJ < phonesJsonArray.length(); indexJ++) {
////
////                            ContactsModel.Phones phones1 = contactsModel.new Phones();
////                            JSONObject dataObj = phonesJsonArray.getJSONObject(indexJ);
////
////                            phones1.setId(dataObj.getString("Id"));
////                            phones1.setContactId(dataObj.getString("ContactId"));
////                            phones1.setPhoneNumber(dataObj.getString("PhoneNumber"));
////                            phones1.setPhoneType(dataObj.getString("Mode"));
//////
////
////
////                            phones.add(phones1);
////                        }
////                        JSONArray emailJsonArray = projectsArray.getJSONObject(index).getJSONArray("EmailAddresses");
////                        ArrayList<ContactsModel.Email> emails = new ArrayList();
////                        for (int indexJ = 0; indexJ < emailJsonArray.length(); indexJ++) {
////
////                            ContactsModel.Email email = contactsModel.new Email();
////                            JSONObject dataObj = emailJsonArray.getJSONObject(indexJ);
////
////                            email.setId(dataObj.getString("Id"));
////                            email.setContactId(dataObj.getString("ContactId"));
////                            email.setAddress(dataObj.getString("Address"));
////                            email.setLabel(dataObj.getString("Label"));
////                            emails.add(email);
////                        }
////
////
////                        JSONArray contactFileJsonArray = projectsArray.getJSONObject(index).getJSONArray("ContactFiles");
////                        ArrayList<ContactsModel.ContactFiles> contactFiles = new ArrayList();
////                        for (int indexJ = 0; indexJ < contactFileJsonArray.length(); indexJ++) {
////
////                            ContactsModel.ContactFiles contactFiles1 = contactsModel.new ContactFiles();
////                            JSONObject dataObj = contactFileJsonArray.getJSONObject(indexJ);
////
////                            contactFiles1.setId(dataObj.getString("Id"));
////                            contactFiles1.setContactId(dataObj.getString("ContactId"));
////                            contactFiles1.setName(dataObj.getString("Name"));
////                            contactFiles1.setType(dataObj.getString("Type"));
////                            contactFiles1.setPath(dataObj.getString("Path"));
////                            contactFiles1.setCompanyId(dataObj.getString("CompanyId"));
////
////
////                            contactFiles.add(contactFiles1);
////                        }
////
//////                            contactsModel.setState(state);
//////                            contactsModel.setCountry(country);
////                        contactsModel.setAddresses(addresses);
////                        contactsModel.setPhones(phones);
////                        contactsModel.setContactFiles(contactFiles);
////                        contactsModel.setEmails(emails);
////
////                        contactsFirebaseModels.add(contactsModel);
////
//////                        if (ContatctTable.getInstance().isContactAvailable(contactsModel.getId())) {
//////                            ContatctTable.getInstance().updateCategory(Integer.parseInt(contactsModel.getId()), contactsModel.getFirstName(), contactsModel.getLastName(), contactsModel.getCompanyName(), contactsModel.getPhones().get(0).getPhoneNumber(), issync, getDateTime());
//////                        } else {
//////                            String phone = "";
//////                            if (contactsModel.getPhones().size() == 1) {
//////                                phone = contactsModel.getPhones().get(0).getPhoneNumber();
//////                            } else {
//////                                phone = "";
//////                            }
//////
//////                            ContatctTable.getInstance().write(Integer.parseInt(contactsModel.getId()), contactsModel.getFirstName(), contactsModel.getLastName(), contactsModel.getCompanyName(), phone, issync, "01-01-2017", "01-01-2017");
//////                            PhoneTable.getInstance().write(Integer.parseInt(contactsModel.getPhones().get(0).getContactId()), Integer.parseInt(contactsModel.getPhones().get(0).getId()), contactsModel.getPhones().get(0).getPhoneNumber(), "01-01-2017", "01-01-2017");
//////
//////                            if (contactsModel.getPhones().size() == 2)
//////                                PhoneTable.getInstance().write(Integer.parseInt(contactsModel.getPhones().get(0).getContactId()), Integer.parseInt(contactsModel.getPhones().get(1).getId()), contactsModel.getPhones().get(1).getPhoneNumber(), "01-01-2017", "01-01-2017");
//////                            contactsFirebaseModels = new ArrayList<>(ContatctTable.getInstance().getContacts());
//////
//////                        }
////
////                    }
////                    Collections.sort(contactsFirebaseModels, new Comparator<ContactsModel>() {
////                        public int compare(ContactsModel s1, ContactsModel s2) {
////                            return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
////                        }
////                    });
//////
////                    contactsView.setAdapter(new ContactsAdapter(getActivity(), removeDuplicates(contactsFirebaseModels)));
//                    LocalContatctTable.getInstance().updateAllSync();
//                    Toast.makeText(getActivity(), "Syncing finished ", Toast.LENGTH_SHORT).show();
//                } catch (JSONException e) {
//
//                    e.printStackTrace();
//
//                }
//
//                onSyncRequest();
//
//
//            }
//        }
//
//    }
//
//
//
//    public static int safeLongToInt(long l) {
//        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
//            throw new IllegalArgumentException
//                    (l + " cannot be cast to int without changing its value.");
//        }
//        return (int) l;
//    }
//
//    public void getAllContacts() {
//        contactList = new ArrayList<String>();
//        String phoneNumber = null;
//        String email = null;
//        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
//        String _ID = ContactsContract.Contacts._ID;
//        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
//        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
//        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
//        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
//        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
//        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
//        String DATA = ContactsContract.CommonDataKinds.Email.DATA;
//        String LAST_UPDATED_TIME = ContactsContract.CommonDataKinds.Email.CONTACT_LAST_UPDATED_TIMESTAMP;
//        StringBuffer output;
//        ContentResolver contentResolver = getActivity().getContentResolver();
//        String savedDateTime = PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.CONTACT_MODIFIED, "15028907728");
//        cursor = contentResolver.query(CONTENT_URI, null, LAST_UPDATED_TIME + ">='" + savedDateTime + "'", null, null);
//        ArrayList<ContactsModel> contactsModels = new ArrayList<>();
//        // Iterate every contact in the phone
//        if (cursor.getCount() > 0) {
//            counter = 0;
//            while (cursor.moveToNext()) {
//                output = new StringBuffer();
//
//
//                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
//                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
//                String lastUpdated = cursor.getString(cursor.getColumnIndex(LAST_UPDATED_TIME));
//                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
//
//                Log.e("last updated", lastUpdated + "--" + name);
//                if (hasPhoneNumber > 0) {
////
//                    //This is to read multiple phone numbers associated with the same contact
//                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
//
//
//                    // Read every email id associated with the contact
//                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);
//                    while (emailCursor.moveToNext()) {
//                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
//                        output.append("\n Email:" + email);
//                    }
//
//
//                    // Add the contact to the ArrayList
//                    Log.e("output", "output" + output.toString());
//
//
//                    if (!LocalContatctTable.getInstance().isContactAvailable(contact_id)) {
//
//                        ContactsModel contactsModel = new ContactsModel();
//                        contactsModel.setFirstName(name);
//
//
//                        while (phoneCursor.moveToNext()) {
//
//                            ContactsModel.Phones phone = contactsModel.new Phones();
//                            phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
//
//
//                            String str = phoneNumber.replaceAll("\\D+", "");
//
//
//                            if (phoneNumber.startsWith("+")) {
//                                str = phoneNumber.replaceAll("\\D+", "");
//                            } else if (str.startsWith("00")) {
//                                str = str.replaceFirst("^0+(?!$)", "");
//                            } else if (str.startsWith("0")) {
//                                str = str.replaceFirst("^0+(?!$)", userCountryCode);
//                            } else {
//                                str = userCountryCode + str;
//                            }
//
//                            phone.setPhoneNumber(str);
//                            Log.e("name", name + " " + str);
//                            // phones.add(phone);
//                            if (!LocalContatctTable.getInstance().isContactAvailable(contact_id))
//                                LocalContatctTable.getInstance().write(Integer.parseInt(contact_id), name, "", "Bn-habitat", str, 0, PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, ""));
//
//                            LocalPhoneTable.getInstance().write(Integer.parseInt(contact_id), 0, str, PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, ""), PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.DATE_TIME, ""));
//                        }
//                    }
//
//
//
//                    emailCursor.close();
//                    phoneCursor.close();
//                    Log.e("output", "output" + output.toString());
//                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.CONTACT_MODIFIED, String.valueOf(System.currentTimeMillis()));
//                }
//            }
//
//
//        }
//
//        //   if (contactsModels.size() > 0)
//        onSyncRequest();
//
//    }
//
//    private void pushContacts(ArrayList<ContactsModel> contactsModels) {
//        try {
//
//            JSONArray jsonArrayTop = new JSONArray();
//
//            for (int i = 0; i < contactsModels.size(); i++) {
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("FirstName", contactsModels.get(i).getFirstName());
//                jsonObject.put("LastName", "");
//                jsonObject.put("CompanyId", "1");
//                jsonObject.put("CreatedById", "2");
//
//                JSONArray jsonArray = new JSONArray();
//                ArrayList<ContactsModel.Phones> phones = contactsModels.get(i).getPhones();
//
//                for (int j = 0; j < phones.size(); j++) {
//
//                    JSONObject jsonObject1 = new JSONObject();
//
//                    jsonObject1.put("PhoneNumber", phones.get(j).getPhoneNumber());
//
//                    jsonObject1.put("Mode", "");
//
//                    jsonArray.put(jsonObject1);
//
//                }
//                jsonObject.put("Phones", jsonArray);
//                jsonArrayTop.put(jsonObject);
//            }
//
//
//            inputJson = jsonArrayTop.toString();
//
//            new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_POST_CONTACTS,
//                    inputJson,
//                    "Loading...",
//                    this,
//                    Urls.URL_POST_CONTACTS,
//                    Constants.POST).execute();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    ArrayList<ContactsModel>contactsModels;
//    @Override
//    public void userListfragmentBecameVisible(int position, View actionBarView) {
//        //new CommonSyncwithoutstatus(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_ALL_CONTACTS + "?$filter=Groups/any(data:data/Id%20eq%2079)", "", "Loading...", this, Urls.URL_ALL_CONTACTS, Constants.GET).execute();
//
////        contactsFirebaseModels.clear();
////        newcontacts.setVisibility(View.VISIBLE);
////        contactsView.setVisibility(View.GONE);
//
//        if(position==0){
//
//            contactsFirebaseModels=ContatctTable.getInstance().getContacts();
//
//
//        }else{
//            ArrayList<GroupModel> groups = ContatctTable.getInstance().getUniquesGroups();
//         String  id="";
//            for (int index=0;index<groups.size();index++) {
//            if(index==(position-1))
//                id=groups.get(index).getId();
//            }
//
//            contactsFirebaseModels=new ArrayList<>(ContatctTable.getInstance().getGroupContacts(id));
//
//        }
//
//               // contactAdapter = new ContactsAdapter(getActivity(), contactsFirebaseModels);
//                adapterMethod();
//               // contactAdapter.notifyDataSetChanged();
//
//
//
////        contactsView.setAdapter(new NewContactsAdapter(getActivity(), contactsModels));
//
//
//    }
//
//    @Override
//    public void onPriceChange() {
//
//    }
//
//    public void adapterMethod(){
//        getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                // contactAdapter = new ContactsAdapter(getActivity(), contactsFirebaseModels);
////                if(contactAdapter==null) {
//                    contactAdapter = new ContactsAdapter(getActivity(), removeDuplicates(contactsFirebaseModels));
//                    contactsView.setAdapter(contactAdapter);
//                contactAdapter.notifyDataSetChanged();
////                }else{
////                    contactAdapter=null;
////                    contactsView=null;
////                    contactAdapter = new ContactsAdapter(getActivity(), removeDuplicates(contactsFirebaseModels));
////                    contactsView.setAdapter(contactAdapter);
////                }
//                // contactAdapter.notifyDataSetChanged();
//            }
//        });
//
//    }
//}