package com.bnhabitat.ui.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.LegalModel;
import com.bnhabitat.models.OtherExpensesModel;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.models.PropAreaModel;
import com.bnhabitat.models.PropertyOwnerModel;
import com.bnhabitat.models.PropertyOwnerShipProofs;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.ui.activities.MyPropertyActivity;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.adapters.LegalAdapter;
import com.bnhabitat.ui.adapters.LegalCompanyAdapter;
import com.bnhabitat.ui.adapters.OtherExpensedAdapter;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.ui.views.CustomTextView;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.google.gson.JsonArray;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class LegalStatusFragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener {
    private static int RESULT_LOAD_IMAGE = 1;
    ArrayList<PropertyUnitsModel> propertyUnitsModels;
    ArrayList<PropertyOwnerModel> propertyOwnerModels = new ArrayList<>();
    ArrayList<String> property_units = new ArrayList<>();
    PropertyOwnerModel currentpropertyOwnerModel;
    SendMessage SM;
    int mYear, mMonth, mDay;
    private JSONObject jsonObject22;
    private JSONObject jsonObject;
    private JSONObject jsonObject1;
    //image_view_lay;
    RadioGroup ownership_status_radiogroup;
    RadioButton radio_free_hold, radio_lease_hold, radio_individual, radio_multiple_owner,
            company_owner_radio, radio_gpa, radio_share, radio_builder_buyer,
            radio_convence_deed, radio_registery, radio_pul_registery,
            radio_builder_transfer, radio_share_transfer, radio_pull_gpa;
    String PropertyId;
    LinearLayout image_view_infate, image_view_inflate1, image_view_inflate2, upload_photos, upload_photos1, upload_photos2, lease_checked_lay, individual_lay, individual_lay1, individual_lay2, company_org_la;
    EditText no_tenure, lease_amount_pay, pull_from_txt, company_organization, first_name, first_name1, first_name2, last_name, last_name1, last_name2, contact_no, contact_no1, contact_no2, email, email1, email2, pan_no, pan_no1, pan_no2, adhar_no, adhar_no1, adhar_no2, occupassion, occupassion1, occupassion2, address, address1, address2;
    //multiple_owner_lay2
    Spinner payment_circle, company_type;
    String payment_circle_txt, encodedImageData;
    ImageView next_legal_btn;
    String userChoosenTask;
    private static int REQUEST_CAMERA = 101;
    private static int SELECT_FILE = 102;
    private static final int REQUEST_CAMERA_PERMISSION = 100;
    int id = 0;
    SpinnerAdapter adapter;
    TextView last_paid_date, date_from, date_of_possesion_legal;
    String radio_ownership_txt, radio_owner_type_text, radio_ownedby_txt, radio_transferway_txt, add_multiple_owner_txt = "", add_company_owner_txt = "", edit_property;
    ArrayList<PhotosModel> photosModels = new ArrayList<>();
    ArrayList<InventoryModel> json_Data;
    private boolean isPermitted = false;
    private String blockCharacterSet = "~#^|$%&/*!'+-:()<>_`€…•™£¥¤§©®¶µ[]αβ√M{}=?";
    int iskeyboaropen = 0;
    String PropertyTypeId;
    ArrayList<PhotosModel> photosModels1 = new ArrayList<>();
    ArrayList<PhotosModel> photosModels2 = new ArrayList<>();
    View view, view_owner, view_owner1, view_owner2;
    LinearLayout add_multi_owner;
    String pid;
    private JSONObject jsonSpecification;
    private JSONArray jsonSpecific;
    private ArrayList<PropAreaModel> propArrayList;
    private String Editid;
    String LegalId = "0";
    String ownerId = "0";
    LinearLayout frameLayout;
    ScrollView scrollView;
    RecyclerView rv_multiple_owner, rv_company;
     ArrayList<LegalModel> legalModelArrayList = new ArrayList<>();
    ArrayList<LegalModel> legalModels = new ArrayList<>();
    ArrayList<LegalModel> legalModelsCompany = new ArrayList<>();
    ArrayList<PropertyOwnerShipProofs> propertyImages = new ArrayList<>();
    CustomTextView lease_allotment;

    LegalAdapter legalAdapter;
    LegalCompanyAdapter legalCompanyAdapter;

    public LegalStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_legal_status, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        onIntializeView(view);
        blockChar();
        onClicks();

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            if (bundle != null) {
                try {
                    pid = bundle.getString("pid");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                edit_property = bundle.getString("edit") == null ? "" : bundle.getString("edit");

                PropertyId = bundle.getString("PropertyId") == null ? "" : bundle.getString("PropertyId");
                PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");

                if (!edit_property.equals("")) {
                    Editid = bundle.getString("editId");

                    json_Data = (ArrayList<InventoryModel>) bundle.getSerializable("json_Data");
                    for (int i = 0; i < json_Data.size(); i++) {

                        if (json_Data.get(i).getId().equalsIgnoreCase(Editid)) {

                            LegalId = json_Data.get(i).getPropertyLegalStatuses().get(0).getId();
                            pull_from_txt.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLegalStatuses().get(0).getTransferRestriction()));
                            date_of_possesion_legal.setText(Utils.getMessageDate(json_Data.get(i).getPropertyLegalStatuses().get(0).getPossessionDate()));
                            date_from.setText(Utils.getMessageDate(json_Data.get(i).getPropertyLegalStatuses().get(0).getDateFrom()));
                            no_tenure.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLegalStatuses().get(0).getNoOfTennure()));

                            if (!json_Data.get(i).getPropertyLegalStatuses().get(0).getLeaseAlignmentPaid().equalsIgnoreCase("null")|| !json_Data.get(i).getPropertyLegalStatuses().get(0).getLeaseAlignmentPaid().equalsIgnoreCase("0")   ){

                                lease_allotment.setText(Utils.getMessageDate(json_Data.get(i).getPropertyLegalStatuses().get(0).getLeaseAlignmentPaid()));

                            }
                            else{

                                lease_allotment.setText("0");

                            }


                            lease_amount_pay.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyLegalStatuses().get(0).getLeaseAmountPayable()));
                            last_paid_date.setText(Utils.getMessageDate(json_Data.get(i).getPropertyLegalStatuses().get(0).getLastPaidDate()));

                            if (json_Data.get(i).getPropertyLegalStatuses().get(0).getOwnerShipStatus().equalsIgnoreCase("Free Hold")) {
                                radio_free_hold.setChecked(true);

                            } else {
                                radio_free_hold.setChecked(false);
                                radio_lease_hold.setChecked(true);
                                lease_checked_lay.setVisibility(View.VISIBLE);
                            }

                            if (json_Data.get(i).getPropertyLegalStatuses().get(0).getTransferByWayOf().equalsIgnoreCase("Registery")) {
                                radio_pul_registery.setChecked(true);
                                radio_builder_transfer.setChecked(false);
                                radio_share_transfer.setChecked(false);
                                radio_pull_gpa.setChecked(false);

                            } else if (json_Data.get(i).getPropertyLegalStatuses().get(0).getTransferByWayOf().equalsIgnoreCase("Builder transfer")) {
                                radio_pul_registery.setChecked(false);
                                radio_builder_transfer.setChecked(true);
                                radio_share_transfer.setChecked(false);
                                radio_pull_gpa.setChecked(false);

                            } else if (json_Data.get(i).getPropertyLegalStatuses().get(0).getTransferByWayOf().equalsIgnoreCase("Share Transfer")) {
                                radio_pul_registery.setChecked(false);
                                radio_builder_transfer.setChecked(false);
                                radio_share_transfer.setChecked(true);
                                radio_pull_gpa.setChecked(false);

                            } else {
                                radio_pul_registery.setChecked(false);
                                radio_builder_transfer.setChecked(false);
                                radio_share_transfer.setChecked(false);
                                radio_pull_gpa.setChecked(true);
                            }

                            if (json_Data.get(i).getPropertyLegalStatuses().get(0).getOwnerShipType().equalsIgnoreCase("GPA/SPA")) {
                                radio_gpa.setChecked(true);
                                radio_share.setChecked(false);
                                radio_builder_buyer.setChecked(false);
                                radio_convence_deed.setChecked(false);
                                radio_registery.setChecked(false);

                            } else if (json_Data.get(i).getPropertyLegalStatuses().get(0).getOwnerShipType().equalsIgnoreCase("Builder buyer")) {
                                radio_gpa.setChecked(false);
                                radio_share.setChecked(false);
                                radio_builder_buyer.setChecked(true);
                                radio_convence_deed.setChecked(false);
                                radio_registery.setChecked(false);
                            } else if (json_Data.get(i).getPropertyLegalStatuses().get(0).getOwnerShipType().equalsIgnoreCase("Share certificate")) {
                                radio_gpa.setChecked(false);
                                radio_share.setChecked(true);
                                radio_builder_buyer.setChecked(false);
                                radio_convence_deed.setChecked(false);
                                radio_registery.setChecked(false);

                            } else if (json_Data.get(i).getPropertyLegalStatuses().get(0).getOwnerShipType().equalsIgnoreCase("Convence deed")) {
                                radio_gpa.setChecked(false);
                                radio_share.setChecked(false);
                                radio_builder_buyer.setChecked(false);
                                radio_convence_deed.setChecked(true);
                                radio_registery.setChecked(false);

                            } else {
                                radio_gpa.setChecked(false);
                                radio_share.setChecked(false);
                                radio_builder_buyer.setChecked(false);
                                radio_convence_deed.setChecked(false);
                                radio_registery.setChecked(true);
                            }

                            if (json_Data.get(i).getPropertyLegalStatuses().get(0).getOwnedBy().equalsIgnoreCase("Individual")) {
                                radio_individual.setChecked(true);
                                if (json_Data.get(i).getPropertyOwnerses().size() > 0) {
                                    ownerId = json_Data.get(i).getPropertyOwnerses().get(0).getId();

                                    if (!json_Data.get(i).getPropertyOwnerses().get(0).getFirstName().equalsIgnoreCase("null"))
                                    first_name.setText(json_Data.get(i).getPropertyOwnerses().get(0).getFirstName());
                                    else
                                    first_name.setText("");

                                    if (!json_Data.get(i).getPropertyOwnerses().get(0).getLastName().equalsIgnoreCase("null"))

                                     last_name.setText(json_Data.get(i).getPropertyOwnerses().get(0).getLastName());
                                    else
                                        last_name.setText("");

                                    if (!json_Data.get(i).getPropertyOwnerses().get(0).getEmail().equalsIgnoreCase("null"))

                                        email.setText(json_Data.get(i).getPropertyOwnerses().get(0).getEmail());
                                    else
                                        email.setText("");

                                    if (!json_Data.get(i).getPropertyOwnerses().get(0).getPhoneNumber().equalsIgnoreCase("null"))

                                        contact_no.setText(json_Data.get(i).getPropertyOwnerses().get(0).getPhoneNumber());
                                    else
                                        contact_no.setText("");

                                    if (!json_Data.get(i).getPropertyOwnerses().get(0).getAddress().equalsIgnoreCase("null"))

                                        address.setText(json_Data.get(i).getPropertyOwnerses().get(0).getAddress());
                                    else
                                        address.setText("");

                                    if (!json_Data.get(i).getPropertyOwnerses().get(0).getOccupation().equalsIgnoreCase("null"))

                                        occupassion.setText(json_Data.get(i).getPropertyOwnerses().get(0).getOccupation());
                                    else
                                        occupassion.setText("");

                                    pan_no.setText(json_Data.get(i).getPropertyOwnerses().get(0).getPanNo());
                                    adhar_no.setText(json_Data.get(i).getPropertyOwnerses().get(0).getAdhaarCardNo());

                                    propertyImages.clear();

                                    if(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().size() > 0){

                                            for(int index = 0; index < json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().size(); index++){
                                                PropertyOwnerShipProofs propertyOwnerShipProofs = new PropertyOwnerShipProofs();
                                                propertyOwnerShipProofs.setId(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().get(index).getId());
                                               // propertyOwnerShipProofs.setName(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().get(index).getName());
                                              //  propertyOwnerShipProofs.setType(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().get(index).getType());
                                                propertyOwnerShipProofs.setUrl(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().get(index).getUrl());
                                                propertyImages.add(propertyOwnerShipProofs);
                                            }

                                            imageinflateview(propertyImages);
                                    }
                                }

                            }

                            else if (json_Data.get(i).getPropertyLegalStatuses().get(0).getOwnedBy().equalsIgnoreCase("Multiple Owner")) {
                                radio_multiple_owner.setChecked(true);
                                company_owner_radio.setChecked(false);
                                rv_company.setVisibility(View.GONE);
                                add_multi_owner.setVisibility(View.VISIBLE);
                                rv_multiple_owner.setVisibility(View.VISIBLE);
                                radio_individual.setChecked(false);
                                individual_lay.setVisibility(View.GONE);
                                int photoPos=0;
                                legalModels.clear();
                                for (int index = 0; index < json_Data.get(i).getPropertyOwnerses().size(); index++) {

                                    LegalModel legalModel = new LegalModel();
                                    legalModel.setId(Integer.parseInt(json_Data.get(i).getPropertyOwnerses().get(index).getId()));
                                    legalModel.setFirstName(json_Data.get(i).getPropertyOwnerses().get(index).getFirstName());
                                    legalModel.setLastName(json_Data.get(i).getPropertyOwnerses().get(index).getLastName());
                                    legalModel.setPhoneNumber(json_Data.get(i).getPropertyOwnerses().get(index).getPhoneNumber());
                                    legalModel.setEmail(json_Data.get(i).getPropertyOwnerses().get(index).getEmail());
                                    legalModel.setPanNo(json_Data.get(i).getPropertyOwnerses().get(index).getPanNo());
                                    legalModel.setAdhaarCardNo(json_Data.get(i).getPropertyOwnerses().get(index).getAdhaarCardNo());
                                    legalModel.setOccupation(json_Data.get(i).getPropertyOwnerses().get(index).getOccupation());
                                    legalModel.setAddress(json_Data.get(i).getPropertyOwnerses().get(index).getAddress());
                                    legalModel.setPropertyOwnerShipProofs(json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs());
                                    if (json_Data.get(i).getPropertyOwnerses().get(index).getCompanyType().equalsIgnoreCase("Multiple Owner")){
                                        legalModels.add(legalModel);

                                        if(json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs().size() > 0){

                                            for(int index1 = 0; index1 < json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs().size(); index1++){
                                                PhotosModel photosModel = new PhotosModel();
                                                photosModel.setId(json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs().get(index1).getId());
                                                photosModel.setPos(photoPos);
                                                // propertyOwnerShipProofs.setName(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().get(index).getName());
                                                //  propertyOwnerShipProofs.setType(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().get(index).getType());
                                                photosModel.setImageUrl(json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs().get(index1).getUrl());
                                                photosModels1.add(photosModel);
                                            }

                                        }
                                        photoPos++;
                                    }


                                }

                                //  photosModels1.clear();



                                setLegalAdapter();

                                legalAdapter.notifyDataSetChanged();

                            } else {
                                company_owner_radio.setChecked(true);
                                individual_lay.setVisibility(View.GONE);
                                add_multi_owner.setVisibility(View.GONE);
                                rv_multiple_owner.setVisibility(View.GONE);
                                radio_multiple_owner.setChecked(false);
                                radio_individual.setChecked(false);
                                rv_company.setVisibility(View.VISIBLE);



                                legalModels.clear();
                                for (int index = 0; index < json_Data.get(i).getPropertyOwnerses().size(); index++) {

                                    LegalModel legalModel = new LegalModel();
                                    legalModel.setId(Integer.parseInt(json_Data.get(i).getPropertyOwnerses().get(index).getId()));
                                    legalModel.setFirstName(json_Data.get(i).getPropertyOwnerses().get(index).getFirstName());
                                    legalModel.setLastName(json_Data.get(i).getPropertyOwnerses().get(index).getLastName());
                                    legalModel.setPhoneNumber(json_Data.get(i).getPropertyOwnerses().get(index).getPhoneNumber());
                                    legalModel.setEmail(json_Data.get(i).getPropertyOwnerses().get(index).getEmail());
                                    legalModel.setPanNo(json_Data.get(i).getPropertyOwnerses().get(index).getPanNo());
                                    legalModel.setAdhaarCardNo(json_Data.get(i).getPropertyOwnerses().get(index).getAdhaarCardNo());
                                    legalModel.setOccupation(json_Data.get(i).getPropertyOwnerses().get(index).getOccupation());
                                    legalModel.setAddress(json_Data.get(i).getPropertyOwnerses().get(index).getAddress());
                                    if (json_Data.get(i).getPropertyOwnerses().get(index).getCompanyType().equalsIgnoreCase("Company/ORG")){
                                        legalModelsCompany.add(legalModel);
                                        if(json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs().size() > 0){
                                            for(int index1 = 0; index1 < json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs().size(); index1++){
                                                PhotosModel photosModel = new PhotosModel();
                                                photosModel.setId(json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs().get(index1).getId());
                                               photosModel.setPos(index);
                                                // propertyOwnerShipProofs.setName(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().get(index).getName());
                                                //  propertyOwnerShipProofs.setType(json_Data.get(i).getPropertyOwnerses().get(0).getPropertyOwnerShipProofs().get(index).getType());
                                                photosModel.setImageUrl(json_Data.get(i).getPropertyOwnerses().get(index).getPropertyOwnerShipProofs().get(index1).getUrl());
                                                photosModels2.add(photosModel);
                                            }

                                        }
                                    }



                                    setCompanylAdapter();

                                    legalCompanyAdapter.notifyDataSetChanged();



                                }

//                                photosModels2.clear();


                            }

                            /*for (int index = 0; index < json_Data.get(i).getPropertyOwnerses().size(); index++) {
                                first_name.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyOwnerses().get(index).getFirstName()));
                                last_name.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyOwnerses().get(index).getLastName()));
                                contact_no.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyOwnerses().get(index).getPhoneNumber()));
                                email.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyOwnerses().get(index).getEmail()));
                                pan_no.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyOwnerses().get(index).getPanNo()));
                                adhar_no.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyOwnerses().get(index).getAdhaarCardNo()));
                                occupassion.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyOwnerses().get(index).getOccupation()));
                                address.setText(Utils.getEmptyValue(json_Data.get(i).getPropertyOwnerses().get(index).getAddress()));
                            }*/
                            try {
                                String payment_circle_str = json_Data.get(i).getPropertyLegalStatuses().get(0).getPaymentCircle();
                                if (payment_circle_str != null) {
                                    int spinnerPos = adapter.getPosition(payment_circle_str);
                                    payment_circle.setSelection(spinnerPos);
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            propertyUnitsModels = (ArrayList<PropertyUnitsModel>) bundle.getSerializable("propertyunits");
            Log.e("propertyUnitsModels", "propertyUnitsModels" + propertyUnitsModels.size());
        }

        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (iskeyboaropen == 1) {
                    hideSoftKeyboard(getActivity());
                    iskeyboaropen = 0;
                }
            }
        });

        frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = frameLayout.getRootView().getHeight() - frameLayout.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");

                    iskeyboaropen = 1;
                } else {
                    Log.e("MyActivity", "keyboard closed");
                    iskeyboaropen = 0;
                }
            }
        });

/*
        scrollView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(getActivity());
            }
        });
*/

        date_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentDate1 = Calendar.getInstance();
                mYear = mcurrentDate1.get(Calendar.YEAR);
                mMonth = mcurrentDate1.get(Calendar.MONTH);
                mDay = mcurrentDate1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        /*      Your code   to get date and time    */

                        date_from.setText(selectedday + "/" + (selectedmonth + 1) + "/" + selectedyear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker1.setTitle("Select Last Paid Date");
                mDatePicker1.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker1.show();
            }
        });
        last_paid_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentDate1 = Calendar.getInstance();
                mYear = mcurrentDate1.get(Calendar.YEAR);
                mMonth = mcurrentDate1.get(Calendar.MONTH);
                mDay = mcurrentDate1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        /*      Your code   to get date and time    */

                        last_paid_date.setText(selectedday + "/" + (selectedmonth + 1) + "/" + selectedyear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker1.setTitle("Select Last Paid Date");
                mDatePicker1.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker1.show();
            }
        });
        date_of_possesion_legal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentDate1 = Calendar.getInstance();
                mYear = mcurrentDate1.get(Calendar.YEAR);
                mMonth = mcurrentDate1.get(Calendar.MONTH);
                mDay = mcurrentDate1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        /*      Your code   to get date and time    */

                        date_of_possesion_legal.setText(selectedday + "/" + (selectedmonth + 1) + "/" + selectedyear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker1.setTitle("Select Last Paid Date");
                mDatePicker1.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker1.show();
            }
        });
        payment_circle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (payment_circle.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    payment_circle_txt = payment_circle.getSelectedItem().toString();


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        radio_free_hold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_ownership_txt = "Free Hold";
                    lease_checked_lay.setVisibility(View.GONE);


                }
            }
        });
        radio_lease_hold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_ownership_txt = "Lease Hold";
                    lease_checked_lay.setVisibility(View.VISIBLE);


                }
            }
        });

        radio_individual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_multiple_owner.setChecked(false);
                    company_owner_radio.setChecked(false);
                    onIndividualInflate();
                    setlayout();
                    photosModels1.clear();
                    photosModels2.clear();
//                  multiple_owner_lay.setVisibility(View.GONE);
                    individual_lay.setVisibility(View.VISIBLE);
                    rv_multiple_owner.setVisibility(View.GONE);
                    rv_company.setVisibility(View.GONE);
                    add_multi_owner.setVisibility(View.GONE);
                }
            }
        });
        radio_multiple_owner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_individual.setChecked(false);
                    company_owner_radio.setChecked(false);
                    //  onMultipleInflate();
                    setlayout();
                    photosModels2.clear();
                    photosModels.clear();
                    individual_lay.setVisibility(View.GONE);
                    rv_multiple_owner.setVisibility(View.VISIBLE);
                    rv_company.setVisibility(View.GONE);

//                  multiple_owner_lay.setVisibility(View.VISIBLE);
                    individual_lay.setVisibility(View.GONE);
                    //  company_owner_lay.setVisibility(View.GONE);
                    //   company_org_lay.setVisibility(View.GONE);
                    add_multi_owner.setVisibility(View.VISIBLE);
                }
            }
        });
        company_owner_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    radio_multiple_owner.setChecked(false);
                    radio_individual.setChecked(false);
                    //onCompanyInflate();
                    setlayout();
                    photosModels1.clear();
                    photosModels.clear();
                    individual_lay.setVisibility(View.GONE);
                    rv_multiple_owner.setVisibility(View.GONE);
                    rv_company.setVisibility(View.VISIBLE);
//                    multiple_owner_lay.setVisibility(View.GONE);
                    individual_lay.setVisibility(View.GONE);
                    add_multi_owner.setVisibility(View.GONE);
                    // company_owner_lay.setVisibility(View.VISIBLE);
                    //   company_org_lay.setVisibility(View.VISIBLE);
                }
            }
        });

        next_legal_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    radioClickEvent();
                    setpropertyOwnerData();
                    if (currentpropertyOwnerModel.getFirst_name().equalsIgnoreCase("")) {
                        if (propertyOwnerModels.isEmpty()) {

                        } else {
                            propertyOwnerModels.add(currentpropertyOwnerModel);
                        }
                    } else {
                        propertyOwnerModels.add(currentpropertyOwnerModel);
                    }

                    if (radio_individual.isChecked()) {
                        onLegalStatus();

                       /* if (!email.getText().toString().trim().equalsIgnoreCase("")) {

                            if (!email.getText().toString().trim().equalsIgnoreCase("")){

                                if (validateEmail(email.getText().toString().trim()))
                                    onLegalStatus();
                                else
                                    Utils.showErrorMessage("Please enter the valid email", getActivity());
                            }

                        } else {
                            if (!pan_no.getText().toString().equalsIgnoreCase("") && !adhar_no.getText().toString().equalsIgnoreCase(""))
                                onLegalStatus();
                            else if (pan_no.getText().toString().equalsIgnoreCase("") || pan_no.length() < 10)
                                Utils.showErrorMessage("Enter valid Pan No. please", getActivity());
                            else if (adhar_no.getText().toString().equalsIgnoreCase("") || adhar_no.length()< 12 || adhar_no.length()> 12)
                                Utils.showErrorMessage("Enter 12 digit valid Adhar No.", getActivity());
                        }*/
                    } else if (radio_multiple_owner.isChecked()) {

/*
                        for(int i=0;i<legalModels.size();i++){


                            if (legalModels.get(i).getEmail().equalsIgnoreCase("")) {
                                Utils.showErrorMessage("Please enter the  email", getActivity());
                                return;

                            }


                              else   if (!legalModels.get(i).getEmail().equalsIgnoreCase("")&& !validateEmail(legalModels.get(i).getEmail()))
                            {
                                    Utils.showErrorMessage("Please enter the valid email", getActivity());
                            return;
                            }


                          */
/*  else if (!validateEmail(legalModels.get(i).getEmail())){

                                Utils.showErrorMessage("Please enter the valid email", getActivity());
                            }*//*

                             else if (legalModels.get(i).getPanNo().equalsIgnoreCase("") || legalModels.get(i).getPanNo().length() < 10){
                                Utils.showErrorMessage("Enter valid Pan No. please", getActivity());
                                return;
                            }

                            else if (legalModels.get(i).getAdhaarCardNo().equalsIgnoreCase("") || legalModels.get(i).getAdhaarCardNo().length()< 12 || legalModels.get(i).getAdhaarCardNo().length()> 12){


                                Utils.showErrorMessage("Enter 12 digit valid Adhar No.", getActivity());
                                return;
                            }

                            else{
                                onLegalStatus();
                                return;
                            }

                        }
*/
                        onLegalStatus();

                    }
                    if (company_owner_radio.isChecked()) {

/*
                            for(int i=0;i<legalModelsCompany.size();i++){

                                if (legalModelsCompany.get(i).getEmail().equalsIgnoreCase("")) {
                                    Utils.showErrorMessage("Please enter the  email", getActivity());
                                    return;
                                }

                                else  if (!legalModelsCompany.get(i).getEmail().equalsIgnoreCase("")&& !validateEmail(legalModelsCompany.get(i).getEmail()))
                                {
                                    Utils.showErrorMessage("Please enter the valid email", getActivity());
                                return;
                                }
                               */
/* else {

                                    if (!validateEmail(legalModelsCompany.get(i).getEmail()))

                                        Utils.showErrorMessage("Please enter the valid email", getActivity());

                                }*//*


*/
/*
                                else if (!validateEmail(legalModelsCompany.get(i).getEmail())){

                                    Utils.showErrorMessage("Please enter the valid email", getActivity());
                                }
*//*

                                else if (legalModelsCompany.get(i).getPanNo().equalsIgnoreCase("") || legalModelsCompany.get(i).getPanNo().length() < 10){
                                    Utils.showErrorMessage("Enter valid Pan No. please", getActivity());
                                return;
                                }

*/
/*
                                else if (legalModelsCompany.get(i).getAdhaarCardNo().equalsIgnoreCase("") || legalModelsCompany.get(i).getAdhaarCardNo().length() < 16){
                                    Utils.showErrorMessage("Enter valid Adhar No. please", getActivity());
                                }
*//*


                                else if (legalModelsCompany.get(i).getAdhaarCardNo().equalsIgnoreCase("") || legalModelsCompany.get(i).getAdhaarCardNo().length()< 12 || legalModelsCompany.get(i).getAdhaarCardNo().length()> 12){


                                    Utils.showErrorMessage("Enter 12 digit valid Adhar No.", getActivity());
                                    return;
                                }

                                else{
                                    onLegalStatus();
                                    return;
                                }

                            }
*/


                        onLegalStatus();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        add_multi_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radio_multiple_owner.isChecked()) {

                    add_multiple_owner_txt = "multiple_owner";

                    LegalModel legalModel = new LegalModel();
                    legalModel.setId(0);
                    legalModel.setFirstName("");
                    legalModel.setLastName("");
                    legalModel.setPhoneNumber("");
                    legalModel.setEmail("");
                    legalModel.setPanNo("");
                    legalModel.setAdhaarCardNo("");
                    legalModel.setAddress("");
                    legalModel.setOccupation("");
                    legalModels.add(legalModel);

                    legalAdapter.notifyDataSetChanged();

                } else {
                    Utils.showErrorMessage("First you need to checked Multiple Owner", getActivity());
                }
            }
        });

        return view;
    }

    private void imageinflateview(final ArrayList<PropertyOwnerShipProofs> propertyImages) {

        image_view_infate.removeAllViews();

        for (int i = 0; i < propertyImages.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + propertyImages.get(i).getUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + propertyImages.get(i).getUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   id=(Integer)view.getTag();

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(String.valueOf(propertyImages.get(id).getId()));
                                    propertyImages.remove(id);
                                    imageinflateview();
                                }
                            })
                            .show();

                }
            });

            image_view_infate.addView(view);

        }
    }

    private void onClicks() {

        lease_allotment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentDate1 = Calendar.getInstance();
                mYear = mcurrentDate1.get(Calendar.YEAR);
                mMonth = mcurrentDate1.get(Calendar.MONTH);
                mDay = mcurrentDate1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        /*      Your code   to get date and time    */

                        String.format("%02d", selectedmonth + 1);
                        String dd= String.format("%02d", selectedmonth + 1) + "/" + selectedday + "/" + selectedyear;
                        lease_allotment.setText(dd);



                        //   holder.tv_date.setText(selectedday + "/" + (selectedmonth + 1) + "/" + selectedyear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker1.setTitle("Select Last Paid Date");
                mDatePicker1.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker1.show();

            }
        });

        radio_gpa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_gpa.setChecked(true);
                radio_share.setChecked(false);
                radio_builder_buyer.setChecked(false);
                radio_convence_deed.setChecked(false);
                radio_registery.setChecked(false);
            }
        });

        radio_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_share.setChecked(true);
                radio_gpa.setChecked(false);
                radio_builder_buyer.setChecked(false);
                radio_convence_deed.setChecked(false);
                radio_registery.setChecked(false);
            }
        });


        radio_builder_buyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_builder_buyer.setChecked(true);
                radio_share.setChecked(false);
                radio_gpa.setChecked(false);
                radio_convence_deed.setChecked(false);
                radio_registery.setChecked(false);
            }
        });

        radio_convence_deed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_convence_deed.setChecked(true);
                radio_share.setChecked(false);
                radio_builder_buyer.setChecked(false);
                radio_gpa.setChecked(false);
                radio_registery.setChecked(false);
            }
        });

        radio_registery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_registery.setChecked(true);
                radio_share.setChecked(false);
                radio_builder_buyer.setChecked(false);
                radio_convence_deed.setChecked(false);
                radio_gpa.setChecked(false);
            }
        });

        //////////////////

        radio_pul_registery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_pul_registery.setChecked(true);
                radio_builder_transfer.setChecked(false);
                radio_share_transfer.setChecked(false);
                radio_pull_gpa.setChecked(false);
            }
        });


        radio_builder_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_builder_transfer.setChecked(true);
                radio_pul_registery.setChecked(false);
                radio_share_transfer.setChecked(false);
                radio_pull_gpa.setChecked(false);
            }
        });

        radio_share_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_share_transfer.setChecked(true);
                radio_builder_transfer.setChecked(false);
                radio_pul_registery.setChecked(false);
                radio_pull_gpa.setChecked(false);
            }
        });


        radio_pull_gpa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radio_pull_gpa.setChecked(true);
                radio_builder_transfer.setChecked(false);
                radio_share_transfer.setChecked(false);
                radio_pul_registery.setChecked(false);
            }
        });


    }

    private void blockChar() {
        pull_from_txt.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(100)});
    }

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    public void onEvent(ArrayList<LegalModel> legalModelArray) {
        legalModelArrayList.clear();

        for (int i = 0; i < legalModelArray.size(); i++) {
            LegalModel legalModel = new LegalModel();
            legalModel.setId(legalModelArray.get(i).getId());
            legalModel.setFirstName(legalModelArray.get(i).getFirstName());
            legalModel.setLastName(legalModelArray.get(i).getLastName());
            legalModel.setPhoneNumber(legalModelArray.get(i).getPhoneNumber());
            legalModel.setEmail(legalModelArray.get(i).getEmail());
            legalModel.setAdhaarCardNo(legalModelArray.get(i).getAdhaarCardNo());
            legalModel.setAddress(legalModelArray.get(i).getAddress());
            legalModel.setPanNo(legalModelArray.get(i).getPanNo());
            legalModel.setOccupation(legalModelArray.get(i).getOccupation());
            legalModelArrayList.add(legalModel);
            //   legalAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
            ((PropertyItemActivity) getActivity()).save_draft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
//
                        radioClickEvent();
                        setpropertyOwnerData();
                        if (currentpropertyOwnerModel.getFirst_name().equalsIgnoreCase("")) {
                            if (propertyOwnerModels.isEmpty()) {

                            } else {
                                propertyOwnerModels.add(currentpropertyOwnerModel);
                            }
                        } else {
                            propertyOwnerModels.add(currentpropertyOwnerModel);
                        }

                        onLegalDraftStatus();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    private void radioClickEvent() {
        if (radio_gpa.isChecked()) {
            radio_owner_type_text = radio_gpa.getText().toString();
        } else if (radio_share.isChecked()) {
            radio_owner_type_text = radio_share.getText().toString();

        } else if (radio_builder_buyer.isChecked()) {
            radio_owner_type_text = radio_builder_buyer.getText().toString();
        } else if (radio_convence_deed.isChecked()) {
            radio_owner_type_text = radio_convence_deed.getText().toString();
        } else if (radio_registery.isChecked()) {
            radio_owner_type_text = radio_registery.getText().toString();
        }

        if (radio_individual.isChecked()) {
            radio_ownedby_txt = "Individual";

        } else if (radio_multiple_owner.isChecked()) {
            radio_ownedby_txt = "Multiple Owner";
        } else if (company_owner_radio.isChecked()) {
            radio_ownedby_txt = "Company/ORG";
        }

        if (radio_pul_registery.isChecked()) {
            radio_transferway_txt = radio_pul_registery.getText().toString();

        } else if (radio_builder_transfer.isChecked()) {
            radio_transferway_txt = radio_builder_transfer.getText().toString();
        } else if (radio_share_transfer.isChecked()) {
            radio_transferway_txt = radio_share_transfer.getText().toString();
        } else if (radio_pull_gpa.isChecked()) {
            radio_transferway_txt = radio_pull_gpa.getText().toString();
        }
    }

    private void setlayout() {

        if (radio_individual.isChecked()) {
            individual_lay.removeAllViews();
            individual_lay.addView(view_owner);

        } else if (radio_multiple_owner.isChecked())

            addDataToLegal();


        else if (company_owner_radio.isChecked())

            addDataToLegalCompany();

    }

    private void onIndividualInflate() {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view_owner = inflater.inflate(R.layout.legal_owner_inflate_view, null);
        individual_lay = (LinearLayout) view.findViewById(R.id.individual_lay);
        first_name = (EditText) view_owner.findViewById(R.id.first_name);
        last_name = (EditText) view_owner.findViewById(R.id.last_name);
        contact_no = (EditText) view_owner.findViewById(R.id.contact_no);
        email = (EditText) view_owner.findViewById(R.id.email);
        pan_no = (EditText) view_owner.findViewById(R.id.pan_no);
        adhar_no = (EditText) view_owner.findViewById(R.id.adhar_no);
        occupassion = (EditText) view_owner.findViewById(R.id.occupassion);
        address = (EditText) view_owner.findViewById(R.id.address);
        upload_photos = (LinearLayout) view_owner.findViewById(R.id.upload_photos);
        image_view_infate = (LinearLayout) view_owner.findViewById(R.id.image_view_infate1);

        upload_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkRunTimePermission();

            }
        });

        first_name.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(100)});
        last_name.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(100)});
        contact_no.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});
        pan_no.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});
        adhar_no.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(14)});
        occupassion.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
       // address.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(200)});
    }

    public void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {
            // if already permition granted
            selectImage();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean openActivityOnce = true;
        boolean openDialogOnce = true;
        if (requestCode == 11111) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView();
                        }
                    }
                }
            }

            selectImage();

        }
    }

    private void alertView() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.AppTheme);

        dialog.setTitle("Permission Denied")
                .setInverseBackgroundForced(true)
                //.setIcon(R.drawable.ic_info_black_24dp)
                .setMessage("Without those permission the app is unable to save your profile. App needs to save profile image in your external storage and also need to get profile image from camera or external storage.Are you sure you want to deny this permission?")

                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }
                })
                .setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        checkRunTimePermission();

                    }
                }).show();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = com.bnhabitat.utils.Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodedImageData = getEncoded64ImageStringFromBitmap(bm);
                postPropertyImages();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ivImage.setImageBitmap(bm);
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
     //   File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encodedImageData = getEncoded64ImageStringFromBitmap(thumbnail);
        postPropertyImages();

    }

    private void imageinflateview() {
        image_view_infate.removeAllViews();

        for (int i = 0; i < photosModels.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   id=(Integer)view.getTag();

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(photosModels.get(id).getId());
                                    photosModels.remove(id);
                                    imageinflateview();
                                }
                            })
                            .show();

                }
            });

            image_view_infate.addView(view);

        }

    }

    private void deletePropertyImages(String position) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REMOVE_PROPERTY_PHOTOS,
                deletephotoInput(position),
                "Loading..",
                this,
                Urls.URL_REMOVE_PROPERTY_PHOTOS,
                Constants.POST).execute();

    }


    private String deletephotoInput(String id) {
        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("Id", id);

            jsondeleteArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }

    public void onIntializeView(View view) {
        frameLayout = (LinearLayout) view.findViewById(R.id.frameLayout);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        ownership_status_radiogroup = (RadioGroup) view.findViewById(R.id.ownership_status);
        radio_individual = (RadioButton) view.findViewById(R.id.radio_individual);
        radio_multiple_owner = (RadioButton) view.findViewById(R.id.radio_multiple_owner);
        company_owner_radio = (RadioButton) view.findViewById(R.id.company_owner_radio);
        radio_lease_hold = (RadioButton) view.findViewById(R.id.radio_lease_hold);
        radio_free_hold = (RadioButton) view.findViewById(R.id.radio_free_hold);
        radio_gpa = (RadioButton) view.findViewById(R.id.radio_gpa);
        radio_convence_deed = (RadioButton) view.findViewById(R.id.radio_convence_deed);
        radio_share = (RadioButton) view.findViewById(R.id.radio_share);
        radio_registery = (RadioButton) view.findViewById(R.id.radio_registery);
        radio_builder_buyer = (RadioButton) view.findViewById(R.id.radio_builder_buyer);
        radio_pull_gpa = (RadioButton) view.findViewById(R.id.radio_pull_gpa);
        radio_share_transfer = (RadioButton) view.findViewById(R.id.radio_share_transfer);
        radio_builder_transfer = (RadioButton) view.findViewById(R.id.radio_builder_transfer);
        radio_pul_registery = (RadioButton) view.findViewById(R.id.radio_pul_registery);
        //     company_org_lay = (LinearLayout) view.findViewById(R.id.company_org_lay);
        company_type = (Spinner) view.findViewById(R.id.company_type);
        company_organization = (EditText) view.findViewById(R.id.company_organization);
        next_legal_btn = (ImageView) view.findViewById(R.id.next_legal_btn);
        date_of_possesion_legal = (TextView) view.findViewById(R.id.date_of_possesion_legal);
        pull_from_txt = (EditText) view.findViewById(R.id.pull_from_txt);
        date_from = (TextView) view.findViewById(R.id.date_from);
        no_tenure = (EditText) view.findViewById(R.id.no_tenure);
        lease_amount_pay = (EditText) view.findViewById(R.id.lease_amount_pay);
        lease_allotment = (CustomTextView) view.findViewById(R.id.lease_allotment);
        last_paid_date = (TextView) view.findViewById(R.id.last_paid_date);
        add_multi_owner = (LinearLayout) view.findViewById(R.id.add_multi_owner);
        payment_circle = (Spinner) view.findViewById(R.id.payment_circle);
        lease_checked_lay = (LinearLayout) view.findViewById(R.id.lease_checked_lay);
        radio_free_hold.setChecked(true);
        radio_pul_registery.setChecked(true);
        radio_individual.setChecked(true);
        onIndividualInflate();
        setlayout();
        rv_multiple_owner = (RecyclerView) view.findViewById(R.id.rv_multiple_owner);
        rv_multiple_owner.setLayoutManager(new LinearLayoutManager(getActivity()));

        rv_company = (RecyclerView) view.findViewById(R.id.rv_company);
        rv_company.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        property_units.add("Monthly");
        property_units.add("Quarterly");
        property_units.add("Yearly");

        adapter.addAll(property_units);
        adapter.add(getString(R.string.select_type));
        payment_circle.setAdapter(adapter);
        payment_circle.setSelection(adapter.getCount());
    }

    @Override
    public void onChangeTab(int position) {


    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    private void onLegalStatus() {

        try {

            // location frag data
            jsonSpecification = new JSONObject();
            jsonSpecification.put("PropertyTypeId", LocationFragment.PropertyTypeId);
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("Cam", OtherExpensesFragment.cam_string);
            jsonSpecification.put("IsDraft", 0);

            if (!edit_property.equals("")) {

                jsonSpecification.put("Id", Editid);

            } else {

                jsonSpecification.put("Id", pid);

            }

            JSONArray jsonLegalArray = new JSONArray();

            JSONObject jsonLegal = new JSONObject();
            jsonLegal.put("Id", LegalId);
            jsonLegal.put("TransferRestriction", pull_from_txt.getText().toString());
            if (radio_free_hold.isChecked())
                jsonLegal.put("OwnerShipStatus", "Free Hold");
            else if (radio_lease_hold.isChecked())
                jsonLegal.put("OwnerShipStatus", "Lease Hold");

            jsonLegal.put("OwnerShipType", radio_owner_type_text);
            jsonLegal.put("TransferByWayOf", radio_transferway_txt);
            jsonLegal.put("OwnedBy", radio_ownedby_txt);
            jsonLegal.put("PossessionDate", date_of_possesion_legal.getText().toString());
            if (date_from.getText().toString().equalsIgnoreCase(""))
                jsonLegal.put("DateFrom", 0);
            else
                jsonLegal.put("DateFrom", date_from.getText().toString());

            if (no_tenure.getText().toString().equalsIgnoreCase(""))
                jsonLegal.put("NoOfTennure", 0);
            else
                jsonLegal.put("NoOfTennure", no_tenure.getText().toString());

            if (lease_allotment.getText().toString().equalsIgnoreCase(""))
                jsonLegal.put("LeaseAlignmentPaid", 0);
            else
                jsonLegal.put("LeaseAlignmentPaid",  lease_allotment.getText().toString());

            if (last_paid_date.getText().toString().equalsIgnoreCase(""))
                jsonLegal.put("LastPaidDate", 0);
            else
                jsonLegal.put("LastPaidDate", last_paid_date.getText().toString());


            if (lease_amount_pay.getText().toString().equalsIgnoreCase(""))

                jsonLegal.put("LeaseAmountPayable", 0);
            else
                jsonLegal.put("LeaseAmountPayable", lease_amount_pay.getText().toString());

            jsonLegal.put("PaymentCircle", payment_circle.getSelectedItem().toString());
            jsonLegal.put("IsDeletable", false);
            jsonLegal.put("IsModify", true);

            jsonLegalArray.put(jsonLegal);

            JSONArray jsonPropOwners = new JSONArray();
            if (radio_individual.isChecked()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Id", ownerId);
                jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);
                jsonObject.put("FirstName", first_name.getText().toString().trim());
                jsonObject.put("LastName", last_name.getText().toString().trim());
                jsonObject.put("PhoneNumber", contact_no.getText().toString().trim());
                jsonObject.put("PanNo", pan_no.getText().toString().trim());
                jsonObject.put("Email", email.getText().toString().trim());
                jsonObject.put("AdhaarCardNo", adhar_no.getText().toString().trim());
                jsonObject.put("Address", address.getText().toString().trim());
                jsonObject.put("Occupation", occupassion.getText().toString().trim());
                jsonObject.put("CompanyType", radio_individual.getText().toString());
                jsonObject.put("CompanyName", "");

                JSONArray PropertyOwnerShipProofs = new JSONArray();
                if (photosModels.size() > 0) {
                    for (int i = 0; i < photosModels.size(); i++) {
                        JSONObject object = new JSONObject();
                        object.put("Id", 0);
                        object.put("Name", photosModels.get(i).getName());
                        object.put("Type", photosModels.get(i).getType());
                        object.put("Url", photosModels.get(i).getImageUrl());
                        object.put("IsDeletable", false);
                        object.put("IsModify", false);
                        PropertyOwnerShipProofs.put(object);
                    }
                }
                jsonObject.put("PropertyOwnerShipProofs", PropertyOwnerShipProofs);
                jsonPropOwners.put(jsonObject);
            } else if (radio_multiple_owner.isChecked()) {
                for (int i = 0; i < legalModelArrayList.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("Id", legalModelArrayList.get(i).getId());
                    jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);
                    jsonObject.put("FirstName", legalModelArrayList.get(i).getFirstName());
                    jsonObject.put("LastName", legalModelArrayList.get(i).getLastName());
                    jsonObject.put("PhoneNumber", legalModelArrayList.get(i).getPhoneNumber());
                    jsonObject.put("PanNo", legalModelArrayList.get(i).getPanNo());
                    jsonObject.put("Email", legalModelArrayList.get(i).getEmail());
                    jsonObject.put("AdhaarCardNo", legalModelArrayList.get(i).getAdhaarCardNo());
                    jsonObject.put("Address", legalModelArrayList.get(i).getAddress());
                    jsonObject.put("Occupation", legalModelArrayList.get(i).getOccupation());
                    jsonObject.put("CompanyType", radio_multiple_owner.getText().toString());
                    jsonObject.put("CompanyName", "");

                    JSONArray PropertyOwnerShipProofs = new JSONArray();
                    if (photosModels1.size() > 0) {
                        for (int j = 0; j < photosModels1.size(); j++) {
                            if (photosModels1.get(j).getPos() == i) {
                                JSONObject object = new JSONObject();
                                object.put("Id", 0);
                                object.put("Name", photosModels1.get(j).getName());
                                object.put("Type", photosModels1.get(j).getType());
                                object.put("Url", photosModels1.get(j).getImageUrl());
                                object.put("IsDeletable", false);
                                object.put("IsModify", false);
                                PropertyOwnerShipProofs.put(object);
                            }

                        }
                    }
                    jsonObject.put("PropertyOwnerShipProofs", PropertyOwnerShipProofs);
                    jsonPropOwners.put(jsonObject);
                }
            } else if (company_owner_radio.isChecked()) {
                for (int i = 0; i < legalModelArrayList.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("Id", legalModelArrayList.get(i).getId());
                    jsonObject.put("CompanyId", Constants.APP_COMPANY_ID);
                    jsonObject.put("FirstName", legalModelArrayList.get(i).getFirstName());
                    jsonObject.put("LastName", legalModelArrayList.get(i).getLastName());
                    jsonObject.put("PhoneNumber", legalModelArrayList.get(i).getPhoneNumber());
                    jsonObject.put("PanNo", legalModelArrayList.get(i).getPanNo());
                    jsonObject.put("Email", legalModelArrayList.get(i).getEmail());
                    jsonObject.put("AdhaarCardNo", legalModelArrayList.get(i).getAdhaarCardNo());
                    jsonObject.put("Address", legalModelArrayList.get(i).getAddress());
                    jsonObject.put("Occupation", legalModelArrayList.get(i).getOccupation());
                    jsonObject.put("CompanyType", company_owner_radio.getText().toString());
                    jsonObject.put("CompanyName", "");

                    JSONArray PropertyOwnerShipProofs = new JSONArray();
                    if (photosModels2.size() > 0) {
                        for (int j = 0; j < photosModels2.size(); j++) {
                            JSONObject object = new JSONObject();
                            object.put("Id", 0);
                            object.put("Name", photosModels2.get(j).getName());
                            object.put("Type", photosModels2.get(j).getType());
                            object.put("Url", photosModels2.get(j).getImageUrl());
                            object.put("IsDeletable", false);
                            object.put("IsModify", false);
                            PropertyOwnerShipProofs.put(object);
                        }
                    }
                    jsonObject.put("PropertyOwnerShipProofs", PropertyOwnerShipProofs);
                    jsonPropOwners.put(jsonObject);
                }
            }


            jsonSpecification.put("PropertyLegalStatus", jsonLegalArray);
            jsonSpecification.put("PropertyOwners", jsonPropOwners);

            postProperty();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void onLegalDraftStatus() {

        try {

            // location frag data
            jsonSpecification = new JSONObject();
            jsonSpecification.put("Id", pid);
            jsonSpecification.put("PropertyTypeId", LocationFragment.PropertyTypeId);
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("IsDraft", 1);


            postProperty_draft();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void postProperty() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_POST,
                Constants.POST).execute();

    }

    private void postProperty_draft() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Constants.URL_PROPERTY_POST_DRAFT,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = jsonSpecification.toString();
        } catch (Exception e) {

        }


        return inputStr;
    }

    private void setpropertyOwnerData() {

        currentpropertyOwnerModel = new PropertyOwnerModel();
        currentpropertyOwnerModel.setFirst_name(first_name.getText().toString());
        currentpropertyOwnerModel.setLast_name(last_name.getText().toString());
        currentpropertyOwnerModel.setContact_no(contact_no.getText().toString());
        currentpropertyOwnerModel.setEmail_id(email.getText().toString());
        currentpropertyOwnerModel.setPan_no(pan_no.getText().toString());
        currentpropertyOwnerModel.setAdhar_no(adhar_no.getText().toString());
        currentpropertyOwnerModel.setOccupassion(occupassion.getText().toString());
        currentpropertyOwnerModel.setAddress(address.getText().toString());

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void addDataToLegal() {

        legalModels.clear();

        LegalModel legalModel = new LegalModel();
        legalModel.setId(0);
        legalModel.setFirstName("");
        legalModel.setLastName("");
        legalModel.setPhoneNumber("");
        legalModel.setEmail("");
        legalModel.setPanNo("");
        legalModel.setAdhaarCardNo("");
        legalModel.setAddress("");
        legalModel.setOccupation("");
        legalModels.add(legalModel);

        setLegalAdapter();

    }

    private void addDataToLegalCompany() {

        legalModelsCompany.clear();

        LegalModel legalModel = new LegalModel();
        legalModel.setId(0);
        legalModel.setFirstName("");
        legalModel.setLastName("");
        legalModel.setPhoneNumber("");
        legalModel.setEmail("");
        legalModel.setPanNo("");
        legalModel.setAdhaarCardNo("");
        legalModel.setAddress("");
        legalModel.setOccupation("");
        legalModelsCompany.add(legalModel);

        setCompanylAdapter();

    }

    private void setLegalAdapter() {

        legalAdapter = new LegalAdapter(getActivity(), legalModels, photosModels1, LegalStatusFragment.this, new LegalAdapter.CallBack() {
            @Override
            public void onPhotoClick(int pos) {
                MultipleOwnerPhotoPosition = pos;
                checkRunTimePermission();
            }

            @Override
            public void onPhotoDelete(int pos) {
                photosModels1.remove(pos);
                legalAdapter.notifyDataSetChanged();
            }
        });


        rv_multiple_owner.setAdapter(legalAdapter);
    }

    private void setCompanylAdapter() {

        legalCompanyAdapter = new LegalCompanyAdapter(getActivity(), legalModelsCompany, photosModels2, LegalStatusFragment.this, new LegalCompanyAdapter.CallBack() {
            @Override
            public void onPhotoClick(int pos) {
                checkRunTimePermission();
            }

            @Override
            public void onPhotoDelete(int pos) {
                photosModels2.remove(pos);
                legalCompanyAdapter.notifyDataSetChanged();
            }
        });

        rv_company.setAdapter(legalCompanyAdapter);
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {

            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_PHOTOS)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        JSONArray propertyPhotos = jsonObject_photo.getJSONArray("Result");

                        if (propertyPhotos.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < propertyPhotos.length(); index++) {
                                JSONObject jsonObject1 = propertyPhotos.getJSONObject(index);

                                PhotosModel photosModel = new PhotosModel();

                                photosModel.setId(jsonObject1.getString("Id"));
                                photosModel.setName(jsonObject1.getString("Name"));
                                photosModel.setImageUrl(jsonObject1.getString("Url"));
                                photosModel.setType(jsonObject1.getString("Type"));
                                if (radio_individual.isChecked()) {
                                    photosModels.add(photosModel);
                                } else if (radio_multiple_owner.isChecked()) {
                                    photosModel.setPos(MultipleOwnerPhotoPosition);
                                    photosModels1.add(photosModel);
                                } else if (company_owner_radio.isChecked()) {
                                    photosModels2.add(photosModel);
                                }

                                setLegalAdapter();

                            }
                            if (radio_individual.isChecked()) {
                                imageinflateview();
                            } else if (radio_multiple_owner.isChecked()) {

                                legalAdapter.notifyDataSetChanged();

//                                imageinflateview_1();
                            } else {
                                legalCompanyAdapter.notifyDataSetChanged();
//                                imageinflateview_2();
                            }

                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_REMOVE_PROPERTY_PHOTOS)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Constants.URL_PROPERTY_POST_DRAFT)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");

                            String message = jsonObject1.getString("Message");


                            Utils.showSuccessErrorMessage("Success", "Draft added successfully", "Ok", getActivity());

                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");

                            String message = jsonObject1.getString("Message");

                            if (PropertyTypeId.equals("1") || PropertyTypeId.equalsIgnoreCase("7") || PropertyTypeId.equalsIgnoreCase("8"))

                                SM.sendData(pid, 5);
                            else {
                                SM.sendData(pid, 7);
                            }

/*
                            if (jsonObject2.getJSONArray("PropertyOwnerShipProofs") != null) {

                                JSONArray propJsonArray = jsonObject2.getJSONArray("PropertyOwnerShipProofs");

                                for (int i = 0; i < propJsonArray.length(); i++) {

                                    propAreaModel = new PropAreaModel();
                                    JSONObject dataObj = propJsonArray.getJSONObject(i);

                                    propAreaModel.setId(dataObj.getInt("Id"));
                                    propAreaModel.setPlotShape(dataObj.getString("PlotShape"));
                                    propAreaModel.setPlotArea(dataObj.getInt("PlotArea"));
                                    propAreaModel.setPlotAreaUnitId(dataObj.getInt("PlotAreaUnitId"));
                                    propAreaModel.setFrontSize(dataObj.getString("FrontSize"));
                                    propAreaModel.setFrontSizeUnitId(dataObj.getInt("FrontSizeUnitId"));
                                    propAreaModel.setDepthSize(dataObj.getString("DepthSize"));
                                    propAreaModel.setDepthSizeUnitId(dataObj.getString("DepthSizeUnitId"));
                                    propAreaModel.setStreetOrRoadType(dataObj.getString("StreetOrRoadType"));
                                    propAreaModel.setRoadWidth(dataObj.getInt("RoadWidth"));
                                    propAreaModel.setRoadWidthUnitId(dataObj.getString("RoadWidthUnitId"));
                                    propAreaModel.setEnteranceDoorFacing(dataObj.getString("EnteranceDoorFacing"));
                                    propAreaModel.isDeletable(dataObj.getBoolean("IsDeletable"));
                                    propAreaModel.isModify(dataObj.getBoolean("IsModify"));

                                    propArrayList.add(propAreaModel);

                                }

                                propAreaModel.setPropOwnership(propArrayList);

                            }
*/

                            Toast.makeText(getActivity(), "Inventory updated successfully", Toast.LENGTH_SHORT).show();

                        } else {


                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void postPropertyImages() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_PHOTOS,
                photoInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_PHOTOS,
                Constants.POST).execute();

    }

    private String photoInput() {
        String inputStr = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("CompanyId", "1");
            jsonObject1.put("Name", new Date().getTime());
            jsonObject1.put("Type", "png");
            jsonObject1.put("ImageCode", encodedImageData);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {

        }
        inputStr = jsonArray.toString();
        return inputStr;
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(getActivity(), MyPropertyActivity.class);
            intent.putExtra("from_savedrafts", "from_savedrafts");
            startActivity(intent);
        }
    }

    int MultipleOwnerPhotoPosition = 0;

    public void onEvent(int Pos) {

        MultipleOwnerPhotoPosition = Pos;
        checkRunTimePermission();

    }

    public void imageinflateview_1() {
        image_view_inflate1.removeAllViews();

        for (int i = 0; i < photosModels1.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels1.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels1.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(photosModels1.get(id).getId());
                                    photosModels1.remove(id);
                                    imageinflateview_1();
                                }
                            })
                            .show();

                }
            });

            image_view_inflate1.addView(view);

        }

    }

    private void imageinflateview_2() {
        image_view_inflate2.removeAllViews();

        for (int i = 0; i < photosModels2.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels2.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels2.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(photosModels2.get(id).getId());
                                    photosModels2.remove(id);
                                    imageinflateview_2();
                                }
                            })
                            .show();

                }
            });

            image_view_inflate2.addView(view);

        }

    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity != null) {

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    private boolean validateEmail(String data) {
        Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher emailMatcher = emailPattern.matcher(data);
        return emailMatcher.matches();
    }


}
