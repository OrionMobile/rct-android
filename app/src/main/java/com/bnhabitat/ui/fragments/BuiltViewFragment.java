package com.bnhabitat.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.PropertyUnitsModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BuiltViewFragment extends Fragment implements TabChangeListener {
    View view;
    TextView tower_num, floor_num, cover_area, super_area, builtup_area, carpet_area, plc, enterence_door_facing, facing_road, tehsil, about_property, address;
    ArrayList<InventoryModel> property_data;

    public BuiltViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_built_view, container, false);
        onIntializeView();
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null)
                property_data = (ArrayList<InventoryModel>) bundle.getSerializable("property_data");
            tower_num.setText(property_data.get(0).getPropertyLocations().get(0).getTowerName());
            floor_num.setText(property_data.get(0).getPropertyLocations().get(0).getFloorNo());
            super_area.setText(property_data.get(0).getPropertyAreas().get(0).getSuperArea());
            builtup_area.setText(property_data.get(0).getPropertyAreas().get(0).getBuiltUpArea());
            carpet_area.setText(property_data.get(0).getPropertyAreas().get(0).getCarpetArea());
            enterence_door_facing.setText(property_data.get(0).getPropertyAreas().get(0).getEnteranceDoorFacing());
            tehsil.setText(property_data.get(0).getPropertyAreas().get(0).getEnteranceDoorFacing());
            facing_road.setText(property_data.get(0).getPropertyAreas().get(0).getStreetOrRoadType() + " " + property_data.get(0).getPropertyAreas().get(0).getRoadWidth());
        } catch (Exception e) {

        }

        return view;
    }

    public void onIntializeView() {
        tower_num = (TextView) view.findViewById(R.id.tower_num);
        floor_num = (TextView) view.findViewById(R.id.floor_num);
        cover_area = (TextView) view.findViewById(R.id.cover_area);
        super_area = (TextView) view.findViewById(R.id.super_area);
        builtup_area = (TextView) view.findViewById(R.id.builtup_area);
        carpet_area = (TextView) view.findViewById(R.id.carpet_area);
        plc = (TextView) view.findViewById(R.id.plc);
        enterence_door_facing = (TextView) view.findViewById(R.id.enterence_door_facing);
        facing_road = (TextView) view.findViewById(R.id.facing_road);
        tehsil = (TextView) view.findViewById(R.id.tehsil);
        about_property = (TextView) view.findViewById(R.id.about_property);
        address = (TextView) view.findViewById(R.id.address);
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
}
