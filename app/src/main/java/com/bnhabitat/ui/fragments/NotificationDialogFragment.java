package com.bnhabitat.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.data.sizeunit.UnitsTable;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.adapters.NotificationDialogAdapter;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Android on 5/17/2017.
 */

public class NotificationDialogFragment extends DialogFragment {

    private RecyclerView mRecyclerView;

    ArrayList<ProjectsDetailModel> projectListingModelArrayList = new ArrayList<>();

    private ArrayList<ProjectsDetailModel.PropertySize> propertySizes = new ArrayList();

    private ArrayList<ProjectsDetailModel.Towers> towerses = new ArrayList();
    ArrayList<Integer> towersarray = new ArrayList<>();
    long minSizeL = 100000000000000l;
    long maxSizeL = 0;
    int min, max;
    String startsFrom = "NA";
    private TextView no_of_car_parking, floor_per_tower, no_of_towers, project_size, date_of_pocession, status_of_property,
            price_range, size_of_property, configurations, location_of_property, type_of_property;
    private ArrayList<Integer> bedrooms = new ArrayList<>();
    int position;
    StringBuilder stringBuilder = new StringBuilder();
    String configuration = "";
    private String numberOfBedrooms = "";
    String unitName = "";
    String status;
    ProjectsDetailModel projectsDetailModel;
    public static NotificationDialogFragment newInstance() {
        return new NotificationDialogFragment();
    }

    // this method create view for your Dialog
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflate layout with recycler view
        View v = inflater.inflate(R.layout.notification_dialog_fragment, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.dialog_recycler_view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            projectListingModelArrayList = (ArrayList<ProjectsDetailModel>) bundle.getSerializable("Arraylist");
            propertySizes= (ArrayList<ProjectsDetailModel.PropertySize>) bundle.getSerializable("propertySizes");
            towerses= (ArrayList<ProjectsDetailModel.Towers>) bundle.getSerializable("towers");
            position = bundle.getInt("Position");
            status = bundle.getString("status");
            projectsDetailModel = (ProjectsDetailModel) bundle.getSerializable("projectModel");
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            //setadapter
            NotificationDialogAdapter adapter = new NotificationDialogAdapter(getActivity(), projectListingModelArrayList);

            mRecyclerView.setAdapter(adapter);
        }


        if(propertySizes==null){
            propertySizes = projectListingModelArrayList.get(position).getPropertySizes();
        }
        if(towerses==null){
            towerses = projectListingModelArrayList.get(position).getTowers();

        }



        Log.e("propertySize", String.valueOf(propertySizes.size()));

        for (int i = 0; i < propertySizes.size(); i++) {

            configuration = configuration + propertySizes.get(i).getTitle();
        }




        Log.e("propertySize", String.valueOf(propertySizes.size()));

        for (int i = 0; i < towerses.size(); i++) {

            towersarray.add(Integer.valueOf(towerses.get(i).getStories()));
        }


        no_of_car_parking = (TextView) v.findViewById(R.id.no_of_car_parking);
        floor_per_tower = (TextView) v.findViewById(R.id.floor_per_tower);
        no_of_towers = (TextView) v.findViewById(R.id.no_of_towers);
        project_size = (TextView) v.findViewById(R.id.project_size);
        date_of_pocession = (TextView) v.findViewById(R.id.date_of_pocession);
        status_of_property = (TextView) v.findViewById(R.id.status_of_property);
        price_range = (TextView) v.findViewById(R.id.price_range);
        size_of_property = (TextView) v.findViewById(R.id.size_of_property);
        configurations = (TextView) v.findViewById(R.id.configurations);
        location_of_property = (TextView) v.findViewById(R.id.location_of_property);
        type_of_property = (TextView) v.findViewById(R.id.type_of_property);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);



            location_of_property.setText(Utils.getFilteredValue(projectListingModelArrayList.get(position).getLocality()));




            type_of_property.setText(Utils.getFilteredValue(projectListingModelArrayList.get(position).getTypeOfProject()));




        if (propertySizes.size() != 0) {
            if (propertySizes.get(0).getMinimumPrice()==0) {
                price_range.setText("Range : N/A");
            } else {

                double maxPrice = Double.parseDouble(String.valueOf(propertySizes.get(0).getMaximumPrice()));
                double minPrice = Double.parseDouble(String.valueOf(propertySizes.get(0).getMinimumPrice()));
                for (ProjectsDetailModel.PropertySize propertySize : propertySizes) {
                    if (minPrice >= Double.parseDouble(String.valueOf(propertySize.getMinimumPrice()))) {
                        minPrice = Double.parseDouble(String.valueOf(propertySize.getMinimumPrice()));
                    }
                }
                price_range.setText(Utils.getConvertedPrice((long) minPrice, getActivity()) + " onwards");

            }
        } else {
            price_range.setText("Range : N/A");
        }


        for (int i = 0; i < propertySizes.size(); i++) {
            if (minSizeL > Double.parseDouble(propertySizes.get(i).getSize()))
                minSizeL = (long) Double.parseDouble(propertySizes.get(i).getSize());

            if (maxSizeL < Double.parseDouble(propertySizes.get(i).getSize()))
                maxSizeL = (long) Double.parseDouble(propertySizes.get(i).getSize());
            int sizeUnitId = propertySizes.get(i).getSizeUnit();

            unitName = UnitsTable.getInstance().getUnitName(sizeUnitId);
        }


        if (minSizeL != maxSizeL)
            startsFrom = minSizeL + "-" + maxSizeL;
        else
            startsFrom = String.valueOf(minSizeL);

        size_of_property.setText(startsFrom + " " + unitName);

        status_of_property.setText(Utils.getFilteredValue(projectListingModelArrayList.get(position).getProjectStatus()));

        if (projectListingModelArrayList.get(position).getPossessionMonth()==0 ) {
            date_of_pocession.setText("N/A");
        } else {
            date_of_pocession.setText(Utils.getMonthName(projectListingModelArrayList.get(position).getPossessionMonth()) +
                    projectListingModelArrayList.get(position).getPossessionYear());
        }


        if (projectListingModelArrayList.get(position).getArea().equalsIgnoreCase("")) {
            project_size.setText("N/A");
        } else {
//            String unitName = UnitsTable.getInstance().getUnitName(projectListingModelArrayList.get(position).getAreaUnit());

            String unitName = UnitsTable.getInstance().getUnitName(Integer.parseInt(projectListingModelArrayList.get(position).getAreaUnitId()));
            project_size.setText(projectListingModelArrayList.get(position).getArea() + " " + unitName);
        }

        if (projectListingModelArrayList.get(position).getNumberOfTowers().equalsIgnoreCase("")
                || projectListingModelArrayList.get(position).getNumberOfTowers().equalsIgnoreCase("0") ||
                projectListingModelArrayList.get(position).getNumberOfTowers() == null) {
            no_of_towers.setText("N/A");
        } else {
            no_of_towers.setText(String.valueOf(projectListingModelArrayList.get(position).getNumberOfTowers())+" Towers");
        }


        if (projectListingModelArrayList.get(position).getNumberOfParking()==0) {
            no_of_car_parking.setText("N/A");
        } else {
            no_of_car_parking.setText(String.valueOf(projectListingModelArrayList.get(position).getNumberOfParking()));
        }


        for (ProjectsDetailModel.PropertySize projectsModel : propertySizes) {
            int bedroom = Integer.parseInt(String.valueOf(projectsModel.getBedRooms()));
            if (bedroom != 0) {
                if (!numberOfBedrooms.contains(String.valueOf(projectsModel.getBedRooms())))
                    numberOfBedrooms = numberOfBedrooms + projectsModel.getBedRooms() + ", ";

                if (!bedrooms.contains(bedroom)) {
                    bedrooms.add(bedroom);
                }

            }
        }

        if (!numberOfBedrooms.equalsIgnoreCase("")) {

            StringBuilder bhk = new StringBuilder();

            Collections.sort(bedrooms);

            for (Integer room : bedrooms) {
                bhk.append(room + ",");
            }

            String bhks = bhk.toString();
            bhks = bhks.substring(0, bhks.length() - 1);
            configurations.setText(bhks + " BHK");


        } else {

            if (propertySizes.size() > 0) {
                configurations.setText(propertySizes.get(0).getTitle());
            }
        }
        if (!towerses.isEmpty())
            floor_per_tower.setText(String.valueOf(towerses.size()));


//        configurations.setText(configuration);
        Log.e("propertySize", configuration);


        //setadapter

        //get your recycler view and populate it.
        return v;
    }
}
