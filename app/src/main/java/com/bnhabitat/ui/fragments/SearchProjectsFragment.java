package com.bnhabitat.ui.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.ui.adapters.AdapterPropertyType;
import com.bnhabitat.ui.adapters.DemoRangeAdapter;
import com.bnhabitat.ui.views.CustomRangeSeekBar;
import com.bnhabitat.ui.views.ExpandableHeightGridView;

import java.util.ArrayList;

/**
 * Created by Android on 5/23/2017.
 */
public class SearchProjectsFragment extends Fragment implements AdapterPropertyType.OnPropertySizesSelected {

    private ArrayList<CommonDialogModel> categoryModel = new ArrayList<CommonDialogModel>();
    private ArrayList<TextView> categoriesTextVwList = new ArrayList<>();
    private ArrayList<LinearLayout> lineLayoutList = new ArrayList<>();
    private ArrayList<CommonDialogModel> categoryTypeModel = new ArrayList<CommonDialogModel>();
    private ExpandableHeightGridView propertyTypeGridVw;
    private LinearLayout propertyTypeslayout;
    private LinearLayout categoriesLayout;
    private int minPriceIntent = 500000;
    private int maxPriceIntent = 50000000;
    private double minBValue = 0.0;
    private double maxBValue = 100.0;
    private String minPrice = "500000";
    private String maxPrice = "50000000";
    private TextView minSlideTxt;
    private TextView maxSlideTxt;
    private Button residential, commercial, industrial, institutional, agricultural;
    private boolean residentialIsOpened ;
    private boolean commercialIsOpened ;
    private boolean industrialIsOpened;
    private boolean institutionalIsOpened;
    private boolean agriculturalIsOpened;

    public SearchProjectsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search_projects, container, false);
        propertyTypeslayout = (LinearLayout) v.findViewById(R.id.propertyTypeslayout);
        propertyTypeGridVw = (ExpandableHeightGridView) v.findViewById(R.id.propertyTypeGridVw);
        minSlideTxt = (TextView) v.findViewById(R.id.minSlideTxt);
        maxSlideTxt = (TextView) v.findViewById(R.id.maxSlideTxt);
        residential = (Button) v.findViewById(R.id.residential);
        commercial = (Button) v.findViewById(R.id.commercial);
        industrial = (Button) v.findViewById(R.id.industrial);
        institutional = (Button) v.findViewById(R.id.institutional);
        agricultural = (Button) v.findViewById(R.id.agricultural);
        residential.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (residentialIsOpened) {
                    removeAllColors();
                    residential.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
                    residentialIsOpened = false;
                } else {
                    residential.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
                    residentialIsOpened = true;
                }
            }
        });

        commercial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (commercialIsOpened) {
                    removeAllColors();
                    commercial.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
                    commercialIsOpened = false;
                } else {
                    commercial.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
                    commercialIsOpened = true;
                }
            }
        } );
        industrial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)

         {

            if (industrialIsOpened) {
                removeAllColors();
                industrial.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
                industrialIsOpened = false;
            } else {
                industrial.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
                industrialIsOpened = true;
            }
        }});
        agricultural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (agriculturalIsOpened) {
                    removeAllColors();
                    agricultural.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
                    agriculturalIsOpened = false;
                } else {
                    agricultural.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
                    agriculturalIsOpened = true;
                }

            }
        });

        institutional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (institutionalIsOpened) {
                    removeAllColors();
                    institutional.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));
                    institutionalIsOpened = false;
                } else {
                    institutional.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
                    institutionalIsOpened = true;
                }
            }


        });
        CustomRangeSeekBar budgetRangeSeekBar = (CustomRangeSeekBar) v.findViewById(R.id.budgetRangeSeekBar);
        budgetRangeSeekBar.setNotifyWhileDragging(true);
        budgetRangeSeekBar.setAdapter(new DemoRangeAdapter());
        budgetRangeSeekBar.setSelectedMinValue(minPriceIntent / 500000);
        budgetRangeSeekBar.setSelectedMaxValue(maxPriceIntent / 500000);
        budgetRangeSeekBar.setOnRangeSeekBarChangeListener(new CustomRangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(CustomRangeSeekBar bar, int minPosition, int maxPosition) {
                minBValue = minPosition;
                maxBValue = maxPosition;

                Log.e("Values", "minBValue" + minBValue + " maxBValue" + maxBValue);
                int maxValueMultiply = 500000;

                if (minPosition == maxPosition)
                    maxValueMultiply = 600000;


                long maxValue = (long) maxBValue * maxValueMultiply;
                long minValue = (long) minBValue * 500000;

                minPrice = String.valueOf(minValue);
                maxPrice = String.valueOf(maxValue);
                minSlideTxt.setText(minPrice);
                maxSlideTxt.setText(maxPrice);
            }
        });
        CommonDialogModel commonDialogModel = new CommonDialogModel();
        commonDialogModel.setTitle("Plot");
        categoryModel.add(commonDialogModel);
        categoryTypeModel.add(commonDialogModel);
        CommonDialogModel commonDialogModel1 = new CommonDialogModel();
        commonDialogModel1.setTitle("Plot");
        categoryModel.add(commonDialogModel1);
        categoryTypeModel.add(commonDialogModel1);
        CommonDialogModel commonDialogModel2 = new CommonDialogModel();
        commonDialogModel2.setTitle("Plot");
        categoryModel.add(commonDialogModel2);
        categoryTypeModel.add(commonDialogModel2);
        setcategoryTypeLayout(0);
//        setCategoryLayout();
        return v;
    }

     private void removeAllColors(){
         institutional.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
         agricultural.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
         commercial.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
         residential.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
         industrial.setBackgroundColor(getActivity().getResources().getColor(R.color.light_green));
     }
    private void setCategoryLayout() {

//        categoriesLayout.removeAllViews();
        categoriesTextVwList.clear();
        lineLayoutList.clear();

        for (int index = 0; index < categoryModel.size(); index++) {

            View view;
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.category_items, null);
            final LinearLayout parentlayout = (LinearLayout) view.findViewById(R.id.parentlayout);
            TextView categoryTxtVw = (TextView) view.findViewById(R.id.categoryTxtVw);
            categoryTxtVw.setText(categoryModel.get(index).getTitle());

            if (index == 0) {
                parentlayout.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));


            }

            parentlayout.setTag(index);
            parentlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int position = (Integer) view.getTag();
                    setcategoryTypeLayout(position);

                    int index = 0;
                    for (CommonDialogModel categoryTypeModel : categoryModel) {

                        categoriesTextVwList.get(index).setTextColor(getResources().getColor(R.color.white));


                        if (index == position) {
                            categoriesTextVwList.get(index).setTextColor(getResources().getColor(R.color.white));
                            parentlayout.setBackgroundColor(getActivity().getResources().getColor(R.color.blue));

                        }

                        index++;
                    }


                }
            });

            categoriesTextVwList.add(categoryTxtVw);


            categoriesLayout.addView(view);
        }

        setcategoryTypeLayout(0);

    }

    private void setcategoryTypeLayout(int Position) {


//        categoryTypeModel = CategoryType.getInstance().getPropertyType(categoryModel.get(Position).getId());

        propertyTypeslayout.setVisibility(View.VISIBLE);
        propertyTypeGridVw.setExpanded(true);
        AdapterPropertyType adapter = new AdapterPropertyType(getActivity(), categoryTypeModel, this);
        propertyTypeGridVw.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPropertySizeSelected(ArrayList<String> ids) {

    }

    @Override
    public void onPropertyTextSelected(String name) {

    }
}
