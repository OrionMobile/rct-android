package com.bnhabitat.ui.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.data.category.CategoryType;
import com.bnhabitat.data.category.CategoryTypeModel;
import com.bnhabitat.data.sizeunit.SizeUnitTable;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ComponentModel;
import com.bnhabitat.models.PaymentPlanCalcModel;
import com.bnhabitat.models.PaymentPlanModel;
import com.bnhabitat.models.PricingModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.ui.activities.CalculateEmiActivity;
import com.bnhabitat.ui.activities.ProjectDetailActivity;
import com.bnhabitat.ui.adapters.AdapterCommonDialog;
import com.bnhabitat.ui.adapters.PricePaymentAdapter;
import com.bnhabitat.ui.adapters.ProjectLenderAdapter;
import com.bnhabitat.ui.adapters.UnitVarientAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PricingUnitFragment extends Fragment implements TabChangeListener {
    LinearLayout view_assured_return, view_price_paymentLayout, view_unit_varient,offer_view,finance_view;
    RelativeLayout unit_varientLayout;
    private  TextView price_paymentLayout,offers_layout,offers,finance_layout;
    int count_assured = 0;
    int count_tower = 0;
    int count_price = 0;
    public static boolean IS_VIEW_INFLATED = false;
    int count_unit_varient = 0;
    private int SELECTED_SIZE = 0;
    private TextView planTitleTxt;
    private TextView disclaimer;
    private int size = 0;
    private int position1 = 0;
    private boolean isDiscountCalculated = false;
    private long totalDiscountValue = 0;
    private ImageView rightIcon;



    Dialog dialog;
    StringBuilder serviceTxBuilder;
    private int sizeUnit;
    private TextView unitTxt;
    private LinearLayout unitsLayout;
    LinearLayout background_color;
    private RelativeLayout Tower_detail_layout;
    int sizeUnitValue = 0;
    double bspUnitPrice = 0;
    double bspTotalPrice = 0;
    double areaUnitPrice = 0;
    long totalAmount = 0;
    private double superAreaValue = 0;
    private double carpetAreaValue = 0;
    private double extraAreaValue = 0;
    long bspCalulatedPrice = 0;
    int discountedUnit = 0;
    long discountedPrice = 0;
    int position;
    private DecimalFormat sizeFormat = new DecimalFormat("#####0.00");
    private TextView unitTxtVw1;
    private TextView unitTxtVw2;
    private TextView unitTxtVw3;
    private TextView unitTxtVw4;
    private TextView unitTxtVw5;
    private TextView unitTxtVw6;

    private TextView totalPaymentPlanTxtVw;
    private TextView discountUnitTxtVw;
    private TextView serviceTaxTxtVw;
    private TextView amountInWordTxtVw;
    private TextView effectiveUnitTxtVw;
    private TextView effectivePriceTxtVw;
    private TextView totalPriceTxtVw;
    private TextView discountPriceTxtVw;
    private LinearLayout realPriceLayout;
    private TextView carpetAreaSizeTxtVw;
    private TextView builtAreaSizeTxtVw;
    private TextView carpetAreaTitleTxtVw;
    private TextView builtUpAreaTitleTxtVw;
    private TextView calculationBasedTxtvw;
    private TextView realPriceTxtVw;
    private TextView superAreaTitleTxtVw;
    private TextView discountedPriceTxtVw;
    private TextView serviceTaxTitleTxtVw;
    private LinearLayout discountPriceLayout;
    private LinearLayout totalDiscountLayout;
    private TextView planSelectedTxt;
    private CardView discountCardView;

    private CardView particular_card;
    private TextView unitPriceTxtVw;
    private TextView superAreaTxtVw;
    private TextView carpetAreaTxtVw;
    private TextView builtUpAreaTxtVw;
    private TextView superAreaSizeTxtVw;
    private LinearLayout discountLayout;
    private LinearLayout towerParentLayout;
    private LinearLayout towerLayout;
    private LinearLayout paymentPlantMasterLayout;
    private LinearLayout paymentPlanLayout;
    private LinearLayout planLayout;
    private LinearLayout layoutPlanParentLayout;
    private boolean isDataSet = false;
    private String numberOfBedrooms = "";
    ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList<>();
    private ArrayList<Integer> bedrooms = new ArrayList<>();
    private ArrayList<Double> serviceCalTaxList = new ArrayList<>();
    private ArrayList<Double> serviceTaxList = new ArrayList<>();
    private ArrayList<Integer> componentIdList = new ArrayList<>();
    private ArrayList<Integer> subCompIdList = new ArrayList<>();
    private ArrayList<String> componentNameList = new ArrayList<>();
    private ArrayList<String> subCompNameList = new ArrayList<>();
    private ArrayList<TextView> compNameTextView = new ArrayList<>();
    private ArrayList<TextView> titleComponentsView = new ArrayList<>();
    private LinearLayout disValuesLayout;
    private double realPrice = 0.0;
    private ViewPager layoutPlanPager;
    double sum = 0;
    private ArrayList<PaymentPlanCalcModel> paymentPlanCalcModels = new ArrayList<>();
    private ArrayList<ComponentModel> componentModels = new ArrayList<>();
    private ArrayList<String> parkingComponents = new ArrayList<>();
    private HashMap<String, ArrayList<CommonDialogModel>> arrayListParkingHashMap = new HashMap<>();
    //    private CardView priceCardView;
    private CardView priceCalcCardView, disclaimer_Card;
    private ArrayList<Long> pricingList = new ArrayList<>();
    private ArrayList<Long> pricingListTemp = new ArrayList<>();
    private LinearLayout pricingLayout;
    private LinearLayout unitVariantLayout;
    private ArrayList<PricingModel> pricingModels = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.PropertySize> propertySizesModels = new ArrayList<>();
    private ArrayList<PaymentPlanModel> paymentPlanModels = new ArrayList<>();
    private ArrayList<CommonDialogModel> floorPlc = new ArrayList<>();
    private ArrayList<CommonDialogModel> plc = new ArrayList<>();
    private ArrayList<CommonDialogModel> bsps = new ArrayList<>();
    private ArrayList<CommonDialogModel> propertyTypeModel = new ArrayList<>();
    private ArrayList<CommonDialogModel> propertyTypeModelOriginal = new ArrayList<>();
    private ArrayList<CommonDialogModel> paymentTypeModel = new ArrayList<>();
    private ArrayList<CommonDialogModel> powerBackupModel = new ArrayList<>();
    private ArrayList<CommonDialogModel> towersModel = new ArrayList<>();
    private ArrayList<CommonDialogModel> coveredParkModel = new ArrayList<>();

    private ArrayList<CommonDialogModel> additionalParkModel = new ArrayList<>();
    private ArrayList<CommonDialogModel> currencyModel = new ArrayList<>();
    ImageView assured_returnDownArrow, unit_varientArrow,tax_show;
    RecyclerView unit_varient_list, price_payment_list;
    LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    ImageView info_icon, share_icon, notification_icon, user_icon, price_pay_diaolg, view_paymentplan, back_price,choose_another,calculate_btn;
    private int plcAmount;
    private LinearLayout planSelectedLayout;
    private int flcAmount;
    private int bspServiceTax;
    private int otherComponentsTax;
    private int edcAmount = 0;
    private String userId = "";
    private String baspServiceTax = "";
    private int SELECTED_SIZE_POSITION = 0;
    private DecimalFormat form = new DecimalFormat("##,##,##,##0");
    private DecimalFormat decimalForm = new DecimalFormat("##.##");
    private View view;
    String originalUnitName = "";
    ArrayList<ProjectsDetailModel.PropertySize.LayoutPlans> layoutPlanses = new ArrayList<>();
    private boolean isAppliedOnCarpetArea = false;
    private ArrayList<TextView> pricingTextView = new ArrayList<>();
    private String selectedPropertyId = "";
    ProjectsDetailModel projectsDetailModel;
    ArrayList<ProjectsModel> projectListingModelArrayList = new ArrayList<>();
    private CardView layoutPlanCardView;

    String unitName = "";
    String plotunitName = "";

    boolean isPlcAdded = false;
    boolean isFloorPlcAdded = false;
    boolean isBspAdded = false;
    public ArrayList<Boolean> checkedItemsListed = new ArrayList<>();
    AdapterCommonDialog adapterCommonDialog;
    private TextView towerTxt;
    GridLayoutManager gridLayoutManager;
    private final int COLUMN_NUMBERS = 3;
    RecyclerView finance_list;
    TextView selected_unitvalue,no_lenders_avail;
    TextView effctive_unittxt;
    TextView effective_pricetxt;
    ProjectLenderAdapter projectLenderAdapter;

    private ArrayList<ProjectsDetailModel.ProjectLenders> projectLendersesModel = new ArrayList<>();
    public PricingUnitFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_pricing_unit, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null)
            projectsDetailModel = (ProjectsDetailModel) bundle.getSerializable("projectModel");
        position = bundle.getInt("position");
        Log.e("position", String.valueOf(position));
        projectListingModelArrayList = (ArrayList<ProjectsModel>) bundle.getSerializable("notification_array");
        view_assured_return = (LinearLayout) view.findViewById(R.id.view_assured_return);
        view_unit_varient = (LinearLayout) view.findViewById(R.id.view_unit_varient);
        unitVariantLayout = (LinearLayout) view.findViewById(R.id.unitVariantLayout);

        view_price_paymentLayout = (LinearLayout) view.findViewById(R.id.view_price_paymentLayout);
        finance_view = (LinearLayout) view.findViewById(R.id.finance_view);
        offer_view = (LinearLayout) view.findViewById(R.id.offer_view);
//        parentLayout = (RelativeLayout) view.findViewById(R.id.parentLayout);

        price_paymentLayout = (TextView) view.findViewById(R.id.price_paymentLayout);
//        Tower_detail_layout = (RelativeLayout) view.findViewById(R.id.Tower_detail_layout);
        towerParentLayout = (LinearLayout) view.findViewById(R.id.towerParentLayout);
        towerLayout = (LinearLayout) view.findViewById(R.id.towerLayout);
        planLayout = (LinearLayout) view.findViewById(R.id.planLayout);


        planSelectedLayout = (LinearLayout) view.findViewById(R.id.planSelectedLayout);
        paymentPlantMasterLayout = (LinearLayout) view.findViewById(R.id.paymentPlantMasterLayout);
        paymentPlanLayout = (LinearLayout) view.findViewById(R.id.paymentPlanLayout);
        unit_varientLayout = (RelativeLayout) view.findViewById(R.id.unit_varientLayout);


        unit_varientArrow = (ImageView) view.findViewById(R.id.unit_varientArrow);

        price_pay_diaolg = (ImageView) view.findViewById(R.id.price_pay_diaolg);
        pricingLayout = (LinearLayout) view.findViewById(R.id.pricingLayout);
        serviceTaxTxtVw = (TextView) view.findViewById(R.id.serviceTaxTxtVw);
        planSelectedTxt = (TextView) view.findViewById(R.id.planSelectedTxt);
        disclaimer = (TextView) view.findViewById(R.id.disclaimer);
        finance_layout = (TextView) view.findViewById(R.id.finance_layout);

        view_paymentplan = (ImageView) view.findViewById(R.id.view_paymentplan);
        back_price = (ImageView) view.findViewById(R.id.back_price);
        tax_show = (ImageView) view.findViewById(R.id.tax_show);
        realPriceLayout = (LinearLayout) view.findViewById(R.id.realPriceLayout);
        realPriceTxtVw = (TextView) view.findViewById(R.id.realPriceTxtVw);

        unitTxt = (TextView) view.findViewById(R.id.unitTxt);
        totalPaymentPlanTxtVw = (TextView) view.findViewById(R.id.totalPaymentPlanTxtVw);
        towerTxt = (TextView) view.findViewById(R.id.towerTxt);
//        planSelectedTxt = (TextView) view.findViewById(R.id.planSelectedTxt);
        disValuesLayout = (LinearLayout) view.findViewById(R.id.disValuesLayout);
        unitsLayout = (LinearLayout) view.findViewById(R.id.unitsLayout);
        builtUpAreaTxtVw = (TextView) view.findViewById(R.id.builtUpAreaTxtVw);
        discountUnitTxtVw = (TextView) view.findViewById(R.id.discountUnitTxtVw);
        effectiveUnitTxtVw = (TextView) view.findViewById(R.id.effectiveUnitTxtVw);
        effectivePriceTxtVw = (TextView) view.findViewById(R.id.effectivePriceTxtVw);
        discountPriceTxtVw = (TextView) view.findViewById(R.id.discountPriceTxtVw);
        carpetAreaTitleTxtVw = (TextView) view.findViewById(R.id.carpetAreaTitleTxtVw);
        builtUpAreaTitleTxtVw = (TextView) view.findViewById(R.id.builtUpAreaTitleTxtVw);
        superAreaTitleTxtVw = (TextView) view.findViewById(R.id.superAreaTitleTxtVw);
        serviceTaxTitleTxtVw = (TextView) view.findViewById(R.id.serviceTaxTitleTxtVw);
        calculationBasedTxtvw = (TextView) view.findViewById(R.id.calculationBasedTxtvw);
        discountedPriceTxtVw = (TextView) view.findViewById(R.id.discountedPriceTxtVw);
        unitPriceTxtVw = (TextView) view.findViewById(R.id.unitPriceTxtVw);
        superAreaTxtVw = (TextView) view.findViewById(R.id.superAreaTxtVw);
        carpetAreaTxtVw = (TextView) view.findViewById(R.id.carpetAreaTxtVw);
        discountCardView = (CardView) view.findViewById(R.id.discountCardView);
        particular_card = (CardView) view.findViewById(R.id.particular_card);
        disclaimer_Card = (CardView) view.findViewById(R.id.disclaimer_Card);


        unitTxtVw1 = (TextView) view.findViewById(R.id.unitTxtVw1);
        unitTxtVw2 = (TextView) view.findViewById(R.id.unitTxtVw2);
        unitTxtVw3 = (TextView) view.findViewById(R.id.unitTxtVw3);
        unitTxtVw4 = (TextView) view.findViewById(R.id.unitTxtVw4);
        unitTxtVw5 = (TextView) view.findViewById(R.id.unitTxtVw5);
        unitTxtVw6 = (TextView) view.findViewById(R.id.unitTxtVw6);
        offers = (TextView) view.findViewById(R.id.offers);
        offers_layout = (TextView) view.findViewById(R.id.offers_layout);
        carpetAreaSizeTxtVw = (TextView) view.findViewById(R.id.carpetAreaSizeTxtVw);
        builtAreaSizeTxtVw = (TextView) view.findViewById(R.id.builtAreaSizeTxtVw);
        superAreaSizeTxtVw = (TextView) view.findViewById(R.id.superAreaSizeTxtVw);
        discountPriceLayout = (LinearLayout) view.findViewById(R.id.discountPriceLayout);
        totalDiscountLayout = (LinearLayout) view.findViewById(R.id.totalDiscountLayout);
        discountLayout = (LinearLayout) view.findViewById(R.id.discountLayout);
        amountInWordTxtVw = (TextView) view.findViewById(R.id.amountInWordTxtVw);
        totalPriceTxtVw = (TextView) view.findViewById(R.id.totalPriceTxtVw);
//
        info_icon = (ImageView) view.findViewById(R.id.info_icon);
        share_icon = (ImageView) view.findViewById(R.id.share_icon);
        priceCalcCardView = (CardView) view.findViewById(R.id.priceCalcCardView);
        pricingLayout = (LinearLayout) view.findViewById(R.id.pricingLayout);
        notification_icon = (ImageView) view.findViewById(R.id.notification_icon);
        user_icon = (ImageView) view.findViewById(R.id.user_icon);
        selected_unitvalue=(TextView) view.findViewById(R.id.selected_unitvalue);
        effctive_unittxt=(TextView) view.findViewById(R.id.effctive_unittxt);
        effective_pricetxt=(TextView) view.findViewById(R.id.effective_pricetxt);
        no_lenders_avail=(TextView) view.findViewById(R.id.no_lenders_avail);
        calculate_btn=(ImageView)view.findViewById(R.id.calculate_btn);
        choose_another=(ImageView)view.findViewById(R.id.choose_another);
        finance_list=(RecyclerView)view.findViewById(R.id.finance_list);
        gridLayoutManager = new GridLayoutManager(getActivity(), COLUMN_NUMBERS);
        finance_list.setHasFixedSize(true);
        finance_list.setLayoutManager(gridLayoutManager);
        calculate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), CalculateEmiActivity.class);

                startActivity(intent);

            }
        });
        choose_another.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                view_price_paymentLayout.setVisibility(View.VISIBLE);
                finance_view.setVisibility(View.GONE);

                offer_view.setVisibility(View.GONE);
                offers_layout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                price_paymentLayout.setBackgroundColor(getResources().getColor(R.color.blue));
                finance_layout.setBackgroundColor(getResources().getColor(R.color.light_blue));
            }
        });

        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                projectRelationshipManagersArrayList = projectsDetailModel.getProjectRelationshipManagers();
                Log.e("project_array", String.valueOf(projectRelationshipManagersArrayList.size()));
                if (!projectRelationshipManagersArrayList.isEmpty()) {
                    CustomDialogFragment fragment = CustomDialogFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Arraylist", projectRelationshipManagersArrayList);
                    fragment.setArguments(bundle);

                    fragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "dialog_fragment");
                } else {
                    Toast.makeText(getActivity(), "No RM associated with the project.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationDialogProjectDetail notificationDialogFragment = NotificationDialogProjectDetail.newInstance();

                Bundle bundle = new Bundle();
//                bundle.putSerializable("Arraylist", projectListingModelArrayList);
//                bundle.putSerializable("propertySizes", propertySizes);
//                bundle.putSerializable("towers", projectsDetailModel.getTowers());
                bundle.putSerializable("projectModel", projectsDetailModel);
//                bundle.putSerializable("status", "1");
//                bundle.putInt("Position", position);
                notificationDialogFragment.setArguments(bundle);

                notificationDialogFragment.show(((ProjectDetailActivity) getActivity()).getSupportFragmentManager(), "notification_dialog_fragment");

            }
        });

        share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, projectsDetailModel.getTitle());

                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://bnhabitat.com/?"+"p="+projectsDetailModel.getId());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
        info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(projectsDetailModel.getCurrentOffers().equalsIgnoreCase("null")||projectsDetailModel.getCurrentOffers().equalsIgnoreCase("")||projectsDetailModel.getCurrentOffers()==null){
                    Toast.makeText(getActivity(), "No offers yet", Toast.LENGTH_SHORT).show();
                }
                else{
                    new AlertDialog.Builder(getActivity())
                            .setMessage(projectsDetailModel.getCurrentOffers())
                            .setPositiveButton("NOTED", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();
                }
            }
        });
        if(projectsDetailModel.getCurrentOffers().equalsIgnoreCase("null")||projectsDetailModel.getCurrentOffers().equalsIgnoreCase("")||projectsDetailModel.getCurrentOffers()==null){
            offers.setText("No offers yet");

        }
        else{
            offers.setText(projectsDetailModel.getCurrentOffers());

        }

        price_pay_diaolg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPaydiaoluge();
            }
        });

//        Tower_detail_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (count_tower == 0) {
//                    tower_view.setVisibility(View.VISIBLE);
//                    towerdetail_Arrow.setImageResource(R.drawable.down_arrow);
//                    count_tower++;
//                } else {
//                    count_tower = 0;
//                    tower_view.setVisibility(View.GONE);
//                    towerdetail_Arrow.setImageResource(R.drawable.right_arrow);
//                }
//
//            }
//        });
        towerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCommonDialog(getActivity(), towersModel, 10, 0);
            }
        });
        offers_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
                view_price_paymentLayout.setVisibility(View.GONE);
                offer_view.setVisibility(View.VISIBLE);
                finance_view.setVisibility(View.GONE);
                offers_layout.setBackgroundColor(getResources().getColor(R.color.blue));
                price_paymentLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                finance_layout.setBackgroundColor(getResources().getColor(R.color.light_blue));



            }
        });
        back_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                priceCalcCardView.setVisibility(View.VISIBLE);
                view_paymentplan.setVisibility(View.VISIBLE);
                particular_card.setVisibility(View.VISIBLE);
                paymentPlantMasterLayout.setVisibility(View.GONE);
                paymentPlanLayout.setVisibility(View.GONE);
                back_price.setVisibility(View.GONE);

            }
        });
        unitsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCommonDialog(getActivity(), propertyTypeModel, 0, 0);
            }
        });
        planLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPlanModel();
                if (paymentTypeModel.size() > 0)
                    showCommonDialog(getActivity(), paymentTypeModel, 1, 0);
                else
                    Toast.makeText(getActivity(), "No payment plan is available for this size", Toast.LENGTH_SHORT).show();
//                    Snackbar.make(parentLayout, "", Snackbar.LENGTH_LONG).show();

            }
        });
        price_paymentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    view_price_paymentLayout.setVisibility(View.VISIBLE);
                finance_view.setVisibility(View.GONE);

                offer_view.setVisibility(View.GONE);
                offers_layout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                price_paymentLayout.setBackgroundColor(getResources().getColor(R.color.blue));
                finance_layout.setBackgroundColor(getResources().getColor(R.color.light_blue));


            }
        });
        finance_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    view_price_paymentLayout.setVisibility(View.GONE);
                finance_view.setVisibility(View.VISIBLE);

                offer_view.setVisibility(View.GONE);
                offers_layout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                price_paymentLayout.setBackgroundColor(getResources().getColor(R.color.light_blue));
                finance_layout.setBackgroundColor(getResources().getColor(R.color.blue));


            }
        });
        setLogosLayout();
        unit_varientLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count_unit_varient == 0) {
                    view_unit_varient.setVisibility(View.VISIBLE);
                    unit_varientArrow.setImageResource(R.drawable.down_arrow);
                    count_unit_varient++;
                } else {
                    count_unit_varient = 0;
                    view_unit_varient.setVisibility(View.GONE);
                    unit_varientArrow.setImageResource(R.drawable.right_arrow);
                }

            }
        });
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager1 = new LinearLayoutManager(getActivity());
        unit_varient_list = (RecyclerView) view.findViewById(R.id.unit_varient_list);
        price_payment_list = (RecyclerView) view.findViewById(R.id.price_payment_list);
        UnitVarientAdapter unitVarientAdapter = new UnitVarientAdapter(getActivity());
        PricePaymentAdapter pricePaymentAdapter = new PricePaymentAdapter(getActivity());
        unit_varient_list.setLayoutManager(linearLayoutManager);
        price_payment_list.setLayoutManager(linearLayoutManager1);
        unit_varient_list.setAdapter(unitVarientAdapter);
        price_payment_list.setAdapter(pricePaymentAdapter);

//        if (projectsDetailModel.getTowers().isEmpty()) {
//            Tower_detail_layout.setVisibility(View.GONE);
//        } else {
//            tower_detail_pager.setAdapter(new TowerPagerAdapter(getActivity(), projectsDetailModel.getTowers()));
//        }
        setData();


        setUnitVariantLayout();
//        pricingModels = projectsDetailModel.getPricingModels();
//        paymentPlanModels = projectsDetailModel.getPaymentPlanModels();
        setPricingLayout(SELECTED_SIZE, 0);

        return view;
    }

    private void showPaydiaoluge() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//               dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.price_pay_dialog);
//               dialog.setCancelable(false);

        dialog.show();

//       }
//    public void cancelCustomDialog() {
//        try {
//            if (null != dialog && dialog.isShowing()) {
//                dialog.cancel();
//                dialog = null;
//            }
//        } catch (Exception e) {
//            dialog = null;
//            e.printStackTrace();
//        }
    }

    ArrayList<ImageView> discountRadioBtns = new ArrayList<>();
    ArrayList<Boolean> isDiscountChecked = new ArrayList<>();

    private void setDiscountLayout(final int size) {
        isDiscountChecked.clear();
        discountRadioBtns.clear();
        disValuesLayout.removeAllViews();
        ArrayList<ProjectsDetailModel.PropertySize.Discounts> discounts = new ArrayList<>();

        if (projectsDetailModel.getPropertySizes().size() > size) {

            discounts = projectsDetailModel.getPropertySizes().get(size).getDiscounts();

            for (int index = 0; index < discounts.size(); index++) {


                View view;
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.discount_items, null);
                LinearLayout discountRadioBtn = (LinearLayout) view.findViewById(R.id.discountRadioBtn);
                ImageView discountRadioImg = (ImageView) view.findViewById(R.id.discountRadioImg);
                TextView discountRadioTitle = (TextView) view.findViewById(R.id.discountRadioTitle);

                String date = "";

                if (!discounts.get(index).getEndDate().equalsIgnoreCase(""))
                    date = " (valid till : " + discounts.get(index).getEndDate() + ")";


                discountRadioTitle.setText(discounts.get(index).getDiscountName() + date);
                disValuesLayout.addView(view);

                isDiscountChecked.add(false);
                discountRadioBtns.add(discountRadioImg);

                discountRadioBtn.setTag(index);
                discountRadioBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int position = (Integer) view.getTag();

                        if (isDiscountChecked.get(position)) {
                            discountRadioBtns.get(position).setImageResource(R.drawable.radio);
                            isDiscountChecked.set(position, false);
                            setDiscountApplied(0, 0, false);
                        } else {
                            discountRadioBtns.get(position).setImageResource(R.drawable.radio_check);
                            isDiscountChecked.set(position, true);
                            setDiscountApplied(size, position, true);
                        }

                        for (int index = 0; index < discountRadioBtns.size(); index++) {
                            if (index != position) {
                                discountRadioBtns.get(index).setImageResource(R.drawable.radio);
                                isDiscountChecked.set(index, false);
                            }
                        }
                    }
                });
            }
        }

        if (discounts.size() == 0) {
            discountLayout.setVisibility(View.GONE);
            discountCardView.setVisibility(View.GONE);
        } else {
            discountLayout.setVisibility(View.VISIBLE);
            discountCardView.setVisibility(View.VISIBLE);
        }
    }

    private void setDiscountApplied(int size, int position, boolean isDiscountCalculated) {

        this.size = size;
        this.position = position;
        this.isDiscountCalculated = isDiscountCalculated;

        if (isDiscountCalculated) {
            realPriceLayout.setVisibility(View.VISIBLE);
            discountPriceLayout.setVisibility(View.VISIBLE);
            totalDiscountLayout.setVisibility(View.VISIBLE);
            ProjectsDetailModel.PropertySize.Discounts discounts = projectsDetailModel.getPropertySizes().get(size).getDiscounts().get(position);

            long discountValue = Integer.valueOf(discounts.getDiscountValue());

            if (discounts.getPaymentOption().equalsIgnoreCase("Lumsum")) {


                long discountedBSP = pricingList.get(0) - discountValue;

                double singlePriceUnit = (double) areaUnitPrice / pricingList.get(0);
                double disountedUnitPrice = singlePriceUnit * discountValue;

                totalDiscountValue = discountValue;
                discountPriceTxtVw.setText(Utils.getConvertedPrice(discountValue, getActivity()));
                discountUnitTxtVw.setText(Utils.getConvertedPrice((long) (discountValue / areaUnitPrice), getActivity()) + "/");
                discountedPriceTxtVw.setText(Utils.getConvertedPrice(discountValue, getActivity()));


                discountedUnit = (int) disountedUnitPrice;
                discountedPrice = discountValue;
                pricingListTemp.set(0, (pricingList.get(0) - discountedPrice));
            } else if (discounts.getPaymentOption().equalsIgnoreCase("Percentage")) {

                discountValue = ((pricingList.get(0) * discountValue) / 100);
                long discountedBSP = pricingList.get(0) - discountValue;

                double singlePriceUnit = (double) areaUnitPrice / pricingList.get(0);
                double disountedUnitPrice = singlePriceUnit * discountValue;

                totalDiscountValue = discountValue;
                discountPriceTxtVw.setText(Utils.getConvertedPrice(discountValue, getActivity()));
                discountUnitTxtVw.setText(Utils.getConvertedPrice((long) (discountValue / areaUnitPrice), getActivity()) + "/");
                discountedPriceTxtVw.setText(Utils.getConvertedPrice(discountValue, getActivity()));

                discountedUnit = (int) disountedUnitPrice;
                discountedPrice = discountValue;
                pricingListTemp.set(0, (pricingList.get(0) - discountedPrice));
            } else if (discounts.getPaymentOption().equalsIgnoreCase("UnitPrice")) {


                float dicountedAreaUnit = (float) (areaUnitPrice * discountValue) / 100;
                double singleUnitPrice = pricingList.get(0) / areaUnitPrice;
                double disountedUnitPrice = singleUnitPrice * dicountedAreaUnit;
                double disountBspFloat = pricingList.get(0) - disountedUnitPrice;
                Float obj = new Float(disountBspFloat);
                long discountedBSP = obj.longValue();


                totalDiscountValue = discountValue;
                discountPriceTxtVw.setText(Utils.getConvertedPrice((long) disountedUnitPrice, getActivity()));
                discountUnitTxtVw.setText(Utils.getConvertedPrice((long) (disountedUnitPrice / areaUnitPrice), getActivity()) + "/");
                discountedPriceTxtVw.setText(Utils.getConvertedPrice((long) disountedUnitPrice, getActivity()));

                discountedUnit = (int) dicountedAreaUnit;
                discountedPrice = new Float(disountedUnitPrice).longValue();
                pricingListTemp.set(0, (pricingList.get(0) - discountedPrice));
            }

        } else {
            realPriceLayout.setVisibility(View.GONE);
            discountPriceLayout.setVisibility(View.GONE);
            totalDiscountLayout.setVisibility(View.GONE);
            discountedPrice = 0;
            totalDiscountValue = discountedPrice;
            pricingListTemp.set(0, pricingList.get(0));
        }

        setTotalAmount();

    }

    private void setPricingLayout(int size, int bspPosition) {

        try {

            SELECTED_SIZE = size;
            realPriceLayout.setVisibility(View.GONE);
            discountPriceLayout.setVisibility(View.GONE);
            totalDiscountLayout.setVisibility(View.GONE);
            priceCalcCardView.setVisibility(View.GONE);
            unitTxt.setText(propertyTypeModel.get(size).getTitle());
            PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.UNIT_VALUE,propertyTypeModel.get(size).getTitle());
            pricingLayout.removeAllViews();

            powerBackupModel.clear();
            pricingTextView.clear();
            titleComponentsView.clear();
            paymentPlanCalcModels.clear();
            parkingComponents.clear();
            compNameTextView.clear();
            componentModels.clear();
            floorPlc.clear();
            plc.clear();
            bsps.clear();
            componentIdList.clear();
            subCompIdList.clear();
            pricingList.clear();
            pricingListTemp.clear();
            serviceTaxList.clear();
            coveredParkModel.clear();
            additionalParkModel.clear();
            checkedItemsListed.clear();
            componentNameList.clear();
            subCompNameList.clear();

            CommonDialogModel commonDialogModel = new CommonDialogModel();
            commonDialogModel.setTitle("None");
            commonDialogModel.setPrice("0");
            commonDialogModel.setActualPrice(0);
            floorPlc.add(commonDialogModel);


            bspUnitPrice = 0;
            bspTotalPrice = 0;
            areaUnitPrice = 0;
            totalAmount = 0;
            bspCalulatedPrice = 0;
            int clickIndex = 0;
            int propertyTypeId = 0;

            if (propertyTypeModel.size() > size) {
                selectedPropertyId = propertyTypeModel.get(size).getId();
                propertyTypeId = propertyTypeModel.get(size).getPropertyTypeId();

            }


            if (pricingModels.size() > size) {
                for (int index = 0; index < pricingModels.get(size).getPriceDetails().size(); index++) {
                    if (pricingModels.get(size).getPriceDetails().get(index).getName().equalsIgnoreCase("BSP")) {
//                        background_color.setBackgroundColor(Color.parseColor("#dff0ef"));
                        double superArea = propertyTypeModel.get(size).getAreaSize();
                        double carpetArea = Double.parseDouble(propertyTypeModel.get(size).getCarpetArea().equalsIgnoreCase("") ? "0" : propertyTypeModel.get(size).getCarpetArea());
                        sizeUnit = propertyTypeModel.get(size).getSizeUnitId();
                        if (pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(bspPosition).getPaymentOption().equalsIgnoreCase("UnitPrice")) {

                            bspUnitPrice = Integer.valueOf(pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(bspPosition).getAmount());


                            if (isAppliedOnCarpetArea) {
                                areaUnitPrice = carpetArea;
                                bspTotalPrice = bspUnitPrice * carpetArea;

                            } else {
                                areaUnitPrice = superArea;
                                bspTotalPrice = bspUnitPrice * superArea;
                            }

                            bspServiceTax = (int) (bspTotalPrice * 3.5f) / 100;

                        } else {
                            if (isAppliedOnCarpetArea) {
                                bspUnitPrice = Integer.valueOf(pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(bspPosition).getAmount()) / carpetArea;
                                areaUnitPrice = carpetArea;
                            } else {
                                bspUnitPrice = Integer.valueOf(pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(bspPosition).getAmount()) / superArea;
                                areaUnitPrice = superArea;
                            }


                            bspTotalPrice = Integer.valueOf(pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(bspPosition).getAmount());
                            bspServiceTax = (int) (bspTotalPrice * 3.5f) / 100;
                        }
                    }
                }

//                unitName = UnitsTable.getInstance().getUnitName(propertyTypeModel.get(size).getSizeUnitId());
//                originalUnitName = unitName;
//
                CategoryTypeModel categoryTypeModel = CategoryType.getInstance().getAreaCategory(propertyTypeId);

                if (propertyTypeModel.get(size).getSuperArea().equalsIgnoreCase("") ||
                        propertyTypeModel.get(size).getSuperArea().equalsIgnoreCase("0")) {
                    superAreaTitleTxtVw.setVisibility(View.GONE);
                    superAreaTxtVw.setVisibility(View.GONE);
                    unitTxtVw4.setVisibility(View.GONE);
                    superAreaSizeTxtVw.setVisibility(View.GONE);
                } else {
                    superAreaTitleTxtVw.setVisibility(View.VISIBLE);
                    superAreaTxtVw.setVisibility(View.VISIBLE);
                    unitTxtVw4.setVisibility(View.VISIBLE);
                    superAreaSizeTxtVw.setVisibility(View.VISIBLE);
                }
//
                if (propertyTypeModel.get(size).getBuiltupArea().equalsIgnoreCase("") ||
                        propertyTypeModel.get(size).getBuiltupArea().equalsIgnoreCase("0")) {
                    builtUpAreaTxtVw.setVisibility(View.GONE);
                    builtUpAreaTitleTxtVw.setVisibility(View.GONE);
                    unitTxtVw5.setVisibility(View.GONE);
                    builtAreaSizeTxtVw.setVisibility(View.GONE);
                } else {
                    builtUpAreaTxtVw.setVisibility(View.VISIBLE);
                    builtUpAreaTitleTxtVw.setVisibility(View.VISIBLE);
                    unitTxtVw5.setVisibility(View.VISIBLE);
                    builtAreaSizeTxtVw.setVisibility(View.VISIBLE);
                }

                if (propertyTypeModel.get(size).getCarpetArea().equalsIgnoreCase("") ||
                        propertyTypeModel.get(size).getCarpetArea().equalsIgnoreCase("0")) {
                    carpetAreaTitleTxtVw.setVisibility(View.GONE);
                    carpetAreaTxtVw.setVisibility(View.GONE);
                    unitTxtVw6.setVisibility(View.GONE);
                    carpetAreaSizeTxtVw.setVisibility(View.GONE);
                } else {
                    carpetAreaTitleTxtVw.setVisibility(View.VISIBLE);
                    carpetAreaTxtVw.setVisibility(View.VISIBLE);
                    unitTxtVw6.setVisibility(View.VISIBLE);
                    carpetAreaSizeTxtVw.setVisibility(View.VISIBLE);
                }

                carpetAreaTitleTxtVw.setText(categoryTypeModel.getArea2());
                builtUpAreaTitleTxtVw.setText(categoryTypeModel.getArea3());
                superAreaTitleTxtVw.setText(categoryTypeModel.getArea1());

                if (isAppliedOnCarpetArea) {
                    calculationBasedTxtvw.setText("Price is calculated on " + categoryTypeModel.getArea2());
                } else {
                    calculationBasedTxtvw.setText("Price is calculated on " + categoryTypeModel.getArea1());
                }
//
                superAreaTxtVw.setText(propertyTypeModel.get(size).getSuperArea().equalsIgnoreCase("") ? "NA" : propertyTypeModel.get(size).getSuperArea());
                builtUpAreaTxtVw.setText(propertyTypeModel.get(size).getBuiltupArea().equalsIgnoreCase("") ? "NA" : propertyTypeModel.get(size).getBuiltupArea());
                carpetAreaTxtVw.setText(propertyTypeModel.get(size).getCarpetArea().equalsIgnoreCase("") ? "NA" : propertyTypeModel.get(size).getCarpetArea());
//
//
//                unitTxtVw1.DaText(unitName);
//                unitTxtVw2.setText(unitName);
//                unitTxtVw3.setText(unitName);
//                unitTxtVw4.setText(unitName);
//                unitTxtVw5.setText(unitName);
//                unitTxtVw6.setText(unitName);
////
//                carpetAreaSizeTxtVw.setText(propertyTypeModel.get(size).getCarpetArea() + " " + unitName);
//                builtAreaSizeTxtVw.setText(propertyTypeModel.get(size).getBuiltupArea() + " " + unitName);
//                superAreaSizeTxtVw.setText(propertyTypeModel.get(size).getSuperArea() + " " + unitName);

                superAreaValue = propertyTypeModel.get(size).getSuperArea().equalsIgnoreCase("") ? 0 : Double.parseDouble(propertyTypeModel.get(size).getSuperArea());
                carpetAreaValue = propertyTypeModel.get(size).getCarpetArea().equalsIgnoreCase("") ? 0 : Double.parseDouble(propertyTypeModel.get(size).getCarpetArea());
                extraAreaValue = propertyTypeModel.get(size).getBuiltupArea().equalsIgnoreCase("") ? 0 : Double.parseDouble(propertyTypeModel.get(size).getBuiltupArea());

                realPriceTxtVw.setText(getActivity().getResources().getString(R.string.rs) + " " + String.valueOf(form.format(bspTotalPrice)));
                unitPriceTxtVw.setText(getActivity().getResources().getString(R.string.rs) + " " + String.valueOf(form.format(bspUnitPrice)));
//
//
                boolean isPlcAdded = false;
                boolean isFloorPlcAdded = false;
                boolean isBspAdded = false;

                for (int index = 0; index < pricingModels.get(size).getPriceDetails().size(); index++) {

                    String name = pricingModels.get(size).getPriceDetails().get(index).getName();
                    String description = pricingModels.get(size).getPriceDetails().get(index).getDescription();
                    double serviceTax = pricingModels.get(size).getPriceDetails().get(index).getServiceTax();
                    int compId = pricingModels.get(size).getPriceDetails().get(index).getProjprprtycomponentID();
                    String calculatedPrice = "";
                    String condition = "";
                    int calculatedAmount = 0;
                    boolean isViewNeedToAdded = false;


                    for (int indexi = 0; indexi < pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().size(); indexi++) {

                        boolean isCompulsory = pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).isCompulsory();
                        String type = pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).getPaymentOption();
                        float amount = Float.valueOf(pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).getAmount());
                        String title = pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).getTitle();
                        int subCompId = pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).getId();
                        int numberOfParkings = pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).getNoOfParking();
                        double subServiceTax = pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).getServiceTax();


                        PaymentPlanCalcModel paymentPlanCalcModel = new PaymentPlanCalcModel();


                        if (name.equalsIgnoreCase("others")) {

                            calculatedAmount = Utils.getInstance().getCalculatedPrice(
                                    type,
                                    areaUnitPrice,
                                    bspUnitPrice,
                                    bspTotalPrice,
                                    amount);
//
                            paymentPlanCalcModel.setSubCompoId(subCompId);
                            paymentPlanCalcModel.setPrice(calculatedAmount);
                            if (title.equalsIgnoreCase("EDC")) {
                                edcAmount = calculatedAmount;
                                paymentPlanCalcModel.setServiceTax(0);

                            } else
                                paymentPlanCalcModel.setServiceTax((int) (calculatedAmount * 14.5 / 100));

                            paymentPlanCalcModels.add(paymentPlanCalcModel);

                            totalAmount = totalAmount + calculatedAmount;
//
                            String typeTxt = Utils.getInstance().getCalculatedPriceType(
                                    getActivity(),
                                    type,
                                    amount, unitName);
//
//
                            setComponent((double) amount, type, "", unitName);
//
                            inflatePriceView(subCompId,
                                    compId,
                                    subServiceTax,
                                    ((long) calculatedAmount),
                                    Utils.getConvertedPrice(calculatedAmount, getActivity()),
                                    title,
                                    typeTxt,
                                    clickIndex,
                                    false,
                                    name,
                                    title);
//
//
                            clickIndex++;
//
                            isViewNeedToAdded = false;
//
//
                        } else if (name.equalsIgnoreCase("EDC & IDC")) {

                            calculatedAmount = Utils.getInstance().getCalculatedPrice(
                                    type,
                                    areaUnitPrice,
                                    bspUnitPrice,
                                    bspTotalPrice,
                                    amount);

                            paymentPlanCalcModel.setSubCompoId(subCompId);
                            paymentPlanCalcModel.setPrice(calculatedAmount);
                            if (title.equalsIgnoreCase("EDC")) {
                                edcAmount = calculatedAmount;
                                paymentPlanCalcModel.setServiceTax(0);
                            } else
                                paymentPlanCalcModel.setServiceTax((int) (calculatedAmount * 14.5 / 100));

                            paymentPlanCalcModels.add(paymentPlanCalcModel);

                            totalAmount = totalAmount + calculatedAmount;

                            String typeTxt = Utils.getInstance().getCalculatedPriceType(
                                    getActivity(),
                                    type,
                                    amount,
                                    unitName);

                            setComponent((double) amount, type, "", unitName);
//
//
                            inflatePriceView(subCompId, compId, serviceTax, ((long) calculatedAmount),
                                    Utils.getConvertedPrice(calculatedAmount, getActivity()),
                                    title, typeTxt, clickIndex, false, name, title);


                            clickIndex++;

                            isViewNeedToAdded = false;


                        } else if (name.equalsIgnoreCase("Parking") || name.equalsIgnoreCase("Power Backup")) {

                            additionalParkModel = new ArrayList<>();
                            String options = pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).getOptions();

                            calculatedAmount = (int) amount;


                            parkingComponents.add(title);

                            boolean isDropDown = false;
                            String titleTxt = "";


                            if (type.equalsIgnoreCase("peritem")) {

                                try {
                                    float singleUnitPrice = amount;
                                    if (null != options && !options.equalsIgnoreCase("")) {
                                        List<String> items = Arrays.asList(options.split("\\s*,\\s*"));
                                        if (!isCompulsory && !items.get(0).equalsIgnoreCase("0")) {
                                            commonDialogModel = new CommonDialogModel();
                                            commonDialogModel.setTitle("Not Required");
                                            commonDialogModel.setActualPrice(0);
                                            commonDialogModel.setPrice(String.valueOf(0));
                                            additionalParkModel.add(commonDialogModel);
                                            numberOfParkings = 0;
                                            titleTxt = "Units: " + "0";
                                        }


                                        for (int kva = 0; kva < items.size(); kva++) {
                                            int value = Integer.valueOf(items.get(kva));
                                            calculatedAmount = (int) (value * singleUnitPrice);
                                            commonDialogModel = new CommonDialogModel();
                                            commonDialogModel.setTitle("Units: " + String.valueOf(value));
                                            commonDialogModel.setActualPrice(calculatedAmount);
                                            commonDialogModel.setPrice(String.valueOf(form.format(calculatedAmount)));
                                            additionalParkModel.add(commonDialogModel);

                                        }
                                        arrayListParkingHashMap.put(title, additionalParkModel);

                                        if (!isCompulsory && !items.get(0).equalsIgnoreCase("0"))
                                            calculatedAmount = 0;
                                        else
                                            calculatedAmount = (int) singleUnitPrice * Integer.valueOf(items.get(0));
                                    }


                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
//
                                isDropDown = true;
                            } else if (type.equalsIgnoreCase("Lumpsum")) {

                                if (!isCompulsory) {
                                    commonDialogModel = new CommonDialogModel();
                                    commonDialogModel.setTitle("Not Required");
                                    commonDialogModel.setActualPrice(0);
                                    commonDialogModel.setPrice(String.valueOf(0));
                                    additionalParkModel.add(commonDialogModel);

                                    commonDialogModel = new CommonDialogModel();
                                    commonDialogModel.setTitle(String.valueOf(numberOfParkings));
                                    commonDialogModel.setActualPrice((int) amount);
                                    commonDialogModel.setPrice(String.valueOf(form.format(amount)));
                                    additionalParkModel.add(commonDialogModel);
                                    isDropDown = true;
                                    calculatedAmount = 0;
                                    arrayListParkingHashMap.put(title, additionalParkModel);
                                    numberOfParkings = 0;
                                    titleTxt = "-";
                                }
                            } else if (type.equalsIgnoreCase("UnitPrice")) {

                                if (!isCompulsory) {
                                    commonDialogModel = new CommonDialogModel();
                                    commonDialogModel.setTitle("Not Required");
                                    commonDialogModel.setActualPrice(0);
                                    commonDialogModel.setPrice(String.valueOf(0));
                                    additionalParkModel.add(commonDialogModel);

                                    commonDialogModel = new CommonDialogModel();
                                    commonDialogModel.setTitle(getActivity().getResources().getString(R.string.rs) + " " + amount + "/" + unitName);
                                    commonDialogModel.setActualPrice((int) (amount * areaUnitPrice));
                                    commonDialogModel.setPrice(String.valueOf(form.format((amount * areaUnitPrice))));
                                    additionalParkModel.add(commonDialogModel);
                                    isDropDown = true;
                                    calculatedAmount = 0;
                                    arrayListParkingHashMap.put(title, additionalParkModel);
                                    numberOfParkings = 0;
                                    titleTxt = "";
                                } else {
                                    calculatedAmount = (int) (amount * areaUnitPrice);
//                                    titleTxt = getActivity().getResources().getString(R.string.rs) + " " + amount + "/" + unitName;
                                    titleTxt = Utils.getConvertedPrice((long) amount, getActivity()) + "/" + unitName;
                                }

                            }
//
                            setComponent((double) calculatedAmount, type, "", unitName);

                            inflatePriceView(subCompId,
                                    compId,
                                    serviceTax,
                                    ((long) calculatedAmount),
                                    Utils.getConvertedPrice(calculatedAmount, getActivity()),
                                    title,
                                    titleTxt,
                                    clickIndex,
                                    isDropDown,
                                    name,
                                    title);

                            clickIndex++;
                        } else if (name.equalsIgnoreCase("plc")) {

                            if (!isPlcAdded) {

                                setComponent(0, type, "", unitName);

                                inflatePriceView(subCompId, compId, serviceTax, ((long) 0),
                                        Utils.getConvertedPrice(0, getActivity()),
                                        name, "", clickIndex, true, name, title);
                                isPlcAdded = true;
                                clickIndex++;
                            }
//
                            calculatedAmount = Utils.getInstance().getCalculatedPrice(
                                    type,
                                    areaUnitPrice,
                                    bspUnitPrice,
                                    bspTotalPrice,
                                    amount);

                            String typeTxt =
                                    Utils.getInstance().getCalculatedPriceType(
                                            getActivity(),
                                            type,
                                            amount,
                                            unitName);

                            commonDialogModel = new CommonDialogModel();
                            commonDialogModel.setTitle(title + " : " + typeTxt);
                            commonDialogModel.setActualPrice(calculatedAmount);
                            commonDialogModel.setPrice(String.valueOf(form.format(calculatedAmount)));
                            plc.add(commonDialogModel);

                            paymentPlanCalcModel.setSubCompoId(subCompId);
                            paymentPlanCalcModel.setPrice(calculatedAmount);
                            paymentPlanCalcModel.setServiceTax((int) (calculatedAmount * 14.5 / 100));

                            paymentPlanCalcModels.add(paymentPlanCalcModel);

                            checkedItemsListed.add(false);

                        } else if (name.equalsIgnoreCase("Floor PLC")) {

                            if (!isFloorPlcAdded) {

                                setComponent(0, type, "", unitName);

                                inflatePriceView(subCompId, compId, serviceTax, ((long) 0),
                                        Utils.getConvertedPrice(0, getActivity()),
                                        name, "", clickIndex, true, name, title);
                                isFloorPlcAdded = true;
                                clickIndex++;
                            }
//
                            calculatedAmount = Utils.getInstance().getCalculatedPrice(
                                    type,
                                    areaUnitPrice,
                                    bspUnitPrice,
                                    bspTotalPrice,
                                    amount);

                            String typeTxt = Utils.getInstance().getCalculatedPriceType(
                                    getActivity(),
                                    type,
                                    amount, unitName);
//
//
                            commonDialogModel = new CommonDialogModel();
                            commonDialogModel.setTitle(title + " : " + typeTxt);
                            commonDialogModel.setActualPrice(calculatedAmount);
                            commonDialogModel.setPrice(String.valueOf(form.format(calculatedAmount)));
                            floorPlc.add(commonDialogModel);

                            paymentPlanCalcModel.setSubCompoId(subCompId);
                            paymentPlanCalcModel.setPrice(calculatedAmount);
                            paymentPlanCalcModel.setServiceTax((int) (calculatedAmount * 14.5 / 100));

                            paymentPlanCalcModels.add(paymentPlanCalcModel);


                        } else if (name.equalsIgnoreCase("BSP")) {


                            calculatedAmount = Utils.getInstance().getCalculatedPrice(
                                    type,
                                    areaUnitPrice,
                                    bspUnitPrice,
                                    bspTotalPrice,
                                    amount);

                            String typeTxt = Utils.getInstance().getCalculatedPriceType(
                                    getActivity(),
                                    type,
                                    amount, unitName);
//
                            if (indexi == bspPosition) {
                                bspCalulatedPrice = calculatedAmount;
                                baspServiceTax = String.valueOf(serviceTax);

                                setComponent(((double) amount), type, title + " - ", unitName);

                                inflatePriceView(subCompId, compId, serviceTax,
                                        ((long) calculatedAmount),
                                        Utils.getConvertedPrice(calculatedAmount, getActivity()),
                                        name, title + " - " + typeTxt, clickIndex, true, name, title);
                                isBspAdded = true;
                                clickIndex++;
                            }
//
//
                            paymentPlanCalcModel.setSubCompoId(subCompId);
                            paymentPlanCalcModel.setPrice(calculatedAmount);
                            paymentPlanCalcModel.setServiceTax((int) (calculatedAmount * 3.50 / 100));

                            paymentPlanCalcModels.add(paymentPlanCalcModel);

                            commonDialogModel = new CommonDialogModel();
                            commonDialogModel.setTitle(title + " - " + typeTxt);
                            commonDialogModel.setActualPrice(calculatedAmount);
                            commonDialogModel.setPrice(String.valueOf(form.format(calculatedAmount)));

                            bsps.add(commonDialogModel);


                        } else {

                            if (pricingModels.get(size).getPriceDetails().get(index).getProjectPropertySubComponents().get(indexi).isCompulsory()) {

                                calculatedAmount = calculatedAmount + Utils.getInstance().getCalculatedPrice(
                                        type,
                                        areaUnitPrice,
                                        bspUnitPrice,
                                        bspTotalPrice,
                                        amount);

                                paymentPlanCalcModel.setSubCompoId(subCompId);
                                paymentPlanCalcModel.setPrice(calculatedAmount);
                                paymentPlanCalcModel.setServiceTax((int) (calculatedAmount * 14.5 / 100));
                                paymentPlanCalcModels.add(paymentPlanCalcModel);
                                isViewNeedToAdded = true;
                            }
                        }
                    }


                    if (isViewNeedToAdded) {

                        totalAmount = totalAmount + calculatedAmount;
                        setComponent(((double) calculatedAmount), "", "", unitName);
                        inflatePriceView(0, 0, serviceTax, totalAmount,
                                Utils.getConvertedPrice(calculatedAmount, getActivity()),
                                name, "", clickIndex, false, "", "");


                        clickIndex++;
                    }

                    priceCalcCardView.setVisibility(View.VISIBLE);
                }
                setTotalAmount();
                setPaymentPlanLayout(0);
                paymentPlanLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void inflatePriceView(int subCompId,
                                  int compId,
                                  double serviceTax,
                                  long actualAmount,
                                  String price,
                                  final String componentName,
                                  String type,
                                  int clickIndex,
                                  boolean isDropDownShow, String compName, String subCompName) {

        View view;
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.pricing_items, null);
        TextView componentNameTv = (TextView) view.findViewById(R.id.componentNameTv);
        TextView priceTxtVw = (TextView) view.findViewById(R.id.priceTxtVw);
        TextView typeTxtVw = (TextView) view.findViewById(R.id.typeTxtVw);
        LinearLayout dropDownIcon = (LinearLayout) view.findViewById(R.id.dropDownIcon);
        background_color = (LinearLayout) view.findViewById(R.id.background_color);
        LinearLayout dropdownLayout = (LinearLayout) view.findViewById(R.id.dropdownLayout);

        if (clickIndex % 3 == 0) {
            background_color.setBackgroundColor(Color.parseColor("#dff0ef"));
        } else if (clickIndex % 2 == 0) {
            background_color.setBackgroundColor(Color.parseColor("#faeae8"));
        } else {
            background_color.setBackgroundColor(Color.parseColor("#faf6e1"));
        }
        Log.e("Actual Price", actualAmount + "---");
        priceTxtVw.setText(price);
        componentNameTv.setText(componentName);
        typeTxtVw.setText(type);

        if (type.equalsIgnoreCase(""))
            typeTxtVw.setText("--");


        if (!isDropDownShow)
            dropDownIcon.setVisibility(View.GONE);


        dropdownLayout.setTag(clickIndex);

        dropdownLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int position = (Integer) view.getTag();


                try {

                    if (compNameTextView.get(position).getText().toString().equalsIgnoreCase("PLC")) {


                        showCommonDialog(getActivity(), plc, 3, position);

                    } else if (compNameTextView.get(position).getText().toString().equalsIgnoreCase("Floor PLC")) {

                        showCommonDialog(getActivity(), floorPlc, 4, position);

                    } else if (compNameTextView.get(position).getText().toString().equalsIgnoreCase("BSP")) {

                        showCommonDialog(getActivity(), bsps, 2, position);

                    } else {
                        ArrayList<CommonDialogModel> parkingModel = arrayListParkingHashMap.get(compNameTextView.get(position).getText().toString());

                        if (null != parkingModel && parkingModel.size() > 0)
                            showCommonDialog(getActivity(), parkingModel, 7, position);


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        serviceTaxList.add(serviceTax);
        pricingList.add(actualAmount);
        pricingListTemp.add(actualAmount);
        compNameTextView.add(componentNameTv);
        pricingTextView.add(priceTxtVw);
        titleComponentsView.add(typeTxtVw);
        pricingLayout.addView(view);
        componentIdList.add(compId);
        subCompIdList.add(subCompId);
        componentNameList.add(compName);
        subCompNameList.add(subCompName);

//        Log.e("compTextSize", compNameTextView.size() + "");
    }

    private void setComponent(double price, String type, String prefix, String unitName) {
        ComponentModel componentModel = new ComponentModel();
        componentModel.setPrice(price);
        componentModel.setType(type);
        componentModel.setUnitName(unitName);
        componentModels.add(componentModel);
    }

    private void setTotalAmount() {

        serviceCalTaxList.clear();
        sum = 0;

        double serviceTax = 0.0;

        for (int i = 0; i < pricingList.size(); i++) {

            double calcTax = ((pricingList.get(i) * serviceTaxList.get(i)) / 100);

            serviceCalTaxList.add(calcTax);

            serviceTax = serviceTax + calcTax;

            sum += pricingList.get(i);

            Log.e("Amounts", pricingList.get(i) + " - -");
        }

        sum = sum + serviceTax;

        realPrice = sum - discountedPrice;


        amountInWordTxtVw.setText(Utils.getInstance().numberToWords(Integer.parseInt(Utils.getConvertedPriceWithoutSymbol((long) Math.round(realPrice), getActivity())), getActivity()));
//
////        realPriceTxtVw.setText(getActivity().getResources().getString(R.string.rs) + " " + form.format(realPrice));
////        unitPriceTxtVw.setText((getActivity().getResources().getString(R.string.rs) + " " + form.format(realPrice / areaUnitPrice) + "/"));
////        totalPriceTxtVw.setText(getActivity().getResources().getString(R.string.rs) + " " + form.format(realPrice));
////         serviceTaxTxtVw.setText(getActivity().getResources().getString(R.string.rs) + " " + form.format(serviceTax));
        serviceTaxTxtVw.setText(Utils.getConvertedPrice((long) serviceTax, getActivity()));
        realPriceTxtVw.setText(Utils.getConvertedPrice((long) realPrice, getActivity()));
        unitPriceTxtVw.setText((Utils.getConvertedPrice((long) (realPrice / areaUnitPrice), getActivity())) + "/");
        totalPriceTxtVw.setText(Utils.getConvertedPrice((long) realPrice, getActivity()));
        effectivePriceTxtVw.setText(Utils.getConvertedPrice((long) sum, getActivity()));


//        //      effectiveUnitTxtVw.setText(GlobalManager.getConvertedPrice((long) Double.parseDouble(getUnitPrice(String.valueOf(sum / areaUnitPrice))), getActivity()) + "/");


        if (superAreaTitleTxtVw.getText().toString().equalsIgnoreCase("Plot Area")) {
            if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_SIZE_UNIT_NAME, "").equalsIgnoreCase("")) {

                plotunitName = PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_SIZE_UNIT_NAME, "");
                unitTxtVw1.setText(plotunitName);
                unitTxtVw2.setText(plotunitName);
                unitTxtVw3.setText(plotunitName);
                unitTxtVw4.setText(plotunitName);
                unitTxtVw5.setText(plotunitName);
                unitTxtVw6.setText(plotunitName);
                carpetAreaSizeTxtVw.setText(getPlotUnitPrice(propertyTypeModel.get(size).getCarpetArea()) + " " + plotunitName);
                builtAreaSizeTxtVw.setText(getPlotUnitPrice(propertyTypeModel.get(size).getBuiltupArea()) + " " + plotunitName);
                superAreaSizeTxtVw.setText(getPlotUnitPrice(propertyTypeModel.get(size).getSuperArea()) + " " + plotunitName);
            } else {
                plotunitName = "sqft";

                Log.e("title", plotunitName);


                unitTxtVw1.setText(plotunitName);
                unitTxtVw2.setText(plotunitName);
                unitTxtVw3.setText(plotunitName);
                unitTxtVw4.setText(plotunitName);
                unitTxtVw5.setText(plotunitName);
                unitTxtVw6.setText(plotunitName);

                carpetAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getCarpetArea()) + " " + plotunitName);
                builtAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getBuiltupArea()) + " " + plotunitName);
                superAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getSuperArea()) + " " + plotunitName);
            }
            if (superAreaValue != 0)
                superAreaTxtVw.setText(Utils.getConvertedPrice((long) (sum / Double.parseDouble(getPlotUnitPrice(String.valueOf(superAreaValue)))), getActivity()) + "/");

            if (extraAreaValue != 0)
                builtUpAreaTxtVw.setText(Utils.getConvertedPrice((long) (sum / Double.parseDouble(getPlotUnitPrice(String.valueOf(extraAreaValue)))), getActivity()) + "/");

            if (carpetAreaValue != 0)
                carpetAreaTxtVw.setText(Utils.getConvertedPrice((long) (sum / Double.parseDouble(getPlotUnitPrice(String.valueOf(carpetAreaValue)))), getActivity()) + "/");

            effectiveUnitTxtVw.setText(Utils.getConvertedPrice((long) (sum / Double.parseDouble(getPlotUnitPrice(String.valueOf(areaUnitPrice)))), getActivity()) + "/");

        } else {
            if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.SIZE_UNIT_NAME, "").equalsIgnoreCase("")) {

                unitName = PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.SIZE_UNIT_NAME, "");

                Log.e("title", unitName);


                unitTxtVw1.setText(unitName);
                unitTxtVw2.setText(unitName);
                unitTxtVw3.setText(unitName);
                unitTxtVw4.setText(unitName);
                unitTxtVw5.setText(unitName);
                unitTxtVw6.setText(unitName);

                carpetAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getCarpetArea()) + " " + unitName);
                builtAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getBuiltupArea()) + " " + unitName);
                superAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getSuperArea()) + " " + unitName);
            } else {
                unitName = "sqft";

                Log.e("title", unitName);


                unitTxtVw1.setText(unitName);
                unitTxtVw2.setText(unitName);
                unitTxtVw3.setText(unitName);
                unitTxtVw4.setText(unitName);
                unitTxtVw5.setText(unitName);
                unitTxtVw6.setText(unitName);

                carpetAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getCarpetArea()) + " " + unitName);
                builtAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getBuiltupArea()) + " " + unitName);
                superAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getSuperArea()) + " " + unitName);
            }
            if (superAreaValue != 0)
                superAreaTxtVw.setText(Utils.getConvertedPrice((long) (sum / Double.parseDouble(getUnitPrice(String.valueOf(superAreaValue)))), getActivity()) + "/");

            if (extraAreaValue != 0)
                builtUpAreaTxtVw.setText(Utils.getConvertedPrice((long) (sum / Double.parseDouble(getUnitPrice(String.valueOf(extraAreaValue)))), getActivity()) + "/");

            if (carpetAreaValue != 0)
                carpetAreaTxtVw.setText(Utils.getConvertedPrice((long) (sum / Double.parseDouble(getUnitPrice(String.valueOf(carpetAreaValue)))), getActivity()) + "/");

            effectiveUnitTxtVw.setText(Utils.getConvertedPrice((long) (sum / Double.parseDouble(getUnitPrice(String.valueOf(areaUnitPrice)))), getActivity()) + "/");

        }


        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.EFFECTIVE_UNIT, effectiveUnitTxtVw.getText().toString());
        PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.EFFECTIVE_PRICE, new DecimalFormat("##.##").format(sum));

        serviceTxBuilder = new StringBuilder();
        for (int indexp = 0; indexp < serviceTaxList.size(); indexp++) {

            String compName = componentNameList.get(indexp);

            if (compName.equalsIgnoreCase("others"))
                compName = subCompNameList.get(indexp);

            if (serviceTaxList.get(indexp) != 0.0)
                serviceTxBuilder.append(serviceTaxList.get(indexp) + "% on " + compName + " and ");
        }

        tax_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showSuccessErrorMessage("GST", Utils.getInstance().subStringChars(serviceTxBuilder.toString(), 4),"ok",getActivity());
            }
        });
//        serviceTaxTitleTxtVw.setText(Utils.getInstance().subStringChars(serviceTxBuilder.toString(), 4));
//

        setPaymentPlanLayout(0);
    }

    public void setData() {


//        if (PreferenceConnector.getInstance(getActivity()).loadBooleanPreferences(Constants.IS_AUTHORIZED)) {


//            ((LinearLayout) view.findViewById(R.id.brokerMessageLayout)).setVisibility(View.GONE);
//            ((LinearLayout) view.findViewById(R.id.messageLayout)).setVisibility(View.GONE);
//            ((LinearLayout) view.findViewById(R.id.detailParentLayout)).setVisibility(View.VISIBLE);

        if (null != projectsDetailModel) {
            if (!isDataSet) {

                paymentPlanModels.clear();
                towersModel.clear();
                propertyTypeModel.clear();
                pricingModels.clear();


                paymentPlanModels = projectsDetailModel.getPaymentPlanModels();
                pricingModels = projectsDetailModel.getPricingModels();


                try {
                    if (paymentPlanModels.get(0).getDisclaimer() == null || paymentPlanModels.get(0).getDisclaimer().equals("")||paymentPlanModels.get(0).getDisclaimer().equals("null")) {
                        disclaimer_Card.setVisibility(View.GONE);
                    } else {
                        disclaimer.setText(Html.fromHtml(paymentPlanModels.get(0).getDisclaimer()));
                    }
                } catch (Exception e) {
                    particular_card.setVisibility(View.GONE);
                    disclaimer_Card.setVisibility(View.GONE);
                    e.printStackTrace();
                }


                if (null != projectsDetailModel.getDisclaimer1() && projectsDetailModel.getDisclaimer1().equalsIgnoreCase("")) {

//                        disclaimerTextVw.setText(Html.fromHtml(projectsDetailModel.getDisclaimer1()));
//                        discmailerCardView.setVisibility(View.VISIBLE);
                } else {
//                        discmailerCardView.setVisibility(View.GONE);
                }

                for (ProjectsDetailModel.Towers towers : projectsDetailModel.getTowers()) {
                    CommonDialogModel commonDialogModel = new CommonDialogModel();
                    commonDialogModel.setId(String.valueOf(towers.getTowerId()));
                    commonDialogModel.setTitle(towers.getTowerName());
                    towersModel.add(commonDialogModel);
                }

                for (ProjectsDetailModel.PropertySize propertySizes : projectsDetailModel.getPropertySizes()) {
                    CommonDialogModel commonDialogModel = new CommonDialogModel();
                    commonDialogModel.setId(String.valueOf(propertySizes.getId()));
                    commonDialogModel.setTitle(propertySizes.getTitle());

                    commonDialogModel.setAreaSize(Double.parseDouble(propertySizes.getSize()));
                    commonDialogModel.setPropertyTypeId(propertySizes.getProperty_typeIdlevel2());
                    commonDialogModel.setSizeUnitId(propertySizes.getSizeUnit());

                    if (null != propertySizes.getCarpetarea())
                        commonDialogModel.setCarpetArea(propertySizes.getCarpetarea());
                    else
                        commonDialogModel.setCarpetArea("");

                    if (null != propertySizes.getBuiltarea())
                        commonDialogModel.setBuiltupArea(propertySizes.getBuiltarea());
                    else
                        commonDialogModel.setBuiltupArea("");

                    if (null != propertySizes.getSize())
                        commonDialogModel.setSuperArea(propertySizes.getSize());
                    else
                        commonDialogModel.setSuperArea("");


                    propertyTypeModel.add(commonDialogModel);
                    propertyTypeModelOriginal.add(commonDialogModel);
                }

                if (propertyTypeModel.size() > 0) {
                    unitTxt.setText(propertyTypeModel.get(0).getTitle());
//
                    setAreaBgColor(0);
//
                    setDiscountLayout(0);
                    setPricingLayout(0, 0);
                    isDataSet = true;
                }

                if (towersModel.size() == 0)
                    towerParentLayout.setVisibility(View.GONE);
                else if (towersModel.size() == 1)
                    towerTxt.setText(towersModel.get(0).getTitle());

            }
        } else {

//                ((LinearLayout) view.findViewById(R.id.messageLayout)).setVisibility(View.VISIBLE);
//                ((LinearLayout) view.findViewById(R.id.detailParentLayout)).setVisibility(View.GONE);
        }
//        }
//        else {
//            setBrokerVerifiedLayout();
//        }
    }

    public void showCommonDialog(Context context,
                                 final ArrayList<CommonDialogModel> commonDialogModels,
                                 final int from, final int clickedPosition) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_common);
        dialog.show();


        ListView listView = (ListView) dialog.findViewById(R.id.listView);
        Button registerBtn = (Button) dialog.findViewById(R.id.registerBtn);


        if (from == 3) {

            ArrayList<CommonDialogModel> commonDialogModels1 = new ArrayList<>();

            dialog.setCancelable(false);
            adapterCommonDialog = new AdapterCommonDialog(getActivity(),
                    commonDialogModels,
                    true,
                    checkedItemsListed);
            listView.setAdapter(adapterCommonDialog);

            registerBtn.setVisibility(View.VISIBLE);

        } else {

            listView.setAdapter(new AdapterCommonDialog(getActivity(), commonDialogModels));
        }


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int amount = 0;
                String resultString = "";

                StringBuilder result = new StringBuilder();
                for (int i = 0; i < adapterCommonDialog.checkedItemsList.size(); i++) {
                    if (adapterCommonDialog.checkedItemsList.get(i) == true) {
                        amount = amount + plc.get(i).getActualPrice();
                        result.append(plc.get(i).getTitle());
                        result.append(", ");
                    }
                }
                resultString = result.toString().trim();
                checkedItemsListed = new ArrayList<Boolean>(adapterCommonDialog.checkedItemsList);

                if (result.length() > 2)
                    resultString = resultString.substring(0, result.length() - 2);
                else
                    resultString = "--";

                plcAmount = amount;
                pricingTextView.get(clickedPosition).setText(Utils.getConvertedPrice(amount, getActivity()));
                titleComponentsView.get(clickedPosition).setText(resultString);

                dialog.cancel();

                pricingList.set(clickedPosition, ((long) plcAmount));
                pricingListTemp.set(clickedPosition, ((long) plcAmount));

                setTotalAmount();


            }
        });


        listView.setItemsCanFocus(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                            @Override
                                            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                                    long arg3) {


                                                if (from == 0) {


                                                    unitTxt.setText(propertyTypeModel.get(arg2).getTitle());
                                                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.UNIT_VALUE, unitTxt.getText().toString());


                                                    SELECTED_SIZE_POSITION = arg2;
                                                    setAreaBgColor(arg2);
                                                    setDiscountLayout(arg2);
                                                    setPricingLayout(arg2, 0);
                                                    setPaymentPlanLayout(0);


                                                } else if (from == 10) {


                                                    towerTxt.setText(towersModel.get(arg2).getTitle());
                                                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.TOWER_VALUE, towerTxt.getText().toString());


                                                    int id = Integer.parseInt(towersModel.get(arg2).getId());

                                                    propertyTypeModel.clear();

                                                    int index = 0;
                                                    for (ProjectsDetailModel.PropertySize propertySize : projectsDetailModel.getPropertySizes()) {
                                                        for (int towerId : propertySize.getTowers()) {
                                                            if (towerId == id) {
                                                                propertyTypeModel.add(propertyTypeModelOriginal.get(index));
                                                                break;
                                                            }
                                                        }
                                                        index++;
                                                    }


                                                    if (propertyTypeModel.size() == 0) {

                                                    }


                                                } else if (from == 1) {

                                                    setPaymentPlanLayout(arg2);
                                                    planSelectedTxt.setText(paymentTypeModel.get(arg2).getTitle());


                                                } else if (from == 2) {

                                                    setPricingLayout(SELECTED_SIZE_POSITION, arg2);

                                                } else if (from == 4) {


                                                    int amount = floorPlc.get(arg2).getActualPrice();
                                                    flcAmount = amount;
                                                    pricingTextView.get(clickedPosition).setText(Utils.getConvertedPrice(amount, getActivity()));
                                                    titleComponentsView.get(clickedPosition).setText(floorPlc.get(arg2).getTitle());


                                                    pricingList.set(clickedPosition, ((long) amount));
                                                    pricingListTemp.set(clickedPosition, ((long) amount));

                                                    setTotalAmount();

                                                } else if (from == 5) {

//                                                    setAreaBgColor(SELECTED_SIZE_POSITION);
                                                    setPricingLayout(SELECTED_SIZE_POSITION, arg2);

                                                } else if (from == 6) {

                                                    int amount = powerBackupModel.get(arg2).getActualPrice();
                                                    pricingTextView.get(clickedPosition).setText(Utils.getConvertedPrice(amount, getActivity()));
                                                    titleComponentsView.get(clickedPosition).setText(powerBackupModel.get(arg2).getTitle());
                                                    pricingList.set(clickedPosition, ((long) amount));
                                                    pricingListTemp.set(clickedPosition, ((long) amount));

                                                    setTotalAmount();

                                                } else if (from == 7) {

                                                    String title = commonDialogModels.get(arg2).getTitle();

                                                    if (title.equalsIgnoreCase("Not Required"))
                                                        title = "-";

                                                    int amount = commonDialogModels.get(arg2).getActualPrice();
                                                    pricingTextView.get(clickedPosition).setText(Utils.getConvertedPrice(amount, getActivity()));

                                                    titleComponentsView.get(clickedPosition).setText(title);

                                                    pricingList.set(clickedPosition, ((long) amount));
                                                    pricingListTemp.set(clickedPosition, ((long) amount));

                                                    setTotalAmount();

                                                } else if (from == 15) {

                                                    String currencyPrice = commonDialogModels.get(arg2).getTitle();
                                                    String currencyName = commonDialogModels.get(arg2).getId();
                                                    float currencyDPrice = (float) commonDialogModels.get(arg2).getCurrencyDPrice();


                                                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.DEFAULT_CURRENCY_NAME, currencyName);
                                                    PreferenceConnector.getInstance(getActivity()).saveFloatPreferences(Constants.DEFAULT_CURRENCY_VALUE, currencyDPrice);
//                                                    currenyTxtVw.setText(currencyPrice);


                                                    setPricingLayout(SELECTED_SIZE, 0);

                                                    setTotalAmount();
                                                    setDiscountApplied(PricingUnitFragment.this.size,
                                                            PricingUnitFragment.this.position,
                                                            PricingUnitFragment.this.isDiscountCalculated);

//                                                    currenyTxtVw.setText(Utils.getCurrenySelected(getActivity()));

//                                                    ((ProjectDetailActivity) getActivity()).onCurrencyChange(0);


//                                                    IS_PRICE_CHANGE = true;

                                                }
//                                                else if (from == 16) {
//
//                                                    String title = commonDialogModels.get(arg2).getTitle();
//                                                    Log.e("ID", commonDialogModels.get(arg2).getId());
//
//                                                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.SIZE_UNIT_ID,Integer.parseInt(commonDialogModels.get(arg2).getId()));
//                                                    PreferenceConnector.getInstance(getActivity()).savePreferences(Constants.SIZE_UNIT_NAME,title);
//
//                                                    sizeTextView.setText(title);
//
//                                                    unitTxtVw1.setText(title);
//                                                    unitTxtVw2.setText(title);
//                                                    unitTxtVw3.setText(title);
//                                                    unitTxtVw4.setText(title);
//                                                    unitTxtVw5.setText(title);
//                                                    unitTxtVw6.setText(title);
//
//                                                    carpetAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getCarpetArea()) + " " + title);
//                                                    builtAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getBuiltupArea()) + " " + title);
//                                                    superAreaSizeTxtVw.setText(getUnitPrice(propertyTypeModel.get(size).getSuperArea()) + " " + title);
//                                                    setTotalAmount();
//
//                                                    setDiscountApplied(PricingUnitFragment.this.size,
//                                                            PricingUnitFragment.this.position,
//                                                            PricingUnitFragment.this.isDiscountCalculated);

//                                                    //                                                    if (sizeUnit == Integer.parseInt(commonDialogModels.get(arg2).getId())) {
////
////                                                        carpetAreaSizeTxtVw.setText(propertyTypeModel.get(size).getCarpetArea() + " " + title);
////                                                        builtAreaSizeTxtVw.setText(propertyTypeModel.get(size).getBuiltupArea() + " " + title);
////                                                        superAreaSizeTxtVw.setText(propertyTypeModel.get(size).getSuperArea() + " " + title);
////
////                                                    } else {
////                                                        float carpetAreaCal = Float.parseFloat(propertyTypeModel.get(size).getCarpetArea());
////                                                        float superAreaCal = Float.parseFloat(propertyTypeModel.get(size).getSuperArea());
////                                                        float builtAreaCal = Float.parseFloat(propertyTypeModel.get(size).getBuiltupArea());
////                                                        if (sizeUnit != 19) {
////
////                                                            String sizeUnitValue = SizeUnitTable.getInstance()
////                                                                    .readCategories(sizeUnit, 1675);
////
////                                                            carpetAreaCal = Float.parseFloat(propertyTypeModel.get(size).getCarpetArea());
////                                                            builtAreaCal= Float.parseFloat(propertyTypeModel.get(size).getBuiltupArea());
////                                                            superAreaCal = Float.parseFloat(propertyTypeModel.get(size).getSuperArea());
////                                                        }
////
////                                                        String sizeUnitValue = SizeUnitTable.getInstance()
////                                                                .readCategories(
////                                                                        Integer.valueOf(commonDialogModels.get(
////                                                                                arg2).getId()), 1675);
////
////                                                        if (!sizeUnitValue.equalsIgnoreCase("")) {
////                                                            Log.e("Value",
////                                                                    "--2" + carpetAreaCal
////                                                                            / Float.parseFloat(sizeUnitValue));
////
////                                                            superAreaCal = superAreaCal / Float.parseFloat(sizeUnitValue);
//////
////                                                            carpetAreaSizeTxtVw.setText(String.valueOf(sizeFormat.format(carpetAreaCal / Float.parseFloat(sizeUnitValue))) + " " + title);
////                                                            builtAreaSizeTxtVw.setText(String.valueOf(sizeFormat.format(superAreaCal/ Float.parseFloat(sizeUnitValue))) + " " + title);
////                                                            superAreaSizeTxtVw.setText(String.valueOf(sizeFormat.format(superAreaCal)) + " " + title);
////
////
////                                                        } else {
////
////                                                            Toast.makeText(getActivity(),
////                                                                    "No value is available", Toast.LENGTH_LONG)
////                                                                    .show();
////                                                        }
////                                                    }
//                                                }
                                                dialog.cancel();
                                            }
                                        }
        );
    }

    private String getUnitPrice(String sum) {
        if (!sum.equalsIgnoreCase("")) {
            String output = sum;

            int unitId = 0;
            if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.SIZE_UNIT_ID, "").equalsIgnoreCase("")) {
                unitId = Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.SIZE_UNIT_ID, ""));

            } else {
                unitId = 19;

            }

            if (sizeUnit == unitId) {

                return sum;

            } else {
                float carpetAreaCal = Float.parseFloat(sum);
                if (sizeUnit != 19) {


                    carpetAreaCal = Float.parseFloat(sum);

                }

                String sizeUnitValue = SizeUnitTable.getInstance()
                        .readCategories(unitId, 1675);

                if (!sizeUnitValue.equalsIgnoreCase("")) {

                    return String.valueOf(sizeFormat.format(carpetAreaCal / Float.parseFloat(sizeUnitValue)));

                } else {

//                    Toast.makeText(getActivity(),
//                            "No value is available", Toast.LENGTH_LONG)
//                            .show();
                }


            }


            return output;
        } else {
            return sum;
        }
    }

    private String getPlotUnitPrice(String sum) {
        if (!sum.equalsIgnoreCase("")) {
            String output = sum;

            int unitId = 0;
            if (!PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_SIZE_UNIT_ID, "").equalsIgnoreCase("")) {
                unitId = Integer.parseInt(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.PLOT_SIZE_UNIT_ID, ""));

            } else {
                unitId = 19;

            }

            if (sizeUnit == unitId) {

                return sum;

            } else {
                float carpetAreaCal = Float.parseFloat(sum);
                if (sizeUnit != 19) {


                    carpetAreaCal = Float.parseFloat(sum);

                }

                String sizeUnitValue = SizeUnitTable.getInstance()
                        .readCategories(unitId, 1675);

                if (!sizeUnitValue.equalsIgnoreCase("")) {

                    return String.valueOf(sizeFormat.format(carpetAreaCal / Float.parseFloat(sizeUnitValue)));

                } else {

//                    Toast.makeText(getActivity(),
//                            "No value is available", Toast.LENGTH_LONG)
//                            .show();
                }


            }


            return output;
        } else {
            return sum;
        }
    }

    private void setAreaBgColor(int position) {


        CategoryTypeModel categoryTypeModel = CategoryType.getInstance().getAreaCategory(projectsDetailModel.getPropertySizes().get(position).getProperty_typeIdlevel2());


        if (projectsDetailModel.getPropertySizes().get(position).isApplicableOnCarpetArea()) {
            isAppliedOnCarpetArea = true;
            calculationBasedTxtvw.setText("Price is calculated on " + categoryTypeModel.getArea2());
        } else {
            isAppliedOnCarpetArea = false;
            calculationBasedTxtvw.setText("Price is calculated on " + categoryTypeModel.getArea1());
        }
    }

    private void setUnitVariantLayout() {

        unitVariantLayout.removeAllViews();

        long minPriceL = 100000000000000l;
        long maxPriceL = 0;

        long minSizeL = 100000000000000l;
        long maxSizeL = 0;

        String startsFrom = "NA";
//        String unitName = "";
        propertySizesModels = projectsDetailModel.getPropertySizes();

        for (int index = 0; index < propertySizesModels.size(); index++) {

            int sizeUnitId = propertySizesModels.get(index).getSizeUnit();

//            unitName = UnitsTable.getInstance().getUnitName(sizeUnitId);
//

            View view;
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.unit_varient_items, null);
            TextView sizeTxt = (TextView) view.findViewById(R.id.sizeTxt);
            TextView titleTxt = (TextView) view.findViewById(R.id.titleTxt);
            TextView rangeTxt = (TextView) view.findViewById(R.id.rangeTxt);
            ImageView detailBtn = (ImageView) view.findViewById(R.id.detailBtn);
            ImageView layout_plan = (ImageView) view.findViewById(R.id.layout_plan);
            ImageView area_varient = (ImageView) view.findViewById(R.id.area_varient);

            sizeTxt.setText(getUnitPrice(propertySizesModels.get(index).getSize()) + " " + unitName);
            titleTxt.setText(propertySizesModels.get(index).getTitle());
            String minPrice = Utils.getUnit(String.valueOf(propertySizesModels.get(index).getMinimumPrice()), getActivity());
            //  String maxPrice = GlobalManager.getUnit(String.valueOf(propertySizesModels.get(index).getMaximumPrice()));

            rangeTxt.setText(minPrice + " onwards");
            layout_plan.setTag(index);
            area_varient.setTag(index);
            layout_plan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    position1 = (Integer) view.getTag();
//                    showLayoutPlanDialog();
                }
            });
            area_varient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    position1 = (Integer) view.getTag();
//                    showVariantDialog();
                }
            });

            detailBtn.setTag(index);
            detailBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    position1 = (Integer) view.getTag();
                    count_unit_varient = 0;
                    view_unit_varient.setVisibility(View.GONE);
                    unit_varientArrow.setImageResource(R.drawable.right_arrow);
                    view_price_paymentLayout.setVisibility(View.VISIBLE);

                    count_price = 0;

                    setDiscountLayout(position1);
                    setPricingLayout(position1, 0);
                    setPlanModel();
                    setPaymentPlanLayout(0);

                    if (paymentTypeModel.size() > 0)
                        planSelectedTxt.setText(paymentTypeModel.get(0).getTitle());
//                    clickedPropertyItem = position;
//                    ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
//                    viewPager.setCurrentItem(1);

                }
            });


            if (minSizeL > Double.parseDouble(propertySizesModels.get(index).getSize()))
                minSizeL = (long) Double.parseDouble(propertySizesModels.get(index).getSize());

            if (maxSizeL < Double.parseDouble(propertySizesModels.get(index).getSize()))
                maxSizeL = (long) Double.parseDouble(propertySizesModels.get(index).getSize());


            if (minPriceL > propertySizesModels.get(index).getMinimumPrice())
                minPriceL = propertySizesModels.get(index).getMinimumPrice();

            if (maxPriceL < propertySizesModels.get(index).getMaximumPrice())
                maxPriceL = propertySizesModels.get(index).getMaximumPrice();

            unitVariantLayout.addView(view);
        }

        if (minSizeL != maxSizeL)
            startsFrom = minSizeL + "-" + maxSizeL;
        else
            startsFrom = String.valueOf(minSizeL);

        if (!numberOfBedrooms.equalsIgnoreCase("")) {

            StringBuilder bhk = new StringBuilder();
            Collections.sort(bedrooms);

            for (Integer room : bedrooms) {
                bhk.append(room + ",");
            }

            String bhks = bhk.toString();
            bhks = bhks.substring(0, bhks.length() - 1);
//            sizesTxt.setText(bhks + " BHK");


        } else {

            if (propertySizesModels.size() > 0) {
//                sizesTxt.setText(propertySizesModels.get(0).getTitle());
            }
        }
        if (propertySizesModels.size() > 0) {
//            proertyTypeTxt.setText(CategoryTable.getInstance().getCategoryName(propertySizesModels.get(0).getProperty_typeIdlevel1()) + ": " + CategoryType.getInstance().getCategoryName(propertySizesModels.get(0).getProperty_typeIdlevel2()));
        }

//        rangeTxt.setText(Utils.getUnit(String.valueOf(minPriceL), getActivity()) + "  onwards");
//        startFromTxt.setText(startsFrom + " " + unitName);
    }





    private void setPlanModel() {
        paymentTypeModel.clear();

        for (PaymentPlanModel paymentPlanModel : paymentPlanModels) {
            for (PaymentPlanModel.PropertySizes propertySizes : paymentPlanModel.getPropertySizes()) {
                if (String.valueOf(propertySizes.getId()).equalsIgnoreCase(selectedPropertyId)) {
                    CommonDialogModel commonDialogModel = new CommonDialogModel();
                    commonDialogModel.setTitle(paymentPlanModel.getName());
                    paymentTypeModel.add(commonDialogModel);
                }
            }
        }
    }

    private void setPaymentPlanLayout(int size) {

        planSelectedLayout.removeAllViews();
        paymentPlanLayout.setVisibility(View.VISIBLE);
//        paymentPlantMasterLayout.setVisibility(View.VISIBLE);
        double totalPaymentPlanAmount = 0;
        double bookinAmount = 0;
        boolean isLumsum = false;


        if (paymentPlanModels.size() > size) {
            for (int index = 0; index < paymentPlanModels.get(size).getMilestones().size(); index++) {

                View view;
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.payment_plan_items, null);
                TextView titleTxtVw = (TextView) view.findViewById(R.id.titleTxtVw);
                TextView priceTxtVw = (TextView) view.findViewById(R.id.priceTxtVw);


                double amount = 0;
                boolean isLumSum = paymentPlanModels.get(size).getMilestones().get(index).isLumsum();


                PaymentPlanModel.Discount discount = paymentPlanModels.get(size).getDiscount();

                StringBuilder percentageName = new StringBuilder();
                StringBuilder discountNames = new StringBuilder();

                for (PaymentPlanModel.Milestones.PaymentPlanMilestoneComponents paymentPlanMilestoneComponents : paymentPlanModels.get(size).getMilestones().get(index).getPaymentPlanMilestoneComponentses()) {

                    if (paymentPlanMilestoneComponents.getMilestoneValue() != 0) {

                        if (paymentPlanMilestoneComponents.getSubComponentId() == 0) {

                            for (int indexComp = 0; indexComp < componentIdList.size(); indexComp++) {

                                if (paymentPlanMilestoneComponents.getComponentId() == componentIdList.get(indexComp)) {

                                    String compName = componentNameList.get(indexComp);
                                    if (isLumSum) {

                                        amount = amount + paymentPlanMilestoneComponents.getMilestoneValue();

                                    } else {

                                        if (!percentageName.toString().contains(componentNameList.get(indexComp)))
                                            percentageName.append(paymentPlanMilestoneComponents.getMilestoneValue() + "% of " + componentNameList.get(indexComp) + ", ");

                                        double compValue = pricingListTemp.get(indexComp) * (double) paymentPlanMilestoneComponents.getMilestoneValue() / 100;
                                        amount = amount + compValue;

                                        double serviceTax = serviceCalTaxList.get(indexComp);

                                        double value = 0.0;
                                        if (paymentPlanMilestoneComponents.isDiscountApplicable()) {

                                            discountNames.append(decimalForm.format(Float.valueOf(discount.getDiscountValue())) + "% of " + compName + ", ");
                                            value = Float.valueOf(discount.getDiscountValue()) * compValue / 100;

                                            Log.e("DiscountValue", discount.getDiscountValue() + "--" + value);

                                            serviceTax = serviceTax - (serviceTax * Float.valueOf(discount.getDiscountValue()) / 100);
                                        }

                                        amount = amount - value;


                                        amount = amount + serviceTax * (double) paymentPlanMilestoneComponents.getMilestoneValue() / 100;

                                        Log.e("With service tax", discount.getDiscountValue() + "--" + value);
                                    }
                                }
                            }

                        } else {
                            for (int indexComp = 0; indexComp < subCompIdList.size(); indexComp++) {
                                if (subCompIdList.get(indexComp) == paymentPlanMilestoneComponents.getSubComponentId()) {

                                    String subCompName = subCompNameList.get(indexComp);

                                    if (isLumSum)

                                        amount = amount + paymentPlanMilestoneComponents.getMilestoneValue();

                                    else {

                                        if (!percentageName.toString().contains(componentNameList.get(indexComp)))
                                            percentageName.append(paymentPlanMilestoneComponents.getMilestoneValue() + "% of " + componentNameList.get(indexComp) + ", ");

                                        double compValue = pricingListTemp.get(indexComp) * (double) paymentPlanMilestoneComponents.getMilestoneValue() / 100;
                                        amount = amount + compValue;

                                        double serviceTax = serviceCalTaxList.get(indexComp);


                                        double value = 0.0;
                                        if (paymentPlanMilestoneComponents.isDiscountApplicable()) {

                                            discountNames.append(Float.valueOf(discount.getDiscountValue()) + "% of " + subCompName + ", ");
                                            Log.e("DiscountValue", discount.getDiscountValue() + "--");
                                            value = Float.valueOf(discount.getDiscountValue()) * compValue / 100;
                                            serviceTax = serviceTax - (serviceTax * Float.valueOf(discount.getDiscountValue()) / 100);
                                        }

                                        amount = amount - value;
                                        amount = amount + serviceTax * (double) paymentPlanMilestoneComponents.getMilestoneValue() / 100;
                                    }
                                }
                            }
                        }
                    }

                }

                //// isInclusive BookingAmount
                boolean isInclusive = false;

                if (paymentPlanModels.get(size).getMilestones().get(index).getMilestoneNumber().equalsIgnoreCase("Booking Amount")) {
                    bookinAmount = amount;
                    isLumsum = paymentPlanModels.get(size).getMilestones().get(index).isLumsum();
                }
                if (index == 1) {
                    isInclusive = paymentPlanModels.get(size).getMilestones().get(index).isInclusive();

                    if (isInclusive && isLumsum) {
                        amount = amount - bookinAmount;
                    }
                }
                /////


                priceTxtVw.setText(Utils.getConvertedPrice((long) amount, getActivity()));

                if (!method(percentageName.toString()).equalsIgnoreCase(""))
                    titleTxtVw.setText((Html.fromHtml(paymentPlanModels.get(size).getMilestones().get(index).getMilestoneNumber() + "<br><br>(" + method(percentageName.toString()) + ")")));
                else
                    titleTxtVw.setText(paymentPlanModels.get(size).getMilestones().get(index).getMilestoneNumber());

                if (discountNames.toString().length() > 2) {
                    titleTxtVw.setText(Html.fromHtml(titleTxtVw.getText().toString() + "<br><br>" + "<b>DISCOUNTS:</b> " + (discountNames.toString().substring(0, discountNames.toString().length() - 2))));
                }

                totalPaymentPlanAmount = totalPaymentPlanAmount + amount;


                planSelectedLayout.addView(view);
            }


            if (realPrice != totalPaymentPlanAmount) {

                discountPriceLayout.setVisibility(View.VISIBLE);
                realPriceLayout.setVisibility(View.VISIBLE);
            } else {

            }

            long finalDiscountValue = (long) (totalDiscountValue + (realPrice - totalPaymentPlanAmount));

            if (finalDiscountValue > 0) {
                discountPriceLayout.setVisibility(View.VISIBLE);
                discountPriceTxtVw.setText(Utils.getConvertedPrice(finalDiscountValue, getActivity()));
                discountUnitTxtVw.setText(Utils.getConvertedPrice((long) (finalDiscountValue / areaUnitPrice), getActivity()) + "/");
            } else {
                discountPriceLayout.setVisibility(View.GONE);
            }
            totalPaymentPlanTxtVw.setText(Utils.getConvertedPrice((long) totalPaymentPlanAmount, getActivity()));
            realPriceTxtVw.setText(Utils.getConvertedPrice((long) totalPaymentPlanAmount, getActivity()));
            unitPriceTxtVw.setText((Utils.getConvertedPrice((long) (totalPaymentPlanAmount / areaUnitPrice), getActivity())) + "/");

        } else {
            paymentPlantMasterLayout.setVisibility(View.GONE);
        }
    }

    public String method(String str) {
        if (str != null && str.length() > 0) {
            str = str.substring(0, str.length() - 2);
        }
        return str;
    }



        @Override
        public void onChangeTab(int position) {
            Log.e("tabb","tab");

            if (!IS_VIEW_INFLATED) {
                setDiscountLayout(VarientFragment.clickedPropertyItem);
                setPricingLayout(VarientFragment.clickedPropertyItem, 0);
                setPlanModel();
                setPaymentPlanLayout(0);

                if (paymentTypeModel.size() > 0)
                    planSelectedTxt.setText(paymentTypeModel.get(0).getTitle());

                IS_VIEW_INFLATED = true;
            }
            if(selected_unitvalue.getText().toString().equals("")&& PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.UNIT_VALUE, "").equals("")){
                selected_unitvalue.setText("Please select size and tower from Payment");
            }else {
                selected_unitvalue.setText(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.TOWER_VALUE, "") + " " + PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.UNIT_VALUE, ""));
            }

//              if(effctive_unittxt.getText().toString().equals("")){
//                  effctive_unittxt.setText("N/A");
//              }else{
            effctive_unittxt.setText(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.EFFECTIVE_UNIT, "")+""+unitTxtVw1.getText().toString());

//              }
//              if(effective_pricetxt.getText().toString().equals("")){
//                  effective_pricetxt.setText("N/A");
//              }else{
            double priceval=Double.parseDouble(PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.EFFECTIVE_PRICE, ""));
            effective_pricetxt.setText(Utils.getConvertedPrice((long)priceval,getActivity()));

//              }
        }



    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }
    private void setLogosLayout() {


        projectLendersesModel=projectsDetailModel.getProjectLenders();

        if(projectLendersesModel.isEmpty()||projectLendersesModel==null){
            no_lenders_avail.setVisibility(View.VISIBLE);
            finance_list.setVisibility(View.GONE);
        }
        else{
            no_lenders_avail.setVisibility(View.GONE);
            finance_list.setVisibility(View.VISIBLE);

        }
        projectLenderAdapter=new ProjectLenderAdapter(getActivity(),projectLendersesModel);
        finance_list.setAdapter(projectLenderAdapter);

//        if (projectLendersesModel.size() > 0)
//            logosParentLayout.setVisibility(View.VISIBLE);

//        for (int index = 0; index < projectLendersesModel.size(); index++) {
//
//
//            LayoutInflater inflaterLogo = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View viewLogo = inflaterLogo.inflate(R.layout.bank_logos_item, null);
//            ImageView logoImageVw = (ImageView) viewLogo.findViewById(R.id.logoImageVw);
//            LinearLayout logoLayout = (LinearLayout) viewLogo.findViewById(R.id.logosLayout);
//
//
//            String url = GlobalManager.getGeneratedImageUrl(projectLendersesModel.get(index).getLogo());
//
//
//            GlobalManager.setImageView(url, logoImageVw, getActivity());
//
//            logoImageVw.setTag(index);
//            logoImageVw.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                }
//            });
//            logosLayout.addView(viewLogo);
//        }
    }


// VIEWPAGER ADAPTER------------------------------------------------


}
