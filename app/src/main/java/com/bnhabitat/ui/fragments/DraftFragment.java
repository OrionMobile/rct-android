package com.bnhabitat.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.ui.adapters.Draft_adapter;


import java.util.ArrayList;

/**
 *
 */
public class DraftFragment extends Fragment implements TabChangeListener {

    View view;
    RecyclerView draft_list;
    ArrayList<InventoryModel> inventoryModelArrayList = new ArrayList<>();
    ArrayList<InventoryModel> draftArrayList = new ArrayList<>();
    Draft_adapter inventoryAdapter;
    LinearLayoutManager linearLayoutManager;

    public DraftFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_draft, container, false);
        draft_list = (RecyclerView) view.findViewById(R.id.draft_list);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        Bundle bundle = this.getArguments();
        try {
            if (bundle != null) {
                draftArrayList.clear();

//              projectsDetailModel = (ProjectsDetailModel) bundle.getSerializable("projectModel");
                inventoryModelArrayList = (ArrayList<InventoryModel>) bundle.getSerializable("property_data");
                for (int i = 0; i < inventoryModelArrayList.size(); i++) {

                    InventoryModel model = inventoryModelArrayList.get(i);
                    boolean isDraft = model.isDraft();

                    if (inventoryModelArrayList.get(i).isDraft()) {
                        draftArrayList.add(model);
                    }
                }

                inventoryAdapter = new Draft_adapter(getActivity(), draftArrayList);
                draft_list.setLayoutManager(linearLayoutManager);
                draft_list.setAdapter(inventoryAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    // TODO: Rename method, update argument and hook method into UI event


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
