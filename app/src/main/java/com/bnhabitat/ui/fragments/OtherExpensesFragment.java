package com.bnhabitat.ui.fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.FeatureModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.OtherExpensesModel;
import com.bnhabitat.models.PropAreaModel;
import com.bnhabitat.models.PropertyOtherExpensesModel;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.ui.activities.MyPropertyActivity;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.adapters.OtherExpensedAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;


public class OtherExpensesFragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener {
    View view;
    String PropertyId, edit_property;
    SendMessage SM;
    int mYear, mMonth, mDay;
    ImageView next_other_expenses;
    EditText cam;
    ArrayList<PropertyUnitsModel> propertyUnitsModels;
    ArrayList<PropertyOtherExpensesModel> PropertyOtherExpences = new ArrayList<>();
    ArrayList<InventoryModel> json_Data;
    PropertyOtherExpensesModel propertyOtherExpensesModel, propertyOtherExpensesModel1, propertyOtherExpensesModel2, propertyOtherExpensesModel3;
    TextView water_txt, munciple_txt, electricity_txt, severage_txt;
    private String blockCharacterSet = "~#^|$%&/*!'+-:()<>_`€…•™£¥¤§©®¶µ[]αβ√M{}=?";
    String PropertyTypeId;
    private JSONObject jsonSpecification;
    private JSONArray jsonSpecific;
    String pid;
    private ArrayList<PropAreaModel> propArrayList;
    private String Editid;
    LinearLayout linear_layout;
    NestedScrollView scroll_view;
    RecyclerView rv_otherexpenses;
    ArrayList<OtherExpensesModel> otherExpensesModels= new ArrayList<>();
    //ArrayList<OtherExpensesModel> otherExpensesModelsResponse= new ArrayList<>();
    public  static  String cam_string ="";
    int iskeyboaropen=0;

    OtherExpensedAdapter otherExpensesAdapter ;
    private ArrayList<OtherExpensesModel> otherExpensesArray= new ArrayList<>();

    public OtherExpensesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_other_expenses, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        onIntializeView(view);
        blockChar();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            try {
                pid = bundle.getString("pid");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (bundle != null) {
            try {
                PropertyId = bundle.getString("PropertyId") == null ? "" : bundle.getString("PropertyId");
                PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");


            } catch (Exception e) {

            }
            try {

                edit_property = bundle.getString("edit") == null ? "" : bundle.getString("edit");
                if (!edit_property.equals("")) {
                    Editid = bundle.getString("editId");
                    json_Data = (ArrayList<InventoryModel>) bundle.getSerializable("json_Data");
                    for (int i = 0; i < json_Data.size(); i++) {



                        if (json_Data.get(i).getId().equalsIgnoreCase(Editid)) {
                            otherExpensesArray.clear();

                            cam.setText(Utils.getEmptyValue(json_Data.get(i).getCam()));

                            for (int d=0 ; d < json_Data.get(i).getPropertyOtherExpences().size(); d++){

                                OtherExpensesModel otherExpensesModel3= new OtherExpensesModel();
                                otherExpensesModel3.setId(json_Data.get(i).getPropertyOtherExpences().get(d).getId());
                                otherExpensesModel3.setTitle(json_Data.get(i).getPropertyOtherExpences().get(d).getTitle());
                                otherExpensesModel3.setValue(json_Data.get(i).getPropertyOtherExpences().get(d).getValue());

                                //  2351
                                //1440
                                otherExpensesModel3.setLastPaidDate(json_Data.get(i).getPropertyOtherExpences().get(d).getLastPaidDate());
                                otherExpensesArray.add(otherExpensesModel3);

                                otherExpensesAdapter = new OtherExpensedAdapter(getActivity(),otherExpensesArray);

                                rv_otherexpenses.setAdapter(otherExpensesAdapter);
                            }

                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            propertyUnitsModels = (ArrayList<PropertyUnitsModel>) bundle.getSerializable("propertyunits");
            Log.e("propertyUnitsModels", "propertyUnitsModels" + propertyUnitsModels.size());


        }
        propertyOtherExpensesModel = new PropertyOtherExpensesModel();
        propertyOtherExpensesModel1 = new PropertyOtherExpensesModel();
        propertyOtherExpensesModel2 = new PropertyOtherExpensesModel();
        propertyOtherExpensesModel3 = new PropertyOtherExpensesModel();

/*
        linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(iskeyboaropen==1) {
                    hideSoftKeyboard(getActivity());
                    iskeyboaropen=0;
                }
            }
        });
*/

        linear_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = linear_layout.getRootView().getHeight() - linear_layout.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");

                    iskeyboaropen=1;
                } else {
                    Log.e("MyActivity", "keyboard closed");
                    iskeyboaropen=0;
                }
            }
        });

/*
        scroll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(getActivity());
            }
        });
*/


        next_other_expenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // if(isValid())
                    onOtherExpensesdata();
            }
        });
        return view;
    }

    private void blockChar() {

        cam.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(15)});

    }

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    private void onIntializeView(View view) {

        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);
        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);
        next_other_expenses = (ImageView) view.findViewById(R.id.next_other_expenses);
        cam = (EditText) view.findViewById(R.id.cam);
        rv_otherexpenses=(RecyclerView)view.findViewById(R.id.rv_otherexpenses);
        rv_otherexpenses.setLayoutManager(new LinearLayoutManager(getActivity()));
        addDataToOtherExpensesList();
    }

    private void addDataToOtherExpensesList() {

        OtherExpensesModel otherExpensesModel= new OtherExpensesModel();
        otherExpensesModel.setId("0");

        otherExpensesModel.setTitle("Muncipal House Tax");
        otherExpensesModel.setValue("0");
        otherExpensesModel.setLastPaidDate("0");
        otherExpensesModels.add(otherExpensesModel);

        OtherExpensesModel otherExpensesModel1= new OtherExpensesModel();
        otherExpensesModel1.setId("0");

        otherExpensesModel1.setTitle("Water Bill");
        otherExpensesModel1.setValue("0");
        otherExpensesModel1.setLastPaidDate("0");
        otherExpensesModels.add(otherExpensesModel1);

        OtherExpensesModel otherExpensesModel2= new OtherExpensesModel();
        otherExpensesModel2.setId("0");

        otherExpensesModel2.setTitle("Electricity Bill");
        otherExpensesModel2.setValue("0");
        otherExpensesModel2.setLastPaidDate("0");
        otherExpensesModels.add(otherExpensesModel2);

        OtherExpensesModel otherExpensesModel3= new OtherExpensesModel();
        otherExpensesModel3.setId("0");

        otherExpensesModel3.setTitle("Sewerage Bill");
        otherExpensesModel3.setValue("0");
        otherExpensesModel3.setLastPaidDate("0");
        otherExpensesModels.add(otherExpensesModel3);

        setOtherExpensesAdapter();

    }

    private void setOtherExpensesAdapter() {

        otherExpensesAdapter = new OtherExpensedAdapter(getActivity(),otherExpensesModels);

        rv_otherexpenses.setAdapter(otherExpensesAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onChangeTab(int position) {

    }

    public  void onEvent(ArrayList<OtherExpensesModel> otherExpensesModels){

        otherExpensesArray.clear();

        for (int i = 0; i < otherExpensesModels.size(); i++) {

            if (!otherExpensesModels.get(i).getTitle().equals("")) {

                OtherExpensesModel otherExpensesModel = new OtherExpensesModel();
                if (!otherExpensesModels.get(i).getTitle().equals("")) {

                    otherExpensesModel.setId(otherExpensesModels.get(i).getId());
                    otherExpensesModel.setTitle(otherExpensesModels.get(i).getTitle());
                    otherExpensesModel.setValue(otherExpensesModels.get(i).getValue());
                    otherExpensesModel.setLastPaidDate(otherExpensesModels.get(i).getLastPaidDate());

                    otherExpensesArray.add(otherExpensesModel);

                }

            }
        }

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }

    private void postProperty() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_POST,
                Constants.POST).execute();

    }

    private void postProperty_draft() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Constants.URL_PROPERTY_POST_DRAFT,
                Constants.POST).execute();

    }


    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = jsonSpecification.toString();
        } catch (Exception e) {

        }


        return inputStr;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
            ((PropertyItemActivity) getActivity()).save_draft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onOtherExpensesDraftdata();
                }
            });
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    public void onOtherExpensesdata() {

        try {

            // location frag data
            jsonSpecification = new JSONObject();
            if (!edit_property.equals("")) {

                jsonSpecification.put("Id", Editid);

            } else {

                jsonSpecification.put("Id", pid);

            }
            jsonSpecification.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("Cam", cam.getText().toString());
            jsonSpecification.put("IsDraft", 0);

            JSONArray otherExpensesJsonArray = new JSONArray();

            for (int i = 0; i < otherExpensesArray.size(); i++) {

                JSONObject bankJson = new JSONObject();
                bankJson.put("Id", otherExpensesArray.get(i).getId());
                bankJson.put("Title", otherExpensesArray.get(i).getTitle());
                bankJson.put("Value", otherExpensesArray.get(i).getValue());
                bankJson.put("LastPaidDate", otherExpensesArray.get(i).getLastPaidDate());
                bankJson.put("IsDeletable", false);
                bankJson.put("IsModify", true);
                otherExpensesJsonArray.put(bankJson);

            }

            jsonSpecification.put("PropertyOtherExpences", otherExpensesJsonArray);

            postProperty();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onOtherExpensesDraftdata() {

        try {

            // location frag data
            jsonSpecification = new JSONObject();
            // jsonSpecification.put("Id", pid);
            jsonSpecification.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonSpecification.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonSpecification.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonSpecification.put("IsDraft", 1);
            jsonSpecification.put("Cam", cam.getText().toString());

            if (!edit_property.equals("")) {

                jsonSpecification.put("Id", Editid);

            } else {

                jsonSpecification.put("Id", pid);

            }

            JSONArray jsonArrayLoc = new JSONArray();

            JSONArray otherExpensesArray = new JSONArray();

            for (int i = 0; i < PropertyOtherExpences.size(); i++) {

                JSONObject bankJson = new JSONObject();
                bankJson.put("Id", 0);
                bankJson.put("Title", PropertyOtherExpences.get(i).getTitle());
                bankJson.put("Value", PropertyOtherExpences.get(i).getValue());
                bankJson.put("LastPaidDate", PropertyOtherExpences.get(i).getLastPaidDate());

                otherExpensesArray.put(bankJson);
            }

            jsonSpecification.put("PropertyLocations", jsonArrayLoc);
            jsonSpecification.put("PropertySpecifications", jsonSpecific);
            jsonSpecification.put("PropertyOtherExpences", otherExpensesArray);

            postProperty_draft();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {
            Intent intent = new Intent(getActivity(), MyPropertyActivity.class);
            intent.putExtra("from_savedrafts", "from_savedrafts");

            startActivity(intent);
        }
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");
                            cam_string = jsonObject2.getString("Cam");
                            if (PropertyTypeId.equals("1") || PropertyTypeId.equalsIgnoreCase("7") || PropertyTypeId.equalsIgnoreCase("8"))

                                SM.sendData(pid, 4);
                            else {
                                SM.sendData(pid, 6);
                            }

                            JSONArray propJsonArray = jsonObject2.getJSONArray("PropertyOtherExpences");

                            String message = jsonObject1.getString("Message");

                            Toast.makeText(getActivity(), "Inventory updated successfully", Toast.LENGTH_SHORT).show();


                        } else {

                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.URL_PROPERTY_POST_DRAFT)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            String pid = jsonObject2.getString("Id");

                            Utils.showSuccessErrorMessage("Success", "Draft added successfully", "Ok", getActivity());


                        } else {
                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

//
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

    public boolean isValid(){

        int k=4;
        for(int i = 0; i< otherExpensesArray.size(); i++){

            if(!otherExpensesArray.get(i).getValue().equalsIgnoreCase("") && !otherExpensesArray.get(i).getLastPaidDate().equalsIgnoreCase("")){
                k++;
                //  return true;
            }
            else if(otherExpensesArray.get(i).getValue().equalsIgnoreCase("") && otherExpensesArray.get(i).getLastPaidDate().equalsIgnoreCase("")){
                k++;
                //return true;
            }

            else if(!otherExpensesArray.get(i).getValue().equalsIgnoreCase("") && otherExpensesArray.get(i).getLastPaidDate().equalsIgnoreCase("")){
                Utils.showErrorMessage("Please select the date corresponding to "+otherExpensesArray.get(i).getValue().toString(), getActivity());
                // return false;
                k--;
            }else if(otherExpensesArray.get(i).getValue().equalsIgnoreCase("") && !otherExpensesArray.get(i).getLastPaidDate().equalsIgnoreCase("")){
                Utils.showErrorMessage("Please select the value for " + otherExpensesArray.get(i).getValue().toString(), getActivity());
                //return false;
                k--;
            }
        }

        if(k==4 ){
            return true;
        }else if(k==8 ) {
            return true;
        }else {
            return false;
        }


    }

}
