package com.bnhabitat.ui.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.interfaces.SendMessage;
import com.bnhabitat.interfaces.TabChangeListener;
import com.bnhabitat.models.AccomDetailModel;
import com.bnhabitat.models.DialogModel;
import com.bnhabitat.models.FeatureModel;
import com.bnhabitat.models.FeatureParkModel;
import com.bnhabitat.models.FloorCalculationModel;
import com.bnhabitat.models.FloorNumberModel;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.ParkingModel;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.models.PropAreaModel;
import com.bnhabitat.models.PropBedroomModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.PropertyBedroomType;
import com.bnhabitat.models.PropertyUnitsModel;
import com.bnhabitat.models.SpecificationListModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.activities.MyPropertyActivity;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.ui.adapters.AddParkingAdapter;
import com.bnhabitat.ui.adapters.BalconyListAdapter;
import com.bnhabitat.ui.adapters.FeatureAddAdapter;
import com.bnhabitat.ui.adapters.RoomAttachedDataAdapter;
import com.bnhabitat.ui.adapters.RoomListAdapter;
import com.bnhabitat.ui.adapters.SpecificationAdapter;
import com.bnhabitat.ui.adapters.SpinnerAdapter;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.MultiTextWatcher;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utility;
import com.bnhabitat.utils.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

import static com.bnhabitat.ui.fragments.DetailFragment.accomDetailModel;
import static com.bnhabitat.ui.fragments.DetailFragment.imgModel;
import static com.bnhabitat.ui.fragments.DetailFragment.picsModel;
import static com.bnhabitat.ui.fragments.DetailFragment.propAreaModel;
import static com.bnhabitat.ui.fragments.LocationFragment.inventoryModelProp;

public class AccomodationResidentialPropertyFragment extends Fragment implements TabChangeListener, CommonAsync.OnAsyncResultListener {
    View view, floor_bedroom_view;
    String userChoosenTask;
    private static final int REQUEST_CAMERA_PERMISSION = 100;
    private static int REQUEST_CAMERA = 101;
    private static int SELECT_FILE = 102;
    String encodedImageData, built, PropertyId;
    ArrayList<PhotosModel> photosModels = new ArrayList<>();
    int id = 0;

    int index = 0;
    int insertIndex = 4;
    int iskeyboaropen=0;
    ArrayList<TextView> textViews = new ArrayList<>();
    FeatureAddAdapter featureAddAdapter;
    AddParkingAdapter addParkingAdapter;
    ArrayList<FloorCalculationModel> floorNumberList = new ArrayList<>();
    RadioButton gound_type_btn;
    String ground_type_txt, floor_txt = "", room_type_txt, select_unit_room_txt, select_unit_room_id, select_unit_txt, select_unit_id, room_id, status;
    int gound_type;
    Dialog dialog, dialog_feature;
    RadioGroup radio_group_type;

    LinearLayout ll_addmoreparking,ll_addmorefeature,floor_inflate, bedroom_inflate, room_detail_inflate, attached_layout, no_room_layout, room_layout, floor_lay, ground_type;
    TextView number, room_balcony_txt, add_more_feature, add_more_parking, add_more_floor, update_more_floor;
    View view_number;
    ImageView iv_close, add_floor, add_bedroom, iv_close_feature;
    String number_count;
    int floorcountPos;
    int number_bed_count = 0, position = 0;
    EditText room_name, length, width, room_size, edit_feature_txt, add_park, floor_size, floor_number;
    Spinner room_type, select_unit_room, select_unit;
    RecyclerView room_data, room_detail_list, rv_featurelist, rv_parking_list, balcony_detail_list, floor_number_list;
    SendMessage SM;
    LinearLayoutManager linearLayoutManager, linearLayoutManager1, balconyLayoutManager1, floor_numberLayoutManager1, floorNumberManager;
    RoomAttachedDataAdapter roomAttachedDataAdapter;
    RoomListAdapter roomListAdapter;
    BalconyListAdapter balconyListAdapter;
    FloornumberAdapter floornumberAdapter;
    ImageView next_btn;
    Button save, add_feature;
    int numberOfColumns = 3;
    int numberOfColumns_feature = 2;
    ArrayList<PropertyUnitsModel> propertyUnitsModels = new ArrayList<>();
    ArrayList<PropertyBedroomType> propertyBedroomTypes = new ArrayList<>();
    ArrayList<PropertyBedroomModel> propertyBedrooms = new ArrayList<>();
    ArrayList<String> checked_room = new ArrayList<>();
    SpinnerAdapter adapter, adapter1;
    ArrayList<String> property_units = new ArrayList<>();
    ArrayList<String> property_units_id = new ArrayList<>();
    ArrayList<String> no_room = new ArrayList<>();
    ArrayList<PropertyBedroomModel> room_list = new ArrayList<>();
    ArrayList<PropertyBedroomModel> balcony_list = new ArrayList<>();
    ArrayList<FeatureParkModel> featureParkModels = new ArrayList<>();
    String add_room = "";
    String category = "";
    RecyclerView rv_floor;
    Floor flooradapter;
    private boolean same = false;
    private String roomName;
    private JSONObject jsonLoc, jsonDet, jsonAccom, jsonBedroom;
    String pid, category_string= "";
    int floor_id = 0;
    public ArrayList<AccomDetailModel> accomDetailList = new ArrayList<>();
    public ArrayList<PropBedroomModel> propBedroomList = new ArrayList<>();
    String PropertyTypeId, edit_property = "", Editid;
    ArrayList<InventoryModel> json_Data;
    private LinearLayout ll_detail;
    private RecyclerView rv_floorlist;
    public static PropBedroomModel propBedroomModel;
    private String floorSizeUnitId, floorSizeText;
    private EditText et_floorno, et_floorsize;
    private int selectedFloorId = 0;
    Spinner spinner_select_floor;
    LinearLayout linear_layout,add_more, add_more_balcony,image_view_infate;
    NestedScrollView scroll_view;
    RadioButton radio_stilt,radio_accomodation;
    String ground_type_text  = "stilt";
    ArrayList<FeatureModel> featureModelArrayList = new ArrayList<>();
    ArrayList<FeatureModel> parkingModelArrayList = new ArrayList<>();
    String recent = " ";
    int editId= 0;
    LinearLayout upload_photos_detail ;

    public AccomodationResidentialPropertyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_accomodation_residential_property, container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        onIntializeview(view);

        Bundle bundle = this.getArguments();
        try {
            if (bundle != null) {
                PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");
                propertyUnitsModels = (ArrayList<PropertyUnitsModel>) bundle.getSerializable("propertyunits");
                for (int i = 0; i < propertyUnitsModels.size(); i++) {
                    property_units_id.add(propertyUnitsModels.get(i).getSizeUnitId());
                }

            }
            if (PropertyTypeId.equals("3")) {
                ground_type.setVisibility(View.GONE);
            } else {
                ground_type.setVisibility(View.VISIBLE);
            }

            try {
                edit_property = bundle.getString("edit") == null ? "" : bundle.getString("edit");
                if (!edit_property.equals("")) {
                    Editid = bundle.getString("editId");
                    json_Data = (ArrayList<InventoryModel>) bundle.getSerializable("json_Data");
                    PropertyTypeId = bundle.getString("PropertyTypeId") == null ? "" : bundle.getString("PropertyTypeId");

                    for (int i = 0; i < json_Data.size(); i++) {

                        if (json_Data.get(i).getId().equalsIgnoreCase(Editid)) {

                            // show edit data here

                            if ( json_Data.get(i).getGroundType().equalsIgnoreCase("accommodation")){
                                radio_accomodation.setChecked(true);
                                radio_stilt.setChecked(false);

                            }
                            else
                            {
                                radio_stilt.setChecked(true);
                                radio_accomodation.setChecked(false);

                            }

                            if (json_Data.get(i).getPropertyAccommodationDetailses().size()>0){

                                parkingModelArrayList.clear();
                                featureModelArrayList.clear();
                            }

                            else {
                                addDataToFeatureList();

                                addDataToParkingList();
                            }


                            // set feature list and parking list

                            for (int a=0;a<json_Data.get(i).getPropertyAccommodationDetailses().size();a++){

                                if (json_Data.get(i).getPropertyAccommodationDetailses().get(a).getType().equalsIgnoreCase("parking")){

                                    FeatureModel featureModel = new FeatureModel();
                                    featureModel.setId(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getId());
                                    featureModel.setPropertyId(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getPropertyId());
                                    featureModel.setName(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getName());
                                    featureModel.setType(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getType());
                                    featureModel.setTotalCount(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getTotalCount());

                                    parkingModelArrayList.add(featureModel);

                                }

                                else {

                                    FeatureModel featureModel = new FeatureModel();
                                    featureModel.setId(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getId());
                                    featureModel.setPropertyId(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getPropertyId());
                                    featureModel.setName(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getName());
                                    featureModel.setType(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getType());
                                    featureModel.setTotalCount(json_Data.get(i).getPropertyAccommodationDetailses().get(a).getTotalCount());

                                    featureModel.setStatus("checked");
                                    featureModelArrayList.add(featureModel);


                                }
                            }

                            setParkingAdapter();

                            setFeatureAdapter();
                        }

                    }


                }

                else {

                    addDataToFeatureList();

                    addDataToParkingList();

                }

                PropertyId = bundle.getString("PropertyId") == null ? "" : bundle.getString("PropertyId");

            } catch (Exception e) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager1 = new LinearLayoutManager(getActivity());
        balconyLayoutManager1 = new LinearLayoutManager(getActivity());
        floorNumberManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));
        floor_numberLayoutManager1 = new LinearLayoutManager(getActivity());
        room_detail_list.setLayoutManager(linearLayoutManager);
        balcony_detail_list.setLayoutManager(balconyLayoutManager1);
        rv_floor.setLayoutManager(floorNumberManager);
        floor_number_list.setLayoutManager(floor_numberLayoutManager1);
        rv_featurelist.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns_feature));

        // setFeature data


        rv_parking_list.setLayoutManager(linearLayoutManager1);

        spinner_select_floor.setAdapter(adapter1);

        getFloorList();

        getBedroomList();
        getBalconyList();

/*
        linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(iskeyboaropen==1) {
                    hideSoftKeyboard(getActivity());
                    iskeyboaropen=0;
                }
            }
        });
*/

        linear_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = linear_layout.getRootView().getHeight() - linear_layout.getHeight();

                if (heightDiff > 100) {
                    Log.e("MyActivity", "keyboard opened");

                    iskeyboaropen=1;
                } else {
                    Log.e("MyActivity", "keyboard closed");
                    iskeyboaropen=0;
                }
            }
        });

/*
        scroll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(getActivity());
            }
        });
*/

        spinner_select_floor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (spinner_select_floor.getSelectedItem() == getActivity().getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    floorSizeText = spinner_select_floor.getSelectedItem().toString();
                    floorSizeUnitId = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        ll_addmoreparking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (add_park.getText().toString().trim().equals("")) {
                    Utils.showErrorMessage("Please enter parking label", getActivity());
                } else {
                    FeatureModel featureModel = new FeatureModel();
                    featureModel.setName(add_park.getText().toString());
                    featureModel.setId("0");
                    featureModel.setTotalCount("0");
                    featureModel.setType("parking");
                    parkingModelArrayList.add(insertIndex,featureModel);
//                    addParkingAdapter.notifyItemInserted(insertIndex);
                    setParkingAdapter();
                    insertIndex++;
                    add_park.setText("");
                }

            }
        });

        ll_addmorefeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number_bed_count = number_bed_count + 1;
                add_feature();

            }
        });

        add_more_floor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (et_floorno.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please add floor number", Toast.LENGTH_SHORT).show();
                } else if (et_floorsize.getText().toString().equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "Please add floor Size", Toast.LENGTH_SHORT).show();

                } else {
                    addFloor();
                }


            }
        });
        update_more_floor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateFloor();

            }
        });

        add_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (floor_txt == null) {
                    floor_txt = "";
                }
                radio_click();

                add_room = "add";
                category = "room";
                roomDetaildialog(0);

            }
        });

        add_more_balcony.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (floor_txt == null) {
                    floor_txt = "";
                }
                radio_click();

                add_room = "add";
                category = "balcony";
                balconyDetaildialog(0);

            }
        });

        upload_photos_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDrawerPermision();
            }
        });


        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (radio_accomodation.isChecked())
                    ground_type_text = "accommodation";
                else
                    ground_type_text = "stilt";

              /*  if(floorNumberList.size()==0){

                    Toast.makeText(getActivity(), "Please add floor", Toast.LENGTH_SHORT).show();

                }
                else if(room_list.size()==0){

                    Toast.makeText(getActivity(), "Please add room by clicking on floor number.", Toast.LENGTH_SHORT).show();
                }
                else{

                    onPropertyBedroomdata();
                }*/

                onPropertyBedroomdata();

            }
        });

        getBedroomType();

        setList();
        return view;

    }

    private void checkDrawerPermision() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);

        } else {

            selectImage();

        }

    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";

                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();

                } else {

                }
                break;
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                encodedImageData = getEncoded64ImageStringFromBitmap(bm);
                postPropertyImages();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ivImage.setImageBitmap(bm);
    }

    private void postPropertyImages() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_PHOTOS,
                photoInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_PHOTOS,
                Constants.POST).execute();

    }

    private String photoInput() {
        String inputStr = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject2 = new JSONObject();
        try {
            jsonObject2.put("CompanyId", "1");
            jsonObject2.put("Name", new Date().getTime());
            jsonObject2.put("Type", "png");
            jsonObject2.put("ImageCode", encodedImageData);
            jsonArray.put(jsonObject2);
        } catch (Exception e) {

        }
        inputStr = jsonArray.toString();


        return inputStr;
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        //  File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        File destination = new File(getActivity().getFilesDir().getPath(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encodedImageData = getEncoded64ImageStringFromBitmap(thumbnail);
        postPropertyImages();
//        ivImage.setImageBitmap(thumbnail);
    }

    private void addDataToFeatureList() {

        FeatureModel featureModel = new FeatureModel();
        featureModel.setId("0");
        featureModel.setPropertyId(pid);
        featureModel.setName("Vastu Compliance");
        featureModel.setType("feature");
        featureModel.setTotalCount("0");
        featureModel.setStatus("unchecked");
        featureModelArrayList.add(featureModel);

        FeatureModel featureModel1 = new FeatureModel();
        featureModel1.setId("0");
        featureModel.setPropertyId(pid);
        featureModel1.setName("Power Backup");
        featureModel1.setType("feature");
        featureModel1.setTotalCount("0");
        featureModel1.setStatus("unchecked");
        featureModelArrayList.add(featureModel1);

        FeatureModel featureModel2 = new FeatureModel();
        featureModel2.setId("0");
        featureModel.setPropertyId(pid);
        featureModel2.setName("Center AC");
        featureModel2.setType("feature");
        featureModel2.setTotalCount("0");
        featureModel2.setStatus("unchecked");
        featureModelArrayList.add(featureModel2);

        FeatureModel featureModel3 = new FeatureModel();
        featureModel3.setId("0");
        featureModel3.setPropertyId(pid);
        featureModel3.setName("Swimming Pool");
        featureModel3.setType("feature");
        featureModel3.setTotalCount("0");
        featureModel3.setStatus("unchecked");
        featureModelArrayList.add(featureModel3);

        setFeatureAdapter();
    }

    private void addDataToParkingList() {

        FeatureModel featureModel = new FeatureModel();
        featureModel.setId("0");
        featureModel.setPropertyId(pid);
        featureModel.setName("Stilt");
        featureModel.setType("parking");
        featureModel.setTotalCount("0");

        parkingModelArrayList.add(featureModel);

        FeatureModel featureModel1 = new FeatureModel();
        featureModel1.setId("0");
        featureModel1.setPropertyId(pid);
        featureModel1.setName("Cover");
        featureModel1.setType("parking");
        featureModel1.setTotalCount("0");

        parkingModelArrayList.add(featureModel1);

        FeatureModel featureModel2 = new FeatureModel();
        featureModel2.setId("0");
        featureModel2.setPropertyId(pid);
        featureModel2.setName("Basement");
        featureModel2.setType("parking");
        featureModel2.setTotalCount("0");

        parkingModelArrayList.add(featureModel2);

        FeatureModel featureModel3 = new FeatureModel();
        featureModel3.setId("0");
        featureModel3.setPropertyId(pid);
        featureModel3.setName("Open");
        featureModel3.setType("parking");
        featureModel3.setTotalCount("0");

        parkingModelArrayList.add(featureModel3);

        setParkingAdapter();
    }

    private void setFeatureAdapter() {

        featureAddAdapter = new FeatureAddAdapter(getActivity(), featureModelArrayList);

        rv_featurelist.setAdapter(featureAddAdapter);

    }

    private void setParkingAdapter() {

        addParkingAdapter = new AddParkingAdapter(getActivity(), parkingModelArrayList);

        rv_parking_list.setAdapter(addParkingAdapter);

    }

    public void radio_click() {
        try {


            gound_type = radio_group_type.getCheckedRadioButtonId();
            gound_type_btn = (RadioButton) view.findViewById(gound_type);
//                Toast.makeText(getActivity(),gound_type_btn.getText(),Toast.LENGTH_SHORT).show();

            if (gound_type_btn.getText().equals("Stilt")) {
                ground_type_txt = "Stilt";
            } else {
                ground_type_txt = "Accomodation";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
            ((PropertyItemActivity) getActivity()).save_draft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (room_list.isEmpty()) {
                        Utils.showErrorMessage("Please add Room Detail", getActivity());
                    } else {
                        try {

                            onPropertyDraftBedroomdata();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void onIntializeview(View view) {
        linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);
        scroll_view = (NestedScrollView) view.findViewById(R.id.scroll_view);
        radio_group_type = (RadioGroup) view.findViewById(R.id.radio_group_type);
        room_detail_list = (RecyclerView) view.findViewById(R.id.room_detail_list);
        rv_featurelist = (RecyclerView) view.findViewById(R.id.feature_list);
        rv_parking_list = (RecyclerView) view.findViewById(R.id.parking_list);
        balcony_detail_list = (RecyclerView) view.findViewById(R.id.balcony_detail_list);
        rv_floor = (RecyclerView) view.findViewById(R.id.rv_floor);
        floor_number_list = (RecyclerView) view.findViewById(R.id.floor_number_list);
        add_floor = (ImageView) view.findViewById(R.id.add_floor);
        add_bedroom = (ImageView) view.findViewById(R.id.add_bedroom);
        add_more = (LinearLayout) view.findViewById(R.id.add_more);
        add_more_balcony = (LinearLayout) view.findViewById(R.id.add_more_balcony);
        add_more_floor = (TextView) view.findViewById(R.id.add_more_floor);
        update_more_floor = (TextView) view.findViewById(R.id.update_more_floor);
        add_park = (EditText) view.findViewById(R.id.add_park);
        ground_type = (LinearLayout) view.findViewById(R.id.ground_type);
        ll_addmorefeature = (LinearLayout) view.findViewById(R.id.ll_addmorefeature);
        ll_addmoreparking = (LinearLayout) view.findViewById(R.id.ll_addmoreparking);
        upload_photos_detail = (LinearLayout) view.findViewById(R.id.upload_photos_detail);
        ll_detail = (LinearLayout) view.findViewById(R.id.ll_detail);
        add_more_feature = (TextView) view.findViewById(R.id.add_more_feature);
        add_more_parking = (TextView) view.findViewById(R.id.add_more_parking);
        next_btn = (ImageView) view.findViewById(R.id.next_btn);
        pid = LocationFragment.pid;
        rv_floorlist = (RecyclerView) view.findViewById(R.id.rv_floorlist);
        spinner_select_floor = (Spinner) view.findViewById(R.id.spinner_select_floor);
        et_floorno = (EditText) view.findViewById(R.id.et_floorno);
        et_floorsize = (EditText) view.findViewById(R.id.et_floorsize);
        radio_stilt = (RadioButton) view.findViewById(R.id.radio_stilt);
        radio_accomodation = (RadioButton) view.findViewById(R.id.radio_accomodation);
        image_view_infate = (LinearLayout) view.findViewById(R.id.image_view_infate);

    }

    @Override
    public void onChangeTab(int position) {

    }

    @Override
    public void onChangeTab(ArrayList<String> contacts) {

    }


    public void onEvent(FloorCalculationModel floorNumberModel) {

        if (floorNumberModel.getDelete().equalsIgnoreCase("Yes")) {

            floorNumberList.remove(floorNumberModel.getDeletePos());

            floornumberAdapter = new FloornumberAdapter(getActivity(), floorNumberList);
            floor_number_list.setAdapter(floornumberAdapter);
            flooradapter = new Floor(floorNumberList);
            rv_floor.setAdapter(flooradapter);

        } else {
            number_count = floorNumberModel.getCount();
            floorcountPos = floorNumberModel.getPosition();
            floorNumberList.set(floorcountPos, floorNumberModel);

            flooradapter.notifyDataSetChanged();
        }

     }

    public  void onEvent(String str){

        category_string= str;

    }

    public  void onEvent(ArrayList<FeatureModel> featureModels){


        if (category_string.equalsIgnoreCase("feature"))
        {
          //  featureModelArrayList.clear();
            this.featureModelArrayList=featureModels;


        }
        else
        {

          //  parkingModelArrayList.clear();
            this.parkingModelArrayList= featureModels;

        }



/*
         for (int i = 0; i < featureModels.size(); i++) {

             if (featureModels.get(i).getType().equals("feature")) {

                 if (!featureModels.get(i).getName().equals("")) {

                     FeatureModel featureModel = new FeatureModel();
                     if (!featureModels.get(i).getName().equals("")) {

                             featureModel.setId(featureModels.get(i).getId());
                             featureModel.setPropertyId(featureModels.get(i).getPropertyId());
                             featureModel.setName(featureModels.get(i).getName());
                             featureModel.setType(featureModels.get(i).getType());
                             featureModel.setTotalCount(featureModels.get(i).getTotalCount());

                         featureModelArrayList.add(featureModel);

                     }

                 }
             }

             if (featureModels.get(i).getType().equals("parking")) {

                 if (!featureModels.get(i).getName().equals("")) {

                     FeatureModel featureModel = new FeatureModel();
                     if (!featureModels.get(i).getName().equals("")) {
                         featureModel.setId(featureModels.get(i).getId());
                         featureModel.setPropertyId(featureModels.get(i).getPropertyId());
                         featureModel.setName(featureModels.get(i).getName());
                         featureModel.setType(featureModels.get(i).getType());
                         featureModel.setTotalCount(featureModels.get(i).getTotalCount());
                         parkingModelArrayList.add(featureModel);

                     }
                 }
             }

         }
*/

     }

    public void onEvent(StatusModel statusModel) {

        if (statusModel.getStatus().equalsIgnoreCase("delete")) {
            status = statusModel.getStatus();
            room_id = statusModel.getId();
            deleteRoom();

        } else if (statusModel.getStatus().equalsIgnoreCase("balcony_delete")) {
            status = statusModel.getStatus();
            room_id = statusModel.getId();
            deleteRoom1();
        }  else if (statusModel.getStatus().equalsIgnoreCase("checked")) {
            checked_room = statusModel.getIds();


        }

/*
        else {
            add_room = "";
            category = "room";
            room_id = statusModel.getId();
            // roomDetaildialog();
            roomDetaildialog(statusModel.getPos());
            room_name.setText(room_list.get(statusModel.getPos()).getRoom_name());
            room_size.setText(room_list.get(statusModel.getPos()).getRoom_size());
            length.setText(room_list.get(statusModel.getPos()).getLength());
            width.setText(room_list.get(statusModel.getPos()).getWidth());
            String room_type_str = room_list.get(statusModel.getPos()).getRoom_type();
            String room_size_str = room_list.get(statusModel.getPos()).getSizeUnit();
            if (room_size_str != null) {
                int spinnerPos = adapter1.getPosition(room_size_str);

                select_unit.setSelection(spinnerPos);
            }
            if (room_type_str != null) {
                int spinnerPos = adapter.getPosition(room_type_str);

                room_type.setSelection(spinnerPos);
            }

        }
*/

        else if (statusModel.getStatus().equalsIgnoreCase("edit")) {

            recent= "edit";
            add_room = "";
            category = "room";
            room_id = statusModel.getId();
            roomDetaildialog(statusModel.getPos());
            room_name.setText(room_list.get(statusModel.getPos()).getRoom_name());
            room_size.setText(room_list.get(statusModel.getPos()).getRoom_size());
            length.setText(room_list.get(statusModel.getPos()).getLength());
            width.setText(room_list.get(statusModel.getPos()).getWidth());
            String room_type_str = room_list.get(statusModel.getPos()).getRoom_type();
            String room_size_str = room_list.get(statusModel.getPos()).getSizeUnit();
            if (room_size_str != null) {
                int spinnerPos = adapter1.getPosition(room_size_str);

                select_unit.setSelection(spinnerPos);
                select_unit_room.setSelection(spinnerPos);
            }
            if (room_type_str != null) {
                int spinnerPos = adapter.getPosition(room_type_str);

                room_type.setSelection(spinnerPos);
            }

        } else {
            {
                add_room = "";
                category = "balcony";
                room_id = statusModel.getId();
                balconyDetaildialog(statusModel.getPos());
                room_name.setText(balcony_list.get(statusModel.getPos()).getRoom_name());
                room_size.setText(balcony_list.get(statusModel.getPos()).getRoom_size());
                length.setText(balcony_list.get(statusModel.getPos()).getLength());
                width.setText(balcony_list.get(statusModel.getPos()).getWidth());
                String room_type_str = balcony_list.get(statusModel.getPos()).getRoom_type();
                String room_size_str = balcony_list.get(statusModel.getPos()).getSizeUnit();
                if (room_size_str != null) {
                    int spinnerPos = adapter1.getPosition(room_size_str);

                    select_unit.setSelection(spinnerPos);
                    select_unit_room.setSelection(spinnerPos);
                }
                if (room_type_str != null) {
                    int spinnerPos = adapter.getPosition(room_type_str);

                    room_type.setSelection(spinnerPos);
                }

            }
        }
    }
    private void imageinflateview() {
        image_view_infate.removeAllViews();

        for (int i = 0; i < photosModels.size(); i++) {

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.image_layout_inflate, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            final ImageView cross = (ImageView) view.findViewById(R.id.cross);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), imageView, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());
            imageView.setTag(i);
            cross.setId(i);


            cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   id=(Integer)view.getTag();

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getActivity().getString(R.string.message))
                            .setContentText(getActivity().getString(R.string.are_you_sure))
                            .setConfirmText(getActivity().getString(R.string.yes))
                            .setCancelText(getActivity().getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    id = cross.getId();
                                    deletePropertyImages(photosModels.get(id).getId());
                                    photosModels.remove(id);
                                    imageinflateview();
                                }
                            })
                            .show();
                }
            });

            image_view_infate.addView(view);

        }

    }
    private void deletePropertyImages(String position) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REMOVE_PROPERTY_PHOTOS,
                deletephotoInput(position),
                "Loading..",
                this,
                Urls.URL_REMOVE_PROPERTY_PHOTOS,
                Constants.POST).execute();

    }

    private String deletephotoInput(String id) {
        String inputStr = "";
        JSONArray jsondeleteArray = new JSONArray();
        JSONObject jsonObject2 = new JSONObject();
        try {
            jsonObject2.put("Id", id);

            jsondeleteArray.put(jsonObject2);
        } catch (Exception e) {

        }
        inputStr = jsondeleteArray.toString();


        return inputStr;
    }

    private void deleteRoom1() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_DELETE_BEDROOM,
                deleteInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_DELETE_BEDROOM + "/balcony",
                Constants.POST).execute();


    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {


            if (which.equalsIgnoreCase(Urls.URL_PROPERTY_PHOTOS)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {


                        JSONObject jsonObject_photo = jsonObject.getJSONObject("Result");
                        JSONArray propertyPhotos = jsonObject_photo.getJSONArray("Result");

                        if (propertyPhotos.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < propertyPhotos.length(); index++) {
                                JSONObject jsonObject1 = propertyPhotos.getJSONObject(index);

                                PhotosModel photosModel = new PhotosModel();

                                photosModel.setId(jsonObject1.getString("Id"));
                                photosModel.setName(jsonObject1.getString("Name"));
                                photosModel.setImageUrl(jsonObject1.getString("Url"));
                                photosModels.add(photosModel);

                            }
                            imageinflateview();

                        }
                    }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_BEDROOM_TYPES)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {


                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                PropertyBedroomType propertyBedroomType = new PropertyBedroomType();

//                              propertyUnitsModel.setId(jsonObject1.getString("Id"));
                                propertyBedroomType.setName(jsonObject1.getString("Name"));
                                propertyBedroomTypes.add(propertyBedroomType);
                                property_units.add(propertyBedroomTypes.get(index).getName());

                            }


                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_ADD_BEDROOM)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject addJson = jsonObject.getJSONObject("Result");

                        boolean success = addJson.getBoolean("Success");
                        if (success) {
                            String message = addJson.getString("Message");
                            if (category.equalsIgnoreCase("room")) {
                                //   Toast.makeText(getActivity(), "Bedroom is added successfully.", Toast.LENGTH_SHORT).show();

                                if (!(room_type_txt.equalsIgnoreCase(null) && room_type_txt.isEmpty()))
                                    Toast.makeText(getActivity(), room_type_txt + " " + "is added successfully.", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(getActivity(), "Room is added successfully.", Toast.LENGTH_SHORT).show();

                                getBedroomList();
                            } else {
                                Toast.makeText(getActivity(), "Balcony is added successfully.", Toast.LENGTH_SHORT).show();

//                              Utils.showSuccessMessage("Success", "Balcony is added successfully.", "Ok", getActivity());
                                getBalconyList();
                                setList();

                            }


                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_POST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            //  String pid = jsonObject2.getString("Id");

                            Toast.makeText(getActivity(), "Inventory updated successfully", Toast.LENGTH_SHORT).show();

                            SM.sendData(pid, 3);

                            JSONArray accomDet = jsonObject2.getJSONArray("PropertyAccommodationDetails");

                            for (int i = 0; i < accomDet.length(); i++) {

                                accomDetailModel = new AccomDetailModel();
                                JSONObject dataObj = accomDet.getJSONObject(i);

                                accomDetailModel.setId(dataObj.getInt("Id"));
                                accomDetailModel.setName(dataObj.getString("Name"));
                                accomDetailModel.setType(dataObj.getString("Type"));
                                accomDetailModel.setDeletable(dataObj.getBoolean("IsDeletable"));

                                accomDetailList.add(accomDetailModel);

                            }

                           // propAreaModel.setAccomDetailModelsList(accomDetailList);

                            JSONArray propBed = jsonObject2.getJSONArray("PropertyBedrooms");

                            for (int i = 0; i < propBed.length(); i++) {

                                propBedroomModel = new PropBedroomModel();
                                JSONObject dataObj = propBed.getJSONObject(i);

                                propBedroomModel.setId(dataObj.getInt("Id"));
                                propBedroomModel.setPropertyId(dataObj.getInt("PropertyId"));
                                propBedroomModel.setGroundType(dataObj.getString("GroundType"));
                                propBedroomModel.setFloorNo(dataObj.getString("FloorNo"));
                                propBedroomModel.setBedRoomName(dataObj.getString("BedRoomName"));
                                propBedroomModel.setBedRoomType(dataObj.getString("BedRoomType"));
                                propBedroomModel.setBedRoomSize(dataObj.getString("BedRoomSize"));
                                propBedroomModel.setBedRoomSizeUnitId(dataObj.getInt("BedRoomSizeUnitId"));
                                propBedroomModel.setLength(dataObj.getInt("Length"));
                                propBedroomModel.setWidth(dataObj.getInt("Width"));
                                propBedroomModel.setLengthWidthUnitId(dataObj.getInt("LengthWidthUnitId"));
                                propBedroomModel.setCategory(dataObj.getString("Category"));
                                propBedroomModel.setDeletable(dataObj.getBoolean("IsDeletable"));
                                propBedroomModel.setModify(dataObj.getBoolean("IsModify"));

                                propBedroomList.add(propBedroomModel);

                            }

                         //   propAreaModel.setPropBedList(propBedroomList);

                        } else {
                           // SM.sendData(pid, 3);


                          Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_REMOVE_FLOOR)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            getFloorList();

                        } else {

                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_FLOOR)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            getFloorList();

                        } else {

                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Constants.URL_PROPERTY_POST_DRAFT)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("Result");
                        String success = jsonObject1.getString("Success");
                        if (success.equals("true")) {

                            JSONObject jsonObject2 = jsonObject1.getJSONObject("Result");
                            //    String pid = jsonObject2.getString("Id");

                            //    Toast.makeText(getActivity(), "Draft added successfully", Toast.LENGTH_SHORT).show();
                            Utils.showSuccessErrorMessage("Success", "Draft added successfully", "Ok", getActivity());

                            //  SM.sendData(pid, 3);

                            JSONArray accomDet = jsonObject2.getJSONArray("PropertyAccommodationDetails");

                            for (int i = 0; i < accomDet.length(); i++) {

                                accomDetailModel = new AccomDetailModel();
                                JSONObject dataObj = accomDet.getJSONObject(i);
                                accomDetailModel.setId(dataObj.getInt("Id"));
                                accomDetailModel.setName(dataObj.getString("Name"));
                                accomDetailModel.setType(dataObj.getString("Type"));
                                accomDetailModel.setDeletable(dataObj.getBoolean("IsDeletable"));

                                accomDetailList.add(accomDetailModel);

                            }

                        //    propAreaModel.setAccomDetailModelsList(accomDetailList);

                            JSONArray propBed = jsonObject2.getJSONArray("PropertyBedrooms");

                            for (int i = 0; i < propBed.length(); i++) {

                                propBedroomModel = new PropBedroomModel();
                                JSONObject dataObj = propBed.getJSONObject(i);

                                propBedroomModel.setId(dataObj.getInt("Id"));
                                propBedroomModel.setPropertyId(dataObj.getInt("PropertyId"));
                                propBedroomModel.setGroundType(dataObj.getString("GroundType"));
                                propBedroomModel.setFloorNo(dataObj.getString("FloorNo"));
                                propBedroomModel.setBedRoomName(Utils.getFilteredValue(dataObj.getString("BedRoomName")));
                                propBedroomModel.setBedRoomType(dataObj.getString("BedRoomType"));
                                propBedroomModel.setBedRoomSize(dataObj.getString("BedRoomSize"));
                                propBedroomModel.setBedRoomSizeUnitId(Utils.getFilteredValueInt(dataObj.getInt("BedRoomSizeUnitId")));
                                propBedroomModel.setLength(dataObj.getInt("Length"));
                                propBedroomModel.setWidth(dataObj.getInt("Width"));
                                propBedroomModel.setLengthWidthUnitId(dataObj.getInt("LengthWidthUnitId"));
                                propBedroomModel.setCategory(dataObj.getString("Category"));
                                propBedroomModel.setDeletable(dataObj.getBoolean("IsDeletable"));
                                propBedroomModel.setModify(dataObj.getBoolean("IsModify"));

                                propBedroomList.add(propBedroomModel);

                            }

                        //    propAreaModel.setPropBedList(propBedroomList);

                        } else {

                            Utils.showWarningErrorMessage("Warning", "Something went wrong", "Ok", getActivity());
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_ADD_BEDROOM + "/edit")) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject addJson = jsonObject.getJSONObject("Result");

                        boolean success = addJson.getBoolean("Success");
                        if (success) {
                            String message = addJson.getString("Message");
                            if (category.equalsIgnoreCase("room")) {
                                Toast.makeText(getActivity(), "Room is updated successfully.", Toast.LENGTH_SHORT).show();
//                                Utils.showSuccessMessage("Success", "Bedroom is added successfully.", "Ok", getActivity());
                                getBedroomList();
                            } else {
                                Toast.makeText(getActivity(), "Balcony is updated successfully.", Toast.LENGTH_SHORT).show();

//                                Utils.showSuccessMessage("Success", "Balcony is added successfully.", "Ok", getActivity());
                                getBalconyList();
                                setList();

                            }


                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_DELETE_BEDROOM)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject addJson = jsonObject.getJSONObject("Result");

                        boolean success = addJson.getBoolean("Success");
                        if (success) {
                            String message = addJson.getString("Message");
                            Utils.showSuccessMessage("Success", message, "Ok", getActivity());
                            getBedroomList();

                        }


                    }

//                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_DELETE_BEDROOM + "/balcony")) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONObject addJson = jsonObject.getJSONObject("Result");

                        boolean success = addJson.getBoolean("Success");
                        if (success) {
                            String message = addJson.getString("Message");
                            Utils.showSuccessMessage("Success", "Balcony removed successfully", "Ok", getActivity());
                            getBalconyList();
                            setList();

                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.FLOOR_LIST)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray result1 = jsonObject.getJSONArray("Result");
                        floorNumberList.clear();
                        for (int i = 0; i < result1.length(); i++) {
                            JSONObject jsonObject2 = result1.getJSONObject(i);

                            final FloorCalculationModel floorCalculationModel = new FloorCalculationModel();
                            floorCalculationModel.setId((Integer) jsonObject2.get("Id"));
                            floorCalculationModel.setFloorNumber(jsonObject2.getInt("FloorNumber"));
                            floorCalculationModel.setFloorSize(jsonObject2.getDouble("FloorSize"));
                            floorCalculationModel.setFloorSizeUnitId(jsonObject2.getInt("FloorSizeUnitId"));
                            floorNumberList.add(floorCalculationModel);
                        }


                        floornumberAdapter = new FloornumberAdapter(getActivity(), floorNumberList);
                        floor_number_list.setAdapter(floornumberAdapter);
                        flooradapter = new Floor(floorNumberList);
                        rv_floor.setAdapter(flooradapter);
                        floor_number_list.setVisibility(View.VISIBLE);
                        update_more_floor.setVisibility(View.GONE);
                        et_floorsize.setText("");
                        et_floorno.setText("");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_BEDROOM_ALL)) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {
                            room_list.clear();
//                            Toast.makeText(getActivity(), "No result found!!!", Toast.LENGTH_LONG).show();
                        } else {

                            room_list.clear();
                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();

                                propertyBedroomModel.setId(jsonObject1.getString("Id"));
                                propertyBedroomModel.setFloor(jsonObject1.getString("FloorNo"));
                                propertyBedroomModel.setRoom_name(jsonObject1.getString("BedRoomName"));
                                propertyBedroomModel.setGoundtype(jsonObject1.getString("GroundType"));
                                propertyBedroomModel.setRoom_type(jsonObject1.getString("BedRoomType"));
                                propertyBedroomModel.setRoom_size(jsonObject1.getString("BedRoomSize"));
                                propertyBedroomModel.setLength(jsonObject1.getString("Length"));
                                propertyBedroomModel.setWidth(jsonObject1.getString("Width"));
                                propertyBedroomModel.setLengthWidthUnitId(jsonObject1.getString("LengthWidthUnitId"));
                                propertyBedroomModel.setCategory(jsonObject1.getString("Category"));

                                try {
                                    JSONObject jsonObject11 = jsonObject1.getJSONObject("PropertySizeUnit");
                                    propertyBedroomModel.setSizeUnit(jsonObject11.getString("SizeUnit"));

                                } catch (Exception e) {

                                }
                                JSONArray jsonArray = jsonObject1.getJSONArray("PropertyBedroomAttachWiths");
                                ArrayList<PropertyBedroomModel.AttachPropertyBedroomId> attachPropertyBedroomIds = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    PropertyBedroomModel.AttachPropertyBedroomId attachPropertyBedroomId = propertyBedroomModel.new AttachPropertyBedroomId();

                                    attachPropertyBedroomId.setAttachPropertyBedroomId(object.getString("AttachPropertyBedroomId"));
                                    attachPropertyBedroomId.setAttachPropertyBedroomName(object.getString("AttachPropertyBedroomName"));
                                    attachPropertyBedroomIds.add(attachPropertyBedroomId);

                                }
                                propertyBedroomModel.setAttachPropertyBedroomIds(attachPropertyBedroomIds);

                                room_list.add(propertyBedroomModel);

                                roomListAdapter = new RoomListAdapter(getActivity(), room_list);
                                room_detail_list.setAdapter(roomListAdapter);

                            }

                        }

                        roomListAdapter = new RoomListAdapter(getActivity(), room_list);
                        room_detail_list.setAdapter(roomListAdapter);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (which.equalsIgnoreCase(Urls.URL_PROPERTY_BEDROOM_ALL + "/balcony")) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (jsonObject.getInt("StatusCode") == 200) {

                        JSONArray propertySize = jsonObject.getJSONArray("Result");

                        if (propertySize.length() == 0) {
                            balcony_list.clear();
                        } else {

                            balcony_list.clear();
                            for (int index = 0; index < propertySize.length(); index++) {
                                JSONObject jsonObject1 = propertySize.getJSONObject(index);

                                PropertyBedroomModel propertyBedroomModel = new PropertyBedroomModel();

                                propertyBedroomModel.setId(jsonObject1.getString("Id"));
                                propertyBedroomModel.setFloor(jsonObject1.getString("FloorNo"));
                                propertyBedroomModel.setRoom_name(jsonObject1.getString("BedRoomName"));
                                propertyBedroomModel.setGoundtype(jsonObject1.getString("GroundType"));
                                propertyBedroomModel.setRoom_type(jsonObject1.getString("BedRoomType"));
                                propertyBedroomModel.setRoom_size(jsonObject1.getString("BedRoomSize"));
                                propertyBedroomModel.setLength(jsonObject1.getString("Length"));
                                propertyBedroomModel.setWidth(jsonObject1.getString("Width"));
                                propertyBedroomModel.setLengthWidthUnitId(jsonObject1.getString("LengthWidthUnitId"));
                                propertyBedroomModel.setCategory(jsonObject1.getString("Category"));

                                try {
                                    JSONObject jsonObject11 = jsonObject1.getJSONObject("PropertySizeUnit");
                                    propertyBedroomModel.setSizeUnit(jsonObject11.getString("SizeUnit"));

                                } catch (Exception e) {

                                }

                                JSONArray jsonArray = jsonObject1.getJSONArray("PropertyBedroomAttachWiths");
                                ArrayList<PropertyBedroomModel.AttachPropertyBedroomId> attachPropertyBedroomIds = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    PropertyBedroomModel.AttachPropertyBedroomId attachPropertyBedroomId = propertyBedroomModel.new AttachPropertyBedroomId();

                                    attachPropertyBedroomId.setAttachPropertyBedroomId(object.getString("AttachPropertyBedroomId"));
                                    attachPropertyBedroomId.setAttachPropertyBedroomName(object.getString("AttachPropertyBedroomName"));
                                    attachPropertyBedroomIds.add(attachPropertyBedroomId);

                                }
                                propertyBedroomModel.setAttachPropertyBedroomIds(attachPropertyBedroomIds);

                                balcony_list.add(propertyBedroomModel);


//                                roomDetailview();
//                                property_units.add(propertyUnitsModels.get(index).getSizeUnitId());

                            }
                            balconyListAdapter = new BalconyListAdapter(getActivity(), balcony_list);
                            balcony_detail_list.setAdapter(balconyListAdapter);
                            setList();

//                          adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
//                          adapter.addAll(property_units);


                        }
                        balconyListAdapter = new BalconyListAdapter(getActivity(), balcony_list);
                        balcony_detail_list.setAdapter(balconyListAdapter);
                        setList();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    private void getBedroomType() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_BEDROOM_TYPES,
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_BEDROOM_TYPES,
                Constants.GET).execute();


    }

    private void postAddRoom() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ADD_BEDROOM,
                getAddinput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_ADD_BEDROOM,
                Constants.POST).execute();

    }

    private void editRoom() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_ADD_BEDROOM,
                getEditinput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_ADD_BEDROOM + "/edit",
                Constants.POST).execute();

    }

    private void deleteRoom() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_DELETE_BEDROOM,
                deleteInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_DELETE_BEDROOM,
                Constants.POST).execute();

    }

    private String deleteInput() {
        String input = "";

        JSONArray jsonArray = new JSONArray();
        try {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("Id", room_id);
            jsonArray.put(jsonObject1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        input = jsonArray.toString();

        return input;
    }

    private void getBedroomList() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_BEDROOM_ALL + pid + "/" + floor_id + "/room",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_BEDROOM_ALL,
                Constants.GET).execute();

    }

    private void getFloorList() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.FLOOR_LIST + pid,
                "",
                "Loading..",
                this,
                Urls.FLOOR_LIST,
                Constants.GET).execute();

    }

    private void getBalconyList() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_BEDROOM_ALL + pid + "/" + floor_id + "/balcony",
                "",
                "Loading..",
                this,
                Urls.URL_PROPERTY_BEDROOM_ALL + "/balcony",
                Constants.GET).execute();

    }

    private void addFloor() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_FLOOR,
                getPropertyFloor(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_FLOOR,
                Constants.POST).execute();

    }

    private void removeFloor(int id) {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_REMOVE_FLOOR,
                getRemoveFloor(id),
                "Loading..",
                this,
                Urls.URL_REMOVE_FLOOR,
                Constants.POST).execute();

    }

    private void updateFloor() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_FLOOR,
                updatePropertyFloor(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_FLOOR,
                Constants.POST).execute();

    }

    private String getAddinput() {
        String input = "";

        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {
            if (!edit_property.equals("")) {

                jsonObject1.put("PropertyId", Editid);

            } else {

                jsonObject1.put("PropertyId",pid );

            }
            jsonObject1.put("Id", 0);
            jsonObject1.put("BedRoomName", roomName);
            jsonObject1.put("BedRoomType", room_type_txt);
            jsonObject1.put("BedRoomSize", room_size.getText().toString());
            jsonObject1.put("BedRoomSizeUnitId", select_unit_id);
            jsonObject1.put("Length", length.getText().toString());
            jsonObject1.put("Width", width.getText().toString());
            jsonObject1.put("LengthWidthUnitId", select_unit_room_id);
            jsonObject1.put("Category", category);
            jsonObject1.put("PropertyFloorId", floor_id);

            JSONObject jsonObject2 = new JSONObject();
            for (int i = 0; i < checked_room.size(); i++) {
                jsonObject2.put("AttachPropertyBedroomId", checked_room.get(i));
                jsonArray1.put(jsonObject2);
                Log.e("checked_room", "checked_room" + checked_room.get(i));
            }

            jsonObject1.put("PropertyBedroomAttachWiths", jsonArray1);

            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }

    private String getPropertyFloor() {
        String input = "";


        JSONObject jsonObjectRoom = new JSONObject();
        // JSONArray jsonArray1 = new JSONArray();
        try {
            //  jsonObject1.put("PropertyId", pid);
            if (!edit_property.equals("")) {

                jsonObjectRoom.put("PropertyId", Editid);

            } else {

                jsonObjectRoom.put("PropertyId", LocationFragment.pid);

            }
            jsonObjectRoom.put("Id", 0);
            jsonObjectRoom.put("FloorNumber", et_floorno.getText().toString());
            jsonObjectRoom.put("FloorSize", et_floorsize.getText().toString());
            jsonObjectRoom.put("FloorSizeUnitId", floorSizeUnitId);

            input = jsonObjectRoom.toString();

        } catch (Exception e) {

            e.printStackTrace();
        }


        return input;

    }

    private String getRemoveFloor(int id) {
        String input = "";


        JSONObject jsonObjectRoom = new JSONObject();
        // JSONArray jsonArray1 = new JSONArray();
        try {
            jsonObjectRoom.put("Id", id);


            input = jsonObjectRoom.toString();

        } catch (Exception e) {

            e.printStackTrace();
        }
        return input;

    }

    private String updatePropertyFloor() {
        String input = "";


        JSONObject jsonObjectRoom = new JSONObject();
        // JSONArray jsonArray1 = new JSONArray();
        try {
            //  jsonObject1.put("PropertyId", pid);
            if (!edit_property.equals("")) {

                jsonObjectRoom.put("PropertyId", Editid);

            } else {

                jsonObjectRoom.put("PropertyId", LocationFragment.pid);

            }
            jsonObjectRoom.put("Id", selectedFloorId);
            jsonObjectRoom.put("FloorNumber", et_floorno.getText().toString());
            jsonObjectRoom.put("FloorSize", et_floorsize.getText().toString());
            jsonObjectRoom.put("FloorSizeUnitId", 1);

            input = jsonObjectRoom.toString();

        } catch (Exception e) {

            e.printStackTrace();
        }


        return input;

    }

    private String getEditinput() {
        String input = "";

        JSONObject jsonObject1 = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();
        try {
            jsonObject1.put("Id", room_id);
            if (!edit_property.equals("")) {

                jsonObject1.put("PropertyId", Editid);

            } else {

                jsonObject1.put("PropertyId", pid);

            }
            jsonObject1.put("BedRoomName", roomName);
            jsonObject1.put("BedRoomType", room_type_txt);
            jsonObject1.put("BedRoomSize", room_size.getText().toString());
            jsonObject1.put("BedRoomSizeUnitId", select_unit_id);
            jsonObject1.put("Length", length.getText().toString());
            jsonObject1.put("Width", width.getText().toString());
            jsonObject1.put("LengthWidthUnitId", select_unit_room_id);
            jsonObject1.put("Category", category);
            jsonObject1.put("PropertyFloorId", floor_id);

         /* jsonObject1.put("PropertyTypeId", PropertyTypeId);
            jsonObject1.put("GroundType", ground_type_txt);
            jsonObject1.put("FloorNo", floor_txt);
            jsonObject1.put("BedRoomSizeUnitId", select_unit_id);*/

            JSONObject jsonObject2 = new JSONObject();
            for (int i = 0; i < checked_room.size(); i++) {
                jsonObject2.put("AttachPropertyBedroomId", checked_room.get(i));
                jsonArray1.put(jsonObject2);
                Log.e("checked_room", "checked_room" + checked_room.get(i));
            }

            jsonObject1.put("PropertyBedroomAttachWiths", jsonArray1);

            input = jsonObject1.toString();

        } catch (Exception e) {

        }


        return input;

    }

    private void add_feature() {
        dialog_feature = new Dialog(getActivity());
        dialog_feature.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_feature.setContentView(R.layout.add_more_feature_lay);
        add_feature = (Button) dialog_feature.findViewById(R.id.add_feature);
        edit_feature_txt = (EditText) dialog_feature.findViewById(R.id.edit_feature_txt);
        iv_close_feature = (ImageView) dialog_feature.findViewById(R.id.iv_close);

        add_feature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edit_feature_txt.getText().toString().trim().equalsIgnoreCase("")) {

                    FeatureModel featureModel = new FeatureModel();
                    featureModel.setName(edit_feature_txt.getText().toString());
                    featureModel.setType("feature");
                    featureModel.setId("0");
                    featureModel.setTotalCount("0");
                    featureModel.setStatus("unchecked");
                    featureModel.setTotalCount("0");

                    featureModelArrayList.add(featureModel);

//                    featureAddAdapter.notifyDataSetChanged();

                    setFeatureAdapter();
                    dialog_feature.cancel();

                } else {

                    Utils.showErrorMessage("Enter your feature name", getActivity());
                }

            }
        });

        iv_close_feature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_feature.cancel();
            }
        });

        dialog_feature.show();
    }

    private void setList() {

        if (balcony_list.size() == 0)

            balcony_detail_list.setVisibility(View.GONE);


        else
            balcony_detail_list.setVisibility(View.VISIBLE);
    }

    private void balconyDetaildialog(int position) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.balcony_detail_popup_room);
        iv_close = (ImageView)dialog.findViewById(R.id.iv_close);
        room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        select_unit_room = (Spinner) dialog.findViewById(R.id.select_unit_room);
        select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units);
        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));
        room_data.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (category.equals("room")) {
            room_balcony_txt.setText("Room Name*");
            lnrRoomType.setVisibility(View.VISIBLE);
        } else {
            room_balcony_txt.setText("Balcony Name*");
            lnrRoomType.setVisibility(View.GONE);
        }
        if (!room_list.isEmpty()) {
            attached_layout.setVisibility(View.VISIBLE);
        } else {
            attached_layout.setVisibility(View.GONE);
        }

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    select_unit_txt = select_unit.getSelectedItem().toString();
                    select_unit_id = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        select_unit_room.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit_room.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    select_unit_room_txt = select_unit_room.getSelectedItem().toString();
                    select_unit_room_id = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//                   int choose = spin_type.getSelectedItemPosition();
                    room_type_txt = room_type.getSelectedItem().toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        //  roomAttachedDataAdapter = new RoomAttachedDataAdapter(getActivity(), room_list);

        roomAttachedDataAdapter = new RoomAttachedDataAdapter(getActivity(), room_list, position);

        room_data.setAdapter(roomAttachedDataAdapter);

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

/*
        room_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                same = false;

            }
        });
*/

        room_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                roomName = room_name.getText().toString().trim();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add

                if ((!(room_list.isEmpty() && room_list.size() == 0)) || (!(balcony_list.isEmpty() && balcony_list.size() == 0))) {

                    if (!roomName.isEmpty() && roomName != null) {

                        if (!roomName.equalsIgnoreCase("")
                                && length.getText().toString().equalsIgnoreCase("")
                                && width.getText().toString().equalsIgnoreCase("")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Please fill Balcony Length and Width", "Ok", getActivity());
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Please fill Room Length and Width", "Ok", getActivity());

                            }
                        }
                    }

                    {

                        if (roomName.equalsIgnoreCase("")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Please fill Balcony Name", "Ok", getActivity());
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Please fill Room Name", "Ok", getActivity());

                            }
                        } else if (length.getText().toString().equalsIgnoreCase("")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Please fill Balcony Length", "Ok", getActivity());
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Please fill Room Length", "Ok", getActivity());

                            }
                        } else if (length.getText().toString().equalsIgnoreCase("0")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Balcony Length should be greater than 0", "Ok", getActivity());
                                length.setText("");
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Room Length should be greater than 0", "Ok", getActivity());
                                length.setText("");
                            }
                        } else if (width.getText().toString().equalsIgnoreCase("")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Please fill Width", "Ok", getActivity());
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Please fill Width", "Ok", getActivity());

                            }
                        } else if (width.getText().toString().equalsIgnoreCase("0")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Balcony Width should be greater than 0", "Ok", getActivity());
                                width.setText("");
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Room Width should be greater than 0", "Ok", getActivity());
                                width.setText("");
                            }
                        } else {

                            if (!recent.equalsIgnoreCase("edit")){

                                if (room_list.size() > 0) {

                                    for (int i = 0; i < room_list.size(); i++) {

                                        if (roomName.equalsIgnoreCase(room_list.get(i).getRoom_name())) {

                                            Utils.showWarningErrorMessage("Warning", "This room name already exists.", "Ok", getActivity());
                                            //room_name.setText("");
                                            same = true;

                                            break;
                                        }

                                    }


                                    if (!same) {

                                        if (!add_room.equalsIgnoreCase("")) {

                                            postAddRoom();

                                            dialog.dismiss();

                                        } else {

                                            recent = " ";

                                            editRoom();

                                            dialog.dismiss();

                                        }
                                    }
                                }

                            }


                            else {
                                recent = " ";

                                editRoom();

                                dialog.dismiss();
                            }
                        }


                    }
                } else {

                    if (roomName.equalsIgnoreCase("")) {

                        if (category.equalsIgnoreCase("balcony")) {
                            Utils.showWarningErrorMessage("Warning", "Please fill Balcony Name", "Ok", getActivity());
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Please fill Room Name", "Ok", getActivity());

                        }
                    } else if (length.getText().toString().equalsIgnoreCase("")
                            ) {

                        if (category.equalsIgnoreCase("balcony")) {
                            Utils.showWarningErrorMessage("Warning", "Please fill Balcony Length", "Ok", getActivity());
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Please fill Room Length", "Ok", getActivity());

                        }
                    } else if (width.getText().toString().equalsIgnoreCase("")) {

                        if (category.equalsIgnoreCase("balcony")) {
                            Utils.showWarningErrorMessage("Warning", "Please fill Balcony Width", "Ok", getActivity());
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Please fill Room Width", "Ok", getActivity());

                        }
                    } else {

                        postAddRoom();
                        dialog.dismiss();

                    }
                }

            }
        });
        select_unit_room.setAdapter(adapter1);
        select_unit_room.setSelection(adapter1.getCount());
        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        room_type.setAdapter(adapter);
        room_type.setSelection(adapter.getCount());
        dialog.show();

    }

    private void roomDetaildialog(int position) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.room_detail_popup);
        iv_close = (ImageView)dialog.findViewById(R.id.iv_close);
        room_balcony_txt = (TextView) dialog.findViewById(R.id.room_balcony_txt);
        room_type = (Spinner) dialog.findViewById(R.id.room_type);
        LinearLayout lnrRoomType = (LinearLayout) dialog.findViewById(R.id.lnrRoomType);
        select_unit_room = (Spinner) dialog.findViewById(R.id.select_unit_room);
        select_unit = (Spinner) dialog.findViewById(R.id.select_unit);
        room_data = (RecyclerView) dialog.findViewById(R.id.room_data);
        attached_layout = (LinearLayout) dialog.findViewById(R.id.attached_layout);
        length = (EditText) dialog.findViewById(R.id.length);
        width = (EditText) dialog.findViewById(R.id.width);
        room_size = (EditText) dialog.findViewById(R.id.room_size);
        room_name = (EditText) dialog.findViewById(R.id.room_name);
        ImageView iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        save = (Button) dialog.findViewById(R.id.save);
        adapter = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter.addAll(property_units);
        adapter.add(getString(R.string.select_type));
        adapter1 = new SpinnerAdapter(getActivity(), R.layout.new_spinner_item);
        adapter1.addAll(property_units_id);
        adapter1.add(getString(R.string.select_type));
        room_data.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (category.equals("room")) {
            room_balcony_txt.setText("Room Name*");
            lnrRoomType.setVisibility(View.VISIBLE);
        } else {
            room_balcony_txt.setText("Balcony Name*");
            lnrRoomType.setVisibility(View.GONE);
        }
        if (!room_list.isEmpty()) {
            attached_layout.setVisibility(View.VISIBLE);
        } else {
            attached_layout.setVisibility(View.GONE);
        }

        select_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    select_unit_txt = select_unit.getSelectedItem().toString();
                    select_unit_id = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        select_unit_room.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (select_unit_room.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {

                    select_unit_room_txt = select_unit_room.getSelectedItem().toString();
                    select_unit_room_id = propertyUnitsModels.get(position).getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        room_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (room_type.getSelectedItem() == getString(R.string.select_type)) {

                    //Do nothing.
                } else {
//                   int choose = spin_type.getSelectedItemPosition();
                    room_type_txt = room_type.getSelectedItem().toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        //  roomAttachedDataAdapter = new RoomAttachedDataAdapter(getActivity(), room_list);

        roomAttachedDataAdapter = new RoomAttachedDataAdapter(getActivity(), room_list, position);

        room_data.setAdapter(roomAttachedDataAdapter);

        length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        room_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                same = false;

            }
        });

        room_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                roomName = room_name.getText().toString().trim();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double v1 = Double.parseDouble(!length.getText().toString().isEmpty() ? length.getText().toString() : "0");
                Double v2 = Double.parseDouble(!width.getText().toString().isEmpty() ?
                        width.getText().toString() : "0");
                Double value = v1 * v2;
                room_size.setText(value.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        room_size.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // same room can't be add

                if ((!(room_list.isEmpty() && room_list.size() == 0)) || (!(balcony_list.isEmpty() && balcony_list.size() == 0))) {

                    same = false;

                    if (!roomName.isEmpty() && roomName != null) {

                        if (!roomName.equalsIgnoreCase("")
                                && length.getText().toString().equalsIgnoreCase("")
                                && width.getText().toString().equalsIgnoreCase("")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Please fill Balcony Length and Width", "Ok", getActivity());
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Please fill Room Length and Width", "Ok", getActivity());

                            }
                        }
                    }

                    {

                        if (roomName.equalsIgnoreCase("")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Please fill Balcony Name", "Ok", getActivity());
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Please fill Room Name", "Ok", getActivity());

                            }
                        } else if (length.getText().toString().equalsIgnoreCase("")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Please fill Balcony Length", "Ok", getActivity());
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Please fill Room Length", "Ok", getActivity());

                            }
                        } else if (length.getText().toString().equalsIgnoreCase("0")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Balcony Length should be greater than 0", "Ok", getActivity());
                                length.setText("");
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Room Length should be greater than 0", "Ok", getActivity());
                                length.setText("");
                            }
                        } else if (width.getText().toString().equalsIgnoreCase("")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Please fill Width", "Ok", getActivity());
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Please fill Width", "Ok", getActivity());

                            }
                        } else if (width.getText().toString().equalsIgnoreCase("0")) {

                            if (category.equalsIgnoreCase("balcony")) {
                                Utils.showWarningErrorMessage("Warning", "Balcony Width should be greater than 0", "Ok", getActivity());
                                width.setText("");
                            } else {
                                Utils.showWarningErrorMessage("Warning", "Room Width should be greater than 0", "Ok", getActivity());
                                width.setText("");
                            }
                        } else {

                            if (!recent.equalsIgnoreCase("edit")){

                                if (room_list.size() > 0) {

                                    for (int i = 0; i < room_list.size(); i++) {

                                        if (roomName.equalsIgnoreCase(room_list.get(i).getRoom_name())) {

                                            Utils.showWarningErrorMessage("Warning", "This room name already exists.", "Ok", getActivity());
                                            //room_name.setText("");
                                            same = true;

                                            break;
                                        }

                                    }


                                    if (!same) {

                                        if (!add_room.equalsIgnoreCase("")) {

                                            postAddRoom();

                                            dialog.dismiss();

                                        } else {

                                            recent = " ";

                                            editRoom();

                                            dialog.dismiss();

                                        }
                                    }

                                }

                            }


                            else {
                                recent = " ";

                                editRoom();

                                dialog.dismiss();
                            }
                        }


                    }
                } else {

                    if (roomName.equalsIgnoreCase("")) {

                        if (category.equalsIgnoreCase("balcony")) {
                            Utils.showWarningErrorMessage("Warning", "Please fill Balcony Name", "Ok", getActivity());
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Please fill Room Name", "Ok", getActivity());

                        }
                    } else if (length.getText().toString().equalsIgnoreCase("")
                            ) {

                        if (category.equalsIgnoreCase("balcony")) {
                            Utils.showWarningErrorMessage("Warning", "Please fill Balcony Length", "Ok", getActivity());
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Please fill Room Length", "Ok", getActivity());

                        }
                    } else if (width.getText().toString().equalsIgnoreCase("")) {

                        if (category.equalsIgnoreCase("balcony")) {
                            Utils.showWarningErrorMessage("Warning", "Please fill Balcony Width", "Ok", getActivity());
                        } else {
                            Utils.showWarningErrorMessage("Warning", "Please fill Room Width", "Ok", getActivity());

                        }
                    } else {

                        postAddRoom();
                        dialog.dismiss();

                    }
                }

            }
        });
        select_unit_room.setAdapter(adapter1);
        select_unit_room.setSelection(adapter1.getCount());
        select_unit.setAdapter(adapter1);
        select_unit.setSelection(adapter1.getCount());
        room_type.setAdapter(adapter);
        room_type.setSelection(adapter.getCount());
        dialog.show();

    }

    public void onPropertyBedroomdata() {
        try {

            // location frag data
            jsonAccom = new JSONObject();
            if (!edit_property.equals("")) {

                jsonAccom.put("Id", Editid);

            } else {

                jsonAccom.put("Id", pid);

            }

            jsonAccom.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonAccom.put("GroundType",ground_type_text );
            jsonAccom.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonAccom.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonAccom.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonAccom.put("IsDraft", 0);


            JSONArray jsonBedroomArray = new JSONArray();
            for (int index = 0; index < room_list.size(); index++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("Id", room_list.get(index).getId());
                jsonObject.put("GroundType", room_list.get(index).getGoundtype());
                jsonObject.put("FloorNo", room_list.get(index).getFloor());
                jsonObject.put("IsDraft", 0);
                jsonObject.put("BedRoomName", room_list.get(index).getRoom_name());
                jsonObject.put("BedRoomType", room_type_txt);
                jsonObject.put("BedRoomSize", room_list.get(index).getRoom_size());
                jsonObject.put("BedRoomSizeUnitId", room_list.get(index).getRoom_size());
                jsonObject.put("Length", room_list.get(index).getLength());
                jsonObject.put("Width", room_list.get(index).getWidth());
                jsonObject.put("Category", room_list.get(index).getCategory());

                JSONArray jsonAttachBedPropArray = new JSONArray();
                JSONObject jsonObjPropBedAttach = new JSONObject();
                for (int i = 0; i < checked_room.size(); i++) {
                    jsonObjPropBedAttach.put("AttachPropertyBedroomId", checked_room.get(i));
                    jsonAttachBedPropArray.put(jsonObjPropBedAttach);
                    Log.e("checked_room", "checked_room" + checked_room.get(i));
                }

                jsonObject.put("PropertyBedroomAttachWiths", jsonAttachBedPropArray);

                jsonBedroomArray.put(jsonObject);

            }


            JSONArray jsonPropBedArray = new JSONArray();
            for (int i = 0; i < propertyBedrooms.size(); i++) {
                JSONObject PropertyBedroomsobject = new JSONObject();
                PropertyBedroomsobject.put("Id", propertyBedrooms.get(i).getPropertyId());

                jsonPropBedArray.put(PropertyBedroomsobject);

                JSONArray PropertyBedroomAttachWithsArray = new JSONArray();
                for (int j = 0; j < propertyBedrooms.get(i).getAttachPropertyBedroomIds().size(); i++) {
                    JSONObject PropertyBedroomAttachWithsObject = new JSONObject();
                    PropertyBedroomAttachWithsObject.put("Id", propertyBedrooms.get(i).getAttachPropertyBedroomIds().get(j).getAttachPropertyBedroomId());
                    PropertyBedroomAttachWithsObject.put("AttachPropertyBedroomId", propertyBedrooms.get(i).getAttachPropertyBedroomIds().get(j).getAttachPropertyBedroomId());

                    PropertyBedroomAttachWithsArray.put(PropertyBedroomAttachWithsObject);
                }

            }

            JSONArray jsonAccomArray = new JSONArray();
            JSONArray jsonAccomArrayparking = new JSONArray();

            if (edit_property.equals("")) {



            } else {



            }

            for (int i = 0; i < featureModelArrayList.size(); i++) {
                JSONObject jsonAccomoObject = new JSONObject();
                jsonAccomoObject.put("Id", featureModelArrayList.get(i).getId());
                jsonAccomoObject.put("Name", featureModelArrayList.get(i).getName());
                jsonAccomoObject.put("Type", featureModelArrayList.get(i).getType());
                jsonAccomoObject.put("TotalCount", featureModelArrayList.get(i).getTotalCount());
                jsonAccomoObject.put("IsDeletable", false);
                jsonAccomoObject.put("IsModify",true);
                jsonAccomArray.put(jsonAccomoObject);
            }

            for (int i = 0; i < parkingModelArrayList.size(); i++) {
                JSONObject jsonAccomoObjectparking = new JSONObject();
                jsonAccomoObjectparking.put("Id", parkingModelArrayList.get(i).getId());
                jsonAccomoObjectparking.put("Name", parkingModelArrayList.get(i).getName());
                jsonAccomoObjectparking.put("Type", parkingModelArrayList.get(i).getType());
                jsonAccomoObjectparking.put("TotalCount", parkingModelArrayList.get(i).getTotalCount());
                jsonAccomoObjectparking.put("IsDeletable", false);
                jsonAccomoObjectparking.put("IsModify",true);

                jsonAccomArrayparking.put(jsonAccomoObjectparking);
            }


            for (int i = 0; i < jsonAccomArray.length(); i++) {
                jsonAccomArrayparking.put(jsonAccomArray.getJSONObject(i));
            }
            jsonAccom.put("PropertyAccommodationDetails", jsonAccomArrayparking);
            jsonAccom.put("PropertyBedrooms", jsonBedroomArray);

            postProperty();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPropertyDraftBedroomdata() {

        try {

            // location frag data
            jsonAccom = new JSONObject();
            jsonAccom.put("Id", pid);

            jsonAccom.put("PropertyTypeId",LocationFragment.PropertyTypeId );
            jsonAccom.put("CompanyId", Constants.APP_COMPANY_ID);
            jsonAccom.put("CreatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonAccom.put("UpdatedById", PreferenceConnector.getInstance(getActivity()).loadSavedPreferences(Constants.USER_ID, ""));
            jsonAccom.put("IsDraft", 1);

            JSONArray jsonArrayLoc = new JSONArray();

            JSONObject jsonObjectLoc = new JSONObject();
            if (inventoryModelProp != null) {
                if (inventoryModelProp.getDeveloper() != null)
                    jsonObjectLoc.put("Developer", inventoryModelProp.getDeveloper());
                if (inventoryModelProp.getProject() != null)
                    jsonObjectLoc.put("Project", inventoryModelProp.getProject());
                if (inventoryModelProp.getZipCode() != null)
                    jsonObjectLoc.put("ZipCode", inventoryModelProp.getZipCode());
                if (inventoryModelProp.getState() != null)

                    jsonObjectLoc.put("State", inventoryModelProp.getState());
                if (inventoryModelProp.getDistrict() != null)

                    jsonObjectLoc.put("District", inventoryModelProp.getDistrict());

                if (inventoryModelProp.getTownName() != null)

                    jsonObjectLoc.put("TownName", inventoryModelProp.getTownName());

                if (inventoryModelProp.getSector() != null)

                    jsonObjectLoc.put("Sector", inventoryModelProp.getSector());

                if (inventoryModelProp.getSubDivision() != null)

                    jsonObjectLoc.put("SubDivision", inventoryModelProp.getSubDivision());
                if (inventoryModelProp.getSubArea() != null)

                    jsonObjectLoc.put("SubArea", inventoryModelProp.getSubArea());

                if (inventoryModelProp.getFloorNo() != null)

                    jsonObjectLoc.put("FloorNo", inventoryModelProp.getFloorNo());
                if (inventoryModelProp.getGooglePlusCode() != null)

                    jsonObjectLoc.put("GooglePlusCode", inventoryModelProp.getGooglePlusCode());
                if (inventoryModelProp.getUnitNo() != null)

                    jsonObjectLoc.put("UnitNo", inventoryModelProp.getUnitNo());
                if (inventoryModelProp.getSector() != null)

                    jsonObjectLoc.put("Sector", inventoryModelProp.getSector());
            }

            jsonArrayLoc.put(jsonObjectLoc);

            // detail frag data

            JSONArray jsonDetailArray = new JSONArray();

            JSONObject jsonObjectDet = new JSONObject();
            jsonObjectDet.put("Id", propAreaModel.getId());

            jsonObjectDet.put("PlotShape", propAreaModel.getPlotShape());
            jsonObjectDet.put("PlotArea",propAreaModel.getPlotArea());
            if (propAreaModel.getPlotAreaUnitId() == null)
                jsonObjectDet.put("PlotAreaUnitId","" );
            else {
                jsonObjectDet.put("PlotAreaUnitId", propAreaModel.getPlotAreaUnitId());
            }
            jsonObjectDet.put("FrontSize", propAreaModel.getFrontSize());
            if (propAreaModel.getFrontSizeUnitId() == null)
                jsonObjectDet.put("FrontSizeUnitId", "");
            else {
                jsonObjectDet.put("FrontSizeUnitId", propAreaModel.getFrontSizeUnitId() );
            }

            jsonObjectDet.put("DepthSize",propAreaModel.getDepthSize() );
            if (propAreaModel.getDepthSizeUnitId() == null)
                jsonObjectDet.put("DepthSizeUnitId", "");
            else {
                jsonObjectDet.put("DepthSizeUnitId", propAreaModel.getDepthSizeUnitId());
            }

            jsonObjectDet.put("StreetOrRoadType",propAreaModel.getStreetOrRoadType() );
            jsonObjectDet.put("RoadWidth", propAreaModel.getRoadWidth());
            jsonObjectDet.put("DepthSize", propAreaModel.getDepthSize());
            if (propAreaModel.getRoadWidthUnitId() == null)
                jsonObjectDet.put("RoadWidthUnitId", "");
            else {
                jsonObjectDet.put("RoadWidthUnitId", propAreaModel.getRoadWidthUnitId() );
            }

            jsonObjectDet.put("SuperArea", propAreaModel.getSuperArea() );
            jsonObjectDet.put("BuiltUpArea", propAreaModel.getBuiltUpArea() );
            jsonObjectDet.put("CarpetArea", propAreaModel.getCarpetArea() );
            jsonObjectDet.put("EnteranceDoorFacing", propAreaModel.getEnteranceDoorFacing() );
            jsonObjectDet.put("IsDeletable", false);

            if (!edit_property.equals("")) {

                jsonObjectDet.put("IsModify", true);

            } else {

                jsonObjectDet.put("IsModify", false);

            }

            if (propAreaModel.getRoadWidthUnitId()  == null)
                jsonObjectDet.put("SuperAreaUnitId", "");
            else {
                jsonObjectDet.put("SuperAreaUnitId", propAreaModel.getSuperAreaUnitId() );
            }
            if (propAreaModel.getRoadWidthUnitId()  == null)
                jsonObjectDet.put("BuiltUpAreaUnitId", "");
            else {
                jsonObjectDet.put("BuiltUpAreaUnitId", propAreaModel.getBuiltUpAreaUnitId() );
            }
            if (propAreaModel.getRoadWidthUnitId()  == null)
                jsonObjectDet.put("CarpetAreaUnitId", "");
            else {
                jsonObjectDet.put("CarpetAreaUnitId", propAreaModel.getCarpetAreaUnitId() );
            }

            // final params
            jsonAccom.put("PropertyLocations", jsonArrayLoc);
            jsonAccom.put("PropertyAreas", jsonDetailArray);

            postProperty_draft();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void postProperty() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Urls.URL_PROPERTY_POST,
                Constants.POST).execute();

    }

    private void postProperty_draft() {

        new CommonAsync(getActivity(), Urls.CONTACT_MANAGEMENT_URL + Urls.URL_PROPERTY_POST,
                propertyInput(),
                "Loading..",
                this,
                Constants.URL_PROPERTY_POST_DRAFT,
                Constants.POST).execute();

    }

    private String propertyInput() {
        String inputStr = "";

        try {
            inputStr = jsonAccom.toString();
        } catch (Exception e) {

        }


        return inputStr;
    }

    public void onEvent(DialogModel dialogModel) {
        if (dialogModel.getClickedOn().equalsIgnoreCase("Ok")) {

            Intent intent = new Intent(getActivity(), MyPropertyActivity.class);
            intent.putExtra("from_savedrafts", "from_savedrafts");

            startActivity(intent);

        }
    }

    public class Floor extends RecyclerView.Adapter<Floor.ViewHolder> {
        ArrayList<FloorCalculationModel> floorNumberList;
        int selected_position = -1;
        private int row_index = -1;

        public Floor(ArrayList<FloorCalculationModel> floorNumberList) {

            this.floorNumberList = floorNumberList;

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.floor_bedroom_inflate_view, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.number.setText(String.valueOf(floorNumberList.get(position).getFloorNumber()));

            holder.number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    row_index = position;
                    floor_id = floorNumberList.get(position).getId();
                    getBedroomList();
                    getBalconyList();
                    ll_detail.setVisibility(View.VISIBLE);
                    notifyDataSetChanged();
                }
            });

            if (row_index == position) {
                holder.number.setTextColor(Color.parseColor("#2C8BCE"));
            } else {
                holder.number.setTextColor(Color.parseColor("#727272"));

            }

            holder.no_room_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    floor_id = floorNumberList.get(position).getId();
                    selected_position = position;
                    notifyDataSetChanged();

                }
            });

        }


        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView number;
            LinearLayout no_room_layout;

            public ViewHolder(View itemView) {
                super(itemView);

                number = (TextView) itemView.findViewById(R.id.number);
                no_room_layout = (LinearLayout) itemView.findViewById(R.id.no_room_layout);

            }

        }
    }

    public class FloorListAdapter extends RecyclerView.Adapter<FloorListAdapter.ViewHolder> {
        ArrayList<FloorCalculationModel> floorNumberList;

        public FloorListAdapter(ArrayList<FloorCalculationModel> floorNumberList) {

            this.floorNumberList = floorNumberList;

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_floor, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.number.setText(floorNumberList.get(position).getCount());

        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView number;
            LinearLayout no_room_layout;

            public ViewHolder(View itemView) {
                super(itemView);

                number = (TextView) itemView.findViewById(R.id.number);
                no_room_layout = (LinearLayout) itemView.findViewById(R.id.no_room_layout);

            }

        }
    }

    public class FloornumberAdapter extends RecyclerView.Adapter<FloornumberAdapter.ViewHolder> {

        Context context;
        ArrayList<FloorCalculationModel> floorNumberList = new ArrayList<>();

        String number_count;
        private String select_unit_txt, select_unit_id;

        public FloornumberAdapter(Context context, ArrayList<FloorCalculationModel> floorNumberList) {
            this.context = context;
            this.floorNumberList = floorNumberList;

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.floor_layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.floor_minus.setTag(position);
            holder.select_floor_unit.setAdapter(adapter1);
            holder.floor_number.setText(String.valueOf(floorNumberList.get(position).getFloorNumber()));
            holder.floor_size.setText(String.valueOf(floorNumberList.get(position).getFloorSize()));

            String currentId = (propertyUnitsModels.get(floorNumberList.get(position).getFloorSizeUnitId()-1).getId());

            holder.select_floor_unit.setSelection(Integer.parseInt(currentId)-1);

            if (floorNumberList.size() > 1) {

                holder.floor_minus.setVisibility(View.VISIBLE);
            } else {

                holder.floor_minus.setVisibility(View.GONE);
            }

            holder.select_floor_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    // TODO Auto-generated method stub

                    if (holder.select_floor_unit.getSelectedItem() == getActivity().getString(R.string.select_type)) {

                        //Do nothing.
                    } else {
                        /*for (int k = 0; k < propertyUnitsModels.size(); k++) {

                                if (json_Data.get(i).getPropertyFinancialses().get(0).getPriceUnitId() == propertyUnitsModels.get(k).getPriceUnitId()) {

                                    price_select.setSelection(k);

                                }

                            }
                        *
                        * */
                        select_unit_txt = holder.select_floor_unit.getSelectedItem().toString();
                        select_unit_id = propertyUnitsModels.get(position).getId();

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });

          /* if (edit_property.equalsIgnoreCase(""))

*/
            holder.floor_size.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    et_floorno.setText(holder.floor_number.getText().toString());
                    et_floorsize.setText(holder.floor_size.getText().toString());
                    selectedFloorId = floorNumberList.get(position).getId();
                    update_more_floor.setVisibility(View.VISIBLE);
                }
            });
            holder.floor_number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    et_floorno.setText(holder.floor_number.getText().toString());
                    et_floorsize.setText(holder.floor_size.getText().toString());
                    selectedFloorId = floorNumberList.get(position).getId();
                    update_more_floor.setVisibility(View.VISIBLE);
                }
            });
            holder.floor_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                   /* int pos = position;
                                    FloorCalculationModel floorCalculationModel = new FloorCalculationModel();
                                    floorCalculationModel.setDeletePos(pos);
                                    floorCalculationModel.setDelete("Yes");
                                    EventBus.getDefault().post(floorCalculationModel);*/

                                    removeFloor(floorNumberList.get(position).getId());

                                }
                            })
                            .show();


                }
            });
        }

        @Override
        public int getItemCount() {
            return floorNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView floor_size, floor_number;
            ImageView floor_minus;
            Spinner select_floor_unit;

            public ViewHolder(View itemView) {
                super(itemView);
                floor_size = (TextView) itemView.findViewById(R.id.floor_size);
                floor_minus = (ImageView) itemView.findViewById(R.id.floor_minus);
                floor_number = (TextView) itemView.findViewById(R.id.floor_number);
                select_floor_unit = (Spinner) itemView.findViewById(R.id.select_floor_unit);

            }


        }
    }

    public static void hideSoftKeyboard(Activity activity) {

        if (activity!=null){

            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }

    }

}
