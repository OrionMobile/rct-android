package com.bnhabitat.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.bnhabitat.R;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.adapters.FilterDialogFragmentAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import de.greenrobot.event.EventBus;


public class FilterDialogFragment extends DialogFragment {


    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    Button go_btn;
    private ArrayList<ProjectsDetailModel> searchProjectsModels = new ArrayList();
    private ArrayList<String> projectStatus = new ArrayList();

    public FilterDialogFragment() {
        // Required empty public constructor
    }

    String FilterType = "";

    public static FilterDialogFragment newInstance() {


        return new FilterDialogFragment();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.fragment_filter_dialog, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.filter_recycler_view);
        go_btn = (Button) view.findViewById(R.id.go_btn);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            searchProjectsModels = (ArrayList<ProjectsDetailModel>) bundle.getSerializable("Arraylist");
        }

        for (ProjectsDetailModel projectsModel : searchProjectsModels) {

            if (!projectsModel.getProjectStatus().equalsIgnoreCase("null")) {
                projectStatus.add(projectsModel.getProjectStatus());

            }
        }

        HashSet<String> hashSet = new HashSet<String>();
        hashSet.addAll(projectStatus);
        projectStatus.clear();
        projectStatus.addAll(hashSet);
        Collections.sort(projectStatus, new Comparator<String>() {
            public int compare(String obj1, String obj2) {
                // ## Ascending order
                return obj1.compareToIgnoreCase(obj2); // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(obj2.empId); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
            }
        });
        FilterDialogFragmentAdapter filterDialogFragmentAdapter = new FilterDialogFragmentAdapter(getActivity(), projectStatus);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(filterDialogFragmentAdapter);


        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    public void onEvent(CommonDialogModel commonDialogModel){
        dialogDismiss();
    }
    public void dialogDismiss() {


        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog_fragment");
        if (prev != null) {
            FilterDialogFragment df = (FilterDialogFragment) prev;
            df.dismiss();
        }
    }

}
