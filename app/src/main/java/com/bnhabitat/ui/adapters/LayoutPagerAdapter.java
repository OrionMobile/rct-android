package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bnhabitat.R;
import com.bnhabitat.models.GalleryModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.utils.Urls;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by developer15 on 3/29/2016.
 */
public class LayoutPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<ProjectsDetailModel.PropertySize.LayoutPlans> layoutPlanses = new ArrayList<>();
    private ArrayList<GalleryModel> layoutImgs = new ArrayList<>();

    public LayoutPagerAdapter(Context context, ArrayList<ProjectsDetailModel.PropertySize.LayoutPlans> layoutPlanses) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layoutPlanses=layoutPlanses;


        for (ProjectsDetailModel.PropertySize.LayoutPlans layoutPlans : layoutPlanses) {
            GalleryModel galleryModel = new GalleryModel();
            galleryModel.setImagePath(layoutPlans.getBigImage());
            galleryModel.setName(layoutPlans.getType());
            galleryModel.setTitle(layoutPlans.getTitle());
            layoutImgs.add(galleryModel);
        }


    }

    @Override
    public int getCount() {
        return layoutPlanses.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

        String url = layoutPlanses.get(position).getBigImage();
//        url = GlobalManager.getGeneratedImageUrl(url);
//
//        GlobalManager.setImageView(url,imageView,mContext);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        imageLoader.displayImage(Urls.BASE_IMAGE_URL+url, imageView,options);


//        imageView.setTag(position);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int position  = (Integer) view.getTag();
//
//                //mContext.startActivity(new Intent(mContext, ZoomActivity.class).putExtra("img",url));
//
//
//                mContext.startActivity(new Intent(mContext, ZoomActivity.class)
//                        .putExtra(Constants.ZOOM_LIST, layoutImgs)
//                        .putExtra(Constants.POSITION, position));
//
//
//            }
//        });


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}