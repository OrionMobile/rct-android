package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.RelationModel;

import java.util.ArrayList;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    Context context;
    private ArrayList<AddressModel> relationList = new ArrayList();

    public AddressAdapter(Context context, ArrayList<AddressModel> relationList) {
        this.context = context;
        this.relationList = relationList;


    }

    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.address_view, parent, false);
        AddressAdapter.ViewHolder viewHolder = new AddressAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AddressAdapter.ViewHolder holder, int position) {

        holder.tower.setText(relationList.get(position).getTower());
        holder.unitname.setText(relationList.get(position).getUnitName());
        holder.town.setText(relationList.get(position).getCity_name());
        holder.pincode.setText(relationList.get(position).getZipcode());
        holder.state.setText(relationList.get(position).getStateName());
        holder.country.setText(relationList.get(position).getCountryName());
        holder.building_name.setText(relationList.get(position).getBuilding_name());
        holder.tehsil.setText(relationList.get(position).getTehsil());
        holder.address_type.setText(relationList.get(position).getAddressType());
        holder.developer_name.setText(relationList.get(position).getDeveloper_name());

    }

    @Override
    public int getItemCount() {
        return relationList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tower, unitname, town,pincode,state,country,building_name,tehsil,address_type,developer_name;

        public ViewHolder(View itemView) {
            super(itemView);
            tower = (TextView) itemView.findViewById(R.id.tower);
            unitname = (TextView) itemView.findViewById(R.id.unitname);
            town = (TextView) itemView.findViewById(R.id.town);
            pincode = (TextView) itemView.findViewById(R.id.pincode);
            state = (TextView) itemView.findViewById(R.id.state);
            country = (TextView) itemView.findViewById(R.id.country);
            building_name = (TextView) itemView.findViewById(R.id.building_name);
            tehsil= (TextView) itemView.findViewById(R.id.tehsil);
            address_type= (TextView) itemView.findViewById(R.id.address_type);
            developer_name= (TextView) itemView.findViewById(R.id.developer_name);
        }


    }


}
