package com.bnhabitat.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.CustomViews.QustomDialogBuilder;
import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.activities.DashboardActivity;
import com.bnhabitat.ui.activities.ProjectDetailActivity;
import com.bnhabitat.ui.fragments.CustomDialogFragment;
import com.bnhabitat.ui.fragments.NotificationDialogFragment;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;


/**
 * Created by Android on 5/16/2017.
 */

public class ProjectListingAdapter extends RecyclerView.Adapter<ProjectListingAdapter.ViewHolder> {
    Context context;

    Activity activity;
    ArrayList<ProjectsDetailModel> projectListingModelArrayList = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList = new ArrayList();
    private ArrayList<ProjectsDetailModel.PropertySize> propertySizes = new ArrayList();
    private ArrayList<ProjectsDetailModel.Towers> towerses = new ArrayList();

    public ProjectListingAdapter(Context context, Activity activity,
                                 ArrayList<ProjectsDetailModel> searchProjectsModels) {
        this.context = context;
        this.activity = activity;
        this.projectListingModelArrayList = searchProjectsModels;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.project_listing_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
//      clearData();
        propertySizes = projectListingModelArrayList.get(position).getPropertySizes();
        towerses = projectListingModelArrayList.get(position).getTowers();
        projectRelationshipManagersArrayList = projectListingModelArrayList.get(position).getProjectRelationshipManagers();

        if (!propertySizes.isEmpty())
            holder.title.setText(String.valueOf(propertySizes.get(0).getTitle()));


        holder.project_type.setText(projectListingModelArrayList.get(position).getTypeOfProject());
        holder.builder_name.setText(projectListingModelArrayList.get(position).getTitle());
//        Picasso.with(context).load(Urls.BASE_IMAGE_URL + projectListingModelArrayList.get(position).getImagePath()).placeholder(context.getResources().getDrawable(R.drawable.demo_project_image)).into(holder.project_image);
//        if (propertySizes.get(0).getMinimumPrice() == null) {
//            holder.price.setText("Range : N/A");
//        } else {
//            holder.price.setText(Utils.getUnit(propertySizes.get(0).getMinimumPrice() +"Onwards",context));
//
//        }
        if (propertySizes.size() != 0) {
            if (propertySizes.get(0).getMinimumPrice()==0) {
                holder.price.setText("Range : N/A");
            } else {

                double maxPrice = Double.parseDouble(String.valueOf(propertySizes.get(0).getMaximumPrice()));
                double minPrice = Double.parseDouble(String.valueOf(propertySizes.get(0).getMinimumPrice()));
                for(ProjectsDetailModel.PropertySize propertySize:propertySizes){
                    if(minPrice>=Double.parseDouble(String.valueOf(propertySize.getMinimumPrice()))){
                        minPrice= Double.parseDouble(String.valueOf(propertySize.getMinimumPrice()));
                    }
                }
                holder.price.setText(Utils.getConvertedPrice((long) minPrice, context) + " onwards");

            }
        } else {
            holder.price.setText("Range : N/A");
        }
        if (projectListingModelArrayList.get(position).getLocality().equalsIgnoreCase("null")) {
            holder.locality.setText("N/A");
        } else {
            holder.locality.setText(projectListingModelArrayList.get(position).getLocality());

        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        imageLoader.displayImage(Urls.BASE_IMAGE_URL + projectListingModelArrayList.get(position).getCompanyLogo(), holder.company_logo, options);
        imageLoader.displayImage(Urls.BASE_IMAGE_URL + projectListingModelArrayList.get(position).getTopImage1()+"?w=300", holder.project_image, options);

        holder.user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                projectRelationshipManagersArrayList = projectListingModelArrayList.get(position).getProjectRelationshipManagers();

                if (projectRelationshipManagersArrayList.size() != 0) {
                    CustomDialogFragment fragment = CustomDialogFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Arraylist", projectRelationshipManagersArrayList);
                    fragment.setArguments(bundle);

                    fragment.show(((DashboardActivity) context).getSupportFragmentManager(), "dialog_fragment");
                }else {
                    Toast.makeText(context, "No RM associated with the project.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProjectDetailActivity.class);
                intent.putExtra("ProjectName", projectListingModelArrayList.get(position).getTitle());
                intent.putExtra("notification_array", projectListingModelArrayList);
                intent.putExtra("position", position);
                context.startActivity(intent);
            }
        });
        holder.notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                NotificationDialogFragment notificationDialogFragment = NotificationDialogFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Arraylist", projectListingModelArrayList);
                Log.e("property_size_adapter", String.valueOf(projectListingModelArrayList.get(position).getPropertySizes().size()));
                bundle.putInt("Position", position);
                notificationDialogFragment.setArguments(bundle);
                notificationDialogFragment.show(((DashboardActivity) context).getSupportFragmentManager(), "notification_dialog_fragment");

            }
        });
        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, projectListingModelArrayList.get(position).getTitle());

                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://bnhabitat.com/?"+"p="+projectListingModelArrayList.get(position).getId());
               context. startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        });
        holder.info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (projectListingModelArrayList.get(position).getCurrentOffers().equalsIgnoreCase("")||
                projectListingModelArrayList.get(position).getCurrentOffers().equalsIgnoreCase("null")
                        || projectListingModelArrayList.get(position).getCurrentOffers() == null) {

                    Toast.makeText(context, "No offers yet", Toast.LENGTH_SHORT).show();

                } else {
                    new AlertDialog.Builder(context)
                            .setMessage(projectListingModelArrayList.get(position).getCurrentOffers())
                            .setPositiveButton("NOTED", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();

                }


            }

        });
        holder.company_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(context).
                        setTitle(projectListingModelArrayList.get(position).getTitle()).
                        setTitleColor("#2a2340").
                        setDividerColor("#2a2340").
                        setMessage(Html.fromHtml(projectListingModelArrayList.get(position).getAboutSubProject()));
                qustomDialogBuilder.show();

//
            }
        });
    }

    public void clearData() {
        propertySizes.clear();
        towerses.clear();
        projectRelationshipManagersArrayList.clear();
    }

    @Override
    public int getItemCount() {
        return projectListingModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView company_logo, info_icon, share_icon, notification_icon, user_icon, project_image;
        TextView title, locality, project_type, builder_name, price;
        LinearLayout view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = (LinearLayout) itemView.findViewById(R.id.view);
            company_logo = (ImageView) itemView.findViewById(R.id.company_logo);
            info_icon = (ImageView) itemView.findViewById(R.id.info_icon);
            share_icon = (ImageView) itemView.findViewById(R.id.share_icon);
            notification_icon = (ImageView) itemView.findViewById(R.id.notification_icon);
            user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
            project_image = (ImageView) itemView.findViewById(R.id.project_image);
            title = (TextView) itemView.findViewById(R.id.title);
            project_type = (TextView) itemView.findViewById(R.id.project_type);
            builder_name = (TextView) itemView.findViewById(R.id.builder_name);
            price = (TextView) itemView.findViewById(R.id.price);
            locality = (TextView) itemView.findViewById(R.id.locality);
        }
    }

}
