package com.bnhabitat.ui.adapters;


import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.FeatureModel;
import com.bnhabitat.models.OtherExpensesModel;
import com.bnhabitat.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.greenrobot.event.EventBus;

public class OtherExpensedAdapter extends RecyclerView.Adapter<OtherExpensedAdapter.ViewHolder> {
    Context context;
    private ArrayList<OtherExpensesModel> otherrExpensArrayList = new ArrayList();
    private ArrayList<OtherExpensesModel> otherrExpensArrayListLocal = new ArrayList();
    int mYear, mMonth, mDay;

    public OtherExpensedAdapter(Context context, ArrayList<OtherExpensesModel> feature_checked_list) {
        this.context = context;
        this.otherrExpensArrayList = feature_checked_list;
        for (int i = 0; i < otherrExpensArrayList.size(); i++) {
            OtherExpensesModel otherExpensesModel = new OtherExpensesModel();
            otherExpensesModel.setId(otherrExpensArrayList.get(i).getId());
            otherExpensesModel.setTitle(otherrExpensArrayList.get(i).getTitle());
            otherExpensesModel.setValue(otherrExpensArrayList.get(i).getValue());
            otherExpensesModel.setLastPaidDate(otherrExpensArrayList.get(i).getLastPaidDate());
            otherrExpensArrayListLocal.add(otherExpensesModel);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_otherexpenses, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tv_title.setText(otherrExpensArrayList.get(position).getTitle());

        holder.et_value.setText(otherrExpensArrayList.get(position).getValue());

        // if date format is fine then don't set the date

        if (!otherrExpensArrayList.get(position).getLastPaidDate().equalsIgnoreCase("null")|| !otherrExpensArrayList.get(position).getLastPaidDate().equalsIgnoreCase("0")   ){

            holder.tv_date.setText(Utils.getMessageDate(otherrExpensArrayList.get(position).getLastPaidDate()));

        }
        else{

            holder.tv_date.setText("0");

        }

        holder.tv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentDate1 = Calendar.getInstance();
                mYear = mcurrentDate1.get(Calendar.YEAR);
                mMonth = mcurrentDate1.get(Calendar.MONTH);
                mDay = mcurrentDate1.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker1 = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        /*      Your code   to get date and time    */

                        String.format("%02d", selectedmonth + 1);
                        String dd= String.format("%02d", selectedmonth + 1) + "/" + selectedday + "/" + selectedyear;
                        holder.tv_date.setText(dd);



                        //   holder.tv_date.setText(selectedday + "/" + (selectedmonth + 1) + "/" + selectedyear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker1.setTitle("Select Last Paid Date");
                mDatePicker1.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker1.show();

            }
        });

        holder.tv_date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                otherrExpensArrayListLocal.get(position).setId(otherrExpensArrayList.get(position).getId());
                otherrExpensArrayListLocal.get(position).setTitle(otherrExpensArrayList.get(position).getTitle());
                otherrExpensArrayListLocal.get(position).setValue(holder.et_value.getText().toString());
                otherrExpensArrayListLocal.get(position).setLastPaidDate(holder.tv_date.getText().toString());
                EventBus.getDefault().post(otherrExpensArrayListLocal);

            }

            @Override
            public void afterTextChanged(Editable s) {

                otherrExpensArrayListLocal.get(position).setId(otherrExpensArrayList.get(position).getId());
                otherrExpensArrayListLocal.get(position).setTitle(otherrExpensArrayList.get(position).getTitle());
                otherrExpensArrayListLocal.get(position).setValue(holder.et_value.getText().toString());
                otherrExpensArrayListLocal.get(position).setLastPaidDate(holder.tv_date.getText().toString());
                EventBus.getDefault().post(otherrExpensArrayListLocal);


                if(holder.tv_date.getText().equals("")){

                    holder.et_value.setText("0");
                }

            }
        });

        holder.et_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0){
                    holder.tv_date.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


                otherrExpensArrayListLocal.get(position).setId(otherrExpensArrayList.get(position).getId());
                otherrExpensArrayListLocal.get(position).setTitle(otherrExpensArrayList.get(position).getTitle());
                otherrExpensArrayListLocal.get(position).setValue(holder.et_value.getText().toString());
                otherrExpensArrayListLocal.get(position).setLastPaidDate(holder.tv_date.getText().toString());
                EventBus.getDefault().post(otherrExpensArrayListLocal);



            }
        });

    }

    @Override
    public int getItemCount() {
        return otherrExpensArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_date;
        EditText et_value;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            et_value = (EditText) itemView.findViewById(R.id.et_value);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
        }
    }

}
