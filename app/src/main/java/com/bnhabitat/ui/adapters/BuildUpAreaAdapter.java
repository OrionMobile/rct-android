package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.bnhabitat.R;
import java.util.ArrayList;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class BuildUpAreaAdapter extends RecyclerView.Adapter<BuildUpAreaAdapter.ViewHolder> {

    Context context;
    ArrayList<String> arealist=new ArrayList<>();
    ArrayList<String> property_units = new ArrayList<>();
    SpinnerAdapter adapter;
    String number_count;
    Intent intent;

    public BuildUpAreaAdapter(Context context,ArrayList<String> arealist, ArrayList<String> property_units) {
        this.context = context;
        this.arealist=arealist;
        this.property_units=property_units;
        adapter = new SpinnerAdapter(context, R.layout.new_spinner_item);
        adapter.addAll(property_units);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.otherside_view_inflate1, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.remove_others.setTag(position);

        holder.other_side.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT) {
                    number_count = holder.other_side.getText().toString();
                    EventBus.getDefault().post(number_count);
                    return true;
                }
                return false;
            }
        });

        holder.other_side_select.setAdapter(adapter);

        holder.other_side_select.setSelection(position);

        holder.remove_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(context.getString(R.string.message))
                        .setContentText(context.getString(R.string.are_you_sure))
                        .setConfirmText(context.getString(R.string.yes))
                        .setCancelText(context.getString(R.string.cancel))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                int i=(Integer)view.getTag();
                                arealist.remove(i);
                                notifyDataSetChanged();
                            }
                        })
                        .show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return arealist.size();
    }
    public  class  ViewHolder extends RecyclerView.ViewHolder {

        EditText other_side;
        Spinner other_side_select;
        ImageView remove_others;
        LinearLayout remove_other_layout;
        public ViewHolder(View itemView) {
            super(itemView);
            other_side_select = (Spinner) itemView.findViewById(R.id.other_side_select);
            remove_others = (ImageView) itemView.findViewById(R.id.remove_other);
            remove_other_layout = (LinearLayout) itemView.findViewById(R.id.remove_other_layout);
            other_side = (EditText) itemView.findViewById(R.id.other_side);
//
        }

    }
}

