package com.bnhabitat.ui.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.MyinvitesModel;
import com.kyleduo.switchbutton.SwitchButton;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by gourav on 9/22/2017.
 */

public class MyinvitesAdapter extends RecyclerView.Adapter<MyinvitesAdapter.ViewHolder> {


    Activity context;
    List<MyinvitesModel> myinvitesModels;




    public MyinvitesAdapter(Activity activity, List<MyinvitesModel> myinvitesModels) {
        this.context = activity;
        this.myinvitesModels = myinvitesModels;

    }




    @Override
    public MyinvitesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_view_items, parent, false);
        MyinvitesAdapter.ViewHolder viewHolder = new MyinvitesAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyinvitesAdapter.ViewHolder holder, final int i) {

        holder.details_contact.setVisibility(View.GONE);
        if(myinvitesModels.get(i).getEmail().isEmpty()) {
            holder.email.setImageDrawable(context.getResources().getDrawable(R.drawable.email_grey));
            holder.email.setEnabled(false);
        }
        else{
            holder.email.setImageDrawable(context.getResources().getDrawable(R.drawable.mail_icon));
            holder.email.setEnabled(true);
        }

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EventBus.getDefault().post(myinvitesModels.get(i).getPhone());
//                Log.e("i=", String.valueOf(myinvitesModels.get(i).getPhone()));

            }
        });
//        getPhones= PhoneTable.getInstance().getPhones(contactsFirebaseModels.get(i).getId());
        holder.country.setText(myinvitesModels.get(i).getFirstName());
        try {
            holder.contact_number.setText(myinvitesModels.get(i).getPhone());
        }catch (Exception e){
            e.printStackTrace();
        }
//
//        holder.details_contact.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                PreferenceConnector.getInstance(context).savePreferences("ContactName",contactsFirebaseModels.get(i).getName());
////                PreferenceConnector.getInstance(context).savePreferences("ContactNumber",contactsFirebaseModels.get(i).getPhonenumber());
//
//                Intent intent=new Intent(context,CreateNewGroupActivity.class);
//                intent.putExtra(Constants.ARRAY_LIST, (Serializable) contactsFirebaseModels);
//                context.startActivity(intent);
//            }
//        });
//        contact_lay.setTag(R.id.contact_lay,i);
//        holder.contact_lay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                PreferenceConnector.getInstance(context).savePreferences("ContactName",contactsFirebaseModels.get(i).getName());
////                PreferenceConnector.getInstance(context).savePreferences("ContactNumber",contactsFirebaseModels.get(i).getPhonenumber());
//                Intent intent = new Intent(context, AddContatctActivity.class);
//                intent.putExtra(contactdetail, contactsFirebaseModels.get(i));
////                intent.putExtra(contactdetail, contactsFirebaseModels.get(i));
//                intent.putExtra("update", "update_contact");
//                intent.putExtra("position", i);
//                context.startActivity(intent);
//            }
//        });
        holder.sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  try {
                      Uri uri = Uri.parse("smsto:" + myinvitesModels.get(i).getPhone());
                      Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                      it.putExtra("sms_body", "");
                      context.startActivity(it);
                  }catch (Exception e){
                      e.printStackTrace();
                  }


            }
        });
        holder.email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { myinvitesModels.get(i).getEmail() });
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, "mail body");
                context.startActivity(Intent.createChooser(intent, ""));

//
            }
        });
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return myinvitesModels.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView call, sms, email,details_contact;
        TextView country,contact_number;
        LinearLayout contact_lay,contact_name_lay;
        SwitchButton dots;
        public ViewHolder(View itemView) {
            super(itemView);
            contact_lay = (LinearLayout) itemView.findViewById(R.id.contact_lay);
            country = (TextView) itemView.findViewById(R.id.contact_name);
            contact_number = (TextView) itemView.findViewById(R.id.contact_number);
            contact_name_lay=(LinearLayout)itemView.findViewById(R.id.contact_name_lay);
            call = (ImageView) itemView.findViewById(R.id.call);
            sms = (ImageView) itemView.findViewById(R.id.sms);
            email = (ImageView) itemView.findViewById(R.id.email);
            details_contact = (ImageView) itemView.findViewById(R.id.details_contact);
//
        }


    }
}
