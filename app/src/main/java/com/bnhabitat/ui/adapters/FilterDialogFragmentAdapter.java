package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.CommonDialogModel;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by Android on 6/30/2017.
 */
public class FilterDialogFragmentAdapter extends RecyclerView.Adapter<FilterDialogFragmentAdapter.ViewHolder>{


    Context context;
    private ArrayList<String> projectStatus = new ArrayList();

    public FilterDialogFragmentAdapter(Context context, ArrayList<String> projectStatus) {
        this.context = context;
        this.projectStatus = projectStatus;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.filter_dialog_item,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.name_of_filter.setText(projectStatus.get(position));
        holder.name_of_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CommonDialogModel commonDialogModel=new CommonDialogModel();
                commonDialogModel.setTitle(projectStatus.get(position));
                EventBus.getDefault().post(commonDialogModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return projectStatus.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name_of_filter;

        public ViewHolder(View itemView) {
            super(itemView);
            name_of_filter=(TextView)itemView.findViewById(R.id.name_of_filter);

        }
    }
}
