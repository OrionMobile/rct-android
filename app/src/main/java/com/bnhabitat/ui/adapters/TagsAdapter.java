package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.AddressModel;
import com.bnhabitat.models.TagModel;

import java.util.ArrayList;

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolder> {
    Context context;
    private ArrayList<String> relationList = new ArrayList();

    public TagsAdapter(Context context, ArrayList<String> relationList) {
        this.context = context;
        this.relationList = relationList;


    }

    @Override
    public TagsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.tagview, parent, false);
        TagsAdapter.ViewHolder viewHolder = new TagsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TagsAdapter.ViewHolder holder, int position) {

        holder.tv_tag.setText(relationList.get(position));

    }

    @Override
    public int getItemCount() {
        return relationList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_tag;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_tag = (TextView) itemView.findViewById(R.id.tv_tag);

        }


    }


}
