package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bnhabitat.R;

/**
 * Created by gourav on 5/19/2017.
 */

public class PricePaymentAdapter extends RecyclerView.Adapter<PricePaymentAdapter.ViewHolder> {
    Context context;


    public PricePaymentAdapter(Context context) {
        this.context = context;

    }

    @Override
    public PricePaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.price_pay_item, parent, false);
        PricePaymentAdapter.ViewHolder viewHolder = new PricePaymentAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PricePaymentAdapter.ViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View itemView) {
            super(itemView);
//            name = (TextView) itemView.findViewById(R.id.name);
//            info_icon = (ImageView) itemView.findViewById(R.id.info_icon);
//            share_icon = (ImageView) itemView.findViewById(R.id.share_icon);
//            notification_icon = (ImageView) itemView.findViewById(R.id.notification_icon);
//            user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
        }
    }
}
