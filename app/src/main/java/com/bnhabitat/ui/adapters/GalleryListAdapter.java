package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel_Area;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.activities.ZoomActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by gourav on 5/21/2018.
 */

public class GalleryListAdapter extends RecyclerView.Adapter<GalleryListAdapter.ViewHolder> {

    Context context;

    private ArrayList<InventoryModel_Area.PropertyImage> propertyImages = new ArrayList();
    public GalleryListAdapter(Context context, ArrayList<InventoryModel_Area.PropertyImage> propertyImages) {
        this.context = context;
        this.propertyImages = propertyImages;


    }
    @Override
    public GalleryListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(R.layout.gallery_images, parent, false);
        GalleryListAdapter.ViewHolder viewHolder = new GalleryListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GalleryListAdapter.ViewHolder holder, final int position) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        if (!propertyImages.isEmpty())
            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + propertyImages.get(position).getUrl(), holder.project_image, options);

        ProjectsDetailModel.LayoutPlans layoutPlans=new ProjectsDetailModel().new LayoutPlans();

        holder.project_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context,ZoomActivity.class);
                intent.putExtra("layout_plan","image_gallery");
                intent.putExtra(Constants.ZOOM_LIST,propertyImages);
                intent.putExtra("position",String.valueOf(position));
                context.startActivity(intent);
//                    getActivity().finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return propertyImages.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView project_image;


        public ViewHolder(View itemView) {
            super(itemView);
            project_image = (ImageView) itemView.findViewById(R.id.project_image);
        }
    }
}
