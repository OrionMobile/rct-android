//package com.bnhabitat.ui.adapters;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.inputmethod.EditorInfo;
//import android.widget.AdapterView;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.Spinner;
//import android.widget.TextView;
//
//import com.bnhabitat.R;
//import com.bnhabitat.models.CommonDialogModel;
//import com.bnhabitat.models.FloorCalculationModel;
//import com.bnhabitat.models.StatusModel;
//
//import java.util.ArrayList;
//
//import cn.pedant.SweetAlert.SweetAlertDialog;
//import de.greenrobot.event.EventBus;
//
//
//
//
//
//public class FloornumberAdapter extends RecyclerView.Adapter<FloornumberAdapter.ViewHolder> {
//
//    Context context;
//    ArrayList<FloorCalculationModel> floorNumberList = new ArrayList<>();
//    String number_count;
//    private String select_unit_txt, select_unit_id;
//
//    public FloornumberAdapter(Context context, ArrayList<FloorCalculationModel> floorNumberList) {
//        this.context = context;
//        this.floorNumberList = floorNumberList;
//
//    }
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.floor_layout, parent, false);
//        ViewHolder viewHolder = new ViewHolder(view);
//        return viewHolder;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }
//
//    @Override
//    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        holder.floor_minus.setTag(position);
//
//        if (floorNumberList.size() > 1) {
//
//            holder.floor_minus.setVisibility(View.VISIBLE);
//        } else {
//
//            holder.floor_minus.setVisibility(View.GONE);
//        }
//
//        holder.select_floor_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                // TODO Auto-generated method stub
//
//                if (holder.select_floor_unit.getSelectedItem() == (R.string.select_type)) {
//
//                    //Do nothing.
//                } else {
//
//                    select_unit_txt = holder.select_floor_unit.getSelectedItem().toString();
//                    select_unit_id = propertyUnitsModels.get(position).getId();
//
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//
//            }
//        });
//
////        holder.floor_number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
////           @Override
////           public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
////               if (i == EditorInfo.IME_ACTION_NEXT) {
////                   FloorCalculationModel floorCalculationModel=new FloorCalculationModel();
////                   number_count = holder.floor_number.getText().toString();
////                   floorCalculationModel.setCount(number_count);
////                   floorCalculationModel.setPosition(position);
////                   EventBus.getDefault().post(floorCalculationModel);
//////                   floor_number.setText("");
//////                no_room.add(""+position+1);
////
////                   return true;
////               }
////               return false;
////           }
////       });
//
//        holder.floor_number.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//                FloorCalculationModel floorCalculationModel = new FloorCalculationModel();
//                number_count = holder.floor_number.getText().toString();
//                floorCalculationModel.setCount(number_count);
//                floorCalculationModel.setDelete("No");
//                int pos = position;
//                floorCalculationModel.setPosition(pos);
//                EventBus.getDefault().post(floorCalculationModel);
//            }
//
//        });
//
//        holder.floor_minus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//
//                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
//                        .setTitleText(context.getString(R.string.message))
//                        .setContentText(context.getString(R.string.are_you_sure))
//                        .setConfirmText(context.getString(R.string.yes))
//                        .setCancelText(context.getString(R.string.cancel))
//                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                            @Override
//                            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                sweetAlertDialog.dismissWithAnimation();
//                            }
//                        })
//
//                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                            @Override
//                            public void onClick(SweetAlertDialog sDialog) {
//                                sDialog.dismissWithAnimation();
//                                int pos = position;
//                                floorNumberList.remove(pos);
//                                FloorCalculationModel floorCalculationModel = new FloorCalculationModel();
//                                floorCalculationModel.setDeletePos(pos);
//                                floorCalculationModel.setDelete("Yes");
//                                EventBus.getDefault().post(floorCalculationModel);
//                                notifyDataSetChanged();
//                            }
//                        })
//                        .show();
//
//
//            }
//        });
//    }
//
//    @Override
//    public int getItemCount() {
//        return floorNumberList.size();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//
//        EditText floor_size, floor_number;
//        ImageView floor_minus;
//        Spinner select_floor_unit;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//            floor_size = (EditText) itemView.findViewById(R.id.floor_size);
//            floor_minus = (ImageView) itemView.findViewById(R.id.floor_minus);
//            floor_number = (EditText) itemView.findViewById(R.id.floor_number);
//            select_floor_unit = (Spinner) itemView.findViewById(R.id.select_floor_unit);
//
//        }
//
//
//    }
//}
