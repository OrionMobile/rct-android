package com.bnhabitat.ui.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;
import com.kyleduo.switchbutton.SwitchButton;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 3/19/2018.
 */

public class RoomAttachedDataAdapter extends RecyclerView.Adapter<RoomAttachedDataAdapter.ViewHolder>  {
    Context context;
    private ArrayList<PropertyBedroomModel> room_data = new ArrayList();
    private ArrayList<ProjectsModel> tempArraylist;
    int id,position1,p;
    StatusModel statusModel;
    ArrayList<String>ids=new ArrayList<>();
    boolean ispublished=false;
    ArrayList<Boolean>booleanArrayList=new ArrayList<>();
    public RoomAttachedDataAdapter() {


    }
    public RoomAttachedDataAdapter(Context context,ArrayList<PropertyBedroomModel> room_data, int pos) {
        this.context = context;
        this.room_data=room_data;
        for(int i=0;i<room_data.size();i++){
            booleanArrayList.add(false);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.room_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.room_name_attached_txt.setText(room_data.get(position).getRoom_name());
//        holder.room_name_attached_txt.setMovementMethod(new ScrollingMovementMethod());
        ArrayList<PropertyBedroomModel.AttachPropertyBedroomId> arrAttachRoomIds = room_data.get(position).getAttachPropertyBedroomIds();

        for (PropertyBedroomModel.AttachPropertyBedroomId attachPropertyBedroomId :arrAttachRoomIds){
            if (room_data.get(position).getRoom_name().equalsIgnoreCase(attachPropertyBedroomId.getAttachPropertyBedroomName())){
                holder.attached_check.setChecked(true);
            }else {
                holder.attached_check.setChecked(false);
            }
        }
        holder.room_name_attached_txt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {


                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){

                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                    alertDialog.setTitle("Room Name");
                    alertDialog.setMessage(room_data.get(position).getRoom_name());
                    alertDialog.show();

                }

                return true;
            }
        });
////        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
//                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
//                .build();
//
//        ImageLoader imageLoader = ImageLoader.getInstance();
//        if (!imageLoader.isInited()) {
//            imageLoader.init(config);
//        }
//        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                .cacheOnDisc(true).resetViewBeforeLoading(true)
//                .showImageForEmptyUri(R.drawable.image_logo)
//                .showImageOnFail(R.drawable.image_logo)
//                .showImageOnLoading(R.drawable.image_logo).build();
//        imageLoader.displayImage(Urls.BASE_IMAGE_URL + tempArraylist.get(position).getTopImage1(), holder.property_image, options);
        holder.attached_check.setTag(position);
        holder.attached_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    id=(Integer)buttonView.getTag();

                    booleanArrayList.set(id,true);
                    ids.clear();
                    for(int i=0;i<booleanArrayList.size();i++){



                        if(booleanArrayList.get(i).booleanValue()==true){
                            ids.add(room_data.get(i).getId());
                             statusModel=new StatusModel();
                            statusModel.setIds(ids);
                            statusModel.setStatus("checked");

                        }
                    }


                } else {
                    booleanArrayList.set(id,false);
                    ids.clear();
                    for(int i=0;i<booleanArrayList.size();i++){



                        if(booleanArrayList.get(id).booleanValue()==true){
                            ids.add(room_data.get(i).getId());
                             statusModel=new StatusModel();
                            statusModel.setIds(ids);
                            statusModel.setStatus("checked");
                        }
                    }
                }
                EventBus.getDefault().post(statusModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return room_data.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView room_image;
        TextView room_name_attached_txt;
        CheckBox attached_check;

        public ViewHolder(View itemView) {
            super(itemView);
            room_name_attached_txt = (TextView) itemView.findViewById(R.id.room_name_attached_txt);

            attached_check = (CheckBox) itemView.findViewById(R.id.attached_check);


        }


    }




}
