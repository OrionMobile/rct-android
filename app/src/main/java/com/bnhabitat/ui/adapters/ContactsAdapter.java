package com.bnhabitat.ui.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.ui.activities.ProfileActivity;
import com.bnhabitat.ui.fragments.ContactFragment;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Android on 1/16/2017.
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    ArrayList<Boolean> arrayList = new ArrayList<>();
    ArrayList<String> ids = new ArrayList<>();
    Activity context;
    List<ContactsModel> contactsFirebaseModels = new ArrayList<>();

    LayoutInflater inflter;
    ArrayList<ContactsModel.Email> emails;
    OnItemClick onItemClick;

    IsCheckBoxShown IsCheckBoxShown;

    public static boolean isVisible = false;
    int p;

    public static final String contactdetail = "contact_detail";
    int count = 0;

    public interface OnItemClick {
        public void onItemClick(ArrayList<String> strings);
    }

    public interface IsCheckBoxShown {
        public void onCheckClick(boolean isClick);
    }



    public ContactsAdapter(Activity activity, List<ContactsModel> contactsFirebaseModel, OnItemClick onItemClick, IsCheckBoxShown IsCheckBoxShown) {
        this.context = activity;
      // this.contactsFirebaseModels = contactsFirebaseModels;

        this.onItemClick = onItemClick;
        this.IsCheckBoxShown = IsCheckBoxShown;
//       this.contactsFirebaseModels=contactsFirebaseModel;
        for (ContactsModel contactModel : contactsFirebaseModel) {



           if(!contactModel.getFirstName().equalsIgnoreCase("")){
               contactsFirebaseModels.add(contactModel);
            }
            arrayList.add(false);
        }



        ContactFragment.setContactSize(contactsFirebaseModels.size());

    }
/*
    public ContactsAdapter(Activity activity, List<ContactsModel> contactsFirebaseModels, OnItemClick onItemClick, IsCheckBoxShown IsCheckBoxShown) {
        this.context = activity;
        this.contactsFirebaseModels = contactsFirebaseModels;

        this.onItemClick = onItemClick;
        this.IsCheckBoxShown = IsCheckBoxShown;

        for (ContactsModel contactModel : contactsFirebaseModels) {
            arrayList.add(false);
        }


        ContactFragment.setContactSize(contactsFirebaseModels.size());

    }
*/


    @Override

    public int getItemViewType(int position) {

        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.contact_item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int i) {


        if (i == 0) {
            holder.upr_divider.setVisibility(View.VISIBLE);
        }

        try {
            if(contactsFirebaseModels.get(i).getEmail()!=null)
            if (contactsFirebaseModels.get(i).getEmail().isEmpty()) {
                holder.imgMsg.setImageDrawable(context.getResources().getDrawable(R.drawable.email_grey));
                holder.imgMsg.setEnabled(false);
            } else {
                holder.imgMsg.setImageDrawable(context.getResources().getDrawable(R.drawable.mail_icon));
                holder.imgMsg.setEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (isVisible) {
            holder.check_contacts.setVisibility(View.VISIBLE);
            holder.moreButton.setVisibility(View.GONE);
        } else {
            holder.check_contacts.setVisibility(View.GONE);
            holder.moreButton.setVisibility(View.VISIBLE);
        }

        holder.check_contacts.setChecked(false);


        holder.contact_name_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ids.isEmpty()) {


//                PreferenceConnector.getInstance(context).savePreferences("ContactName",contactsFirebaseModels.get(i).getName());
//                PreferenceConnector.getInstance(context).savePreferences("ContactNumber",contactsFirebaseModels.get(i).getPhonenumber());
                    Intent intent = new Intent(context, ProfileActivity.class);
                    intent.putExtra("contact_id", contactsFirebaseModels.get(i).getId());
//                intent.putExtra(contactdetail, contactsFirebaseModels.get(i));
//                intent.putExtra("update", "update_contact");
//                intent.putExtra("position", i);
                    context.startActivity(intent);


                } else {

                    arrayList.set(i, true);
                    //  check_contacts.setVisibility(View.VISIBLE);
                    ids.clear();
                    for (int i = 0; i < arrayList.size(); i++) {

                        if (arrayList.get(i).booleanValue() == true) {
                            ids.add(contactsFirebaseModels.get(i).getId());
                        }


                    }
                    onItemClick.onItemClick(ids);
                }


//                Intent intent = new Intent(context, ProfileActivity.class);
//                intent.putExtra("contact_id", contactsFirebaseModels.get(i).getId());
//                context.startActivity(intent);

            }
        });

        holder.lnrMoreCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogShowDetail(contactsFirebaseModels.get(i),i);
            }
        });

        holder.lnrMoreCheck.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {


                isVisible = true;
                notifyDataSetChanged();

                IsCheckBoxShown.onCheckClick(true);

                return true;
            }
        });

        holder.check_contacts.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                p = i;
                arrayList.set(p, true);
                ids.clear();
                for (int i = 0; i < arrayList.size(); i++) {

                    if (arrayList.get(i).booleanValue() == true) {
                        ids.add(contactsFirebaseModels.get(i).getId());
                    }


                }


                onItemClick.onItemClick(ids);

            }
        });


        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CALL_PHONE}, 198);
                } else {
                    callIntent.setData(Uri.parse("tel:" + contactsFirebaseModels.get(i).getPhonenumber()));
                    context.startActivity(callIntent);
                }
            }
        });


        holder.imgMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");

                final PackageManager pm = context.getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
                ResolveInfo best = null;
                for (final ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail")) {
                        best = info;
                        break;
                    }
                }
                if (best != null) {
                    intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                }

                context.startActivity(intent);


            }
        });


        holder.country.setText(contactsFirebaseModels.get(i).getFirstName() + " " + Utils.getEmptyValue(contactsFirebaseModels.get(i).getLastName()));

        try {
            if (!contactsFirebaseModels.get(i).getEmail().isEmpty()) {
                holder.email_id.setText(contactsFirebaseModels.get(i).getEmail());
            } else {
                holder.email_id.setText("None");
            }
        } catch (Exception e) {
            if (!contactsFirebaseModels.get(i).getEmails().isEmpty()) {
                holder.email_id.setText(contactsFirebaseModels.get(i).getEmails().get(0).getAddress());
            } else {
                holder.email_id.setText("None");
            }

        }

        try {
            if(contactsFirebaseModels.get(i).getPhonenumber()!=null)
            if (!contactsFirebaseModels.get(i).getPhonenumber().isEmpty()) {
                holder.contact_number.setText(contactsFirebaseModels.get(i).getPhonenumber());
            } else {
                holder.contact_number.setText("None");
            }
        } catch (Exception e) {
            holder.contact_number.setText("None");

//          holder.contact_number.setText(contactsFirebaseModels.get(i).getPhones().get(0).getPhoneNumber());
            e.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {

        return contactsFirebaseModels.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView call, sms, email, imgCall, imgMsg, moreButton;
        TextView country, contact_number, email_id;
        LinearLayout contact_lay, contact_name_lay, lnrMoreCheck;
        View upr_divider;
        //        ItemLongClickListener itemLongClickListener;

        CheckBox check_contacts;

        public ViewHolder(final View itemView) {
            super(itemView);
            contact_lay = (LinearLayout) itemView.findViewById(R.id.contact_lay);
            country = (TextView) itemView.findViewById(R.id.contact_name);
            contact_number = (TextView) itemView.findViewById(R.id.contact_number);
            email_id = (TextView) itemView.findViewById(R.id.email_id);
            contact_name_lay = (LinearLayout) itemView.findViewById(R.id.contact_name_lay);
            check_contacts = (CheckBox) itemView.findViewById(R.id.check_contacts);
            upr_divider = (View) itemView.findViewById(R.id.upr_divider);
            imgCall = (ImageView) itemView.findViewById(R.id.imgCall);
            imgMsg = (ImageView) itemView.findViewById(R.id.imgMsg);
            moreButton = (ImageView) itemView.findViewById(R.id.moreButton);
            lnrMoreCheck = (LinearLayout) itemView.findViewById(R.id.lnrMoreCheck);


        }


    }

    public void setChange(boolean isCheck) {
        isVisible = isCheck;
        notifyDataSetChanged();

    }


    private void dialogShowDetail(ContactsModel models, final int k) {

        final Dialog planDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        View v = context.getLayoutInflater().inflate(R.layout.custom_contact_detail, null);

        Button btnClose = (Button) v.findViewById(R.id.btnClose);
        TextView phn = (TextView) v.findViewById(R.id.phn);
        TextView phn1 = (TextView) v.findViewById(R.id.phn1);
        TextView phn2 = (TextView) v.findViewById(R.id.phn2);
        Button btnviewAll = (Button) v.findViewById(R.id.btnviewAll);


        ImageView iv_fb =(ImageView) v.findViewById(R.id.iv_fb);
        ImageView iv_twitter =(ImageView) v.findViewById(R.id.iv_twitter);
        ImageView iv_share =(ImageView) v.findViewById(R.id.iv_share);
        String fbLink = "";
        String twitterLink = "";

        if (!models.getContactSocialDetails().isEmpty()) {

            for (int i=0;i<models.getContactSocialDetails().size();i++){

                if (models.getContactSocialDetails().get(i).getType().equalsIgnoreCase("facebook")){

                    fbLink =models.getContactSocialDetails().get(i).getDetail();

                }

                if (models.getContactSocialDetails().get(i).getType().equalsIgnoreCase("twitter")){

                    twitterLink = models.getContactSocialDetails().get(i).getDetail();

                }

            }

        }


         final String finalFbLink = fbLink;
        iv_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(finalFbLink));
                context.startActivity(browserIntent);
            }
        });
        final String finalTwitterLink = twitterLink;
        iv_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(finalTwitterLink));
                context.startActivity(browserIntent);
            }
        });

    /*    iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                context.startActivity(browserIntent);
            }
        });*/

        TextView email = (TextView) v.findViewById(R.id.email);
       // TextView email1 = (TextView) v.findViewById(R.id.email1);
        CircleImageView profile_img =(CircleImageView) v.findViewById(R.id.profile_img);

        TextView name =(TextView) v.findViewById(R.id.name);

        if(models.getLastName()!=null){
            if (!models.getLastName().equalsIgnoreCase("None"))
                name.setText(models.getFirstName() + " " + models.getLastName());
            else {
                name.setText(models.getFirstName());
            }
        }
        else {
            name.setText(models.getFirstName());
        }


        if(models.getImageUrl()!=null){
            if (!models.getImageUrl().equalsIgnoreCase("None")) {


                Picasso.with(context).load(Urls.BASE_CONTACT_IMAGE_URL + models.getImageUrl()).placeholder(R.drawable.user_profile_img).into(profile_img);

            }

            else{
                profile_img.setImageResource(R.drawable.user_profile_img);

            }
        }
        else {
            profile_img.setImageResource(R.drawable.user_profile_img);
        }



        phn.setText(models.getPhonenumber());

//        if (!models.getPhonenumber().isEmpty()) {
//            for (int i = 0; i < models.getPhones().size(); i++) {
//
//                if (i == 0) {
//                    phn.setText(models.getPhones().get(i).getPhoneNumber());
//                } else if (i == 1) {
//                    phn1.setText(models.getPhones().get(i).getPhoneNumber());
//                } else if (i == 2) {
//                    phn2.setText(models.getPhones().get(i).getPhoneNumber());
//                }
//
//
//            }
//        } else {
//            phn.setText("None");
//            phn1.setText("None");
//            phn2.setText("None");
//        }


        if (!models.getEmail().isEmpty()) {

/*
            for (int i = 0; i < models.getEmail().size(); i++) {

                if (i == 0) {
                    email.setText(models.getEmail().get(i).getAddress());
                } else if (i == 1) {
                    email1.setText(models.getPhones().get(i).getPhoneNumber());
                }

                }
*/
            email.setText(models.getEmail());



        } else {
            email.setText("None");
           // email1.setText("None");
        }

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                planDialog.dismiss();
            }
        });

        btnviewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ids.isEmpty()) {


                    Intent intent = new Intent(context, ProfileActivity.class);
                    intent.putExtra("contact_id", contactsFirebaseModels.get(k).getId());
                    context.startActivity(intent);


                } else {

                    arrayList.set(k, true);
                    ids.clear();
                    for (int i = 0; i < arrayList.size(); i++) {

                        if (arrayList.get(i).booleanValue() == true) {
                            ids.add(contactsFirebaseModels.get(i).getId());
                        }


                    }
                    onItemClick.onItemClick(ids);
                }


                planDialog.dismiss();
            }
        });

        planDialog.setCancelable(false);
        planDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        planDialog.setContentView(v);
        planDialog.show();

    }

    public void removeAllValues(){
        contactsFirebaseModels.clear();
        notifyDataSetChanged();
    }
}

