package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsDetailModel;

import java.util.ArrayList;

/**
 * Created by Android on 5/17/2017.
 */

public class NotificationDialogAdapter extends RecyclerView.Adapter<NotificationDialogAdapter.ViewHolder> {
    Context context;
    ArrayList<ProjectsDetailModel> projectListingModelArrayList=new ArrayList<>();

    public NotificationDialogAdapter(Context context,  ArrayList<ProjectsDetailModel> projectListingModelArrayList) {
        this.context = context;
        this.projectListingModelArrayList = projectListingModelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.notification_dialog_items,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return projectListingModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
