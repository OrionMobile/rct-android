package com.bnhabitat.ui.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.ui.views.MultiSelectionSpinner;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 3/29/2018.
 */

public class SpinnerCommonAdapter extends RecyclerView.Adapter<SpinnerCommonAdapter.CustomViewHolder> {
    private ArrayList<CommonDialogModel> commonModels;
    private Context mContext;
    private Dialog dialog;

    public SpinnerCommonAdapter(Context context, ArrayList<CommonDialogModel> commonModels, Dialog dialog) {
        this.commonModels = commonModels;
        this.mContext = context;
        this.dialog = dialog;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.check_list_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int position) {
        final CommonDialogModel commonModel = commonModels.get(position);
//        customViewHolder.check_items.setItems(commonModel.getTitle());
        customViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CommonDialogModel commonModel1 = new CommonDialogModel();
                commonModel1 = commonModel;
                EventBus.getDefault().post(commonModel1);
                if (null != dialog)
                    dialog.cancel();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != commonModels ? commonModels.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected MultiSelectionSpinner check_items;
        protected View view;

        public CustomViewHolder(View view) {
            super(view);
            this.check_items = (MultiSelectionSpinner) view.findViewById(R.id.check_items);
            this.view = view;
        }
    }
}
