package com.bnhabitat.ui.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.PermissionsModel;
import com.bnhabitat.models.StatusModel;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 12/27/2017.
 */

public class PermissionAdapter extends RecyclerView.Adapter<PermissionAdapter.ViewHolder> {
    Activity context;
    ArrayList<StatusModel>checkboxes=new ArrayList<>();
    ArrayList<PermissionsModel> permissionsModels;
    ArrayList<Boolean> isChecked=new ArrayList<>();
    public PermissionAdapter(Activity activity, ArrayList<PermissionsModel> permissionsModels) {
        this.context = activity;
        this.permissionsModels = permissionsModels;

        for(PermissionsModel permissionsModel:permissionsModels){

            isChecked.add(false);
        }

    }

    @Override
    public PermissionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.permission_items, parent, false);
        PermissionAdapter.ViewHolder viewHolder = new PermissionAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PermissionAdapter.ViewHolder holder,final int position) {
        holder.mutlicheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){

                    isChecked.set(position,true);
                    checkboxes.clear();
                    for(int i=0;i<isChecked.size();i++){
                        if(isChecked.get(i).booleanValue()==true){
                            StatusModel statusModel=new StatusModel();
                            statusModel.setId(permissionsModels.get(i).getId());
                            statusModel.setName(permissionsModels.get(i).getPermissionName());
                            statusModel.setStatus("permission");

                            checkboxes.add(statusModel);
                        }
                    }

//                        builder.append(paymentPlanModels.get(position).getTitle()+",");
                    EventBus.getDefault().post(checkboxes);


                }else{
                    isChecked.set(position,false);
                    checkboxes.clear();
                    for(int i=0;i<isChecked.size();i++){
                        if(isChecked.get(i).booleanValue()==true){
                            StatusModel statusModel=new StatusModel();
                            statusModel.setId(permissionsModels.get(i).getId());
                            statusModel.setName(permissionsModels.get(i).getPermissionName());
                            statusModel.setStatus("permission");

                            checkboxes.add(statusModel);
                        }
                    }
                    EventBus.getDefault().post(checkboxes);
                }
            }
        });

        holder.titleText.setText(permissionsModels.get(position).getPermissionName());
    }

    @Override
    public int getItemCount() {
        return permissionsModels.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView titleText;

        CheckBox mutlicheckBox;
        public ViewHolder(View itemView) {
            super(itemView);
           titleText = (TextView) itemView.findViewById(R.id.titleText);

            mutlicheckBox = (CheckBox) itemView.findViewById(R.id.mutlicheckBox);
//
        }


    }
}
