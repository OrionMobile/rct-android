package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.RelationModel;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

public class RelationAdapter extends RecyclerView.Adapter<RelationAdapter.ViewHolder> {
    Context context;
    private ArrayList<RelationModel> relationList = new ArrayList();

    public RelationAdapter(Context context, ArrayList<RelationModel> relationList) {
        this.context = context;
        this.relationList = relationList;


    }

    @Override
    public RelationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.relation_view, parent, false);
        RelationAdapter.ViewHolder viewHolder = new RelationAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RelationAdapter.ViewHolder holder, int position) {

        if(!relationList.get(position).getRelation().equalsIgnoreCase("Select Item"))
        holder.relation.setText(relationList.get(position).getRelation());

        holder.relation_name.setText(relationList.get(position).getRelationFirstName() +" "+
                relationList.get(position).getRelationLastName());

    }

    @Override
    public int getItemCount() {
        return relationList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView relation, relation_name;

        public ViewHolder(View itemView) {
            super(itemView);
            relation = (TextView) itemView.findViewById(R.id.relation);
            relation_name = (TextView) itemView.findViewById(R.id.relation_name);
        }


    }


}
