package com.bnhabitat.ui.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.StatusModel;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 4/30/2018.
 */

public class BalconyListAdapter extends RecyclerView.Adapter<BalconyListAdapter.ViewHolder>  {
    Context context;
    private ArrayList<PropertyBedroomModel> balcony_data = new ArrayList();
    private ArrayList<ProjectsModel> tempArraylist;
    int id,position1;




    public BalconyListAdapter(Context context,ArrayList<PropertyBedroomModel> balcony_data) {
        this.context = context;
        this.balcony_data=balcony_data;



    }

    @Override
    public BalconyListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.room_data_show, parent, false);
        BalconyListAdapter.ViewHolder viewHolder = new BalconyListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BalconyListAdapter.ViewHolder holder, final int position) {
        holder.room_name_txt.setText(balcony_data.get(position).getRoom_name());
        holder.delete.setVisibility(View.VISIBLE);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(context.getString(R.string.message))
                        .setContentText(context.getString(R.string.are_you_sure))
                        .setConfirmText(context.getString(R.string.yes))
                        .setCancelText(context.getString(R.string.cancel))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                StatusModel statusModel=new StatusModel();
                                statusModel.setId(balcony_data.get(position).getId());
                                statusModel.setStatus("balcony_delete");
                                EventBus.getDefault().post(statusModel);
                            }
                        })
                        .show();

            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatusModel statusModel=new StatusModel();
                statusModel.setId(balcony_data.get(position).getId());
                statusModel.setPos(position);
                statusModel.setStatus("edit_balcony");
                EventBus.getDefault().post(statusModel);
            }
        });
//
    }

    @Override
    public int getItemCount() {
        return balcony_data.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView delete, edit;
        TextView room_size_txt,room_name_txt;

        public ViewHolder(View itemView) {
            super(itemView);
//
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit= (ImageView) itemView.findViewById(R.id.edit_room);
            room_name_txt = (TextView) itemView.findViewById(R.id.room_name_txt);
//
//
        }


    }



}
