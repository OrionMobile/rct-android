package com.bnhabitat.ui.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.utils.Utils;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;


public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.ViewHolder>  {
    Context context;
    private ArrayList<PropertyBedroomModel> room_data = new ArrayList();
    private ArrayList<ProjectsModel> tempArraylist;
    int id,position1;
    boolean ispublished=false;


    public RoomListAdapter(Context context,ArrayList<PropertyBedroomModel> room_data) {
        this.context = context;
        this.room_data=room_data;

    }

    @Override
    public RoomListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.room_data_show, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RoomListAdapter.ViewHolder holder, final int position) {
        holder.room_name_txt.setText(room_data.get(position).getRoom_name());

        holder.delete.setVisibility(View.VISIBLE);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(context.getString(R.string.message))
                        .setContentText(context.getString(R.string.are_you_sure))
                        .setConfirmText(context.getString(R.string.yes))
                        .setCancelText(context.getString(R.string.cancel))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                StatusModel statusModel=new StatusModel();
                                statusModel.setId(room_data.get(position).getId());
                                statusModel.setStatus("delete");
                                EventBus.getDefault().post(statusModel);
                            }
                        })
                        .show();

            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatusModel statusModel=new StatusModel();
                statusModel.setId(room_data.get(position).getId());
                statusModel.setPos(position);
                statusModel.setStatus("edit");
                EventBus.getDefault().post(statusModel);
            }
        });

    }

    @Override
    public int getItemCount() {
        return room_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView delete, edit;
        TextView room_size_txt,room_name_txt;
        SwitchButton dots;
        public ViewHolder(View itemView) {
            super(itemView);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit= (ImageView) itemView.findViewById(R.id.edit_room);
            room_name_txt = (TextView) itemView.findViewById(R.id.room_name_txt);

        }


    }

    private void showPublishedDialog(){

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.published_dioalog);
        dialog.show();

        final CheckBox checkBox=(CheckBox)dialog.findViewById(R.id.checked);
        final Button done=(Button)dialog.findViewById(R.id.done);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(checkBox.isChecked()){
                    ispublished=true;
                }else{
                    ispublished=false;
                }
            }
        });

    }

}
