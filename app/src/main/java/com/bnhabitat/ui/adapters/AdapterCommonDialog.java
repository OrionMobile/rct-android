package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;


public class AdapterCommonDialog extends BaseAdapter implements CompoundButton.OnCheckedChangeListener {

    private Context context;
    public SparseBooleanArray mCheckStates;
    public ArrayList<Boolean> checkedItemsList = new ArrayList<>();

    private ArrayList<CommonDialogModel> paymentPlanModels = new ArrayList<CommonDialogModel>();
    private boolean showRadioBtn = false;
    private boolean isFromPrice = false;

    public AdapterCommonDialog(Context context, ArrayList<CommonDialogModel> paymentPlanModels) {

        this.context = context;
        this.paymentPlanModels = paymentPlanModels;

    }

    public AdapterCommonDialog(Context context, ArrayList<CommonDialogModel> paymentPlanModels, boolean isFromPrice) {

        this.isFromPrice = isFromPrice;
        this.context = context;
        this.paymentPlanModels = paymentPlanModels;

    }

    public AdapterCommonDialog(Context context,
                               ArrayList<CommonDialogModel> paymentPlanModels,
                               boolean showRadioBtn,
                               ArrayList<Boolean> checkedItemsList) {

        isFromPrice = true;
        this.isFromPrice = isFromPrice;
        this.context = context;
        this.paymentPlanModels = paymentPlanModels;
        this.showRadioBtn = showRadioBtn;
        //mCheckStates = new SparseBooleanArray(paymentPlanModels.size());
        this.checkedItemsList = new ArrayList<>();
        this.checkedItemsList.clear();
        this.checkedItemsList = checkedItemsList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return paymentPlanModels.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (rowView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            rowView = inflater.inflate(R.layout.adapter_common_dialog_items, parent, false);
            viewHolder.titleText = (TextView) rowView.findViewById(R.id.titleText);
            viewHolder.priceText = (TextView) rowView.findViewById(R.id.priceText);
            viewHolder.mutlicheckBox = (CheckBox) rowView.findViewById(R.id.mutlicheckBox);

            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        viewHolder.mutlicheckBox.setVisibility(View.GONE);
        String title = paymentPlanModels.get(position).getTitle();
        viewHolder.titleText.setText(title.substring(0, 1).toUpperCase() + title.substring(1));

        if (!paymentPlanModels.get(position).getPrice().equalsIgnoreCase("")) {

            viewHolder.priceText.setVisibility(View.VISIBLE);
            viewHolder.priceText.setText(Utils.getConvertedPrice(paymentPlanModels.get(position).getActualPrice(), context));

        }

        if (showRadioBtn) {
            viewHolder.mutlicheckBox.setVisibility(View.VISIBLE);
            viewHolder.mutlicheckBox.setTag(position);
            viewHolder.mutlicheckBox.setOnCheckedChangeListener(this);

            if (checkedItemsList.get(position)) {
                viewHolder.mutlicheckBox.setChecked(true);
            } else
                viewHolder.mutlicheckBox.setChecked(false);
        }

        return rowView;
    }

    static class ViewHolder {
        protected TextView priceText;
        protected TextView titleText;
        protected RelativeLayout mainLayout;
        protected CheckBox mutlicheckBox;

    }


    public boolean isChecked(int position) {
        return mCheckStates.get(position, false);
    }

    public void setChecked(int position, boolean isChecked) {
        mCheckStates.put(position, isChecked);

    }

    public void toggle(int position) {
        setChecked(position, !isChecked(position));

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView,
                                 boolean isChecked) {

        int index = (Integer) buttonView.getTag();

        checkedItemsList.set(index, isChecked);

    }
}


