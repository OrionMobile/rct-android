package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.CommonDialogModel;

import java.util.ArrayList;

/**
 * Created by Android on 5/23/2017.
 */

public class AdapterPropertyType extends BaseAdapter {

    private Context context;

    private ArrayList<CommonDialogModel> paymentPlanModels = new ArrayList<CommonDialogModel>();
    private ArrayList<ImageView> propertyImages = new ArrayList<>();
    private ArrayList<Boolean> isProperyImageChecked = new ArrayList<>();
    private ArrayList<String> selectedIds = new ArrayList<>();

    public AdapterPropertyType(Context context, ArrayList<CommonDialogModel> paymentPlanModels, OnPropertySizesSelected onPropertySizesSelected) {

        this. onPropertySizesSelected=onPropertySizesSelected;
        this.context = context;
        this.paymentPlanModels = paymentPlanModels;
        propertyImages.clear();
        for (CommonDialogModel commonDialogModel:paymentPlanModels){
            isProperyImageChecked.add(false);
            propertyImages.add(new ImageView(context));
            selectedIds.add("-1");

        }

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return paymentPlanModels.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (rowView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            rowView = inflater.inflate(R.layout.adapter_property_type, parent, false);
            viewHolder.titleText = (TextView) rowView.findViewById(R.id.titleText);
            viewHolder.mainLayout = (LinearLayout) rowView.findViewById(R.id.mainLayout);
            viewHolder.typeImg = (ImageView) rowView.findViewById(R.id.typeImg);


            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        String title = paymentPlanModels.get(position).getTitle();
        viewHolder.titleText.setText(title.substring(0, 1).toUpperCase() + title.substring(1));

        propertyImages.set(position,viewHolder.typeImg);

        viewHolder.typeImg.setImageResource(R.drawable.builder_house_normal);

        if (isProperyImageChecked.get(position))
            viewHolder.typeImg.setImageResource(R.drawable.builder_house_active);


        viewHolder.mainLayout.setTag(position);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i=0;i<isProperyImageChecked.size();i++){
                    isProperyImageChecked.set(i, false);
                }
                int position = (Integer) view.getTag();

                if (isProperyImageChecked.get(position)) {
                    isProperyImageChecked.set(position, false);
                    propertyImages.get(position).setImageResource(R.drawable.builder_house_normal);
                    selectedIds.set(position,"-1");
                }else{
                    String title = paymentPlanModels.get(position).getTitle();
                    isProperyImageChecked.set(position, true);
                    propertyImages.get(position).setImageResource(R.drawable.builder_house_active);
                    onPropertySizesSelected.onPropertyTextSelected(title);
                    selectedIds.set(position,paymentPlanModels.get(position).getId());
                }
                onPropertySizesSelected.onPropertySizeSelected(selectedIds);

                AdapterPropertyType.this.notifyDataSetChanged();

            }
        });


        return rowView;
    }

    OnPropertySizesSelected onPropertySizesSelected;
    public interface OnPropertySizesSelected{



        public void onPropertySizeSelected(ArrayList<String> ids);
        public void onPropertyTextSelected(String name);
    }

    static class ViewHolder {
        protected TextView titleText;
        protected ImageView typeImg;
        protected LinearLayout mainLayout;

    }
}
