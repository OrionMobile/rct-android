package com.bnhabitat.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.SelectPropertyTypeModel;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.utils.Constants;

import java.util.ArrayList;

/**
 * Created by gourav on 1/18/2018.
 */

public class SelectPropertyAdapter extends RecyclerView.Adapter<SelectPropertyAdapter.ViewHolder> {
    Activity context;
    ArrayList<SelectPropertyTypeModel> selectPropertyTypeModels = new ArrayList<>();
    String from;

    public SelectPropertyAdapter(Activity activity, ArrayList<SelectPropertyTypeModel> selectPropertyTypeModels, String from) {
        this.context = activity;
        this.selectPropertyTypeModels = selectPropertyTypeModels;
        this.from = from;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.select_property_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.property_type_name.setText(selectPropertyTypeModels.get(position).getName());

        holder.select_property_item_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, PropertyItemActivity.class);
                intent.putExtra("PropertyId", selectPropertyTypeModels.get(position).getId());
                intent.putExtra("PropertyName", selectPropertyTypeModels.get(position).getName());
                intent.putExtra("PropertyTypeId",selectPropertyTypeModels.get(position).getId() );
                intent.putExtra(Constants.FROM, from);
                context.startActivity(intent);
                //context.finish();

            }

        });
    }

    @Override
    public int getItemCount() {
        return selectPropertyTypeModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout select_property_item_lay;
        TextView property_type_name;


        public ViewHolder(View itemView) {
            super(itemView);
            select_property_item_lay = (LinearLayout) itemView.findViewById(R.id.select_property_item_lay);
            property_type_name = (TextView) itemView.findViewById(R.id.property_type_name);

//
        }


    }
}
