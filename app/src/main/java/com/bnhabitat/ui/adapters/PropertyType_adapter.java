package com.bnhabitat.ui.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bnhabitat.R;

import java.util.ArrayList;

public class PropertyType_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity activity;
    ArrayList<String> listTypes;
     int row_index = 0;
    public static  String type_id="";

    public PropertyType_adapter(Activity activity, ArrayList<String> listTypes) {
        this.activity = activity;
        this.listTypes = listTypes;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(activity);

        return new PostHolder(inflater.inflate(R.layout.custom_types, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PostHolder) holder).bindData(listTypes.get(position));
    }

    @Override
    public int getItemCount() {
        return listTypes.size();
    }

    public class PostHolder extends RecyclerView.ViewHolder {

        Button btnType;

        void bindData(final String type) {
            btnType.setText(type);

            btnType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    row_index = getAdapterPosition();

                    type_id= "PropertyTypeId%20eq%20"+row_index+1+"and%20";

                    notifyDataSetChanged();

                }
            });

            if (row_index == getAdapterPosition()) {

                btnType.setBackground(activity.getResources().getDrawable(R.drawable.orange_background));


            } else {
                btnType.setBackground(activity.getResources().getDrawable(R.drawable.btn_with_border));
            }


        }

        public PostHolder(View itemView) {
            super(itemView);

            btnType = (Button) itemView.findViewById(R.id.btnType);


        }

    }


}
