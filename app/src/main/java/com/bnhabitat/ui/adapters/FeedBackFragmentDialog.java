package com.bnhabitat.ui.adapters;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.bnhabitat.R;

/**
 * Created by Android on 5/19/2017.
 */

public class FeedBackFragmentDialog extends DialogFragment {

    public static FeedBackFragmentDialog newInstance() {
        return new FeedBackFragmentDialog();
    }
    // this method create view for your Dialog
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflate layout with recycler view
        View v = inflater.inflate(R.layout.feedback_dialog, container, false);
        Button button=(Button)v.findViewById(R.id.submit_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog_fragment");
                if (prev != null) {
                    FeedBackFragmentDialog df = (FeedBackFragmentDialog) prev;
                    df.dismiss();
                }
            }
        });
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return v;
    }
}
