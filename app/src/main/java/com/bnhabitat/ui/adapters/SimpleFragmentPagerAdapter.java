package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.ui.fragments.AddressContactInfo;
import com.bnhabitat.ui.fragments.ContactSourceInfo;
import com.bnhabitat.ui.fragments.PersonalContactInfo;
import com.bnhabitat.ui.fragments.ProfessionalContactInfo;

import java.util.ArrayList;

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    ArrayList<ContactsModel> contactsModels = new ArrayList<>();

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm,ArrayList<ContactsModel> contactsModels) {
        super(fm);
        mContext = context;
        this.contactsModels=contactsModels;
    }


    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {

            PersonalContactInfo personalContactInfo= new PersonalContactInfo();
            if(!contactsModels.isEmpty()){
                Bundle bundle=new Bundle();
                bundle.putSerializable("ModelName",contactsModels);
                personalContactInfo.setArguments(bundle);
            }

            return personalContactInfo;
        } else if (position == 1){
            ProfessionalContactInfo professionalContactInfo =  new ProfessionalContactInfo();
            if(!contactsModels.isEmpty()){
                Bundle bundle=new Bundle();
                bundle.putSerializable("ModelName",contactsModels);
                professionalContactInfo.setArguments(bundle);
            }

            return professionalContactInfo;
        } else if (position == 2){

            AddressContactInfo addressContactInfo = new AddressContactInfo();
            if(!contactsModels.isEmpty()){
                Bundle bundle=new Bundle();
                bundle.putSerializable("ModelName",contactsModels);
                addressContactInfo.setArguments(bundle);
            }

            return addressContactInfo;
        } else {

            ContactSourceInfo contactSourceInfo = new ContactSourceInfo();
            if(!contactsModels.isEmpty()){
                Bundle bundle=new Bundle();
                bundle.putSerializable("ModelName",contactsModels);
                contactSourceInfo.setArguments(bundle);
            }

            return contactSourceInfo;
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {

        return 4;

    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Personal";
            case 1:
                return "Professional";
            case 2:
                return "Addresses";
            case 3:
                return "Contact Source";
            default:
                return null;
        }
    }

}