package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.OnEditTextChanged;
import com.bnhabitat.models.ContactsModel;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.HolderRecycler> {

    Context context;
    ArrayList<HashMap<Integer, ContactsModel.Phones>> arrayList;
    OnEditTextChanged onEditTextChanged;

    public RecyclerAdapter(Context context, ArrayList<HashMap<Integer, ContactsModel.Phones>> arrayList, OnEditTextChanged onEditTextChanged) {
        this.context = context;
        this.arrayList = arrayList;
        this.onEditTextChanged = onEditTextChanged;
    }

    @NonNull
    @Override
    public HolderRecycler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_phone_layout, parent, false);
        HolderRecycler holderRecycler = new HolderRecycler(view);
        return holderRecycler;
    }

    public void updateList(ArrayList<HashMap<Integer, ContactsModel.Phones>> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull HolderRecycler holder, final int position) {

        final HashMap<Integer, ContactsModel.Phones> hashMap = arrayList.get(position);
       final ContactsModel.Phones phone=hashMap.get(position);

            holder.editText.setText(phone.getPhoneNumber());
        if(phone.getPhoneCode()==null){
            holder.Phone_code.setText("91");
        }else if(phone.getPhoneCode().equalsIgnoreCase("None")||phone.getPhoneCode().equalsIgnoreCase("null")){
            holder.Phone_code.setText("91");
        }else{
            holder.Phone_code.setText(phone.getPhoneCode());
        }



        holder.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                phone.setPhoneNumber(s.toString());
                hashMap.put(position, phone);
                onEditTextChanged.onTextChange(arrayList);
            }
        });
        holder.Phone_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                phone.setPhoneCode(s.toString());
                hashMap.put(position, phone);
                onEditTextChanged.onTextChange(arrayList);
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class HolderRecycler extends RecyclerView.ViewHolder {

        @BindView(R.id.phone)
        EditText editText;
        @BindView(R.id.Phone_code)
        EditText Phone_code;

        public HolderRecycler(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
