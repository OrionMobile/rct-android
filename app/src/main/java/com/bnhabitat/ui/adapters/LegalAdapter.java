package com.bnhabitat.ui.adapters;


import android.app.DatePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.areaModule.fragment.LegalStatus;
import com.bnhabitat.models.LegalModel;
import com.bnhabitat.models.OtherExpensesModel;
import com.bnhabitat.models.PhotosModel;
import com.bnhabitat.ui.fragments.LegalStatusFragment;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class LegalAdapter extends RecyclerView.Adapter<LegalAdapter.ViewHolder> {
    Context context;
    private ArrayList<LegalModel> legalList = new ArrayList();
    private ArrayList<LegalModel> legalListLocal = new ArrayList();
    ArrayList<PhotosModel> photosModels1 = new ArrayList<>();
    LegalStatusFragment legalStatusFragment;
    int id = 0;
    CallBack callBack;

    public LegalAdapter(Context context, ArrayList<LegalModel> feature_checked_list, ArrayList<PhotosModel> photosModels1, LegalStatusFragment legalStatusFragment,CallBack callBack) {
        this.context = context;
        this.legalList = feature_checked_list;
        this.legalListLocal = feature_checked_list;
        this.legalStatusFragment = legalStatusFragment;
        this.photosModels1 = photosModels1;
        this.callBack=callBack;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_legal, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        id = legalListLocal.get(position).getId();
        if (!legalListLocal.get(position).getFirstName().equalsIgnoreCase("null"))
        holder.et_fname.setText(legalListLocal.get(position).getFirstName());
        else
        holder.et_fname.setText("");

        if (!legalListLocal.get(position).getLastName().equalsIgnoreCase("null"))
            holder.et_lastname.setText(legalListLocal.get(position).getLastName());
        else
            holder.et_lastname.setText("");

        if (!legalListLocal.get(position).getPhoneNumber().equalsIgnoreCase("null"))
            holder.et_contactno.setText(legalListLocal.get(position).getPhoneNumber());
        else
            holder.et_contactno.setText("");

        if (!legalListLocal.get(position).getEmail().equalsIgnoreCase("null"))
            holder.et_email.setText(legalListLocal.get(position).getEmail());
        else
            holder.et_email.setText("");

        if (!legalListLocal.get(position).getEmail().equalsIgnoreCase("null"))
            holder.et_email.setText(legalListLocal.get(position).getEmail());
        else
            holder.et_email.setText("");

        if (!legalListLocal.get(position).getPanNo().equalsIgnoreCase("null"))
            holder.et_panno.setText(legalListLocal.get(position).getPanNo());
        else
            holder.et_panno.setText("");

        if (!legalListLocal.get(position).getAdhaarCardNo().equalsIgnoreCase("null"))
            holder.et_adhar_card.setText(legalListLocal.get(position).getAdhaarCardNo());
        else
            holder.et_adhar_card.setText("");

        if (!legalListLocal.get(position).getOccupation().equalsIgnoreCase("null"))
            holder.et_occupation.setText(legalListLocal.get(position).getOccupation());
        else
            holder.et_occupation.setText("");


        if (!legalListLocal.get(position).getAddress().equalsIgnoreCase("null"))
            holder.et_address.setText(legalListLocal.get(position).getAddress());
        else
            holder.et_address.setText("");

        holder.upload_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos=position;
//                EventBus.getDefault().post(pos);
                callBack.onPhotoClick(pos);

            }
        });

        holder.et_fname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                legalList.get(position).setId(legalListLocal.get(position).getId());
                legalList.get(position).setFirstName(holder.et_fname.getText().toString().trim());
                legalList.get(position).setLastName(holder.et_lastname.getText().toString().trim());
                legalList.get(position).setAddress(holder.et_address.getText().toString().trim());
                legalList.get(position).setPanNo(holder.et_panno.getText().toString().trim());
                legalList.get(position).setOccupation(holder.et_occupation.getText().toString().trim());
                legalList.get(position).setEmail(holder.et_email.getText().toString().trim());
                legalList.get(position).setAdhaarCardNo(holder.et_adhar_card.getText().toString().trim());
                legalList.get(position).setPhoneNumber(holder.et_contactno.getText().toString().trim());
                EventBus.getDefault().post(legalList);

            }
        });

        holder.et_lastname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                legalList.get(position).setId(legalListLocal.get(position).getId());
                legalList.get(position).setFirstName(holder.et_fname.getText().toString().trim());
                legalList.get(position).setLastName(holder.et_lastname.getText().toString().trim());
                legalList.get(position).setAddress(holder.et_address.getText().toString().trim());
                legalList.get(position).setPanNo(holder.et_panno.getText().toString().trim());
                legalList.get(position).setOccupation(holder.et_occupation.getText().toString().trim());
                legalList.get(position).setEmail(holder.et_email.getText().toString().trim());
                legalList.get(position).setAdhaarCardNo(holder.et_adhar_card.getText().toString().trim());
                legalList.get(position).setPhoneNumber(holder.et_contactno.getText().toString().trim());
                EventBus.getDefault().post(legalList);
            }
        });

        holder.et_contactno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                legalList.get(position).setId(legalListLocal.get(position).getId());
                legalList.get(position).setFirstName(holder.et_fname.getText().toString().trim());
                legalList.get(position).setLastName(holder.et_lastname.getText().toString().trim());
                legalList.get(position).setAddress(holder.et_address.getText().toString().trim());
                legalList.get(position).setPanNo(holder.et_panno.getText().toString().trim());
                legalList.get(position).setOccupation(holder.et_occupation.getText().toString().trim());
                legalList.get(position).setEmail(holder.et_email.getText().toString().trim());
                legalList.get(position).setAdhaarCardNo(holder.et_adhar_card.getText().toString().trim());
                legalList.get(position).setPhoneNumber(holder.et_contactno.getText().toString().trim());
                EventBus.getDefault().post(legalList);
            }
        });

        holder.et_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                legalList.get(position).setId(legalListLocal.get(position).getId());
                legalList.get(position).setFirstName(holder.et_fname.getText().toString().trim());
                legalList.get(position).setLastName(holder.et_lastname.getText().toString().trim());
                legalList.get(position).setAddress(holder.et_address.getText().toString().trim());
                legalList.get(position).setPanNo(holder.et_panno.getText().toString().trim());
                legalList.get(position).setOccupation(holder.et_occupation.getText().toString().trim());
                legalList.get(position).setEmail(holder.et_email.getText().toString().trim());
                legalList.get(position).setAdhaarCardNo(holder.et_adhar_card.getText().toString().trim());
                legalList.get(position).setPhoneNumber(holder.et_contactno.getText().toString().trim());
                EventBus.getDefault().post(legalList);
            }
        });

        holder.et_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                legalList.get(position).setId(legalListLocal.get(position).getId());
                legalList.get(position).setFirstName(holder.et_fname.getText().toString().trim());
                legalList.get(position).setLastName(holder.et_lastname.getText().toString().trim());
                legalList.get(position).setAddress(holder.et_address.getText().toString().trim());
                legalList.get(position).setPanNo(holder.et_panno.getText().toString().trim());
                legalList.get(position).setOccupation(holder.et_occupation.getText().toString().trim());
                legalList.get(position).setEmail(holder.et_email.getText().toString().trim());
                legalList.get(position).setAdhaarCardNo(holder.et_adhar_card.getText().toString().trim());
                legalList.get(position).setPhoneNumber(holder.et_contactno.getText().toString().trim());
                EventBus.getDefault().post(legalList);
            }
        });

        holder.et_adhar_card.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                legalList.get(position).setId(legalListLocal.get(position).getId());
                legalList.get(position).setFirstName(holder.et_fname.getText().toString().trim());
                legalList.get(position).setLastName(holder.et_lastname.getText().toString().trim());
                legalList.get(position).setAddress(holder.et_address.getText().toString().trim());
                legalList.get(position).setPanNo(holder.et_panno.getText().toString().trim());
                legalList.get(position).setOccupation(holder.et_occupation.getText().toString().trim());
                legalList.get(position).setEmail(holder.et_email.getText().toString().trim());
                legalList.get(position).setAdhaarCardNo(holder.et_adhar_card.getText().toString().trim());
                legalList.get(position).setPhoneNumber(holder.et_contactno.getText().toString().trim());
                EventBus.getDefault().post(legalList);
            }
        });

        holder.et_panno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                legalList.get(position).setId(legalListLocal.get(position).getId());
                legalList.get(position).setFirstName(holder.et_fname.getText().toString().trim());
                legalList.get(position).setLastName(holder.et_lastname.getText().toString().trim());
                legalList.get(position).setAddress(holder.et_address.getText().toString().trim());
                legalList.get(position).setPanNo(holder.et_panno.getText().toString().trim());
                legalList.get(position).setOccupation(holder.et_occupation.getText().toString().trim());
                legalList.get(position).setEmail(holder.et_email.getText().toString().trim());
                legalList.get(position).setAdhaarCardNo(holder.et_adhar_card.getText().toString().trim());
                legalList.get(position).setPhoneNumber(holder.et_contactno.getText().toString().trim());
                EventBus.getDefault().post(legalList);
            }
        });

        holder.et_occupation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                legalList.get(position).setId(legalListLocal.get(position).getId());
                legalList.get(position).setFirstName(holder.et_fname.getText().toString().trim());
                legalList.get(position).setLastName(holder.et_lastname.getText().toString().trim());
                legalList.get(position).setAddress(holder.et_address.getText().toString().trim());
                legalList.get(position).setPanNo(holder.et_panno.getText().toString().trim());
                legalList.get(position).setOccupation(holder.et_occupation.getText().toString().trim());
                legalList.get(position).setEmail(holder.et_email.getText().toString().trim());
                legalList.get(position).setAdhaarCardNo(holder.et_adhar_card.getText().toString().trim());
                legalList.get(position).setPhoneNumber(holder.et_contactno.getText().toString().trim());
                EventBus.getDefault().post(legalList);
            }
        });

        ArrayList<PhotosModel> photosModels = new ArrayList<>();
        for (int i = 0; i < photosModels1.size(); i++) {

            if (photosModels1.get(i).getPos() == position) {

                photosModels.add(photosModels1.get(i));

            }
        }

        ImageInfalteAdapter imageInfalteAdapter = new ImageInfalteAdapter(photosModels);
      //  holder.rv_images_multiple.setLayoutManager(new LinearLayoutManager(context));

        holder.rv_images_multiple.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        holder.rv_images_multiple.setAdapter(imageInfalteAdapter);

    }

    @Override
    public int getItemCount() {
        return legalListLocal.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        EditText et_fname, et_lastname, et_contactno, et_email, et_panno, et_adhar_card, et_occupation, et_address;
        RecyclerView rv_images_multiple;
        LinearLayout upload_photos;

        public ViewHolder(View itemView) {
            super(itemView);
            et_fname = (EditText) itemView.findViewById(R.id.et_fname);
            et_lastname = (EditText) itemView.findViewById(R.id.et_lastname);
            et_contactno = (EditText) itemView.findViewById(R.id.et_contactno);
            et_email = (EditText) itemView.findViewById(R.id.et_email);
            et_panno = (EditText) itemView.findViewById(R.id.et_panno);
            et_adhar_card = (EditText) itemView.findViewById(R.id.et_adhar_card);
            et_occupation = (EditText) itemView.findViewById(R.id.et_occupation);
            et_address = (EditText) itemView.findViewById(R.id.et_address);
            rv_images_multiple = (RecyclerView) itemView.findViewById(R.id.rv_images_multiple);
            upload_photos = (LinearLayout) itemView.findViewById(R.id.upload_photos);

        }
    }


    public class ImageInfalteAdapter extends RecyclerView.Adapter<ImageInfalteAdapter.ViewHolder> {

        ArrayList<PhotosModel> photosModels = new ArrayList<>();

        public ImageInfalteAdapter(ArrayList<PhotosModel> photosModels1) {
            this.photosModels = photosModels1;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(context).inflate(R.layout.image_layout_inflate, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

//            Picasso.with(context).load(photosModels.get(i).getImageUrl()).into(viewHolder.image_view);
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                    .build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            if (!imageLoader.isInited()) {
                imageLoader.init(config);
            }

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.image_logo)
                    .showImageOnFail(R.drawable.image_logo)
                    .showImageOnLoading(R.drawable.image_logo).build();

            //    Picasso.with(getActivity()).load(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl()).into(imageView);

            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl(), viewHolder.image_view, options);
            Log.e("", "" + Urls.BASE_CONTACT_IMAGE_URL + photosModels.get(i).getImageUrl());

            viewHolder.cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(context.getString(R.string.message))
                            .setContentText(context.getString(R.string.are_you_sure))
                            .setConfirmText(context.getString(R.string.yes))
                            .setCancelText(context.getString(R.string.cancel))
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })

                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {

                                    for (int j = 0; j < photosModels1.size(); j++) {

                                        if (photosModels1.get(j).getImageUrl().equalsIgnoreCase(photosModels.get(i).getImageUrl())) {

                                            callBack.onPhotoDelete(j);
                                            sDialog.dismissWithAnimation();

                                        }
                                    }
                                }
                            })
                            .show();

                }
            });



        }

        @Override
        public int getItemCount() {
            return photosModels.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ImageView image_view, cross;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                image_view = itemView.findViewById(R.id.image_view);
                cross = itemView.findViewById(R.id.cross);
            }
        }
    }

  public interface CallBack{

        public void onPhotoClick(int pos);
        void onPhotoDelete(int pos);
    }
}
