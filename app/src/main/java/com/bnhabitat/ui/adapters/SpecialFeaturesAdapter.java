package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.utils.Urls;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by gourav on 6/5/2017.
 */

public class SpecialFeaturesAdapter extends RecyclerView.Adapter<SpecialFeaturesAdapter.ViewHolder> {
    Context context;
    ArrayList<ProjectsDetailModel.SpecialFeatures> specialFeatures = new ArrayList<>();

    public SpecialFeaturesAdapter(Context context, ArrayList<ProjectsDetailModel.SpecialFeatures> specialFeatures) {
        this.context = context;
        this.specialFeatures = specialFeatures;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.amenties_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(specialFeatures.get(position).getFeatureName());

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        imageLoader.displayImage(Urls.BASE_IMAGE_URL + specialFeatures.get(position).getImage(), holder.icon_imag, options);


    }

    @Override
    public int getItemCount() {
        return specialFeatures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView company_logo, icon_imag, share_icon, notification_icon, user_icon;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            icon_imag = (ImageView) itemView.findViewById(R.id.icon_imag);
//            share_icon = (ImageView) itemView.findViewById(R.id.share_icon);
//            notification_icon = (ImageView) itemView.findViewById(R.id.notification_icon);
//            user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
        }
    }
}
