package com.bnhabitat.ui.adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.NotesModel;
import com.bnhabitat.utils.MultiTextWatcher;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class AddNotesAdapter extends RecyclerView.Adapter<AddNotesAdapter.ViewHolder> {

    Context context;
    ArrayList<NotesModel> arealist = new ArrayList<>();
    String number_count;

    public AddNotesAdapter(Context context, ArrayList<NotesModel> arealist) {
        this.context = context;
        this.arealist = arealist;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.more_notes_add, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.remove_more_notes.setTag(position);
        holder.noteHeading.setText("Notes");
        holder.notes.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT) {
                    number_count = holder.notes.getText().toString();
                    EventBus.getDefault().post(number_count);
                    return true;
                }
                return false;
            }
        });

        new MultiTextWatcher()
                .registerEditText(holder.notes)
                .setCallback(new MultiTextWatcher.TextWatcherWithInstance() {
                    @Override
                    public void beforeTextChanged(EditText editText, CharSequence s, int start, int count, int after) {
                        // TODO: Do some thing with editText
                    }

                    @Override
                    public void onTextChanged(EditText editText, CharSequence s, int start, int before, int count) {
                        // TODO: Do some thing with editText
                    }

                    @Override
                    public void afterTextChanged(EditText editText, Editable editable) {
                        // TODO: Do some thing with editText

                        final NotesModel notesModel = new NotesModel();
                        notesModel.setNote(holder.notes.getText().toString());
                        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                        notesModel.setCreatedDate(date);
                        if (holder.checkbox.isChecked())
                            notesModel.setPrivate(true);
                        else
                            notesModel.setPrivate(false);

                        arealist.set(position, notesModel);

                    }
                });

        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (buttonView.isChecked()) {
                    // checked
                    holder.checkbox.setChecked(true);
                } else {
                    holder.checkbox.setChecked(false);

                    // not checked
                }

            }
        });

        holder.remove_more_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(context.getString(R.string.message))
                        .setContentText(context.getString(R.string.are_you_sure))
                        .setConfirmText(context.getString(R.string.yes))
                        .setCancelText(context.getString(R.string.cancel))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                int i = (Integer) view.getTag();
                                arealist.remove(i);
                                notifyDataSetChanged();
                            }
                        })
                        .show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return arealist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView noteHeading;
        LinearLayout remove_more_notes;
        EditText notes;
        CheckBox checkbox;

        public ViewHolder(View itemView) {
            super(itemView);
            noteHeading = (TextView) itemView.findViewById(R.id.noteHeading);
            notes = (EditText) itemView.findViewById(R.id.notes);
            remove_more_notes = (LinearLayout) itemView.findViewById(R.id.remove_more_notes);
            checkbox = (CheckBox) itemView.findViewById(R.id.cb_private);
        }

    }
}


