package com.bnhabitat.ui.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.bnhabitat.R;

/**
 * Created by gourav on 5/19/2017.
 */

public class UnitVarientAdapter extends RecyclerView.Adapter<UnitVarientAdapter.ViewHolder> {
    Context context;
    Dialog dialog;


    public UnitVarientAdapter(Context context) {
        this.context = context;

    }

    @Override
    public UnitVarientAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.unit_varient_items, parent, false);
        UnitVarientAdapter.ViewHolder viewHolder = new UnitVarientAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UnitVarientAdapter.ViewHolder holder, int position) {

        holder.area_varient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPaydiaoluge();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView area_varient;


        public ViewHolder(View itemView) {
            super(itemView);
//            name = (TextView) itemView.findViewById(R.id.name);
            area_varient = (ImageView) itemView.findViewById(R.id.area_varient);
//            share_icon = (ImageView) itemView.findViewById(R.id.share_icon);
//            notification_icon = (ImageView) itemView.findViewById(R.id.notification_icon);
//            user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
        }
    }
    private void showPaydiaoluge(){

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//               dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.unit_varient_dialog);
//               dialog.setCancelable(false);

        dialog.show();

//       }
//    public void cancelCustomDialog() {
//        try {
//            if (null != dialog && dialog.isShowing()) {
//                dialog.cancel();
//                dialog = null;
//            }
//        } catch (Exception e) {
//            dialog = null;
//            e.printStackTrace();
//        }
    }
}