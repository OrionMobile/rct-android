package com.bnhabitat.ui.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.helper.CommonAsync;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.kyleduo.switchbutton.SwitchButton;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 5/18/2017.
 */

public class PublishedProjectAdapter extends RecyclerView.Adapter<PublishedProjectAdapter.ViewHolder>  implements Filterable,CommonAsync.OnAsyncResultListener {
    Context context;
    private ArrayList<ProjectsModel> projectsModels = new ArrayList();
    private ArrayList<ProjectsModel> tempArraylist;
    int id,position1;
    boolean ispublished=false;
    public PublishedProjectAdapter(Context context,ArrayList<ProjectsModel> projectsModels) {
        this.context = context;
        this.projectsModels=projectsModels;
        this.tempArraylist=projectsModels;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.published_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.city.setText(tempArraylist.get(position).getTitle());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        imageLoader.displayImage(Urls.BASE_IMAGE_URL + tempArraylist.get(position).getTopImage1(), holder.property_image, options);
        holder.dots.setTag(position);

        holder.dots.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    id=(Integer)buttonView.getTag();
                    position1=Integer.parseInt(projectsModels.get(id).getId());
                    publishProject();
                } else {

                }
            }
        });
//        holder.dots.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                showPublishedDialog();
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return tempArraylist.size();
    }

    @Override
    public void onResultListener(String result, String which) {
        if (null != result && !result.equalsIgnoreCase("")) {
            if (which.equalsIgnoreCase(Urls.UNPUBLISHED_PROJECT)) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.getInt("StatusCode") == 200) {
                        StatusModel statusModel=new StatusModel();
                        statusModel.setStatus("published");
                        EventBus.getDefault().post(statusModel);
                    }
                } catch (Exception e) {

                }
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView property_image, share_icon, notification_icon, user_icon;
        TextView city;
        SwitchButton dots;

        public ViewHolder(View itemView) {
            super(itemView);
            city = (TextView) itemView.findViewById(R.id.city);
            property_image = (ImageView) itemView.findViewById(R.id.property_image);
            dots = (SwitchButton) itemView.findViewById(R.id.dots);
//            share_icon = (ImageView) itemView.findViewById(R.id.share_icon);
//            notification_icon = (ImageView) itemView.findViewById(R.id.notification_icon);
//            user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
        }
    }
    private void showPublishedDialog(){

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.published_dioalog);
        dialog.show();

        final CheckBox checkBox=(CheckBox)dialog.findViewById(R.id.checked);
        final Button done=(Button)dialog.findViewById(R.id.done);
        final TextView publish_project_txt=(TextView)dialog.findViewById(R.id.publish_project_txt);
        publish_project_txt.setText("Unpublish Project");

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(checkBox.isChecked()){
                    ispublished=true;
                }else{
                    ispublished=false;
                }
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ispublished){
                    publishProject();
                    dialog.dismiss();
                }
            }
        });
    }
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {

                tempArraylist = (ArrayList<ProjectsModel>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<ProjectsModel> FilteredArrList = new ArrayList<ProjectsModel>();

                if (projectsModels == null) {
                    projectsModels = new ArrayList<ProjectsModel>(tempArraylist); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = projectsModels.size();
                    results.values = projectsModels;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < projectsModels.size(); i++) {
                        String data = projectsModels.get(i).getCityName();
                        if (data.toLowerCase().startsWith(constraint.toString())) {


                            FilteredArrList.add(new ProjectsModel(projectsModels.get(i).getCityName(),projectsModels.get(i).getTopImage1()));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }
    private void publishProject() {
        new CommonAsync(context , Urls.URL + Urls.UNPUBLISHED_PROJECT ,
                getunpublishInputJson(),
                "Loading..",
                this,
                Urls.UNPUBLISHED_PROJECT,
                Constants.POST).execute();
    }
    private String getunpublishInputJson() {

        String inputJson = "";


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserId", Constants.APP_USER_ID);
            jsonObject.put("ProjectId", position1);

//
            inputJson = jsonObject.toString();
        } catch (Exception e) {

        }
        return inputJson;
    }
}
