package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;

/**
 * Created by gourav on 5/15/2018.
 */

public class OtherExpensesViewAdapter extends RecyclerView.Adapter<OtherExpensesViewAdapter.ViewHolder>  {

    private Context context;
    //
    ArrayList<InventoryModel.PropertyOtherExpences> propertyOtherExpences=new ArrayList<>();// header titles

    public OtherExpensesViewAdapter(Context context,ArrayList<InventoryModel.PropertyOtherExpences> propertyOtherExpences
    ) {
        this.context = context;
        this.propertyOtherExpences = propertyOtherExpences;


    }




    //
    @Override
    public OtherExpensesViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.otherexpenses_listitems, parent, false);
        OtherExpensesViewAdapter.ViewHolder viewHolder = new OtherExpensesViewAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final OtherExpensesViewAdapter.ViewHolder holder, int position) {
        holder.department.setText(Utils.getEmptyValue(propertyOtherExpences.get(position).getTitle()));
        holder.amount.setText(Utils.getEmptyValue(propertyOtherExpences.get(position).getValue()));
        holder.last_paid.setText(Utils.getEmptyValue(propertyOtherExpences.get(position).getLastPaidDate()));

//
    }

    @Override
    public int getItemCount() {
        return propertyOtherExpences.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView department, amount, last_paid;


        public ViewHolder(View itemView) {
            super(itemView);
            department = (TextView) itemView
                    .findViewById(R.id.department);
            amount = (TextView) itemView
                    .findViewById(R.id.amount);
            last_paid = (TextView) itemView
                    .findViewById(R.id.last_paid);

//
//
        }


    }
}
