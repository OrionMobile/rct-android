package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.FeatureParkModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.SpecificationListModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.views.MultiSelectionSpinner;
import com.bnhabitat.utils.BackPressEdit;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class WhiteGoodsAdapter extends RecyclerView.Adapter<WhiteGoodsAdapter.ViewHolder>  {
    Context context;
    ArrayList<SpecificationListModel> specificationListModels = new ArrayList<>();
    private ArrayList<SpecificationListModel> editTextArrayList = new ArrayList();
    SpinnerAdapter spinnerAdapter;
    List<String>room_name=new ArrayList<>();
    List<Integer> room_ids = new ArrayList<>();
    ArrayList<FeatureParkModel>arrayList=new ArrayList<>();
    ArrayList<SpecificationListModel.PropertyBedroom> room_idsString = new ArrayList<>();
    ArrayList<PropertyBedroomModel> propertyBedroomModels = new ArrayList<>();


    public WhiteGoodsAdapter(FragmentActivity activity, ArrayList<SpecificationListModel> specificationListModels, ArrayList<PropertyBedroomModel> room_list) {

        this.context = activity;
        this.specificationListModels = specificationListModels;
        this.propertyBedroomModels = room_list;
        this.editTextArrayList= specificationListModels;

/*
        for (int i = 0; i < specificationListModels.size(); i++) {
            SpecificationListModel statusModel = new SpecificationListModel();
            statusModel.setDescription(specificationListModels.get(i).getDescription());
            statusModel.setName("");
            editTextArrayList.add(statusModel);
        }
*/
        for (int i = 0; i < propertyBedroomModels.size(); i++) {

            room_name.add(propertyBedroomModels.get(i).getRoom_name());
        }


        spinnerAdapter = new SpinnerAdapter(context, R.layout.new_spinner_item);
        spinnerAdapter.addAll(room_name);

        spinnerAdapter.add(context.getString(R.string.select_type));

    }
    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.white_goods_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.specification_name.setText(specificationListModels.get(position).getDescription());
        holder.specification_room_edittxt.setText(specificationListModels.get(position).getName());
        holder.capacity_size.setText(specificationListModels.get(position).getCapacity());

        holder.attched_room_balcony.setAdapter(spinnerAdapter);
        holder.attched_room_balcony.setSelection(spinnerAdapter.getCount());

        holder.multiselectSpinner.setItems(room_name);
        holder.multiselectSpinner.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {
                room_ids=indices;
                room_idsString.clear();
                for(int i=0;i<room_ids.size();i++){

                    SpecificationListModel.PropertyBedroom propertyBedroom  = new SpecificationListModel.PropertyBedroom();

                    propertyBedroom.setId(propertyBedroomModels.get(i).getId());
                    room_idsString.add(propertyBedroom);

                }
                editTextArrayList.get(position).setId(specificationListModels.get(position).getId());
                editTextArrayList.get(position).setCategory(specificationListModels.get(position).getCategory());

                editTextArrayList.get(position).setDescription(specificationListModels.get(position).getDescription());
                editTextArrayList.get(position).setPropertyBedrooms(room_idsString);
                editTextArrayList.get(position).setName(holder.specification_room_edittxt.getText().toString());

                editTextArrayList.get(position).setCategory("Specification");
                EventBus.getDefault().post("Specification");

                EventBus.getDefault().post(editTextArrayList);
            }

            @Override
            public void selectedStrings(List<String> strings) {
                room_name=strings;
                room_idsString.clear();
                for(int i=0;i<room_ids.size();i++){

                    SpecificationListModel.PropertyBedroom propertyBedroom  = new SpecificationListModel.PropertyBedroom();

                    propertyBedroom.setId(propertyBedroomModels.get(i).getId());
                    room_idsString.add(propertyBedroom);

                }
                editTextArrayList.get(position).setId(specificationListModels.get(position).getId());
                editTextArrayList.get(position).setCategory(specificationListModels.get(position).getCategory());

                editTextArrayList.get(position).setDescription(specificationListModels.get(position).getDescription());
                editTextArrayList.get(position).setPropertyBedrooms(room_idsString);
                editTextArrayList.get(position).setName(holder.specification_room_edittxt.getText().toString());

                editTextArrayList.get(position).setCategory("Specification");
                EventBus.getDefault().post("Specification");

                EventBus.getDefault().post(editTextArrayList);
            }
        });

        holder.specification_room_edittxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    room_idsString.clear();
                    for(int i=0;i<room_ids.size();i++){

                        SpecificationListModel.PropertyBedroom propertyBedroom  = new SpecificationListModel.PropertyBedroom();

                        propertyBedroom.setId(propertyBedroomModels.get(i).getId());
                        room_idsString.add(propertyBedroom);

                    }
                    editTextArrayList.get(position).setId(specificationListModels.get(position).getId());
                    editTextArrayList.get(position).setCategory(specificationListModels.get(position).getCategory());
                    editTextArrayList.get(position).setCapacity(holder.capacity_size.getText().toString());

                    editTextArrayList.get(position).setDescription(specificationListModels.get(position).getDescription());
                    editTextArrayList.get(position).setPropertyBedrooms(room_idsString);
                    editTextArrayList.get(position).setName(holder.specification_room_edittxt.getText().toString());
//                  editTextArrayList.get(position).setCategory("Whitegood");
                    EventBus.getDefault().post("Whitegood");

                    EventBus.getDefault().post(editTextArrayList);
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(holder.specification_room_edittxt.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        holder.specification_room_edittxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                room_idsString.clear();
                for(int i=0;i<room_ids.size();i++){

                    SpecificationListModel.PropertyBedroom propertyBedroom  = new SpecificationListModel.PropertyBedroom();

                    propertyBedroom.setId(propertyBedroomModels.get(i).getId());
                    room_idsString.add(propertyBedroom);

                }
                editTextArrayList.get(position).setId(specificationListModels.get(position).getId());
                editTextArrayList.get(position).setCategory(specificationListModels.get(position).getCategory());
                editTextArrayList.get(position).setCapacity(holder.capacity_size.getText().toString());

                editTextArrayList.get(position).setDescription(specificationListModels.get(position).getDescription());
                editTextArrayList.get(position).setPropertyBedrooms(room_idsString);
                editTextArrayList.get(position).setName(holder.specification_room_edittxt.getText().toString());
               // editTextArrayList.get(position).setCategory("Whitegood");
                EventBus.getDefault().post("Whitegood");

                EventBus.getDefault().post(editTextArrayList);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.capacity_size.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                room_idsString.clear();
                for(int i=0;i<room_ids.size();i++){

                    SpecificationListModel.PropertyBedroom propertyBedroom  = new SpecificationListModel.PropertyBedroom();

                    propertyBedroom.setId(propertyBedroomModels.get(i).getId());
                    room_idsString.add(propertyBedroom);

                }
                editTextArrayList.get(position).setId(specificationListModels.get(position).getId());
                editTextArrayList.get(position).setCategory(specificationListModels.get(position).getCategory());
                editTextArrayList.get(position).setCapacity(holder.capacity_size.getText().toString());

                editTextArrayList.get(position).setDescription(specificationListModels.get(position).getDescription());
                editTextArrayList.get(position).setPropertyBedrooms(room_idsString);
                editTextArrayList.get(position).setName(holder.specification_room_edittxt.getText().toString());
                // editTextArrayList.get(position).setCategory("Whitegood");
                EventBus.getDefault().post("Whitegood");

                EventBus.getDefault().post(editTextArrayList);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.specification_room_edittxt.setKeyImeChangeListener(new BackPressEdit.KeyImeChange() {
            @Override
            public void onKeyIme(int keyCode, KeyEvent event) {
                if (KeyEvent.KEYCODE_BACK == event.getKeyCode()) {
                    // do something
                    room_idsString.clear();
                    for(int i=0;i<room_ids.size();i++){

                        SpecificationListModel.PropertyBedroom propertyBedroom  = new SpecificationListModel.PropertyBedroom();

                        propertyBedroom.setId(propertyBedroomModels.get(i).getId());
                        room_idsString.add(propertyBedroom);

                    }
                    editTextArrayList.get(position).setId(specificationListModels.get(position).getId());
                    editTextArrayList.get(position).setCategory(specificationListModels.get(position).getCategory());
                    editTextArrayList.get(position).setCapacity(holder.capacity_size.getText().toString());

                    editTextArrayList.get(position).setDescription(specificationListModels.get(position).getDescription());
                    editTextArrayList.get(position).setPropertyBedrooms(room_idsString);
                    editTextArrayList.get(position).setName(holder.specification_room_edittxt.getText().toString());
                 //   editTextArrayList.get(position).setCategory("Whitegood");
                    EventBus.getDefault().post("Whitegood");

                    EventBus.getDefault().post(editTextArrayList);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return specificationListModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView specification_name, no_of_count;
        Spinner attched_room_balcony;
        BackPressEdit specification_room_edittxt,capacity_size;
        private MultiSelectionSpinner multiselectSpinner;
        ImageView minus, plus;

        public ViewHolder(View itemView) {
            super(itemView);
            multiselectSpinner = (MultiSelectionSpinner) itemView.findViewById(R.id.multiselectSpinner);
            specification_name = (TextView) itemView.findViewById(R.id.specification_name);
            attched_room_balcony = (Spinner) itemView.findViewById(R.id.attched_room);
            specification_room_edittxt = (BackPressEdit) itemView.findViewById(R.id.specification_edittxt);
            capacity_size = (BackPressEdit) itemView.findViewById(R.id.capacity_size);
        }


    }
}