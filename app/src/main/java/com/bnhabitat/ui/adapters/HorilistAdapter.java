package com.bnhabitat.ui.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.models.GroupModel;
import com.bnhabitat.models.StatusModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 1/16/2017.
 */
public class HorilistAdapter extends RecyclerView.Adapter<HorilistAdapter.ViewHolder> {


    Activity context;
    List<GroupModel> contactsFirebaseModels;
    LayoutInflater inflter;
    ArrayList<ContactsModel.Phones>getPhones;
    int  id;
    private ArrayList<Boolean> contactListsClicked;
    int selectedPosition = -1;
    String selecteditem="";
    public HorilistAdapter(Activity activity, List<GroupModel> contactsFirebaseModels) {
        contactListsClicked = new ArrayList<>();


        this.context = activity;
        this.contactsFirebaseModels = contactsFirebaseModels;
        for (GroupModel arrayList : contactsFirebaseModels) {
            contactListsClicked.add(false);
        }
//        for(int index=0;index<contactsFirebaseModels.size();index++){
//            id=contactsFirebaseModels.get(index).getId();
//        }

    }

//    public void updateContactsAdapter( List<ContactsModel> contactsFirebaseModels) {
////        if(this.contactsFirebaseModels!=null){
////            this.contactsFirebaseModels.clear();
////            this.contactsFirebaseModels.addAll(contactsFirebaseModels);
////        }else{
////            this.contactsFirebaseModels= contactsFirebaseModels;
////        }
//
//        this.contactsFirebaseModels=null;
//        this.contactsFirebaseModels =  contactsFirebaseModels;
//        notifyDataSetChanged();
//    }


    @Override
    public HorilistAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.group_item, parent, false);
        HorilistAdapter.ViewHolder viewHolder = new HorilistAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HorilistAdapter.ViewHolder holder, final int i) {

//        getPhones= PhoneTable.getInstance().getPhones(contactsFirebaseModels.get(i).getId());
        holder.group_name.setText(contactsFirebaseModels.get(i).getName());
        holder.view.setVisibility(View.GONE);
//        holder.group_name.setBackgroundColor(Color.parseColor("#303f9f"));
        if (selectedPosition == i) {

            holder.view.setVisibility(View.VISIBLE);

        }
//        else{
//
//            if(i==0){
//
//                holder.view.setVisibility(View.VISIBLE);
//            }
//        }


//            holder.group_name.setBackgroundColor(Color.parseColor("#303f9f"));

        holder.full_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                holder.view.setVisibility(View.GONE);
                selecteditem=contactsFirebaseModels.get(i).getId();
                if (selectedPosition == i) {


                    selectedPosition = -1;
                } else
                    selectedPosition = i;
                    notifyDataSetChanged();

//                int arrayPos=Integer.parseInt(contactsFirebaseModels.get(i).getId());
//                if(arrayPos == i){
//                    holder.view.setVisibility(View.VISIBLE);
//                  Log.e("11","1222");
//                }
//                else{
//                    holder.view.setVisibility(View.GONE);
//
//                }
                StatusModel statusModel=new StatusModel();
                statusModel.setPos(i);
                statusModel.setId(contactsFirebaseModels.get(i).getId());
                EventBus.getDefault().post(statusModel);

            }
        });

    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return contactsFirebaseModels.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView group_name;
        View view;
        RelativeLayout full_lay;

        public ViewHolder(View itemView) {
            super(itemView);

            group_name = (TextView) itemView.findViewById(R.id.group_name);
            full_lay = (RelativeLayout) itemView.findViewById(R.id.full_lay);
            view = (View) itemView.findViewById(R.id.view);


//
        }


    }
}

