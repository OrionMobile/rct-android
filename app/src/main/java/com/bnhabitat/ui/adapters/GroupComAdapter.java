package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.StatusModel;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 1/16/2018.
 */

public class GroupComAdapter  extends BaseAdapter {

    private Context context;
    ArrayList<StatusModel> checkboxes = new ArrayList<>();
    StringBuilder builder;
    ArrayList<Boolean> isChecked=new ArrayList<>();
    private ArrayList<CommonDialogModel> paymentPlanModels = new ArrayList<CommonDialogModel>();
    private boolean showRadioBtn = false;
    private boolean isFromPrice = false;
    int from;

    public GroupComAdapter(Context context, ArrayList<CommonDialogModel> paymentPlanModels, int from) {

        this.context = context;
        this.paymentPlanModels = paymentPlanModels;
        this.from = from;

        for(CommonDialogModel commonDialogModel:paymentPlanModels){

            isChecked.add(false);
        }

    }

    public int getCount() {
        return paymentPlanModels.size();
    }

    public Object getItem(int position) {
        return paymentPlanModels.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(final int position, View rowView, ViewGroup parent) {
        CommonApdater.ViewHolder viewHolder = null;

        if (rowView == null) {

            viewHolder = new CommonApdater.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            rowView = inflater.inflate(R.layout.group_com_items, parent, false);
            viewHolder.titleText = (TextView) rowView.findViewById(R.id.titleText);

            viewHolder.mutlicheckBox = (CheckBox) rowView.findViewById(R.id.mutlicheckBox);
//        viewHolder.ok = (TextView) rowView.findViewById(R.id.ok);
//            if (from == 3) {
//                viewHolder.mutlicheckBox.setVisibility(View.VISIBLE);
//            } else {
//                viewHolder.mutlicheckBox.setVisibility(View.GONE);
//            }


            rowView.setTag(viewHolder);

        } else {
            viewHolder = (CommonApdater.ViewHolder) rowView.getTag();
        }
        viewHolder = (CommonApdater.ViewHolder) rowView.getTag();
        viewHolder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatusModel statusModel = new StatusModel();
                statusModel.setId(paymentPlanModels.get(position).getId());
                statusModel.setName(paymentPlanModels.get(position).getTitle());
                statusModel.setPos(from);


//                        builder.append(paymentPlanModels.get(position).getTitle()+",");
                EventBus.getDefault().post(statusModel);
            }
        });
        viewHolder.mutlicheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    StatusModel statusModel = new StatusModel();
                    statusModel.setId(paymentPlanModels.get(position).getId());
                    statusModel.setName(paymentPlanModels.get(position).getTitle());
                    statusModel.setPos(from);
                    statusModel.setStatus("field");

                    checkboxes.add(statusModel);

//                  builder.append(paymentPlanModels.get(position).getTitle()+",");
                    EventBus.getDefault().post(checkboxes);

                    Log.e("", "" + paymentPlanModels.get(position).getTitle());
                } else {
                    checkboxes.remove(position);
                    EventBus.getDefault().post(checkboxes);
                }
            }
        });

        viewHolder.titleText.setText(paymentPlanModels.get(position).getTitle());


        return rowView;
    }

    static class ViewHolder {
        protected TextView priceText;
        protected TextView titleText, ok;

        protected CheckBox mutlicheckBox;

    }


}



