package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bnhabitat.R;
import com.bnhabitat.models.GetFeelerModel;
import com.bnhabitat.ui.activities.ManageProjectActivity;

import java.util.ArrayList;

/**
 * Created by Android on 5/19/2017.
 */

public class GetFeelerAdapter extends RecyclerView.Adapter<GetFeelerAdapter.ViewHolder> {


    Context context;
    ArrayList<GetFeelerModel> getFeelerModels=new ArrayList<>();
    boolean isOpened;

    public GetFeelerAdapter(Context context, ArrayList<GetFeelerModel> getFeelerModels) {
        this.context = context;
        this.getFeelerModels = getFeelerModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View view= LayoutInflater.from(context).inflate(R.layout.get_feeler_items,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.project_name_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isOpened){
                    holder.project_detail_lay.setVisibility(View.VISIBLE);
                    holder.drop_down_image.setRotation(90);
                    isOpened=false;

                }
                else{
                    holder.project_detail_lay.setVisibility(View.GONE);
                    holder.drop_down_image.setRotation(0);
                    isOpened=true;
                }

            }
        });

        holder.submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FeedBackFragmentDialog fragment = FeedBackFragmentDialog.newInstance();
                fragment.show(((ManageProjectActivity)context).getSupportFragmentManager(), "dialog_fragment");
            }
        });
    }

    @Override
    public int getItemCount() {
        return getFeelerModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       ImageView drop_down_image;
        LinearLayout project_name_lay,project_detail_lay;
        Button submit_button;
        public ViewHolder(View itemView) {
            super(itemView);
            project_name_lay=(LinearLayout)itemView.findViewById(R.id.project_name_lay);
            project_detail_lay=(LinearLayout)itemView.findViewById(R.id.project_detail_lay);
            drop_down_image=(ImageView)itemView.findViewById(R.id.drop_down_image);
            submit_button=(Button)itemView.findViewById(R.id.submit_button);
        }
    }
}
