package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.CommonDialogModel;
import com.bnhabitat.models.StatusModel;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 12/20/2017.
 */

public class GroupCommonAdapter extends BaseAdapter {

    private Context context;
    ArrayList<StatusModel> checkboxes=new ArrayList<>();
    StringBuilder builder;

    private ArrayList<CommonDialogModel> paymentPlanModels = new ArrayList<CommonDialogModel>();
    private boolean showRadioBtn = false;
    private boolean isFromPrice = false;
    int from;

    public GroupCommonAdapter(Context context, ArrayList<CommonDialogModel> paymentPlanModels,int from) {

        this.context = context;
        this.paymentPlanModels = paymentPlanModels;
        this.from = from;

    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return paymentPlanModels.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(final int position, View rowView, ViewGroup parent) {
        CommonApdater.ViewHolder viewHolder = null;

        if (rowView == null) {

            viewHolder = new CommonApdater.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            rowView = inflater.inflate(R.layout.group_item_data, parent, false);
            viewHolder.titleText = (TextView) rowView.findViewById(R.id.titleText);
            viewHolder.priceText = (TextView) rowView.findViewById(R.id.priceText);
//        viewHolder.ok = (TextView) rowView.findViewById(R.id.ok);

            rowView.setTag(viewHolder);

        } else {
            viewHolder = (CommonApdater.ViewHolder) rowView.getTag();
        }
        viewHolder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatusModel statusModel=new StatusModel();
                    statusModel.setId(paymentPlanModels.get(position).getId());
                    statusModel.setName(paymentPlanModels.get(position).getTitle());
                    statusModel.setPos(from);



//                        builder.append(paymentPlanModels.get(position).getTitle()+",");
                    EventBus.getDefault().post(statusModel);
            }
        });
//        viewHolder.mutlicheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                if(b){
//                    StatusModel statusModel=new StatusModel();
//                    statusModel.setId(paymentPlanModels.get(position).getId());
//                    statusModel.setName(paymentPlanModels.get(position).getTitle());
//                    statusModel.setPos(from);
//
//                    checkboxes.add(statusModel);
//
////                        builder.append(paymentPlanModels.get(position).getTitle()+",");
//                    EventBus.getDefault().post(checkboxes);
//
//                    Log.e("",""+paymentPlanModels.get(position).getTitle())  ;
//                }else{
//                    checkboxes.remove(position);
//                    EventBus.getDefault().post(checkboxes);
//                }
//            }
//        });

        viewHolder.titleText.setText(paymentPlanModels.get(position).getTitle());



        return rowView;
    }

    static class ViewHolder {

        protected TextView titleText,ok;



    }







}



