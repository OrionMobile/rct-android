package com.bnhabitat.ui.adapters;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.ui.activities.AddContatctActivity;
import com.bnhabitat.ui.activities.CreateNewGroupActivity;
import com.bnhabitat.utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 1/16/2017.
 */
public class NewContactsAdapter extends BaseAdapter {


    Activity context;
    List<ContactsModel> contactsFirebaseModels;
    LayoutInflater inflter;
    ArrayList<ContactsModel.Phones>getPhones;
    public static final String contactdetail = "contact_detail";


    public NewContactsAdapter(Activity applicationContext, List<ContactsModel> contactsFirebaseModels) {
        this.context = applicationContext;
        this.contactsFirebaseModels = contactsFirebaseModels;
        inflter = (LayoutInflater.from(applicationContext));
    }

    public void updateContactsAdapter( List<ContactsModel> contactsFirebaseModels) {
        //this.contactsFirebaseModels.clear();
        this.contactsFirebaseModels =  contactsFirebaseModels;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return contactsFirebaseModels.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.list_view_items, null);
        LinearLayout contact_lay = (LinearLayout) view.findViewById(R.id.contact_lay);
        TextView country = (TextView) view.findViewById(R.id.contact_name);
        TextView contact_number = (TextView) view.findViewById(R.id.contact_number);
        LinearLayout contact_name_lay=(LinearLayout)view.findViewById(R.id.contact_name_lay);
        ImageView call = (ImageView) view.findViewById(R.id.call);
        ImageView sms = (ImageView) view.findViewById(R.id.sms);
        ImageView email = (ImageView) view.findViewById(R.id.email);
        if(contactsFirebaseModels.get(i).getEmails().isEmpty()) {
            email.setImageDrawable(context.getResources().getDrawable(R.drawable.email_grey));
            email.setEnabled(false);
        }
        else{
            email.setImageDrawable(context.getResources().getDrawable(R.drawable.mail_icon));
            email.setEnabled(true);
        }
        ImageView details_contact = (ImageView) view.findViewById(R.id.details_contact);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EventBus.getDefault().post(contactsFirebaseModels.get(i).getPhonenumber());
                Log.e("i=", String.valueOf(contactsFirebaseModels.get(i).getPhonenumber()));

            }
        });
//        getPhones= PhoneTable.getInstance().getPhones(contactsFirebaseModels.get(i).getId());
        country.setText(contactsFirebaseModels.get(i).getFirstName());
        try {
            contact_number.setText(contactsFirebaseModels.get(i).getPhonenumber());
        }catch (Exception e){
            e.printStackTrace();
        }
//
        details_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                PreferenceConnector.getInstance(context).savePreferences("ContactName",contactsFirebaseModels.get(i).getName());
//                PreferenceConnector.getInstance(context).savePreferences("ContactNumber",contactsFirebaseModels.get(i).getPhonenumber());

                Intent intent=new Intent(context,CreateNewGroupActivity.class);
                  intent.putExtra(Constants.ARRAY_LIST, (Serializable) contactsFirebaseModels);
                context.startActivity(intent);
            }
        });
//        contact_lay.setTag(R.id.contact_lay,i);
        contact_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                PreferenceConnector.getInstance(context).savePreferences("ContactName",contactsFirebaseModels.get(i).getName());
//                PreferenceConnector.getInstance(context).savePreferences("ContactNumber",contactsFirebaseModels.get(i).getPhonenumber());
                Intent intent = new Intent(context, AddContatctActivity.class);
                intent.putExtra(contactdetail, contactsFirebaseModels.get(i));
                intent.putExtra("update", "update_contact");
                intent.putExtra("position", i);
                context.startActivity(intent);
            }
        });
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address",contactsFirebaseModels.get(i).getPhonenumber() );
                smsIntent.putExtra("sms_body"," ");
               context.startActivity(smsIntent);

            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { contactsFirebaseModels.get(i).getEmails().get(0).getAddress() });
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, "mail body");
                context.startActivity(Intent.createChooser(intent, ""));

//                Uri uri = Uri.parse("mailto:" + contactsFirebaseModels.get(i).getEmails().get(0).getAddress())
//                        .buildUpon()
//                        .appendQueryParameter("subject", "")
//                        .appendQueryParameter("body", "")
//                        .build();
//
//                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
//                context.startActivity(Intent.createChooser(emailIntent, ""));
            }
        });

//        contact_name_lay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                PreferenceConnector.getInstance(context).savePreferences("ContactName",contactsFirebaseModels.get(i).getName());
//                PreferenceConnector.getInstance(context).savePreferences("ContactNumber",contactsFirebaseModels.get(i).getPhonenumber());
//                Intent intent=new Intent(context,CallLogsActivityDashBoard.class);
//                context.startActivity(intent);
//            }
//        });
        return view;
    }

//    @Override
//    public void notifyDataSetChanged() {
//        super.notifyDataSetChanged();
//    }
}

