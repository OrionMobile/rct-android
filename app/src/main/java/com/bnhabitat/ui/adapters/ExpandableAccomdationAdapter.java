package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bnhabitat.R;
import com.bnhabitat.models.ChildItemModel;
import com.bnhabitat.models.ExpandableAccomodationModel;
import com.bnhabitat.ui.views.ChildItemViewholder;
import com.bnhabitat.ui.views.GroupAccomViewHolder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;


import java.util.List;

/**
 * Created by gourav on 5/15/2018.
 */

public class ExpandableAccomdationAdapter extends ExpandableRecyclerViewAdapter<GroupAccomViewHolder,ChildItemViewholder> {
    Context context;
    public ExpandableAccomdationAdapter(Context context,List<? extends ExpandableGroup> groups) {
        super(groups);
        this.context=context;
    }
    @Override
    public GroupAccomViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.specification_list_items, parent, false);
        return new GroupAccomViewHolder(view);
    }

    @Override
    public ChildItemViewholder onCreateChildViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expandable_list_subgroup_items, parent, false);
        return new ChildItemViewholder(view);

    }

    @Override
    public void onBindChildViewHolder(ChildItemViewholder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final ChildItemModel phone = ((ExpandableAccomodationModel) group).getItems().get(childIndex);
        holder.onBind(phone,group);

    }

    @Override
    public void onBindGroupViewHolder(GroupAccomViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGroupName(group);
        holder.expand();
    }
}
