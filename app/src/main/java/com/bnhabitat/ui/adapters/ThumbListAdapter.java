package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.activities.ZoomActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by gourav on 7/13/2017.
 */

public class ThumbListAdapter extends RecyclerView.Adapter<ThumbListAdapter.ViewHolder> {
    Context context;
    ArrayList<ProjectsDetailModel.LayoutPlans>layoutPlanses  = new ArrayList<>();

    public ThumbListAdapter(Context context, ArrayList<ProjectsDetailModel.LayoutPlans> layoutPlanses) {
        this.context = context;
        this.layoutPlanses = layoutPlanses;
    }

    @Override
    public ThumbListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.thumb_item, parent, false);
        ThumbListAdapter.ViewHolder viewHolder = new ThumbListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ThumbListAdapter.ViewHolder holder, final int position) {


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        imageLoader.displayImage(Urls.BASE_IMAGE_URL+layoutPlanses.get(position).getBigImage(),holder.icon_imag,options);
        holder.icon_imag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context,ZoomActivity.class);
                intent.putExtra("layout_plan","layout_planImage");
                intent.putExtra(Constants.ZOOM_LIST,layoutPlanses);
                intent.putExtra("position",String.valueOf(position));
                context.startActivity(intent);
//                    getActivity().finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return layoutPlanses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView  icon_imag;
        TextView name,description;

        public ViewHolder(View itemView) {
            super(itemView);

            icon_imag = (ImageView) itemView.findViewById(R.id.thumb_images);
//            share_icon = (ImageView) itemView.findViewById(R.id.share_icon);
//            notification_icon = (ImageView) itemView.findViewById(R.id.notification_icon);
//            user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
        }
    }
}

