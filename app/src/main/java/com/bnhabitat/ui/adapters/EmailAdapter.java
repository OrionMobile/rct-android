package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bnhabitat.R;
import com.bnhabitat.interfaces.OnEditTextChanged;
import com.bnhabitat.models.ContactsModel;
import com.bnhabitat.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gourav on 5/31/2018.
 */

public class EmailAdapter extends RecyclerView.Adapter<EmailAdapter.HolderRecycler> {

    Context context;
    ArrayList<HashMap<Integer, ContactsModel.Email>> arrayList;
    OnEditTextChanged onEditTextChanged;

  public   static String isEmailValid = "";

    public EmailAdapter(Context context, ArrayList<HashMap<Integer, ContactsModel.Email>> arrayList, OnEditTextChanged onEditTextChanged) {
        this.context = context;
        this.arrayList = arrayList;
        this.onEditTextChanged = onEditTextChanged;
    }

    @NonNull
    @Override
    public EmailAdapter.HolderRecycler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_emails_layout, parent, false);
        EmailAdapter.HolderRecycler holderRecycler = new EmailAdapter.HolderRecycler(view);
        return holderRecycler;
    }

    public void updateList(ArrayList<HashMap<Integer, ContactsModel.Email>> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull EmailAdapter.HolderRecycler holder, final int position) {

        final HashMap<Integer, ContactsModel.Email> hashMap = arrayList.get(position);
        final ContactsModel.Email email = hashMap.get(position);

        holder.editText.setText(email.getAddress());


        holder.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equalsIgnoreCase("")){
                    if (Utils.isEmailAddressValid(s.toString())) {

                        isEmailValid = "true";

                        email.setAddress(s.toString());
                        hashMap.put(position, email);
                        onEditTextChanged.onTextChange1(arrayList);
                    }
                    else {
                        isEmailValid = "false";

                    }
                }
                else{
                    isEmailValid = "false";
                    email.setAddress(s.toString());
                    hashMap.put(position, email);
                    onEditTextChanged.onTextChange1(arrayList);
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class HolderRecycler extends RecyclerView.ViewHolder {

        @BindView(R.id.email)
        EditText editText;


        public HolderRecycler(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
