package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.FeatureModel;
import com.bnhabitat.models.FeatureParkModel;
import com.bnhabitat.models.StatusModel;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;


public class AddParkingAdapter extends RecyclerView.Adapter<AddParkingAdapter.ViewHolder> {
    Context context;
    private ArrayList<FeatureModel> parking_list = new ArrayList();
    private ArrayList<FeatureModel> parkingArrayListLocal = new ArrayList();

    public AddParkingAdapter(Context context, ArrayList<FeatureModel> parking_list) {
        this.context = context;
        this.parking_list = parking_list;
        this.parkingArrayListLocal = parking_list;

/*
        for (int i = 0; i < parking_list.size(); i++) {
            FeatureModel featureModel = new FeatureModel();
            featureModel.setId(parking_list.get(i).getId());
            featureModel.setName(parking_list.get(i).getName());
            featureModel.setType(parking_list.get(i).getType());
            featureModel.setTotalCount(parking_list.get(i).getTotalCount());

            parkingArrayListLocal.add(featureModel);
        }
*/

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.add_park_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (parking_list!=null){

            holder.parking_name.setText(parking_list.get(position).getName());
            holder.plus.setTag(position);
            holder.minus.setTag(position);

            if (parking_list.get(position).getTotalCount()!= null && !parking_list.get(position).getTotalCount().equalsIgnoreCase("0"))
                holder.no_of_count.setText(parking_list.get(position).getTotalCount());
            else
                holder.no_of_count.setText("0");

            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(holder.no_of_count.getText().toString());

                    if (quantity > 0)
                        quantity--;
                    holder.no_of_count.setText(String.valueOf(quantity));
                    parkingArrayListLocal.get(position).setId(parking_list.get(position).getId());

                    parkingArrayListLocal.get(position).setName(parking_list.get(position).getName());
                    parkingArrayListLocal.get(position).setType(parking_list.get(position).getType());
                    parkingArrayListLocal.get(position).setTotalCount(String.valueOf(quantity));
                    EventBus.getDefault().post("parking");

                    EventBus.getDefault().post(parkingArrayListLocal);

                }
            });

            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int quantity = Integer.parseInt(holder.no_of_count.getText().toString());

                    quantity++;
                    holder.no_of_count.setText(String.valueOf(quantity));
                    parkingArrayListLocal.get(position).setId(parking_list.get(position).getId());

                    parkingArrayListLocal.get(position).setName(parking_list.get(position).getName());
                    parkingArrayListLocal.get(position).setType(parking_list.get(position).getType());
                    parkingArrayListLocal.get(position).setTotalCount(String.valueOf(quantity));
                    EventBus.getDefault().post("parking");

                    EventBus.getDefault().post(parkingArrayListLocal);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return parking_list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView parking_name, no_of_count;
        ImageView minus, plus;

        public ViewHolder(View itemView) {
            super(itemView);

            parking_name = (TextView) itemView.findViewById(R.id.parking_name);
            no_of_count = (TextView) itemView.findViewById(R.id.no_of_count);
            minus = (ImageView) itemView.findViewById(R.id.minus_btn);
            plus = (ImageView) itemView.findViewById(R.id.plus_btn);

        }

    }

}
