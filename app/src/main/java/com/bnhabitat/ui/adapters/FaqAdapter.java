package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsDetailModel;

import java.util.ArrayList;

/**
 * Created by gourav on 5/19/2017.
 */

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.ViewHolder> implements Filterable {
    Context context;
    private ArrayList<ProjectsDetailModel.FAQs> faQses = new ArrayList<>();
    private ArrayList<ProjectsDetailModel.FAQs> tempfaQses = new ArrayList<>();
//    boolean isOpened;
    ArrayList<Boolean> isOpened=new ArrayList<>();
    int selectedPosition=-1;

    public FaqAdapter(Context context, ArrayList<ProjectsDetailModel.FAQs> faQses) {
        this.context = context;
        this.faQses = faQses;
        this.tempfaQses = faQses;
        for(ProjectsDetailModel.FAQs faQs:faQses){
            isOpened.add(false);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.faq_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.question_name.setText(Html.fromHtml(tempfaQses.get(position).getQuestion()));
        holder.answer.setText(Html.fromHtml(tempfaQses.get(position).getAnswer()));
        holder.answer.setVisibility(View.GONE);
        if(isOpened.get(position)){
            holder.answer.setVisibility(View.VISIBLE);
        }
        holder.question_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int index = 0; index < tempfaQses.size(); index++) {
                    isOpened.set(index, false);
                }

                if (position==selectedPosition) {
//                    holder.answer.setVisibility(View.VISIBLE);
                    selectedPosition=-1;
                    FaqAdapter.this.notifyDataSetChanged();

//                    isOpened = false;
                } else {
                    selectedPosition = position;
                    isOpened.set(position, true);
//                    FaqAdapter.this.notifyDataSetChanged();
//                    isOpened = true;
                }
                FaqAdapter.this.notifyDataSetChanged();
            }
        });
        holder.add_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int index = 0; index < tempfaQses.size(); index++) {
                    isOpened.set(index, false);
                }

                if (position==selectedPosition) {
//                    holder.answer.setVisibility(View.VISIBLE);
                    selectedPosition=-1;
                    FaqAdapter.this.notifyDataSetChanged();


//                    isOpened = false;
                } else {
                    selectedPosition = position;
                    isOpened.set(position, true);
//                    FaqAdapter.this.notifyDataSetChanged();
//                    isOpened = true;
                }
                FaqAdapter.this.notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {

        return tempfaQses.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView question_name, answer;

        ImageView add_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            question_name = (TextView) itemView.findViewById(R.id.question_name);
            answer = (TextView) itemView.findViewById(R.id.answer);
            add_icon = (ImageView) itemView.findViewById(R.id.add_icon);
//            share_icon = (ImageView) itemView.findViewById(R.id.share_icon);
//            notification_icon = (ImageView) itemView.findViewById(R.id.notification_icon);
//            user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
        }
    }

    @Override
    public  Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {

                tempfaQses = (ArrayList<ProjectsDetailModel.FAQs>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<ProjectsDetailModel.FAQs> FilteredArrList = new ArrayList<>();

                if (faQses == null) {
                    faQses = new ArrayList<ProjectsDetailModel.FAQs>(tempfaQses); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = faQses.size();
                    results.values = faQses;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < faQses.size(); i++) {
                        String data = faQses.get(i).getQuestion();
                        if (data.toLowerCase().startsWith(constraint.toString())) {



                            FilteredArrList.add(faQses.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }
}
