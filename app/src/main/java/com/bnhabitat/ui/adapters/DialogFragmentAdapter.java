package com.bnhabitat.ui.adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.models.StatusModel;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by Android on 5/16/2017.
 */

public class DialogFragmentAdapter extends RecyclerView.Adapter<DialogFragmentAdapter.ViewHolder> {

    Context context;
    ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList=new ArrayList<>();

    public DialogFragmentAdapter(Context context, ArrayList<ProjectsDetailModel.ProjectRelationshipManagers> projectRelationshipManagersArrayList) {
        this.context = context;
        this.projectRelationshipManagersArrayList = projectRelationshipManagersArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.custom_dialog_items,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.name.setText(projectRelationshipManagersArrayList.get(position).getName());
        holder.designation.setText(projectRelationshipManagersArrayList.get(position).getDesignation());

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StatusModel statusModel=new StatusModel();
                statusModel.setStatus("call");
                statusModel.setId(projectRelationshipManagersArrayList.get(position).getPhoneNumber());

                EventBus.getDefault().post(statusModel);


            }
        });

        holder.msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.addCategory(Intent.CATEGORY_DEFAULT);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.setData(Uri.parse("sms:" + projectRelationshipManagersArrayList.get(position).getPhoneNumber()));
//                        smsIntent.putExtra("sms_body","your desired message");
                try {
                    context.startActivity(smsIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(context, "No Activity Found", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    // Display some sort of error message here.
                }

            }
        });
        holder.email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { projectRelationshipManagersArrayList.get(position).getEmail() });
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, "mail body");
                context.startActivity(Intent.createChooser(intent, ""));
//                Uri uri = Uri.parse("mailto:" + projectRelationshipManagersArrayList.get(position).getEmail())
//                        .buildUpon()
//                        .appendQueryParameter("subject", "")
//                        .appendQueryParameter("body", "")
//                        .build();
//
//                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
//                context.startActivity(Intent.createChooser(emailIntent, ""));
            }
        });

    }

    @Override
    public int getItemCount() {
        return projectRelationshipManagersArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,designation;
        ImageView call,email,msg;
        public ViewHolder(View itemView) {
            super(itemView);

            name=(TextView)itemView.findViewById(R.id.name);
            designation=(TextView)itemView.findViewById(R.id.designation);
            email=(ImageView)itemView.findViewById(R.id.email);
            call=(ImageView)itemView.findViewById(R.id.call);
            msg=(ImageView)itemView.findViewById(R.id.msg);


        }
    }



}

