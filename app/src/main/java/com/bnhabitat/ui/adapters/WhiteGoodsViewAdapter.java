package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel;

import java.util.ArrayList;

/**
 * Created by gourav on 5/17/2018.
 */

public class WhiteGoodsViewAdapter extends RecyclerView.Adapter<WhiteGoodsViewAdapter.ViewHolder> {
    private Context context;
    //    private ArrayList<InventoryModel.PropertyBedrooms> _listDataHeader;
    ArrayList<InventoryModel.PropertySpecifications> _listDataChild=new ArrayList<>();//

    public WhiteGoodsViewAdapter(Context context,ArrayList<InventoryModel.PropertySpecifications> _listDataChild) {
        this.context = context;
        this._listDataChild = _listDataChild;


    }
    @Override
    public WhiteGoodsViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.specification_list_items, parent, false);
        WhiteGoodsViewAdapter.ViewHolder viewHolder = new WhiteGoodsViewAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(WhiteGoodsViewAdapter.ViewHolder holder, int position) {


        holder.item_description.setText(_listDataChild.get(position).getDescription());
        holder.item_name.setText(_listDataChild.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return _listDataChild.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView area_squnit, item_description,item_name, walls, ceiling, window, doors, storage, sanitary_wave, cabinet, locker, cirtains, carpets, air_conditioners, geyser;
//        LinearLayout layout_child;
//        RecyclerView recyclerViewSpecifications;

        public ViewHolder(View itemView) {
            super(itemView);
            item_description = (TextView) itemView
                    .findViewById(R.id.item_description);
            item_name = (TextView) itemView
                    .findViewById(R.id.item_name);
        }
    }
}
