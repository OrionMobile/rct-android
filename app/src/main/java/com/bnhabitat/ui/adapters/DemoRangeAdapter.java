package com.bnhabitat.ui.adapters;

import com.bnhabitat.ui.views.CustomRangeSeekBar;

/**
 * Created by Android on 5/23/2017.
 */

public class DemoRangeAdapter implements CustomRangeSeekBar.RangeAdapter {

    private static final int RANGE_MIN_VALUE = 1;
    private static final int RANGE_MAX_VALUE = 100;

    private static final int[] RANGE_STEPS = new int[] {
            47, 98, 149, 200, 251, 302
    };

    @Override
    public int getMinRangeValue() {
        return RANGE_MIN_VALUE;
    }

    @Override
    public int getMaxRangeValue() {
        return RANGE_MAX_VALUE;
    }

    @Override
    public boolean hasRangeSteps() {
        return false;
    }

    @Override
    public int getRangeStepsCount() {
        return RANGE_STEPS.length;
    }

    @Override
    public int getRangeStep(int stepIndex) {
        return RANGE_STEPS[stepIndex];
    }

}