package com.bnhabitat.ui.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bnhabitat.R;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.utils.Urls;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by gourav on 6/22/2017.
 */

public class ProjectLenderAdapter extends RecyclerView.Adapter<ProjectLenderAdapter.CustomViewHolder> {
    Activity context;
    ArrayList<ProjectsDetailModel.ProjectLenders> projectLenderses;
    public ProjectLenderAdapter(Activity context,  ArrayList<ProjectsDetailModel.ProjectLenders> projectLenderses) {
        this.projectLenderses = projectLenderses;
        this.context = context;
    }
    @Override
    public ProjectLenderAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_projectlender, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProjectLenderAdapter.CustomViewHolder holder, int position) {

//

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        imageLoader.displayImage(Urls.BASE_IMAGE_URL+projectLenderses.get(position).getLogo(), holder.lender,options);
//        Utils.setImageView(context, Urls.BASE_IMAGE_URL+services1.getImagePath(),holder.service_img,R.drawable.loading,R.drawable.loading);

//        LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        buttonLayoutParams.setMargins(50, 10, 0, 0);
//        holder.mainLayout.setLayoutParams(buttonLayoutParams);


    }

    @Override
    public int getItemCount() {
        return projectLenderses.size();
    }
    public class CustomViewHolder extends RecyclerView.ViewHolder {


        protected ImageView lender;




        public CustomViewHolder(View view) {
            super(view);
//

            this.lender = (ImageView) view.findViewById(R.id.lender);

//
//
        }
    }
}
