package com.bnhabitat.ui.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.bnhabitat.R;
import com.bnhabitat.models.FeatureModel;
import com.bnhabitat.models.SpecificationListModel;
import com.bnhabitat.models.StatusModel;
import java.util.ArrayList;
import de.greenrobot.event.EventBus;

public class FeatureAddAdapter extends RecyclerView.Adapter<FeatureAddAdapter.ViewHolder> {
    Context context;
    private ArrayList<FeatureModel> featureModelArrayList = new ArrayList();
    private ArrayList<FeatureModel> featureModelArrayListLocal = new ArrayList();

    public FeatureAddAdapter(Context context, ArrayList<FeatureModel> feature_checked_list) {
        this.context = context;
        this.featureModelArrayList = feature_checked_list;
        this.featureModelArrayListLocal = feature_checked_list;

/*
        for (int i = 0; i < featureModelArrayList.size(); i++) {
            FeatureModel featureModel = new FeatureModel();
            featureModel.setId(featureModelArrayList.get(i).getId());
            featureModel.setName(featureModelArrayList.get(i).getName());
            featureModel.setType(featureModelArrayList.get(i).getType());
            featureModel.setStatus(featureModelArrayList.get(i).getStatus());

            featureModelArrayListLocal.add(featureModel);
        }
*/

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.feature_checked_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (featureModelArrayList!=null){

            holder.feature_checked.setText(featureModelArrayList.get(position).getName());

            if (featureModelArrayList.get(position).getTotalCount().equals("1"))
                holder.feature_checked.setChecked(true);
            else
                holder.feature_checked.setChecked(false);

            holder.feature_checked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked){
                        featureModelArrayListLocal.get(position).setId(featureModelArrayList.get(position).getId());

                        featureModelArrayListLocal.get(position).setName(featureModelArrayList.get(position).getName());
                        featureModelArrayListLocal.get(position).setType(featureModelArrayList.get(position).getType());
                        featureModelArrayListLocal.get(position).setTotalCount("1");

                        featureModelArrayListLocal.get(position).setStatus("checked");
                        EventBus.getDefault().post("feature");

                        EventBus.getDefault().post(featureModelArrayListLocal);
                    }

                    else {
                        featureModelArrayListLocal.get(position).setId(featureModelArrayList.get(position).getId());
                        featureModelArrayListLocal.get(position).setName(featureModelArrayList.get(position).getName());
                        featureModelArrayListLocal.get(position).setType(featureModelArrayList.get(position).getType());
                        featureModelArrayListLocal.get(position).setTotalCount("0");
                        featureModelArrayListLocal.get(position).setStatus("unchecked");
                        EventBus.getDefault().post("feature");

                        EventBus.getDefault().post(featureModelArrayListLocal);
                    }

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return featureModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox feature_checked;
        public ViewHolder(View itemView) {
            super(itemView);
            feature_checked = (CheckBox) itemView.findViewById(R.id.feature_checked);
        }
    }

}
