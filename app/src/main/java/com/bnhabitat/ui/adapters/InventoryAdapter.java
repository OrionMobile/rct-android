package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.ProjectsModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.ui.activities.PropertyItemActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.ViewHolder> {
    Context context;
    private ArrayList<InventoryModel> inventoryModels = new ArrayList();
    private ArrayList<InventoryModel.PropertyLocation> propertyLocations = new ArrayList();


    public InventoryAdapter() {


    }

    public InventoryAdapter(Context context, ArrayList<InventoryModel> inventoryModels) {
        this.context = context;
        this.inventoryModels = inventoryModels;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inventory_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        propertyLocations = inventoryModels.get(position).getPropertyLocations();
        if (inventoryModels.get(position).getTypeobject() != null && inventoryModels.get(position).getTypeobject().equalsIgnoreCase("null") && inventoryModels.get(position).getNameobject().equalsIgnoreCase("null")) {

            holder.property_type.setText(inventoryModels.get(position).getTypeobject() + " " + inventoryModels.get(position).getNameobject());

        } else

            holder.property_type.setVisibility(View.GONE);


        String strlocality = "";


        if (propertyLocations.size() > 0) {

            holder.locality.setText(Utils.getEmptyValue(propertyLocations.get(0).getProject()));

        }

        if (propertyLocations.size() > 0 && inventoryModels.get(position).getPropertyAccommodationDetailses().size() != 0) {

            try {
                holder.parking.setText(inventoryModels.get(position).getPropertyAccommodationDetailses().get(0).getTotalCount() + " Parking");

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                double price_txt = Double.parseDouble(String.valueOf(inventoryModels.get(position).getPropertyFinancialses().get(0).getDemandPrice()));
                holder.price.setText(Utils.getConvertedPrice((long) price_txt, context));

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                holder.area_unit.setText(inventoryModels.get(position).getPropertyAreas().get(0).getPlotArea() + " " + inventoryModels.get(position).getPropertyAreas().get(0).getPropertySizeUnit4());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
       // if (!inventoryModels.get(position).getPropertyImages().isEmpty())
        //    imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + inventoryModels.get(position).getPropertyImages().get(0).getUrl(), holder.project_image, options);

        holder.edit_property.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    Intent intent = new Intent(context, PropertyItemActivity.class);
                    intent.putExtra("edit", "edit_property");
                    intent.putExtra("json_Data", inventoryModels);
                    intent.putExtra("Id", inventoryModels.get(position).getId());
                    intent.putExtra("PropertyTypeId", inventoryModels.get(position).getPropertyTypeId());
                    intent.putExtra("PropertyName", inventoryModels.get(position).getNameobject());
                    intent.putExtra(Constants.FROM, inventoryModels.get(position).getTypeobject());


                    context.startActivity(intent);

                } catch (Exception e) {

                    e.printStackTrace();
                }

            }
        });

        holder.kitchen.setText(String.valueOf(inventoryModels.get(position).getTotalKitchen()) + " Kitchen");
        holder.bathroom.setText(String.valueOf(inventoryModels.get(position).getTotalBathrooms()) + " Bathroom");
        holder.parking.setText(String.valueOf(inventoryModels.get(position).getTotalParkings()) + " Parking");
        holder.bedroom.setText(String.valueOf(inventoryModels.get(position).getTotalRooms()) + " Bedroom");

    }

    @Override
    public int getItemCount() {
        return inventoryModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView project_image, edit_property;
        TextView property_type, locality, area_unit, price, bedroom, kitchen, parking, bathroom;

        public ViewHolder(View itemView) {
            super(itemView);
            property_type = (TextView) itemView.findViewById(R.id.property_type);
            area_unit = (TextView) itemView.findViewById(R.id.area_unit);
            price = (TextView) itemView.findViewById(R.id.price);
            locality = (TextView) itemView.findViewById(R.id.locality);
            parking = (TextView) itemView.findViewById(R.id.parking);
            kitchen = (TextView) itemView.findViewById(R.id.kitchen);
            bedroom = (TextView) itemView.findViewById(R.id.bedroom);
            bathroom = (TextView) itemView.findViewById(R.id.bathroom);
            project_image = (ImageView) itemView.findViewById(R.id.project_image);
            edit_property = (ImageView) itemView.findViewById(R.id.iv_edit);

        }


    }


}
