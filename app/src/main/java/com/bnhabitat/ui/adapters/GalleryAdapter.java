package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.InventoryModel;
import com.bnhabitat.models.ProjectsDetailModel;
import com.bnhabitat.ui.activities.ZoomActivity;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.Urls;
import com.bnhabitat.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

/**
 * Created by gourav on 5/10/2018.
 */

public class GalleryAdapter extends PagerAdapter {
    Context context;

    private ArrayList<InventoryModel.PropertyImage> propertyImages = new ArrayList();

    LayoutInflater mLayoutInflater;


    public GalleryAdapter(Context context, ArrayList<InventoryModel.PropertyImage> propertyImages) {
        this.context = context;
        this.propertyImages = propertyImages;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return propertyImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container,final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.gallery_images, container, false);



        ImageView project_image = (ImageView) itemView.findViewById(R.id.project_image);
//        ImageView edit_property = (ImageView) itemView.findViewById(R.id.edit_property);
//        ImageView more_details = (ImageView) itemView.findViewById(R.id.more_details);
//        ImageView share_property = (ImageView) itemView.findViewById(R.id.share_property);




        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.image_logo)
                .showImageOnFail(R.drawable.image_logo)
                .showImageOnLoading(R.drawable.image_logo).build();
        if (!propertyImages.isEmpty())
            imageLoader.displayImage(Urls.BASE_CONTACT_IMAGE_URL + propertyImages.get(position).getUrl(), project_image, options);

        ProjectsDetailModel.LayoutPlans layoutPlans=new ProjectsDetailModel().new LayoutPlans();

        project_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context,ZoomActivity.class);
                intent.putExtra("layout_plan","image_gallery");
                intent.putExtra(Constants.ZOOM_LIST,propertyImages);
                intent.putExtra("position",String.valueOf(position));
                context.startActivity(intent);
//                    getActivity().finish();
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }


}
