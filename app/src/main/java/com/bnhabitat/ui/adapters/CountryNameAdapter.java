package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.CountryModel;
import com.bnhabitat.models.FeatureParkModel;
import com.bnhabitat.models.PropertyBedroomModel;
import com.bnhabitat.models.StatusModel;
import com.bnhabitat.utils.BackPressEdit;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by gourav on 3/27/2018.
 */

public class CountryNameAdapter extends RecyclerView.Adapter<CountryNameAdapter.ViewHolder> {
    Context context;
    private ArrayList<CountryModel> countryList = new ArrayList();
    SpinnerAdapter spinnerAdapter;
    String attched_room_balcony_txt, attched_room_balcony_id;
    ArrayList<String> room_name = new ArrayList<>();
    ArrayList<FeatureParkModel> arrayList = new ArrayList<>();
    ArrayList<PropertyBedroomModel> propertyBedroomModels = new ArrayList<>();


    public CountryNameAdapter() {


    }

    public CountryNameAdapter(Context context, ArrayList<CountryModel> countryList) {
        this.context = context;
        this.countryList = countryList;
        spinnerAdapter = new SpinnerAdapter(context, R.layout.new_spinner_item);
        spinnerAdapter.addAll(room_name);
        spinnerAdapter.add(context.getString(R.string.select_type));

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.specification_item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.specification_name.setText(countryList.get(position).getDescription());

        holder.attched_room_balcony.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (holder.attched_room_balcony.getSelectedItem() == context.getString(R.string.select_type)) {

                    //Do nothing.
                } else {
                    try{
                        attched_room_balcony_txt = holder.attched_room_balcony.getSelectedItem().toString();
                        attched_room_balcony_id = propertyBedroomModels.get(position-1).getId();
                    }catch (Exception e){

                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView specification_name;
        Spinner attched_room_balcony;
        BackPressEdit specification_room_edittxt;

        public ViewHolder(View itemView) {
            super(itemView);
            specification_name = (TextView) itemView.findViewById(R.id.specification_name);
            attched_room_balcony = (Spinner) itemView.findViewById(R.id.attched_room_balcony);
            specification_room_edittxt = (BackPressEdit) itemView.findViewById(R.id.specification_room_edittxt);
        }
    }

}
