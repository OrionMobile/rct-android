package com.bnhabitat.ui.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.models.ChildItemModel;
import com.bnhabitat.models.ExpandableAccomodationModel;
import com.bnhabitat.models.InventoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gourav on 5/10/2018.
 */


public class ExpandableListAdapter extends RecyclerView.Adapter<ExpandableListAdapter.ViewHolder> {
    ArrayList<InventoryModel.PropertySpecifications> white_goods = new ArrayList<>();
    ArrayList<InventoryModel.PropertySpecifications> specificationsArrayList = new ArrayList<>();
    ArrayList<InventoryModel.PropertySpecifications> soft_furnishing_list = new ArrayList<>();
    private Context context;
    //    private ArrayList<InventoryModel.PropertyBedrooms> _listDataHeader;
    ArrayList<ExpandableAccomodationModel> expandableAccomodationModels = new ArrayList<>();// header titles
    ArrayList<InventoryModel.PropertyBedrooms> _listDataHeader = new ArrayList<>();// header titles
    ArrayList<InventoryModel.PropertySpecifications> _listDataChild = new ArrayList<>();// header titles
    // header titles
    // child data in format of header title, child title
//    private HashMap<String, List<String>> _listDataChild;
    SpecificationViewApapter specificationViewApapter;
    SoftFurnishingViewAdapter softFurnishingAdapter;
    WhiteGoodsViewAdapter whiteGoodsViewAdapter;

    public ExpandableListAdapter(Context context, ArrayList<InventoryModel.PropertyBedrooms> _listDataHeader, ArrayList<InventoryModel.PropertySpecifications> specificationsArrayList, ArrayList<InventoryModel.PropertySpecifications> soft_furnishing_list, ArrayList<InventoryModel.PropertySpecifications> white_goods
    ) {
        this.context = context;
        this._listDataHeader = _listDataHeader;
        this.white_goods = white_goods;
        this.specificationsArrayList = specificationsArrayList;
        this.soft_furnishing_list = soft_furnishing_list;

    }


    //
    @Override
    public ExpandableListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.expandable_list_items, parent, false);
        ExpandableListAdapter.ViewHolder viewHolder = new ExpandableListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ExpandableListAdapter.ViewHolder holder, int position) {
        holder.lblListHeader.setText(_listDataHeader.get(position).getGroundType());
        holder.recyclerViewSpecifications.setLayoutManager(new LinearLayoutManager(context));
        holder.recyclerViewsoftfurnish.setLayoutManager(new LinearLayoutManager(context));
        holder.recyclerViewwhite_goods.setLayoutManager(new LinearLayoutManager(context));

        if (specificationsArrayList.isEmpty()) {
            holder.specification_txt.setVisibility(View.GONE);
            holder.recyclerViewSpecifications.setVisibility(View.GONE);
        }
        if (soft_furnishing_list.isEmpty()) {
            holder.soft_txt.setVisibility(View.GONE);
            holder.recyclerViewsoftfurnish.setVisibility(View.GONE);
        }
        if (white_goods.isEmpty()) {
            holder.white_txt.setVisibility(View.GONE);
            holder.recyclerViewwhite_goods.setVisibility(View.GONE);
        }
        holder.lblListHeader.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);


//        try {
//            if (_listDataChild.get(position).getCategory().equalsIgnoreCase("White Goods")) {
//                InventoryModel.PropertySpecifications white_goods_propertySpecifications = new InventoryModel().new PropertySpecifications();
//                white_goods_propertySpecifications.setName(_listDataChild.get(position).getName());
//                white_goods.add(white_goods_propertySpecifications);
//
//            }
//        }catch (Exception e){
//
//        }


//
        holder.lblListHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.lblListHeader) {
                    if (holder.layout_child.getVisibility() == View.VISIBLE) {
                        holder.layout_child.setVisibility(View.GONE);
                        holder.lblListHeader.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24dp, 0);
                    } else {
                        holder.lblListHeader.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_up_black_24dp, 0);
                        holder.layout_child.setVisibility(View.VISIBLE);
                        specificationViewApapter = new SpecificationViewApapter(context, specificationsArrayList);
                        holder.recyclerViewSpecifications.setAdapter(specificationViewApapter);
                        softFurnishingAdapter = new SoftFurnishingViewAdapter(context, soft_furnishing_list);
                        holder.recyclerViewsoftfurnish.setAdapter(softFurnishingAdapter);
                        whiteGoodsViewAdapter = new WhiteGoodsViewAdapter(context, white_goods);
                        holder.recyclerViewwhite_goods.setAdapter(whiteGoodsViewAdapter);
//                        holder.recyclerViewfurnishing_list.setAdapter(specificationViewApapter);
                    }
                } else {
                    TextView textViewClicked = (TextView) view;
                    Toast.makeText(context, "" + textViewClicked.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
//    holder.flooring.setText(_listDataHeader.get(position).getFloorNo());
//    holder.walls.setText(_listDataHeader.get(position).getChildDataItems().get(position).getName());
    }

    @Override
    public int getItemCount() {
        return _listDataHeader.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView white_txt, soft_txt, specification_txt, ceiling, window, doors, storage, sanitary_wave, cabinet, locker, cirtains, carpets, air_conditioners, geyser, lblListHeader;
        LinearLayout layout_child;
        RecyclerView recyclerViewSpecifications, recyclerViewsoftfurnish, recyclerViewwhite_goods;

        public ViewHolder(View itemView) {
            super(itemView);
            lblListHeader = (TextView) itemView
                    .findViewById(R.id.lblListHeader);
            layout_child = (LinearLayout) itemView
                    .findViewById(R.id.layout_child);
            recyclerViewSpecifications = (RecyclerView) itemView
                    .findViewById(R.id.specification_list);
            recyclerViewsoftfurnish = (RecyclerView) itemView
                    .findViewById(R.id.furnishing_list);
            recyclerViewwhite_goods = (RecyclerView) itemView
                    .findViewById(R.id.white_goods_recycler);
            specification_txt = (TextView) itemView
                    .findViewById(R.id.specification_txt);
            soft_txt = (TextView) itemView
                    .findViewById(R.id.soft_txt);
            white_txt = (TextView) itemView
                    .findViewById(R.id.white_txt);

//
        }


        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.lblListHeader) {
                if (layout_child.getVisibility() == View.VISIBLE) {
                    layout_child.setVisibility(View.GONE);
                } else {
                    layout_child.setVisibility(View.VISIBLE);
                }
            } else {
                TextView textViewClicked = (TextView) view;
                Toast.makeText(context, "" + textViewClicked.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}