package com.bnhabitat.ui.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.bnhabitat.R;
import com.bnhabitat.models.CapturePlcModel;
import com.bnhabitat.models.FeatureModel;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class CapturePlcAddAdapter extends RecyclerView.Adapter<CapturePlcAddAdapter.ViewHolder> {
    Context context;
    private ArrayList<CapturePlcModel> featureModelArrayList = new ArrayList();
    private ArrayList<CapturePlcModel> featureModelArrayListLocal = new ArrayList();

    public CapturePlcAddAdapter(Context context, ArrayList<CapturePlcModel> feature_checked_list) {
        this.context = context;
        this.featureModelArrayList = feature_checked_list;
        this.featureModelArrayListLocal = feature_checked_list;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.feature_checked_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (featureModelArrayList!=null){

            holder.feature_checked.setText(featureModelArrayList.get(position).getName());

            if (featureModelArrayList.get(position).isChecked())
                holder.feature_checked.setChecked(true);
            else
                holder.feature_checked.setChecked(false);

            holder.feature_checked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked){
                        featureModelArrayListLocal.get(position).setId(featureModelArrayList.get(position).getId());

                        featureModelArrayListLocal.get(position).setName(featureModelArrayList.get(position).getName());
                        featureModelArrayListLocal.get(position).setChecked(true);
                     //   EventBus.getDefault().post("feature");

                        EventBus.getDefault().post(featureModelArrayListLocal);
                    }

                    else {
                        featureModelArrayListLocal.get(position).setId(featureModelArrayList.get(position).getId());
                        featureModelArrayListLocal.get(position).setName(featureModelArrayList.get(position).getName());
                        featureModelArrayListLocal.get(position).setChecked(false);
                       // EventBus.getDefault().post("feature");

                        EventBus.getDefault().post(featureModelArrayListLocal);
                    }

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return featureModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox feature_checked;
        public ViewHolder(View itemView) {
            super(itemView);
            feature_checked = (CheckBox) itemView.findViewById(R.id.feature_checked);
        }
    }

}
