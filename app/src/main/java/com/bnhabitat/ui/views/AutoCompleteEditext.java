package com.bnhabitat.ui.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by gourav on 1/30/2018.
 */

public class AutoCompleteEditext extends android.support.v7.widget.AppCompatAutoCompleteTextView {

    public AutoCompleteEditext(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AutoCompleteEditext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AutoCompleteEditext(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "OpenSans-Regular.ttf");
            setTypeface(tf);
        }
    }
    @Override
    protected void performFiltering(final CharSequence text, final int keyCode) {
        String filterText = "";
        super.performFiltering(filterText, keyCode);
    }
}