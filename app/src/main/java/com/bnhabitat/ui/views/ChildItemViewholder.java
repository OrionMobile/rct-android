package com.bnhabitat.ui.views;

import android.view.View;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ChildItemModel;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

/**
 * Created by gourav on 5/17/2018.
 */

public class ChildItemViewholder extends ChildViewHolder {
    private TextView item_name,item_description;
    public ChildItemViewholder(View itemView) {
        super(itemView);

        item_description = (TextView) itemView
                .findViewById(R.id.item_description);
        item_name = (TextView) itemView
                .findViewById(R.id.item_name);
    }
    public void onBind(ChildItemModel ChildItemModel, ExpandableGroup group) {
        item_name.setText(ChildItemModel.getName());
        item_description.setText(ChildItemModel.getDescription());
//        if (group.getTitle().equals("Android")) {
//            phoneName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.nexus, 0, 0, 0);
//        } else if (group.getTitle().equals("iOS")) {
//            phoneName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.iphone, 0, 0, 0);
//        } else {
//            phoneName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.window_phone, 0, 0, 0);
//        }
    }
}
