package com.bnhabitat.ui.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by gourav on 2/20/2017.
 */

public class CustomTextViewItalic extends android.support.v7.widget.AppCompatTextView {
    public CustomTextViewItalic(Context context) {
        super(context);
        setFont();
    }
    public CustomTextViewItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public CustomTextViewItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "Roboto-BoldItalic.ttf");
        setTypeface(font, Typeface.BOLD_ITALIC);
    }
}
