package com.bnhabitat.ui.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by gourav on 5/16/2017.
 */

public class CustomTextviewSemiBod extends android.support.v7.widget.AppCompatTextView {
    public CustomTextviewSemiBod(Context context) {
        super(context);
        setFont();
    }
    public CustomTextviewSemiBod(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public CustomTextviewSemiBod(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "OpenSans-Semibold.ttf");
        setTypeface(font, Typeface.NORMAL);
    }
}
