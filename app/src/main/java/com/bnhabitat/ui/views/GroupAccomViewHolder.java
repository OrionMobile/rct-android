package com.bnhabitat.ui.views;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.bnhabitat.R;
import com.bnhabitat.models.ExpandableAccomodationModel;
import com.bnhabitat.models.InventoryModel;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;



/**
 * Created by gourav on 5/15/2018.
 */

public class GroupAccomViewHolder extends GroupViewHolder {
    TextView category;
    public GroupAccomViewHolder(View itemView) {
        super(itemView);
//        category = (TextView) itemView.findViewById(R.id.category);
//        arrow = (ImageView) itemView.findViewById(R.id.list_item_genre_arrow);
//        icon = (ImageView) itemView.findViewById(R.id.list_item_genre_icon);
    }
    @Override
    public void expand() {
//        headerName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow, 0);
        Log.i("Adapter", "expand");
    }

    @Override
    public void collapse() {
        Log.i("Adapter", "collapse");
//        headerName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow, 0);
    }

    public void setGroupName(ExpandableGroup group) {
        category.setText(group.getTitle());
    }
}
