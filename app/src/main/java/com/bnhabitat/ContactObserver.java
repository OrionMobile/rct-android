package com.bnhabitat;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

/**
 * Created by gourav on 8/18/2017.
 */

public  class ContactObserver extends ContentObserver {
    public ContactObserver(Handler handler) {
        super(handler);
    }

//    void observe() {
//        ContentResolver resolver = mContext.getContentResolver();
//        // set our watcher for the Uri we are concerned with
//        resolver.registerContentObserver(
//                Settings.System.getUriFor(Settings.System.MY_SUPER_SWEET_MOD_0),
//                false, // only notify that Uri of change *prob won't need true here often*
//                this); // this innerclass handles onChange
//
//        // another watcher
//        resolver.registerContentObserver(Settings.System.getUriFor(
//                Settings.System.MY_SUPER_SWEET_MOD_1), false, this);
//    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
        Log.e("", "~~~~~~" + selfChange);
        // Override this method to listen to any changes
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        Log.e("changes--", "changes add" + selfChange);
        // depending on the handler you might be on the UI
        // thread, so be cautious!
    }


}
