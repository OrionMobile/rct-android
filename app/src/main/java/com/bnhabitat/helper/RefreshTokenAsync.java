package com.bnhabitat.helper;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.connection.ConnectionManager;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Urls;

import org.json.JSONObject;

import java.io.InputStream;

/**
 * Created by developer15 on 4/28/2016.
 */
public class RefreshTokenAsync extends AsyncTask<String, Void, String> {

    private static int TIMEOUT_ERROR = 1;
    private static int CONVERTION_ERROR = 2;

    private String userId;
    private String authKey;
    private ConnectionManager connectionManager;
    private Context context;
    private String url;
    private String authUrl;
    private CommonAsync.OnAsyncResultListener onAsyncResultListener;
    private OnRefreshAsyncResultListener onReferhAsyncResultListener;
    private String input = "";
    private InputStream inputStream;
    private String inputS;
    private boolean isAuthenticateCalled = false;
    private ProgressDialog authDialog;
    private String message;
    private String output = "";
    private String which = "";
    private int requestType;
    private long startTime;
    private int RESPONSE = 0;
    private boolean showProgress = true;
    private String accessToken = Constants.UTILITY_TOKEN;

    public RefreshTokenAsync(Context context,
                             OnRefreshAsyncResultListener onReferhAsyncResultListener,
                             boolean showProgress) {

        isAuthenticateCalled = false;
        this.context = context;
        this.onReferhAsyncResultListener = onReferhAsyncResultListener;
        connectionManager = ConnectionManager.getInstance(context);
        startTime = System.currentTimeMillis();
        authUrl = Urls.CONTACT_MANAGEMENT_URL + Urls.URL_GET_TOKEN;
        this.showProgress = showProgress;
        userId = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_ID, "");
        authKey = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_AUTH_KEY, "");
        Log.e("Data", "url: " + url + "  input: " + input);
    }


    public void setOnAsyncResultListener(OnRefreshAsyncResultListener onReferhAsyncResultListener) {

        this.onReferhAsyncResultListener = onReferhAsyncResultListener;
    }

    public interface OnRefreshAsyncResultListener {

        public void onRefreshResultListener(String result, String which);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //viewProgressVisible(message);
        if (showProgress)
            showProgressDialog();
    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub

        try {
            String inputJson = "";

            Log.e("Authenticate", "UserId : " + userId + " authKey" + authKey);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserId", userId);
            jsonObject.put("AuthKey", authKey);

            inputJson = jsonObject.toString();


//            List<BasicNameValuePair> parameter = new ArrayList<>();
//            parameter.add(new BasicNameValuePair("UserId",userId));
//            parameter.add(new BasicNameValuePair("AuthKey", authKey));
            inputS = connectionManager.excutePost(authUrl, inputJson);


//                output = connectionManager.converResponseToString(inputStream);


        } catch (Exception e) {

            inputS = "";
            e.printStackTrace();

        }

        return inputS;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (showProgress)
            animtion();


        if (RESPONSE == 0) {

            long elapsedTime = System.currentTimeMillis() - startTime;

            Log.e("API RESPONSE TIME : ", elapsedTime / 1000 + "");


            if (null != onReferhAsyncResultListener)
                try {

                    JSONObject jsonObject = new JSONObject(result);

                    PreferenceConnector.getInstance(context).savePreferences(Constants.APP_AUTH_KEY, jsonObject.getString("AuthToken"));
                    onReferhAsyncResultListener.onRefreshResultListener(output, which);

                } catch (Exception e) {

                }


        } else if (RESPONSE == TIMEOUT_ERROR) {

            Toast.makeText(context, "Unable to connect. Please check that you are connected to the Internet and try again.", Toast.LENGTH_LONG).show();

            if (null != onReferhAsyncResultListener)
                onReferhAsyncResultListener.onRefreshResultListener("", Constants.TIMEOUT);
        }
    }

    public void viewProgressGone() {
        authDialog.dismiss();
    }


    public void viewProgressVisible(String paramString) {
        authDialog = ProgressDialog.show(context, "Please wait..", message, true, false);
    }


    ImageView mAnimLogo;
    AnimationDrawable mAnimation;
    boolean mStart;
    Dialog dialog;

    private void showProgressDialog() {

        if (dialog == null) {


            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.loading_dialog);
            dialog.setCancelable(false);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            ImageView loaderimg = (ImageView) dialog.findViewById(R.id.loaderimg);


            //  mAnimation = (AnimationDrawable) mAnimLogo.getDrawable();

            Animation a = AnimationUtils.loadAnimation(context, R.anim.rotate_img);
            a.setDuration(1000);
            loaderimg.startAnimation(a);

            a.setInterpolator(new Interpolator() {
                private final int frameCount = 8;

                @Override
                public float getInterpolation(float input) {
                    return (float) Math.floor(input * frameCount) / frameCount;
                }
            });
        }


        dialog.show();
    }


    private void animtion() {
        try {
            if ((this.dialog != null) && this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            this.dialog = null;
        }

    }
}
