package com.bnhabitat.helper;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bnhabitat.R;
import com.bnhabitat.connection.ConnectionManager;
import com.bnhabitat.utils.Constants;
import com.bnhabitat.utils.PreferenceConnector;
import com.bnhabitat.utils.Utils;

import org.json.JSONObject;

import java.io.InputStream;

public class CommonAsync extends AsyncTask<Void, Void, Void> implements RefreshTokenAsync.OnRefreshAsyncResultListener {

    private static int TIMEOUT_ERROR = 1;
    private static int CONVERTION_ERROR = 2;

    private String userId;
    private String authKey;
    private ConnectionManager connectionManager;
    private Context context;
    private String url;
    private String authUrl;
    private OnAsyncResultListener onAsyncResultListener;
    private String input = "";
    private InputStream inputStream;
    private boolean isAuthenticateCalled = false;
    private ProgressDialog authDialog;
    private String message;
    private String output = "";
    private String which = "";
    private int requestType;
    private long startTime;
    private int RESPONSE = 0;
    private boolean showProgress = true;
    private String accessToken = Constants.UTILITY_TOKEN;
    private int authCountAttempt = 0;

    public CommonAsync(Context context,
                       String url, String input,
                       String message,
                       OnAsyncResultListener onAsyncResultListener,
                       String which, int requestType, boolean showProgress) {

        isAuthenticateCalled = false;
        this.context = context;
        this.url = url;
        this.input = input;
        this.message = message;
        this.onAsyncResultListener = onAsyncResultListener;
        this.which = which;
        connectionManager = ConnectionManager.getInstance(context);
        this.requestType = requestType;
        startTime = System.currentTimeMillis();
        this.showProgress = showProgress;
        Log.e("Data", "url: " + url + "  input: " + input);

        if (url.contains("utilities"))
            accessToken = Constants.UTILITY_TOKEN;
        else
            accessToken = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.APP_AUTH_KEY, "c4cdd3aa-6b9c-4eac-98b9-077fe2fad6ef");


        userId = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_ID, "");
//                Constants.APP_USER_ID;

//                PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_ID, "");
        authKey = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_AUTH_KEY, "c4cdd3aa-6b9c-4eac-98b9-077fe2fad6ef");

//        Constants.APP_AUTH_KEY;
//                PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_AUTH_KEY, "c4cdd3aa-6b9c-4eac-98b9-077fe2fad6ef");
        authCountAttempt = 0;
    }


    public CommonAsync(Context context,
                       String url, String input,
                       String message,
                       OnAsyncResultListener onAsyncResultListener,
                       String which, int requestType) {

        isAuthenticateCalled = false;
        this.context = context;
        this.url = url;
        this.input = input;
        this.message = message;
        this.onAsyncResultListener = onAsyncResultListener;
        this.which = which;
        connectionManager = ConnectionManager.getInstance(context);
        this.requestType = requestType;
        startTime = System.currentTimeMillis();

        userId = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_ID, "");
//                Constants.APP_USER_ID;
//
//        authKey = Constants.APP_AUTH_KEY;
        authKey = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.USER_AUTH_KEY, "c4cdd3aa-6b9c-4eac-98b9-077fe2fad6ef");

        if (url.contains("utilities"))
            accessToken = Constants.UTILITY_TOKEN;//aa54a7f0-93b8-4bee-9b4b-ba8af0b68d09
        else
            accessToken = PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.APP_AUTH_KEY, "aa54a7f0-93b8-4bee-9b4b-ba8af0b68d09");
        authCountAttempt = 0;
        Log.e("Data", "url: " + url + "  input: " + input);
    }


    public void setOnAsyncResultListener(OnAsyncResultListener onAsyncResultListener) {

        this.onAsyncResultListener = onAsyncResultListener;
    }

    public interface OnAsyncResultListener {

        public void onResultListener(String result, String which);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //viewProgressVisible(message);
        if (showProgress)
            showProgressDialog();
    }

    @Override
    protected Void doInBackground(Void... params) {
        // TODO Auto-generated method stub

        try {

            if (!isAuthenticateCalled) {

                if (requestType == Constants.GET) {

                    inputStream = connectionManager.connectionEstablishedGet(url, accessToken, PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.EMAIL, ""));

                } else {

                    if (input.trim().equalsIgnoreCase(""))
                        inputStream = connectionManager.connectionEstablished(url, accessToken, PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.EMAIL, ""));
                    else
                        inputStream = connectionManager.connectionEstablished(url, input, accessToken, PreferenceConnector.getInstance(context).loadSavedPreferences(Constants.EMAIL, ""));

                }
            } else {
                Log.e("Authenticate", "UserId : " + userId + " authKey" + authKey);
//              inputStream = connectionManager.connectionTokenEstablished(authUrl, userId, authKey, context);
            }

            if (null != inputStream)
                output = connectionManager.converResponseToString(inputStream);
            else
                RESPONSE = TIMEOUT_ERROR;


        } catch (Exception e) {

            output = "";
            e.printStackTrace();

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        boolean isProgressStop = true;
        if (RESPONSE == 0) {

            long elapsedTime = System.currentTimeMillis() - startTime;
            Log.e("API RESPONSE TIME : ", elapsedTime / 1000 + "" + output);

            try {

                JSONObject jsonObject = new JSONObject(output);

                if (jsonObject.getInt("StatusCode") == 403) {

                    if (authCountAttempt < 2) {
                        new RefreshTokenAsync(context, CommonAsync.this, false).execute();
                        authCountAttempt++;
                        isProgressStop = true;
                    } else {

                        Toast.makeText(context, "Not able to generate Auth Key", Toast.LENGTH_LONG).show();
                    }
                } else if (jsonObject.getInt("StatusCode") == 404) {

                    Toast.makeText(context, "No Projects Found", Toast.LENGTH_LONG).show();

                } else {

                    if (null != onAsyncResultListener)
                        onAsyncResultListener.onResultListener(output, which);

                }

            } catch (Exception e) {

                e.printStackTrace();
            }

        } else if (RESPONSE == TIMEOUT_ERROR) {

//            Toast.makeText(context, "Unable to connect. Please check that you are connected to the Internet and try again.", Toast.LENGTH_LONG).show();
            if (null != onAsyncResultListener)
                onAsyncResultListener.onResultListener("", Constants.TIMEOUT);
            Utils.showErrorMessagestatus(context.getString(R.string.unable_to_connect), context.getString(R.string.ok), context);
        }


        if (showProgress)
            if (isProgressStop)
                animtion();

    }

    public void viewProgressGone() {
        authDialog.dismiss();
    }

    public void viewProgressVisible(String paramString) {
        authDialog = ProgressDialog.show(context, "Please wait..", message, true, false);
    }

  //  ImageView mAnimLogo;
    AnimationDrawable mAnimation;
    boolean mStart;
    Dialog dialog;

    public void showProgressDialog() {

        if (context != null) {

            if (dialog == null) {
                dialog = new Dialog(context, android.R.style.Theme_NoTitleBar);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.custom_loading_dialog);
                dialog.setCancelable(false);
                TextView loadertext = (TextView) dialog.findViewById(R.id.message);
                loadertext.setText("");
                dialog.show();
            }

        }
    }

    private void animtion() {

        try {
            if ((this.dialog != null) && this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (final Exception e) {
            // Handle or log or ignore
        } finally {
            this.dialog = null;
        }

    }

    @Override
    public void onRefreshResultListener(String result, String from) {


        if (showProgress) {
            new CommonAsync(context,
                    url, input,
                    message,
                    onAsyncResultListener,
                    which, requestType, showProgress).execute();
        } else {
            new CommonAsync(context,
                    url, input,
                    message,
                    onAsyncResultListener,
                    which, requestType).execute();
        }
    }

}
