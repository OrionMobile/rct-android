package com.bnhabitat;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.bnhabitat.data.BaseManagerInterface;
import com.bnhabitat.listeners.BaseUIListener;
import com.bnhabitat.listeners.OnInitializedListener;
import com.bnhabitat.listeners.OnLoadListener;
import com.splunk.mint.Mint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;


//import com.google.android.gcm.GCMRegistrar;


/**
* Base entry point.
*
* @author alexander.ivanov
*/
public class Application extends android.app.Application {

	public static final int SDK_INT = Integer.valueOf(Build.VERSION.SDK);

	private static Application instance;

	public static Application getInstance() {
		if (instance == null)
			throw new IllegalStateException();
		return instance;
	}

	private final ArrayList<Object> registeredManagers;

	/**
	 * Unmodifiable collections of managers that implement some common
	 * interface.
	 */
	private Map<Class<? extends BaseManagerInterface>, Collection<? extends BaseManagerInterface>> managerInterfaces;

	private Map<Class<? extends BaseUIListener>, Collection<? extends BaseUIListener>> uiListeners;
	
	/**
	 * Thread to execute tasks in background..
	 */
	private final ExecutorService backgroundExecutor;

	/**
	 * Handler to execute runnable in UI thread.
	 */
	private final Handler handler;

	/**
	 * Where data load was requested.
	 */
	private boolean serviceStarted;

	/**
	 * Whether application was initialized.
	 */
	private boolean initialized;

	/**
	 * Whether user was notified about some action in contact list activity
	 * after application initialization.
	 */
	private boolean notified;

	/**
	 * Whether application is to be closed.
	 */
	private boolean closing;

	/**
	 * Whether {@link #onServiceDestroy()} has been called.
	 */
	private boolean closed;

	/**
	 * Future for loading process.
	 */
	private Future<Void> loadFuture;

//	private final Runnable timerRunnable = new Runnable() {
//
//		@Override
//		public void run() {
//			for (OnTimerListener listener : getManagers(OnTimerListener.class))
//				listener.onTimer();
//			if (!closing)
//				startTimer();
//		}
//
//	};

	public Application() {
		instance = this;
		serviceStarted = false;
		initialized = false;
		notified = false;
		closing = false;
		closed = false;
		uiListeners = new HashMap<Class<? extends BaseUIListener>, Collection<? extends BaseUIListener>>();
		managerInterfaces = new HashMap<Class<? extends BaseManagerInterface>, Collection<? extends BaseManagerInterface>>();
		registeredManagers = new ArrayList<Object>();

		handler = new Handler();
		backgroundExecutor = Executors
				.newSingleThreadExecutor(new ThreadFactory() {
					@Override
					public Thread newThread(Runnable runnable) {
						Thread thread = new Thread(runnable,
								"Background executor service");
						thread.setPriority(Thread.MIN_PRIORITY);
						thread.setDaemon(true);
						return thread;
					}
				});
	}

	/**
	 * Whether application is initialized.
	 */
	public boolean isInitialized() {
		return initialized;
	}

	private void onLoad() {
		for (OnLoadListener listener : getManagers(OnLoadListener.class)) {
			//LogManager.i(listener, "onLoad");
			listener.onLoad();
		}
	}
//
	private void onInitialized() {
		for (OnInitializedListener listener : getManagers(OnInitializedListener.class)) {
			//LogManager.i(listener, "onInitialized");
			listener.onInitialized();
		}
		initialized = true;

	}

//	private void onClose() {
////gManager.i(this, "onClose");
//		for (Object manager : registeredManagers)
//			if (manager instanceof OnCloseListener)
//				((OnCloseListener) manager).onClose();
//		closed = true;
//	}
//
//	private void onUnload() {
//		//LogManager.i(this, "onUnload");
//		for (Object manager : registeredManagers)
//			if (manager instanceof OnUnloadListener)
//				((OnUnloadListener) manager).onUnload();
//		android.os.Process.killProcess(android.os.Process.myPid());
//	}

	/**
	 * @return <code>true</code> only once per application life. Subsequent
	 *         calls will always returns <code>false</code>.
	 */
	public boolean doNotify() {
		if (notified)
			return false;
		notified = true;
		return true;
	}

	/**
	 * Starts data loading in background if not started yet.
	 *
	 * @return
	 */
	public void onServiceStarted() {
//		if (serviceStarted)
//			return;
//		serviceStarted = true;
//		//LogManager.i(this, "onStart");
//		loadFuture = backgroundExecutor.submit(new Callable<Void>() {
//			@Override
//			public Void call() throws Exception {
//				try {
////					onLoad();
//				} finally {
//					runOnUiThread(new Runnable() {
//						@Override
//						public void run() {
//							// Throw exceptions in UI thread if any.
//							try {
//								loadFuture.get();
//							} catch (InterruptedException e) {
//								throw new RuntimeException(e);
//							} catch (ExecutionException e) {
//								throw new RuntimeException(e);
//							}
//							onInitialized();
//						}
//					});
//				}
//				return null;
//			}
//		});
	}

	/**
	 * Requests to close application in some time in future.
	 */
	public void requestToClose() {
		closing = true;

	}

	/**
	 * @return Whether application is to be closed.
	 */
	public boolean isClosing() {
		return closing;
	}

	/**
	 * Returns whether system contact storage is supported.
	 *
	 * Note:
	 *
	 * Please remove *_CONTACTS, *_ACCOUNTS, *_SETTINGS permissions,
	 * SyncAdapterService and AccountAuthenticatorService together from manifest
	 * file.
	 *
	 * @return
	 */
	public boolean isContactsSupported() {
		return SDK_INT >= 5
				&& checkCallingOrSelfPermission("android.permission.READ_CONTACTS") == PackageManager.PERMISSION_GRANTED;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		try {
			Class.forName("android.os.AsyncTask");

			try{
				Mint.initAndStartSession(getApplicationContext(), "87648537");




//			TagManager tagManager = TagManager.getInstance(this);
//			tagManager.setVerboseLoggingEnabled(true);
//			PendingResult<ContainerHolder> pending =
//					tagManager.loadContainerPreferNonDefault(Constants.CONTAINER_ID,
//							R.raw.update_ana);
//			pending.setResultCallback(new ResultCallback<ContainerHolder>() {
//				@Override
//				public void onResult(ContainerHolder containerHolder) {
//					//ContainerHolderSingleton.setContainerHolder(containerHolder);
//
//					Container container = containerHolder.getContainer();
//					Log.e("CuteAnimals", "failure loading container"+containerHolder.getStatus().isSuccess());
//					if (!containerHolder.getStatus().isSuccess()) {
//						Log.e("CuteAnimals", "failure loading container");
//						//displayErrorToUser(R.string.load_error);
//						return;
//					}
//
//					Log.e("CuteAnimals", "Successfully loaded container"+container.getContainerId());
//
//					DataLayer dataLayer = TagManager.getInstance(getApplicationContext()).getDataLayer();
//					dataLayer.push("BrokerName", "Sandeeep");
//
//				}
//			}, 2, TimeUnit.SECONDS);




				//initGTM();
			}catch (Exception e){
				e.printStackTrace();
			}



		}
		catch(Throwable ignore) {

			Log.e("ignore","ignore");

			// ignored
		}


		TypedArray tableClasses = getResources().obtainTypedArray(R.array.tables);
		for (int index = 0; index < tableClasses.length(); index++)
			try {
				Class.forName(tableClasses.getString(index));
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		tableClasses.recycle();
	}


	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
//		MultiDex.install(this);
	}

	private void initGTM(){
//		TagManager tagManager = TagManager.getInstance(this);
//
//		//TODO : input your gtm info
//		PendingResult<ContainerHolder> pending = tagManager.loadContainerPreferNonDefault(Constants.CONTAINER_ID,R.raw.gtm/* your gtm default container resource */);
//
//		pending.setResultCallback(new ResultCallback<ContainerHolder>() {
//			@Override
//			public void onResult(ContainerHolder containerHolder) {
//				if(!containerHolder.getStatus().isSuccess()){
//					Log.e("Sandy", "failure loading container");
//					return;
//				}
//
//				ContainerHolderSingleton.setContainerHolder(containerHolder);
//				containerHolder.setContainerAvailableListener(new ContainerHolder.ContainerAvailableListener() {
//					@Override
//					public void onContainerAvailable(ContainerHolder containerHolder, String containerVer) {
//						Log.e("JuL", "containerVer = "+containerVer);
//					}
//				});
//
//
//			}
//		}, 2, TimeUnit.SECONDS);
	}

	@Override
	public void onLowMemory() {
//		for (OnLowMemoryListener listener : getManagers(OnLowMemoryListener.class))
//			listener.onLowMemory();
		super.onLowMemory();
	}

	/**
	 * Service have been destroyed.
	 */
	public void onServiceDestroy() {
//		if (closed)
//			return;
//		onClose();
//		runInBackground(new Runnable() {
//			@Override
//			public void run() {
//				onUnload();
//			}
//		}
//		);
	}

	@Override
	public void onTerminate() {
		requestToClose();
		super.onTerminate();
	}

	/**
	 * Start periodically callbacks.
	 */
	private void startTimer() {
//		runOnUiThreadDelay(timerRunnable, OnTimerListener.DELAY);
	}

	/**
	 * Register new manager.
	 *
	 * @param manager
	 */
	public void addManager(Object manager) {
		registeredManagers.add(manager);
	}

	/**
	 * @param cls
	 *            Requested class of managers.
	 * @return List of registered manager.
	 */
	@SuppressWarnings("unchecked")
	public <T extends BaseManagerInterface> Collection<T> getManagers(Class<T> cls) {
		if (closed)
			return Collections.emptyList();
		Collection<T> collection = (Collection<T>) managerInterfaces.get(cls);
		if (collection == null) {
			collection = new ArrayList<T>();
			for (Object manager : registeredManagers)
				if (cls.isInstance(manager))
					collection.add((T) manager);
			collection = Collections.unmodifiableCollection(collection);
			managerInterfaces.put(cls, collection);
		}
		return collection;
	}

	/**
	 * Request to clear application data.
	 */
	public void requestToClear() {
//		runInBackground(new Runnable() {
//			@Override
//			public void run() {
//				clear();
//			}
//		});
	}

//	private void clear() {
//		for (Object manager : registeredManagers)
//			if (manager instanceof OnClearListener)
//				((OnClearListener) manager).onClear();
//	}

	/**
	 * Request to wipe all sensitive application data.
	 */
//	public void requestToWipe() {
//		runInBackground(new Runnable() {
//			@Override
//			public void run() {
//				clear();
//				for (Object manager : registeredManagers)
//					if (manager instanceof OnWipeListener)
//						((OnWipeListener) manager).onWipe();
//			}
//		});
//	}
//
//	@SuppressWarnings("unchecked")
//	private <T extends BaseUIListener> Collection<T> getOrCreateUIListeners(
//			Class<T> cls) {
//		Collection<T> collection = (Collection<T>) uiListeners.get(cls);
//		if (collection == null) {
//			collection = new ArrayList<T>();
//			uiListeners.put(cls, collection);
//		}
//		return collection;
//	}
//
//	/**
//	 * @param cls
//	 *            Requested class of listeners.
//	 * @return List of registered UI listeners.
//	 */
//	public <T extends BaseUIListener> Collection<T> getUIListeners(Class<T> cls) {
//		if (closed)
//			return Collections.emptyList();
//		return Collections.unmodifiableCollection(getOrCreateUIListeners(cls));
//	}
//
//	/**
//	 * Register new listener.
//	 *
//	 * Should be called from {@link android.app.Activity#onResume()}.
//	 *
//	 * @param cls
//	 * @param listener
//	 */
//	public <T extends BaseUIListener> void addUIListener(Class<T> cls,
//			T listener) {
//		getOrCreateUIListeners(cls).add(listener);
//	}
//
//	/**
//	 * Unregister listener.
//	 *
//	 * Should be called from {@link android.app.Activity#onPause()}.
//	 *
//	 * @param cls
//	 * @param listener
//	 */
//	public <T extends BaseUIListener> void removeUIListener(Class<T> cls,
//			T listener) {
//		getOrCreateUIListeners(cls).remove(listener);
//	}
//
//	/**
//	 * Notify about error.
//	 *
//	 * @param resourceId
//	 */
//	public void onError(final int resourceId) {
//		runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				for (OnErrorListener onErrorListener : getUIListeners(OnErrorListener.class))
//					onErrorListener.onError(resourceId);
//			}
//		});
//	}
//
//	/**
////	 * Notify about error.
////	 *
////	 * @param networkException
////	 */
////	public void onError(NetworkException networkException) {
////		LogManager.exception(this, networkException);
////		onError(networkException.getResourceId());
////	}
//
//	/**
//	 * Submits request to be executed in background.
//	 *
//	 * @param runnable
//	 */
//	public void runInBackground(final Runnable runnable) {
//		backgroundExecutor.submit(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					runnable.run();
//				} catch (Exception e) {
//					//LogManager.exception(runnable, e);
//				}
//			}
//		});
//	}
//
//	/**
//	 * Submits request to be executed in UI thread.
//	 *
//	 * @param runnable
//	 */
//	public void runOnUiThread(final Runnable runnable) {
//		handler.post(runnable);
//	}
//
//	/**
//	 * Submits request to be executed in UI thread.
//	 *
//	 * @param runnable
//	 * @param delayMillis
//	 */
//	public void runOnUiThreadDelay(final Runnable runnable, long delayMillis) {
//		handler.postDelayed(runnable, delayMillis);
//	}
//
//
//
//	private  final int MAX_ATTEMPTS = 5;
//	private  final int BACKOFF_MILLI_SECONDS = 2000;
//	private  final Random random = new Random();
//
//
//
//
//	// Register this account with the server.
////	public void register(final Context context, String name, String email, final String regId) {
////
////		Log.i(Config.TAG, "registering device (regId = " + regId + ")");
////
////		String serverUrl = Config.YOUR_SERVER_URL;
////
////		Map<String, String> params = new HashMap<String, String>();
////		params.put("regId", regId);
////		params.put("name", name);
////		params.put("email", email);
////
////		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
////
////		// Once GCM returns a registration id, we need to register on our server
////		// As the server might be down, we will retry it a couple
////		// times.
////		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
////
////			Log.d(Config.TAG, "Attempt #" + i + " to register");
////
////			try {
////				//Send Broadcast to Show message on screen
////				displayMessageOnScreen(context, context.getString(
////						R.string.server_registering, i, MAX_ATTEMPTS));
////
////				// Post registration values to web server
////				post(serverUrl, params);
////
////				GCMRegistrar.setRegisteredOnServer(context, true);
////
////				//Send Broadcast to Show message on screen
////				String message = context.getString(R.string.server_registered);
////				displayMessageOnScreen(context, message);
////
////				return;
////			} catch (IOException e) {
////
////				// Here we are simplifying and retrying on any error; in a real
////				// application, it should retry only on unrecoverable errors
////				// (like HTTP error code 503).
////
////				Log.e(Config.TAG, "Failed to register on attempt " + i + ":" + e);
////
////				if (i == MAX_ATTEMPTS) {
////					break;
////				}
////				try {
////
////					Log.d(Config.TAG, "Sleeping for " + backoff + " ms before retry");
////					Thread.sleep(backoff);
////
////				} catch (InterruptedException e1) {
////					// Activity finished before we complete - exit.
////					Log.d(Config.TAG, "Thread interrupted: abort remaining retries!");
////					Thread.currentThread().interrupt();
////					return;
////				}
////
////				// increase backoff exponentially
////				backoff *= 2;
////			}
////		}
////
////		String message = context.getString(R.string.server_register_error,
////				MAX_ATTEMPTS);
////
////		//Send Broadcast to Show message on screen
////		displayMessageOnScreen(context, message);
////	}
//
//	// Unregister this account/device pair within the server.
////	public void unregister(final Context context, final String regId) {
////
////		Log.i(Config.TAG, "unregistering device (regId = " + regId + ")");
////
////		String serverUrl = Config.YOUR_SERVER_URL + "/unregister";
////		Map<String, String> params = new HashMap<String, String>();
////		params.put("regId", regId);
////
////		try {
////			post(serverUrl, params);
////			GCMRegistrar.setRegisteredOnServer(context, false);
////			String message = context.getString(R.string.server_unregistered);
////			displayMessageOnScreen(context, message);
////		} catch (IOException e) {
////
////			// At this point the device is unregistered from GCM, but still
////			// registered in the our server.
////			// We could try to unregister again, but it is not necessary:
////			// if the server tries to send a message to the device, it will get
////			// a "NotRegistered" error message and should unregister the device.
////
////			String message = context.getString(R.string.server_unregister_error,
////					e.getMessage());
////			displayMessageOnScreen(context, message);
////		}
////	}
//
//	// Issue a POST request to the server.
//	private static void post(String endpoint, Map<String, String> params)
//			throws IOException {
//
//		URL url;
//		try {
//
//			url = new URL(endpoint);
//
//		} catch (MalformedURLException e) {
//			throw new IllegalArgumentException("invalid url: " + endpoint);
//		}
//
//		StringBuilder bodyBuilder = new StringBuilder();
//		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
//
//		// constructs the POST body using the parameters
//		while (iterator.hasNext()) {
//			Entry<String, String> param = iterator.next();
//			bodyBuilder.append(param.getKey()).append('=')
//			.append(param.getValue());
//			if (iterator.hasNext()) {
//				bodyBuilder.append('&');
//			}
//		}
//
//		String body = bodyBuilder.toString();
//
//		Log.v(Config.TAG, "Posting '" + body + "' to " + url);
//
//		byte[] bytes = body.getBytes();
//
//		HttpURLConnection conn = null;
//		try {
//
//			Log.e("URL", "> " + url);
//
//			conn = (HttpURLConnection) url.openConnection();
//			conn.setDoOutput(true);
//			conn.setUseCaches(false);
//			conn.setFixedLengthStreamingMode(bytes.length);
//			conn.setRequestMethod("POST");
//			conn.setRequestProperty("Content-Type",
//					"application/x-www-form-urlencoded;charset=UTF-8");
//			// post the request
//			OutputStream out = conn.getOutputStream();
//			out.write(bytes);
//			out.close();
//
//			// handle the response
//			int status = conn.getResponseCode();
//
//			// If response is not success
//			if (status != 200) {
//
//				throw new IOException("Post failed with error code " + status);
//			}
//		} finally {
//			if (conn != null) {
//				conn.disconnect();
//			}
//		}
//	}
//
//
//
//	// Checking for all possible internet providers
//	public boolean isConnectingToInternet(){
//
//		ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//		if (connectivity != null)
//		{
//			NetworkInfo[] info = connectivity.getAllNetworkInfo();
//			if (info != null)
//				for (int i = 0; i < info.length; i++)
//					if (info[i].getState() == NetworkInfo.State.CONNECTED)
//					{
//						return true;
//					}
//
//		}
//		return false;
//	}
//
//	// Notifies UI to display a message.
//	public void displayMessageOnScreen(Context context, String message) {
//
//		Intent intent = new Intent(Config.DISPLAY_MESSAGE_ACTION);
//		intent.putExtra(Config.EXTRA_MESSAGE, message);
//
//		// Send Broadcast to Broadcast receiver with message
//		context.sendBroadcast(intent);
//
//	}
//
//
//	//Function to display simple Alert Dialog
////	public void showAlertDialog(Context context, String title, String message,Boolean status) {
////		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
////
////		// Set Dialog Title
////		alertDialog.setTitle(title);
////
////		// Set Dialog Message
////		alertDialog.setMessage(message);
////
////		if(status != null)
////			// Set alert dialog icon
////			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
////
////		// Set OK Button
////		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
////			public void onClick(DialogInterface dialog, int which) {
////
////			}
////		});
////
////		// Show Alert Message
////		alertDialog.show();
////	}
//
//	private PowerManager.WakeLock wakeLock;
//
//	public  void acquireWakeLock(Context context) {
//		if (wakeLock != null) wakeLock.release();
//
//		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//
//		wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
//				PowerManager.ACQUIRE_CAUSES_WAKEUP |
//				PowerManager.ON_AFTER_RELEASE, "WakeLock");
//
//		wakeLock.acquire();
//	}
//
//	public  void releaseWakeLock() {
//		if (wakeLock != null) wakeLock.release(); wakeLock = null;
//	}


   
   


}
