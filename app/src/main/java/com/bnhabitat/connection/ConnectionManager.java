package com.bnhabitat.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.AndroidHttpClient;
import android.util.Log;

import com.bnhabitat.helper.InsecureSSLSocketFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;


public class ConnectionManager {
    private static ConnectionManager sharedInstance;
    private static long TIME_OUT_IN_SECONDS = 120;
    private Context context;
    private HttpClient client;
    String serviceVersion = "v0.0.1";

    public static ConnectionManager getInstance(Context context) {

        if (sharedInstance == null) {
            sharedInstance = new ConnectionManager(context);
            return sharedInstance;
        }
        return sharedInstance;
    }

    public ConnectionManager(Context context) {
        this.context = context;
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {

            NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
            return activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
        }

        return false;
    }


//
public InputStream connectionEstablishedGet(String url, String accessToken) {

    InputStream inputStream = null;
    HttpParams httpParameters = new BasicHttpParams();
    HttpClient client = getSSLClient();
    HttpResponse response;

    if (url.startsWith("https")) {
        httpParameters = client.getParams();
    }
    try {


        int timeoutConnection = 40000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = 45000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Content-type", "application/json");
        httpGet.addHeader("Token", accessToken);

        if (url.startsWith("https"))
            response = client.execute(httpGet);
        else
            response = httpClient.execute(httpGet);


        StatusLine statusLine = response.getStatusLine();


        Log.e("Token", accessToken);

        if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            inputStream = response.getEntity().getContent();

        } else if (statusLine.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {

            response.getEntity().getContent().close();
            String json = "{\"StatusCode\": 401}";
            inputStream = new ByteArrayInputStream(json.getBytes());
            ;
            return inputStream;

        } else {

            response.getEntity().getContent().close();
            inputStream = null;
            return inputStream;
        }

    } catch (Exception e) {
        e.printStackTrace();
    }

    return inputStream;
}


    public InputStream connectionEstablishedGet(String url, String accessToken,String username) {

        InputStream inputStream = null;
        HttpParams httpParameters = new BasicHttpParams();
        HttpClient client = getSSLClient();
        HttpResponse response;

        if (url.startsWith("https")) {
            httpParameters = client.getParams();
        }
        try {


            int timeoutConnection = 40000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 45000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Content-type", "application/json");
            httpGet.addHeader("AuthToken", accessToken);
            httpGet.addHeader("UserId", username);


            if (url.startsWith("https"))
                response = client.execute(httpGet);
            else
                response = httpClient.execute(httpGet);


            StatusLine statusLine = response.getStatusLine();


            Log.e("Token", accessToken);
            Log.e("Token", statusLine.toString());

            if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
//                String json = "{\"StatusCode\": 200}";
                inputStream = response.getEntity().getContent();

            } else if (statusLine.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {

                response.getEntity().getContent().close();
                String json = "{\"StatusCode\": 401}";
                inputStream = new ByteArrayInputStream(json.getBytes());
                ;
                return inputStream;

            }else if (statusLine.getStatusCode() == HttpStatus.SC_FORBIDDEN) {

                response.getEntity().getContent().close();
                String json = "{\"StatusCode\": 403}";
                inputStream = new ByteArrayInputStream(json.getBytes());
                ;
                return inputStream;

            } else {

                response.getEntity().getContent().close();
                inputStream = null;


                return inputStream;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return inputStream;
    }
    public InputStream connectionEstablished(String mUrl, String accessToken) {


        HttpParams httpParameters = new BasicHttpParams();
        DefaultHttpClient sHttpclient;
        HttpPost sHttppost;
        HttpResponse sResponse;
        InputStream mInputStreamis = null;
        HttpClient client = getSSLClient();

        if (mUrl.startsWith("https")) {
            httpParameters = client.getParams();
        }

        try {
            sHttppost = new HttpPost(mUrl);
            sHttppost.addHeader("Content-Type", "application/json");
            sHttppost.setHeader("Token", accessToken);

            int timeoutConnection = 40000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 45000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            sHttpclient = new DefaultHttpClient(httpParameters);

            if (mUrl.startsWith("https"))
                sResponse = client.execute(sHttppost);
            else
                sResponse = sHttpclient.execute(sHttppost);

            HttpEntity sEntity = sResponse.getEntity();
            mInputStreamis = sEntity.getContent();

            Log.e("Token", accessToken);

        } catch (ConnectTimeoutException e) {
            Log.e("Connection time Out", "Error");
        } catch (Exception e) {

        }
        return mInputStreamis;
    }
    public InputStream connectionEstablished(String mUrl, String accessToken,String username) {


        HttpParams httpParameters = new BasicHttpParams();
        DefaultHttpClient sHttpclient;
        HttpPost sHttppost;
        HttpResponse sResponse;
        InputStream mInputStreamis = null;
        HttpClient client = getSSLClient();

        if (mUrl.startsWith("https")) {
            httpParameters = client.getParams();
        }

        try {
            sHttppost = new HttpPost(mUrl);
            sHttppost.addHeader("Content-Type", "application/json");
            sHttppost.addHeader("AuthToken", accessToken);
            sHttppost.addHeader("UserId", username);

            int timeoutConnection = 40000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 45000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            sHttpclient = new DefaultHttpClient(httpParameters);

            if (mUrl.startsWith("https"))
                sResponse = client.execute(sHttppost);
            else
                sResponse = sHttpclient.execute(sHttppost);

            HttpEntity sEntity = sResponse.getEntity();
            mInputStreamis = sEntity.getContent();

            Log.e("Token", accessToken);

        } catch (ConnectTimeoutException e) {
            Log.e("Connection time Out", "Error");
        } catch (Exception e) {

        }
        return mInputStreamis;
    }
    public  String excutePost(String mUrl, String mInput) {
        DefaultHttpClient sHttpclient;
        HttpPost sHttppost;
        HttpResponse sResponse;
        InputStream mInputStreamis = null;
        HttpParams httpParameters = new BasicHttpParams();
        HttpClient client = getSSLClient();
        String res="";
        if (mUrl.startsWith("https")) {
            httpParameters = client.getParams();
        }

        try {
            sHttppost = new HttpPost(mUrl);
            sHttppost.setEntity(new StringEntity(mInput));
//            sHttppost.setHeader("Accept", "application/json");
//            sHttppost.addHeader("AuthToken", accessToken);
//            sHttppost.addHeader("UserId", username);
            sHttppost.addHeader("Content-Type", "application/json");



            int timeoutConnection = 40000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 45000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            sHttpclient = new DefaultHttpClient(httpParameters);

            if (mUrl.startsWith("https"))
                sResponse = client.execute(sHttppost);
            else
                sResponse = sHttpclient.execute(sHttppost);

            HttpEntity sEntity = sResponse.getEntity();
            mInputStreamis = sEntity.getContent();
         res=converResponseToString(mInputStreamis);
//            Log.e("Token", accessToken);
        } catch (ConnectTimeoutException e) {
            Log.e("Connection time Out", "Error");
        } catch (Exception e) {
        }
        return res;
    }

//    public InputStream connectionTokenEstablished(String mUrl, String userId, String authToken, Context context) {
//
//        HttpParams httpParameters = new BasicHttpParams();
//        HttpClient sHttpclient;
//        HttpPost sHttppost;
//        HttpResponse sResponse;
//        InputStream mInputStreamis = null;
////        HttpClient client = getSSLClient();
//
////        if (mUrl.startsWith("https")) {
////            httpParameters = client.getParams();
////        }
//
//        try {
//            sHttpclient = new DefaultHttpClient();
//            sHttppost = new HttpPost(mUrl);
//            sHttppost.addHeader("Content-Type", "application/json");
//            List<BasicNameValuePair> params = new ArrayList<>();
//            params.add(new BasicNameValuePair("UserId",userId));
//            params.add(new BasicNameValuePair("AuthKey", authToken));
//            sHttppost.setEntity(new UrlEncodedFormEntity(params));
////            sHttppost.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(userId, authToken), "UTF-8", false));
//
//            int timeoutConnection = 40000;
//            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//            int timeoutSocket = 45000;
//            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//
//
//            if (mUrl.startsWith("https"))
//                sResponse = sHttpclient.execute(sHttppost);
//            else
//                sResponse = sHttpclient.execute(sHttppost);
//
//            HttpEntity sEntity = sResponse.getEntity();
//
//
////            mInputStreamis = sEntity.getContent();
////
////            for (Header header : sResponse.getAllHeaders()) {
////                if (header.getName().equalsIgnoreCase("token")) {
////                    Log.e("Token Value", header.getValue() + "==");
////
////                    PreferenceConnector.getInstance(context).savePreferences(Constants.APP_TOKEN, header.getValue());
////                }
////            }
//        } catch (ConnectTimeoutException e) {
//            Log.e("Connection time Out", "Error");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return mInputStreamis;
//    }


    public InputStream connectionEstablished(String mUrl, String mInput, String accessToken,String username) {
        DefaultHttpClient sHttpclient;
        HttpPost sHttppost;
        HttpResponse sResponse;
        InputStream mInputStreamis = null;
        HttpParams httpParameters = new BasicHttpParams();
        HttpClient client = getSSLClient();

        if (mUrl.startsWith("https")) {
            httpParameters = client.getParams();
        }

        try {
            sHttppost = new HttpPost(mUrl);
            sHttppost.setEntity(new StringEntity(mInput));
            sHttppost.setHeader("Accept", "application/json");
            sHttppost.addHeader("AuthToken", accessToken);
            sHttppost.addHeader("UserId", username);
            sHttppost.addHeader("Content-Type", "application/json");



            int timeoutConnection = 40000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 45000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            sHttpclient = new DefaultHttpClient(httpParameters);

            if (mUrl.startsWith("https"))
                sResponse = client.execute(sHttppost);
            else
                sResponse = sHttpclient.execute(sHttppost);

            HttpEntity sEntity = sResponse.getEntity();
            mInputStreamis = sEntity.getContent();

            Log.e("Token", accessToken);
        } catch (ConnectTimeoutException e) {
            Log.e("Connection time Out", "Error");
        } catch (Exception e) {
        }
        return mInputStreamis;
    }

    public String converResponseToString(InputStream InputStream) {
        String mResult = "";
        StringBuilder mStringBuilder;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(InputStream, "UTF8"), 8);
            mStringBuilder = new StringBuilder();
            mStringBuilder.append(reader.readLine() + "\n");
            String line = "0";
            while ((line = reader.readLine()) != null) {
                mStringBuilder.append(line + "\n");
            }
            InputStream.close();
            mResult = mStringBuilder.toString();
            Log.d("Login result Array==", mResult);
            return mResult;
        } catch (Exception e) {
            return mResult;
        }
    }


    private HttpClient getSSLClient() {

        AndroidHttpClient insecure = AndroidHttpClient
                .newInstance("builderupload/" + serviceVersion);
        try {
            KeyStore store = KeyStore.getInstance(KeyStore.getDefaultType());
            store.load(null, null);

            SSLSocketFactory sslFactory = new InsecureSSLSocketFactory(store);
            sslFactory
                    .setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 3080));
            registry.register(new Scheme("https", sslFactory, 443));

            SingleClientConnManager manager = new SingleClientConnManager(
                    insecure.getParams(), registry);
            client = new DefaultHttpClient(manager, insecure.getParams());

        } catch (KeyManagementException e) {
            Log.e(this.getClass().getCanonicalName(), e.getClass().getName(), e);

        } catch (UnrecoverableKeyException e) {
            Log.e(this.getClass().getCanonicalName(), e.getClass().getName(), e);

        } catch (NoSuchAlgorithmException e) {
            Log.e(this.getClass().getCanonicalName(), e.getClass().getName(), e);

        } catch (KeyStoreException e) {
            Log.e(this.getClass().getCanonicalName(), e.getClass().getName(), e);

        } catch (CertificateException e) {
            Log.e(this.getClass().getCanonicalName(), e.getClass().getName(), e);

        } catch (IOException e) {
            Log.e(this.getClass().getCanonicalName(), e.getClass().getName(), e);

        }
        insecure.close();

        return client;
    }


}
